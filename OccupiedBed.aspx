﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="OccupiedBed.aspx.cs" Inherits="OccupiedBed" Title="OccupiedBed" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <style type="text/css">
        #tbsearch td
        {
            text-align: left;
            vertical-align: middle;
        }
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        #gvcol div
        {
            margin-top: -10px;
        }
        .head1
        {
            width: 25%;
        }
        .head2
        {
            width: 20%;
        }
        .head3
        {
            width: 20%;
        }
        .head4
        {
            width: 25%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Occupied Patient Details</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                     <li class="qtab-Demo first" id="li1"><a class="qt_tab active" href="WardNBedStatus.aspx">
                                            Bed Details</a> </li>
                                        <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">
                                            Occupied Bed Details</a> </li>
                                        <li runat="server" class="qtab-HTML  last" id="li_9"><a class="qt_tab active" href="UnOccupiedBed.aspx">
                                            Unoccupied Bed Details</a> </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto;">
                                        <div style="padding: 15px; height: auto;">
                                            <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblGridHeader" runat="server" Style="float: right; color: Red;"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                            <tr>
                                                                <th class="head1">
                                                                    Name
                                                                </th>
                                                                <th class="head2">
                                                                    Ward No
                                                                </th>
                                                                <th class="head3">
                                                                    Bed No
                                                                </th>
                                                                <th class="head4">
                                                                    In Date
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th class="head1">
                                                                    <asp:TextBox ID="txtName" runat="server" class="field" Width="200px"></asp:TextBox>
                                                                </th>
                                                                <th class="head2">
                                                                    <asp:TextBox ID="txtward" runat="server" class="field" Width="100px"></asp:TextBox>
                                                                </th>
                                                                <th class="head3">
                                                                    <asp:TextBox ID="txtBed" runat="server" class="field" Width="100px"></asp:TextBox>
                                                                </th>
                                                                <th class="head4">
                                                                    <div style="display: block;">
                                                                        <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none; color: #FFF;
                                                                            text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click">
                                                                <img src="images/filter.png" style="border:0px;"/>
                                                                Filter</asp:LinkButton>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="gvcol" colspan="3">
                                                        <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                            CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" ShowHeader="false" Width="100%"
                                                            OnPageIndexChanging="GRV1_PageIndexChanging" EmptyDataText="No records found...">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("PName")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("WNO")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("BedNo")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("InDate", "{0:dd-MMM-yyyy}")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
