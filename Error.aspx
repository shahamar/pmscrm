﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Erro</title>

<%-- <link rel="stylesheet" type="text/css" href="css/style.css" />--%>   
 
<style type="text/css">

body {margin:0;}
.style5 {	
	font-size: 20px;
	font-family: Calibri,Arial,sans-serif,Tahoma;
	color: #fff;
	font-weight:400;

}	
.style1 
{	
	font-size: 100px;
	font-family: Calibri,Arial,sans-serif,Tahoma;
	color: #F27400;
	font-weight:bold;
}
h2{font-size:30px;font-family: Calibri,Arial,sans-serif,Tahoma;}
h3{font-size:24px;font-family: Calibri,Arial,sans-serif,Tahoma;}
.NFButton 
{
    background: #333;
    border: medium none;
    color: #ffffff;
    cursor: pointer;
    font-weight: bold;
    height: 33px;
    margin-top: -2px;
    padding: 0 15px;
    vertical-align: middle;
    width: auto;
}
</style>

</head>
<body>
    <form id="form1" runat="server">
  
	<center>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" valign="middle" bgcolor="#768607">
					<table width="1000px" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="250px" align="center" valign="middle" height="90px">
								<img src="images/logo.png" style="margin-left:20px" alt="Aditya" width="300px" border="0" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
   
			<tr>
				<td bgcolor="#f27400"><div style="height:5px;">&nbsp;</div></td>
			</tr>
			<tr>
				<td height="60">&nbsp;</td>
			</tr>

			<tr>
				<td  align="center" valign="top">
					<div class="main-body">
						<table width="600" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="600" align="center" valign="top" class="p1">
									<table width="95%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center" valign="middle">&nbsp;</td>
										</tr>
										<tr>
											<td align="center" valign="top"><span class="style1">Oops! </span>
											<h2>Error - Page not found</h2></td>
										</tr>
										<tr>
											<td align="center"><h3>Please Check the URL</h3></td>
										</tr> 
										<tr>
											<td align="center"><h3>otherwise, <a href="Home.aspx">Click here</a> to be redirected to the homepage</h3></td>
										</tr>										
									</table>
								</td>
							</tr>
						</table>
				   </div>
				</td>
		    </tr>
					
			<tr valign="bottom">			  
				<td style="height:65px;"></td>
			</tr>

			<tr valign="bottom">
				<td align="center" bgcolor="#768607">
				<span class="style5">PMS : CRM | Powered by </span><a href="http://www.ocs.net.in" class="style4" style="text-decoration:none;">OCS</a></td>
			</tr>
		</table>
    </center>
</form>
</body>
</html>