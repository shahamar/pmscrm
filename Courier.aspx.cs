﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Courier : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen=new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string userrole;
    string strSQL;
    int CID = 0;
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["cmId"] != null)
            CID = Convert.ToInt32(Request.QueryString["cmId"].ToString());

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 34";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();

        if (!IsPostBack)
        {
            
            FillData();

        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
       
        if (CID == 0)
            strSQL = "sp_InsCourierMaster";
        else
            strSQL = "sp_UpdtCourierMaster";

        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@cmCourierName", txtCourierName.Text.Trim());
        SqlCmd.Parameters.AddWithValue("@cmContactPerson", txtContactPerson.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmMobileNo", txtMobileNo.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmOfficeNo", txtOfficeNo.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmEmailId", txtEmailID.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmRemark", txtRemark.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmCreatedBy", userid);
        SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
        if (CID != 0)
        {
            SqlCmd.Parameters.AddWithValue("@cmID", CID);

      }
       
       if (con.State == ConnectionState.Closed)
            con.Open();
        int reslt;
        SqlCmd.ExecuteNonQuery();
        reslt = (int)SqlCmd.Parameters["@Error"].Value;
        if (reslt == 0)
        {
            if (CID == 0)
            {
                Response.Redirect("Courier.aspx?msg=Courier details saved successfully");
            }
            else
            {
                Response.Redirect("ManageCourier.aspx?msg=1");
            }
        }

        SqlCmd.Dispose();
        con.Close();

    }

    private void FillData()
    {
        if (CID != 0)
        {


            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "Select * from dbo.tblCourierMaster Where cmId= " + CID + " ";
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader sqldr = SqlCmd.ExecuteReader();
            while (sqldr.Read())
            {
                txtCourierName.Text = sqldr["cmCourierName"].ToString();
                txtContactPerson.Text = sqldr["cmContactPerson"].ToString();
                txtMobileNo.Text = sqldr["cmMobileNo"].ToString();
                txtOfficeNo.Text = sqldr["cmOfficeNo"].ToString();
                txtEmailID.Text = sqldr["cmEmailId"].ToString();
                txtRemark.Text = sqldr["cmRemark"].ToString();
            }

            sqldr.Close();
            sqldr.Dispose();
            con.Close();
            con.Dispose();
        }
    }
}
