﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewSelectedScanDoc : System.Web.UI.Page
{
    string CPNO = "";
    private string path = @"c:\PDF_Files\";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["CPNO"] != null)
        {
            CPNO = Request.QueryString["CPNO"].ToString();
            CPNO = CPNO.Replace("/", "_");
        }

        string str = "";
        str = "~/UploadScanDocuments/" + CPNO + ".PDF#toolbar=0";
        //Response.Redirect("995_46.PDF#toolbar=0");
        Response.Redirect(str);
    }
}