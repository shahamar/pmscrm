USE [PMS]
GO

/****** Object:  StoredProcedure [dbo].[Sp_GetConsultingReport]    Script Date: 04/17/2013 15:45:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_GetConsultingReport]
(
     @frmDate SMALLDATETIME      
	,@toDate SMALLDATETIME  
)
AS 
BEGIN
    
    SELECT 
			fdDate
		   ,PD.pdInitial + ' ' + PD.pdFname + ' ' + PD.pdMname + '' + PD.pdLname  'Name'
		   ,'CONSULTING' AS 'N OF S'
		   ,fdConsultingCharges
           ,ROW_NUMBER()OVER(PARTITION by  fdDate order by fdDate asc)as 'rank'
  FROM  
		  tblFollowUpDatails fud 
		  INNER JOIN 
		  tblPatientDetails pd ON fud.fdPatientId=PD.pdID
  WHERE 
		  fud.fdFlag='C' AND fud.fdDate between @frmDate AND @toDate
  ORDER BY fdDate ASC
    
END
 

GO


