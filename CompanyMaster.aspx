<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CompanyMaster.aspx.cs" Inherits="CompanyManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Company Master</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
	<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
</head>
<body style="background-color:#FFFFFF;">
	<form id="form1" runat="server">
	<div>
		<asp:ScriptManager ID="ScriptManager1" runat="server">
		</asp:ScriptManager>
		<table style="width:100%; background-color:#FFFFFF;">
			<tr>
				<td></td>
			</tr>
			<tr>
				<td>
				<div class="login-area margin_ten_right" style="width:100%;">
					<h2>Company Master</h2>
					<div class="formmenu">
						<div class="loginform">
							<div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel" style="height:auto;border:1.5px solid #E2E2E2;">
								<div style="padding:15px; height:auto;">
								<table width="100%">
									
									<tr>
										<td colspan="6" style="text-align: center; color: Red;"><asp:Label ID="lblMessage" runat="server"></asp:Label></td>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td style="border: 0px solid #EFEFEF; font-size:15px;" cellspacing="5px" cellpadding="2px" width="150px">Company Name:</td>
										<td style="padding-right:10px;"  width="10%"><asp:TextBox ID="txtCompany" runat="server" CssClass="field"></asp:TextBox>
														<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCompany" Display="Dynamic" ErrorMessage="Please Enter Company Name...!" ValidationGroup="val">
                                                    								<span class="error" >Please Enter Company Name...!</span>
								                                                </asp:RequiredFieldValidator></td>
										<td colspan="2" align="Left"><asp:Button ID="btn_Save" CssClass="textbutton b_submit" Text="Save" runat="server" OnClick="btnSave_Click" ValidationGroup="val" /></td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
                                                    				<td colspan="3">
                                                        				<table  class="mGrid" border="0" cellpadding="0" cellspacing="0" style="width: 80%">
                                                    					<tr>
			                                                        		<th>Company Name</th>
		                                                			        <th>Action</th>
                		                                			</tr>
                                		                    			<tr>
                                                		        			<th width="30%">
			                                        		                    <asp:TextBox ID="txtCompanyName" runat="server" class="field" Width="210px"></asp:TextBox>
                        			                                		</th>
		                                                			        <th class="head6" width="15%">
                                                					            <div style="display: block;">
			                                        		                        <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none;
                        			                                		            text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click" >
		                                                	        			<img src="images/filter.png" style="border:0px;"/>&nbsp; &nbsp;Filter
                		                                        			        </asp:LinkButton>
			        		                                                    </div>
                        					                                </th>
                                                					</tr>
                                                    					<tr>
		                                                        			<td id="gvcol" colspan="2">
					                                                            <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                        					                                        CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="10" ShowHeader="false" Width="100%"
                                                					                OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                                					<Columns>
                   				                                		                 <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left">
                                                			                        		 <ItemTemplate>
		                                                                         			      <%# Eval("Cname")%>
					                                                                         </ItemTemplate>
                        					                                                 </asp:TemplateField>
                                                                   
                                                	                  
                                                        		          				 <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
			                                                		                         <ItemTemplate>
                           			                                        		          <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("Cid") %>'
                                                			                                		CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
		                                                                        			  <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("Cid") %>'
					                                                                                CommandName="del" ImageUrl="Images/delete.ico" OnClientClick="return confirm('Are you sure you want to delete this?');"
                        					                                                        ToolTip="Delete" />
                                                						                 </ItemTemplate>
			                                        		                                <ItemStyle HorizontalAlign="Center" />
                        			                                		                 </asp:TemplateField>
		                                                     				       </Columns>
					                                                            </asp:GridView>
                        					                                </td>
                                                					</tr>
		                                                		        <tr>
                    			                                        		<td>
		                                            				                &nbsp;
					                                                        </td>
                        					                                <td>
                                                					                &nbsp;
			                                        		                </td>
			                       			                        </tr>		
											</table>
										</td>
									</tr>
						
								</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				</td>
			</tr>
		</table>
	</div>
	</form>
</body>
</html>