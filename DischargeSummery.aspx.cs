﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class DischargeSummery : System.Web.UI.Page
{
    string strSQL = "";
    genral gen = new genral();
    string idID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
            idID = Request.QueryString["id"].ToString();

        FillData();

    }
 
    protected void FillData()
    {
        object o = null;
        strSQL = "select distinct(ipdId),pdID,CONVERT(VARCHAR(10), ipdAdmissionDate, 105) AS DOA,CONVERT(VARCHAR(10), ipdDischargeDate, 105) AS DOC,pdReceiptNo,isnull(pdAge,'NA')pdAge,pdSex,isnull(ipdRefBy,'NA')ipdRefBy,pdInitial +' '+pdFname +' '+pdMname +' '+ pdLname AS 'PName',ipdFinalDiagnosis,[dbo].[TestList](ipdId) as 'Investigation'";
        strSQL += "from tblIpdMaster i  inner join tblPatientDetails p on i.ipdpdId =p.pdID left join tblIpdTestDetails itd on  itd.itdipdId = i.ipdId left join tblTestMaster t on t.tmId = itd.itdtmId   left join tblFollowUpDatails f on f.fdpatientID=p.pdID";
        strSQL += " Where ipdId  = " + idID + "";
        DataTable dt = gen.getDataTable(strSQL);
        rptParent.DataSource = dt;
        rptParent.DataBind();
        foreach (RepeaterItem itm in rptParent.Items)
        {
            HiddenField hdnpdID = (HiddenField)itm.FindControl("hdnpdID");
            Literal litReConsultCharges = (Literal)itm.FindControl("litReConsultCharges");
            HtmlTableRow tr_1 = (HtmlTableRow)itm.FindControl("tr_1");
            
            hdnpdID.Value = dt.Rows[0]["pdID"].ToString();

            strSQL = "SELECT [dbo].[GetReconsultingAmt] (" + hdnpdID.Value + ")";
            o = gen.executeScalar(strSQL);
            litReConsultCharges.Text =Convert.ToString(o);
            if (litReConsultCharges.Text == "")
            {
                tr_1.Visible = false;
            }
        }
        
    }

}
