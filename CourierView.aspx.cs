﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class CourierView : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole,msg;
    int userid;
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 36";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindGVR();
        }

    }

    public void BindGVR()
    {
        string[] _frm = txtfrm.Text.ToString().Split('-');
        string[] _to = txtto.Text.ToString().Split('-');

        string frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
        string to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";


        strSQL = "Select * From tblCourierMedicinsDetails cmd inner join tblCourierMaster cm on cm.cmID=cmd.cmID where 1=1 AND IsDelete = 0";

        if (chkCourierDate.Checked == true)
            strSQL += " AND cmdDate BETWEEN '" + frm + "' and '" + to + "'";

        if (txtTest.Text.ToString() != "")
            strSQL += " AND cmdName Like '%" + txtTest.Text.ToString() + "%'";

        strSQL += " order by cmdDate desc";

        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        createpaging(dt.Rows.Count, GRV1.PageCount);
    }

    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string pdId = e.CommandArgument.ToString();
        if (e.CommandName == "edt")
        {
            int aId = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("UpdateCourierDetails.aspx?cmId=" + aId);
        }

        if (e.CommandName == "del")
        {
            try
            {
                int cmdId = Convert.ToInt32(e.CommandArgument);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                SqlCmd = new SqlCommand();
                SqlCmd.Connection = con;
                SqlCmd.CommandText = "proc_DeleteCourierMedicineDetails";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@cmdID", cmdId);

                SqlCmd.ExecuteNonQuery();

                Response.Redirect("CourierView.aspx?msg=Courier Details deleted successfully");
            }
            catch (Exception ex)
            {
                lblMessage.Text = "There is some problem.Data not deleted";
            }
        }
    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;
    }
}
