﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
using System.IO;

public partial class OnlineWorkshop : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string Uroll = "0";
    int Page_no = 0, Page_size = 20;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (Session["urole"] != null)
            Uroll = Session["urole"].ToString();

        if(!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindOnlineWorkshop();
        }
        System.Web.UI.ScriptManager.GetCurrent(this).RegisterPostBackControl(imgexport);
    }
    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        ExportGridToExcel();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        //required to avoid the runtime error "  
        //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
    }
    private void ExportGridToExcel()
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string FileName = "WorkshopReg" + DateTime.Now + ".xls";
        StringWriter strwritter = new StringWriter();
        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        GRV1.AllowPaging = false;
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        GRV1.GridLines = GridLines.Both;
        GRV1.HeaderStyle.Font.Bold = true;
        GRV1.RenderControl(htmltextwrtter);
        Response.Write(strwritter.ToString());
        Response.End();
    }



    protected void imgexport_Click1(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "WorkshopReg.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRV1.AllowPaging = false;
        this.BindGridView();
        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    private void BindGridView()
    {

        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];

        StrSQL = "GetOnlineWorkshopDetails";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FrmDate", Frmdate);
        SqlCmd.Parameters.AddWithValue("@ToDate", Todate);
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                //pnlHead.Visible = true;
                GRV1.DataSource = dt;
                GRV1.DataBind();


                export.Style.Add("display", "block");
                createpaging(dt.Rows.Count, GRV1.PageCount);

            }
            else
            {
                //pnlHead.Visible = false;
                export.Style.Add("display", "none");
                //lblGridHeader.Text = "No record found";
            }
        }
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        //lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

    private void BindOnlineWorkshop()
    {
        lblSubTotal.Text = "0.00";
        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];

        StrSQL = "GetOnlineWorkshopDetails";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FrmDate", Frmdate);
        SqlCmd.Parameters.AddWithValue("@ToDate", Todate);
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();


                decimal Total = 0;

                foreach (GridViewRow row in GRV1.Rows)
                {

                    Label lblsub = (Label)row.FindControl("lblPayment");
                    if (lblsub.Text == "")
                    {
                        Total += 0;
                        lblsub.Text = "0";
                    }
                    else
                    {
                        Total += Decimal.Parse(lblsub.Text.ToString());
                    }

                }
                decimal tot = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string total = "0";
                    decimal temp = 0;
                    total = (dt.Rows[i]["PaidAmt"].ToString());

                    temp = total.ToString() == "" ? Decimal.Parse("{0:0.00}") : Decimal.Parse(total.ToString());
                    tot += temp;
                }

                
                lblGrandTotal.Text = tot.ToString("f2");
                lblSubTotal.Text = Total.ToString();
                
                decimal subtot = decimal.Parse(lblSubTotal.Text);
                lblSubTotal.Text = subtot.ToString();
                Label1.Visible = true;
                lblSubTotal.Visible = true;
            }
            else
            {
                GRV1.DataSource = null;
                GRV1.DataBind();
            }
            conn.Close();
            dr.Dispose();

        }
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindOnlineWorkshop();
    }
    protected void GRV_Workshop_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindOnlineWorkshop();
    }
    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //string WrId = e.CommandArgument.ToString();
        //if (e.CommandName == "_ADDPayment")
        //{
        //    Session["WrId"] = WrId;
        //    Response.Redirect("WorkshopAddPayment.aspx?WrId=" + WrId);
        //}
    }
}