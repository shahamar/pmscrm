<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="TodaysList.aspx.cs" Inherits="TodaysList"  EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker" TagPrefix="MKB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<style>
.AksDisplay {display:none;}
.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
 <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <div class="login-area margin_ten_right" style="width: 100%; text-align:center">
                <h2><asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h2>
                       <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                          <table style="width: 100%; background: #f2f2f2">
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="width: 98%;">
                                                        <h2>
                                                            Todays Consultation List <span class="fltrt" Style=""><a href="SearchPatient.aspx?AppType=T" class='example7'>
                                                                <asp:ImageButton ID="img_OPD" Width="60px" Style="vertical-align: middle; color: DarkRed !important;
                                                                    font-weight: bold;" runat="server" ImageUrl="~/images/add-button-blue-th.png"
                                                                    ToolTip="Add Patient" />
                                                            </a></span>
                                                        </h2>
                                                        <div class="formmenu">
                                                        <div class="loginform">
                                                        <ul class="quicktabs_tabs quicktabs-style-excel">
                                                            <li class="qtab-Demo active first" id="li_1">
                                    	                        <a class="qt_tab active" href="javascript:void(0)">Todays List</a> 
                                                            </li>
                                                            <li class="qtab-HTML last" id="li_9">
                                    	                        <a class="qt_tab active" href="TodaysListReport.aspx">Todays List Report</a> 
                                                            </li>
                                                        </ul>
                                                        </div>
                                                            </div><br />
							<div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
							<table style="width:100%; background:#f2f2f2;" cellspacing="0px" cellpadding="5px">
								<tr>
									<td width="40%" align="Right" Style="padding-right:30px;"><strong>From Date :</strong>
										<asp:TextBox ID="txtfrm" runat="server" CssClass="field textbox"></asp:TextBox>
										<cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                    				runat="server" Enabled="True" TargetControlID="txtfrm">
				                                                </cc1:CalendarExtender>&nbsp;
                                				                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"  />
									</td>
									<td width="15%" align="left"><strong>To :&nbsp;</strong>
										<asp:TextBox ID="txtto" runat="server" CssClass="field textbox"></asp:TextBox>
                                                				<cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                    				Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                				</cc1:CalendarExtender>&nbsp;
                                                				<asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"  />
                            						</td>
									<td width="10%" align="left">Search By CPN :	
									</td>
									<td width="20%" align="left"><asp:TextBox ID="txtSearchCPN" CssClass="field textbox" width="50%" runat="server"></asp:TextBox>
									</td>
									<td width="10%" align="left"><asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" /></td>
									<td Width="42%"><div id="export" runat="server" style="float: right; display: none">
                                                                                <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" 
                                                                                OnClick="imgexport_Click" />
                                                                            </div>
									</td>
								</tr>
							</table>
							</div>
							<asp:Panel ID="pnlHead" runat="server">
                                                        <div class="formmenu">
                                                            <div class="loginform">
                                                                <div id="Div1" class="quicktabs_main quicktabs-style-excel" style="height: auto;">
                                                                    <div style="padding: 15px; height: auto;">
                                                                        <asp:Label ID="lblMsg" runat="server" ForeColor="DarkRed" Font-Bold="true" Font-Size="Medium" Text=""></asp:Label><br />
                                                                        <asp:GridView ID="GVD_PatientList" runat="server" AllowPaging="True" AllowSorting="true"
                                                                            AutoGenerateColumns="False" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20"
                                                                            ShowHeader="true" Width="100%" OnRowCommand="GVD_PatientList_RowCommand" OnRowDataBound="GVD_PatientList_RowDataBound"
                                                                            OnPageIndexChanging="GVD_PatientList_PageIndexChanging" EmptyDataText="No records found...">
                                                                            <Columns>
										<asp:TemplateField ItemStyle-Width="19%" ItemStyle-HorizontalAlign="Left" HeaderText="Date">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtDOA" runat="server" Width="60%" CssClass="field" Text='<%# Eval("Date")%>'></asp:TextBox>
												<cc1:CalendarExtender ID="txtDOA_CalenderExtender" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgDOA" TargetControlID="txtDOA"></cc1:CalendarExtender>
												<asp:Image ID="imgDOA" runat="server" ImageUrl="~/images/calimg.gif" Style="position:inherit;width: 18px; height: 18px; top: 5px; left: -5px;" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="4%" ItemStyle-HorizontalAlign="Center" HeaderText="S.No.">
                                                                                    <ItemTemplate>
											<%-- <asp:TextBox ID="txtSno" runat="server" Text='<%# Container.DataItemIndex + 1 %>' CssClass="field required" Style="width:50%;"></asp:TextBox> PlaceHolder="<%# Container.DataItemIndex+1 %>"--%>
                                                                                        <%# Container.DataItemIndex + 1 %>
											
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Center" HeaderText="App Time">
                                                                                    <ItemTemplate>
											<%--<asp:Label ID="lblHour" runat="server" Visible="false" Text='<%# Eval("THour")%>'></asp:Label>--%>
											<asp:HiddenField ID="hdnHour" runat="server" Value='<%# Eval("THour") %>' />
											<asp:DropDownList ID="ddlHour" runat="server">
												<asp:ListItem Text="1" value="1"></asp:ListItem>
												<asp:ListItem Text="2" value="2"></asp:ListItem>
												<asp:ListItem Text="3" value="3"></asp:ListItem>
												<asp:ListItem Text="4" value="4"></asp:ListItem>
												<asp:ListItem Text="5" value="5"></asp:ListItem>
												<asp:ListItem Text="6" value="6"></asp:ListItem>			
												<asp:ListItem Text="7" value="7"></asp:ListItem>
												<asp:ListItem Text="8" value="8"></asp:ListItem>
												<asp:ListItem Text="9" value="9"></asp:ListItem>
												<asp:ListItem Text="10" value="10"></asp:ListItem>
												<asp:ListItem Text="11" value="11"></asp:ListItem>
												<asp:ListItem Text="12" value="12"></asp:ListItem>
											</asp:DropDownList>
											<%--<asp:Label ID="lblMinute" runat="server" Text='<%# Eval("TMinute")%>'></asp:Label>--%>
											<asp:HiddenField ID="hdnMinute" runat="server" Value='<%#Eval("TMinute") %>' />  
											<asp:DropDownList ID="ddlMinute" runat="server">
												<asp:ListItem Text="00" value="0"></asp:ListItem>
												<asp:ListItem Text="30" value="1"></asp:ListItem>
											</asp:DropDownList>
											<%--<asp:Label ID="lblAMPM" runat="server" Visible="false" Text='<%# Eval("TAMPM")%>'></asp:Label>--%>
											<%-- <asp:HiddenField ID="hdnAMPM" runat="server" Value='<%#Eval("TAMPM") %>' />
											<asp:DropDownList ID="ddlAMPM" runat="server">
												<asp:ListItem Text="AM" Value="AM"></asp:ListItem>
												<asp:ListItem Text="PM" Value="PM"></asp:ListItem>
											</asp:DropDownList>--%>
											<%--<asp:TextBox ID="txtTOA" runat="server" Value='<%# Eval("App_Time")%>' Style="width:70%;" PlaceHolder="HH:MM PM/AM"></asp:TextBox>--%>
											<%--<MKB:TimeSelector runat="server" ID="txtTOA" Text='<%# Eval("App_Time")%>' ToolTip="HH:MM AM/PM">
                                                                			</MKB:TimeSelector>--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="center" ItemStyle-Width="18%" HeaderText="Patient Name">
                                           					    <ItemTemplate>
                                                					<asp:LinkButton ID="lnkNewCasepdName" runat="server" Text='<%# Eval("pdName")%>' CommandArgument='<%# Eval("pdId") + ";" +Eval("fdId") %>'
                                                    						CommandName="CaseFol" ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("FStatus").ToString())%>' Style="text-decoration: none; font-weight: normal;"></asp:LinkButton>
                                            					    </ItemTemplate>
                                         					</asp:TemplateField>
                                                                                <asp:BoundField DataField="pdAge" ItemStyle-Width="5%" HeaderText="Age" />
                                          					<asp:BoundField DataField="pdSex" HeaderText="Gen" ItemStyle-Width="3%" />   
					                                        <asp:BoundField DataField="pdCasePaperNo" HeaderText="C.P.No." />                           
                                                                                <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Left" HeaderText="Patient Type">
                                                                                    <ItemTemplate>
                                                                                        <asp:DropDownList ID="ddlpttype" runat="server">
                                                                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                                                                            <asp:ListItem Text="Regular" Value="Reg"></asp:ListItem>
                                                                                            <asp:ListItem Text="Important" Value="Imp"></asp:ListItem>
                                                                                            <asp:ListItem Text="Very Important" Value="VImp"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:HiddenField ID="hdnpttype" runat="server" Value='<%#Eval("patType") %>' />
                                                                                        <asp:Label ID="lblResult" runat="server" Visible="false"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                        					<%-- <asp:BoundField DataField="ConsDate" ItemStyle-Width="10%" HeaderText="Con Date" />--%>
					                                        <asp:BoundField DataField="DocName" ItemStyle-Width="10%" HeaderText="Doctor Name" />
                                        					<%--<asp:BoundField DataField="pdOccupation" ItemStyle-Width="8%" HeaderText="Occup" /> --%>
					                                        <asp:BoundField DataField="City" HeaderText="City" ItemStyle-Width="5%" />

                                                                                <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Left" HeaderText="Mobile No">
                                                                                    <ItemTemplate>
											<asp:TextBox ID="txtMob" runat="server" Style="Width:80px;" Value='<%# Eval("ContactNo")%>' CssClass="field required" ></asp:TextBox>
                                                                                        <asp:Label ID="txtpdId" runat="server" Visible="false" Text='<%# Eval("pdId")%>'></asp:Label>
											<%-- <%# Eval("pdMob")%> --%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%" HeaderText="Action">
                                                                                    <ItemTemplate>
											<asp:ImageButton ID="btnDone" runat="server" CommandArgument='<%# Eval("Tid") %>'
                                                                                            CommandName="done" ImageUrl="Images/button_ok.png" ToolTip="Done"/>
                                                                                        <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("Tid") %>'
                                                                                            CommandName="editRow" ImageUrl="Images/edit.ico" ToolTip="Edit" Visible="false" />
                                                                                        <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("Tid") %>'
                                                                                            CommandName="del" ImageUrl="Images/delete.ico" OnClientClick="return confirm('Are you sure you want to delete this?');"
                                                                                            ToolTip="Delete" />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                                <div class="clr">
                                                                </div>
                                                            </div>
                                                        </div>
							</asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                        </div>
                       <%-- <div class="clr">
                        </div>
                    </div>
                </div>--%>
            </div>
        </td>
    </tr>
    
</table>
</asp:Content>

