﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Collections.Generic;
using System.Drawing;
using AjaxControlToolkit;

public partial class WaitingPatientStatus : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlCommand SqlCmd1;
    string StrSQL;
    string StrSQL1;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn1 = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    genral gen1 = new genral();
    string uid = "0";
    DataTable dt = new DataTable();
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (!IsPostBack)
        {
            
            BindWaitingListGrid();
        }

    }


    private void BindWaitingListGrid()
    {

        StrSQL = "Proc_GetWaitingPatientList";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            //DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                export.Style.Add("display", "block");
                GRVWaitingList.DataSource = dt;
                GRVWaitingList.DataBind();
            }
            conn.Close();
            dr.Dispose();

        }

    }

    protected void GRVWaitingList_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        try
        {
            lblMsg.Text = "";

            if (e.CommandName == "IPD")
            {
                //int wID = Convert.ToInt32(e.CommandArgument);
                //if (wID != null)
                //{
                GridViewRow gvRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                DropDownList ddWard = (DropDownList)gvRow.FindControl("ddWard");
                HiddenField hdnWard = (HiddenField)gvRow.FindControl("hdnWard");
                HiddenField hdnwID = (HiddenField)gvRow.FindControl("hdnwID");
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                Session["CasePatientID"] = pdID;
                Session["WaitingPatientID"] = hdnwID.Value;


                Session["Ward"] = hdnWard.Value;

                int pdId = Convert.ToInt32(e.CommandArgument);
                int cnt = Convert.ToInt32(gen.executeScalar("SELECT COALESCE( (Select ipdId from tblIpdMaster Where ipdDischargeDate is null And IsDelete = 0 And ipdpdId = '" + pdID + "'),'')").ToString());

                if (cnt > 0)
                {

                    Response.Redirect("IPDViewAks.aspx?DISPROW=ALL", false);
                }
                else
                {
                    Response.Redirect("IPD.aspx", false);
                }
                //}
            }


            if (e.CommandName == "done")
            {
                int wID = Convert.ToInt32(e.CommandArgument);
                if (wID != null)
                {
                    GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

                    DropDownList ddWard = (DropDownList)gvRow.FindControl("ddWard");
                    ImageButton btnDone = (ImageButton)gvRow.FindControl("btnDone");
                    ImageButton btnEdit = (ImageButton)gvRow.FindControl("btnEdit");
                    HiddenField hdnWard = (HiddenField)gvRow.FindControl("hdnWard");
                    Label lblResult = (Label)gvRow.FindControl("lblResult");
                    TextBox txtMob = (TextBox)gvRow.FindControl("txtMob");
                    Label txtpdid = (Label)gvRow.FindControl("txtpdid");

                    TextBox txtDOW = (TextBox)gvRow.FindControl("txtDOW");
                    CalendarExtender txtDOW_CalenderExtender = (CalendarExtender)gvRow.FindControl("txtDOW_CalenderExtender");

                    Session["Ward"] = ddWard.SelectedValue;
                    hdnWard.Value = ddWard.SelectedValue;

                    if (ddWard.SelectedValue == "")
                    {
                        lblResult.Visible = true;
                        lblResult.Text = "Compulsory";
                        lblResult.ForeColor = Color.DarkRed;
                        return;
                    }
                    else
                    {
                        lblResult.Visible = true;
                        lblResult.Text = "";
                    }

                    txtDOW_CalenderExtender.Enabled = false;
                    btnDone.Visible = ddWard.Enabled = txtDOW.Enabled = txtMob.Enabled = false;
                    btnEdit.Visible = true;

                    
                    string[] _dow = txtDOW.Text.ToString().Split('/');
                    string dow = _dow[2] + "/" + _dow[1] + "/" + _dow[0];

                    SqlCmd = new SqlCommand();
                    SqlCmd.CommandText = "Proc_UpdatePaitientWaitingList";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.AddWithValue("@wID", wID);
                    SqlCmd.Parameters.AddWithValue("@wWardTypeId", Session["Ward"]);
                    SqlCmd.Parameters.AddWithValue("@wCreatedBy", uid);
                    SqlCmd.Parameters.AddWithValue("@wBookingDate", dow);
                    SqlCmd.Parameters.AddWithValue("@pdID", txtpdid.Text.ToString());
                    SqlCmd.Parameters.AddWithValue("@Mob",txtMob.Text.ToString());
                    SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
                    SqlCmd.Connection = conn;
                    if (conn.State == ConnectionState.Closed)
                        conn.Open();
                    
                    SqlCmd.ExecuteNonQuery();
                    conn.Close();
                    lblMsg.Text = "Record Updated Successfully...!";
                    Response.Redirect("WaitingPatientStatus.aspx", true);

                }

            }
            if (e.CommandName == "del")
            {
                int wID = Convert.ToInt32(e.CommandArgument);
                if (wID != null)
                {
                    SqlCmd = new SqlCommand();

                    SqlCmd.CommandText = "Proc_DeletePaitientWaitingList";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.AddWithValue("@wID", wID);
                    SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
                    SqlCmd.Connection = conn;
                    if (conn.State == ConnectionState.Closed)
                        conn.Open();

                    SqlCmd.ExecuteNonQuery();
                    conn.Close();
                    Response.Redirect("WaitingPatientStatus.aspx", true);
                }

            }

            if (e.CommandName == "editRow")
            {
                int wID = Convert.ToInt32(e.CommandArgument);
                if (wID != null)
                {
                    GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    DropDownList ddWard = (DropDownList)gvRow.FindControl("ddWard");
                    ImageButton btnDone = (ImageButton)gvRow.FindControl("btnDone");
                    ImageButton btnEdit = (ImageButton)gvRow.FindControl("btnEdit");
                    TextBox txtMob = (TextBox)gvRow.FindControl("txtMob");
                    TextBox txtDOW = (TextBox)gvRow.FindControl("txtDOW");
                    CalendarExtender txtDOW_CalenderExtender = (CalendarExtender)gvRow.FindControl("txtDOW_CalenderExtender");

                    txtDOW_CalenderExtender.Enabled = true;
                    btnDone.Visible = ddWard.Enabled = txtDOW.Enabled = txtMob.Enabled = true;
                    btnEdit.Visible = false;
                }
                
            }
        }
        catch (Exception ex)
        {


        }

    }

    protected void GRVWaitingList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
        {
            DropDownList ddWard = (DropDownList)e.Row.FindControl("ddWard");
            HiddenField hdnWard = (HiddenField)e.Row.FindControl("hdnWard");
            ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
            ImageButton btnDone = (ImageButton)e.Row.FindControl("btnDone");
            TextBox txtMob = (TextBox)e.Row.FindControl("txtMob");
            TextBox txtDOW = (TextBox)e.Row.FindControl("txtDOW");
            CalendarExtender txtDOW_CalenderExtender = (CalendarExtender)e.Row.FindControl("txtDOW_CalenderExtender");

            ddWard.DataSource = dt;
            ddWard.Items.Clear();

            ddWard.Items.Insert(0, new ListItem("Select", ""));

            gen.FillDropDownList("SELECT Distinct WardTypeID As wmWardTypeId , WardTypeName As wmType FROM tblWardTypeMaster Where IsDelete = 0", "wmWardTypeId", "wmType", ddWard);

            ddWard.SelectedValue = hdnWard.Value;
            if (ddWard.Text != "")
            {
                txtDOW_CalenderExtender.Enabled = false;
                ddWard.Enabled = txtDOW.Enabled = txtMob.Enabled = btnDone.Visible = false;
                btnEdit.Visible = true;
            }
            else
            {
                txtDOW_CalenderExtender.Enabled = true;
                ddWard.Enabled = txtDOW.Enabled = txtMob.Enabled = btnDone.Visible = true;
                btnEdit.Visible = false;
            }

        }


    }
    protected void GRVWaitingList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRVWaitingList.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindWaitingListGrid();
    }
    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "WaitingPatientList.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRVWaitingList.AllowPaging = false;
        this.BindWaitingListGrid();
        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Convert.ToString(Session["update"]);
    }

    [WebMethod]
    public static List<string> GetPatientDetails(string PName)
    {
        List<string> PNameResult = new List<string>();
        using (SqlConnection con = new SqlConnection((ConfigurationManager.AppSettings["constring"])))
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "Select pd.pdID,pdName,pdCasePaperNo,pdName+' - '+pdCasePaperNo Name ,wmWardNo,bmNoOfBads from dbo.tblPatientDetails pd LEFT JOIN vwGetPatient p on pd.pdID=p.pdid Inner Join tblIpdMaster IM On IM.ipdpdId = pd.pdID Inner Join tblPatientsWardDetails pwd On pwd.wipdId = IM.ipdId Left Join tblWardMaster WM On pwd.wNo = WM.wmID Left Join tblBadMaster BM On WM.wmID = BM.wmID And pwd.wBedNo = BM.bmID where IsCancel=0 And ipdDischargeDate is Null And ipdIsDischarge = 0 And wOutDate is Null And IM.IsDelete = 0 And IsNull(pwd.WIsDelete,0) = 0 and pdName LIKE REPLACE('%" + PName + "%', ' ' , '%') or pdCasePaperNo LIKE REPLACE('%" + PName + "%', ' ' , '%')";
                cmd.Connection = con;
                con.Open();
                //cmd.Parameters.AddWithValue("@SearchPatient", PName);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    //PNameResult.Add(dr["PName"].ToString());
                    //while (dr.Read())
                    //{
                    PNameResult.Add(string.Format("{0}${1}${2}${3}${4}${5}", dr["pdId"].ToString(), dr["pdName"].ToString(), dr["pdCasePaperNo"].ToString(), dr["Name"].ToString(), dr["wmWardNo"].ToString(), dr["bmNoOfBads"].ToString()));
                    //}
                }
                con.Close();
                return PNameResult;
            }
        }
    }
}
