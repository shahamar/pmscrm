﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="AddNewPatient.aspx.cs" Inherits="AddNewPatient" Title="New Patient Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Date.ascx" TagName="Date" TagPrefix="uc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>

    <%--  <script type="text/javascript">
    function Validate() {
        //alert("ok");
        var fname = document.getElementById("<%= txtfname.ClientID %>").value;
        var lname = document.getElementById("<%= txtlname.ClientID %>").value;
        var age = document.getElementById("<%= txtage.ClientID %>").value;
        var add1 = document.getElementById("<%= txtAddress1.ClientID %>").value;
        var add2 = document.getElementById("<%= txtAddress2.ClientID %>").value;
        var add2 = document.getElementById("<%= txtAddress2.ClientID %>").value;
        //alert("ok");

        var Err = "";

        if (fname == "") {
            //alert("fname_if");
            Err += "Please Fill the First Name";
            document.getElementById("<%= txtfname.ClientID %>").className = "field input-validation-error";
        }
        else {
            //alert("fname_else");
            document.getElementById("<%= txtfname.ClientID %>").className = "field";
        }

        if (lname == "") {
            //alert("lname_if");
            Err += "Please Fill the Last Name";
            document.getElementById("<%= txtlname.ClientID %>").className = "field input-validation-error";
        }
        else {
            //alert("lname_else");
            document.getElementById("<%= txtlname.ClientID %>").className = "field";
        }

        if (Err != "") {
            return false;
        }
    }
</script>--%>

    <script language="javascript" type="text/javascript">
        function CalculateAge(birthday) {
            var d = new Date(birthday);
            var myDate = birthday.value;
            var chunks = myDate.split('/');
            var formattedDate = chunks[1] + '/' + chunks[0] + '/' + chunks[2];
            if (birthday.value != '') {
                birthdayDate = new Date(formattedDate);
                dateNow = new Date();
                var Age = dateNow.getFullYear() - birthdayDate.getFullYear();
                var AgeInMonth =  birthdayDate.getMonth() - dateNow.getMonth();
                $('#<%= txtage.ClientID %>').val(Age);
                $('#<%= txt_agein_month.ClientID %>').val(AgeInMonth);

        }
    }


 

    //    function BindStateNCity() {
    //        var Country = document.getElementById("<%= DDL_Country.ClientID %>").value;
        //        document.getElementById("<%= DDL_State.ClientID %>").value = "40";
        //        $("#<%=DDL_State.ClientID %>").select();
        //    } 

    </script>

    <script type="text/javascript">
        function openinnewTab(pid) {
            //            alert(pid)
            var win = window.open("ConsultingBill.aspx?id='" + pid + "'", '_blank');
            var win1 = window.open("AcuteBill.aspx?id='" + pid + "'", '_blank');
            // window.open();
            win.focus();
            win1.focus();
        }
    </script>

    <script type="text/javascript">

        function ConfirmPrint(FormDep) {

            var chkid = document.getElementById("<%= chk_PrintConsBill.ClientID %>");
        var chkRF = document.getElementById("<%= chk_RecForm.ClientID %>");
        var chkAB = document.getElementById("<%= chk_AcuteBill.ClientID %>");
        if (chkid.checked == true || chkRF.checked == true || chkAB.checked == true) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Print?")) {

                confirm_value.value = "Yes";

                if (FormDep == "COMM_Print")
                { OPDPrintFunction(); }

                else {
                    confirm_value.value = "No";
                }
            }
            document.forms[0].appendChild(confirm_value);

        } else {

        }

    }


    function OPDPrintFunction() {


        setTimeout('DelayOPDPrintFunction()', 200);

    }

    function DelayOPDPrintFunction() {
        var myWindow = window.open("PrintReport.aspx");
        myWindow.focus();
        myWindow.print();
    }

    </script>
 <script type="text/javascript">

        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <%-- <asp:UpdatePanel ID="UP1" runat="server"><ContentTemplate>--%>
    <center>
        <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <div class="login-area margin_ten_right" style="width: 100%;">
                        <span style="text-align: center;">
                            <h2>Patient Registration</h2>
                        </span>
                        <div class="formmenu">
                            <div class="loginform">
                                <ul class="quicktabs_tabs quicktabs-style-excel">
                                    <li class="qtab-Demo active first" id="li_1">
                                        <a class="qt_tab active" href="javascript:void(0)">Add New Patients</a>
                                    </li>
                                    <li class="qtab-HTML last" id="li_9">
                                        <a class="qt_tab active" href="ManagePatients.aspx">Manage Patients</a>
                                    </li>
                                </ul>
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                    <table class="bolder_label" style="width: 100%; background: #f2f2f2; font-weight: bold; padding-left: 20px" cellspacing="0px" cellpadding="5px">
                                        <tr>
                                            <td colspan="5" style="text-align: center; color: Red;">
                                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Initial* :</td>
                                            <td>First Name* :</td>
                                            <td>Middle Name :</td>
                                            <td>Last Name* :</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddintial" runat="server" CssClass="field">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="MR" Value="MR"></asp:ListItem>
                                                    <asp:ListItem Text="MRS" Value="MRS"></asp:ListItem>
                                                    <asp:ListItem Text="MISS" Value="MISS"></asp:ListItem>
                                                    <asp:ListItem Text="MAST" Value="MAST"></asp:ListItem>
                                                    <asp:ListItem Text="BABY" Value="BABY"></asp:ListItem>
                                                    <asp:ListItem Text="DR" Value="DR"></asp:ListItem>
                                                    <asp:ListItem Text="SADHVI" Value="SADHVI"></asp:ListItem>
                                                    <asp:ListItem Text="SHREE" Value="SHREE"></asp:ListItem>
                                                    <asp:ListItem Text="ANIMAL" Value="ANIMAL"></asp:ListItem>
                                                    <asp:ListItem Text="BIRD" Value="BIRD"></asp:ListItem>
                                                    <asp:ListItem Text="SMT" Value="SMT"></asp:ListItem>
                                                </asp:DropDownList><br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddintial"
                                                    Display="Dynamic" ErrorMessage="Please specify Initial." ValidationGroup="val">
                                                    <span class="error" >Please specify Initial.</span>
                                                </asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtfname" runat="server" CssClass="field required" MaxLength="50"
                                                    Style="float: left; text-transform: uppercase;"></asp:TextBox>
                                                <br />
                                                <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="txtfname"
                                                    Display="Dynamic" ErrorMessage="First Name is required." ValidationGroup="val">
                                                    <span class="error">Please specify First Name.</span>
                                                </asp:RequiredFieldValidator>
                                                <%--<asp:RegularExpressionValidator ID="reg_age" runat="server" CssClass="field required"
                                                    ControlToValidate="txtlname" Display="Dynamic" ErrorMessage="Please enter valid Last Name"
                                                    ValidationExpression="^[a-zA-Z .\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid Last Name!</span>
                                                </asp:RegularExpressionValidator>--%>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtmname" runat="server" CssClass="field" MaxLength="50" Style="text-transform: uppercase;"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtlname" runat="server" CssClass="field" MaxLength="50" Style="float: left; text-transform: uppercase;">
                                                </asp:TextBox><br />
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtlname"
                                                    Display="Dynamic" ErrorMessage="Last Name is required." ValidationGroup="val">
                                                    <span class="error">Please specify Last Name.</span>
                                                </asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="reg_age" runat="server" CssClass="field required"
                                                    ControlToValidate="txtlname" Display="Dynamic" ErrorMessage="Please enter valid Last Name"
                                                    ValidationExpression="^[a-zA-Z0-9 .\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid Last Name!</span>
                                                </asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gender* :</td>
                                            <td>

                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 50%">DOB :
                                                        </td>

                                                        <td style="width: 60%">Age* :
                                                        </td>

                                                    </tr>
                                                </table>


                                            </td>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <%--<td style="width: 47%">Blood Group : 
                                                        </td>--%>

                                                        <td>Weight :
                                                        </td>
                                                         <td>Height :
                                                        </td>

                                                    </tr>
                                                </table>

                                            </td>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="center">Kid
                                                        </td>
                                                        <td align="center">Teen
                                                        </td>
                                                        <td align="center">Adult
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddSex" runat="server" CssClass="required field">
                                                    <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                                    <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                                </asp:DropDownList><br />

                                                <asp:RequiredFieldValidator ID="reqSex" runat="server" ControlToValidate="ddSex"
                                                    Display="Dynamic" ErrorMessage="Please specify Gender." ValidationGroup="val">
                                                    <span class="error" >Please specify Gender.</span>
                                                </asp:RequiredFieldValidator>
                                            </td>
                                            <td>


                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 50%">
                                                             <%--<asp:TextBox ID="txtDOB" runat="server" CssClass="field" placeholder="DD/MM/YYYY" Width="100px" onchange="CalculateAge(this);">
                                                            </asp:TextBox>--%>
                                                            <asp:TextBox ID="txtDOB" runat="server" CssClass="field" placeholder="DD/MM/YYYY" Width="100px" AutoPostBack="true" OnTextChanged="txtDOB_TextChanged">
                                                            </asp:TextBox>
                                                            <cc1:CalendarExtender ID="ce1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                PopupButtonID="imgDOB" TargetControlID="txtDOB">
                                                            </cc1:CalendarExtender>
                                                            &nbsp;<asp:Image ID="imgDOB" runat="server" ImageUrl="~/images/calimg.gif"
                                                                Style="position: inherit; width: 18px; height: 18px; top: 5px; left: -5px;" />
                                                            <%--   <cc1:MaskedEditExtender ID="meeMonthYear" runat="server" TargetControlID="txtDOB" Mask="99/99/9999" 
                                                    MaskType="Date" InputDirection="RightToLeft">
                                                </cc1:MaskedEditExtender>--%>
                                                        </td>

                                                        <td style="width: 50%">
                                                            <asp:TextBox ID="txtage" runat="server" CssClass="field" Width="20px"></asp:TextBox>
                                                            Year
                                                   <asp:TextBox ID="txt_agein_month" runat="server" CssClass="field" Width="20px"></asp:TextBox>
                                                            Month
                                             <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtage"
                                                    Display="Dynamic" ErrorMessage="Please specify Age." ValidationGroup="val">
                                                    <span class="error" >Please specify Age.</span>
                                                </asp:RequiredFieldValidator> --%>

                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="field required"
                                                                ControlToValidate="txtage" Display="Dynamic" ErrorMessage="Please enter valid Age"
                                                                ValidationExpression="^[0-9]*$" ValidationGroup="val">
                                                    <span class="error">Please enter valid Age!</span>
                                                            </asp:RegularExpressionValidator>

                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" CssClass="field required"
                                                                ControlToValidate="txt_agein_month" Display="Dynamic" ErrorMessage="Please enter valid Age"
                                                                ValidationExpression="^[0-9]*$" ValidationGroup="val">
                                                    <span class="error">Please enter valid Age!</span>
                                                            </asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <%--<td style="width: 44%">
                                                            <asp:DropDownList ID="DDL_BloodGroup" runat="server" CssClass="required field" Width="130px">
                                                                <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                                                <asp:ListItem Text="A+" Value="A+"></asp:ListItem>
                                                                <asp:ListItem Text="B+" Value="B+"></asp:ListItem>
                                                                <asp:ListItem Text="AB+" Value="AB+"></asp:ListItem>
                                                                <asp:ListItem Text="O+" Value="O+"></asp:ListItem>
                                                                <asp:ListItem Text="A-" Value="A-"></asp:ListItem>
                                                                <asp:ListItem Text="B-" Value="B-"></asp:ListItem>
                                                                <asp:ListItem Text="AB-" Value="AB-"></asp:ListItem>
                                                                <asp:ListItem Text="O-" Value="O-"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>--%>

                                                        <td style="width: 50%">
                                                            <asp:TextBox ID="txtWeight" runat="server" CssClass="field" Width="80px" onkeypress="return isNumberKey(event,this)" MaxLength="6"></asp:TextBox>
                                                            kg.
                                                    <br />
                                                           
                                                        </td>
                                                         <td style="width: 50%">
                                                            <asp:TextBox ID="txtHeight" runat="server" CssClass="field" Width="80px" onkeypress="return isNumberKey(event,this)" MaxLength="6"></asp:TextBox>
                                                            cm.
                                                    <br />
                                                            
                                                        </td>

                                                    </tr>
                                                </table>

                                            </td>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chk_kids" runat="server" Text="(Bellow 10)" Font-Size="Small" Font-Bold="false" AutoPostBack="true" OnCheckedChanged="chk_kids_CheckedChanged" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chk_teen" runat="server" Text="Age(10-20)" Font-Size="Small" Font-Bold="false" AutoPostBack="true" OnCheckedChanged="chk_teen_CheckedChanged" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chk_adult" runat="server" Text="(Above 20)" Font-Size="Small" Font-Bold="false" AutoPostBack="true" OnCheckedChanged="chk_adult_CheckedChanged" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td>Case Paper No* :</td>
                                            <%--Re-Consulting No.--%>
                                            <td style="width: 47%">Blood Group : 
                                                        </td>
                                            <td>Occupation</td>
                                            <td>Patient Type</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtCasePaperNo" runat="server" CssClass="field" MaxLength="20" Width="240" Style="text-transform: uppercase;" ReadOnly="true">
                                                </asp:TextBox><br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCasePaperNo"
                                                    ValidationGroup="val" Display="Dynamic" ErrorMessage="Case Paper No!" CssClass="field required">
                                                    <span class="error">Please specify Consulting Case Paper No</span>
                                                </asp:RequiredFieldValidator>

                                                <%-- <asp:RegularExpressionValidator ID="regCasepaperno" runat="server" Display="Dynamic"
                                                    CssClass="field required" ControlToValidate="txtCasePaperNo" ErrorMessage="Please enter valid case paper no!"
                                                    ValidationExpression="^[a-zA-Z0-9 / .\s]{0,50}$" ValidationGroup="val">
                                                <span class="error">Please enter valid case paper no!</span></asp:RegularExpressionValidator>--%>
                                           
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt_ReconsultingNo" runat="server" CssClass="field" MaxLength="20" Style="text-transform: uppercase;" Visible="false">
                                                </asp:TextBox>

                                                
                                                            <asp:DropDownList ID="DDL_BloodGroup" runat="server" CssClass="required field">
                                                                <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                                                <asp:ListItem Text="A+" Value="A+"></asp:ListItem>
                                                                <asp:ListItem Text="B+" Value="B+"></asp:ListItem>
                                                                <asp:ListItem Text="AB+" Value="AB+"></asp:ListItem>
                                                                <asp:ListItem Text="O+" Value="O+"></asp:ListItem>
                                                                <asp:ListItem Text="A-" Value="A-"></asp:ListItem>
                                                                <asp:ListItem Text="B-" Value="B-"></asp:ListItem>
                                                                <asp:ListItem Text="AB-" Value="AB-"></asp:ListItem>
                                                                <asp:ListItem Text="O-" Value="O-"></asp:ListItem>
                                                            </asp:DropDownList>
                                            </td>
                                            <td>

                                                <asp:TextBox ID="txtCassetteNo" runat="server" CssClass="field" MaxLength="20" Visible="false"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regCassetteNo" runat="server" Display="Dynamic"
                                                    CssClass="field required" ControlToValidate="txtCassetteNo" ErrorMessage="Please enter valid cassette no!"
                                                    ValidationExpression="^[a-zA-Z0-9 -.\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid Cassette no!</span>
                                                </asp:RegularExpressionValidator>
                                                <asp:TextBox ID="txtOccupation" runat="server" CssClass="field"></asp:TextBox>
                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtOccupation"
                                                    ValidationGroup="val" Display="Dynamic" ErrorMessage="Occupation !" CssClass="field required">
                                                    <span class="error">Please specify Occupation</span>
                                                </asp:RequiredFieldValidator>--%>

                                            </td>
                                            <td>
                                                <asp:DropDownList ID="DDL_PatType" runat="server" Width="260px" CssClass="required field">
                                                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="EMG" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="VIP" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Country :</td>
                                            <td>State :</td>
                                            <td>City* :</td>
                                            <td>Pin :</td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <asp:DropDownList ID="DDL_Country" runat="server" CssClass="required field" AutoPostBack="true" OnSelectedIndexChanged="DDL_Country_SelectedIndexChanged">
                                                    <asp:ListItem Text="INDIA" Value="INDIA"></asp:ListItem>
                                                    <asp:ListItem Text="OTHER" Value="OTHER"></asp:ListItem>
                                                </asp:DropDownList>

                                            </td>
                                            <td>
                                                <asp:DropDownList ID="DDL_State" CssClass="field" runat="server" AutoPostBack="true"
                                                    OnSelectedIndexChanged="DDL_State_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="DDL_City" CssClass="field" runat="server"></asp:DropDownList><br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="DDL_City"
                                                    Display="Dynamic" ErrorMessage="City is required." ValidationGroup="val">
                                                    <span class="error">Please specify City.</span>
                                                </asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt_Pin" runat="server" CssClass="field" MaxLength="6"></asp:TextBox><br />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txt_Pin"
                                                    Display="Dynamic" ErrorMessage="Pin is required." ValidationGroup="val">
                                                    <span class="error">Please specify Pin.</span>
                                                </asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">Address* :</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:TextBox ID="txtAddress1" runat="server" CssClass="required field" Width="1155px" Style="float: left;"></asp:TextBox>
                                                <br />
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtAddress1"
                                                    Display="Dynamic" ErrorMessage="Address is required." ValidationGroup="val">
                                                    <span class="error">Please specify Address.</span>
                                                </asp:RequiredFieldValidator>

                                                <asp:RegularExpressionValidator ID="regAddress1" runat="server" Display="Dynamic"
                                                    CssClass="field required" ControlToValidate="txtAddress1" ErrorMessage="Please enter valid address!"
                                                    ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,150}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid address!</span>
                                                </asp:RegularExpressionValidator>

                                                <asp:TextBox ID="txtAddress2" runat="server" CssClass="required field" MaxLength="50" Text="Test"
                                                    Style="float: left;" Width="300px" Visible="false"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regAddress2" runat="server" Display="Dynamic" Visible="false"
                                                    CssClass="field required" ControlToValidate="txtAddress2" ErrorMessage="Please enter valid address!"
                                                    ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid address!</span>
                                                </asp:RegularExpressionValidator>

                                                <asp:TextBox ID="txtAddress3" runat="server" CssClass="field" MaxLength="50" TextMode="MultiLine" Text="Test"
                                                    Visible="false" Width="300px"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regAddress3" runat="server" Display="Dynamic"
                                                    CssClass="field required" ControlToValidate="txtAddress3" ErrorMessage="Please enter valid address!"
                                                    ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid address!</span>
                                                </asp:RegularExpressionValidator>

                                                <asp:TextBox ID="txtAddress4" runat="server" CssClass="field" TextMode="MultiLine" Text="Test" Visible="false"
                                                    MaxLength="50" Width="300px"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regAddress4" runat="server" Display="Dynamic"
                                                    CssClass="field required" ControlToValidate="txtAddress4" ErrorMessage="Please enter valid address!"
                                                    ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid address!</span>
                                                </asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Contact No* :   <img src="images/mobile_phone.png" /></td>
                                            <td></td>
                                            <td>E-mail :</td>
                                            <td>Doctor's Name :</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtmob" runat="server" CssClass="field" MaxLength="10" OnKeyPress="txtmob_KeyPress"></asp:TextBox>
                                              <%-- <img src="images/mobile_phone.png" /><br />--%>
                                                <br />
                                                <asp:RegularExpressionValidator ID="regtele" runat="server" Display="Dynamic" CssClass="field required"
                                                    ControlToValidate="txttele" ErrorMessage="Please enter valid telephone number!"
                                                    ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val">
                                                   <span class="error">Please enter valid telephone number!</span>
                                                </asp:RegularExpressionValidator>

                                                <asp:RegularExpressionValidator ID="regmob" runat="server" Display="Dynamic" CssClass="field required"
                                                    Style="padding-left: 140px;" ControlToValidate="txtmob" ErrorMessage="Please enter valid mobile number!"
                                                    ValidationExpression="[0-9]{10}$" ValidationGroup="val">
                                                   <span class="error">Please enter valid mobile number!</span>
                                                </asp:RegularExpressionValidator>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtmob"
                                                    Display="Dynamic" ErrorMessage="Please specify Contact No." ValidationGroup="val">
                                                   <span class="error" >Please specify Contact No.</span>
                                                </asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txttele" runat="server" CssClass="field" MaxLength="10"></asp:TextBox>&nbsp;
                                                   <img src="images/phone.png" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtemail" runat="server" CssClass="field required" MaxLength="50" Style="float: left"></asp:TextBox>
                                                <br />
                                                <asp:RegularExpressionValidator ID="regEmailID1" runat="server" ControlToValidate="txtemail"
                                                    CssClass="field required" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
                                                    ValidationGroup="val" Display="Dynamic">
                                                    <span class="error">Please enter valid email Id</span>
                                                </asp:RegularExpressionValidator>
                                                <asp:RegularExpressionValidator ID="reg_EmailId1" runat="server" ControlToValidate="txtemail"
                                                    Display="Dynamic" CssClass="field required" ValidationExpression="^[\s\S]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Max 50 characters allowed</span>
                                                </asp:RegularExpressionValidator>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="DDDoctor" CssClass="field" runat="server"></asp:DropDownList><br />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="DDDoctor"
                                                    Display="Dynamic" ErrorMessage="Please specify Doctor Name." ValidationGroup="val">
                                                    <span class="error">Please specify Doctor Name.</span>
                                                </asp:RequiredFieldValidator>--%>       
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Payment Mode :</td>
                                            <td>Registration Fees :</td>
                                            <td>Consulting Amount :</td>
                                            <td>Consulting Date* :</td>
                                            <td></td>
                                        </tr>
                                        <tr>

                                            <td>
                                                <asp:DropDownList ID="DDL_Pay_Mode" runat="server" CssClass="field">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                    <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                                    <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                    <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                    <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                    <%--<asp:ListItem Text="Other" Value="Other"></asp:ListItem>--%>
                                                    <asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
                                                </asp:DropDownList><br />

                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                     Display="Dynamic" ErrorMessage="Please specify Payment Mode" ValidationGroup="val">
                                                     <span class="error" >Please specify Payment Mode</span>
                                                </asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddRegFees" runat="server" CssClass="required field" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddRegFees_SelectedIndexChanged">
                                                    <asp:ListItem Text="Select" Value="999"></asp:ListItem>
                                                    <asp:ListItem Text="Paid" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Unpaid" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Free" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Proxy Case" Value="3"></asp:ListItem>
                                                </asp:DropDownList><br />
                                                <%--<asp:RequiredFieldValidator ID="reqRegFees" runat="server" ControlToValidate="ddRegFees"
                                                    Display="Dynamic" ErrorMessage="Please Select Registration Fees." ValidationGroup="val">
                                                    <span class="error">Please Select Registration Fees.</span>
                                                </asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCounsultingAmt" runat="server" CssClass="field" Text="0"
                                                    MaxLength="20"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regCounsultingAmt" runat="server" ControlToValidate="txtCounsultingAmt"
                                                    ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$" Display="Dynamic"
                                                    ValidationGroup="val"><span class="error">Please enter Only Numbers.</span>
                                                </asp:RegularExpressionValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDOC" runat="server" CssClass="field"></asp:TextBox>
                                                <cc1:CalendarExtender ID="txtDOC_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" PopupButtonID="imgDOC" TargetControlID="txtDOC">
                                                </cc1:CalendarExtender>
                                                &nbsp;<asp:Image ID="imgDOC" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit; width: 18px; height: 18px; top: 5px; left: -5px;" />
                                                <asp:RequiredFieldValidator ID="req_DOC" runat="server" ControlToValidate="txtDOC"
                                                    ValidationGroup="val" Display="Dynamic" ErrorMessage="Consulting date is required!" CssClass="field required">
                                                    <span class="error">Consulting date is required!</span>
                                                </asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regDOC" runat="server" CssClass="field required" ValidationGroup="val" Display="Dynamic"
                                                    ControlToValidate="txtDOC" ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                                                    <span class="error">Enter valid date!</span></asp:RegularExpressionValidator>
                                                <asp:CustomValidator ID="cvDOC" runat="server" Display="Dynamic" ControlToValidate="txtDOC" ValidationGroup="val"
                                                    CssClass="field required" ClientValidationFunction="CallDateFun" ErrorMessage="Please enter valid date!">
                                                    <span class="error">Please enter valid date!</span>
                                                </asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblRelNm" runat="server" Text="Relative Name" Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblRelation" runat="server" Text="Relation" Visible="false"></asp:Label>
                                            </td>
                                            <td id="td_r" runat="server" style="display: none;">Reconsulting Case Paper No.</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txt_RelNm" runat="server" Width="150" Visible="false" CssClass="field"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt_Relation" runat="server" Width="150" Visible="false" CssClass="field"></asp:TextBox>
                                            </td>

                                            <td id="td_R1" runat="server" style="display: none;">
                                                <asp:TextBox ID="txtRCasePaperNo" runat="server" CssClass="field" MaxLength="20" Width="130px" Text="0"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regRCasePaperNo" runat="server" Display="Dynamic"
                                                    CssClass="field required" ControlToValidate="txtRCasePaperNo" ErrorMessage="Please enter valid reconsulting case paper no!"
                                                    ValidationExpression="^[a-zA-Z0-9 / .\s]{0,20}$" ValidationGroup="val">
                                                    <span class="error">Please enter reconsulting valid case paper no!</span>
                                                </asp:RegularExpressionValidator>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr id="tr_Bank" runat="server" style="display: none;">
                                            <td>Card Name :</td>
                                            <td>Card Type :</td>
                                            <td>Reference No. :</td>
                                            <td style="display: none;">Cheque Date :</td>
                                        </tr>
                                        <tr id="tr_Cheque" runat="server" style="display: none;">
                                            <td>
                                                <asp:TextBox ID="txtCardnm" runat="server" CssClass="field" Width="140px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="DDL_CardType" runat="server" Width="160px"
                                                    CssClass="field" AutoPostBack="true">
                                                    <asp:ListItem Text="Debit" Value="Debit" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Credit" Value="Credit"></asp:ListItem>
                                                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCardno" runat="server" CssClass="field" Width="210px"></asp:TextBox>
                                            </td>
                                            <td class="last">
                                                <asp:TextBox ID="txtChqdt" Visible="false" runat="server" CssClass="field" Width="200px" placeholder="DD/MM/YYYY"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                    TargetControlID="txtChqdt">
                                                </cc1:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                                OnClientClick="ConfirmPrint('COMM_Print');"
                                                                Style="margin-top: 2px;" Text="Save" ValidationGroup="val" />
                                                            <asp:TextBox runat="server" ID="txtPID" Style="display: none"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnQR" runat="server" Width="150px" CssClass="textbutton b_submit" OnClick="btnQR_Click"
                                                                Style="margin-top: 2px;" Text="Quick Registration" Visible="false" />

                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chk_PrintConsBill" runat="server" Text="" />
                                                            <asp:Label ID="Label1" runat="server" Text="Generate New Case Form"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="Chk_ConsultingBill" runat="server" Text="" OnCheckedChanged="Chk_ConsultingBill_CheckedChanged" AutoPostBack="true" />
                                                            <asp:Label ID="Label2" runat="server" Text="Print Consulting Bill"></asp:Label>
                                                        </td>

                                                        <td>
                                                            <asp:CheckBox ID="chk_RecForm" runat="server" Text="" Style="display: none" />
                                                            <asp:Label ID="Label3" runat="server" Text="Reconsulting Form" Style="display: none"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chk_AcuteBill" runat="server" Text="" OnCheckedChanged="chk_AcuteBill_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
                                                            <asp:Label ID="Label4" runat="server" Text="Acute Bill"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnReconsulting" runat="server" CssClass="textbutton b_submit" OnClick="btnReconsulting_Click" Style="margin-top: 2px; margin-left: 20px; width: 110px;" Text="Reconsulting" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnVoucher" runat="server" CssClass="textbutton b_submit" OnClick="btnVoucher_Click" Style="margin-top: 2px; margin-left: 20px; width: 110px;" Text="Voucher" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>

        </table>
    </center>
    <%-- </ContentTemplate>
</asp:UpdatePanel>--%>

    <asp:Panel BorderStyle="Outset" BorderWidth="5px" BorderColor="#C4D4B6" ID="PNLWorkSheet" runat="server" Visible="false">
        <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right" ImageUrl="~/Images/FancyClose.png" />
        <asp:Panel ID="pnl1" runat="server" ScrollBars="Auto" Width="945px">
            <CR:CrystalReportViewer ID="CrystalReportViewer2" runat="server" AutoDataBind="True" Height="500px" EnableParameterPrompt="False"
                ReuseParameterValuesOnRefresh="True" ToolPanelView="None" GroupTreeImagesFolderUrl="" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
                EnableDatabaseLogonPrompt="False" />
            <CR:CrystalReportSource ID="CrystalReportSource2" runat="server">
                <Report FileName="~/Reports/OpdReportAks.rpt"></Report>
            </CR:CrystalReportSource>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
