﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterHome1.master" CodeFile="UserLogedInReport.aspx.cs" Inherits="UserLogedInReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
    <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 100%; text-align: center;">
                    <h2>
                        <asp:Label ID="lblHeader" runat="server" Text="User Report"></asp:Label></h2>
                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                        <table width="100%" cellspacing="0px" cellpadding="2px">
                                            <tr>
                                                <td>
                                                    <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                        <tr>
                                                            <td class="tableh1">
                                                                <table id="tbsearch" style="width: 98%;">
                                                                    <tr>
                                                                        <td>
                                                                           Date From
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtfrm" runat="server" CssClass="field" Width="100px"></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                                runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                width: 20px; height: 20px;" />
                                                                        </td>
                                                                        <td style="width:2%">
                                                                         <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                                                                OnClientClick="aspnetForm.target ='_self';" OnClick="btnView_Click" />
                                                                        </td>
                                                                        <td style="width:2%">
                                                                             <div id="export" runat="server" style="float: right; display: block">
                                                                                <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" OnClick="imgexport_Click" />
                                                                            </div>
                                                                        </td>
                                                                      </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                        <table style="width: 100%; background: #f2f2f2" cellspacing="0px" cellpadding="5px">
                            <tr>
                                <td style="text-align: center;">
                                    <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Green;">
                                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></font>
                                </td>
                            </tr>
                            <tr>
                                <td id="gvcol" align="center" style="width: 100%; text-align: center;">
                                    <center>
                                        <asp:GridView ID="GRV" Width="100%" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                            CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="30" ShowHeader="true" OnPageIndexChanging="GRV_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Login Date" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDOB" runat="server" Text='<%# Eval("LoginDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Login Time" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltime" runat="server" Text='<%# Eval("LoginTime")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mobile No." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbluContact" runat="server" Text='<%# Eval("uContact")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="IP Address" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIpAddress" runat="server" Text='<%# Eval("IpAddress")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
