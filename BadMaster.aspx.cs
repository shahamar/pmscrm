﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class BadMaster : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    int userid = 0;
    genral gen = new genral();
    int bmID = 0;
    string strSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["Id"] != null)
            bmID = int.Parse(Request.QueryString["Id"].ToString());

        
        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();

        if (!IsPostBack)
        {
            FillData();
        }

    }

    protected void FillData()
    {
        DDWardType.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT Distinct WardTypeID As wmWardTypeId , WardTypeName As wmType FROM tblWardTypeMaster Where IsDelete = 0", "wmWardTypeId", "wmType", DDWardType);

        
        if (bmID != 0)
        {

            strSQL = "SELECT b.bmNoOfBads , w.wmWardTypeId , w.wmID FROM tblBadMaster b Inner Join tblWardMaster w On b.wmID = w.wmID WHERE bmID=" + bmID + "";
            SqlCommand SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandText = strSQL;
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader sdr = SqlCmd.ExecuteReader();
            while (sdr.Read())
            {
                DDWardType.SelectedValue = sdr["wmWardTypeId"].ToString();
                gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId=" + DDWardType.SelectedValue + "", "wmID", "wmWardNo", DDWardNo);
                DDWardNo.SelectedValue = sdr["wmID"].ToString();
                txtNoOfBads.Text = sdr["bmNoOfBads"].ToString();
            }
            sdr.Close();
            sdr.Dispose();
            con.Close();

        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();

            if (bmID == 0)
                strSQL = "INSERT INTO tblBadMaster(wmID,bmNoOfBads,bmCreatedBy) VALUES (@wmID,@bmNoOfBads,@bmCreatedBy)";
            else
                strSQL = "UPDATE tblBadMaster SET wmID=@wmID,bmNoOfBads=@bmNoOfBads, bmCreatedBy=@bmCreatedBy WHERE bmID=" + bmID + "";

            SqlCommand SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.Parameters.AddWithValue("@wmID", DDWardNo.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@bmNoOfBads", txtNoOfBads.Text.Trim());
            SqlCmd.Parameters.AddWithValue("@bmCreatedBy", userid);

            SqlCmd.ExecuteNonQuery();
            SqlCmd.Dispose();
            con.Close();

            if (bmID == 0)
                Response.Redirect("BadMaster.aspx?msg=Bed Details saved successfully");
            else
                Response.Redirect("ManageBadMaster.aspx?flag=1");
        }
    }

    protected void DDWardType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDWardNo.Items.Clear();
        DDWardNo.Items.Insert(0, new ListItem("Select", ""));
        //gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + DDWardType.SelectedValue + "'", "wmID", "wmWardNo", DDWardNo);
        gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + DDWardType.SelectedValue + "' And IsDelete = 0 ", "wmID", "wmWardNo", DDWardNo);
        
    }
}
