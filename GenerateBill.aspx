﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="GenerateBill.aspx.cs" Inherits="GenerateBill" Title="Generate Bill" %>

<%@ Register Src="billtab.ascx" TagName="Tab" TagPrefix="tab1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <style type="text/css">
        .col1
        {
            width: 14%;
            font-family: Times New Roman;
            color: Black;
            font-size: 16px;
        }
        
        .col2
        {
            width: 86%;
            font-family: Times New Roman;
            color: Red;
            font-size: 16px;
        }
        
        .col3
        {
            width: 10%;
            font-family: Times New Roman;
            color: Black;
            font-size: 16px;
        }
        
        .col4
        {
            width: 30%;
            font-family: Times New Roman;
            color: Red;
            font-size: 16px;
        }
        
        .col5
        {
            width: 20%;
            font-family: Times New Roman;
            color: Black;
            font-size: 16px;
        }
        
        .col6
        {
            width: 36%;
            font-family: Times New Roman;
            color: Red;
            font-size: 16px;
        }
        
        .col7
        {
            width: 36%;
            font-family: Times New Roman;
            font-size: 16px;
            text-align: center;
        }
    </style>
  

<script type="text/javascript">
    $(function () {
        $('[id*=txtOtherCharges]').keydown(function (e) {
            if (e.shiftKey || e.ctrlKey || e.altKey) {
                e.preventDefault();
            } else {
                var key = e.keyCode;
                if (!((key == 8) || (key == 13) ||  (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                    e.preventDefault();
                    $('[id*=txtOtherCharges]').val('');
                }
            }
        });


//        $('[id*=txtOtherCharges]').keypress(function (e) {
//            var regex = new RegExp("^[0-9-]+$");
//            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
//            if (regex.test(str)) {
//                return true;
//            }

//            e.preventDefault();
//            return false;
//        });


    });
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="sc1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        Generate Bills</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto; border: 1.5px solid #E2E2E2;">
                                <table width="100%" cellspacing="0px" cellpadding="2px">
                                    <tr>
                                        <td>
                                            <tab1:Tab ID="tabcon" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; color: Red;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="80%" align="center">
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:GridView ID="GRV1" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
                                                            Width="100%">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Particulars" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="55%">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Particulars")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Base Charge" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <%# Eval("BaseCharge")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Units" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblUnits" runat="server" Text='<%# Eval("Units")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Total Amount" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblGridTotal" runat="server" Text='<%# Eval("TotAmt")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="center" border="1" cellpadding="3" cellspacing="3" style="width: 80%;
                                                border-collapse: collapse; border-style: double; border-width: 1">
                                                <tr>
                                                    <td align="left" style="width: 55%;">
                                                        Other Charges
                                                    </td>
                                                    <td align="right" style="width: 15%;">
                                                        <asp:UpdatePanel ID="UP1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtOtherCharges" runat="server" ClientIDMode="Static" Width="100px" Style="text-align: right;"
                                                                    OnTextChanged="txtOtherCharges_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                             </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td align="right" style="width: 15%;">
                                                    </td>
                                                    <td align="right" style="width: 15%;">
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblTotal" runat="server" Text="00.00"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>

                                                <tr>
                                                <td>
                                                 Payment Mode *
                                                </td>

                                                <td colspan="3">
                                                     <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="160px"
                                    CssClass="field">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                    <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                    <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                      <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                    <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                </asp:DropDownList>

                                    <br />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                        Display="Dynamic" ErrorMessage="Please specify Payment Mode" ValidationGroup="check">
                                                        <span class="error" >Please specify Payment Mode</span></asp:RequiredFieldValidator>
                                                </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="4">

                                                        <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                            Style="margin-top: 2px;" Text="Print" OnClientClick="aspnetForm.target ='_blank';" />

                                                             <asp:Button ID="btn_FinalBillPrint" runat="server" CssClass="textbutton b_submit" OnClick="btn_FinalBillPrint_Click"
                                                            Style="margin-top: 2px; width:110px" Text="Final Bill Print" OnClientClick="aspnetForm.target ='_blank';" />

                                                               <asp:Button ID="btn_Voucher" runat="server" CssClass="textbutton b_submit" OnClick="btn_Voucher_Click"
                                                            Style="margin-top: 2px; width:140px" Text="Generate Voucher" OnClientClick="aspnetForm.target ='_blank';"/>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
