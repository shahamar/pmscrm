﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Drawing;

public partial class PaymentCounter : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string RepType = "";
    string frm, to;
    int i = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            //FillData();
            Bind_FollowUpPaymentDetails();
        }

    }

    //protected void FillData()
    //{
    //    DDL_Doctor.Items.Insert(0, new ListItem("Select", "%"));
    //    gen.FillDropDownList("Select uId , uFname +' '+ uMname +' '+ uLname As DNAME from tblUser Where IsDelete = 0 and uRole = 2 ", "uId", "DNAME", DDL_Doctor);
    // }

    private void Bind_FollowUpPaymentDetails()
    {
        string[] _frm;
        string[] _to;


        if (txtfrm.Text.Contains("/"))
        {
            _frm = txtfrm.Text.ToString().Split('/');
            _to = txtto.Text.ToString().Split('/');

            frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";
        }

        if (txtfrm.Text.Contains("-"))
        {
            _frm = txtfrm.Text.ToString().Split('-');
            _to = txtto.Text.ToString().Split('-');

            frm = _frm[2] + "-" + _frm[1] + "-" + _frm[0] + " 00:00:00";
            to = _to[2] + "-" + _to[1] + "-" + _to[0] + " 23:55:00";
        }

        StrSQL = "SELECT ROW_NUMBER() over( Order By fdId desc) As SNO, fdId As fdId, pdInitial + ' '+ pdFname + ' ' + pdMname + ' '+ pdLname As PNAME, pdCasePaperNo ";
        StrSQL += ", Rem_Per_Description As RemedyPeriod, Case When fdDoseType = 1 Then 'Regular(3 Times/Day)' when fdDoseType = 2 Then 'Double(6 Times/Day)' When fdDoseType = 3 Then 'Hourly(10 Times/Day)' WHEN fdDoseType = 4 THEN 'Add Emg' When fdDoseType = 5 Then 'Liquid' When fdDoseType = 6 Then 'Single' else '' end As DoseType ";
        StrSQL += ",isNull(fdAddEmgAmt,0) As  AddEmg ,CASE WHEN IsExtraDoses=1 THEN 100 ELSE IsNull(ExtraDosAmt, 0) END ExtraDos,Case When  fdIsReconsulting = 0 Then Case when Rem_Per_Description='Add Emg' then 0  when Rem_Per_Description='Extra Doses' then isNull(fdCharges,0)-isNull(ExtraDosAmt,0) else isNull(fdCharges,0) End  when  fdIsReconsulting = 1 Then IsNull(fdCharges,0) else 0 end As  Charges ";
        //StrSQL += ",[dbo].[GetPreviousBalanceAks] (fdPatientId,fdId) As PreviousBalance, isNull(fdAddEmgAmt,0) + IsNull(ExtraDosAmt,0) + IsNull(Case When fdIsReconsulting = 0 Then isNull(fdCharges,0) when fdIsReconsulting = 1 Then IsNull(fdConsultingCharges,0) else 0 end,0) + IsNull([dbo].[GetPreviousBalanceAks] (fdPatientId,fdId),0) As TotalAmt ";
        StrSQL += ",0.00 As PreviousBalance,CASE WHEN Rem_Per_Description = 'Add Emg' THEN fdCharges WHEN Rem_Per_Description = 'Extra Doses' THEN fdCharges else IsNull(isNull(fdAddEmgAmt, 0)+isNull(ExtraDosAmt, 0)+CASE WHEN fdIsReconsulting = 0 THEN isNull(fdCharges, 0) WHEN fdIsReconsulting = 1 THEN IsNull(fdCharges, 0) ELSE 0 END, 0) end AS TotalAmt ";
        StrSQL += ",IsNull(PaidAmount, CASE WHEN Rem_Per_Description = 'Add Emg' THEN fdCharges WHEN Rem_Per_Description = 'Extra Doses' THEN fdCharges else IsNull(isNull(fdAddEmgAmt, 0)+isNull(ExtraDosAmt, 0)+CASE WHEN fdIsReconsulting = 0 THEN isNull(fdCharges, 0) WHEN fdIsReconsulting = 1 THEN IsNull(fdConsultingCharges, 0) ELSE 0 END, 0) end + IsNull([dbo].[GetPreviousBalanceAks](fdPatientId, fdId), 0)) AS PaidAmount ";
        StrSQL += ",IsNull(FD.Payment_Mode,'Cash') As Payment_Mode, fdCreatedBy, Case When PaidAmount IS Null Then '0' else '1' end As Stat, CASE ISNULL(CAST(fdCharges AS VARCHAR),'') WHEN '' THEN '' ELSE '<a href=''OPDBill.aspx?id='+ CAST(fdId AS VARCHAR) + '&IsReconsulting='+ CAST(ISNULL(fdIsReconsulting,0) AS VARCHAR) + '''& target=_blank><img src=''images/print1.png'' /></a>' END AS 'PrintBill',ISNULL(fdIsCourier,0) AS fdIsCourier ";
        StrSQL += "From tblPatientDetails PD Inner Join tblFollowUpDatails FD On PD.pdID = FD.fdPatientId Left Join tblRemedyPeriodMaster RPM On RPM.Rem_Per_ID = FD.fdRemedyPeriod Where IsPharmacyDone = 1 And ";

        if(frm != "" && to != "")
        {
            StrSQL += "fdDate Between '"+ frm +"' and '"+ to +"' ";
        }
        //if(DDL_Doctor.SelectedValue != "")
        //{
        //    StrSQL += "And fdCreatedBy Like '%"+ DDL_Doctor.SelectedValue +"%' ";
        //}
        if(txtSearchCPN.Text != "")
        {
            StrSQL += "And PD.pdCasePaperNo Like '%" + txtSearchCPN.Text + "%' ";
        }
        if(DDL_Pay_Mode.SelectedValue != "")
        {
            StrSQL += "And FD.Payment_Mode Like '%"+ DDL_Pay_Mode.SelectedValue +"%' ";
        }
        if(txtSearch.Text != "")
        {
            StrSQL += "AND PD.pdFname +' '+ PD.pdMname +' '+ PD.pdLname Like REPLACE('%" + txtSearch.Text.Trim() + "%', ' ' , '%') ";
        }

        StrSQL += "Order By fdId desc";
        DataTable dt = gen.getDataTable(StrSQL);
        if (dt.Rows.Count > 0)
        {
            GVD_FollowupPaymentDetails.DataSource = dt;
            GVD_FollowupPaymentDetails.DataBind();
        }
        else
        {
            GVD_FollowupPaymentDetails.EmptyDataText.ToString();
            GVD_FollowupPaymentDetails.DataBind();
        }
            //StrSQL = "Proc_GetFollowupPaymentDetails";
            //SqlCmd = new SqlCommand(StrSQL, conn);
            //SqlDataReader dr;
            //SqlCmd.CommandType = CommandType.StoredProcedure;

            //SqlCmd.Parameters.AddWithValue("@FrmDate", frm);
            //SqlCmd.Parameters.AddWithValue("@ToDate", to);
            //SqlCmd.Parameters.AddWithValue("@User", DDL_Doctor.SelectedValue);
            //SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
            //if (conn.State == ConnectionState.Closed)
            //{
            //    conn.Open();
            //    dr = SqlCmd.ExecuteReader();
            //    DataTable dt = new DataTable();
            //    dt.Load(dr);
            //    if (dt.Rows.Count > 0)
            //    {
            //        GVD_FollowupPaymentDetails.DataSource = dt;
            //        GVD_FollowupPaymentDetails.DataBind();
            //    }
            //    else
            //    {
            //        GVD_FollowupPaymentDetails.DataSource = null;
            //        GVD_FollowupPaymentDetails.DataBind();
            //    }
            //    conn.Close();
            //    dr.Dispose();
            //}
        }

    protected void btnView_Click(object sender, EventArgs e)
    {
        Bind_FollowUpPaymentDetails();
    }

    protected void GVD_FollowupPaymentDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "Payment")
        {
            try
            {
                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                TextBox txt_ChargesPaid = row.FindControl("txt_ChargesPaid") as TextBox;
                DropDownList DDL_Pay_Mode = row.FindControl("DDL_Pay_Mode") as DropDownList;
                Label lblResult = row.FindControl("lblResult") as Label;
                Label lbl_IsCourier = row.FindControl("lblIsCourier") as Label;
                
                if (DDL_Pay_Mode.SelectedValue == "" && lbl_IsCourier.Text == "false")
                {
                    lblResult.Visible = true;
                    lblResult.Text = "Compulsory";
                    lblResult.ForeColor = Color.DarkRed;
                    return;
                }
                

                decimal paidcharge = txt_ChargesPaid.Text == "" ? 0 : decimal.Parse(txt_ChargesPaid.Text);
                SqlCmd = new SqlCommand();
                SqlCmd.CommandText = "proc_SaveFollowupPaymentDetails";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlCmd.Parameters.AddWithValue("@FDID", e.CommandArgument);
                SqlCmd.Parameters.AddWithValue("@PaidAmt", paidcharge);
                if(lbl_IsCourier.Text == "True")
                {
                    SqlCmd.Parameters.AddWithValue("@Payment_Mode", "Courier");
                }
                else
                {
                    SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
                }
                
                SqlCmd.Parameters.AddWithValue("@PaidBy", uid);
              
                i = SqlCmd.ExecuteNonQuery();
                con.Close();
                SqlCmd.Dispose();

                if (i > 0)
                {
                    lblMessage.Text = "Follow up Charges Saved Successfully";
                    lblMessage.ForeColor = Color.Green;
                    Bind_FollowUpPaymentDetails();
                    
                }
                else
                {
                    lblMessage.Text = "Follow up Charges Not Saved....!!!!!";
                    lblMessage.ForeColor = Color.DarkRed;
                }

            }
            catch
            { }
        }

        if (e.CommandName == "Allow")
        {
            GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
            TextBox txt_ChargesPaid = row.FindControl("txt_ChargesPaid") as TextBox;
            DropDownList DDL_Pay_Mode = row.FindControl("DDL_Pay_Mode") as DropDownList;
            ImageButton btnPayAmount = row.FindControl("btnPayAmount") as ImageButton;

            txt_ChargesPaid.Enabled = false;
            DDL_Pay_Mode.Enabled = true;
            btnPayAmount.Visible = true;
        }
        
    }

    protected void GVD_FollowupPaymentDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList DDL_Pay_Mode = (DropDownList)e.Row.FindControl("DDL_Pay_Mode");
            string lbl_Pay_Mode = (e.Row.FindControl("lbl_Pay_Mode") as Label).Text;
            Label lbl_IsCourier = (Label)e.Row.FindControl("lblIsCourier");

            if (lbl_IsCourier.Text == "True")
            {
                DDL_Pay_Mode.Items.FindByValue("Courier").Selected = true;
                DDL_Pay_Mode.Enabled = false;
                //DDL_Pay_Mode.Items.FindByValue(lbl_Pay_Mode.ToString()).Selected = true;
            }
            else
            {
                DDL_Pay_Mode.Items.FindByValue(lbl_Pay_Mode.ToString()).Selected = true;
            }
            Label lbl_Stat = (Label)e.Row.FindControl("lbl_Stat");
            TextBox txt_ChargesPaid = (TextBox)e.Row.FindControl("txt_ChargesPaid");
            ImageButton btnPayAmount = (ImageButton)e.Row.FindControl("btnPayAmount");
           
            txt_ChargesPaid.Enabled = false;
            if (lbl_Stat.Text == "1")
            {
                txt_ChargesPaid.Enabled = false;
                DDL_Pay_Mode.Enabled = false;
            }
            if (lbl_Stat.Text == "0")
            {
               txt_ChargesPaid.BackColor = Color.Yellow;	
            }
            if (DDL_Pay_Mode.SelectedValue != "" && lbl_Pay_Mode!="")
            {
                btnPayAmount.Visible = false;
            }

            
        }
	
    }

    protected void GVD_FollowupPaymentDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVD_FollowupPaymentDetails.PageIndex = e.NewPageIndex;
        Bind_FollowUpPaymentDetails();
        
    }

    //protected void GVD_FollowupPaymentDetails_RowEditing(object sender, GridViewEditEventArgs e)
    //{

    //}



}