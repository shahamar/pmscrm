﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="billtab.ascx.cs" Inherits="billtab" %>
<style>
.tabdiv
{
	border-width: 2px 2px 0px; 
	border-style: solid solid none; 
	border-color: #E2E2E2; 
	position: inherit; 
	height: auto;
}

#tabtbl
{
	color: #000;
    font-size: 12px;
    font-weight: lighter;
    padding: 2px;
    background-color:#F1F1F1
}

.alt1 
{
	width:65px;
    }
.alt2 {
    width:125px;
}

</style>

<div class="tabdiv">
    <table id="tabtbl" style="width:100%;" cellpadding="2px" cellspacing="0px" border="1px">
        <tr>
            <td colspan="6">
                <div style="float:right;">&nbsp;
                <a href="" onclick="parent.history.back(); return false;">Back</a> 
                <%--<a href="ProfarmaOfCase.aspx">Back To My Case</a>--%>
                </div> 
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Patient Name :</td>
            <td class="alt2">
                <asp:Label ID="lblPatientName" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Age :</td>
            <td class="alt2">
                <asp:Label ID="lblAge" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Gender :</td>
            <td class="alt2">
                <asp:Label ID="lblGender" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Tele :</td>
            <td class="alt2">
                <asp:Label ID="lbltele" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Mobile :</td>
            <td class="alt2">
                <asp:Label ID="lblmob" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Email :</td>
            <td class="alt2">
                <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Patient ID :</td>
            <td class="alt2">
                <asp:Label ID="lblPatientId" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Case Paper No :</td>
            <td class="alt2">
                <asp:Label ID="lblCasePaperNo" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Cassette No :</td>
            <td class="alt2">
                <asp:Label ID="lblCassetteNo" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td class="alt1">Occupation :</td>
            <td class="alt2"><asp:Label ID="lblOccupation" runat="server" Text=""></asp:Label></td>
            <td class="alt1">Doctor Name :</td>
            <td class="alt2"><asp:Label ID="lblDoctorName" runat="server"></asp:Label></td>
            <td class="alt1"></td>
            <td class="alt2"></td>
        </tr>
        
    </table>

    
</div>