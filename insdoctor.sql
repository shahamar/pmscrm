USE [PMS]
GO

/****** Object:  StoredProcedure [dbo].[Ins_DoctorMaster]    Script Date: 04/17/2013 15:44:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Ins_DoctorMaster]
@dFName VARCHAR(100)  
,@dMName VARCHAR(100)  
,@dLName VARCHAR(100)  
,@dContactNo VARCHAR(20)  
,@dMobileNo VARCHAR(20)  
,@dCreatedBy INT  
,@dDOJ SMALLDATETIME  
,@dAddressLine1 VARCHAR(100)  
,@dAddressLine2 VARCHAR(100)  
,@dType VARCHAR(50) 
,@dEmailId VARCHAR(60) 
AS  
BEGIN  
 INSERT INTO tblDoctorMaster  
 (  
  dFName  
 ,dMName  
 ,dLName  
 ,dContactNo  
 ,dMobileNo  
 ,dCreatedBy   
 ,dDOJ  
 ,dAddressLine1  
 ,dAddressLine2  
 ,dType
 ,dEmailId  
 )  
 VALUES  
 (  
  @dFName  
 ,@dMName  
 ,@dLName  
 ,@dContactNo  
 ,@dMobileNo  
 ,@dCreatedBy  
 ,@dDOJ  
 ,@dAddressLine1  
 ,@dAddressLine2  
 ,@dType  
 ,@dEmailId
 )  
END
GO


