﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvestigationHistory.aspx.cs"
    Inherits="InvestigationHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Search Patient</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />

    <script type="text/javascript" src="js/custom-form-elements.js"></script>

    <style>
        .b_submit
        {
            background: url(        "images/b_submit.gif" ) no-repeat scroll 0 0 transparent;
            border: 0 none;
            display: inline-block;
            font-family: arial;
            vertical-align: middle;
            width: 67px;
            height: 32px;
        }
        .textbutton
        {
            color: #FFFFFF !important;
            font-size: 12px;
            font-weight: bold;
            text-align: center;
            margin-top: -8px;
        }
        .loginform
        {
            font-family: Verdana;
        }
        .left
        {
            float: left;
        }
        .formmenu
        {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #E6E6E6;
            margin-bottom: 10px;
            padding: 10px;
        }
        .tableh1
        {
            background-image: url(        "images/tile_back1.gif" );
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-size: 10px;
            height: 18px;
            padding: 8px 12px 8px 8px;
            font-family: verdana;
            font-weight: bold;
        }
    </style>
    <style>
        #tbsearch td
        {
            text-align: left;
            vertical-align: middle;
        }
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        #gvcol div
        {
            margin-top: -10px;
        }
        .head1
        {
            width: 15%;
            font-size: small;
        }
        .head2
        {
            width: 5%;
            font-size: small;
        }
        .head3
        {
            width: 15%;
            font-size: small;
        }
        .head4
        {
            width: 20%;
        }
        .head5
        {
            width: 10%;
            font-size: small;
        }
        .head6
        {
            width: 10%;
            font-size: small;
        }
        .head7
        {
            width: 10%;
            font-size: small;
        }
    </style>
</head>
<body style="background-color: #FFFFFF;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div>
                <table style="width: 100%; background-color: #FFFFFF;">
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="login-area margin_ten_right" style="width: 98%;">
                                <h2>
                                    Investigation History</h2>
                                <div class="formmenu">
                                    <div class="loginform">
                                        <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                            style="height: auto; border: 1.5px solid #E2E2E2;">
                                            <div style="padding: 15px; height: auto;">
                                                <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                    <tr>
                                                        <td colspan="3">
                                                            <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 100%;
                                                                border: 1px solid #C1C1C1;">
                                                                <tr>
                                                                    <th class="head7">
                                                                        Visit Time
                                                                    </th>
                                                                    <th class="head1">
                                                                        Patient Name
                                                                    </th>
                                                                    <th class="head2">
                                                                        Bed No
                                                                    </th>
                                                                    <th class="head3">
                                                                        Complaint
                                                                    </th>
                                                                    <th class="head4">
                                                                        Remedy
                                                                    </th>
                                                                    <th class="head5">
                                                                        BP MIN
                                                                    </th>
                                                                    <th class="head6">
                                                                        BP MAX
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <tr>
                                                            <td id="gvcol" colspan="3">
                                                                <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                    CssClass="mGrid" PagerStyle-CssClass="pgr" ShowHeader="false" Width="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <%# Eval("VisitTime")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <%# Eval("PName")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <%# Eval("BedNo")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <%# Eval("Complaint")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <%# Eval("Remedy")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <%# Eval("BpMin")%></ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <%# Eval("BpMax")%></ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
