using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class StockManager : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlCommand Sqlcmd, SqlCmd;
    SqlDataReader Sqldr;
    SqlDataAdapter dapt;
    string strSQL = "";
    string StrSQL = "";
    int Pid = 0;
    string uid;

    protected void Page_Load(object sender, EventArgs e)
	{
        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }

        if(Request.QueryString["Pid"] != null)
        {
            Pid = Convert.ToInt32(Request.QueryString["Pid"].ToString());
        }

        if (Request.QueryString["flag"] == "1")
        {
            lblMessage.Text = "Stock Added Stuccessfully...!"; 
        }
        if (Request.QueryString["flag"] == "2")
        {
            lblMessage.Text = "Stock Updated Successfully...!";
        }
        if (!IsPostBack)
        {
            txtReceivedDate.Text = String.Format("{0:dd/MM/yyyy}",DateTime.Now);
            this.FillData();
            
        }
	}
    public void BindCompany()
    {
        DDLCompany.Items.Clear();
        DDLCompany.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select Cid, Cname  from tblCompanyMaster Where IsDelete = 0", "Cid", "Cname", DDLCompany);
    }

    private void FillData()
    {
        BindCompany();
        if (Pid != 0)
        {
            TR_Bal.Visible = true;

            strSQL = "Select * from tblStockManage sm Inner join tblCompanyMaster cm on sm.StkCompanyName = cm.Cid where sm.StkPid = " + Pid;
            Sqlcmd = new SqlCommand(strSQL, con);
            Sqlcmd.CommandType = CommandType.Text;
            Sqlcmd.Connection = con;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
                SqlDataReader Sqldr = Sqlcmd.ExecuteReader();
                while (Sqldr.Read())
                {
                    if (DDLCompany.SelectedValue == "")
                    {
                        DDLCompany.SelectedValue = Sqldr["StkCompanyName"].ToString();
                    }
                    //txtCompanyName.Text = Sqldr["StkCompanyName"].ToString();
                    txtReceivedDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["StkDOR"].ToString()));
                    txtInvoiceNo.Text = Sqldr["StkInvoiceNo"].ToString();
                    txtProduct.Text = Sqldr["StkProductName"].ToString();
                    txtQty.Text = Sqldr["StkQty"].ToString();
                    txtUnit.Text = Sqldr["StkUnit"].ToString();
                    txtProDescription.Text = Sqldr["StkProductDesc"].ToString();
                    txtCGST.Text = Sqldr["StkCGST"].ToString();
                    txtSGST.Text = Sqldr["StkSGST"].ToString();
                    txtTotalPrice.Text = Sqldr["StkTotalPrice"].ToString();
                    txtGrandTot.Text = Sqldr["StkGrandTotal"].ToString();
                    txtIssueStock.Text = Sqldr["StkIssueStock"].ToString();
                    txtReceivedStock.Text = Sqldr["StkReceivedStock"].ToString();
                    txtBalanceStock.Text = Sqldr["StkBalanceStock"].ToString();
                }
                Sqldr.Close();
                Sqlcmd.Dispose();
                con.Close();
                con.Dispose();
            }
        }
        else
        {
            TR_Bal.Visible = false;
        }
     }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (Pid == 0)
            {
                strSQL = "Sp_InsStockManage";
            }
            else
            {
                strSQL = "Sp_UptStockManage";
            }

            string[] _dor = txtReceivedDate.Text.ToString().Split('/');
            string dor = _dor[2] + '/' + _dor[1] + '/' + _dor[0];
            
            Sqlcmd = new SqlCommand(strSQL, con);
            Sqlcmd.CommandType = CommandType.StoredProcedure;
            Sqlcmd.Parameters.AddWithValue("@StkCompanyName", DDLCompany.SelectedValue);
            Sqlcmd.Parameters.AddWithValue("@StkDOR", dor);
            Sqlcmd.Parameters.AddWithValue("@StkInvoiceNo", txtInvoiceNo.Text);
            Sqlcmd.Parameters.AddWithValue("@StkProductName", txtProduct.Text);
            Sqlcmd.Parameters.AddWithValue("@StkQty", txtQty.Text);
            Sqlcmd.Parameters.AddWithValue("@StkUnit", txtUnit.Text);
            Sqlcmd.Parameters.AddWithValue("@StkProductDesc", txtProDescription.Text);
            Sqlcmd.Parameters.AddWithValue("@StkCGST", txtCGST.Text);
            Sqlcmd.Parameters.AddWithValue("@StkSGST", txtSGST.Text);
            Sqlcmd.Parameters.AddWithValue("@StkTotalPrice", txtTotalPrice.Text);
            Sqlcmd.Parameters.AddWithValue("@StkGrandTotal", txtGrandTot.Text);
            Sqlcmd.Parameters.AddWithValue("@StkCreatedBy", uid);
            
            Sqlcmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
                if (Pid != 0)
                {
                    Sqlcmd.Parameters.AddWithValue("@StkPid", Pid);
                    Sqlcmd.Parameters.AddWithValue("@StkIssueStock", txtIssueStock.Text);
                    Sqlcmd.Parameters.AddWithValue("@StkReceivedStock", txtReceivedStock.Text);
                    Sqlcmd.Parameters.AddWithValue("@StkBalanceStock", txtBalanceStock.Text);
                   
                }
                else
                {
                    Sqlcmd.Parameters.AddWithValue("@StkPid", Pid);
                }
                Sqlcmd.ExecuteNonQuery();
                if (Pid == 0)
                {
                    Response.Redirect("StockManager.aspx?flag=1");
                }
                else
                {
                    Response.Redirect("StockManager.aspx?flag=2");
                    
                }
                Sqlcmd.Dispose();
                con.Close();
            }
        }
    }
}