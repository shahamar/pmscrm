﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BillFormat : System.Web.UI.Page
{
    string strSQL = "";
    genral gen = new genral();
    string ptdID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
            ptdID = Request.QueryString["id"].ToString();

        //lblDate.Text = DateTime.Now.ToString("dd/MMM/yyyy");
        FillData();
    }
    protected void FillData()
    {
        strSQL = "SELECT CONVERT(VARCHAR(10), ptdFromDate, 105) AS Date,ptdReceiptNo,ptd.ptdId,ptd.ptdTMId,ptd.ptdpdId,ptd.ptdFromDate,ptd.ptdToDate,[dbo].[fnNumToWords](ptd.ptdCharges,'Y') 'Charges',ptd.ptdCharges,tm.tmTestName";
        strSQL += ",pdInitial +' '+pdFname +' '+pdMname +' '+ pdLname AS 'PName',CASE tmTestName WHEN 'P.F.T' THEN 'PULMONARY FUNCTION TEST' WHEN '3D-CCG' THEN '3 DIMENSIONAL CARDIOVASCULAR CARTOGRAPHY (3 DCCG)' WHEN 'E. C. G' THEN 'E. C. G' WHEN 'Glucometer' THEN 'GLUCOMETER' WHEN 'Monitor' THEN 'MONITOR'  WHEN 'EECP' THEN 'Enhanced External Counter Pulsation' END AS 'RName' ";
        strSQL += ",pdAge +' Year' As  pdAge,pdCasePaperNo,'Paid By ' + IsNull(ptd.Payment_Mode,'CASH') As 'Payment_Mode',uFname As 'UNAME' ";
        strSQL += " FROM dbo.tblPatientTestDetails ptd INNER JOIN dbo.tblTestMaster tm ON ptd.ptdTMId = tm.tmId INNER JOIN dbo.tblPatientDetails PD ON PD.pdID = ptd.ptdpdId Left Join tblUser U On U.[uId] = ptd.ptdCreatedBy ";
        strSQL += " Where ptdId  IN (" + ptdID + ")";
        DataTable dt = gen.getDataTable(strSQL);

        //lblPName.Text = dt.Rows[0]["PName"].ToString();
        //lblPaidAmt.Text = AmountInWords(Convert.ToDecimal(dt.Rows[0]["ptdCharges"].ToString()));
        //lblRs.Text = dt.Rows[0]["ptdCharges"].ToString();
        //lblRcptNo.Text = dt.Rows[0]["ptdReceiptNo"].ToString();

        //if (dt.Rows[0]["tmTestName"].ToString() == "P.F.T")
        //    lblReportName.Text = "PULMONARY FUNCTION TEST";

        //if (dt.Rows[0]["tmTestName"].ToString() == "E. C. G")
        //    lblReportName.Text = "E. C. G";

        //if (dt.Rows[0]["tmTestName"].ToString() == "3D-CCG")
        //    lblReportName.Text = "3 DIMENSIONAL CARDIOVASCULAR CARTOGRAPHY (3 DCCG)";

        //if (dt.Rows[0]["tmTestName"].ToString() == "Glucometer")
        //    lblReportName.Text = "GLUCOMETER";

        //if (dt.Rows[0]["tmTestName"].ToString() == "Monitor")
        //    lblReportName.Text = "MONITOR";

        rptBill.DataSource = dt;
        rptBill.DataBind();

    }

    #region AmountInWords
    public string AmountInWords(decimal Num)
    {
        string returnValue;
        //I have created this function for converting amount in indian rupees (INR).
        //You can manipulate as you wish like decimal setting, Doller (any currency) Prefix.


        string strNum;
        string strNumDec;
        string StrWord;
        strNum = Num.ToString();


        if (strNum.IndexOf(".") + 1 != 0)
        {
            strNumDec = strNum.Substring(strNum.IndexOf(".") + 2 - 1);


            if (strNumDec.Length == 1)
            {
                strNumDec = strNumDec + "0";
            }
            if (strNumDec.Length > 2)
            {
                strNumDec = strNumDec.Substring(0, 2);
            }
            strNum = strNum.Substring(0, strNum.IndexOf(".") + 0);
            StrWord = ((double.Parse(strNum) == 1) ? " Rupee " : " Rupees ") + NumToWord((decimal)(double.Parse(strNum))) + ((double.Parse(strNumDec) > 0) ? (" and Paise" + cWord3((decimal)(double.Parse(strNumDec)))) : "");
        }
        else
        {
            StrWord = ((double.Parse(strNum) == 1) ? " Rupee " : " Rupees ") + NumToWord((decimal)(double.Parse(strNum)));
        }
        returnValue = StrWord + " Only";
        return returnValue;
    }
    public static string NumToWord(decimal Num)
    {
        string returnValue;


        //I divided this function in two part.
        //1. Three or less digit number.
        //2. more than three digit number.
        string strNum;
        string StrWord;
        strNum = Num.ToString();


        if (strNum.Length <= 3)
        {
            StrWord = cWord3((decimal)(double.Parse(strNum)));
        }
        else
        {
            StrWord = cWordG3((decimal)(double.Parse(strNum.Substring(0, strNum.Length - 3)))) + " " + cWord3((decimal)(double.Parse(strNum.Substring(strNum.Length - 2 - 1))));
        }
        returnValue = StrWord;
        return returnValue;
    }
    public static string cWordG3(decimal Num)
    {
        string returnValue;
        //2. more than three digit number.
        string strNum = "";
        string StrWord = "";
        string readNum = "";
        strNum = Num.ToString();
        if (strNum.Length % 2 != 0)
        {
            readNum = System.Convert.ToString(double.Parse(strNum.Substring(0, 1)));
            if (readNum != "0")
            {
                StrWord = retWord(decimal.Parse(readNum));
                readNum = System.Convert.ToString(double.Parse("1" + strReplicate("0", strNum.Length - 1) + "000"));
                StrWord = StrWord + " " + retWord(decimal.Parse(readNum));
            }
            strNum = strNum.Substring(1);
        }
        while (!System.Convert.ToBoolean(strNum.Length == 0))
        {
            readNum = System.Convert.ToString(double.Parse(strNum.Substring(0, 2)));
            if (readNum != "0")
            {
                StrWord = StrWord + " " + cWord3(decimal.Parse(readNum));
                readNum = System.Convert.ToString(double.Parse("1" + strReplicate("0", strNum.Length - 2) + "000"));
                StrWord = StrWord + " " + retWord(decimal.Parse(readNum));
            }
            strNum = strNum.Substring(2);
        }
        returnValue = StrWord;
        return returnValue;
    }
    public static string cWord3(decimal Num)
    {
        string returnValue;
        //1. Three or less digit number.
        string strNum = "";
        string StrWord = "";
        string readNum = "";
        if (Num < 0)
        {
            Num = Num * -1;
        }
        strNum = Num.ToString();

        if (strNum.Length == 3)
        {
            readNum = System.Convert.ToString(double.Parse(strNum.Substring(0, 1)));
            StrWord = retWord(decimal.Parse(readNum)) + " Hundred";
            strNum = strNum.Substring(1, strNum.Length - 1);
        }


        if (strNum.Length <= 2)
        {
            if (double.Parse(strNum) >= 0 && double.Parse(strNum) <= 20)
            {
                StrWord = StrWord + " " + retWord((decimal)(double.Parse(strNum)));
            }
            else
            {
                StrWord = StrWord + " " + retWord((decimal)(System.Convert.ToDouble(strNum.Substring(0, 1) + "0"))) + " " + retWord((decimal)(double.Parse(strNum.Substring(1, 1))));
            }
        }
        strNum = Num.ToString();
        returnValue = StrWord;
        return returnValue;
    }
    public static string retWord(decimal Num)
    {
        string returnValue;
        //This two dimensional array store the primary word convertion of number.
        returnValue = "";
        object[,] ArrWordList = new object[,] { { 0, "" }, { 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" }, { 6, "Six" }, { 7, "Seven" }, { 8, "Eight" }, { 9, "Nine" }, { 10, "Ten" }, { 11, "Eleven" }, { 12, "Twelve" }, { 13, "Thirteen" }, { 14, "Fourteen" }, { 15, "Fifteen" }, { 16, "Sixteen" }, { 17, "Seventeen" }, { 18, "Eighteen" }, { 19, "Nineteen" }, { 20, "Twenty" }, { 30, "Thirty" }, { 40, "Forty" }, { 50, "Fifty" }, { 60, "Sixty" }, { 70, "Seventy" }, { 80, "Eighty" }, { 90, "Ninety" }, { 100, "Hundred" }, { 1000, "Thousand" }, { 100000, "Lakh" }, { 10000000, "Crore" } };


        int i;
        for (i = 0; i <= (ArrWordList.Length - 1); i++)
        {
            if (Num == System.Convert.ToDecimal(ArrWordList[i, 0]))
            {
                returnValue = (string)(ArrWordList[i, 1]);
                break;
            }
        }
        return returnValue;
    }
    public static string strReplicate(string str, int intD)
    {
        string returnValue;
        //This fucntion padded "0" after the number to evaluate hundred, thousand and on....
        //using this function you can replicate any Charactor with given string.
        int i;
        returnValue = "";
        for (i = 1; i <= intD; i++)
        {
            returnValue = returnValue + str;
        }
        return returnValue;
    }
    #endregion
}
