﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class NewAppointment : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string userrole;
    string strSQL;
    int pdID = 0;
    SqlDataReader Sqldr;
    string flag;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 25";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();

        if (!IsPostBack)
        {
            //txtDOC.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            //gen.FillDropDownList("Select Distinct pdInitial From dbo.tblPatientDetails Where pdInitial IS NOT NULL", "pdInitial", "pdInitial", ddintial);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string[] _doc = txtDOC.Text.ToString().Split('/');
            string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

            strSQL = "InsAppoinmentDetails";

            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;

            SqlCmd.Parameters.AddWithValue("@PatientID", 0);
            SqlCmd.Parameters.AddWithValue("@AppointmentDate", doc);
            SqlCmd.Parameters.AddWithValue("@adCreatedBy", userid);
            SqlCmd.Parameters.AddWithValue("@App_Type", "CALL");

            SqlCmd.Parameters.AddWithValue("@pdInitial", ddintial.SelectedValue.ToString());
            SqlCmd.Parameters.AddWithValue("@pdFname", txtfname.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdMname", txtmname.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdLname", txtlname.Text.ToString());
            
            SqlCmd.Parameters.AddWithValue("@pdAdd1", txtAddress1.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdAdd2", txtAddress2.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdAdd3", txtAddress3.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdAdd4", txtAddress4.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdTele", txttele.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdMob", txtmob.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdemail", txtemail.Text.ToString());
            
            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.Add("@adid", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.AddWithValue("@pdWeight", txtWeight.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@Pat_Type", "N");

            if (con.State == ConnectionState.Closed)
                con.Open();
            int reslt;
            SqlCmd.ExecuteNonQuery();
            reslt = (int)SqlCmd.Parameters["@Error"].Value;

            if (reslt == 0)
            {
                //Response.Redirect("NewAppointment.aspx?msg=Appointment saved successfully.");
                Response.Redirect("Home.aspx?MSG=Appointment saved successfully");
            }
            SqlCmd.Dispose();
            con.Close();
        }
    }
}
