USE [PMS]
GO

/****** Object:  StoredProcedure [dbo].[Upt_DoctorMaster]    Script Date: 04/17/2013 15:45:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Upt_DoctorMaster]
@did INT  
,@dFName VARCHAR(100)  
,@dMName VARCHAR(100)  
,@dLName VARCHAR(100)  
,@dContactNo VARCHAR(20)  
,@dMobileNo VARCHAR(20)  
,@dCreatedBy INT  
,@dDOJ SMALLDATETIME  
,@dAddressLine1 VARCHAR(100)  
,@dAddressLine2 VARCHAR(100)  
,@dType VARCHAR(50) 
,@dEmailId VARCHAR(60) 
AS  
BEGIN  
 UPDATE tblDoctorMaster SET   
  dFName=@dFName  
 ,dMName=@dMName  
 ,dLName=@dLName  
 ,dContactNo=@dContactNo  
 ,dMobileNo=@dMobileNo  
 ,dCreatedBy=@dCreatedBy  
 ,dDOJ=@dDOJ   
 ,dAddressLine1=@dAddressLine1   
 ,dAddressLine2=@dAddressLine2   
 ,dType=@dType
 ,dEmailId=@dEmailId   
 WHERE did=@did  
   
END
GO


