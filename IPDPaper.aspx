﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IPDPaper.aspx.cs" Inherits="IPDPaper" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>IPD Paper</title>
    <script type="text/javascript">
        function PrintDiv() {
            w = window.open();
            w.document.write(document.getElementById("pnlPrint").innerHTML);
            w.print();
            w.close();
            close();
        }
	function PrintDiv1() {
            w = window.open();
            w.document.write(document.getElementById("pnlPrint1").innerHTML);
            w.print();
            w.close();
            close();
        }
    </script>
    <style>
       body
       {
           font-family:Times New Roman;
           font-size:12px;
    </style>
</head>
<body>
<center>
    <form id="form1" runat="server">
    <div style="text-align: center;">
        <table width="60%" align="center">
            <tr>
                <td align="right">
                    <img src="images/print.png" style="cursor: pointer;" onclick="PrintDiv()" />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: center">
        <asp:Panel ID="pnlPrint" runat="server">
            <style type="text/css">
                .address
                {
                    font-family: Calibri;
                    font-size: 18px;
                    font-weight: bold;
                    border-bottom: solid 2px;
                    text-align: center;
                }
                .Date
                {
                    font-family: Calibri;
                    border-bottom: solid 2px black;
                }
                .details
                {
                    font-family: Calibri;
                    font-size: 15px;
                    border-bottom: solid 2px;
                    text-align: left;
                }
                .col1
                {
                    text-align: left;
                    width: 30%;
                }
                .col2
                {
                    text-align: center;
                    width: 70%;
                    font-weight: bold;
                    margin-left: 100px;
                }
                .maintable
                {
                    border: solid 2px;
                    border-radius: 10px 10px 10px 10px;
                    padding: 4px 4px 4px 4px;
                    font-family: Calibri;
                }
                .reportname
                {
                    text-align: center;
                    border-bottom: solid thin;
                    font-weight: bold;
                    font-family: Calibri;
                }
                .label
                {
                    border-bottom: solid thin;
                }
            </style>
            <table width="100%" cellpadding="0" cellspacing="0" align="center" class="maintable">
                <tr  style="display:none;">
                    <td style="float: left;">
                        IPD No.:
                        <asp:Label ID="lblIpdNo" runat="server"></asp:Label>
                    </td>
                    <td style="float: right; display:none;">
                        Tel.:&nbsp; 020-27412197, 020-27412549
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; color: Red; font-size: 25px; width:70%;">
   <div style="float: left; width: 100px; margin-top: 0px;position: relative;left: 10px;">
       <img src="images/Aditya_logo_final.png" height="60px" />
                            </div>
                         
                        <div style="font-size: 16px;text-align: left;margin-top: 0px;">
                          <table border="0" cellpadding="0" style="position: relative;left: 10px;top:3px;">
                              <tbody>
                                <tr>
                                  <td><font style="font-size: 40px; color: black;position: relative; top:-4px;"> ADITYA </font></td>
                                  <td style="position: relative; left: 0px;font-size: 15px; color: Red;"><%--Homoeopathic Hospital
                                  <br />
                                  &amp; Healing Centre--%>

                                  <table width="100%">
                                  <tr>
                                  <td><font style="font-size: 15px; color: black;">
                                  Homoeopathic Hospital </font>
                                  </td>
                                  </tr>

                                  <tr>
                                  <td>
                               <font style="font-size: 15px; color: black;position:relative; top: -7px;">  & Healing Centre  </font>
                                  </td>
                                  </tr>

                                  </table>

                                  </td>
                                </tr>
                            <%--  <tr>
                                <td style="position: relative;top: -8px;left: 0px;font-size: 15px;  color: Red;">Homoeopathic Hospital <br />
                                Healing Centre
                                </td>
                                <td>
                                </td>
                                <td style="position: relative;top: -25px;left: 7px;font-size: 15px;  color: Red;"> &amp; Healing Centre</td>
                              </tr>--%>
                            </tbody>
                         </table>
                        </div>

                           <br />
                           <div style="font-size: 16px; color: black; float: left; width: 220px;">
                                   <strong>DR. AMARSINHA D. NIKAM</strong><br />
                                    M.D.(Homoeopath)
                           </div>
                    </td>

                    <td>
                    <div style="float: right;position:relative; right:20px;">
                  <table>
                 <tr>
                     <td style="text-align: justify;">
                                    <div style="float: left; ">
                                        <strong>Add. :</strong></div>
                                    <div style="float: left; margin-left: 5px;">
                                        Old Kate Pimple Road,
                                        Near Shivaji Chawk,<br />
                                        Pimprigaon, <br />
                                        Pune – 411017<br />
                                        Tel : 020-27412197, 8806061061 <br />
                                        <span style="color: Blue;"> www.drnikam.com </span> <br />
                                        dr.amar@ahhhc.com <br />
                                        </div>
                               </td>           
                 </tr>
                  </table>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table cellpadding="0" cellspacing="0" align="center" width="100%">
                            <tr style="display:none;">
                                <td style="text-align: left; padding-left: 10px;" colspan="4">
                                    <strong>DR. AMARSINHA D. NIKAM</strong><br />
                                    M.D.(Homoeopath)
                                </td>
                           </tr>
                             <tr>
                                <td colspan="4">
                                  <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                 Case Paper No. :  <asp:Label ID="lblCasePaperNo" runat="server"></asp:Label>
                                  <%-- <b>  </b>--%>
                                </td>
                                <td style="width: 25%;" align="left">
                                Date : <asp:Label ID="lblDate" runat="server"></asp:Label>
                               <%--    <b>   </b>--%>
                                </td>
                                <td style="width: 25%;" align="left">
                                 Remedy :
                                  <b>   <asp:Label ID="lblRemedy" runat="server"></asp:Label> </b>
                                  
                                </td>
                                <td style="width: 25%;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%;" align="left">
                                  Patient's Name :
                                </td>
                                <td style="width: 25%;" align="left">
                                 <b> <u><asp:Label ID="lblFName" runat="server"></asp:Label></u> </b>
                                  <%--<br />  ________________  --%>
                                  <%-- <hr /> --%>
                                  <br />
                                   (First Name) 
                                </td>
                                <td style="width: 25%;" align="left">
                                 <b><u><asp:Label ID="lblMName" runat="server"></asp:Label></u></b>
                                 <%-- <br />  ________________ --%>
                                  <%--<hr />--%>
                                  <br />
                                  (Middle Name)
                                </td>
                                <td style="width: 25%;" align="left">
                                 <b> <u><asp:Label ID="lblLName" runat="server"></asp:Label></u></b>
                                   <%--<br />  --%>
                                    <%--<hr />--%>
                                 <%-- ________________--%>
                                    <br />
                                  (Last Name) 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                 <br />   <%-- <hr />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%;" align="left">
                                 Age : <asp:Label ID="lblAge" runat="server"></asp:Label>  &nbsp; Year 
                               <%--  <b>    </b>--%>
                                   
                                </td>
                                <td style="width: 25%;" align="left">
                                 Sex :  <asp:Label ID="lblSex" runat="server"></asp:Label>
                              <%--    <b>   </b>--%>
                                   
                                </td>
                                <td style="width: 25%;" align="left">
                                Height :   <asp:Label ID="lblHeight" runat="server"></asp:Label>
                                 <%--  <b>   </b>--%>
                                  
                                </td>
                                <td style="width: 25%;" align="left">
                                  Weight :  <asp:Label ID="lblWt" runat="server"></asp:Label>
                               <%-- <b>   </b>--%>
                                 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                 <br />   <%-- <hr />--%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="left">
                                Full Address :  <asp:Label ID="lblAddress" runat="server"></asp:Label> 
                                <%--   <b> </b>--%>
                                    
                                </td>
                                 <td style="width: 25%;" align="left">
                                  City : <asp:Label ID="lblCity" runat="server"></asp:Label>
                                 <%-- <b>   </b>--%>
                                  
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                <br />   <%--  <hr />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%;" align="left">
                                 State : <asp:Label ID="lblState" runat="server"></asp:Label> 
                              <%--  <b>    </b>--%>
                                  
                                </td>
                                <td style="width: 25%;" align="left">
                                Pin : <asp:Label ID="lblPin" runat="server"></asp:Label> 
                              <%--  <b>    </b>--%>
                                 
                                </td>
                                <td style="width: 50%;" align="left" colspan="2">
                                  Mobile No. / Phone No. :
                                   <asp:Label ID="lblMobileNo" runat="server"></asp:Label>  &nbsp; 
                                    /  &nbsp;
                                      <asp:Label ID="lblPhoneNo" runat="server"></asp:Label>  
                                    
                                </td>
                              
                            </tr>
                            <tr>
                                <td colspan="4">
                                   <br />  <%--<hr />--%>
                                </td>
                            </tr>
                           
                            <tr>
                                <td colspan="4" align="left">
                                 Provisional Diagnosis : 
                                  <b>   <asp:Label ID="lblDiagnosis" Visible="true" runat="server"></asp:Label> </b>
                                  
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                <%-- <br /> --%>   <hr />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="left">
                                 Final Diagnosis :  <asp:Label ID="lblFinalDiag" runat="server"></asp:Label> 
                                <%-- <b>  </b>--%>
                                
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                 <%--<br />   --%> <hr />
                                </td>
                            </tr>

                              <tr  style="outline: thin solid">
                                <td colspan="4" style="text-align:left;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="border-right: solid 1px;" width="359" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td colspan="2" height="20"><span class="style10">Ward Type : <asp:Label ID="lblWtype" runat="server"></asp:Label></span></td>
      </tr>
      <tr>
        <td colspan="2"  height="20"><span class="style10">Ward No :  <b> <asp:Label ID="lblwNo" runat="server"></asp:Label></b> </span></td>
      </tr>
      <tr>
        <td colspan="2" height="20"><span class="style10">Bed No : <asp:Label ID="lblBedNo" runat="server"></asp:Label></span></td>
      </tr>
       <tr>
           <td style="border-top: solid 1px;" colspan="2">
                           
                                </td>
                            </tr>

      <tr>
        <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td width="20%" height="25"><span class="style19">Admission</span></td>
            <td width="41%"><span class="style19">Date :-  <asp:Label ID="lblAdmissionDate" runat="server"></asp:Label> </span></td>
            <td width="39%"><span class="style19">Time :-   <asp:Label ID="lblAdmissionTime" runat="server"></asp:Label></span></td>
          </tr>
          <tr>
            <td height="25"><span class="style19">Discharge</span></td>
            <td><span class="style19">Date :-</span></td>
            <td><span class="style19">Time :-</span></td>
          </tr>
        </table></td>
        </tr>
    </table></td>
    <td width="421" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" valign="middle"><span class="style12" style="color: Red; font-size: 18px;">Investigation</span>
        </td>
       
      </tr>
      <tr>
                                <td>
                                    <hr />
                                </td>
                            </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td width="8%" align="left" valign="middle"><label>
            <%--  <input type="checkbox" name="checkbox" id="checkbox" />--%>
            <div style="height: 8px; width: 8px; border-bottom: 2px solid #666; border-left: 2px solid #666;
                                                                border-right: 2px solid #666; border-top: 2px solid #666;">
                                                            </div>
            </label></td>
            <td width="41%" align="left" valign="middle"><span class="style16">E.C.G</span></td>
            <td width="9%" align="left" valign="middle">
          <%--  <input name="checkbox4" type="checkbox" id="checkbox4" style="border-bottom-style:solid ; border-left-style: solid ;border-right-style: solid ; border-top-style: solid;" />--%>
              <div style="height: 8px; width: 8px; border-bottom: 2px solid #666; border-left: 2px solid #666;
                                                                border-right: 2px solid #666; border-top: 2px solid #666;">
                                                            </div>
            </td>
            <td width="42%" align="left" valign="middle"><span class="style16">Dietitian</span></td>
          </tr>
          <tr>
            <td align="left" valign="middle">
          <%--  <input type="checkbox" name="checkbox2" id="checkbox2" />--%>
          <div style="height: 8px; width: 8px; border-bottom: 2px solid #666; border-left: 2px solid #666;
                                                                border-right: 2px solid #666; border-top: 2px solid #666;">
                                                            </div>
            </td>
            <td align="left" valign="middle"><span class="style16">P.F.T</span></td>
            <td align="left" valign="middle">
         <%--   <input name="checkbox5" type="checkbox" id="checkbox5" />--%>
            <div style="height: 8px; width: 8px; border-bottom: 2px solid #666; border-left: 2px solid #666;
                                                                border-right: 2px solid #666; border-top: 2px solid #666;">
                                                            </div>
            </td>
            <td align="left" valign="middle"><span class="style16">Glucometer</span></td>
          </tr>
          <tr>
            <td align="left" valign="middle">
           <%-- <input type="checkbox" name="checkbox3" id="checkbox3" />--%>
            <div style="height: 8px; width: 8px; border-bottom: 2px solid #666; border-left: 2px solid #666;
                                                                border-right: 2px solid #666; border-top: 2px solid #666;">
                                                            </div>
            </td>
            <td align="left" valign="middle"><span class="style16">Reconsulting</span></td>
            <td align="left" valign="middle"><span class="style11"></span></td>
            <td align="left" valign="middle"><span class="style11"></span></td>
          </tr>
        </table></td>
	<tr>
		<td><hr /></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="50%"><strong>Shifting:</strong> Ward:</td>
				<td>Date:</td>
			</tr>
			</table>
		</td>
	</tr>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td style="border-right: solid 1px;" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td style="border-top: solid 1px;" width="21%" align="left" valign="top"><span class="style16">Deposit:-</span></td>
        <td style="border-top: solid 1px;" width="34%" align="left" valign="top"><span class="style16">1)    &nbsp; <asp:Label ID="lblDepositAmt" runat="server"></asp:Label></span></td>
        <td style="border-top: solid 1px;" width="45%" align="left" valign="top"><span class="style16">Date: &nbsp; <asp:Label ID="lblDepDt" runat="server"></asp:Label></span></td>
      </tr>
      <tr>
        <td align="left" valign="top"><span class="style11"></span></td>
        <td align="left" valign="top"><span class="style16">2)</span></td>
        <td align="left" valign="top"><span class="style16">Date: </span></td>
      </tr>
      <tr>
        <td align="left" valign="top"><span class="style11"></span></td>
        <td align="left" valign="top"><span class="style16">3)</span></td>
        <td align="left" valign="top"><span class="style16">Date: </span></td>
      </tr>
    </table></td>
    <td align="left" valign="top">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
    <td width="30%" align="left" valign="top">
    <table width="100%" border="1" cellspacing="0" cellpadding="5">
    <tr>
    <td>
    Cartography
    </td>
    </tr>

     <tr>
    <td>
    1.
    </td>
    </tr>

     <tr>
    <td>
    2.
    </td>
    </tr>

    </table>
    </td>

    <td  width="70%" align="left" valign="top">
    <table width="100%" border="1" cellspacing="0" cellpadding="5">
    <tr>
    <td width="20%" align="left" valign="top">
    Monitoring
    </td>

    <td>
    	Date
    </td>

    <td>
    Time
    </td>
   </tr>

   <tr>
    <td>
    Start   
    </td>

    <td>
    
    </td>

    <td>
   
    </td>
   </tr>

   <tr>
    <td>
    Remove
    </td>

    <td>
    	
    </td>

    <td>
   
    </td>
   </tr>

    </table>
    </td>
    </tr>
    </table>
    
    </td>
  </tr>
</table>
                                  </td>
                            </tr>

                             <%-- <tr colspan="4">
                                <td>
                                    &nbsp;
                                </td>
                            </tr>--%>

                            <tr>
                                <td colspan="4" align="center">
                                   <font style="font-size:16px;">  <strong> <u> Patient's / Relative's Consent (पेशंट / नातेवाईक संमती पत्र) </u> </strong> </font> 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="left">
                                    <%--  Permission is hereby given for the performance of any diagnostic examination, biopsy,
                                    transfusion or operation and for the administration of any kind of anaesthesia as
                                    may be demand advisable in the course of this hospital admission.--%>

                                    <font style="font-size:12px; font-weight:bold;">The possible risks and benefits associated with the treatment have been explained to me. However, I understand there is no full assurance anyone can give in terms of health issues. I am assured for the best possible treatment but is well informed regarding risks and possible undesirable consequences associated with the disease patient is suffering from.
I am also well informed about the requirement to be shifted to secondary or tertiary care unit if any deterioration in health especially in case of progressive disease or critical condition. I also agree to all investigations to be performed as per the guidance of Dr. Nikam Sir. I have been explained above CONSENT very well in my own communicative language orally also, I have fully understood it and I am signing the Consent after the same ….
</font>
<br />
  <font style="font-size:11px;font-weight:bold;">
आम्ही आमच्या जबाबदारी वरती पेशंटला एडमिट करत आहोत त्या दरम्यान लागणाऱ्या वैद्यकीय तपासण्या/चाचण्या यासाठी आमची सहमती आहे. आम्हाला पेशन्टच्या गंभीर आजाराची पूर्णतः कल्पना दिलेली आहे. व मोठया हॉस्पिटल मध्ये स्थलांतर करण्याची गरज लागू शकते याची पूर्ण सूचना आहे. पेशंटला एडमिट असताना कोणताही त्रास उद्भवल्यास येथील डॉ. व स्टाफ बद्दल आमची कोणतीही तक्रार असणार नाही.</font>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                           
                                <tr>
                                <td colspan="4" style="text-align:left;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="5">

                                <tr>
                                <td>
                                Name : ______________________________
                                <br />
                                 नाव
                                </td>

                                <td>
                                Relation : ______________________________
                                <br />
                                नाते
                                </td>

                                  <td>
                                Signature : ______________________________
                                <br />
                                सही

                                </td>

                                </tr>

                                </table>
                                  </td>
                                </tr>


                        </table>
                    </td>
                </tr>              
            </table>
        </asp:Panel>
    </div><br /><br />
	<div style="text-align: center;">
        <table width="60%" align="center">
            <tr>
                <td align="right">
                    <img src="images/print.png" style="cursor: pointer;" onclick="PrintDiv1()" />
                </td>
            </tr>
        </table>
    	</div>
	<div style="text-align: center">
        <asp:Panel ID="pnlPrint1" runat="server">
	<table width="100%" cellpadding="0" cellspacing="0" align="center" class="maintable">
		<tr>
			<td> <br /></td>
		</tr>
		<tr>
			<td> <br /></td>
		</tr>
		<tr>
			<td align="left">
				<div style="font-size: 20px; color: black; float: left; width: 220px;">
                                   <strong><u>Chief Complaints</u></strong>:- 
                           	</div><br />
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<br /><br /><hr />
			</td>
		<tr>
		    <td colspan="3">
                <br />
                <hr />
            </td>
		</tr>
			<tr>
                <td colspan="3">
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <hr />
                    <td></td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <hr />
                    <td></td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <hr />
                    <td></td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <hr />
                    <td></td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <hr />
                    <td></td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <hr />
                    <td></td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <hr />
                    <br />
                    <br />
                    <td></td>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <div style="font-size: 20px; color: black; float: left; width: 220px;">
                        On Admission:-
                        <br />
                    </div>
                </td>
                <td>
                    <div style="font-size: 20px; color: blank; float: left; width: 220px;">
                        Date:-
                        <br />
                    </div>
                </td>
                <td>
                    <div style="font-size: 20px; color: black; float: left; width: 220px;">
                        Time:-
                        <br />
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <div style="font-size: 20px; color: black; float: left; width: 220px;">
                        <br />
                        Weight:-
                        <br />
                    </div>
                </td>
                <td>
                    <div style="font-size: 20px; color: black; float: left; width: 220px;">
                        <br />
                        BP:-
                        <br />
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <div style="font-size: 20px; color: black; float: left; width: 220px;">
                        <br />
                        Temperature:-
                        <br />
                    </div>
                </td>
                <td>
                    <div style="font-size: 20px; color: black; float: left; width: 220px;">
                        Pulse:-
                        <br />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <div style="font-size: 20px; color: black; float: left; width: 220px">
                        Checked By:-
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <div style="font-size: 20px; color: black; float: left; width: 220px">
                        <br />
                        Dr.______________________
                        <br />
                        <br />
                    </div>
                </td>
                <td>
                    <div style="font-size: 20px; color: black; float: left; width: 220px">
                        Sister._______________________
                        <br />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <div style="font-size: 20px; color: black; float: left; width: 100%">
                        <strong><u>Prepared By</u></strong>:-
                        <asp:Label ID="lblPreparedBy" runat="server"></asp:Label>
                        <br />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
		</tr>
	</table>
	</asp:Panel>
	</div>
    </form>
</center>
</body>
</html>
