﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class SearchContent : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "kco_*" };
        var directoryArg = Server.MapPath("Files/KCO/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "Hemiplagia";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            int startindex = fls.IndexOf('_') + 1;
            int length = fls.Length - fls.IndexOf('_') - 1;
            lblFiles.Text += fls.Substring(startindex, length) + "<br />";
        }
    
    }


    static IEnumerable<string> FindInFiles(string directoryArg,string containsTextArg,bool ignoreCaseArg,System.IO.SearchOption searchSubDirsArg,params string[] fileWildcardsArg )
    {
        List<String> files = new List<string>(); // This List accumulates files found.
 
        foreach (string fileWildcard in fileWildcardsArg )
        {
            string[] xFiles = System.IO.Directory.GetFiles(directoryArg, fileWildcard, searchSubDirsArg);
     
            foreach ( string x in xFiles )
            {
                if ( !files.Contains(x) ) // If file not already found...
                {
                    // See if the file contains the search text.
                    // Assume a null search string matches any file.
                    // Use ToLower to perform a case-insensitive search.
                    bool containsText = 
                        containsTextArg.Length==0 ||
                        ignoreCaseArg ? 
                        System.IO.File.ReadAllText(x).ToLower().Contains(containsTextArg.ToLower()) : 
                        System.IO.File.ReadAllText(x).Contains(containsTextArg);
     
                    if ( containsText )
                    {
                        files.Add(x); // This file is a keeper. Add it to the list.
                    } // if
                } // if
            } // foreach file
        }//foreach wildcard 
       return files;
    }


    static IEnumerable<string> FindInFiles1(
    string directoryArg,
    string containsTextArg,
    bool ignoreCaseArg,
    System.IO.SearchOption searchSubDirsArg,
    params string[] fileWildcardsArg)
    {

        IEnumerable<string> files =
            from fileWildcard in fileWildcardsArg
            from file in System.IO.Directory.GetFiles(directoryArg, fileWildcard, searchSubDirsArg)
            where
                containsTextArg.Length == 0 ||
                ignoreCaseArg ?
                System.IO.File.ReadAllText(file).ToLower().Contains(containsTextArg.ToLower()) :
                System.IO.File.ReadAllText(file).Contains(containsTextArg)
            select file;

        return files;
    }

}
