﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PatientcountReports : System.Web.UI.Page
{
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    private void BindGrid()
    {
        if (txtfrm.Text != "")
        {
            string[] frmdate = txtfrm.Text.ToString().Split('/');

            string fmdate = frmdate[2] + "-" + frmdate[1] + "-" + frmdate[0];

            string[] date = txttodate.Text.ToString().Split('/');

            string todate = date[2] + "-" + date[1] + "-" + date[0];


            strSQL = "Proc_GetPatientcountReports";
            SqlCommand SqlCmd = new SqlCommand(strSQL, con);
            SqlDataReader dr;
            SqlCmd.CommandType = CommandType.StoredProcedure;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
                SqlCmd.Parameters.AddWithValue("@Fromdate", fmdate);
                SqlCmd.Parameters.AddWithValue("@Todate", todate);
                dr = SqlCmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                if (dt.Rows.Count > 0)
                {
                    GRV.DataSource = dt;
                    GRV.DataBind();
                }
                else
                {
                    GRV.DataSource = null;
                    GRV.DataBind();
                }
                con.Close();
                dr.Dispose();
            }
        }
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGrid();
    }
    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "PatientcountReports_'" + DateTime.Now.ToString("dd-MM-yyyy") + "'.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRV.AllowPaging = false;
        this.BindGrid();
        GRV.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    protected void GRV_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
}