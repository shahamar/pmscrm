﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class FollowupDetail : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "", msg, StrSQL;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    //string pdId = "0";
    string uid = "0";
    string fdId = "0";
    string Stat = "0";
    string Source = "MC";
    int cnt = 0;
    string OLID = "0";
    int multifdid = 0;
    //added by nikita on 12th march 2015------------------------------
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-----------------------------------------------------------------
    private void LoadMenu()
    {
        SqlCommand cmd = new SqlCommand("Proc_GetFolloupAccess", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@RoleId", Session["urole"].ToString());
        cmd.Parameters.AddWithValue("@MenuType", 2);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            //Response.Redirect("Error.aspx");
        }
        else
        {
            Response.Redirect("Error.aspx");
        }
        try
        {
            //connection.Open();
            //SqlDataReader reader = selectCommand.ExecuteReader();
            //if (reader.HasRows)
            //{
            //    dt.Load(reader);
            //}
            //reader.Close();
        }
        catch (SqlException)
        {
            throw;
        }
        finally
        {
            con.Close();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        LoadMenu();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["fdId"] != null)
            fdId = Request.QueryString["fdId"].ToString();

        if (Request.QueryString["multifdid"] != null)
            multifdid = Convert.ToInt32(Request.QueryString["multifdid"]);


        if (Request.QueryString["OLID"] != null)
            OLID = Request.QueryString["OLID"].ToString();

        if (Request.QueryString["Stat"] != null)
            Stat = Request.QueryString["Stat"].ToString();

        if (Request.QueryString["SC"] != null)
            Source = Request.QueryString["SC"].ToString();


        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 29";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------

        LoadMenu();

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            //---Added By Nikita On 30thMarch 2015----------------------------------------------------
            //if (msg == "1")
            //{
            //    btnSave.Enabled = false;
            //}
            //else
            //{
            //    btnSave.Enabled = true;
            //}
            //-----------------------------------------------------------------------------------------

            txtDOC.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtNfollow.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtCourierDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtCOnCharges.Text = "0";
            //if (fdId != "0")
            //{
            //    ddDoseType.SelectedValue = "";
            //}
            //{
            //    ddDoseType.SelectedValue = "3";
            //}
            FillData();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Calc", "CalculateRemedyCharges()", true);

        }
    }

    public void FillData()
    {

        //DDCourier.Items.Insert(0, new ListItem("Select", ""));
        //gen.FillDropDownList("Select cmID,cmCourierName From tblCourierMaster", "cmID", "cmCourierName", DDCourier);

        ddRemedyPeriod.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select CAST(Rem_Per_ID  As Varchar(10)) + ' - '+ CAST(Rem_Per_InDays  As Varchar(10)) As REMID ,  Rem_Per_Description As REMDESC from tblRemedyPeriodMaster Where Rem_Per_IsDelete = 0 order by Rem_Per_InDays", "REMID", "REMDESC", ddRemedyPeriod);

        DDL_RemList.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select RMID , upper(RMNAME) As RMNAME  from tblRemedyMaster Where IsDelete = 0 and RMNAME != '0' order by replace(RMNAME,' ','') asc ", "RMID", "RMNAME", DDL_RemList);


        if (fdId != "0")
        {
            id1.Visible = true;
            id2.Visible = true;
            strSQL = "Select FD.fdDate ,fdIsSeminar ,FD.fdRemedy  , FD.fdDoseType , FD.fdCharges , FD.fdConsultingCharges , FD.fdNextFollowupDate , FD.fdRemark , FD.fdCasseteNo , FD.fdReconsulting , FD.fdIsReconsulting  , CAST(Rem_Per_ID  As Varchar(10)) + ' - '+ CAST(Rem_Per_InDays  As Varchar(10)) As fdRemedyPeriod , FD.PatObservation ,FD.Payment_Mode , FD.Bank_Name , FD.Bank_Branch , FD.Cheque_No , FD.Cheque_Date , IsNull(fdAddEmgAmt,0) As fdAddEmgAmt , IsNull(ExtraDosAmt,0) As ExtraDosAmt , IsNull(fdAddEmgAmt,0) + IsNull(ExtraDosAmt,0) + IsNull(Case When fdIsReconsulting = 0 Then IsNull(fdCharges,0) when fdIsReconsulting = 1 Then IsNull(fdConsultingCharges,0) else 0 end,0) As fdTotalAmt,[dbo].[GetPreviousBalanceAks] (fdPatientId,fdId) As PreviousBalance  From tblFollowUpDatails FD Left Join tblRemedyPeriodMaster RM On FD.fdRemedyPeriod = RM.Rem_Per_ID Where FD.fdId=" + fdId;
        }
        else
        {
            // strSQL = "Select top 1 FD.fdDate , FD.fdRemedy  , FD.fdDoseType , FD.fdCharges , FD.fdConsultingCharges , FD.fdNextFollowupDate , FD.fdRemark , FD.fdCasseteNo , FD.fdReconsulting , FD.fdIsReconsulting  , CAST(Rem_Per_ID  As Varchar(10)) + ' - '+ CAST(Rem_Per_InDays  As Varchar(10)) As fdRemedyPeriod , FD.PatObservation ,FD.Payment_Mode , FD.Bank_Name , FD.Bank_Branch , FD.Cheque_No , FD.Cheque_Date , IsNull(fdAddEmgAmt,0) As fdAddEmgAmt , IsNull(ExtraDosAmt,0) As ExtraDosAmt , 0 As fdTotalAmt,[dbo].[GetPreviousBalanceAks] (fdPatientId,fdId) As PreviousBalance  From tblFollowUpDatails FD Left Join tblRemedyPeriodMaster RM On FD.fdRemedyPeriod = RM.Rem_Per_ID Where FD.fdPatientId = " + pdId + " Order By FD.fdId Desc ";
            strSQL = "Exec Proc_GetLatestRemedyDetails " + pdId;
        }

        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();


        if (fdId != "0")
        {
            while (Sqldr.Read())
            {
                //if (Sqldr["fdDate"].ToString() == "NULL")
                //{
                //    txtDOC.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["fdDate"].ToString()));
                //}

                if (Sqldr["fdDate"].ToString() != "NULL" || Sqldr["fdDate"].ToString() != "")
                {
                    txtDOC.Text = Sqldr["fdDate"] != DBNull.Value ? string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["fdDate"].ToString())) : "";
                }
                if (Sqldr["fdIsSeminar"].ToString() == "1")
                {
                    IsSeminar.Checked = true;
                }
                else
                {
                    IsSeminar.Checked = false;
                }

                DDL_RemList.SelectedValue = Sqldr["fdRemedy"].ToString();
                ddRemedyPeriod.SelectedValue = Sqldr["fdRemedyPeriod"].ToString();
                ddDoseType.SelectedValue = Sqldr["fdDoseType"].ToString();
                txtCharges.Text = Sqldr["fdCharges"].ToString();
                txt_TotalCharge.Text = Sqldr["fdTotalAmt"].ToString();
                txtCOnCharges.Text = Sqldr["fdConsultingCharges"].ToString();
                //if (Sqldr["fdNextFollowupDate"] != DBNull.Value)
                //{
                //    txtNfollow.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["fdNextFollowupDate"].ToString()));
                //}

                if (Sqldr["fdNextFollowupDate"].ToString() != "NULL" || Sqldr["fdNextFollowupDate"].ToString() != "")
                {
                    txtNfollow.Text = Sqldr["fdNextFollowupDate"] != DBNull.Value ? string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["fdNextFollowupDate"].ToString())) : "";
                }

                txtRemark.Text = Sqldr["fdRemark"].ToString();
                txtFPCassetteNo.Text = Sqldr["fdCasseteNo"].ToString();
                txtReconsulting.Text = Sqldr["fdReconsulting"].ToString();
                //txtCName.Text = Sqldr["pdFname"].ToString() + Sqldr["fdReconsulting"].ToString();
                DDL_PatObservation.SelectedValue = Sqldr["PatObservation"].ToString();
                if (Sqldr["fdIsReconsulting"].ToString() == "True")
                    chkIsReconsulting.Checked = true;
                else
                    chkIsReconsulting.Checked = false;

                if (Sqldr["fdIsSeminar"].ToString() == "True")
                    IsSeminar.Checked = true;
                else
                    IsSeminar.Checked = false;

                if (Sqldr["Payment_Mode"] != DBNull.Value || Sqldr["Payment_Mode"].ToString() != "")
                {
                    DDL_Pay_Mode.SelectedValue = Sqldr["Payment_Mode"].ToString();
                }
                txtCardnm.Text = Sqldr["Bank_Name"].ToString();
                DDL_CardType.SelectedValue = Sqldr["Bank_Branch"].ToString();
                txtCardno.Text = Sqldr["Cheque_No"].ToString();
                if (Sqldr["Cheque_Date"].ToString() != "NULL" || Sqldr["Cheque_Date"].ToString() != "")
                {
                    txtChqdt.Text = Sqldr["Cheque_Date"] != DBNull.Value ? string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["Cheque_Date"].ToString())) : "";
                }

                DDL_AddEmg.SelectedValue = Sqldr["fdAddEmgAmt"].ToString();
                DDL_ExtranDos.SelectedValue = Sqldr["ExtraDosAmt"].ToString();

                //if (DDL_Pay_Mode.SelectedIndex == 2)
                //{
                //    tr_Bank.Style.Add("display", "table-row");
                //    tr_Cheque.Style.Add("display", "table-row");
                //    tr_Bank1.Style.Add("display", "table-row");
                //    tr_Cheque1.Style.Add("display", "table-row");
                //}
                //else
                //{
                //    tr_Bank.Style.Add("display", "none");
                //    tr_Cheque.Style.Add("display", "none");
                //    tr_Bank1.Style.Add("display", "none");
                //    tr_Cheque1.Style.Add("display", "none");
                //}
                //txt_PreviousBal.Text = Sqldr["PreviousBalance"].ToString(); 
                txt_PreviousBal.Text = "0.00";
            }
        }
        else
        {
            while (Sqldr.Read())
            {
                DDL_RemList.SelectedValue = Sqldr["fdRemedy"].ToString();
                ddRemedyPeriod.SelectedValue = Sqldr["fdRemedyPeriod"].ToString();
                ddDoseType.SelectedValue = Sqldr["fdDoseType"].ToString();
                DDL_PatObservation.SelectedValue = Sqldr["PatObservation"].ToString();
                txtCharges.Text = Sqldr["fdCharges"].ToString();
                txt_TotalCharge.Text = Sqldr["fdTotalAmt"].ToString();
                DDL_AddEmg.SelectedValue = Sqldr["fdAddEmgAmt"].ToString();
                DDL_ExtranDos.SelectedValue = Sqldr["ExtraDosAmt"].ToString();
                //txt_PreviousBal.Text = Sqldr["PreviousBalance"].ToString();
                txt_PreviousBal.Text = "0.00";
                txtRemark.Text = Sqldr["fdRemark"].ToString();

                if (Sqldr["fdIsSeminar"].ToString() == "True")
                {
                    IsSeminar.Checked = true;
                }
                else
                {
                    IsSeminar.Checked = false;
                }
            }
        }

        if (File.Exists(Server.MapPath("Files/KCO/kco_" + pdId)))
        {
            lblkco.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + pdId).ToString());
            divkco.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/Investigation/inv_" + pdId)))
        {
            lblInv.Text = File.ReadAllText(Server.MapPath("Files/Investigation/inv_" + pdId).ToString());
            divinv.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/ChiefCo/chief_" + pdId)))
        {
            lblChiefCo.Text = File.ReadAllText(Server.MapPath("Files/ChiefCo/chief_" + pdId).ToString());
            divchief.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/PastHo/pho_" + pdId)))
        {
            lblpho.Text = File.ReadAllText(Server.MapPath("Files/PastHo/pho_" + pdId).ToString());
            divpho.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/FamilyHo/fho_" + pdId)))
        {
            lblfho.Text = File.ReadAllText(Server.MapPath("Files/FamilyHo/fho_" + pdId).ToString());
            divfho.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/Mind/mnd_" + pdId)))
        {
            lblMind.Text = File.ReadAllText(Server.MapPath("Files/Mind/mnd_" + pdId).ToString());
            divmnd.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/AF/af_" + pdId)))
        {
            lblAF.Text = File.ReadAllText(Server.MapPath("Files/AF/af_" + pdId).ToString());
            divAF.Style.Add("display", "block");
        }

        if (File.Exists(Server.MapPath("Files/Thermal/thermal_" + pdId)))
        {
            lblThermal.Text = File.ReadAllText(Server.MapPath("Files/Thermal/thermal_" + pdId).ToString());
            divThermal.Style.Add("display", "block");
        }

        Sqldr.Close();
        SqlCmd.Dispose();
        FillGrid();
        FillIPDRoundGrid();

        if (Source == "ACR")
        {
            chkIsCourier.Checked = true;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Calc", "OnModeChangeOnline();", true);

            if (OLID != "0")
            {

                FillCourierAddress();
            }


        }

    }

    protected void FillGrid()
    {
        // strSQL = "Select  fdId , fdDate , fdReconsulting , fdCasseteNo , RMNAME As fdRemedy , Rem_Per_Description As fdRemedyPeriod , fdDoseType , Case fdFlag When 'F' Then 'block' ELSE 'none' END 'DEL' ,pdFname , pdMname , pdLname , pdAdd1 , pdAdd2 , pdAdd3 , pdAdd4 , pdMob , pdemail ";
        // strSQL += " from vwConsultingHistory c inner join dbo.tblPatientDetails p on p.pdID = c.fdPatientId Inner Join tblRemedyMaster RM On RM.RMID = c.fdRemedy Inner Join tblRemedyPeriodMaster RPM On RPM.Rem_Per_ID = c.fdRemedyPeriod Where fdPatientId= " + pdId + " Order By fdDate Desc ";

        strSQL = "Select * ,uFname +' '+uLname as Modified from vwConsultingHistoryNewAks_FollowupDetails_His s left join tblUser u on u.uId=s.ModifiedBy Where fdPatientId= " + pdId + " Order By fdDate Desc ";

        DataTable dt = gen.getDataTable(strSQL);


        if (dt.Rows.Count > 0)
        {
            GRV1.DataSource = dt;
            GRV1.DataBind();

            if (msg == "1")
                GRV1.Columns[6].Visible = false;
            else
            {
                if (fdId != "0")
                {
                    //GRV1.Columns[6].Visible = false;
                }
                else
                {
                    //GRV1.Columns[6].Visible = true;
                }
            }
            //txtCName.Text = dt.Rows[0]["pdFname"].ToString() + ' ' + dt.Rows[0]["pdMname"].ToString() + ' ' + dt.Rows[0]["pdLname"].ToString();
            //txtCAddress.Text = dt.Rows[0]["pdAdd1"].ToString() + ' ' + dt.Rows[0]["pdAdd2"].ToString() + ' ' + dt.Rows[0]["pdAdd3"].ToString() + ' ' + dt.Rows[0]["pdAdd4"].ToString();
            //txtCMobileNo.Text = dt.Rows[0]["pdMob"].ToString();
            //txtcEmailID.Text = dt.Rows[0]["pdemail"].ToString();
        }




    }

    protected void FillCourierAddress()
    {
        strSQL = "Select * from vwConsultingHistoryNewAks_OLT Where cmId= " + OLID + " ";

        DataTable dt = gen.getDataTable(strSQL);

        if (dt.Rows.Count > 0)
        {
            //txtCName.Text = dt.Rows[0]["pdName"].ToString();
            //txtCAddress.Text = dt.Rows[0]["PADD"].ToString();
            //txtCMobileNo.Text = dt.Rows[0]["pdMob"].ToString();
            //txtcEmailID.Text = dt.Rows[0]["pdemail"].ToString();
            //txtCCharges.Text = dt.Rows[0]["RegistrationFees"].ToString();
            //DDL_CourierPaymentMode.SelectedValue = "OnLine";
        }

    }

    protected void FillIPDRoundGrid()
    {
        strSQL = "Select RMNAME As irRemedy , irStartDate  From tblIpdRemedy IPR Left Join tblRemedyMaster RM On RM.RMID = IPR.irRemedy Where iripdId IN (Select ipdId From tblIpdMaster Where ipdpdId=" + pdId + ") Order By irStartDate Desc ";

        DataTable dt = gen.getDataTable(strSQL);


        if (dt.Rows.Count > 0)
        {
            GVD_IPDRound.DataSource = dt;
            GVD_IPDRound.DataBind();

        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //Page.Validate("check");
        //if (ddRemedyPeriod.SelectedItem.Text== "Select")
        //{
        //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SomestartupScript", " alert('Please Select Remedy Period.');", true);
        //}
        if (DDL_RemList.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SomestartupScript", " alert('Please Select Remedy.');", true);
        }
        else
        {
            if (Stat == "NO")
            {

                string msg = "Follow-Up Details Allready Saved.....!!!!!";
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                "alert",
                "alert('" + msg + "');window.location ='FollowupDetail.aspx?Stat=NO&fdId=" + fdId + "';",
                true);
                return;

            }

            string finyear = gen.GetCurrentFinYear();
            string cmdReceiptNo = "";
            string FollReceiptNo = "";
            string doc = "";
            string ndof = "";
            string CDate = "";
            object o = gen.executeScalar("Select CONVERT(VARCHAR,ISNull(MAX(CAST(SubString(cmdReceiptNo,1,len(cmdReceiptNo)-4) As INT)),0)+1)+'" + finyear + "' From dbo.tblCourierMedicinsDetails where cmdCreatedDate > '2016-04-01 00:00:00.001'");
            cmdReceiptNo = o.ToString();
            object p = gen.executeScalar("Select CONVERT(VARCHAR,ISNull(MAX(CAST(SubString(fdReceiptNo,1,len(fdReceiptNo)-4) As INT)),0)+1)+'" + finyear + "' From dbo.tblFollowUpDatails where fdCreatedDate > '2016-04-01 00:00:00.001'");
            FollReceiptNo = p.ToString();
            if (fdId == "0")

                strSQL = "sp_InsFollowupDetails";
            else
                strSQL = "sp_UpdtFollowupDetails";

            if (fdId != "0")
            {
                if (ddlreasonforedt.SelectedValue == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Enter Reason for edit')", true);
                    return;
                }
            }
            if (txtDOC.Text.Contains('-'))
            {
                string[] _doc = txtDOC.Text.ToString().Split('-');
                doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

                string[] _ndof = txtNfollow.Text.ToString().Split('-');
                ndof = _ndof[2] + "/" + _ndof[1] + "/" + _ndof[0];

                string[] _CDate = txtCourierDate.Text.ToString().Split('-');
                CDate = _CDate[2] + "/" + _CDate[1] + "/" + _CDate[0];
            }
            else
            {
                string[] _doc = txtDOC.Text.ToString().Split('/');
                doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

                string[] _ndof = txtNfollow.Text.ToString().Split('/');
                ndof = _ndof[2] + "/" + _ndof[1] + "/" + _ndof[0];

                string[] _CDate = txtCourierDate.Text.ToString().Split('/');
                CDate = _CDate[2] + "/" + _CDate[1] + "/" + _CDate[0];
            }


            int IsCourier = 0;
            int IsExtraDoses = 0;

            string val = "";
            string val1 = "";

            if (ddRemedyPeriod.SelectedValue != "")
            {
                string[] restr = ddRemedyPeriod.SelectedValue.Split('-');
                val = restr[0];
                val1 = restr[1];
            }

            if (chkIsCourier.Checked == true)
            {
                IsCourier = 1;
            }

            if (chk_ExtraDoses.Checked == true)
            {
                IsExtraDoses = 1;
            }

            string CChqdt;

            if (txtChqdt.Text != "")
            {
                if (txtChqdt.Text.Contains('/'))
                {
                    string[] _CheckDt = txtChqdt.Text.ToString().Split('/');
                    CChqdt = _CheckDt[2] + "/" + _CheckDt[1] + "/" + _CheckDt[0];
                }
                else
                {
                    string[] _CheckDt = txtChqdt.Text.ToString().Split('-');
                    CChqdt = _CheckDt[2] + "/" + _CheckDt[1] + "/" + _CheckDt[0];
                }

            }
            else
            {
                CChqdt = "";
            }
            int IsPatient = 0;
            if (chkIsPatient.Checked == true)
            {
                IsPatient = 1;
            }

            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@fdPatientId", pdId);
            SqlCmd.Parameters.AddWithValue("@fdDate", doc);
            SqlCmd.Parameters.AddWithValue("@fdRemedy", DDL_RemList.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@fdNextFollowupDate", ndof);
            SqlCmd.Parameters.AddWithValue("@fdRemark", txtRemark.Text);
            SqlCmd.Parameters.AddWithValue("@fdRemedyPeriod", val);
            if (chk_ExtraDoses.Checked == true)
            {
                SqlCmd.Parameters.AddWithValue("@fdCharges", txtCharges.Text);
            }
            else
            {
                SqlCmd.Parameters.AddWithValue("@fdCharges", txtCharges.Text);
            }

            SqlCmd.Parameters.AddWithValue("@fdCreatedBy", uid);
            SqlCmd.Parameters.AddWithValue("@fdDoseType", ddDoseType.SelectedValue.ToString());
            SqlCmd.Parameters.AddWithValue("@fdConsultingCharges", txtCOnCharges.Text);
            SqlCmd.Parameters.AddWithValue("@fdReconsulting", txtReconsulting.Text);
            SqlCmd.Parameters.AddWithValue("@fdCasseteNo", txtFPCassetteNo.Text);
            SqlCmd.Parameters.AddWithValue("@fdIsCourier", IsCourier);
            SqlCmd.Parameters.AddWithValue("@cmdName", txtCName.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@cmdAddress", txtCAddress.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@cmdMobileNo", txtCMobileNo.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@cmdEmailId", txtcEmailID.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@cmId", DDCourier.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@cmdDate", CDate);
            SqlCmd.Parameters.AddWithValue("@cmdDocketNo", txtCDocketNo.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@cmdReceiptNo", cmdReceiptNo);
            SqlCmd.Parameters.AddWithValue("@PatObservation", DDL_PatObservation.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@Bank_Name", txtCardnm.Text.ToString().ToUpper());
            SqlCmd.Parameters.AddWithValue("@Bank_Branch", DDL_CardType.SelectedValue.ToString());
            SqlCmd.Parameters.AddWithValue("@Cheque_No", txtCardno.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@Cheque_Date", CChqdt);
            SqlCmd.Parameters.AddWithValue("@IsPatient", IsPatient);

            if (txtCCharges.Text != "")
            {
                SqlCmd.Parameters.AddWithValue("@cmdCharges", txtCCharges.Text.ToString());
            }
            else
            {
                SqlCmd.Parameters.AddWithValue("@cmdCharges", 0);
            }

            if (chkIsReconsulting.Checked == true)
                SqlCmd.Parameters.AddWithValue("@fdIsReconsulting", 1);
            else
                SqlCmd.Parameters.AddWithValue("@fdIsReconsulting", 0);
            if (IsSeminar.Checked == true)
                SqlCmd.Parameters.AddWithValue("@fdIsSeminar", 1);
            else
                SqlCmd.Parameters.AddWithValue("@fdIsSeminar", 0);
            if (IsSeminar.Checked == true)
                SqlCmd.Parameters.AddWithValue("@multifdid", multifdid);
            else
                SqlCmd.Parameters.AddWithValue("@multifdid", 0);


            if (fdId != "0")
            {
                SqlCmd.Parameters.AddWithValue("@fdId", fdId);
                SqlCmd.Parameters.AddWithValue("@followReceiptNo", 0);
            }
            else
            {
                SqlCmd.Parameters.AddWithValue("@followReceiptNo", FollReceiptNo);
                SqlCmd.Parameters.Add("@fdId", SqlDbType.Int).Direction = ParameterDirection.Output;
            }

            SqlCmd.Parameters.AddWithValue("@CourierPaymentMode", DDL_CourierPaymentMode.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@fdAddEmgAmt", DDL_AddEmg.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@ExtraDosAmt", DDL_ExtranDos.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@Source", Source);
            SqlCmd.Parameters.AddWithValue("@OLID", OLID);
            SqlCmd.Parameters.AddWithValue("@IsExtraDoses", IsExtraDoses);
            SqlCmd.Parameters.AddWithValue("@Reasonforedit", ddlreasonforedt.SelectedValue);
            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;

            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            int reslt;
            reslt = (int)SqlCmd.Parameters["@Error"].Value;
            if (reslt == 0)
            {
                if (chkIsCourier.Checked == true)
                {
                    if (txtCMobileNo.Text.Trim() != "" && txtCDocketNo.Text.Trim() != "")
                    {
                        String smsstring;

                        SqlCmd = new SqlCommand();
                        decimal TemplateId = 1207160931676032697;
                        smsstring = "" + txtCName.Text.ToString() + " Your medicine has been dispatched from aditya homoeopathic hospital on " + txtCourierDate.Text.ToString() + " via " + DDCourier.SelectedItem.Text + " courier with tracking (CN) number " + txtCDocketNo.Text + ". For any queries contact non tariff number 33004444... visit us at www.drnikam.com";
                        genral.GetResponse(txtCMobileNo.Text.Trim(), smsstring, TemplateId);
                        //genral.GetResponse("9821479720", smsstring);

                        SqlCmd.CommandText = "sp_insSmsTransaction";
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Connection = con;
                        SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                        SqlCmd.Parameters.AddWithValue("@MobileNo", txtCMobileNo.Text.Trim());
                        SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                        SqlCmd.Parameters.AddWithValue("@SenderID", uid);
                        SqlCmd.Parameters.AddWithValue("@Pdid", pdId);
                        SqlCmd.Parameters.AddWithValue("@Sources", "Followup");
                        SqlCmd.ExecuteNonQuery();
                    }
                }
                //if (chkIsReconsulting.Checked == true)
                //{
                //    CrystalReportViewer2.Visible = true;
                //    string AksConn = ConfigurationManager.AppSettings["constring"];
                //    bindReport(AksConn, pdId);
                //}

                if (fdId == "0")
                {
                    //Response.Redirect("ProfarmaOfCase.aspx", false);
                    Response.Redirect("Home.aspx?MSG=Follow-up details saved successfully");
                }
                else
                {
                    //Response.Redirect("FollowupHistory.aspx", false);
                    Response.Redirect("Home.aspx?MSG=Follow-up details Modify successfully");
                }
            }

            SqlCmd.Dispose();
            con.Close();
        }
    }

    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            cnt = cnt + 1;
            if (cnt == 1)
            {

                //e.Row.BackColor = System.Drawing.Color.FromName("#FAB500");
            }
            string fdIsReconsulting = ((DataRowView)e.Row.DataItem)["fdIsReconsulting"].ToString();

            foreach (TableCell cell in e.Row.Cells)
            {
                if (fdIsReconsulting == "True")
                {
                    //cell.ForeColor = System.Drawing.Color.Red;
                    e.Row.BackColor = System.Drawing.Color.FromName("yellow");
                }
            }
        }
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "edt")
        {
            id1.Visible = true;
            id2.Visible = true;
            string arg = e.CommandArgument.ToString();
            string[] _arg = arg.Split('-');

            string fdid = _arg[0].ToString();
            string flag = _arg[1].ToString();

            if (flag == "C")
                Response.Redirect("ConsultPatient.aspx?fdId=" + fdid);
            else
                Response.Redirect("FollowupDetail.aspx?fdId=" + fdid);
        }

        if (e.CommandName == "del")
        {
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteFollowupDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@fdId", e.CommandArgument);
            SqlCmd.Parameters.AddWithValue("@deletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd.ExecuteNonQuery();
            con.Close();
            SqlCmd.Dispose();
            Response.Redirect("FollowupDetail.aspx");
            FillData();
        }
    }

    protected void ddRemedyPeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddRemedyPeriod.Items.Count == 0)
        {
            ddDoseType.SelectedValue = "3";

        }
        else
        {
            ddDoseType.SelectedValue = "3";
            DDL_AddEmg.SelectedValue = "0";
            DDL_ExtranDos.SelectedValue = "0";
            txt_PreviousBal.Text = "0.00";
            //txt_TotalCharge.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "CalculateRemedyCharges();", true);
        }
        if (ddRemedyPeriod.SelectedItem.Text == "IPD" || ddRemedyPeriod.SelectedItem.Text == "Select")
        {
            ddDoseType.SelectedValue = "0";
            DDL_AddEmg.SelectedValue = "0";
            DDL_ExtranDos.SelectedValue = "0";
            txt_PreviousBal.Text = "0.00";
            txt_TotalCharge.Text = "";
        }
        if (ddRemedyPeriod.SelectedItem.Text == "Extra Doses")
        {
            ddDoseType.SelectedValue = "6";
            DDL_AddEmg.SelectedValue = "0";
            DDL_ExtranDos.SelectedValue = "100";
            txt_PreviousBal.Text = "0.00";
            txt_TotalCharge.Text = "100";
        }
        if (ddRemedyPeriod.SelectedItem.Text == "Add Emg")
        {
            ddDoseType.SelectedValue = "4";
            DDL_AddEmg.SelectedValue = "100";
            DDL_ExtranDos.SelectedValue = "0";
            txt_PreviousBal.Text = "0.00";
            txt_TotalCharge.Text = "100";
        }

        //string[] restr = ddRemedyPeriod.SelectedValue.Split('-');
        //string val = restr[0];
        //string val1 = restr[1];

        //txtNfollow.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(Convert.ToInt16(val1)));


    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        FillGrid();
    }

    protected void GVD_IPDRound_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVD_IPDRound.PageIndex = e.NewPageIndex;
        FillIPDRoundGrid();
    }

    protected void DDL_Pay_Mode_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDL_Pay_Mode.SelectedIndex == 2)
        {
            tr_Bank.Style.Add("display", "table-row");
            tr_Cheque.Style.Add("display", "table-row");
            tr_Bank1.Style.Add("display", "table-row");
            tr_Cheque1.Style.Add("display", "table-row");
        }
        else
        {
            tr_Bank.Style.Add("display", "none");
            tr_Cheque.Style.Add("display", "none");
            tr_Bank1.Style.Add("display", "none");
            tr_Cheque1.Style.Add("display", "none");
        }
    }

    //public void bindReport(string AksConn, int pdid)
    //{
    //    DataTable dt = new DataTable();
    //    ReportDocument cryRpt = new ReportDocument();
    //    TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
    //    TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
    //    ConnectionInfo crConnectionInfo = new ConnectionInfo();
    //    CrystalDecisions.CrystalReports.Engine.Tables CrTables = default(CrystalDecisions.CrystalReports.Engine.Tables);
    //    System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

    //    builder.ConnectionString = AksConn;

    //    string AksServer = builder["Server"] as string;
    //    string AksDatabase = builder["Database"] as string;
    //    string AksUsername = builder["Uid"] as string;
    //    string AksPassword = builder["Pwd"] as string;


    //    dt = GetReport( int.Parse(fdId));

    //    cryRpt.Load(Server.MapPath("~/Reports/ReconsultingPrintReport.rpt"));

    //    CrTables = cryRpt.Database.Tables;
    //    foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
    //    {
    //        crtableLogoninfo = CrTable.LogOnInfo;
    //        crtableLogoninfo.ConnectionInfo = crConnectionInfo;
    //        CrTable.ApplyLogOnInfo(crtableLogoninfo);
    //    }

    //    cryRpt.SetDataSource(dt);
    //    cryRpt.Refresh();

    //    //cryRpt.SetParameterValue("FromDate", "");
    //    //cryRpt.SetParameterValue("ToDate", "");

    //    CrystalReportViewer2.ReportSourceID = "CrystalReportSource2";
    //    CrystalReportViewer2.ReportSource = cryRpt;

    //    System.IO.Stream oStream = null;
    //    byte[] byteArray = null;
    //    oStream = cryRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
    //    byteArray = new byte[oStream.Length];

    //    Session.Add("COMMPrintingData", byteArray);
    //    Session.Add("PrintingEventForm", "COMM_Print");

    //    oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
    //    Session["byteArray"] = byteArray;
    //    cryRpt.Close();
    //    cryRpt.Dispose();


    //}

    //public DataTable GetReport(int pdid)
    //{
    //    DataTable dt = new DataTable();

    //    StrSQL = "Proc_GetNewCaseNReconsultingData";
    //    SqlCmd = new SqlCommand(StrSQL, conn);
    //    SqlDataReader dr;
    //    SqlCmd.CommandType = CommandType.StoredProcedure;
    //    SqlCmd.Parameters.AddWithValue("@pdID", pdid);
    //    SqlCmd.Parameters.AddWithValue("@Flag", "Reconsulting");
    //    if (conn.State == ConnectionState.Closed)
    //    {
    //        conn.Open();
    //        dr = SqlCmd.ExecuteReader();

    //        dt.Load(dr);

    //    }

    //    return dt;

    //}

    //protected void Page_PreRender(object sender, EventArgs e)
    //{
    //    ViewState["update"] = Convert.ToString(Session["update"]);
    //}

    protected void DDL_AddEmg_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (DDL_AddEmg.SelectedIndex !=0)
        //{
        //    txt_TotalCharge.Text = DDL_AddEmg.SelectedValue;
        //    //txtCharges.Text = txt_TotalCharge.Text;
        //} 
    }

    protected void chk_Multiplefollowup_CheckedChanged(object sender, EventArgs e)
    {
        Response.Redirect("FollowupDetail.aspx?SC=HM&fdId=0&multifdid=" + fdId);
    }
}