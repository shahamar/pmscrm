﻿<%@ WebHandler Language="C#" Class="SessionHeartbeat" %>

using System;
using System.Web;
using System.Web.SessionState;

public class SessionHeartbeat : IHttpHandler, IRequiresSessionState{
    
    public void ProcessRequest (HttpContext context) {
        context.Session["heartbeat"] = DateTime.Now;
        context.Response.Write(context.Session["heartbeat"].ToString());
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}