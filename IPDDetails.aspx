﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IPDDetails.aspx.cs" Inherits="IPDDetails" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>IPD Details</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    
    <style>
    
    .b_submit {
    background: url("images/b_submit.gif") no-repeat scroll 0 0 transparent;
    border: 0 none;
    display: inline-block;
    font-family: arial;
    vertical-align: middle;
    width: 67px;
    height:32px; 
}

.textbutton 
{
    color: #FFFFFF !important;
    font-size: 12px;
    font-weight: bold;
    text-align: center;
    margin-top: -8px;
}
.loginform 
{
	font-family:Verdana; 
}

.left
{
	float:left;
}

.formmenu 
{
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #E6E6E6;
    margin-bottom: 10px;
    padding: 10px;
}
.tableh1 {
    background-image: url("images/tile_back1.gif");
    border-bottom: 1px solid #CED2D6;
    border-left: 1px solid #FFFFFF;
    border-right: 1px solid #FFFFFF;
    color: #606F79;
    font-size: 10px;
    height: 18px;
    padding: 8px 12px 8px 8px;
    font-family: verdana;
    font-weight: bold;
}

#tbsearch td
{
	text-align:left;
	vertical-align: middle;
}

#tbsearch label
{
	  display: inline;
    margin-top: 3px;
    position: absolute;
}

#tbhead th
{
	text-align:left;
	background: url(images/nav-back.gif) repeat-x top;
	color:#FFFFFF;
	padding-left:2px;
	padding-top:3px; 
}

#gvcol div
{
	margin-top:-10px;
}

.head1
{
	width:12%;
	font-size:small;
}

.head2
{
	width:30%;
	font-size:small;
}   

.head4
{
	width:12%;
	font-size:small;
}

.head5
{
	width:20%;
	font-size:small;
}

.head5i
{
	width:20%;
	font-size:small;
}

.head6
{
	width:6%;
	font-size:small;
	
}

.alt1 
{
	width:70px;
    }
.alt2 {
    width:120px;
}

</style>
    
</head>
<body style="background-color:#FFFFFF;">
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UP1" runat="server">
    <ContentTemplate>
    <div>
    <table style="width:100%; background-color:#FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                        <h2>IPD Details</h2>
                        
                        <div class="formmenu">
                            <div class="loginform">
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel" style="height:auto;border:1.5px solid #E2E2E2;">
                                    <div style="padding:15px; height:auto;">
                                        <table style="width: 100%; border: 1px solid #EFEFEF;">
                                            <tr>
                                                <td>
                                                      <table id="tabtbl" style="width:100%;" cellpadding="2px" cellspacing="0px" border="1px">
                                                        <tr>
                                                            <td class="alt1">
                                                                Patient Name :</td>
                                                            <td class="alt2">
                                                                <asp:Label ID="lblPatientName" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="alt1">
                                                                Age :</td>
                                                            <td class="alt2">
                                                                <asp:Label ID="lblAge" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="alt1">
                                                                Gender :</td>
                                                            <td class="alt2">
                                                                <asp:Label ID="lblGender" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="alt1">
                                                                Tele :</td>
                                                            <td class="alt2">
                                                                <asp:Label ID="lbltele" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="alt1">
                                                                Mobile :</td>
                                                            <td class="alt2">
                                                                <asp:Label ID="lblmob" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="alt1">
                                                                Email :</td>
                                                            <td class="alt2">
                                                                <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="alt1">
                                                                Patient ID :</td>
                                                            <td class="alt2">
                                                                <asp:Label ID="lblPatientId" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="alt1">
                                                                Case Paper No :</td>
                                                            <td class="alt2">
                                                                <asp:Label ID="lblCasePaperNo" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="alt1">
                                                                Cassette No :</td>
                                                            <td class="alt2">
                                                                <asp:Label ID="lblCassetteNo" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>  
                                                </td>
                                            </tr>
                                            <tr>
                                                 <td id="gvcol">
                                                    <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" 
                                    AutoGenerateColumns="False" CssClass="mGrid"  
                                    PagerStyle-CssClass="pgr" PageSize="20" ShowHeader="True" Width="100%" onrowcommand="GRV1_RowCommand">
                                    <Columns>
                                        
                                        <asp:TemplateField ItemStyle-Width="32%" ItemStyle-HorizontalAlign="Left" HeaderText="Provisional Diagnosis" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("ipdProvisionalDiagnosis")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-Width="32%" ItemStyle-HorizontalAlign="Left" HeaderText="Final Diagnosis" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("ipdFinalDiagnosis")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                        
                                        <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Addmission Date" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("ipdAdmissionDate", "{0:dd-MMM-yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Discharge Date" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("ipdDischargeDate", "{0:dd-MMM-yyyy}")%>
                                                <div style="cursor: pointer; float: right; margin-top: 0px;">
                                                    <%# Eval("Bill")%>    
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                  
                                        <asp:TemplateField HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgSelect" runat="server" CommandArgument='<%# Eval("ipdId") %>' CommandName="sel" ImageUrl="Images/mselect.ico" ToolTip="Select"/>
                                                <asp:ImageButton ID="imgDelete" runat="server" CommandArgument='<%# Eval("ipdId") %>' CommandName="del" ImageUrl="~/images/delete.ico" ToolTip="Delete" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>           
                                                </td>
                                            </tr>
                                        </table>           
                                    </div>
                                </div>
                            <div class="clr"></div>
                        </div>               
                        </div>
                    </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
                                
    </div>
    </form>
</body>
</html>
