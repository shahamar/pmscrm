﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="RecNNewCasePrint.aspx.cs" Inherits="RecNNewCasePrint" %>

    <%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

    <%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <script type="text/javascript">

     function ConfirmPrint(FormDep) {
         var confirm_value = document.createElement("INPUT");
         confirm_value.type = "hidden";
         confirm_value.name = "confirm_value";
         if (confirm("Do you want to Print?")) {

             confirm_value.value = "Yes";

             if (FormDep == "COMM_Print")
             { OPDPrintFunction(); }

             else {
                 confirm_value.value = "No";
             }
         }
         document.forms[0].appendChild(confirm_value);
     }


     function OPDPrintFunction() {


         setTimeout('DelayOPDPrintFunction()', 200);

     }

     function DelayOPDPrintFunction() {
         var myWindow = window.open("PrintReport.aspx");
         myWindow.focus();
         myWindow.print();
     }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
	<tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <div class="login-area margin_ten_right"  style="width: 100%;margin-bottom:10px" cellspacing="0px" cellpadding="5px">
            <span style="text-align:center;"><h2> Confirm Appointment</h2></span>
   			<div class="formmenu">
            <div class="loginform">
                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                    <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">
                        <tr>
                            <td align="right" width="50%">
                            <asp:DropDownList ID="DDL_ReportType" runat="server" CssClass="required field"
                                    Width="150px" >
                                    <asp:ListItem Text="New Case" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Reconsulting" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" width="50%">                            
                             <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="textbutton b_submit left"
                               OnClick="btnPrint_Click" OnClientClick="ConfirmPrint('COMM_Print');" />
                            </td>
						</tr>
					</table>
                  </div>
              </div>
              </div>
              </div>
           </td>
        </tr>
     </table>

    <asp:Panel BorderStyle="Outset"  BorderWidth="5px" BorderColor="#C4D4B6" ID="PNLWorkSheet" runat="server" Visible="false">                                   
    <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right"  ImageUrl="~/Images/FancyClose.png" />
        <asp:Panel ID="pnl1" runat="server" ScrollBars="Auto" Width="945px">
        <CR:CrystalReportViewer ID="CrystalReportViewer2" runat="server" AutoDataBind="True" Height="500px" EnableParameterPrompt="False" 
        ReuseParameterValuesOnRefresh="True" ToolPanelView="None" GroupTreeImagesFolderUrl="" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
           EnableDatabaseLogonPrompt="False" />
           <CR:CrystalReportSource ID="CrystalReportSource2" runat="server">
               <Report FileName="~/Reports/OpdReportAks.rpt"></Report>
           </CR:CrystalReportSource>
       </asp:Panel>
   </asp:Panel> 
</asp:Content>
