﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript">
    
        function Validate()
        {
            var uname = document.getElementById("<%= txtuname.ClientID %>").value;
            var pwd = document.getElementById("<%= txtpwd.ClientID %>").value;
            var Err = "";            
            if(uname == "")
            {
                Err += "Please enter user name";
                document.getElementById("<%= txtuname.ClientID %>").className = "field input-validation-error";
            }
            else
            {
                document.getElementById("<%= txtuname.ClientID %>").className = "field";
            }
            
            if(pwd == "")
            {
                Err += "Please enter password";
                document.getElementById("<%= txtpwd.ClientID %>").className = "field input-validation-error";
            }
            else
            {
                document.getElementById("<%= txtpwd.ClientID %>").className = "field";
            }
            
            if(Err != "")
            {
                return false;   
            }  
        }
        
    </script>
    
    <script type="text/javascript">
	
		if (top.location!= self.location) {
			top.location = self.location.href
		}
	
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="wrapper">
        <div class="page_wrapper">
            <div class="page" style="width: 845px;">
                <div>
                    <div class="intro"><h1>Login</h1></div><br />
                    <div class="login-area margin_ten_right">
                        <h2>User Area</h2>
                        <div class="formmenu">
                            <div class="loginform">
                                <label>Username*</label>
                                <asp:TextBox ID="txtuname" runat="server" CssClass="field"></asp:TextBox>
                                <label>Password*</label>
                                <asp:TextBox ID="txtpwd" runat="server" CssClass="field" TextMode="Password"></asp:TextBox>
                                <div class="clr" style="height:20px"></div>
                                <%--<asp:LinkButton ID="lnkForgetpassword" runat="server">Forget Password ?</asp:LinkButton>--%>
                                <label></label>
                                <asp:Button ID="btnSignIn" runat="server" Text="Login" 
                                    onclick="btnSignIn_Click" CssClass="textbutton b_submit right" OnClientClick="return Validate()" style="margin-top:-24px;"/>
                                
                            </div>
                        </div>
                    </div>                    
                    
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
