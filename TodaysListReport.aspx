﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="TodaysListReport.aspx.cs" Inherits="TodaysListReport" Title="Todays List Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <script type="text/javascript">
        function DisableButtons() {
            var inputs = document.getElementsByTagName("INPUT");
            for (var i in inputs) {
                if (inputs[i].type == "button" || inputs[i].type == "submit") {
                    inputs[i].disabled = true;
                }
            }
        }

        window.onbeforeunload = DisableButtons;
    </script>
    <style type="text/css">
        .auto-style2 {
            width: 25%;
        }
        .auto-style3 {
            width: 27%;
        }
        .auto-style4 {
            width: 5%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #f2f2f2;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Todays List Report</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo first" id="li_1"><a class="qt_tab active" href="TodaysList.aspx">
                                            Todays List</a> </li>
                                        <li class="qtab-HTML active last" id="li_9"><a class="qt_tab active" href="javascript:void(0)">
                                            Todays List Report</a> </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto;">
                                        <div style="padding: 15px; height: auto;">
                                            <table style="width: 100%; border: 1px solid #EFEFEF; background-color:#f2f2f2;">
                                                <tr>
                                                    <td colspan="3" style="text-align: center; color: Red;">
                                                        <%-- <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="text-align: right; color: Red;">
                                                        <table style="width:100%;" cellspacing="5px" cellpadding="5px">
                                                        <tr>
                                                        <td class="auto-style4">From :</td>
                                                        <td class="auto-style2">
                                                            <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                runat="server" Enabled="True" TargetControlID="txtfrm">
                                                            </cc1:CalendarExtender>&nbsp;
                                                            <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                        </td>
                                                        <td class="auto-style4">To :</td>
                                                        <td class="auto-style2">
                                                            <asp:TextBox ID="txtto" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                            </cc1:CalendarExtender>&nbsp;
                                                            <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        &nbsp;
                                                    </td>
                                                    <tr>
                                                        <td id="gvcol" colspan="3">
                                                            <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                CssClass="mGrid" EmptyDataText="No Records Found." PagerStyle-CssClass="pgr"
                                                                PageSize="20" Width="50%">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText ="Date">
                                                                        <ItemTemplate>
                                                                            <%# Eval("Date", "{0:dd-MMM-yyyy}")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
								                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText ="Total Cases Count">
                                                                        <ItemTemplate>
                                                                            <%# Eval("TotalCases")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
