﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="ManagePatients.aspx.cs" Inherits="ManagePatients" Title="Manage Patients" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<script type="text/javascript" src="js/custom-form-elements.js"></script>

<style type="text/css">
#tbsearch td { text-align: left; vertical-align: middle;}
#tbsearch label { display: inline; margin-top: 3px; position: absolute;}
#tbhead th
{
    text-align: left;
    background: url(images/nav-back.gif) repeat-x top;
    color: #FFFFFF;
    padding-left: 2px;
    padding-top: 3px;
}
#gvcol div { margin-top: -10px;}
.head1 { width: 10%;}
.head2 { width: 20%;}
.head3 { width: 8%;}
.head4 { width: 10%;}
.head5 { width: 13%;}
.head5i { width: 13%;}
.head5ii { width: 19%;}
.head6 { width: 7%;}
.datetime {position: absolute; width: 23px; height: 28px;}
.textbox {width:70px; padding:4px;}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UP1" runat="server">
<ContentTemplate>
     <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 100%; text-align:center">
                    <h2>Patient Registration</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <ul class="quicktabs_tabs quicktabs-style-excel">
                                <li class="qtab-Demo first" id="li_1">
                                	<a class="qt_tab active" href="AddNewPatient.aspx">Add New Patients</a> 
                                </li>
                                <li class="qtab-HTML active last" id="li_9">
                                	<a class="qt_tab active" href="javascript:void(0)">Manage Patients</a> 
                                </li>
                            </ul>
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">                                
                                <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">
                                        <tr>
                                            <td colspan="3" style="text-align: center; color: Red;">
                                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="tableh1">
                                                <table style="width:100%;" cellspacing="5px" cellpadding="5px">
                                                    <tr>
                                                        <td style="width: 14%">
                                                            <asp:CheckBox ID="chkConsultingPeriod" runat="server" Text="&nbsp;Consulting Period :" />
                                                        </td>
                                                        <td style="width: 12%">
                                                            <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                runat="server" Enabled="True" TargetControlID="txtfrm">
                                                            </cc1:CalendarExtender>&nbsp;
                                                            <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                        </td>
                                                        <td style="width: 3%">To :</td>
                                                        <td style="width: 12%">
                                                            <asp:TextBox ID="txtto" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                            </cc1:CalendarExtender>&nbsp;
                                                            <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" />
                                                        </td>
                                                        <td style="color: Red; text-align: right;">
                                                            <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table  class="mGrid" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                    <tr>
                                                        <th class="head1" style="display:none">Card No</th>
                                                        <th class="head2">Name</th>
                                                        <th class="head3">Gender</th>
                                                        <th class="head4">DOC</th>
                                                        <th class="head5">Case Paper No</th>
                                                         <th class="head5">ReConsulting Case Paper No</th>
                                                        <th class="head5i" style="display:none">Cassette No</th>
                                                        <th class="head5ii">Mobile No</th>
                                                        <th class="head6">Action</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="head1" style="display:none">
                                                            <asp:TextBox ID="txtCardNo" runat="server" class="field" Width="75px"></asp:TextBox>
                                                        </th> 
                                                        <th class="head2">
                                                            <asp:TextBox ID="txtname" runat="server" class="field" Width="224px"></asp:TextBox>
                                                        </th>
                                                        <th class="head3"></th>
                                                        <th class="head4"></th>
                                                        <th class="head5">
                                                            <asp:TextBox ID="txtcasepaperno" runat="server" class="field" Width="139px"></asp:TextBox>
                                                        </th>
                                                         <th class="head5">
                                                            <asp:TextBox ID="txtRCasePaperno" runat="server" class="field" Width="139px"></asp:TextBox>
                                                        </th>
                                                        <th class="head5i" style="display:none">
                                                            <asp:TextBox ID="txtcassetteno" runat="server" class="field" Width="100px"></asp:TextBox>
                                                        </th>
                                                        <th class="head5ii">
                                                            <asp:TextBox ID="txtOccupation" runat="server" class="field" Width="170px"></asp:TextBox> <%--Mobile no search--%>
                                                        </th>
                                                        <th class="head6">
                                                            <div style="display: block;">
                                                                <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none;
                                                                    text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click">
                                                        			<img src="images/filter.png" style="border:0px;"/>&nbsp; &nbsp;Filter
                                                                </asp:LinkButton>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="gvcol" colspan="3" style="width:100%; text-align: center;background:#fff">
                                                <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                    CssClass="mGrid"  PageSize="20" ShowHeader="false" Width="100%" EmptyDataText="No records found..."
                                                    OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" Visible="false">
                                                            <ItemTemplate>
                                                                <%# Eval("pdPatientICardNo")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkpdName" runat="server" Text='<%# Eval("pdName")%>' CommandArgument='<%# Eval("pdID") %>'
                                                                    CommandName="edt" Style="text-decoration: none; color: Blue; font-weight: normal;"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("pdSex")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("pdDOC", "{0:dd-MMM-yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("pdCasePaperNo")%> 
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                       <asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("pdRCasePaperNo")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="center" Visible="false">
                                                            <ItemTemplate>
                                                                <%# Eval("pdCassetteNo")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="19%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%--<%# Eval("pdOccupation")%>--%>
                                                                <%# Eval("pdMob")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center" ItemStyle-Width="7%">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnCancel" runat="server" CommandArgument='<%# Eval("pdID") %>'
                                                                    CommandName="_cancel" ImageUrl="images/delete.ico" ToolTip="Delete" 
                                                                    OnClientClick="return confirm('If you delete the patient, all the OPD/IPD details will be deleted regarding
                                                                     this patient. \nAre you sure you want to delete the patient? ');" />
                                                                <%# Eval("PrintBill") %>
								  <asp:ImageButton ID="btnRec" runat="server" CommandArgument='<%# Eval("pdID") %>' CommandName="_Reconsulting" ImageUrl="images/bill.ico" Tooltip="Reconsulting" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>                                        
                                    </table>                        
                            	</div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
