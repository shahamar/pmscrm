﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="AwaitingCourierReq.aspx.cs" Inherits="AwaitingCourierReq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <%--<meta http-equiv="refresh" content="10;" />--%>
    <style type="text/css">
        .blanket
        {
            background-color: #111;
            opacity: 0.65;
            position: absolute;
            z-index: 9001; /*ooveeerrrr nine thoussaaaannnd*/
            top: 0px;
            left: 0px;
            width: 100%;
        }
        .popUpDiv
        {
            position: absolute;
            background-color: #eeeeee;
            width: 70%;
            height: auto;
            z-index: 9002; /*ooveeerrrr nine thoussaaaannnd*/
            -moz-box-shadow: inset 0 0 5px #585858;
            -webkit-box-shadow: inset 0 0 5px #585858;
        }
        .fltrt
        {
            float: right;
            margin-right: 10px;
        }
        .tblbox
        {
            height: 700px;
            width: 100%;
            margin-top: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%;" cellspacing="5px" cellpadding="0px">
        <tr>
            <td colspan="4">
                <span style="text-align: center;">
                    <h2>
                        Awaiting Courier Request </h2>
                </span>
            </td>
        </tr>
        <tr>
            <td>
            <asp:CheckBox ID="chkDT" runat="server" />

                From Date :

                 <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox"></asp:TextBox>
                <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                    runat="server" Enabled="True" TargetControlID="txtfrm">
                </cc1:CalendarExtender>
                &nbsp;
                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />

            </td>
            
            <td>
                To Date :

                  <asp:TextBox ID="txtto" runat="server" CssClass="textbox"></asp:TextBox>
                <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                    Format="dd/MM/yyyy" PopupButtonID="imgto">
                </cc1:CalendarExtender>
                &nbsp;
                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />

            </td>
          
            <td>
                <asp:DropDownList ID="DDL_Stat" runat="server" CssClass="required field" Width="150px" Visible="false">
                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    <asp:ListItem Text="Request Done" Value="RD"></asp:ListItem>
                    <asp:ListItem Text="Request Pending" Value="RP"></asp:ListItem>
                </asp:DropDownList>
            </td>

             <td>
                    <asp:Button ID="btnView" runat="server" CssClass="textbutton b_submit" OnClick="btnView_Click"
                        Style="margin-top: 2px;" Text="View" ValidationGroup="val" />
                </td>


             </tr>

          
        <tr>
            <td id="Td1" align="center" style="width: 100%; text-align: center;"  colspan="4">
                <center>
                    <asp:GridView ID="GVD_OnLinePat" runat="server" CssClass="mGrid" AutoGenerateColumns="false" AllowPaging="true"
                        OnRowCommand="GVD_OnLinePat_RowCommand" PageSize="50" OnPageIndexChanging="GVD_OnLinePat_PageIndexChanging">
                        <Columns>
                            <%--<asp:BoundField DataField="SNO" HeaderText="SN." ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="10%" />--%>
			    <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderText="Sr.No.">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
			    </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkNewCasepdName" runat="server" Text='<%# Eval("pdName")%>'
                                        CommandArgument='<%# Eval("pdId") + ";" + Eval("fdId") + ";" + Eval("OLID") %>' 
                                        CommandName="NewCaseFol" Style="text-decoration: none; font-weight: normal;"
                                        ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("FStatus").ToString())%>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-HorizontalAlign="center" HeaderText="DOR">
                                <ItemTemplate>
                                    <%# Eval("DOR", "{0:dd-MMM-yyyy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="State" HeaderText="State" />
                            <asp:BoundField DataField="City" HeaderText="City" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderText="Mobile No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblpbMob" runat="server" Text='<%# Eval("pdMob")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="RegistrationFees" HeaderText="Paid Amount" />
                            <asp:BoundField DataField="PaymentReferenceNo" HeaderText="Transaction Number" />
                           
                        </Columns>
                    </asp:GridView>
                </center>
            </td>
        </tr>
    </table>

</asp:Content>

