﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PatientReport.aspx.cs" Inherits="PatientReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Patient Report</title>
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <style type="text/css">
        .colfirst
        {
            font-family: Times New Roman;
            border-left: 2px solid #000000;
            border-right: 2px solid #000000;
            border-top: 2px solid #000000;
        }
        
        .collast
        {
            font-family: Times New Roman;
            border-left: 2px solid #000000;
            border-right: 2px solid #000000;
            border-bottom: 2px solid #000000;
        }
        
        .colmiddle
        {
            font-family: Times New Roman;
            border-left: 2px solid #000000;
            border-right: 2px solid #000000;
        }
        .tbl_pt_inform_col1
        {
            width: 17%;
            text-align: left;
        }
        
        #tbl_pt_inform tr, td
        {
            font-family: Verdana;
            font-size: 13px;
        }
        .tbl_pt_inform_col2
        {
            width: 33%;
            text-align: left;
        }
        
        .tbl_pt_inform_col3
        {
            width: 10%;
        }
        
        .tbl_pt_inform_col4
        {
            width: 10%;
        }
        
        .ltd
        {
            border-left: 1px solid #DEDEDE;
            border-top: 1px solid #DEDEDE;
        }
        .rtd
        {
            border-left: 1px solid #DEDEDE;
            border-top: 1px solid #DEDEDE;
            border-right: 1px solid #DEDEDE;
        }
        .btd
        {
            border-left: 1px solid #DEDEDE;
            border-top: 1px solid #DEDEDE;
            border-bottom: 1px solid #DEDEDE;
        }
        
        .brtd
        {
            border-left: 1px solid #DEDEDE;
            border-top: 1px solid #DEDEDE;
            border-bottom: 1px solid #DEDEDE;
            border-right: 1px solid #DEDEDE;
        }
        
        fieldset
        {
            padding: 0.5em 1em;
            border: solid Red 1px;
            width: 95%;
        }
        legend
        {
            font-weight: bold;
            color: #056735;
            font-family: Verdana;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%" cellpadding="2px" cellspacing="0px">
            <tr>
                <td class="colfirst" align="center">
                    <font style="font-size: 18px; color: Red;"><b>ADITYA HOMOEOPATHIC HOSPITAL & HEALING
                        CENTRE.</b></font><br />
                    Near Dakshinmukhi Hanuman Mandir, S/No. 309/1/2,<br />
                    Shivaji Chowk, Pimpri Gaon, Pune - 411017<br />
                    Phone No :- 020-27412197, 020-27412549
                </td>
            </tr>
            <tr>
                <td class="colmiddle">
                    <div style="float: left; font-weight: bold; padding-left: 10px; color: #09703C;">
                        DR. AMARSINHA D. NIKAM<br />
                        Consulting Homeopath
                    </div>
                </td>
            </tr>
            <tr>
                <td class="colfirst">
                </td>
            </tr>
            <tr>
                <td class="colmiddle" align="center">
                    <fieldset>
                        <legend>General Information</legend>
                        <table id="tbl_pt_inform" width="95%" align="center" cellspacing="0px;" cellpadding="3px;">
                            <tr>
                                <td class="tbl_pt_inform_col1 ltd">
                                    Name :
                                </td>
                                <td class="tbl_pt_inform_col2 ltd">
                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                </td>
                                <td class="tbl_pt_inform_col1 ltd">
                                    Patient ID :
                                </td>
                                <td class="tbl_pt_inform_col2 rtd">
                                    <asp:Label ID="lblPatientId" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tbl_pt_inform_col1 ltd">
                                    Gender :
                                </td>
                                <td class="tbl_pt_inform_col2 ltd">
                                    <asp:Label ID="lblGender" runat="server" Text=""></asp:Label>
                                </td>
                                <td class="tbl_pt_inform_col1 ltd">
                                    Age :
                                </td>
                                <td class="tbl_pt_inform_col2 rtd">
                                    <asp:Label ID="lblAge" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tbl_pt_inform_col1 ltd">
                                    Case Paper No :
                                </td>
                                <td class="tbl_pt_inform_col2 ltd">
                                    <asp:Label ID="lblCasePaperNo" runat="server" Text=""></asp:Label>
                                </td>
                                <td class="tbl_pt_inform_col1 ltd">
                                    Cassette No :
                                </td>
                                <td class="tbl_pt_inform_col2 rtd">
                                    <asp:Label ID="lblCassetteNo" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tbl_pt_inform_col1 btd">
                                    Occupation :
                                </td>
                                <td class="tbl_pt_inform_col2 btd">
                                    <asp:Label ID="lblOccupation" runat="server" Text=""></asp:Label>
                                </td>
                                <td class="tbl_pt_inform_col1 btd">
                                    Contact Details :
                                </td>
                                <td class="tbl_pt_inform_col2 brtd">
                                    <asp:Label ID="lblContact" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tbl_pt_inform_col1 btd">
                                    Doctor Name :
                                </td>
                                <td class="tbl_pt_inform_col2 btd">
                                    <asp:Label ID="lblDoctorName" runat="server" Text=""></asp:Label>
                                </td>
                                <td class="tbl_pt_inform_col1 btd">
                                </td>
                                <td class="tbl_pt_inform_col2 brtd">
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td class="colmiddle">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="colmiddle" align="center" style="font-family: Verdana; font-size: 12px;">
                    <div>
                        <div runat="server" id="divkco" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>K/C/O</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblkco" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divinv" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Investigation</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblInv" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divchief" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Chief C/O</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblChiefCo" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divpho" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Past HO</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblpho" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divfho" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Family HO</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblfho" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divphy" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Physical Generals</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblphy" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divmnd" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Mind</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblMind" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divAF" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>A/F</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblAF" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divThermal" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Thermal</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblThermal" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divFollowupHistory" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Consulting & Followup History</legend>
                                <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                    CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" Width="100%" OnPageIndexChanging="GRV1_PageIndexChanging" OnRowDataBound="GRV1_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderText="Date">
                                            <ItemTemplate>
                                                <%# Eval("fdDate", "{0:dd-MMM-yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Reconsulting/Acute">
                                            <ItemTemplate>
                                                <%# Eval("fdReconsulting")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderText="Casstte No">
                                            <ItemTemplate>
                                                <%# Eval("fdCasseteNo")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Remedy">
                                            <ItemTemplate>
                                                <%# Eval("fdRemedy")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderText="Remedy Period">
                                            <ItemTemplate>
                                                <%# Eval("fdRemedyPeriod")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%" HeaderText="Dose">
                                            <ItemTemplate>
                                                <%# Eval("fdDoseType")%>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </div>
                        <div id="divOpdTestDetails" runat="server" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>OPD Test Details</legend>
                                <asp:GridView ID="GRV2" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                    CssClass="mGrid" Width="55%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Test" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="45%">
                                            <ItemTemplate>
                                                <%# Eval("tmTestName")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Test Charges" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="20%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBaseCharges2" runat="server" Text='<%# Eval("ptdCharges")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Test Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25%">
                                            <ItemTemplate>
                                                <asp:Label ID="ltDate2" runat="server" Text='<%# Eval("ptdFromDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="collast">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
