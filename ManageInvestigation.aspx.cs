﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Drawing;

public partial class ManageInvestigation : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string uid;
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx", false);

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 24";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            txt_InvDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindGVR();
        }
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }

    public void BindGVR()
    {

        string[] _IOD = txt_InvDate.Text.ToString().Split('/');
        string IOD = _IOD[2] + "/" + _IOD[1] + "/" + _IOD[0];

        try
        {
           // strSQL = "select VisitTime,id.BedNo,wmWardNo,pdInitial +'  '+pdFname +'  '+pdMname +'  '+ pdLname AS 'PName',id.Complaint,id.Remedy,id.BpMin,id.BpMax,id.PatientID from tblInvestigationMaster im inner join tblInvestigationDetails id on im.InvestID=id.InvestID inner join dbo.tblWardMaster w on w.wmID=im.wmID inner join tblPatientDetails p on id.PatientID  =p.pdID WHERE flag=0";
            //strSQL = ";With AksTable As ( Select Rank() over (Partition BY pdid order by IM.InvestID DESC) as [Rank] , InvDate , VisitTime , wmWardNo , bmNoOfBads , VWP.PName PName , VWP.pdid , VWP.ipdid , Complaint , RM.RMNAME ,InvPulseRate Pulse , BP BP , BpMin Temp , BpMax RR , LatestSymptom SuggInv from VW_GetWardNBedStatus_Test VWP Inner Join tblInvestigationDetails ID On ID.PatientID = VWP.pdid Inner Join tblInvestigationMaster IM On IM.InvestID = ID.InvestID Left Join tblRemedyMaster RM On RM.RMID = ID.InvRemedyNew Where CONVERT(date, InvDate) = '" + IOD + "'  AND ID.IncDecStat = " + DDL_INDC.SelectedValue + " ) Select * from AksTable Where [Rank] = 1";
            //strSQL += " order by wmWardNo,bmNoOfBads asc";

            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "GetIPDRoundDetailsByTimeSlot";
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.AddWithValue("@FrmDate", IOD);
            sqlcmd.Parameters.AddWithValue("@ToDate", IOD);
            sqlcmd.Parameters.AddWithValue("@Status", DDL_INDC.SelectedValue);
            sqlcmd.Parameters.AddWithValue("@TimeSlot", DDL_TimeSlot.SelectedValue);
            
            DataTable dt = gen.getDataTable(sqlcmd);
            if (dt.Rows.Count > 0)
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;
    }

    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
        {
            string pdid = (string)this.GRV1.DataKeys[e.Row.RowIndex]["pdId"].ToString();

            Label lblkco = (Label)e.Row.FindControl("lblkco");
            Label lblco = (Label)e.Row.FindControl("lblco");

            if (File.Exists(Server.MapPath("Files/KCO/kco_" + pdid)))
            {
                lblkco.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + pdid).ToString());
            }
            else
            {
                lblkco.Text = "";
            }


            if (File.Exists(Server.MapPath("Files/ChiefCo/chief_" + pdid)))
            {
                lblco.Text = File.ReadAllText(Server.MapPath("Files/ChiefCo/chief_" + pdid).ToString());
            }


        }
    }

    protected void DDL_INDC_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGVR();
    }
}
