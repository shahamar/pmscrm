﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class IPDView : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole,msg;
    //string pdId = "";
    string uid = "";
    //string ipdid = "";
    //added by nikita on 12th march 2015------------------------------
    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-------------------------------------------------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            else
            {
                Session["ipdid"] = Convert.ToInt32(gen.executeScalar("Select ipdId From tblIpdMaster Where ipdpdId =" + PdID).ToString());
                ViewState["ipdid"] = Session["ipdid"];
            }

            //---Added By Nikita On 30thMarch 2015----------------------------------------------------
            if (msg == "1")
            {
                btnPrint.Enabled = false;
                btnPrintIPD.Visible = false;
            }
            else
            {
                btnPrint.Enabled = true;
                btnPrintIPD.Visible = true;
            }
            //-----------------------------------------------------------------------------------------

            FillData();
        }
    }

    public void FillData()
    {
        strSQL = "Select LEFT(ipdAdmissionTime,LEN(ipdAdmissionTime)-5) +' '+ Right(ipdAdmissionTime,LEN(ipdAdmissionTime)-7) As 'ipdAdmissionTime'  , * From tblIpdMaster Where IsDelete = 0 AND ipdpdId =" + PdID;
        //ipdId=" + ipdid;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            if (Sqldr["ipdProvisionalDiagnosis"] != DBNull.Value && Sqldr["ipdProvisionalDiagnosis"].ToString() != "")
                lblProvisionalDiagnosis.Text = "<font class='fontin'>Provisional Diagnosis :</font><br /><div class='divin'>" + Sqldr["ipdProvisionalDiagnosis"].ToString().Replace("\n", "<br />") + "</div>";
            else
                lblProvisionalDiagnosis.Text = "<font class='fontin'>Provisional Diagnosis :</font><br /><div class='divin'><font style='color:Red;'>Not Mentioned</font></div>";

            if (Sqldr["ipdFinalDiagnosis"] != DBNull.Value && Sqldr["ipdFinalDiagnosis"].ToString() != "")
                lblFinalDiagnosis.Text = "<font class='fontin'>Final Diagnosis :</font><br /><div class='divin'>" + Sqldr["ipdFinalDiagnosis"].ToString().Replace("\n", "<br />") + "</div>";
            else
                lblFinalDiagnosis.Text = "<font class='fontin'>Final Diagnosis :</font><br /><div class='divin'><font style='color:Red;'>Not Mentioned</font></div>";

            if (Sqldr["ReconsultingDiagnosis"] != DBNull.Value && Sqldr["ReconsultingDiagnosis"].ToString() != "")
                lblReconsultingDiagnosis.Text = "<font class='fontin'>Reconsulting Diagnosis :</font><br /><div class='divin'>" + Sqldr["ReconsultingDiagnosis"].ToString().Replace("\n", "<br />") + "</div>";
            else
                lblReconsultingDiagnosis.Text = "<font class='fontin'>Reconsulting Diagnosis :</font><br /><div class='divin'><font style='color:Red;'>Not Mentioned</font></div>";
            
            if (Sqldr["ipdOperation"] != DBNull.Value && Sqldr["ipdOperation"].ToString() != "")
                lblOperation.Text = "<font class='fontin'>Operation :</font><br /> <div class='divin'>" + Sqldr["ipdOperation"].ToString().Replace("\n", "<br />") + "</div>";
            else
                lblOperation.Text = "<font class='fontin'>Operation :</font><br /> <div class='divin'><font style='color:Red;'>Not Mentioned</font></div>";

            if (Sqldr["ipdAdmissionDate"] != DBNull.Value && Sqldr["ipdAdmissionDate"].ToString() != "")
                lblDOA.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["ipdAdmissionDate"].ToString()));
            else
                lblDOA.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdAdmissionTime"] != DBNull.Value && Sqldr["ipdAdmissionTime"].ToString() != "")
                lblTime.Text = Sqldr["ipdAdmissionTime"].ToString();
            else
                lblTime.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdRefBy"] != DBNull.Value && Sqldr["ipdRefBy"].ToString() != "")
                lblRefBy.Text = Sqldr["ipdRefBy"].ToString();
            else
                lblRefBy.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdCheckeddBy"] != DBNull.Value && Sqldr["ipdCheckeddBy"].ToString() != "")
                lblChecked.Text = Sqldr["ipdCheckeddBy"].ToString();
            else
                lblChecked.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdPreparedBy"] != DBNull.Value && Sqldr["ipdPreparedBy"].ToString() != "")
                lblPrepared.Text = Sqldr["ipdPreparedBy"].ToString();
            else
                lblPrepared.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdTreatment"] != DBNull.Value && Sqldr["ipdTreatment"].ToString() != "")
                lblTreatment.Text = Sqldr["ipdTreatment"].ToString();
            else
                lblTreatment.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdDischargeDate"] != DBNull.Value && Sqldr["ipdDischargeDate"].ToString() != "")
                lblDichargeDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["ipdDischargeDate"].ToString()));
            else
                lblDichargeDate.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdDischargeTime"] != DBNull.Value && Sqldr["ipdDischargeTime"].ToString() != "")
                lblDischargeTime.Text = Sqldr["ipdDischargeTime"].ToString();
            else
                lblDischargeTime.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdDepositAmount"] != DBNull.Value && Sqldr["ipdDepositAmount"].ToString() != "")
                lblDepAmt.Text = Sqldr["ipdDepositAmount"].ToString();
            else
                lblDepAmt.Text = "<font style='color:Red;'>Not Mentioned</font>";
        }
        Sqldr.Close();
        Sqldr.Dispose();
        //***********************************************************************************************************************************
        FillWardDetails();

        //if (gen.doesExist("Select * From tblIpdMaster Where ipdId=" + ipdid + " AND ipdIsDischarge = 1") == true)
        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if(msg == "1")
            divshiftward.Visible = false;
        else
        {
            if (gen.doesExist("Select * From tblIpdMaster Where IsDelete = 0 AND ipdId=" + ipdid + "") == true)
                divshiftward.Visible = true;
        }

        if (msg == "1")
            divrelativward.Visible = false;
        else
        {
            if (gen.doesExist("Select * From tblIpdMaster Where IsDelete = 0 AND ipdId=" + ipdid + " AND ipdIsDischarge = 1") == true)
                divrelativward.Visible = false;
            else
                divrelativward.Visible = true;
        }

        if (msg == "1")
        {
            div_print.Style.Add("display", "none");
            div_Addmission.Style.Add("display", "none");
            div_Diagnosis.Style.Add("display", "none");
            div_Remedy.Style.Add("display", "none");
        }
        else
        {
            div_print.Style.Add("display", "block");
            div_Addmission.Style.Add("display", "block");
            div_Diagnosis.Style.Add("display", "block");
            div_Remedy.Style.Add("display", "block");
        }
        //-----------------------------------------------------------------------------------------
        //***********************************************************************************************************************************
        //strSQL = "Select * From tblPatientsWardDetails Where wipdId=" + ipdid;
        //SqlCmd = new SqlCommand(strSQL, con);
        //Sqldr = SqlCmd.ExecuteReader();

        //***********************************************************************************************************************************
        FillTestDetails();
        FillRemedyDetails();
        FillRelativeWardDetails();
    }
    
    public void FillTestDetails()
    {
        strSQL = "sp_getIPDTestDetails";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@itdipdId", ipdid);
        dapt = new SqlDataAdapter(SqlCmd);
        DataTable dt = new DataTable();
        dapt.Fill(dt);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        
        if(msg == "1")
            GRV1.Columns[3].Visible = false;
        else
            GRV1.Columns[3].Visible = true;

        if (dt.Rows.Count > 0)
        {
            pnlfeat.Visible = true;
            divInv.Visible = false;
            btnPrint.Visible = true;
        }
        else
        {
            pnlfeat.Visible = false;
            divInv.Visible = true;
            btnPrint.Visible = false;
        }
        dt.Dispose();
    }

    protected void GRV2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GRV2.EditIndex = -1;
        FillWardDetails();
    }

    protected void GRV2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRV2.EditIndex = e.NewEditIndex;
        FillWardDetails();
    }

    protected void GRV2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GRV2.Rows[e.RowIndex];
        HiddenField hidwId = (HiddenField)row.FindControl("hdnwId");

        DropDownList ddwType = (DropDownList)row.FindControl("ddwType");
        DropDownList ddWardNo = (DropDownList)row.FindControl("ddWardNo");
        DropDownList ddBedNo = (DropDownList)row.FindControl("ddBedNo");

      //  TextBox txtWardNo = (TextBox)row.FindControl("txtWardNo");
       // TextBox txtBedNo = (TextBox)row.FindControl("txtBedNo");

        TextBox InDate = (TextBox)row.FindControl("txtInDate");
        TextBox OutDate = (TextBox)row.FindControl("txtOutDate");

        object InDateAks = InDate.Text == "" ? null : InDate.Text.Trim();
        object OutDateAks = OutDate.Text == "" ? null : OutDate.Text.Trim();

       
        //strSQL = "Update tblPatientsWardDetails SET wType=" + ddwType.SelectedValue + ",wNo = '" + txtWardNo.Text + "',wBedNo='" + txtBedNo.Text + "' ,wInDate='" + InDate.Text.Trim() + "',wOutDate='" + OutDate.Text.Trim() + "'";
        //strSQL += " where wId = " + hidwId.Value + "";
        //gen.executeQuery(strSQL);
        //GRV2.EditIndex = -1;

        SqlCmd = new SqlCommand();
        SqlCmd.CommandText = "proc_EditPatientWardDetails";
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@wId", hidwId.Value);
        SqlCmd.Parameters.AddWithValue("@wType", ddwType.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@wNo", ddWardNo.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@wBedNo", ddBedNo.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@wInDate", InDateAks);
        SqlCmd.Parameters.AddWithValue("@wOutDate", OutDateAks);
        SqlCmd.Parameters.AddWithValue("@modifiedBy", uid);
        SqlCmd.Connection = con;
        if (con.State == ConnectionState.Closed)
            con.Open();

        SqlCmd.ExecuteNonQuery();
        con.Close();
        SqlCmd.Dispose();
        GRV2.EditIndex = -1;
        FillWardDetails();
    }

    protected void GRV2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int wId = int.Parse(e.CommandArgument.ToString());

        if (e.CommandName == "delete")
        {
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeletePatientWardDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@wId", wId);
            SqlCmd.Parameters.AddWithValue("@DeletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            FillWardDetails();

        }
    }

    public void FillWardDetails()
    {
        //strSQL = "Select *,Case wType When 1 Then 'General' When 2 Then 'Semi Special' When 3 Then 'Special' When 4 Then 'Delux'  END _wType From tblPatientsWardDetails Where wipdId=" + ipdid;
        strSQL = "Select * From VW_GetPatientWardDetails Where wipdId=" + ipdid;
        //strSQL += " AND wId = (SELECT MAX(wId) FROM dbo.tblPatientsWardDetails Where wipdId=" + ipdid + ")";

        DataTable _dt = gen.getDataTable(strSQL);
        GRV2.DataSource = _dt;
        GRV2.DataBind();

        if (_dt.Rows.Count > 1)
        {
            GRV2.Columns[7].Visible = true;
        }
        else
        {
            GRV2.Columns[7].Visible = false;
        }
        if (msg == "1")
            GRV2.Columns[6].Visible = false;
        else
            GRV2.Columns[6].Visible = true;
        _dt.Dispose();
    }

    public void FillRelativeWardDetails()
    {
        strSQL = "Select *,Case rwType When 1 Then 'General' When 2 Then 'Semi Special' END _wType From tblRelativeWardDetails Where ipdId=" + ipdid;


        DataTable _dt = gen.getDataTable(strSQL);
        GRVRelative.DataSource = _dt;
        GRVRelative.DataBind();
        if (msg == "1")
            GRVRelative.Columns[5].Visible = false;
        else
            GRVRelative.Columns[5].Visible = true;

        _dt.Dispose();
    }

    public void FillRemedyDetails()
    {
        strSQL = "Select RM.RMNAME , IR.irRemedy , IR.irId , IR.irStartDate  From tblIpdRemedy IR Inner Join tblRemedyMaster RM On IR.irRemedy = RM.RMID Where iripdId=" + ipdid;
        //strSQL += " AND wId = (SELECT MAX(wId) FROM dbo.tblPatientsWardDetails Where wipdId=" + ipdid + ")";
        DataTable _dt = gen.getDataTable(strSQL);
        GRV3.DataSource = _dt;
        GRV3.DataBind();
        if(msg == "1")
            GRV3.Columns[2].Visible = false;
        else
            GRV3.Columns[2].Visible = true;

        if (_dt.Rows.Count >= 1)
        {
            pnlremedy.Visible = true;
            divRemedy.Visible = false;
        }
        else
        {
            pnlremedy.Visible = false;
            divRemedy.Visible = true;
        }
        _dt.Dispose();


    }

    protected void GRV2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && GRV2.EditIndex == e.Row.RowIndex)
        {
            DropDownList ddwType = (DropDownList)e.Row.FindControl("ddwType");
            DropDownList ddWardNo = (DropDownList)e.Row.FindControl("ddWardNo");
            DropDownList ddBedNo = (DropDownList)e.Row.FindControl("ddBedNo");
           
            ddwType.Items.Clear();
            ddwType.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT Distinct WardTypeID As wmWardTypeId , WardTypeName FROM tblWardTypeMaster Where IsDelete = 0", "wmWardTypeId", "WardTypeName", ddwType);
            ddwType.Items.FindByValue((e.Row.FindControl("hdnwType") as HiddenField).Value).Selected = true;

            ddWardNo.Items.Clear();
            ddWardNo.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + ddwType.SelectedValue + "' And IsDelete = 0 ", "wmID", "wmWardNo", ddWardNo);
            ddWardNo.Items.FindByValue((e.Row.FindControl("hdnWardNo") as HiddenField).Value).Selected = true;

            ddBedNo.Items.Clear();
            ddBedNo.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT bmNoOfBads , bmID FROM tblBadMaster WHERE wmID='" + ddWardNo.SelectedValue + "' And IsDelete = 0 ", "bmID", "bmNoOfBads", ddBedNo);
            ddBedNo.Items.FindByValue((e.Row.FindControl("hdnBedNo") as HiddenField).Value).Selected = true;


        }
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string itdId = e.CommandArgument.ToString();
        if (e.CommandName == "del")
        {
            //gen.executeQuery("Delete From tblIpdTestDetails Where itdId = " + itdId + "");
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteIPDTestDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@itdId", itdId);
            SqlCmd.Parameters.AddWithValue("@deletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            FillTestDetails();
        }
    }

    protected void GRV3_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GRV3.EditIndex = -1;
        FillRemedyDetails();
    }

    protected void GRV3_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRV3.EditIndex = e.NewEditIndex;
        FillRemedyDetails();
    }

    protected void GRV3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GRV3.Rows[e.RowIndex];
        HiddenField hdnirId = (HiddenField)row.FindControl("hdnirId");
        DropDownList DDL_RemList = (DropDownList)row.FindControl("DDL_RemList");

        TextBox StartDate = (TextBox)row.FindControl("txtStartDate");
       
        strSQL = "Update tblIpdRemedy SET irRemedy = '" + DDL_RemList.SelectedValue + "',irStartDate='" + StartDate.Text.Trim() + "'  Where irId = " + hdnirId.Value + "";
        gen.executeQuery(strSQL);
        GRV3.EditIndex = -1;
        FillRemedyDetails();
    }

    protected void GRV3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && GRV3.EditIndex == e.Row.RowIndex)
        {
            DropDownList DDL_RemList = (DropDownList)e.Row.FindControl("DDL_RemList");
            Label lblirRemID = (Label)e.Row.FindControl("lblirRemID");

            DDL_RemList.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("Select RMID , RMNAME  from tblRemedyMaster Where IsDelete = 0", "RMID", "RMNAME", DDL_RemList);
            DDL_RemList.Items.FindByValue((e.Row.FindControl("lblirRemID") as Label).Text).Selected = true;
        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        string ptdId = "";
        string newptdId = "";
        string fptdId = "";

        foreach (GridViewRow grvrow in GRV1.Rows)
        {
            HtmlInputCheckBox chk = (HtmlInputCheckBox)grvrow.FindControl("cbAC");
            if (chk.Checked == true)
            {
                ptdId = chk.Value;
                newptdId += ',' + ptdId;
            }

        }
        if (newptdId != "")
        {
            fptdId = newptdId.Substring(1);
            Response.Redirect("IPDBillFormat.aspx?id=" + fptdId + "");
        }
    }
    protected void GRVRelative_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GRVRelative.EditIndex = -1;
        FillRelativeWardDetails();
    }

    protected void GRVRelative_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && GRVRelative.EditIndex == e.Row.RowIndex)
        {
            DropDownList ddrwType = (DropDownList)e.Row.FindControl("ddrwType");
            HiddenField hdnrwType = (HiddenField)e.Row.FindControl("hdnrwType");

            ddrwType.SelectedValue = hdnrwType.Value;
        }

    }

    protected void GRVRelative_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRVRelative.EditIndex = e.NewEditIndex;
        FillRelativeWardDetails();

    }

    protected void GRVRelative_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //GridViewRow row = GRVRelative.Rows[e.RowIndex];
        //HiddenField hidrwId = (HiddenField)row.Cells[0].FindControl("hdnrwId");
        //DropDownList ddrwType = (DropDownList)row.Cells[0].FindControl("ddrwType");
        //TextBox txtrWardNo = (TextBox)row.Cells[1].FindControl("txtrWardNo");
        //TextBox txtrBedNo = (TextBox)row.Cells[2].FindControl("txtrBedNo");

        //strSQL = "Update tblRelativeWardDetails SET rwType=" + ddrwType.SelectedValue + ",rwNo = '" + txtrWardNo.Text + "',rwBedNo='" + txtrBedNo.Text + "'";
        //strSQL += " where rwId = " + hidrwId.Value + "";
        //gen.executeQuery(strSQL);
        //GRVRelative.EditIndex = -1;
        //FillRelativeWardDetails();
    }

    protected void GRVRelative_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            GRVRelative.EditIndex = -1;
        }

        if (e.CommandName == "del")
        {
            int rwid = Convert.ToInt32(e.CommandArgument);
            //gen.executeQuery("Delete From tblRelativeWardDetails where rwId=" + rwid + "");
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteRelativeWardDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@rwid", rwid);
            SqlCmd.Parameters.AddWithValue("@deletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
             FillRelativeWardDetails();
        }

        if (e.CommandName == "update")
        {
           
            foreach (GridViewRow row in GRVRelative.Rows)
            {

                GRVRelative.EditIndex = -1;
                HiddenField hidrwId = (HiddenField)row.Cells[0].FindControl("hdnrwId");
                if (hidrwId != null)
                {
                    DropDownList ddrwType = (DropDownList)row.Cells[0].FindControl("ddrwType");
                    TextBox txtrWardNo = (TextBox)row.Cells[1].FindControl("txtrWardNo");
                    TextBox txtrBedNo = (TextBox)row.Cells[2].FindControl("txtrBedNo");
                    TextBox doi = (TextBox)row.Cells[3].FindControl("txtInDate");
                    TextBox doO = (TextBox)row.Cells[4].FindControl("txtOutDate");


                    strSQL = "Update tblRelativeWardDetails SET rwType=" + ddrwType.SelectedValue + ",rwNo = '" + txtrWardNo.Text + "',rwBedNo='" + txtrBedNo.Text + "', rwInDate='" + doi.Text.Trim() + "',rwOutDate='" + doO.Text.Trim() + "'";
                    strSQL += " where rwId = " + hidrwId.Value + "";
                    gen.executeQuery(strSQL);
                    GRVRelative.EditIndex = -1;
                    FillRelativeWardDetails();
                }
            }
        }
       
    }
    protected void btnPrintIPD_Click(object sender, EventArgs e)
    {
        Response.Redirect("IPDPaper.aspx",false);
    }

    protected void ddwType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddwType = (DropDownList)sender;
        GridViewRow gvr = (GridViewRow)ddwType.NamingContainer;
        DropDownList ddWardNo = (DropDownList)gvr.FindControl("ddWardNo");

        ddWardNo.Items.Clear();
        ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + ddwType.SelectedValue + "' And IsDelete = 0 ", "wmID", "wmWardNo", ddWardNo);
    }

    protected void ddWardNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddWardNo = (DropDownList)sender;
        GridViewRow gvr = (GridViewRow)ddWardNo.NamingContainer;
        DropDownList ddBedNo = (DropDownList)gvr.FindControl("ddBedNo");

        ddBedNo.Items.Clear();
        ddBedNo.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT bmNoOfBads , bmID FROM tblBadMaster WHERE wmID='" + ddWardNo.SelectedValue + "' And IsDelete = 0 ", "bmID", "bmNoOfBads", ddBedNo);
    }

}
