﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class RemedyMaster : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string userrole;
    string strSQL;
    int rmId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["RMId"] != null)
            rmId = Convert.ToInt32(Request.QueryString["RMId"].ToString());

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 8";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["flag"] == "1")
            lblMessage.Text = "Test details saved successfully";

        if (!IsPostBack)
        {
            FillData();

        }
    }

    private void FillData()
    {
        if (rmId != 0)
        {


            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "Select RMNAME from tblRemedyMaster Where RMId= " + rmId + " ";
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader sqldr = SqlCmd.ExecuteReader();
            while (sqldr.Read())
            {
                txtRemedyName.Text = sqldr["RMNAME"].ToString();
            }

            sqldr.Close();
            sqldr.Dispose();
            con.Close();
            con.Dispose();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (rmId == 0)
                strSQL = "InsRemedyMaster";
            else
                strSQL = "UptRemedyMaster";

            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@RMNAME", txtRemedyName.Text.Trim());
            SqlCmd.Parameters.AddWithValue("@CreatedBy", userid);
            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            if (rmId == 0)
            {
                SqlCmd.Parameters.Add("@RMId", SqlDbType.Int).Direction = ParameterDirection.Output;

            }
            else
            {
                SqlCmd.Parameters.AddWithValue("@RMId", rmId);
            }
            if (con.State == ConnectionState.Closed)
                con.Open();
            int reslt;
            SqlCmd.ExecuteNonQuery();
            reslt = (int)SqlCmd.Parameters["@Error"].Value;
            if (reslt == 0)
            {
                if (rmId == 0)
                {
                    Response.Redirect("RemedyMaster.aspx?flag=1", false);
                }
                else
                {
                    Response.Redirect("ManageRemedy.aspx?flag=1", false);
                }
            }

            SqlCmd.Dispose();
            con.Close();

        }
    }


}