﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class DeletedIPDReport : System.Web.UI.Page
{
    SqlCommand sqlcmd;
    int userid;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    int Page_no = 0, Page_size = 10;
    bool IsExport = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx", false);

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 53";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();
            BindGVR();
        }
    }

    protected void FillData()
    {
        DDL_User.Items.Insert(0, new ListItem("Select", "%"));
        gen.FillDropDownList("Select uId , uFname +' '+ uMname +' '+ uLname As DNAME from tblUser Where IsDelete = 0 ", "uId", "DNAME", DDL_User);
        //DDL_User.SelectedValue = Convert.ToString(userid);

    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
        ScriptManager1.RegisterPostBackControl(this.imgexport);
    }
    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        IsExport = true;
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "DeletedIPDReport.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRV1.AllowPaging = false;
        this.BindGVR();
        GRV1.HeaderRow.Style.Add("background-color", "#FFFFFF");
        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void BindGVR()
    {
        try
        {
            string[] _frm = txtfrm.Text.ToString().Split('/');
            string[] _to = txtto.Text.ToString().Split('/');

            string frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            string to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";

            sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "Sp_GetDeletedIpdReport";
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Connection = con;
            sqlcmd.Parameters.AddWithValue("@FrmDate", frm);
            sqlcmd.Parameters.AddWithValue("@ToDate", to);
            sqlcmd.Parameters.AddWithValue("@User", DDL_User.SelectedValue);
            SqlDataReader dr;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
                dr = sqlcmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                if (dt.Rows.Count > 0)
                {
                    pnlHead.Visible = true;
                    rptHead.Visible = true;
                    GRV1.DataSource = dt;
                    GRV1.DataBind();
                    export.Style.Add("display", "block");
                    createpaging(dt.Rows.Count, GRV1.PageCount);
                }
                else
                {
                    pnlHead.Visible = false;
                    rptHead.Visible = false;
                    export.Style.Add("display", "none");
                    lblGridHeader.Text = "No record found";
                }
                sqlcmd.Dispose();
            }
        }
        catch
        {
        }
    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "case")
        {
            string pdID = e.CommandArgument.ToString();
            Session["CasePatientID"] = pdID;
            //Session["ipdid"] = IpdID;
            Session["msg"] = "1";
            Response.Redirect("IPDView.aspx", false);
        }
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (IsExport == true)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkpdName = (LinkButton)e.Row.FindControl("lnkpdName");
                Literal lit = new Literal();
                lit.Text = lnkpdName.Text;
                e.Row.Cells[2].Controls.Add(lit);
                e.Row.Cells[2].Controls.Remove(lnkpdName);
            }
        }
    }
}