﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class TodaysListReport : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string strSQL = "";
    int uid = 0;
    genral gen = new genral();
    int urole = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Convert.ToInt32(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");
        
        if(!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindGRV();
        }
    }

    private void BindGRV()
    {
        
        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];
        

        strSQL = "Sp_GetTodaysListRecord";
        SqlCommand sqlcmd = new SqlCommand(strSQL, con);
        sqlcmd.CommandType = CommandType.StoredProcedure;
        SqlDataReader sqldr;
        sqlcmd.Parameters.AddWithValue("@frmDate", Frmdate);
        sqlcmd.Parameters.AddWithValue("@toDate", Todate);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            sqldr = sqlcmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(sqldr);
            if(dt.Rows.Count > 0)
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();
            }
            else
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();
            }
            sqldr.Dispose();
            con.Close();
        }
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGRV();
    }
}