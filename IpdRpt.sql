USE [PMS]
GO

/****** Object:  StoredProcedure [dbo].[Sp_GetIpdReport]    Script Date: 04/30/2013 12:21:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_GetIpdReport]   
   
 @frmdate SMALLDATETIME  
,@todate  SMALLDATETIME  
  
AS  
BEGIN   
  
CREATE TABLE #tempIpdReport  
  (   
    IpdID INT   
   ,wInDate SMALLDATETIME  
   ,wOutDate SMALLDATETIME  
   ,Name VARCHAR(100)  
   ,Wordtype VARCHAR(50)  
   ,BaseCharge DECIMAL(18,2)  
   ,Days1 INT  
   ,NATUREOFSERVICES VARCHAR(50)  
   ,FLAG INT  
   ,ECG INT 
   ,Moniter INT 
   ,Day2 INT 
   ,TotAmt DECIMAL(18,2)
  )  
  INSERT INTO #tempIpdReport(IpdID,wInDate,wOutDate,Name,Wordtype,BaseCharge,Days1,NATUREOFSERVICES,FLAG,ECG,Moniter,Day2,TotAmt)   
  SELECT   
  
    ipdId 
   ,wInDate  
   ,wOutDate  
   ,tpd.pdInitial + ' ' + tpd.pdFname + ' ' + tpd.pdMname + ' ' + tpd.pdLname 'Name'  
   ,CASE WHEN tPwd.wType = '1' THEN 'GENERAL'  
        WHEN tPwd.wType='2'THEN 'SEMI SPECIAL'  
    END Wordtype    
   ,CASE wType WHEN 1 THEN 250  
        WHEN 2 THEN 300 END 'BaseCharge'   
   ,CASE WHEN wInDate = tIpm.ipdAdmissionDate THEN CONVERT(VARCHAR,DATEDIFF(Day, wInDate,wOutDate)+1)  
    ELSE CONVERT(VARCHAR,DATEDIFF(Day, wInDate,wOutDate)) END  'Days1'  
   
   ,'IPD CHARGES'as 'NATUREOFSERVICES'   
   ,CASE WHEN wInDate=tIpm.ipdAdmissionDate THEN '1'  
    ELSE '0' END 'FLAG'   
   ,'' as  'ECG'
   ,'' as 'Moniter'    
   ,'' as 'Day'
   ,CASE wType WHEN 1 THEN (CASE WHEN wInDate = tIpm.ipdAdmissionDate THEN 250*(DATEDIFF(Day, wInDate,wOutDate)+1)
							  ELSE 250*(DATEDIFF(Day, wInDate,wOutDate)) END)		
			   WHEN 2 THEN (CASE WHEN wInDate = tIpm.ipdAdmissionDate THEN 300*(DATEDIFF(Day, wInDate,wOutDate)+1)
							  ELSE 300*(DATEDIFF(Day, wInDate,wOutDate)) END)
    END 'TotAmt'  
   
FROM   
     tblIpdMaster tIpm   
     INNER JOIN 
     tblPatientDetails tPd ON tIpm.ipdId=tPd.pdID  
     INNER JOIN   
     tblPatientsWardDetails tPwd ON tPwd.wipdId= tPd.pdID  
     
      
WHERE    
         tIpm.ipdDischargeDate IS NOT NULL
		 AND tIpm.ipdAdmissionDate BETWEEN @frmdate AND @todate  
      
ORDER BY  tIpm.ipdAdmissionDate,ipdId ASC  


    
 	  UPDATE #tempIpdReport  
	  SET 
	      ECG=itdcharges
	     
	  FROM #tempIpdReport tIr,tblIpdTestDetails tItd
	  WHERE tIr.IpdID=tItd.itdipdId AND tItd.itdtmId=1 
	  
	  
	  UPDATE #tempIpdReport  
	  SET 
	      Moniter=
	      (SELECT SUM(itdcharges) FROM tblIpdTestDetails tItd 
	       WHERE  tIr.ipdId=tItd.itdipdId AND tItd.itdtmId=5
	       GROUP BY itdipdId)
	   
	  FROM  #tempIpdReport tIr
	 
	 UPDATE #tempIpdReport  
	 SET 
	      Day2=
	      (SELECT SUM(itdHours)/24 FROM tblIpdTestDetails tItd 
	       WHERE  tIr.ipdId=tItd.itdipdId AND tItd.itdtmId=5
	       GROUP BY itdipdId)
	   
	 FROM  #tempIpdReport tIr
	 
INSERT INTO #tempIpdReport(TotAmt) 	 
	Select Sum(tmBaseCharges)  'TotAmt'
	From  tblIpdTestDetails itd INNER JOIN tblTestMaster tm ON itd.itdtmId = tm.tmId 
	inner join 	#tempIpdReport tir on tir.ipdId=itd.itdipdId
	where tir.IpdID is  null
	group by  tmBaseCharges 
	 
     SELECT * FROM #tempIpdReport  
    
     DROP TABLE #tempIpdReport   
  
 END  
--Sp_GetIpdReport '2011-04-04','2013-04-02'  
GO


