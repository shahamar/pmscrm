<%@ Page Title="ManageStock" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" 
CodeFile="ManageStock.aspx.cs" Inherits="ManageStock" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:content ID="Content1" runat="server" ContentPlaceHolderID="head">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UP1" runat="server">
<ContentTemplate>
	<table Style="Width:100%;background-color:#FFF;">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<div class="login-area margin_ten_right" style="width: 100%; text-align:center; color:White;">
                		    <h2>Manage Stock</h2>
                    		    <div class="formmenu">
                        		<div class="loginform">
                            		    <ul class="quicktabs_tabs quicktabs-style-excel">
                                		<li class="qtab-Demo first" id="li_1">
                                			<a class="qt_tab active" href="StockManager.aspx">Add New Stock</a> 
                                		</li>
                                		<li class="qtab-HTML active last" id="li_9">
                                			<a class="qt_tab active" href="javascript:void(0)">Manage Stock</a> 
                                		</li>
                            		    </ul>
					    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">                                
                                		<table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">
                                        		<tr>
                                            			<td style="text-align: center; color: Red;">
                                                			<asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                            			</td>
                                        		</tr>
							<tr>
                                           			<td class="tableh1">
                                                			<table style="width:60%;" cellspacing="5px" cellpadding="5px">
                                                    			<tr>
										<td style="width: 14%">
                                                            			<asp:CheckBox ID="chkStockPeriod" runat="server" Text="" />
                                                        			</td>
                                                        			<td>
                                                            			<asp:TextBox ID="txtfrm" runat="server" CssClass="textbox"></asp:TextBox>
                                                            			<cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                			runat="server" Enabled="True" TargetControlID="txtfrm">
                                                            			</cc1:CalendarExtender>&nbsp;
                                                            			<asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                        			</td>
                                                        			<td>To :</td>
                                                        			<td>
                                                            			<asp:TextBox ID="txtto" runat="server" CssClass="textbox"></asp:TextBox>
                                                            			<cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                		Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                            			</cc1:CalendarExtender>&nbsp;
                                                            			<asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                        			</td>
                                                        			<td>
                                                            				<asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" />
                                                        			</td>
                                                        			<td style="color: Red; text-align: right;">
                                                            			<asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                        			</td>
                                                    			</tr>
                                                			</table>
                                            			</td>
                                        		</tr>
							<tr>
                                            			<td>
                                                			<table  class="mGrid" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                    			<tr>
			                                                        <th class="head1">Company Name</th>
                                                			        <th class="head2">Product</th>
										<th Class="head3">DOR</th>
			                                                        <th class="head4">Qty</th>
                        			                                <th class="head5">Issue Stock</th>
                                                			        <th class="head5i">Received Stock</th>
			                                                        <th class="head5ii">Balance Stock</th>
                        			                                <th class="head6">Action</th>
                                                			</tr>
                                                    			<tr>
                                                        			<th class="head1" width="20%">
			                                                            <asp:TextBox ID="txtCompanyName" runat="server" class="field" Width="210px"></asp:TextBox>
                        			                                </th>
                                                			        <th class="head2" width="20%"><asp:TextBox ID="txtProduct" runat="server" class="field" Width="180px"></asp:TextBox></th>
			                                                        <th class="head3" width="18%"><%--<asp:TextBox ID="txtDOR" runat="server" class="field" Width="150px"></asp:TextBox>--%></th>
										<th class="head4" width="8%"></th>
                        			                                <th class="head5" width="8%"></th>
                        			                                <th class="head5i" width="8%">
			                                                        </th>
                        			                                <th class="head5ii" Width="8%">
                                                			        </th>
										<th class="head6" width="10%">
                                                			            <div style="display: block;">
			                                                                <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none;
                        			                                            text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click" >
                                                	        			<img src="images/filter.png" style="border:0px;"/>&nbsp; &nbsp;Filter
                                                        			        </asp:LinkButton>
			                                                            </div>
                        			                                </th>
                                                			</tr>
									<tr>
										<td id="gvcol" colspan="8" style="width:100%; text-align: center;background:#fff">
                                                					<asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
					                                                    CssClass="mGrid"  PageSize="20" ShowHeader="false" Width="100%" EmptyDataText="No records found..."
                                        					            OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
					                                                    <Columns>
                                        					                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center">
					                                                            <ItemTemplate>
                                        					                        <%# Eval("Cname")%>
					                                                            </ItemTemplate>
                                        					                </asp:TemplateField>
					                                                        <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center">
                                        					                    <ItemTemplate>
													<%# Eval("StkProductName")%>
					                                                            </ItemTemplate>
                                        					                </asp:TemplateField>
												<asp:TemplateField ItemStyle-Width="18%" ItemStyle-HorizontalAlign="center" >
													<ItemTemplate>
														<%# Eval("StkDOR","{0:dd/MM/yyyy}")%>
													</ItemTemplate>
												</asp:TemplateField>
					                                                        <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="center">
                                        					                    <ItemTemplate>
					                                                                <%# Eval("StkQty")%>
                                        					                    </ItemTemplate>
					                                                        </asp:TemplateField>
                                        					                <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="center">
					                                                            <ItemTemplate>
                                        					                        <%# Eval("StkIssueStock")%>
					                                                            </ItemTemplate>
                                        					                </asp:TemplateField>
					                                                        <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="center">
                                        					                    <ItemTemplate>
					                                                                <%# Eval("StkReceivedStock")%> 
                                        					                    </ItemTemplate>
					                                                        </asp:TemplateField>
                                        					                <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="center">
					                                                            <ItemTemplate>
                                        					                        <%# Eval("StkBalanceStock")%>
					                                                            </ItemTemplate>
                                        					                </asp:TemplateField>
												<asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center">
												    <ItemTemplate>
													<asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("StkPid") %>'
                                                                                				CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                            				<asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("StkPid") %>'
                                                                                				CommandName="del" ImageUrl="Images/delete.ico" OnClientClick="return confirm('Are you sure you want to delete this?');"
                                                                                				ToolTip="Delete" />
												    </ItemTemplate>
												</asp:TemplateField>
					                                                    </Columns>
                                        				        	</asp:GridView>
					                                            </td>									
										</tr>
                                                			</table>
                                            			</td>
                                        		</tr>
						</table>
					    </div>
					    <div class="clr"></div>
					</div>
                		    </div>
				</div>
			</td>
		</tr>
	</table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:content>