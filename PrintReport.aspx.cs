﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;


public partial class PrintReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Byte[] renderedBytes1;
        WebClient client = new WebClient();

        if (Convert.ToString(Session["PrintingEventForm"]) == "COMM_Print")
        {
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "inline;filename=" + "AllExhibitorsPro-FormaRecipt.pdf");
            //Response.AddHeader("Content-Disposition","inline:Filename="++")
            renderedBytes1 = (byte[])Session["COMMPrintingData"]; //Session["IPDPrintingData"];
            //renderedBytesGeneral
            //Response.BinaryWrite(r
            Response.BinaryWrite(renderedBytes1);
            Response.Write("AllExhibitorsPro-FormaRecipt.pdf");
            // Response.BinaryWrite(buffer);
            Response.Flush();
        }
    }
}