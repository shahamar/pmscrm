﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShiftWard.aspx.cs" Inherits="ShiftWard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Shift Ward</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
    <style type="text/css">
        .b_submit
        {
            background: url("images/b_submit.gif") no-repeat scroll 0 0 transparent;
            border: 0 none;
            display: inline-block;
            font-family: arial;
            vertical-align: middle;
            width: 67px;
            height: 32px;
        }
        
        .textbutton
        {
            color: #FFFFFF !important;
            font-size: 12px;
            font-weight: bold;
            text-align: center;
            margin-top: -8px;
        }
        .loginform
        {
            font-family: Verdana;
        }
        
        .left
        {
            float: left;
        }
        
        .formmenu
        {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #E6E6E6;
            margin-bottom: 10px;
            padding: 10px;
        }
        .tableh1
        {
            background-image: url("images/tile_back1.gif");
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-size: 10px;
            height: 18px;
            padding: 8px 12px 8px 8px;
            font-family: verdana;
            font-weight: bold;
        }
        
        textarea
        {
            border: 1px solid #A5ACB2;
            font-family: verdana;
            font-size: 12px;
            padding: 5px;
            width: 71%;
        }
        
        .style1
        {
            width: 16%;
        }
        
        .style2
        {
            width: 84%;
        }
        
        #tddischarge label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        
        .hide
        {
            display: none;
        }
        .vis
        {
            display: table-row;
        }
    </style>
</head>
<body style="background-color: #FFFFFF;">
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UP1" runat="server">
            <ContentTemplate>
                <table style="width: 100%; background-color: #FFFFFF;">
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="login-area margin_ten_right" style="width: 98%;">
                                <h2>
                                    Shift Ward</h2>
                                <div class="formmenu">
                                    <div class="loginform">
                                        <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                            style="height: auto; border: 1.5px solid #E2E2E2;">
                                            <div style="padding: 15px; height: auto;">
                                                <table style="width: 100%; border: 1px solid #EFEFEF;" cellspacing="5px" cellpadding="2px">
                                                    <tr>
                                                        <td class="style1">
                                                            Shift To Ward :
                                                        </td>
                                                        <td class="style2">
                                                            <asp:DropDownList ID="ddWard" runat="server" class="required field" Width="150px"
                                                                AutoPostBack="True" OnSelectedIndexChanged="ddWard_SelectedIndexChanged">
                                                               <%-- <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                                <asp:ListItem Text="General" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Semi-Special" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Special" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Delux" Value="4"></asp:ListItem>--%>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="valward" runat="server" ControlToValidate="ddWard"
                                                                CssClass="reqPos" ErrorMessage="Please specify Ward Type."><span class="error" 
                                                         style="margin-left:1px;">Please specify Ward Type.</span>
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style1">
                                                            In Date :
                                                        </td>
                                                        <td class="style2">
                                                            <asp:TextBox ID="txtInDate" runat="server" CssClass="required field" Width="70px"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtInDate_CalendarExtender" runat="server" Enabled="True"
                                                                Format="dd/MM/yyyy" PopupButtonID="imgInDate" TargetControlID="txtInDate">
                                                            </cc1:CalendarExtender>
                                                            <asp:Image ID="imgInDate" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                width: 20px; height: 20px;" />
                                                            <asp:RegularExpressionValidator ID="regValInDate" runat="server" ControlToValidate="txtInDate"
                                                                ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}">
                                                            <span class="error" style="margin-left:21px;">Please enter date in dd/mm/yyyy</span>    
                                                            </asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style1">
                                                            Ward No :
                                                        </td>
                                                        <td class="style2">
                                                            <asp:DropDownList ID="ddWardNo" runat="server" Width="150px" class="required field"
                                                                OnSelectedIndexChanged="ddWardNo_SelectedIndexChanged" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddWardNo"
                                                                CssClass="reqPos" ErrorMessage="Please specify Ward Type."><span class="error" 
                                                            style="margin-left:21px;">Please specify Ward No.</span>
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style1">
                                                            Bed No :
                                                        </td>
                                                        <td class="style2">
                                                            <asp:DropDownList ID="ddBedNo" runat="server" class="required field" Width="150px">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddBedNo"
                                                                CssClass="reqPos" ErrorMessage="Please specify bed No."><span class="error" 
                                                            style="margin-left:21px;">Please specify Bed No.</span>
                                                            </asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style1">
                                                            &nbsp;
                                                        </td>
                                                        <td class="style2">
                                                            <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                                Style="margin-top: 2px;" Text="Save" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>