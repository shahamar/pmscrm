﻿<%@ Page Title="User Access" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="UserAccess.aspx.cs" Inherits="UserAccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />


    <head>
        <title>User Access</title>
        <script type="text/javascript">

            function SelectAllCheckboxes(chk) {
                $('#<%=gridUserAccess.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }
        </script>
    </head>

    <%--<table width="1000" cellpadding="0" cellspacing="0">
    <tr>
      <td width="85" align="left" valign="middle" class="style8">Information</td>
      <td width="11" align="center" valign="middle"><img src="images/arrow.png" width="10" /></td>
      <td width="95" align="center" valign="middle" class="style8">User Access</td>
     
     <td width="850" align="center" valign="middle"></td>
    </tr>
  </table>--%>


    <style type="text/css">
        .mGrid1 {
            width: 100%;
            background-color: #fff;
            border: solid 1px #525252;
            border-collapse: collapse;
        }

            .mGrid1 td {
                padding: 5px;
                border: solid 1px #c1c1c1;
                color: Black;
                text-transform: uppercase;
            }

            .mGrid1 th {
                padding: 8px 5px;
                color: #595130;
                background: #e9f7bc;
                border-left: solid 1px #525252;
                font-size: 0.9em;
                text-transform: uppercase;
                font-size: 12px;
            }

            .mGrid1 .alt {
                background: #fcfcfc url(grd_alt.png) repeat-x top;
            }

            .mGrid1 .pgr {
                background: url(../images/nav-back.gif) repeat-x top;
            }

                .mGrid1 .pgr table {
                    margin: 5px 0;
                }

                .mGrid1 .pgr td {
                    border-width: 0;
                    padding: 0 6px;
                    border-left: solid 1px #666;
                    font-weight: bold;
                    color: #fff;
                    line-height: 12px;
                }

                .mGrid1 .pgr a {
                    color: #C1C1C1;
                    text-decoration: none;
                }

                    .mGrid1 .pgr a:hover {
                        color: #000;
                        text-decoration: none;
                    }
    </style>
    <style type="text/css">
        .b_submit input[type="submit"]:disabled {
            color: Black;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <center>
        <div class="formmenu">
            <div class="loginform">
                <ul class="quicktabs_tabs quicktabs-style-excel">
                    <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">User Access Master</a> </li>
                    <li class="qtab-HTML last" id="li_9"><a class="qt_tab active" href="ManageUserAccess.aspx">Manage User Access</a> </li>
                </ul>
                <div class="container box" id="main">
                    <div class="container ab">
                        <div class="mandatory">
                            <span class="required" style="color: Red;">*</span> <span>Indicate mandatory field</span>
                        </div>
                    </div>
                    <div class="form" id="div_form">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" valign="top">
                                    <table width="98%" cellpadding="0" cellspacing="0" class="form">
                                        <tr>
                                            <td colspan="6" align="left" valign="top" style="font-weight: bold; color: rgba(54, 140, 169, 0.9);">
                                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="20%" valign="top" valign="middle" class="form-group">
                                                <label for="adate" class="req">Select User Role *:</label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlUsersRole" runat="server" CssClass="field" AutoPostBack="true" OnSelectedIndexChanged="ddlUsersRole_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlUsersRole" ValidationGroup="validateProduct"
                                                    Display="Dynamic" ErrorMessage="User Role required!" CssClass="field required">
                                			<span class="error">User required!</span></asp:RequiredFieldValidator>
                                            </td>
                                            <%--<td width="25%" valign="top" class="form-group">
                                        <asp:DropDownList ID="ddlUsers" runat="server" CssClass="field" AutoPostBack="true" OnSelectedIndexChanged=
                                        "ddlUsers_OnSelectedIndexChanged">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlUsers" ValidationGroup="validateProduct"
                                            Display="Dynamic" ErrorMessage="User required!" CssClass="field required">
                                			<span class="error">User required!</span></asp:RequiredFieldValidator>
                                    </td>--%>
                                            <td width="10%" valign="top" valign="middle" class="form-group">
                                                <label for="adate" class="req">Menu Type *:</label>
                                            </td>
                                            <td width="25%" valign="top" class="form-group">
                                                <asp:DropDownList ID="ddlmenutype" runat="server" class="field" AutoPostBack="true" OnSelectedIndexChanged="ddlmenutype_OnSelectedIndexChanged">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlmenutype" ValidationGroup="validateProduct"
                                                    Display="Dynamic" ErrorMessage="Menu Type required!" CssClass="field required">
                                			<span class="error">Menu Type required!</span></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="6"><span style="margin-left: 100px;">

                                                <asp:GridView ID="gridUserAccess" CssClass="mGrid1" runat="server" Width="400px" CellPadding="5" AutoGenerateColumns="false" CellSpacing="100">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Select All" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkstat1" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />Select All
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkstat" runat="server" Checked='<%# Eval("ChkStat").ToString().Equals("False") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Page Description" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblpagetext" runat="server" Text='<%# Eval("DisplayName")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Select" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMenuId" runat="server" Text='<% #Eval("Menu_ID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="left" valign="middle">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="center" valign="top">
                                                <table width="20%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <asp:Button Style="margin-right: 5px; width: auto;"
                                                                ID="save2" runat="server" Text="Submit" CssClass="textbutton b_submit"
                                                                ValidationGroup="validateProduct" OnClick="save2_Click" />
                                                        </td>
                                                        <td align="center" valign="top">
                                                            <asp:Button ID="btnCancel" runat="server" Text="Exit"
                                                                CssClass="textbutton b_submit" OnClick="btnCancel_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="left" valign="middle">&nbsp;</td>
                                        </tr>
                                        <!--/end container box1-->

                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div style="height: 313px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </center>
</asp:Content>
