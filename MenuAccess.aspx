﻿<%@ Page Title="User Menu Access" Language="C#" MasterPageFile="~/MasterHome1.master"
    AutoEventWireup="true" CodeFile="MenuAccess.aspx.cs" Inherits="MenuAccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <style type="text/css">
        #custd.td
        {
            height: 16px;
            width: 13%;
        }
        .style1
        {
            color: black;
            font-family: Verdana;
            font-size: 9pt;
            font-weight: lighter;
            text-align: center;
        }
        .style3
        {
            color: black;
            font-family: Verdana;
            font-size: 9pt;
            font-weight: lighter;
            text-align: left;
            width: 268px;
        }
        .style5
        {
            color: black;
            font-family: Verdana;
            font-size: 9pt;
            font-weight: lighter;
            text-align: left;
            width: 121px;
        }
        
        .Compulsary
        {
            font-family: Times, serif;
            font-size: 11px;
            color: red;
            text-decoration: none;
        }
        
        #treemenu a
        {
            text-decoration: none;
            color: #3B5998;
            margin-left: 2px;
        }
        
        #treemenu a:hover
        {
            text-decoration: none;
            color: #3B5998;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        User Menu Access</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <table id="custd" cellpadding="0px" cellspacing="9px" style="width: 100%;">
                                    <tr>
                                        <td class="style1" colspan="3">
                                            User Name:
                                            <asp:Label ID="lbluname" runat="server"></asp:Label>
                                            &nbsp;and User Role :
                                            <asp:Label ID="lblurole" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hiduid" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style1" style="width: 35%">
                                        </td>
                                        <td class="style1" style="width: 30%">
                                            <div id="treemenu" style="border: 1px solid #000000; height: 400px; overflow-y: scroll;
                                                width: 350px;">
                                                <asp:TreeView ID="trvwMenuAccess" runat="server" BackColor="White" Font-Names="Verdana"
                                                    ShowCheckBoxes="All" ShowLines="True" Width="300px" Style="text-align: left;"
                                                    SkipLinkText="" onclick="OnTreeClick(event)">
                                                    <HoverNodeStyle ForeColor="#0000CC" />
                                                </asp:TreeView>
                                            </div>
                                        </td>
                                        <td class="style1" style="width: 35%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style1" colspan="3">
                                            <asp:Button ID="btnSave" runat="server" Font-Names="Verdana" OnClick="btnSave_Click"
                                                Text="Save" CssClass="NFButton" />
                                        </td>
                                    </tr>
                                </table>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
