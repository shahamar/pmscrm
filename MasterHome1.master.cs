﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;

public partial class MasterHome1 : System.Web.UI.MasterPage
{

    genral gen = new genral();
    DataTable allCategories = new DataTable();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] == null)
            Response.Redirect("Login.aspx");
        if (Session["uName"] != null)
            lbluname.Text = "Welcome  " + Session["uName"].ToString();

       
        //string strPage = Page.AppRelativeVirtualPath;
        //string output = strPage.Replace("~/", "");
        //string qrystring = Page.ClientQueryString;

        //if (qrystring == "")
        //{
        //    output = output;
        //}
        //else
        //{
        //  //  output = output + "?" + qrystring;
        //}

        //if (output.ToLower() != "MyHome.aspx")
        //{
        //    DataTable dt = new DataTable();
        //    dt = gen.UserAccess(Convert.ToInt32(Session["userid"].ToString()), output);
        //    if (dt.Rows.Count > 0)
        //    {

        //    }
        //    else
        //    {
        //        Response.Write("<script language='javascript'>window.alert('User Not Authorized to view this Page');window.location='MyHome.aspx';</script>");
        //    }

        //}
        if(!IsPostBack)
        {
            LoadCategories();
        }

    }

    protected override void Render(HtmlTextWriter writer)
    {
        string validatorOverrideScripts = "<script src=\"js/validators.js\" type=\"text/javascript\"></script>";
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ValidatorOverrideScripts", validatorOverrideScripts, false);
        base.Render(writer);

    }

    private void LoadCategories()
    {
        allCategories = GetAllCategories();
        rptMasterMenu.DataSource = GetCategories();
        rptMasterMenu.DataBind();
        rptNewCaseMenu.DataSource = GetNewCaseCategories();
        rptNewCaseMenu.DataBind();
        rptOPDMenu.DataSource = GetOPDCategories();
        rptOPDMenu.DataBind();
        rptIPDMenu.DataSource = GetIPDCategories();
        rptIPDMenu.DataBind();
        rptUtilityMenu.DataSource = GetUtilityCategories();
        rptUtilityMenu.DataBind();
    }
    private DataTable GetCategories()
    {

        SqlCommand cmd = new SqlCommand("Proc_GetUserMenu", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@RoleId", Session["urole"].ToString());
        cmd.Parameters.AddWithValue("@MenuType", 1);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        try
        {
            //con.Open();
            //SqlDataReader reader = cmd.ExecuteReader();
            //if (reader.HasRows)
            //{
            //    dt.Load(reader);
            //}
            //reader.Close();
        }
        catch (SqlException)
        {
            throw;
        }
        finally
        {
            con.Close();
        }
        return dt;
    }
    private DataTable GetNewCaseCategories()
    {
        SqlCommand cmd = new SqlCommand("Proc_GetUserMenu", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@RoleId", Session["urole"].ToString());
        cmd.Parameters.AddWithValue("@MenuType", 2);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        try
        {
            //connection.Open();
            //SqlDataReader reader = selectCommand.ExecuteReader();
            //if (reader.HasRows)
            //{
            //    dt.Load(reader);
            //}
            //reader.Close();
        }
        catch (SqlException)
        {
            throw;
        }
        finally
        {
            con.Close();
        }
        return dt;
    }
    private DataTable GetOPDCategories()
    {
        SqlCommand cmd = new SqlCommand("Proc_GetUserMenu", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@RoleId", Session["urole"].ToString());
        cmd.Parameters.AddWithValue("@MenuType", 3);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        try
        {
            //connection.Open();
            //SqlDataReader reader = selectCommand.ExecuteReader();
            //if (reader.HasRows)
            //{
            //    dt.Load(reader);
            //}
            //reader.Close();
        }
        catch (SqlException)
        {
            throw;
        }
        finally
        {
            con.Close();
        }
        return dt;
    }
    private DataTable GetIPDCategories()
    {
        SqlCommand cmd = new SqlCommand("Proc_GetUserMenu", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@RoleId", Session["urole"].ToString());
        cmd.Parameters.AddWithValue("@MenuType", 4);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        try
        {
            //connection.Open();
            //SqlDataReader reader = selectCommand.ExecuteReader();
            //if (reader.HasRows)
            //{
            //    dt.Load(reader);
            //}
            //reader.Close();
        }
        catch (SqlException)
        {
            throw;
        }
        finally
        {
            con.Close();
        }
        return dt;
    }
    private DataTable GetUtilityCategories()
    {
        SqlCommand cmd = new SqlCommand("Proc_GetUserMenu", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@RoleId", Session["urole"].ToString());
        cmd.Parameters.AddWithValue("@MenuType", 5);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        try
        {
            //connection.Open();
            //SqlDataReader reader = selectCommand.ExecuteReader();
            //if (reader.HasRows)
            //{
            //    dt.Load(reader);
            //}
            //reader.Close();
        }
        catch (SqlException)
        {
            throw;
        }
        finally
        {
            con.Close();
        }
        return dt;
    }
    private DataTable GetAllCategories()
    {
        SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
        SqlCommand selectCommand = new SqlCommand("SELECT M.Root,M.Menu_ID,M.Text,M.Path FROM tblMenu M  inner join tblMenu_Privilage MP on M.Menu_ID=MP.Menu_ID  where MP.[User_Role]= " + Session["urole"].ToString(), connection);
        DataTable dt = new DataTable();
        try
        {
            connection.Open();
            SqlDataReader reader = selectCommand.ExecuteReader();
            if (reader.HasRows)
            {
                dt.Load(reader);
            }
            reader.Close();
        }
        catch (SqlException)
        {
            throw;
        }
        finally
        {
            connection.Close();
        }
        return dt;
    }
    protected void rptMasterMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (allCategories != null)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                string ID = drv["Menu_ID"].ToString();
                DataRow[] rows = allCategories.Select("Root=" + ID, "Text");

                if (rows.Length > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    foreach (var item in rows)
                    {
                        sb.Append("<li><a href='" + item["Path"] + "'>" + item["Text"] + "</a></li>");
                    }
                    sb.Append("</ul>");
                    (e.Item.FindControl("ltrlSubMenu") as Literal).Text = sb.ToString();
                }
            }
        }
    }
    
    protected void rptNewCaseMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (allCategories != null)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                string ID = drv["Menu_ID"].ToString();
                DataRow[] rows = allCategories.Select("Root=" + ID, "Text");
                if (rows.Length > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    foreach (var item in rows)
                    {
                        sb.Append("<li><a href='" + item["Path"] + "'>" + item["Text"] + "</a></li>");
                    }
                    sb.Append("</ul>");
                    (e.Item.FindControl("ltrlNewCaseMenu") as Literal).Text = sb.ToString();
                }
            }
        }
    }

    protected void rptOPDMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (allCategories != null)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                string ID = drv["Menu_ID"].ToString();
                DataRow[] rows = allCategories.Select("Root=" + ID, "Text");
                if (rows.Length > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    foreach (var item in rows)
                    {
                        sb.Append("<li><a href='" + item["Path"] + "'>" + item["Text"] + "</a></li>");
                    }
                    sb.Append("</ul>");
                    (e.Item.FindControl("ltrlOPDMenu") as Literal).Text = sb.ToString();
                }
            }
        }
    }

    protected void rptIPDMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (allCategories != null)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                string ID = drv["Menu_ID"].ToString();
                DataRow[] rows = allCategories.Select("Root=" + ID, "Text");
                if (rows.Length > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    foreach (var item in rows)
                    {
                        sb.Append("<li><a href='" + item["Path"] + "'>" + item["Text"] + "</a></li>");
                    }
                    sb.Append("</ul>");
                    (e.Item.FindControl("ltrlIPDMenu") as Literal).Text = sb.ToString();
                }
            }
        }
    }

    protected void rptUtilityMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (allCategories != null)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                string ID = drv["Menu_ID"].ToString();
                DataRow[] rows = allCategories.Select("Root=" + ID, "Text");
                if (rows.Length > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    foreach (var item in rows)
                    {
                        sb.Append("<li><a href='" + item["Path"] + "'>" + item["Text"] + "</a></li>");
                    }
                    sb.Append("</ul>");
                    (e.Item.FindControl("ltrlUtilityMenu") as Literal).Text = sb.ToString();
                }
            }
        }
    }
}
