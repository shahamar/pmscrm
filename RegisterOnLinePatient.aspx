﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="RegisterOnLinePatient.aspx.cs" Inherits="RegisterOnLinePatient" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <%--<meta http-equiv="refresh" content="10;" />--%>
    <style type="text/css">
        .blanket {
            background-color: #111;
            opacity: 0.65;
            position: absolute;
            z-index: 9001; /*ooveeerrrr nine thoussaaaannnd*/
            top: 0px;
            left: 0px;
            width: 100%;
        }

        .popUpDiv {
            position: absolute;
            background-color: #eeeeee;
            width: 70%;
            height: auto;
            z-index: 9002; /*ooveeerrrr nine thoussaaaannnd*/
            -moz-box-shadow: inset 0 0 5px #585858;
            -webkit-box-shadow: inset 0 0 5px #585858;
        }

        .fltrt {
            float: right;
            margin-right: 10px;
        }

        .tblbox {
            height: 700px;
            width: 100%;
            margin-top: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%;" cellspacing="5px" cellpadding="0px">
        <tr>
            <td colspan="4">
                <span style="text-align: center;">
                    <h2>Online Patient Registration</h2>
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center; color: Red;">
                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkDT" runat="server" />
                From Date :
                <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox"></asp:TextBox>
                <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                    runat="server" Enabled="True" TargetControlID="txtfrm">
                </cc1:CalendarExtender>
                &nbsp;
                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
            </td>
            <td>To Date :
                <asp:TextBox ID="txtto" runat="server" CssClass="textbox"></asp:TextBox>
                <cc1:CalendarExtender ID="CalendarExtender1" Format="dd/MM/yyyy" PopupButtonID="imgto"
                    runat="server" Enabled="True" TargetControlID="txtto">
                </cc1:CalendarExtender>
                &nbsp;
                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
            </td>
            <td>
                <asp:DropDownList ID="DDL_Stat" runat="server" CssClass="required field" Width="150px">
                    <asp:ListItem Text="Non-Registered" Value="NR" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Registered" Value="R"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnView" runat="server" CssClass="textbutton b_submit" OnClick="btnView_Click"
                    Style="margin-top: 2px;" Text="View" ValidationGroup="val" />
            </td>
        </tr>
        <tr>
            <td id="Td1" align="center" style="width: 100%; text-align: center;" colspan="4">
                <center>
                    <asp:GridView ID="GVD_OnLinePat" runat="server" CssClass="mGrid" AutoGenerateColumns="false"
                        OnRowCommand="GVD_OnLinePat_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="SNO" HeaderText="SN." ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="5%" />
                            <asp:TemplateField ItemStyle-Width="40%" ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                <ItemTemplate>
                                    <%--<asp:LinkButton ID="lnkNewCasepdName" runat="server" Text='<%# Eval("pdName")%>'
                                        CommandArgument='<%# Eval("pdId") + ";" +Eval("fdId")%>' Enabled='<%# (Convert.ToBoolean(Eval("FolStat"))) %>'
                                        CommandName="NewCaseFol" Style="text-decoration: none; font-weight: normal;"
                                        ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("FStatus").ToString())%>'></asp:LinkButton>--%>

                                    <asp:LinkButton ID="lnkNewCasepdName" runat="server" Text='<%# Eval("pdName")%>'
                                        CommandArgument='<%# Eval("pdId")%>' Enabled='<%# (Convert.ToBoolean(Eval("FolStat"))) %>'
                                        CommandName="NewCaseFol" Style="text-decoration: none; font-weight: normal;"
                                        ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("FStatus").ToString())%>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:BoundField DataField="CasePaperNo" ItemStyle-HorizontalAlign="center" HeaderText="DOB" />--%>
                            <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderText="DOB">
                                <ItemTemplate>
                                    <%# Eval("pdDOB", "{0:dd-MMM-yyyy}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="State" HeaderText="State" />
                            <asp:BoundField DataField="City" HeaderText="City" />
                            <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderText="Mobile No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblpbMob" runat="server" Text='<%# Eval("pdMob")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderText="Mobile No.2">
                                <ItemTemplate>
                                    <asp:Label ID="lblpbMob2" runat="server" Text='<%# Eval("pdMobNo2")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="RegistrationFees" HeaderText="Paid Amount" />--%>
                            <asp:BoundField DataField="PaymentMode" HeaderText="Payment Mode" />
                            <%--  <asp:BoundField DataField="PaymentReferenceNo" HeaderText="Transaction Number" />--%>
                            <%-- <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%"
                                Visible="false">
                                <ItemTemplate>
                                    <%# Eval("PrintBill") %>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderStyle-Width="13%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--                                    <asp:ImageButton ID="btnNewCase" runat="server" CommandName="View" CommandArgument='<%# Eval("pdId") %>'
                                        ImageUrl="~/images/user_edit.png" ToolTip="Register Patient" Visible='<%# (Convert.ToBoolean(Eval("RegStat"))) %>' />--%>
                                    <asp:ImageButton ID="btnNewCase" runat="server" CommandName="View" CommandArgument='<%# Eval("pdId") %>'
                                        ImageUrl="~/images/user_edit.png" ToolTip="Register Patient" Visible='<%# (Convert.ToBoolean(Eval("RStatus"))) %>' />
                                    <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("pdId") %>'
                                        CommandName="del" ImageUrl="Images/delete.ico" OnClientClick="return confirm('Are you sure you want to delete this?');"
                                        ToolTip="Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </center>
            </td>
        </tr>
    </table>
</asp:Content>
