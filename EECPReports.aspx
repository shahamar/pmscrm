﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterHome1.master" CodeFile="EECPReports.aspx.cs" Inherits="EECPReports" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <link rel="stylesheet" type="text/css" href="css/radiotabstrip.css" />
    <style>
        #tbsearch td {
            text-align: left;
            vertical-align: middle;
        }

        #tbsearch label {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }

        #tbhead th {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }

        #gvcol div {
            margin-top: -10px;
        }

        .head1 {
            width: 10%;
        }

        .head2 {
            width: 18%;
        }

        .head3 {
            width: 9%;
        }

        .head4 {
            width: 10%;
        }

        .head4i {
            width: 18%;
        }

        .head5 {
            width: 10%;
        }

        .head6 {
            width: 10%;
        }

        .head7 {
            width: 18%;
        }

        .head8 {
            width: 5%;
            text-align: center;
        }

        .textbox {
            width: 70px;
            padding: 4px;
        }

        .datetime {
            position: absolute;
            width: 23px;
            height: 28px;
        }
    </style>

    <script type="text/javascript">

        function ConfirmPrint(FormDep) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Print?")) {

                confirm_value.value = "Yes";

                if (FormDep == "COMM_Print") { OPDPrintFunction(); }

                else {
                    confirm_value.value = "No";
                }
            }
            document.forms[0].appendChild(confirm_value);
        }


        function OPDPrintFunction() {


            setTimeout('DelayOPDPrintFunction()', 200);

        }

        function DelayOPDPrintFunction() {
            var myWindow = window.open("PrintReport.aspx");
            myWindow.focus();
            myWindow.print();
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <%--<asp:UpdatePanel ID="UP1" runat="server"> 
        <ContentTemplate> --%>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 100%; text-align: center">
                    <h2>Report</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <ul class="quicktabs_tabs quicktabs-style-excel">
                                <li class="qtab-HTML active first" id="li_1">
                                    <asp:LinkButton ID="lnkOpd" runat="server" CssClass="qt_tab active" Enabled="false">OPD</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_2">
                                    <asp:LinkButton ID="lnk3dccg" runat="server" CssClass="qt_tab active" PostBackUrl="~/3DCCGReport.aspx">3DCCG</asp:LinkButton></li>
                                <li class="qtab-HTML " id="li_3">
                                    <asp:LinkButton ID="lnlipd" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDReport.aspx" Visible="False">IPD</asp:LinkButton></li>
                                <li class="qtab-HTML" id="li_4">
                                    <asp:LinkButton ID="lnkConsulting" runat="server" CssClass="qt_tab active" PostBackUrl="~/ConsultingReport.aspx">Consulting</asp:LinkButton></li>
                                <li class="qtab-HTML" id="li_5">
                                    <asp:LinkButton ID="lnkPft" runat="server" CssClass="qt_tab active" PostBackUrl="~/PFTReport.aspx?flag=new">PFT</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_6">
                                    <asp:LinkButton ID="LnkReconsulting" runat="server" CssClass="qt_tab active" PostBackUrl="~/ReConsulting.aspx">Reconsulting</asp:LinkButton></li>
                                <li class="qtab-HTML" id="li_7">
                                    <asp:LinkButton ID="LnkECG" runat="server" CssClass="qt_tab active" PostBackUrl="~/ECGReport.aspx">ECG</asp:LinkButton>
                                </li>
                                <li class="qtab-Demo last" id="li_8">
                                    <asp:LinkButton ID="LikCourier" runat="server" CssClass="qt_tab active" PostBackUrl="~/CourierReport.aspx">Courier</asp:LinkButton></li>
                                <li class="qtab-Demo last" id="li_9">
                                    <asp:LinkButton ID="LnkAdvance" runat="server" CssClass="qt_tab active" PostBackUrl="~/AdvanceReport.aspx">Advance</asp:LinkButton>
                                </li>
                                <li class="qtab-Demo last" id="li_10">
                                    <asp:LinkButton ID="LnkBalance" runat="server" CssClass="qt_tab active" PostBackUrl="~/BalanceReport.aspx">Balance</asp:LinkButton>
                                </li>
                                <li class="qtab-Demo last" id="li_11">
                                    <asp:LinkButton ID="LnkPnt" runat="server" CssClass="qt_tab active" PostBackUrl="~/DeletedPatientReport.aspx" Style="display: none;">Deleted Patient</asp:LinkButton>
                                </li>
                                <li class="qtab-Demo last" id="li_12">
                                    <asp:LinkButton ID="LnkDietn" runat="server" CssClass="qt_tab active" PostBackUrl="~/DieticianReport.aspx">Dietician</asp:LinkButton>
                                </li>
                                <li class="qtab-Demo last" id="li_13">
                                    <asp:LinkButton ID="LnkDelIPD" runat="server" CssClass="qt_tab active" PostBackUrl="~/DeletedIPDReport.aspx" Style="display: none;">Deleted IPD</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_14">
                                    <asp:LinkButton ID="LnkVoucher" runat="server" CssClass="qt_tab active" PostBackUrl="~/VoucherReport.aspx">Voucher</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML last" id="li_15">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="qt_tab active" PostBackUrl="~/EECPReports.aspx">EECP</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML last" id="li_16">
                                    <asp:LinkButton ID="LnkTotal" runat="server" CssClass="qt_tab active" PostBackUrl="~/GetTotalInvoiceAmountofAllReport.aspx">Total Report</asp:LinkButton>
                                </li>
                            </ul>
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto; border: 1.5px solid #E2E2E2;">
                                <table width="100%" cellspacing="5px" cellpadding="2px">
                                    <tr>
                                        <td width="100%">
                                            <table style="width: 100%; border: 1px solid #EFEFEF;" cellspacing="5px" cellpadding="5px">
                                                <tr>
                                                    <td class="tableh1" width="100%">
                                                        <table id="tbsearch" style="width: 100%;">
                                                            <tr>
                                                                <td>Follow Up From : &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox">
                                                                    </asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                        runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                    </cc1:CalendarExtender>
                                                                    &nbsp;
                                                                                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                                </td>
                                                                <td>To : &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtto" runat="server" CssClass="textbox"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True"
                                                                        TargetControlID="txtto" Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                    </cc1:CalendarExtender>
                                                                    &nbsp;
                                                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                                </td>
                                                                <td>Prepared By : &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DDL_Doctor" runat="server" class="required field" Width="150px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>Payment Mode : &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="80px" CssClass="field">
                                                                        <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                                        <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                                        <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                                        <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                                                        <asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
                                                                        <asp:ListItem Text="All" Value="%"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td colspan="8">

                                                                    <table width="100%">

                                                                        <tr>
                                                                            <td style="width: 5%">
                                                                                <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                                                                    OnClick="btnView_Click" Style="margin-right: 5px" />
                                                                            </td>

                                                                            <%-- <td style="width: 5%">
                                                                                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="textbutton b_submit left"
                                                                                    OnClick="btnPrint_Click" OnClientClick="ConfirmPrint('COMM_Print');" />
                                                                            </td>--%>

                                                                            <td style="width: 2%">
                                                                                <div id="export" runat="server" style="float: right; display: none;">
                                                                                    <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" Height="27"
                                                                                        />
                                                                                </div>
                                                                            </td>

                                                                            <td style="color: Red; text-align: right;">
                                                                                <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>


                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Panel ID="pnlHead" runat="server" Visible="false">
                                    <div id="rptHead" runat="server" visible="false">
                                        <table width="100%" id="rounded-corner-report">
                                            <tr>
                                                <td align="center">
                                                    <strong>Aditya Homoeopathic Hospital & Healing Center</strong><br />
                                                    Pimpri Gaon, Pune - 411 017.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" class="rptname">EECP Report</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td id="gvcol">
                                                    <asp:GridView runat="server" ID="GRV1" AutoGenerateColumns="false" CssClass="mGrid" AllowPaging="True" AllowSorting="true"
                                                        PagerStyle-CssClass="pgr" PageSize="20"
                                                        Width="100%" >
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="Prepared By" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblDoctor" Text='<%#Eval("Doctor")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Test Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lbldate" Text='<%#Eval("ptdFromDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                      

                                                            <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%#Eval("Name")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Case Paper No" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%#Eval("pdCasePaperNo")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> 
                                                              <asp:TemplateField HeaderText="Payment" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblDoctor" Text='<%#Eval("ptdCharges")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          
                                                            <asp:TemplateField HeaderText="Payment Mode" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%#Eval("Payment_Mode")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          

                                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                                                                <ItemTemplate>
                                                                    <%# Eval("PrintBill") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                          <%--  <tr>
                                                <td colspan="10">
                                                    <table width="100%" cellspacing="0" border="1" class="mGrid">
                                                        <tr>
                                                            <td style="width: 75%">
                                                                <b>
                                                                    <asp:Label ID="Label1" Font="Bold" runat="server" Text="Sub Payment"></asp:Label></b>
                                                            </td>
                                                            <td style="width: 25%" align="right">
                                                                <b>
                                                                    <asp:Label ID="lblSubTotal" runat="server" Text="0.00"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 75%"><b>Total Payment</b></td>
                                                            <td style="width: 25%" align="right">
                                                                <b>
                                                                    <asp:Label ID="lblGrandTotal" runat="server" Text="0.00"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 75%"><b>Total Balance</b></td>
                                                            <td style="width: 25%" align="right">
                                                                <b>
                                                                    <asp:Label ID="lblBalTotal" runat="server" Text="0.00"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>--%>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>

    <asp:Panel BorderStyle="Outset" BorderWidth="5px" BorderColor="#C4D4B6" ID="PNLWorkSheet" runat="server" Visible="false">
        <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right" ImageUrl="~/Images/FancyClose.png" />
        <asp:Panel ID="pnl1" runat="server" ScrollBars="Auto" Width="945px">
            <CR:CrystalReportViewer ID="CrystalReportViewer2" runat="server" AutoDataBind="True" Height="500px"
                EnableParameterPrompt="False" ReuseParameterValuesOnRefresh="True" ToolPanelView="None" GroupTreeImagesFolderUrl=""
                ToolbarImagesFolderUrl="" ToolPanelWidth="200px" EnableDatabaseLogonPrompt="False" />
            <CR:CrystalReportSource ID="CrystalReportSource2" runat="server">
                <Report FileName="~/Reports/OpdReportAks.rpt"></Report>
            </CR:CrystalReportSource>
        </asp:Panel>
    </asp:Panel>
    <%-- </ContentTemplate> 
    </asp:UpdatePanel> --%>
</asp:Content>
