﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class AddDoctor : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string userrole;
    string strSQL;
    int did = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["did"] != null)
            did = Convert.ToInt32(Request.QueryString["did"].ToString());

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 14";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["flag"] == "1")
            lblMessage.Text = "Doctor details saved successfully";

        if (!IsPostBack)
        {
            txtDOJ.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
           // txtDOB.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();
        }
    }
    private void FillData()
    {
        if (did != 0)
        {

            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "Select * from tblDoctorMaster Where did= " + did + " ";
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader sqldr = SqlCmd.ExecuteReader();
            while (sqldr.Read())
            {
                txtfname.Text = sqldr["dFName"].ToString();
                txtmname.Text = sqldr["dMName"].ToString();
                txtlname.Text = sqldr["dLName"].ToString();
                txttele.Text = sqldr["dContactNo"].ToString();
                txtmob.Text = sqldr["dMobileNo"].ToString();

                txtDOJ.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(sqldr["dDOJ"].ToString()));
                txtAddress1.Text = sqldr["dAddressLine1"].ToString();
                txtAddress2.Text = sqldr["dAddressLine2"].ToString();
                DDType.SelectedValue = sqldr["dType"].ToString();
                txtEmailid.Text = sqldr["dEmailId"].ToString();
             
                if (sqldr["DOB"].ToString() != "NULL" || sqldr["DOB"].ToString() != "")
                {
                    txtDOB.Text = sqldr["DOB"] != DBNull.Value ? string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(sqldr["DOB"].ToString())) : "";
                }
            }

            sqldr.Close();
            sqldr.Dispose();
            con.Close();
            con.Dispose();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (did == 0)
                strSQL = "Ins_DoctorMaster";
            else
                strSQL = "Upt_DoctorMaster";

            string[] _doj = txtDOJ.Text.ToString().Split('/');
            string doj = _doj[2] + "/" + _doj[1] + "/" + _doj[0];

            //string[] _dob = txtDOB.Text.ToString().Split('/');
            //string dob = _dob[2] + "/" + _dob[1] + "/" + _dob[0];

            string dob;
            if (txtDOB.Text != "")
            {
                string[] _dob = txtDOB.Text.ToString().Split('/');
                dob = _dob[2] + "/" + _dob[1] + "/" + _dob[0];
            }
            else
            {
                dob = "";

            }

            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@dFName", txtfname.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@dMName", txtmname.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@dLName", txtlname.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@dContactNo", txttele.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@dMobileNo", txtmob.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@dCreatedBy", userid);

            SqlCmd.Parameters.AddWithValue("@dDOJ", doj);
            SqlCmd.Parameters.AddWithValue("@dAddressLine1", txtAddress1.Text.Trim());
            SqlCmd.Parameters.AddWithValue("@dAddressLine2", txtAddress2.Text.Trim());
            SqlCmd.Parameters.AddWithValue("@dType", DDType.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@dEmailId", txtEmailid.Text.Trim());
            SqlCmd.Parameters.AddWithValue("@dDOB", dob);

            if (con.State == ConnectionState.Closed)
                con.Open();

            if (did != 0)
            {
                SqlCmd.Parameters.AddWithValue("@did", did);
            }

            SqlCmd.ExecuteNonQuery();
            if (did == 0)
            {

                Response.Redirect("AddDoctor.aspx?flag=1");
            }
            else
            {
                Response.Redirect("ManageDoctor.aspx?flag=1");
            }
            SqlCmd.Dispose();
            con.Close();
        }
    }
}
