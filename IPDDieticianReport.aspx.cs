﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Drawing;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class IPDDieticianReport : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string RepType = "";
    string frm, to;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (!IsPostBack)
        {
            Bind_All_Patient();
        }

        if (!(Session["Report"] as string == "{CrystalDecisions.CrystalReports.Engine.ReportDocument}"))
        {
            CrystalReportViewer2.ReportSource = (ReportDocument)Session["Report"];
        }

    }

    private void Bind_All_Patient()
    
    {
        StrSQL = "Proc_GetDiaticianReport";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_PatientList.DataSource = dt;
                GVD_PatientList.DataBind();
            }
            else
            {
                GVD_PatientList.DataSource = null;
                GVD_PatientList.DataBind();
            }
        }

    }

    protected void GVD_PatientList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string pdid = "";
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            pdid = rowView["pdId"].ToString();

            Label lblkco = (Label)e.Row.FindControl("lblkco");
            Label lblDietGiven = (Label)e.Row.FindControl("lblDietGiven");
            Label lblDiet = (Label)e.Row.FindControl("lblDiet");
            LinkButton lnkpdName = (LinkButton)e.Row.FindControl("lnkpdName");

            if (File.Exists(Server.MapPath("Files/KCO/kco_" + pdid)))
            {
                lblkco.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + pdid).ToString());
            }
            else
            {
                lblkco.Text = "";
            }

            if (lblDietGiven.Text == "YES" || lblDiet.Text == "Not-Req" )
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    cell.ForeColor = Color.Black;
                    lnkpdName.ForeColor = Color.Black;
                }
            }
            else if (lblDiet.Text == "Req")
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    cell.ForeColor = Color.Red;
                    lnkpdName.ForeColor = Color.Red;
                }
            }
            else
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    cell.ForeColor = Color.DarkGreen;
                    lnkpdName.ForeColor = Color.DarkGreen;
                }
            }
        }
    }

    protected void GVD_PatientList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVD_PatientList.PageIndex = e.NewPageIndex;
        Bind_All_Patient();
    }
    protected void GVD_PatientList_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "IPDDetails")
        {
            try
            {
               
                string pdid = "";
                string ipdid = "";

                ipdid = e.CommandArgument.ToString();

                LinkButton lnkpdName = (LinkButton)e.CommandSource;
                GridViewRow myRow = (GridViewRow)lnkpdName.Parent.Parent;  
                GridView myGrid = (GridView)sender;
                pdid = myGrid.DataKeys[myRow.RowIndex].Value.ToString();

                Session["ipdid"] = ipdid;
                Session["CasePatientID"] = pdid;

                Response.Redirect("AddIPDRound.aspx?DISPROW=IR", false);

            }
            catch
            { }
        }

        if (e.CommandName == "Print")
        {
            try
            {
                string pdid = "";
                string ipdid = "";

                ipdid = e.CommandArgument.ToString();

                ImageButton btnPrint = (ImageButton)e.CommandSource;
                GridViewRow myRow = (GridViewRow)btnPrint.Parent.Parent;
                GridView myGrid = (GridView)sender;
                pdid = myGrid.DataKeys[myRow.RowIndex].Value.ToString();

                CrystalReportViewer2.Visible = true;
                string AksConn = ConfigurationManager.AppSettings["constring"];
                bindReport(AksConn, Convert.ToInt32(ipdid), pdid);
            }
            catch
            { }
        }

    }

    public void bindReport(string AksConn, int ipdid, string PdID)
    {
        DataTable dt = new DataTable();
        ReportDocument cryRpt = new ReportDocument();
        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = default(CrystalDecisions.CrystalReports.Engine.Tables);
        System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

        builder.ConnectionString = AksConn;

        string AksServer = builder["Server"] as string;
        string AksDatabase = builder["Database"] as string;
        string AksUsername = builder["Uid"] as string;
        string AksPassword = builder["Pwd"] as string;


        dt = GetReport(ipdid);

        cryRpt.Load(Server.MapPath("~/Reports/DiaticianForm.rpt"));

        CrTables = cryRpt.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        cryRpt.SetDataSource(dt);
        cryRpt.Refresh();

        string KCO = "";
        if (File.Exists(Server.MapPath("Files/KCO/kco_" + PdID)))
        {
            KCO = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + PdID).ToString());
        }

        KCO = KCO.Replace("<br />", "");
        KCO = KCO.Replace("\r", " ");
        KCO = KCO.Replace("&amp;", " ");
        //KCO = KCO.Replace("\n", " ");

        cryRpt.SetParameterValue("KCO", KCO);
        //cryRpt.SetParameterValue("ToDate", "");

        CrystalReportViewer2.ReportSourceID = "CrystalReportSource2";
        CrystalReportViewer2.ReportSource = cryRpt;

        System.IO.Stream oStream = null;
        byte[] byteArray = null;
        oStream = cryRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        byteArray = new byte[oStream.Length];

        Session.Add("COMMPrintingData", byteArray);
        Session.Add("PrintingEventForm", "COMM_Print");

        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
        Session["byteArray"] = byteArray;
        cryRpt.Close();
        cryRpt.Dispose();


    }

    public DataTable GetReport(int ipdid)
    {
        DataTable dt = new DataTable();

        StrSQL = "Proc_GetIPDDieticianForm";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@ipdid", ipdid);
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();

            dt.Load(dr);

        }

        return dt;

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Convert.ToString(Session["update"]);
    }


}