﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class ManageUser : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    int status;
    int userid;
    int Page_no = 0, Page_size = 50;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Session["urole"].ToString() != "1")
            Response.Redirect("Home.aspx?access=0");
        else
            urole = Session["urole"].ToString();

        if (Request.QueryString["flag"] == "1")
            lblMessage.Text = "User updated successfully";

        if (Request.QueryString["flag"] == "2")
            lblMessage.Text = "User deleted successfully";

        if (Request.QueryString["status"] != null)
            status = Convert.ToInt32(Request.QueryString["status"]);


        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 6";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            if (status == 2)
                lblMessage.Text = "User Access Details updated successfully";

            BindGVR();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }

    public void BindGVR()
    {
        strSQL = "Select uId, uFname+' '+ uMname +' '+ uLname As 'FullName',uContact,convert(varchar(50) , uDOB ,106) As BirthDate,uRole,CASE uRole WHEN 1 THEN 'Admin' WHEN 2 THEN 'RECEPTIONIST' WHEN 3 THEN 'OPERATOR' WHEN 4 THEN 'PHARMACIST' WHEN 5 THEN 'DOCTOR' WHEN 6 THEN 'SISTER' END _uRole,uName  From tblUser where 1=1 and IsDelete = 0";

        if (txtUName.Text.ToString() != "")
            strSQL += " AND uFname+' '+ uMname +' '+ uLname Like '%" + txtUName.Text.ToString() + "%'";

        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        createpaging(dt.Rows.Count, GRV1.PageCount);
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = 0;
        rowIndex = int.Parse(e.CommandArgument.ToString());
        string urole = (string)this.GRV1.DataKeys[rowIndex]["_uRole"].ToString();
        string uname = (string)this.GRV1.DataKeys[rowIndex]["uName"].ToString();
        string uid = (string)this.GRV1.DataKeys[rowIndex]["uId"].ToString();
        if (e.CommandName == "edt")
        {
            
            Response.Redirect("AddUser.aspx?uId=" + uid);
        }

        if (e.CommandName == "del")
        {
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteUserDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@uid", uid);
            SqlCmd.Parameters.AddWithValue("@deletedBy", userid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            Response.Redirect("ManageUser.aspx?flag=2",false);
        }

        if (e.CommandName == "acc")
        {
            Response.Redirect("MenuAccess.aspx?username=" + uname + "&userrole=" + urole + "&_userid=" + uid + "&mode=edt");
        }
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;
    }
}