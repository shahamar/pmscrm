﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class ExtraParticularMaster : System.Web.UI.Page
{
    SqlCommand Sqlcmd;
    genral gen = new genral();
    int EPID = 0;
    SqlConnection com = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string strSQL = "";
    int userid = 0;
    SqlDataReader Sqldr;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["Id"] != null)
            EPID = int.Parse(Request.QueryString["Id"].ToString());

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 17";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();

        if (!IsPostBack)
        {
            FillData();
        }

    }

    protected void FillData()
    {
        if (EPID != 0)
        {
            strSQL = "Exec  Proc_GetExtraParticularDetails " + EPID + " , Details";
            if (com.State == ConnectionState.Closed)
                com.Open();
            Sqlcmd = new SqlCommand(strSQL, com);
            Sqldr = Sqlcmd.ExecuteReader();
            while (Sqldr.Read())
            {
                txt_Name.Text = Sqldr["Name"].ToString();
                txt_charge.Text = Sqldr["Charge"].ToString();
            }
            Sqldr.Close();
            Sqlcmd.Dispose();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (com.State == ConnectionState.Closed)
                com.Open();

            if (EPID == 0)
                strSQL = "INSERT INTO tblExtraParticularMaster(EP_NAME,EP_CreatedBy,EP_Charge) VALUES (@EP_NAME,@EP_CreatedBy,@EP_Charge)";
            else
                strSQL = "UPDATE tblExtraParticularMaster SET EP_NAME = @EP_NAME , EP_ModifiedBy = @EP_CreatedBy , EP_Charge = @EP_Charge WHERE EP_ID=" + EPID + "";
            SqlCommand Sqlcmd = new SqlCommand(strSQL, com);
            Sqlcmd.CommandType = CommandType.Text;

            Sqlcmd.Parameters.AddWithValue("@EP_NAME", txt_Name.Text.ToString());
            Sqlcmd.Parameters.AddWithValue("@EP_CreatedBy", userid);
            Sqlcmd.Parameters.AddWithValue("@EP_Charge", txt_charge.Text);

            Sqlcmd.ExecuteNonQuery();

            if (EPID == 0)
                Response.Redirect("ExtraParticularMaster.aspx?msg=Extra Particular details saved successfully");
            else
                Response.Redirect("ManageExtraParticular.aspx?flag=1");

            Sqlcmd.Dispose();
            com.Close();
        }
    }

}