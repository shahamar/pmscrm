﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnlineWorkshop.aspx.cs" Inherits="OnlineWorkshop" MasterPageFile="~/MasterHome1.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <link rel="stylesheet" type="text/css" href="css/radiotabstrip.css" />
    <style>
        #tbsearch td {
            text-align: left;
            vertical-align: middle;
        }

        #tbsearch label {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }

        #tbhead th {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }

        #gvcol div {
            margin-top: -10px;
        }

        .head1 {
            width: 10%;
        }

        .head2 {
            width: 18%;
        }

        .head3 {
            width: 9%;
        }

        .head4 {
            width: 10%;
        }

        .head4i {
            width: 18%;
        }

        .head5 {
            width: 10%;
        }

        .head6 {
            width: 10%;
        }

        .head7 {
            width: 18%;
        }

        .head8 {
            width: 5%;
            text-align: center;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
        <ContentTemplate>
                <table style="width: 100%;" cellspacing="5px" cellpadding="0px">
                    <tr>
                        <td colspan="4">
                            <span style="text-align: center;">
                                <h2>Online Workshop Registration</h2>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center; color: Red;">
                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>From Date :
                <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                runat="server" Enabled="True" TargetControlID="txtfrm">
                            </cc1:CalendarExtender>
                            &nbsp;
                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                        </td>
                        <td>To Date :                            

                <asp:TextBox ID="txtto" runat="server" CssClass="textbox"></asp:TextBox>
                            <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                Format="dd/MM/yyyy" PopupButtonID="imgto">
                            </cc1:CalendarExtender>
                            &nbsp;
                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                        </td>
                        <td>
                            <asp:Button ID="btnView" runat="server" CssClass="textbutton b_submit" OnClick="btnView_Click"
                                Style="margin-top: 2px;" Text="View" ValidationGroup="val" />
                        </td>
                        <td style="width: 2%">
                            <div id="export" runat="server" style="float: right;">
                                <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" Height="27"
                                    OnClick="imgexport_Click" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="DarkRed" Font-Bold="true" Font-Size="Medium"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Panel ID="pnlHead" runat="server">

                                <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true"
                                    AutoGenerateColumns="False" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20"
                                    ShowHeader="true" Width="100%" OnRowCommand="GRV1_RowCommand"
                                    OnPageIndexChanging="GRV_Workshop_PageIndexChanging" EmptyDataText="No records found...">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center" HeaderText="S.No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblCreatedDate" Text='<%#Eval("CreatedDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Name" HeaderText="Doctors Name" />
                                        <asp:BoundField DataField="Qualification" HeaderText="Qualification" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Mobile_no" HeaderText="Mobile No." ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="PaymentReferenceNo" HeaderText="Reference No" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="Paid Amount" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblPayment" Text='<%#Eval("PaidAmt", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                       	     <ItemTemplate>
                              	<%# Eval("PrintBill") %>
				<asp:ImageButton ID="btnADD" runat="server" CommandArgument='<%# Eval("WrId") %>'
                                 CommandName="_ADDPayment" ImageUrl="images/add.png" ToolTip="Add Payment"  />
                             </ItemTemplate>
                        </asp:TemplateField>--%>
                                        <%--<asp:BoundField DataField="PaidAmt" HeaderText="Paid Amt" DataFormatString="{0:0.00}" />--%>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10">
                            <table width="100%" cellspacing="0" border="1" class="mGrid">
                                <tr>
                                    <td style="width: 75%">
                                        <b>
                                            <asp:Label ID="Label1" Font="Bold" runat="server" Text="Sub Payment"></asp:Label></b>
                                    </td>
                                    <td style="width: 25%" align="right">
                                        <b>
                                            <asp:Label ID="lblSubTotal" runat="server" Text="0.00"></asp:Label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 75%"><b>Total Payment</b></td>
                                    <td style="width: 25%" align="right">
                                        <b>
                                            <asp:Label ID="lblGrandTotal" runat="server" Text="0.00"></asp:Label></b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
        </ContentTemplate>
<%--        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="imgexport" EventName="Click" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
