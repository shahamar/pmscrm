﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.SqlClient;

public partial class UpcomingFollowUpReport : System.Web.UI.Page
{
    string strSQL;
    int Page_no = 0, Page_size = 20;
    genral gen = new genral();
    string Frmdate;
    string Todate;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGVR();
        }
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void BindGVR()
    {
        if (txtfromdate.Text != "" && txttodate.Text != "")
        {
            string[] frmdate = txtfromdate.Text.ToString().Split('/');
            string[] todate = txttodate.Text.ToString().Split('/');
            Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
            Todate = todate[2] + "/" + todate[1] + "/" + todate[0];
        }
        SqlCommand sqlcmd = new SqlCommand();
        sqlcmd.CommandText = "Proc_PatientUpcomingFollowUp";
        sqlcmd.CommandType = CommandType.StoredProcedure;
        if (txtname.Text != "")
        {
            sqlcmd.Parameters.AddWithValue("@pdName", txtname.Text);
        }
        if (txtcasepaperno.Text != "")
        {
            sqlcmd.Parameters.AddWithValue("@pdCasePaperNo", txtcasepaperno.Text);
        }
        if (txtfromdate.Text != "" && txttodate.Text != "")
        {
            sqlcmd.Parameters.AddWithValue("@Fromdate", Frmdate);
            sqlcmd.Parameters.AddWithValue("@Todate", Todate);
        }


        DataTable dt = gen.getDataTable(sqlcmd);
        if (dt.Rows.Count > 0)
        {
            GRV1.DataSource = dt;
            GRV1.DataBind();
            createpaging(dt.Rows.Count, GRV1.PageCount);
        }

        //        //string[] frmdate = txtfrm.Text.ToString().Split('/');
        //        //string[] todate = txtto.Text.ToString().Split('/');
        //        //string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        //        //string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];

        //        //        strSQL = "select (isnull(pdInitial,'')+' '+isnull(pdFname,'')+' '+isnull(pdMname,'')+' '+isnull(pdLname,'')) PdName"+" "+
        //        //",isnull(pdSex, '') pdSex,isnull(pdMob, '') pdMob,isnull(pdCasePaperNo, '') pdCasePaperNo,isnull(FD.fdNextFollowupDate, '') fdNextFollowupDate"+" "+
        //        //    "from tblPatientDetails PD inner join tblFollowUpDatails FD on PD.pdid = FD.fdPatientId where FD.fdNextFollowupDate between (select dateadd(day, -30, getdate())) and getdate()";

        //        strSQL = "select (isnull(pdInitial,'')+' '+isnull(pdFname,'')+' '+isnull(pdMname,'')+' '+isnull(pdLname,'')) PdName" + " " +
        //",isnull(pdSex, '') pdSex,isnull(pdMob, '') pdMob,isnull(pdCasePaperNo, '') pdCasePaperNo,isnull(FD.fdNextFollowupDate, '') fdNextFollowupDate" + " " +
        //   "from tblPatientDetails PD inner join tblFollowUpDatails FD on PD.pdid = FD.fdPatientId where FD.fdNextFollowupDate between DATEADD(month, DATEDIFF(month, 0, getdate()), 0) and DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0))";
        //        DataTable dt = gen.getDataTable(strSQL);
        //        GRV1.DataSource = dt;
        //        GRV1.DataBind();
        //        createpaging(dt.Rows.Count, GRV1.PageCount);
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }

    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        //Label1.Visible = false;
        //lblSubTotal.Visible = false;
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "UpcomingFollowupReport.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRV1.AllowPaging = false;
        this.BindGVR();
        //GRV1.Columns[9].Visible = false;
        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void btnsendsms_Click(object sender, EventArgs e)
    {
        string message = null;
        foreach (GridViewRow grvrow in GRV1.Rows)
        {
            CheckBox chkPdID = (CheckBox)grvrow.FindControl("chkstatID");
            Label lblpdid = (Label)grvrow.FindControl("lblpdid");
            if (chkPdID.Checked == true)
            {
                if (message == null)
                    message = lblpdid.Text;
                else
                    message = message + "," + lblpdid.Text;
            }
        }
        strSQL = "Select pdId, pdPatientICardNo,(isnull(pdInitial,' ')+' '+isnull(pdFname,' ')+' '+isnull(pdMname,' ')+' '+isnull(pdLname,' ')) AS PdName,pdCasePaperNo,ISNULL(pdMob,'') AS pdMob From dbo.tblPatientDetails where pdId in (" + message + ")";

        DataTable dt = gen.getDataTable(strSQL);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count-1; i++)
            {
                string pdName = dt.Rows[i]["PdName"].ToString();
                string pdMob = dt.Rows[i]["pdMob"].ToString();
                if (pdMob!="" || pdMob!=null)
                {
                    //string patientMsg = "Dear " + pdName + ", " + txt_message.Text.ToString();
                    //genral.GetResponse(pdMob, patientMsg);
                }
            }
        }
    }
}