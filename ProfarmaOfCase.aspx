﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="ProfarmaOfCase.aspx.cs" Inherits="ProfarmaOfCase" Title="Profarma Of Case Taking-K/C/O" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Src="tab.ascx" TagName="Tab" TagPrefix="tab1" %>
<%@ Register Src="~/tabReconsult.ascx" TagName="tabR" TagPrefix="TabR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sp_counter').text("0");
            setHeartbeat();
        });

        function setHeartbeat() {
            //alert("ok");
            //setTimeout("heartbeat()", 10000); // about to 2 minutes
            setTimeout("heartbeat()", 300000); // about to 5 minutes
        }

        function heartbeat() {
            $.get(
                '<%= ResolveUrl("Handler/SessionHeartbeat.ashx") %>',
                null,
                function () {
                    //                    //$("#heartbeat").show().fadeOut(1000); // just a little "red flash" in the corner :)
                    //alert('<%= Session["heartbeat"] %>');                    
                    //                    var cnt = $('#sp_counter').text();
                    //                    cnt = parseInt(cnt) + 5;
                    //                    $('#sp_counter').text(cnt);
                    //                    alert("ok")
                    //                    

                },
                "json"
            );
            setHeartbeat();
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Timer ID="timkco" runat="server" Interval="15000" OnTick="timkco_Tick">
    </asp:Timer>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblmsg" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%; text-align:center;">
                    <h2>
                        Profarma Of Case Taking-K/C/O</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <tab1:Tab ID="tabKCO" runat="server"></tab1:Tab>
                            <div id="div1" runat="server" style="display: none;">
                                <TabR:tabR ID="tabRKCO" runat="server" />
                            </div>
                            <ul class="quicktabs_tabs quicktabs-style-excel" style="border-left: 2px solid #E2E2E2;
                                border-right: 2px solid #E2E2E2;">
                                <li class="qtab-Demo active first" id="li_1">
                                    <asp:LinkButton ID="lnkKCO" runat="server" CssClass="qt_tab active" Enabled="False">K/C/O</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_2">
                                    <asp:LinkButton ID="lnkInvestigation" runat="server" CssClass="qt_tab active" PostBackUrl="~/Investigation.aspx">Investigation</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_3">
                                    <asp:LinkButton ID="lnkChiefCo" runat="server" CssClass="qt_tab active" PostBackUrl="~/ChiefCO.aspx">Chief C/O</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_4">
                                    <asp:LinkButton ID="lnkPastHo" runat="server" CssClass="qt_tab active" PostBackUrl="~/PastHO.aspx">Past H/o</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_5">
                                    <asp:LinkButton ID="lnkFamilyHo" runat="server" CssClass="qt_tab active" PostBackUrl="~/FamilyHo.aspx">Family H/o</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_6">
                                    <asp:LinkButton ID="lnkPhysicalgenerals" runat="server" CssClass="qt_tab active"
                                        PostBackUrl="~/PhysicalGenerals.aspx">Physical Generals</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_7">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="qt_tab active" PostBackUrl="~/Mind.aspx">Mind</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_8">
                                    <asp:LinkButton ID="lnkAf" runat="server" CssClass="qt_tab active" PostBackUrl="~/AF.aspx">A/F</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_9">
                                    <asp:LinkButton ID="lnkThermal" runat="server" CssClass="qt_tab active" PostBackUrl="~/Thermal.aspx">Thermal</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML last" id="li_10">
                                    <asp:LinkButton ID="lnkUploadDoc" runat="server" CssClass="qt-tab active" PostBackUrl="~/UploadScanDocs.aspx">Upload Docs</asp:LinkButton>
                                </li>
                            </ul>
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                <div style="height: 400px;">
                                    <cc1:Editor ID="kco" runat="server" Height="400px" />
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="timkco" EventName="Tick" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <%--<div class="kwordcontdiv" style="margin-top:21px;">
                                    <div style="text-align: center; width: 36%; float: left; margin-top: 10px;">
                                        Keywords To Search (seperated by comma) :
                                    </div>
                                    <div style="text-align: right; width: 64%; float: right;">
                                        <asp:TextBox ID="txtKco" runat="server" TextMode="MultiLine" Width="500px" Height="30px"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="savecontdiv">
                                    <asp:Button ID="btnNext" runat="server" CssClass="textbutton b_submit" Style="margin-top: 0px;"
                                        Text="Save" OnClick="btnNext_Click" />
                                </div>--%>

                                <div class="kwordcontdiv" style="margin-top:18px;">
                                <table width="100%">
                                <tr>
                                <td>
                                 <asp:Button ID="btnNext" runat="server" CssClass="textbutton b_submit" Style="margin-top: 0px;"
                                        Text="Save" OnClick="btnNext_Click" />
                                </td>
                                <td>
                                Keywords To Search (seperated by comma) :
                                </td>
                                <td>
                                 <asp:TextBox ID="txtKco" runat="server" TextMode="MultiLine" Width="470px" Height="20px"> </asp:TextBox>
                                </td>
                                </tr>
                                </table>
                                 </div>


                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
