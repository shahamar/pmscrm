﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="QuickRegistration.aspx.cs" Inherits="QuickRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

  <center>
        <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <div class="login-area margin_ten_right" style="width: 100%;">
                      	<span style="text-align:center;">
                        	<h2>Patient Registration</h2>
                        </span>
                        <div class="formmenu">
                            <div class="loginform">

                             <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">

                                <table style="width:100%; background:#f2f2f2; padding-left:20px" cellspacing="0px" cellpadding="5px">

                                       <tr>
                                            <td colspan="3" align="center">Mobile No.*</td>
                                       </tr>

                                       <tr>
                                       <td colspan="3" align="center">
                                         <asp:TextBox ID="txtmob" runat="server" CssClass="field" MaxLength="10" Width="140px"></asp:TextBox>&nbsp;
                                               <img src="images/mobile_phone.png" /><br />
                                               <asp:RegularExpressionValidator ID="regtele" runat="server" Display="Dynamic" CssClass="field required"
                                                   ControlToValidate="txtmob" ErrorMessage="Please enter valid telephone number!"
                                                   ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val">
                                                   <span class="error">Please enter valid telephone number!</span>
                                               </asp:RegularExpressionValidator>
    
                                               <asp:RegularExpressionValidator ID="regmob" runat="server" Display="Dynamic" CssClass="field required"
                                                   Style="padding-left: 140px;" ControlToValidate="txtmob" ErrorMessage="Please enter valid mobile number!"
                                                   ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val">
                                                   <span class="error">Please enter valid mobile number!</span>
                                               </asp:RegularExpressionValidator>
    
                                               <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtmob"
                                                   Display="Dynamic" ErrorMessage="Please specify Contact No." ValidationGroup="val">
                                                   <span class="error" >Please specify Contact No.</span>
                                               </asp:RequiredFieldValidator>
                                       </td>
                                        
                                       </tr>
                                    <tr>
                                         <td colspan="3" align="center">
                                         <asp:Button ID="btnSend" runat="server" CssClass="textbutton b_submit" OnClick="btnSend_Click" 
                                          Style="margin-top: 2px;" Text="Send" ValidationGroup="val" />
                                       </td>
                                           
                                    </tr>
                                       
                                </table>

                             </div>


                             <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            
        </table>
    </center>


</asp:Content>

