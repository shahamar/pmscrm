﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="AddNewReconsulting.aspx.cs" Inherits="AddNewReconsulting" Title="ReConsulting Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="Date.ascx" TagName="Date" TagPrefix="uc1" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
 <script type="text/javascript" src="js/custom-form-elements.js"></script>

    <style type="text/css">
        .style1
        {
            width: 274px;
        }
    </style>
<style>
        ._tableh1
        {
            background-image: url(    "images/tile_back1.gif" );
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
            height: 18px;
            padding: 8px 12px 8px 8px;
            text-align: center;
        }
    </style>
<script type="text/javascript">
function openinnewTab(RecID) {
        //            alert(pid)
        var win = window.open("ReConsultingBill.aspx?Id='" + RecID + "'", '_blank');
        // window.open();
        win.focus();
    }

function ConfirmPrint(FormDep) {

        var chkid = document.getElementById("<%= chk_PrintReconBill.ClientID %>");

        if (chkid.checked == true) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Print?")) {

                confirm_value.value = "Yes";

                if (FormDep == "COMM_Print")
                { OPDPrintFunction(); }

                else {
                    confirm_value.value = "No";
                }
            }
            document.forms[0].appendChild(confirm_value);

        } 
	else 
	{

        }

    }
 function OPDPrintFunction() {


        setTimeout('DelayOPDPrintFunction()', 200);

    }

function DelayOPDPrintFunction() {
        var myWindow = window.open("PrintReport.aspx");
        myWindow.focus();
        myWindow.print();
    }

</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <center>
        <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
            <tr>
                <td>
                    <div class="login-area margin_ten_right" style="width: 100%;">
                      	<span style="text-align:center;">
                        	<h2>Reconsulting</h2>
                        </span>
                        <div class="formmenu">
                            <div class="loginform">
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                    <table style="width:100%; background:#f2f2f2; padding-left:20px" cellspacing="0px" cellpadding="5px">
                                       <tr>
                                            <td colspan="5" style="text-align: center; color: Red; font-size:18px;">
                                              <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5"><br /></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-right:30px; text-align:right;">Old Case Paper No:</td>
					    <td class="style1"><asp:TextBox ID="txtoldCPN" runat="server" Enabled="false" MaxLength="50" 
                                Style="float: left; text-transform:uppercase;" Width="160px"></asp:TextBox></td>
                                            <td style="padding-right:30px; text-align:right;">Reconsulting No*:</td>
					    <td><asp:TextBox ID="txtReconsultingNo" runat="server" MaxLength="50" CssClass="field required"
                                		Style="float: left; text-transform:uppercase;" Width="160px"></asp:TextBox>
                                			<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReconsultingNo"
                                                    ValidationGroup="val" Display="Dynamic" ErrorMessage="Reconsulting no. is required!" CssClass="field required">
                                                    <span class="error">Reconsulting no. is required!</span>
                                                    </asp:RequiredFieldValidator>
                        </td>
                                        </tr>
					<tr>
						<td style="padding-right:30px; text-align:right;">Reconsulting Date:</td>
						<td class="style1"><asp:TextBox ID="txtDOR" runat="server" CssClass="field" Width="160px"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txtDOR_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgDOR" TargetControlID="txtDOR">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="imgDOR" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit;
                                                    width: 18px; height: 18px; top: 5px; left: -5px;" />
                                                    <asp:RequiredFieldValidator ID="req_DOR" runat="server" ControlToValidate="txtDOR"
                                                    ValidationGroup="val" Display="Dynamic" ErrorMessage="Reconsulting date is required!" CssClass="field required">
                                                    <span class="error">Reconsulting date is required!</span>
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regDOR" runat="server" CssClass="field required" ValidationGroup="val" Display="Dynamic" 
                                                    ControlToValidate="txtDOR" ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                                                    <span class="error">Enter valid date!</span></asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="cvDOR" runat="server" Display="Dynamic" ControlToValidate="txtDOR" ValidationGroup="val" 
                                                	CssClass="field required" ClientValidationFunction="CallDateFun" ErrorMessage="Please enter valid date!">
                                                    <span class="error">Please enter valid date!</span>
                                                </asp:CustomValidator>
						</td>
						<td style="padding-right:30px; text-align:right;">Doctors Name*:</td>
						<td><asp:DropDownList ID="DDDoctor" Width="190px"  CssClass="field" runat="server">
						</asp:DropDownList><br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DDDoctor"
                                                    Display="Dynamic" ErrorMessage="Please specify Doctor Name." ValidationGroup="val">
                                                    <span class="error">Please specify Doctor Name.</span>
                                                </asp:RequiredFieldValidator></td>
					</tr>
					<tr>
						<td style="padding-right:30px; text-align:right;">Payment Mode:</td>
						<td class="style1">
							<asp:DropDownList ID="DDL_Pay_Mode" runat="server" CssClass="field" Width="160px">
							    <asp:ListItem Text="Select" Value=""></asp:ListItem>
							    <asp:ListItem Text="IPD" Value="IPD"></asp:ListItem>
                                                    	    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>                                                   
                                                             <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                    	    <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                            <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                            <asp:ListItem Text="Free" Value="Free"></asp:ListItem>
						        </asp:DropDownList><br />
							<asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                    	Display="Dynamic" ErrorMessage="Please select Payment Mode." ValidationGroup="val">
                                                    	<span class="error">Please select Payment Mode.</span>
                                                	</asp:RequiredFieldValidator>
						</td>
						<td style="padding-right:30px; text-align:right;">Registration Fees:</td>
						<td>
							<asp:DropDownList ID="ddRegFees" runat="server" CssClass="required field" AutoPostBack="true"
                                                         Width="150px" OnSelectedIndexChanged="ddRegFees_SelectedIndexChanged">
                                                            <asp:ListItem Text="Select" Value="999"></asp:ListItem>
                                                            <asp:ListItem Text="Paid" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Unpaid" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Free" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Proxy Case" Value="3"></asp:ListItem>
                                                        </asp:DropDownList>
							<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddRegFees"
                                                    	Display="Dynamic" ErrorMessage="Please select Registration fees." ValidationGroup="val">
                                                    	<span class="error">Please select Registration fees.</span>
                                                	</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr>
						<td style="padding-right:30px; text-align:right;">Reconsulting Amount:</td>
						<td class="style1"><asp:TextBox ID="txtReconsultingAmt" runat="server" CssClass="field" Text="750" MaxLength="20" Width="150px">
						    </asp:TextBox>
						</td>
						<td></td>
						                                                  
                                                
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" align="Right"><asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                         		Style="margin-top:5px;margin-right:20px;" Text="Save" ValidationGroup="val" OnClientClick="ConfirmPrint('COMM_Print');" /></td>
						
						<td><%--<input id="chk_PrintReconBill" type="checkbox" value="ConsFlag" runat="server" onchange="AddConsultingCharge();" />--%>
						    <asp:CheckBox ID="chk_PrintReconBill" runat="server" Text = ""/>
						    <asp:Label ID="Label2" runat="server" Text = "Reconsulting Form"></asp:Label></td>
						<td><asp:CheckBox ID="chk_ReconsultingBill" runat="server" Text = "" Visible="false"/>
						    <%--<asp:Label ID="Label1" runat="server" Text = "Reconsulting Bill"></asp:Label></td>--%>
						
					</tr>
					<tr>	
						<td>&nbsp;</td>
					</tr>
					<tr>
                                                <td colspan="4">
                                                    <asp:Panel ID="pnlhis" runat="server">
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="_tableh1">
                                                                    Histroy of Reconsulting
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                        </tr>
					<tr>
                                                <td colspan="6">
						    <asp:UpdatePanel ID="UP1" runat="server">
    						    <ContentTemplate>
                                                    <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                        CssClass="mGrid" Width="100%" PageSize="10" OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Sr. No." ItemStyle-HorizontalAlign="center" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <%# Eval("rank")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Reconsulting No." ItemStyle-HorizontalAlign="center" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <%# Eval("ReconsultingNo")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
							    <asp:BoundField DataField="Doctor" HeaderText="Doctor" ItemStyle-HorizontalAlign="Center"/>
                                                            <asp:TemplateField HeaderText="Reconsulting Charges" ItemStyle-HorizontalAlign="center" ItemStyle-Width="30%">
                                                                <ItemTemplate>
                                                                    <%# Eval("ReconsultingCharges")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Reconsulting Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <%# Eval("RecDate", "{0:dd-MMM-yyyy}")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="center" HeaderText="Action">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("RecId") %>'
                                                                        CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                    <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("RecId") %>'
                                                                        CommandName="del" ImageUrl="Images/delete.ico" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete ?');" />
									<%--<%# Eval("PrintBill") %>--%>
                                                                    <a href="ReConsultingBill.aspx?Id=<%# Eval("RecId") %>" target="_blank">
                                                                        <img src="images/print1.png" /></a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
						   </ContentTemplate>
						   </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                    </table>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            
        </table>
    </center>
    
	<asp:Panel BorderStyle="Outset"  BorderWidth="5px" BorderColor="#C4D4B6" ID="PNLWorkSheet" runat="server" Visible="false">                                   
	<asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right"  ImageUrl="~/Images/FancyClose.png" />
    	<asp:Panel ID="pnl1" runat="server" ScrollBars="Auto" Width="945px">
        	<CR:CrystalReportViewer ID="CrystalReportViewer2" runat="server"  AutoDataBind="True" Height="500px" EnableParameterPrompt="False" 
            	ReuseParameterValuesOnRefresh="True" ToolPanelView="None" GroupTreeImagesFolderUrl="" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
           	EnableDatabaseLogonPrompt="False" />
        	<CR:CrystalReportSource ID="CrystalReportSource2" runat="server">
           	<Report FileName="~/Reports/OpdReportAks.rpt"></Report>
        	</CR:CrystalReportSource>
	</asp:Panel>
	</asp:Panel>
</asp:Content>
