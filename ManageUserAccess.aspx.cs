﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ManageUserAccess : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    DataTable dt;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    int userid = 0;
    string strSQL = "";
    int aId = 0;
    genral gen = new genral();
    int Page_no = 0, Page_size = 10;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Convert.ToInt32(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?msg=You are not logged in. Please login to proceed further");


        if (Session["userid"] == null)
            Response.Redirect("Login.aspx");
        if (!IsPostBack)
        {
            bindgrd();
            BindUser();
        }
    }
    public void bindgrd()
    {

        SqlCommand cmd = new SqlCommand("Proc_GetUsersDetails", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Uid",ddlUsers.SelectedValue);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            gridUser.DataSource = dt;
            gridUser.DataBind();
            createpaging(dt.Rows.Count, gridUser.PageCount,lblMsg);
        }
        else
        {
            gridUser.DataSource = null;
            gridUser.DataBind();
            createpaging(dt.Rows.Count, gridUser.PageCount,lblMsg);
        }
    }

    protected void gridUser_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName== "Access")
        {
            int roleid = Convert.ToInt32(e.CommandArgument);
            ViewState["Roleid"] = roleid;
            tr1.Visible = true;
            divAccess.Visible = true;
            bindAccessByRole(roleid);
        }
    }

    public void bindAccessByRole(int URole)
    {

        SqlCommand cmd = new SqlCommand("Proc_BindAccessRightsBYrole", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@User_Role", URole);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            grvRights.DataSource = dt;
            grvRights.DataBind();
            createpaging(dt.Rows.Count, gridUser.PageCount, lblMsg2);
        }
        else
        {
            grvRights.DataSource = null;
            grvRights.DataBind();
            createpaging(dt.Rows.Count, gridUser.PageCount, lblMsg2);
        }
    }


    protected void gridUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridUser.PageIndex = e.NewPageIndex;
        bindgrd();
    }

    protected void createpaging(int intTotalRecords, int intTotalPages, Label lblText)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblText.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

    protected void grvRights_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvRights.PageIndex = e.NewPageIndex;
        bindAccessByRole(Convert.ToInt32(ViewState["Roleid"]));
    }

    public void BindUser()
    {
        SqlDataAdapter da = new SqlDataAdapter("Proc_FillUsersAccess", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            ddlUsers.DataSource = dt;
            ddlUsers.DataTextField = "UName";
            ddlUsers.DataValueField = "uid";
            ddlUsers.DataBind();
            ddlUsers.Items.Insert(0, new ListItem("Select User", ""));
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bindgrd();
    }
}