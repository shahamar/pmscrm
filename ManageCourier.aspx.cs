﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;


public partial class ManageCourier : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole,msg;
    int userid;
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 35";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            msg = Request.QueryString["msg"].ToString();

        
        if (!IsPostBack)
        {
            if (msg == "1")
                lblMessage.Text = "Courier details updated successfully";
            if (msg == "2")
                lblMessage.Text = "Courier details deleted successfully";

            BindGVR();
        }
       
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();

    }
    public void BindGVR()
    {
        strSQL = "Select * From dbo.tblCourierMaster where 1=1";

        if (txtTest.Text.ToString() != "")
            strSQL += " AND cmCourierName Like '%" + txtTest.Text.ToString() + "%'";

        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        createpaging(dt.Rows.Count, GRV1.PageCount);
    }
    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string Id = e.CommandArgument.ToString();
        if (e.CommandName == "edt")
        {
            int aId = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("Courier.aspx?cmId=" + Id);
        }
        if (e.CommandName == "del")
        {
            //gen.executeQuery("Delete From dbo.tblCourierMaster where cmId=" + Id + "");
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteCourierMaster";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd.Parameters.AddWithValue("@cmId", Id);
            SqlCmd.Parameters.AddWithValue("@deletedBy", userid);
            SqlCmd.ExecuteNonQuery();
            con.Close();
            SqlCmd.Dispose();
            lblMessage.Text = string.Empty;
            Response.Redirect("ManageCourier.aspx?msg=2", false);
        }
        BindGVR();
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;
    }
}
