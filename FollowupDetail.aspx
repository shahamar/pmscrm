﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="FollowupDetail.aspx.cs" Inherits="FollowupDetail" Title="Followup Detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="tab1.ascx" TagName="Tab" TagPrefix="tab1" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />

    <style type="text/css">
        .reconsult {
            display: none;
        }
    </style>

    <script type="text/javascript">
        function CalculateRemedyCharges() {

            var EDStat;
            EDStat = document.getElementById("<%= chk_ExtraDoses.ClientID%>").checked;

            if (EDStat == true) {
                document.getElementById("<%= DDL_ExtranDos.ClientID %>").value = "100";
                document.getElementById("<%= ddRemedyPeriod.ClientID %>").value = "";
                document.getElementById("<%= ddDoseType.ClientID %>").value = "";
                document.getElementById("<%= DDL_AddEmg.ClientID %>").value = "0";
            } <%--
        else
        {
            document.getElementById("<%= DDL_ExtranDos.ClientID %>").value = "0";
        }--%>


            var period = document.getElementById("<%= ddRemedyPeriod.ClientID %>").value;
            var dose = document.getElementById("<%= ddDoseType.ClientID %>").value;
            var AECharges = document.getElementById("<%= DDL_AddEmg.ClientID %>").value;
            var EDCharges = document.getElementById("<%= DDL_ExtranDos.ClientID %>").value;
            var charge = 0;
            var TotalCharge = 0;
            var Newperiod = period.split('-');
            var IDS = Newperiod[0];
            var DAY = Newperiod[1];

            if (Newperiod != "") {
                SetFollowUp(DAY);
            }

        <%--
        if (period != "")
        {
            document.getElementById("<%= ddDoseType.ClientID %>").value = "3";
        }--%>

            if (IDS.trim() == "7" || IDS.trim() == "8" || IDS.trim() == "9" || IDS.trim() == "10" || IDS.trim() == "11" || IDS.trim() == "12") {

                if (dose != "") {
                    if (dose == "1") {
                        charge = 10 * DAY
                    }
                    if (dose == "2") {
                        charge = 20 * DAY
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 50 * DAY
                    }
                }
            }
            else if (IDS.trim() == "1") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 80
                    }
                    if (dose == "2") {
                        charge = 150
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 400
                    }
                }
            }

            else if (IDS.trim() == "2") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 160
                    }
                    if (dose == "2") {
                        charge = 300
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 800
                    }
                }
            }

            else if (IDS.trim() == "3") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 240
                    }
                    if (dose == "2") {
                        charge = 450
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 1200
                    }
                }
            }
            else if (IDS.trim() == "4") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 300
                    }
                    if (dose == "2") {
                        charge = 600
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 1600
                    }
                }
            }
            else if (IDS.trim() == "5") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 600
                    }
                    if (dose == "2") {
                        charge = 1200
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 3200
                    }
                }
            }
            else if (IDS.trim() == "6") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 900
                    }
                    if (dose == "2") {
                        charge = 1800
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 4800
                    }
                }
            }
            else if (IDS.trim() == "13") {
                if (dose != "") {
                    if (dose == "6") {
                        charge = 0
                        document.getElementById("<%= txtCharges.ClientID %>").value = charge;
                        document.getElementById("<%= txt_TotalCharge.ClientID %>").value = charge;
                    }
                }
            }
            else if (IDS.trim() == "13" || IDS.trim() == "22" || IDS.trim() == "23") {
                if (dose != "") {
                    charge = 0
                    TotalCharge = parseInt(AECharges) + parseInt(EDCharges)
                    document.getElementById("<%= txtCharges.ClientID %>").value = TotalCharge;
                    document.getElementById("<%= txt_TotalCharge.ClientID %>").value = TotalCharge;
                }
            }
            else if (IDS.trim() == "14") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 450
                    }
                    if (dose == "2") {
                        charge = 900
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 2400
                    }
                }
            }
            else if (IDS.trim() == "15") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 1200
                    }
                    if (dose == "2") {
                        charge = 2400
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 6400
                    }
                }
            }
            else if (IDS.trim() == "16") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 1500
                    }
                    if (dose == "2") {
                        charge = 3000
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 8000
                    }
                }
            }
            else if (IDS.trim() == "17") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 1800
                    }
                    if (dose == "2") {
                        charge = 3600
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 9600
                    }
                }
            }
            else if (IDS.trim() == "" || dose.trim() == "") {
                charge = 0
                document.getElementById("<%= txtCharges.ClientID %>").value = charge;
            }

            if (charge != 0) {
                document.getElementById("<%= txtCharges.ClientID %>").value = charge;
            }


            if (charge != 0) {

                var RemedyCharge = 0;
                RemedyCharge = document.getElementById("<%= txtCharges.ClientID %>").value;
            //                  var AECharges = 0;
            //                  AECharges = document.getElementById("<%= DDL_AddEmg.ClientID %>").value;
            //                  var EDCharges = 0;
            //                  EDCharges = document.getElementById("<%= DDL_ExtranDos.ClientID %>").value;
                TotalCharge = parseInt(RemedyCharge) + parseInt(AECharges) + parseInt(EDCharges);
                //alert(TotalCharge);
                document.getElementById("<%= txt_TotalCharge.ClientID %>").value = TotalCharge;
            }
            else if (period == 0 && dose == 0) {
                TotalCharge = parseInt(AECharges) + parseInt(EDCharges);
                document.getElementById("<%= txtCharges.ClientID %>").value = 0;
                document.getElementById("<%= txt_TotalCharge.ClientID %>").value = TotalCharge;
            }
        }

        function SetFollowUp(days) {
            var doc = document.getElementById("<%= txtDOC.ClientID %>").value;

            if (doc != "") {
                _doc = doc.split('/');
                if (_doc.length == 3) {

                    datedoc = new Date(_doc[2], _doc[1] * 1 - 1, _doc[0]);
                    followdate = new Date(datedoc.getTime() + days * 24 * 60 * 60 * 1000);
                    var _day = followdate.getDate()

                    if (_day.toString().length == 1) {
                        _day = "0" + _day;
                    }
                    var _month = parseFloat(followdate.getMonth()) + 1;

                    if (_month.toString().length == 1) {
                        _month = "0" + _month;
                    }
                    var _year = followdate.getFullYear();

                    document.getElementById("<%= txtNfollow.ClientID %>").value = _day + "/" + _month + "/" + _year;
                }
            }
        }


    <%-- function AddConsultingCharge()
        {
            //alert("I am running...")
            var chkid = document.getElementById("<%= chkIsReconsulting.ClientID %>");

            if (chkid.checked == true)
            {
                //alert("true");
                document.getElementById("<%= txtCOnCharges.ClientID %>").value = "750.00";

                document.getElementById("<%= DDL_RemList.ClientID %>").value = "";
                document.getElementById("<%= ddRemedyPeriod.ClientID %>").value = "";
                document.getElementById("<%= ddDoseType.ClientID %>").value = "";
                document.getElementById("<%= txtCharges.ClientID %>").value = "0";
                document.getElementById("<%= DDL_PatObservation.ClientID %>").value = "";
                document.getElementById("<%= DDL_AddEmg.ClientID %>").value = "";
                document.getElementById("<%= DDL_ExtranDos.ClientID %>").value = "";
                document.getElementById("<%= txt_TotalCharge.ClientID %>").value = "750.00";

            }
            else
            {
                document.getElementById("<%= txtCOnCharges.ClientID %>").value = "0.00";
            }
        }--%>
    </script>


    <script language="javascript" type="text/javascript">
        <%--function DisplayCurrior()
        {
            if (document.getElementById("<%= chkIsCourier.ClientID%>").checked == true)
            {
                document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'block';
            }
            else
            {
                document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'none';
            }
        }--%>

        function OnModeChange() {
            var com;
            com = document.getElementById("<%= chkIsCourier.ClientID%>").checked;
            if (com == true) {

                <%--document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'block';--%>

                var charges;
                charges = document.getElementById("<%= txtCharges.ClientID %>").value;
                var date;
                date = document.getElementById("<%= txtDOC.ClientID %>").value;
                //           alert(charges);
                //           alert(date);

                <%--document.getElementById("<%= txtCCharges.ClientID %>").value = charges;
                document.getElementById("<%= txtCourierDate.ClientID%>").value = date;


                ValidatorEnable(document.getElementById("<%= rcname.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcaddress.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rccharge.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDate.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDocketNo.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDDCourier.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcMobileNo.ClientID %>"), true);--%>

            }

            else {

                <%--document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'none';--%>

               <%-- ValidatorEnable(document.getElementById("<%= rcname.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcaddress.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rccharge.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDate.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDocketNo.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDDCourier.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcMobileNo.ClientID %>"), false);--%>
            }
        }


        function OnModeChangeOnline() {
            var com;
            com = document.getElementById("<%= chkIsCourier.ClientID%>").checked;
            if (com == true) {

                <%--document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'block';--%>

                var charges;
                charges = document.getElementById("<%= txtCharges.ClientID %>").value;
                var date;
                date = document.getElementById("<%= txtDOC.ClientID %>").value;
                //           alert(charges);
                //           alert(date);

                 <%--document.getElementById("<%= txtCCharges.ClientID %>").value = charges;--%>
               <%-- document.getElementById("<%= txtCourierDate.ClientID%>").value = date;


                ValidatorEnable(document.getElementById("<%= rcname.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcaddress.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rccharge.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDate.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDocketNo.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDDCourier.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcMobileNo.ClientID %>"), true);--%>

            }

            else {

                <%--document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'none';

                ValidatorEnable(document.getElementById("<%= rcname.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcaddress.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rccharge.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDate.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDocketNo.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDDCourier.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcMobileNo.ClientID %>"), false);--%>
            }
        }

    </script>

    <style type="text/css">
        .reqPos {
            position: absolute;
        }

        ._tableh1 {
            background-image: url("images/tile_back1.gif");
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
            height: 18px;
            padding: 8px 12px 8px 8px;
            text-align: center;
        }

        #rowcon label {
            display: inline;
            margin-top: 3px;
            position: absolute;
            color: #056735;
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
        }
    </style>


    <style type="text/css">
        .colmiddle {
            font-family: Times New Roman;
            border-left: 1px solid #000000;
            border-right: 1px solid #000000;
            border-top: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }

        fieldset {
            padding: 0.5em 1em;
            border: solid Red 1px;
            width: 95%;
        }

        legend {
            font-weight: bold;
            color: #056735;
            font-family: Verdana;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function checkCollectionDateAks(sender, args) {
            var x = new Date('2016-09-13');
            if (sender._selectedDate < x) {
                alert("You Can Not select A Date Less Than To 2016-09-13 Date...!!!");
                sender._selectedDate = new Date();
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }

        }

    </script>

    <%-- <script type="text/javascript">

          function ConfirmPrint(FormDep) {

              var chkid = document.getElementById("<%= chkIsReconsulting.ClientID %>");
              if (chkid.checked == true) {
                  var confirm_value = document.createElement("INPUT");
                  confirm_value.type = "hidden";
                  confirm_value.name = "confirm_value";
                  if (confirm("Do you want to Print?")) {

                      confirm_value.value = "Yes";

                      if (FormDep == "COMM_Print")
                      { OPDPrintFunction(); }

                      else {
                          confirm_value.value = "No";
                      }
                  }
                  document.forms[0].appendChild(confirm_value);

              } else {

              }

          }


          function OPDPrintFunction() {


              setTimeout('DelayOPDPrintFunction()', 200);

          }

          function DelayOPDPrintFunction() {
              var myWindow = window.open("PrintReport.aspx");
              myWindow.focus();
              myWindow.print();
          }




 </script>  --%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>

            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <span style="text-align: center;">
                                <h2>Followup Patient</h2>
                            </span>
                            <div class="formmenu">
                                <div class="loginform">
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto; border: 1.5px solid #E2E2E2;">
                                        <table style="width: 100%;" cellspacing="0px" cellpadding="2px">
                                            <tr>
                                                <td colspan="3">
                                                    <tab1:Tab ID="tabcon" runat="server" />
                                                </td>
                                            </tr>
                                            <%-- <tr>
                                                <td colspan="3" style="text-align: center; color: Red;">
                                                    &nbsp;
                                                </td>
                                            </tr>--%>

                                            <tr>
                                                <td colspan="3" class="_tableh1">
                                                    <table width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td width="70%">FOLLOWUP HISTORY </td>
                                                                <td width="30%">IPD REMEDIES</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <div style="overflow: scroll; height: 100px; border: 1; float: left; width: 70%">
                                                        <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False" Height="50px"
                                                            CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" Width="100%" OnRowDataBound="GRV1_RowDataBound"
                                                            OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Center" HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblfdDate" runat="server" Text='<%# Eval("fdDate", "{0:dd-MMM-yyyy}")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("IsPatient").ToString())%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="By">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblfdCreatedBy" runat="server" Text='<%# Eval("fdCreatedBy")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("IsPatient").ToString())%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center" HeaderText="Reconsulting/Acute" Visible="False">
                                                                    <ItemTemplate>
                                                                        <%# Eval("fdIsReconsulting")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center" HeaderText="Casstte No" Visible="false">
                                                                    <ItemTemplate>
                                                                        <%# Eval("fdCasseteNo")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center" HeaderText="Remedy">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblfdRemedy" runat="server" Text='<%# Eval("fdRemedy")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("IsPatient").ToString())%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="11%" ItemStyle-HorizontalAlign="Center" HeaderText="Period">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblfdRemedyPeriod" runat="server" Text='<%# Eval("fdRemedyPeriod")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("IsPatient").ToString())%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%" HeaderText="Dose">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblfdDoseType" runat="server" Text='<%# Eval("fdDoseType")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("IsPatient").ToString())%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>                                                              
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%" HeaderText="edt">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("Flag") %>' CausesValidation="false"
                                                                            CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                        <%--<asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("fdId") %>'
                                                                        CausesValidation="false" CommandName="del" ToolTip="Delete" ImageUrl="~/images/delete.ico"
                                                                        Style='<%# "display:" + DataBinder.Eval(Container.DataItem, "DEL") + ";" %>' />--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center" HeaderText="Modified By">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Modified")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>

                                                        </asp:GridView>
                                                    </div>
                                                    <div style="overflow-y: scroll; height: 100px; border: 1; float: right; width: 30%">
                                                        <asp:GridView ID="GVD_IPDRound" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False" Height="50px"
                                                            CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" Width="100%"
                                                            OnPageIndexChanging="GVD_IPDRound_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("irStartDate", "{0:dd-MMM-yyyy}")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center" HeaderText="Remedy">
                                                                    <ItemTemplate>
                                                                        <%# Eval("irRemedy")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>

                                                        </asp:GridView>
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                                <td>&nbsp;
                                                </td>
                                                <td>&nbsp;
                                                </td>
                                            </tr>

                                            <tr id="rowcon">

                                                <td width="25%"></td>

                                                <td width="25%"></td>

                                                <td width="50%" rowspan="15" valign="top" class="colmiddle" align="left" style="font-family: Verdana; font-size: 12px;">


                                                    <h2 style="text-align: center; margin-top: -20px; z-index: 999999; border-color: #fff">
                                                        <span class="_tableh1">Patient Details </span>
                                                    </h2>

                                                    <div style="overflow-y: scroll; height: 300px;">

                                                        <div runat="server" id="divkco" style="display: none; padding-top: 10px;">
                                                            <fieldset>
                                                                <legend>K/C/O</legend>
                                                                <div style="text-align: left;">
                                                                    <asp:Label ID="lblkco" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>

                                                        <div runat="server" id="divinv" style="display: none; padding-top: 10px;">
                                                            <fieldset>
                                                                <legend>Investigation</legend>
                                                                <div style="text-align: left;">
                                                                    <asp:Label ID="lblInv" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div runat="server" id="divchief" style="display: none; padding-top: 10px;">
                                                            <fieldset>
                                                                <legend>Chief C/O</legend>
                                                                <div style="text-align: left;">
                                                                    <asp:Label ID="lblChiefCo" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div runat="server" id="divpho" style="display: none; padding-top: 10px;">
                                                            <fieldset>
                                                                <legend>Past HO</legend>
                                                                <div style="text-align: left;">
                                                                    <asp:Label ID="lblpho" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div runat="server" id="divfho" style="display: none; padding-top: 10px;">
                                                            <fieldset>
                                                                <legend>Family HO</legend>
                                                                <div style="text-align: left;">
                                                                    <asp:Label ID="lblfho" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div runat="server" id="divphy" style="display: none; padding-top: 10px;">
                                                            <fieldset>
                                                                <legend>Physical Generals</legend>
                                                                <div style="text-align: left;">
                                                                    <asp:Label ID="lblphy" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div runat="server" id="divmnd" style="display: none; padding-top: 10px;">
                                                            <fieldset>
                                                                <legend>Mind</legend>
                                                                <div style="text-align: left;">
                                                                    <asp:Label ID="lblMind" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div runat="server" id="divAF" style="display: none; padding-top: 10px;">
                                                            <fieldset>
                                                                <legend>A/F</legend>
                                                                <div style="text-align: left;">
                                                                    <asp:Label ID="lblAF" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div runat="server" id="divThermal" style="display: none; padding-top: 10px;">
                                                            <fieldset>
                                                                <legend>Thermal</legend>
                                                                <div style="text-align: left;">
                                                                    <asp:Label ID="lblThermal" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </fieldset>
                                                        </div>


                                                    </div>

                                                </td>


                                            </tr>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>Followup Date*
                                                            </td>
                                                            <td style="padding-left: 30px;">
                                                                <input id="chkIsPatient" type="checkbox" value="ConsFlag" runat="server" checked />
                                                                IsPatient
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td>Remedy*
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtDOC" runat="server" CssClass="required field" Width="70px" onchange="CalculateRemedyCharges()"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txtDOC_CalendarExtender" runat="server" Enabled="True" OnClientDateSelectionChanged="checkCollectionDateAks"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgDOC" TargetControlID="txtDOC">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="imgDOC" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute; width: 20px; height: 20px;" />
                                                    <asp:RequiredFieldValidator ID="valDOC" runat="server" ControlToValidate="txtDOC" Display="Dynamic"
                                                        ErrorMessage="Date Of Consulting is Required." CssClass="reqPos"><span class="error" style="margin-left:21px;">Date Of Consulting is Required.</span>
                                                    </asp:RequiredFieldValidator>
                                                    <br />
                                                    <%-- <asp:RangeValidator runat="server" id="rngDate" Display="Dynamic" controltovalidate="txtDOC" type="Date" minimumvalue="13/09/2016"  maximumvalue="31/12/2200"
                                                    errormessage="Please Select Date Greater Than 13-09-2016" />--%>

                                                </td>
                                                <td>
                                                    <%-- <asp:TextBox ID="txtRemedy" runat="server" Width="250px" CssClass="required field"></asp:TextBox> --%>
                                                    <asp:DropDownList ID="DDL_RemList" runat="server" Width="170px" CssClass="required field">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="valRemedy" runat="server" ControlToValidate="DDL_RemList"
                                                        ErrorMessage="Remedy is Required." CssClass="reqPos" ValidationGroup="check"><span class="error">Remedy is Required.</span>
                                                    </asp:RequiredFieldValidator>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Remedy Period*
                                                </td>
                                                <td>Dose Type
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddRemedyPeriod" runat="server" CssClass="field" Width="170px" AutoPostBack="true"
                                                        onchange="CalculateRemedyCharges()" OnSelectedIndexChanged="ddRemedyPeriod_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RFVddRemedyPeriod" runat="server" Display="Dynamic" ControlToValidate="ddRemedyPeriod" ValidationGroup="check" ErrorMessage="Please Select Dose Type."><span class="error">Please Select Dose Type.</span></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddDoseType" runat="server" CssClass="field" Width="170px"
                                                        onchange="CalculateRemedyCharges()">
                                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Regular(3 Times/Day)" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Double(6 Times/Day)" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Hourly(10 Times/Day)" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="Liquid" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="Add Emg" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="Extra Doses" Value="6"></asp:ListItem>
                                                        <%-- <asp:ListItem Text="Free" Value="5"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RFVDoseType" runat="server" Display="Dynamic" ControlToValidate="ddDoseType" ValidationGroup="check" ErrorMessage="Please Select Dose Type."><span class="error">Please Select Dose Type.</span></asp:RequiredFieldValidator>
                                                    <%--<asp:RequiredFieldValidator ID="RFVDoseType" runat="server" ControlToValidate="ddDoseType"
                                                        ErrorMessage="Dose type is Required." CssClass="reqPos" ValidationGroup="check"><span class="error">Dose Type is Required.</span>
                                                    </asp:RequiredFieldValidator>--%>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Additional Emergency
                                                   
                                                </td>
                                                <td>Extra doses 
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="DDL_AddEmg" runat="server" Width="170px" CssClass="field"
                                                        onchange="CalculateRemedyCharges()"
                                                        OnSelectedIndexChanged="DDL_AddEmg_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="Select" Value="0" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                                        <asp:ListItem Text="200" Value="200"></asp:ListItem>
                                                        <asp:ListItem Text="300" Value="300"></asp:ListItem>
                                                        <asp:ListItem Text="400" Value="400"></asp:ListItem>
                                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                                        <asp:ListItem Text="600" Value="600"></asp:ListItem>
                                                        <asp:ListItem Text="700" Value="700"></asp:ListItem>
                                                        <asp:ListItem Text="800" Value="800"></asp:ListItem>
                                                        <asp:ListItem Text="900" Value="900"></asp:ListItem>
                                                        <asp:ListItem Text="1000" Value="1000"></asp:ListItem>
                                                    </asp:DropDownList>

                                                </td>

                                                <td>

                                                    <asp:DropDownList ID="DDL_ExtranDos" runat="server" Width="170px" CssClass="field" onchange="CalculateRemedyCharges()">
                                                        <asp:ListItem Text="Select" Value="0" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="5-100" Value="100"></asp:ListItem>
                                                        <asp:ListItem Text="10-200" Value="200"></asp:ListItem>
                                                        <asp:ListItem Text="15-300" Value="300"></asp:ListItem>
                                                        <asp:ListItem Text="20-400" Value="400"></asp:ListItem>
                                                        <asp:ListItem Text="25-500" Value="500"></asp:ListItem>
                                                    </asp:DropDownList>

                                                </td>

                                            </tr>


                                            <tr style="display: none;">
                                                <td>Remedy Charges*
                                                </td>
                                                <td>Consulting Charges*
                                                </td>
                                            </tr>


                                            <tr style="display: none;">
                                                <td>
                                                    <asp:TextBox ID="txtCharges" runat="server" CssClass="field" Width="160px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valRemedyCharge" runat="server" ControlToValidate="txtCharges" Display="Dynamic"
                                                        CssClass="reqPos" ErrorMessage="Remedy Charge is Required."><span class="error">Remedy Charge 
                                            is Required.</span>
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regremcharge" runat="server" ControlToValidate="txtCharges" Display="Dynamic"
                                                        ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                                        ValidationGroup="check">
                                            <span class="error">Please enter Only Numbers.</span>
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCOnCharges" runat="server" CssClass="field" Enabled="false" Width="160px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valccharge" runat="server" ControlToValidate="txtCOnCharges" Display="Dynamic"
                                                        ErrorMessage="Consulting Charge is Required." CssClass="reqPos"><span class="error">Consulting Charge 
                                            is Required is Required.</span>
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regconcharge" runat="server" ControlToValidate="txtCOnCharges" Display="Dynamic"
                                                        ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                                        ValidationGroup="check">
                                            <span class="error">Please enter Only Numbers.</span>
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Previous Balance
                                                </td>
                                                <td>Charges
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txt_PreviousBal" runat="server" Enabled="false" CssClass="field" Width="160px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_TotalCharge" runat="server" CssClass="field" Width="160px"></asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <%--  Reconsulting/Acute--%>  Patient Observation 
                                                </td>
                                                <td>Remark</td>

                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtReconsulting" runat="server" Visible="false" CssClass="field" Width="170px"></asp:TextBox>

                                                    <asp:TextBox ID="txtFPCassetteNo" runat="server" CssClass="field" Visible="false" Width="170px"></asp:TextBox>

                                                    <asp:DropDownList ID="DDL_PatObservation" runat="server" CssClass="required field" Width="170px">
                                                        <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Good" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Not Good" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="High" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="Low" Value="4"></asp:ListItem>
                                                    </asp:DropDownList>

                                                </td>
                                                <td valign="top">

                                                    <div style="display: none;">
                                                        <asp:TextBox ID="txtNfollow" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                                        <cc1:CalendarExtender ID="txtNfollow_CalendarExtender" runat="server" Enabled="True"
                                                            Format="dd/MM/yyyy" PopupButtonID="imgNfollow" TargetControlID="txtNfollow">
                                                        </cc1:CalendarExtender>
                                                        <asp:Image ID="imgNfollow" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute; width: 20px; height: 20px;"
                                                            Visible="false" />
                                                    </div>


                                                    <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="160px" Visible="false"
                                                        CssClass="field"
                                                        OnSelectedIndexChanged="DDL_Pay_Mode_SelectedIndexChanged">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                        <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                                        <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                        <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                        <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                        <%--<asp:ListItem Text="Other" Value="Other"></asp:ListItem>--%>
                                                        <asp:ListItem Text="Free" Value="Free"></asp:ListItem>
                                                        <asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
                                                    </asp:DropDownList>


                                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                        Display="Dynamic" ErrorMessage="Please specify Payment Mode" ValidationGroup="check">
                                                        <span class="error" >Please specify Payment Mode</span></asp:RequiredFieldValidator>--%>
                                                    <asp:TextBox ID="txtRemark" CssClass="field" runat="server"
                                                        Width="160px"></asp:TextBox>
                                                </td>

                                            </tr>
                                            <tr id="id1" runat="server" visible="false">
                                                <td>Reason for edit <span>*</span></td>
                                            </tr>
                                            <tr id="id2" runat="server" visible="false">
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlreasonforedt" Width="160px" CssClass="field">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="Call from Reception" Value="Call from Reception"></asp:ListItem>
                                                        <asp:ListItem Text="Wrong Medicine Entry" Value="Wrong Medicine Entry"></asp:ListItem>
                                                        <asp:ListItem Text="Wrong Patient Entry" Value="Wrong Patient Entry"></asp:ListItem>
                                                        <asp:ListItem Text="Patient want Changes" Value="Patient want Changes"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">

                                                    <%-- Remark --%> <%--FP Cassette No--%>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td colspan="2"></td>
                                            </tr>




                                            <tr id="rowcon">
                                                <td>
                                                    <%--<input id="chk_ExtraDoses" type="checkbox" value="ConsFlag" runat="server" onchange="CalculateRemedyCharges();" />
                                                    <label>
                                                        Is Extra Doses
                                                    </label>--%>

                                                    <input id="chkIsReconsulting" type="checkbox" value="ConsFlag" runat="server" />
                                                    <label>
                                                        Reconsulting</label>
                                                </td>

                                                <td>

                                                    <input id="chkIsCourier" type="checkbox" value="ConsFlag" runat="server" onclick="javascript: OnModeChange();" />
                                                    <label>
                                                        Is Courier</label>

                                                </td>

                                            </tr>

                                            <tr id="rowcon">
                                                <td>
                                                    <input id="chk_ExtraDoses" type="checkbox" value="ConsFlag" runat="server" />
                                                    <label>
                                                        Is Extra Doses
                                                    </label>
                                                </td>

                                                <td>
                                                    <input id="IsSeminar" type="checkbox" runat="server" />
                                                    <label>
                                                        For Seminar
                                                    </label>
                                                </td>

                                            </tr>
                                            <tr id="rowcon">
                                                <td>
                                                    <asp:CheckBox runat="server" AutoPostBack="true" ID="chk_Multiplefollowup" OnCheckedChanged="chk_Multiplefollowup_CheckedChanged" />
                                                    <label>
                                                        Add Multiple Followup
                                                    </label>
                                                </td>
                                            </tr>

                                            <tr id="tr_Bank" runat="server" style="display: none;">
                                                <td>Card Name
                                                </td>
                                                <td>Card Type
                                                </td>

                                            </tr>
                                            <tr id="tr_Cheque" runat="server" style="display: none;">
                                                <td>
                                                    <asp:TextBox ID="txtCardnm" runat="server" CssClass="field" Width="170px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="DDL_CardType" runat="server" Width="160px"
                                                        CssClass="field" AutoPostBack="true">
                                                        <asp:ListItem Text="Debit" Value="Debit" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Credit" Value="Credit"></asp:ListItem>
                                                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>

                                            </tr>
                                            <tr id="tr_Bank1" runat="server" style="display: none;">
                                                <td>Reference No.
                                                </td>
                                                <td style="display: none;">Cheque Date
                                                </td>
                                            </tr>
                                            <tr id="tr_Cheque1" runat="server" style="display: none;">
                                                <td>
                                                    <asp:TextBox ID="txtCardno" runat="server" CssClass="field" MaxLength="6" Width="170px"></asp:TextBox>
                                                </td>

                                                <td class="last">
                                                    <asp:TextBox ID="txtChqdt" Visible="false" runat="server" CssClass="field" Width="200px" placeholder="DD/MM/YYYY"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                        TargetControlID="txtChqdt">
                                                    </cc1:CalendarExtender>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="4">
                                                    <asp:Panel ID="pnlcourier" runat="server" Style="display: none">
                                                        <table cellpadding="0" cellspacing="0" width="100%" border="1px">
                                                            <tr>
                                                                <td>Receiver Name
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCName" runat="server" Width="400px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rcname" runat="server" ControlToValidate="txtCName"
                                                                        CssClass="reqPos" ErrorMessage="Receiver Name Is Required."><span class="error">Receiver Name Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Address
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCAddress" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rcaddress" runat="server" Enabled="false" ControlToValidate="txtCAddress"
                                                                        CssClass="reqPos" ErrorMessage="Address Is Required."><span class="error">Address Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>MoblieNo
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCMobileNo" runat="server" Width="200px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rcMobileNo" runat="server" Enabled="false" ControlToValidate="txtCMobileNo"
                                                                        CssClass="reqPos" ErrorMessage="Mobile No Is Required."><span class="error">Mobile No Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>EmailID
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtcEmailID" runat="server" Width="200px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Courier Name
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DDCourier" runat="server" Width="200px">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rcDDCourier" runat="server" Enabled="false" ControlToValidate="DDCourier"
                                                                        CssClass="reqPos" ErrorMessage="Courier Name Is Required."><span class="error">Courier Name Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Courier Date
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:TextBox ID="txtCourierDate" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                        PopupButtonID="imgcfollow" TargetControlID="txtCourierDate">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:Image ID="imgcfollow" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute; width: 20px; height: 20px;" />
                                                                    <asp:RequiredFieldValidator ID="rcDate" runat="server" ControlToValidate="txtCourierDate"
                                                                        CssClass="reqPos" ErrorMessage="Date Is Required." Enabled="false"><span class="error">Date Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>DocketNo
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCDocketNo" runat="server" Width="200px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rcDocketNo" runat="server" Enabled="false" ControlToValidate="txtCDocketNo"
                                                                        CssClass="reqPos" ErrorMessage="Docket No Is Required."><span class="error">Docket No Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Charges
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCCharges" runat="server" Width="200px"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="rccharge" runat="server" ControlToValidate="txtCCharges"
                                                                        ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                                                        ValidationGroup="check">
                                                                        <span class="error">Please enter Only Numbers.</span>
                                                                    </asp:RegularExpressionValidator>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td>Payment Mode *
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DDL_CourierPaymentMode" runat="server" Width="200px" CssClass="field">
                                                                        <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                                        <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                                        <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                                        <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>


                                            <tr>
                                                <td align="center" style="padding-left: 150px;">
                                                    <asp:Button ID="btnSave" runat="server" CausesValidation="false" CssClass="textbutton b_submit" OnClick="btnSave_Click" OnClientClick="ConfirmPrint('COMM_Print');"
                                                        ValidationGroup="check" Style="margin-top: 2px;" Text="Save" />

                                                </td>

                                            </tr>

                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>

            <asp:Panel BorderStyle="Outset" BorderWidth="5px" BorderColor="#C4D4B6" ID="PNLWorkSheet" runat="server" Visible="false">
                <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right" ImageUrl="~/Images/FancyClose.png" />
                <asp:Panel ID="pnl1" runat="server" ScrollBars="Auto" Width="945px">


                    <CR:CrystalReportViewer ID="CrystalReportViewer2" runat="server"
                        AutoDataBind="True" Height="500px"
                        EnableParameterPrompt="False" ReuseParameterValuesOnRefresh="True"
                        ToolPanelView="None" GroupTreeImagesFolderUrl=""
                        ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
                        EnableDatabaseLogonPrompt="False" />
                    <CR:CrystalReportSource ID="CrystalReportSource2" runat="server">
                        <Report FileName="~/Reports/OpdReportAks.rpt">
                        </Report>
                    </CR:CrystalReportSource>




                </asp:Panel>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
