USE [PMS]
GO

/****** Object:  StoredProcedure [dbo].[Sp_GetOPDReport]    Script Date: 04/17/2013 15:48:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_GetOPDReport]
(
   @frmDate SMALLDATETIME      
  ,@toDate SMALLDATETIME  
)
AS
BEGIN 
 SELECT 
			fdDate
		   ,PD.pdInitial + ' ' + PD.pdFname + ' ' + PD.pdMname + '' + PD.pdLname  'Name'
		   ,'MEDICINE' AS 'N OF S'
		   ,fdCharges
           ,ROW_NUMBER()OVER(PARTITION by  fdDate order by fdDate asc)as 'rank'
  FROM  
		  tblFollowUpDatails fud 
		  left outer JOIN 
		  tblPatientDetails pd ON fud.fdPatientId=PD.pdID
  WHERE 
		   fud.fdDate between @frmDate AND @toDate
  ORDER BY fdDate ASC
END
GO


