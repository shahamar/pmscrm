﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;


public partial class Voucher : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole, msg;
    string uid = "";
    string Source = "IPD";

    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["PdID"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["PdID"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["SC"] != null)
            Source = Request.QueryString["SC"].ToString();

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["PdID"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            else
            {
                Session["ipdid"] = Convert.ToInt32(gen.executeScalar("Select ipdId From tblIpdMaster Where ipdpdId =" + PdID).ToString());
                ViewState["ipdid"] = Session["ipdid"];
            }

            BindPatientBillingDetails();
            txt_VoucherDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
      
        string[] _IOD = txt_VoucherDate.Text.ToString().Split('/');
        string IOD = _IOD[2] + "/" + _IOD[1] + "/" + _IOD[0];

        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "proc_InsVoucherDetails";
        cmd.CommandType = CommandType.StoredProcedure;

        if (con.State == ConnectionState.Closed)
            con.Open();

        cmd.Connection = con;
        cmd.Parameters.AddWithValue("@VoucherDate", IOD.ToString());
        cmd.Parameters.AddWithValue("@ReturmAmt", txt_Amt.Text);
        cmd.Parameters.AddWithValue("@Being", txt_Being.Text);
        cmd.Parameters.AddWithValue("@FinYear", gen.GetCurrentFinYear());
        cmd.Parameters.AddWithValue("@CreatedBy", uid);
        cmd.Parameters.AddWithValue("@Source", Source);
        cmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
        if(Source == "IPD")
        {
            cmd.Parameters.AddWithValue("@IPDID", ipdid);
            cmd.Parameters.AddWithValue("@pdID", 0);
        }
        else
        {
            cmd.Parameters.AddWithValue("@IPDID", 0);
            cmd.Parameters.AddWithValue("@pdID", PdID);
        }
        cmd.ExecuteNonQuery();
        int error = (int)cmd.Parameters["@Error"].Value;

        if (error == 0)
        {
            Response.Redirect("Home.aspx");
            //if(Source == "IPD")
            //{ 
            //    Response.Redirect("VoucherReceipt.aspx?IpdId=" + ipdid);
            //}
            //else
            //{
            //    Response.Redirect("VoucherReceipt.aspx?pdId=" + PdID);
            //}
        }
    }

    public void BindPatientBillingDetails()
    {
        if (Source == "IPD")
        {
            strSQL = "Exec  sp_GenerateBill_CurrentBill " + ipdid;
            if (con.State == ConnectionState.Closed)
            con.Open();
            SqlCmd = new SqlCommand(strSQL, con);
            Sqldr = SqlCmd.ExecuteReader();
            while (Sqldr.Read())
            {
                txt_Amt.Text = Sqldr["CurrentBillAmount"].ToString();
                if (txt_Amt.Text.ToString().Contains("-"))
                {
                txt_Amt.Text = txt_Amt.Text.Replace("-", "");
                }
            }
            Sqldr.Close();
            SqlCmd.Dispose();
        }
        else 
        {
        }
    }

}