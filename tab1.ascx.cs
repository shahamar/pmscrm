﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class tab1 : System.Web.UI.UserControl
{
    //string pdId = "0";
    string strSQL = "";
    SqlDataReader Sqldr;
    SqlCommand SqlCmd;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    //added by nikita on 12th march 2015------------------------------
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-----------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
            {
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            }
            else
            {
                Response.Redirect("Home.aspx?flag=1");
            }

            FillData();
            FillIsseminar();
        }
    }

    private void FillData()
    {
        //strSQL = "Select *,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', Upper(City) As City , pdAdd1 AddS ,(select (dFName+' '+dMName+' '+dLName) AS DName From tblDoctorMaster WHERE did=dbo.tblPatientDetails.did)as DoctorName From dbo.tblPatientDetails Left Join tblCityMaster On CityID = pdcity where pdID = " + pdId;

        strSQL = "Select *,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', Upper(City) As City , pdAdd1 AddS ,ReconsultingNo,IsNull(DM.dFName,DMH.dFName) + ' ' + IsNull(DM.dMName,DMH.dMName) + ' ' + isNull(DM.dLName,DMH.dLName) AS DoctorName , * From dbo.tblPatientDetails PD Left Join tblCityMaster On CityID = pdcity Left Join tblDoctorMaster DM On PD.did = DM.did Left Join tblDoctorHistoryDetails DMH On PD.did = DMH.did left Join tblReconsultingDetails RD On PD.pdID = RD.RecPatientId where pdID = " + pdId;

        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            lblPatientName.Text = Sqldr["pdName"].ToString();
            lblPatientId.Text = Sqldr["pdPatientICardNo"].ToString();
            lblAge.Text = Sqldr["pdAge"].ToString();
            lblCasePaperNo.Text = Sqldr["pdCasePaperNo"].ToString();
            lblCassetteNo.Text = Sqldr["pdCassetteNo"].ToString();
            lblemail.Text = Sqldr["pdemail"].ToString();
            lblGender.Text = Sqldr["pdSex"].ToString();
            lblmob.Text = Sqldr["pdMob"].ToString();
            lbltele.Text = Sqldr["pdTele"].ToString();
            lblDoctorName.Text = Sqldr["DoctorName"].ToString();
            if (Sqldr["pdOccupation"] != DBNull.Value)
            {
                lblOccupation.Text = Sqldr["pdOccupation"].ToString();
            }
            if (Sqldr["ReconsultingNo"] != DBNull.Value)
            {
                lblRecNO.Text = Sqldr["ReconsultingNo"].ToString();
            }
            lblCity.Text = Sqldr["City"].ToString();
            lblAddress.Text = Sqldr["AddS"].ToString();
            lblConsDate.Text = Sqldr["pdDOC"] != DBNull.Value ? string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["pdDOC"].ToString())) : "";
        }
        Sqldr.Close();
        SqlCmd.Dispose();

        lblIPDStatus.Text = gen.executeScalar("Select [dbo].[IsIPDStatus](" + pdId + ")").ToString();

    }

    private void FillIsseminar()
    {
        //strSQL = "Select *,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', Upper(City) As City , pdAdd1 AddS ,(select (dFName+' '+dMName+' '+dLName) AS DName From tblDoctorMaster WHERE did=dbo.tblPatientDetails.did)as DoctorName From dbo.tblPatientDetails Left Join tblCityMaster On CityID = pdcity where pdID = " + pdId;

        strSQL = "select case when fdIsSeminar=1 then 'This case Is selected for Seminar' else '' end as fdIsSeminar  from tblFollowUpDatails where fdPatientId = " + pdId;

        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            lblisSeminar.Text = Sqldr["fdIsSeminar"].ToString();
        }
        Sqldr.Close();
        SqlCmd.Dispose();

    }

    protected void btnPatientDetails_Click(object sender, EventArgs e)
    {
        Session["edtPdId"] = pdId;
        Response.Redirect("AddNewPatient.aspx");
    }
}