﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditAddmissionDetails.aspx.cs" Inherits="EditAddmissionDetails" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker" TagPrefix="MKB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Addmission Details</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
    
    <style type="text/css">
    
    .b_submit {
    background: url("images/b_submit.gif") no-repeat scroll 0 0 transparent;
    border: 0 none;
    display: inline-block;
    font-family: arial;
    vertical-align: middle;
    width: 67px;
    height:32px; 
}

.textbutton 
{
    color: #FFFFFF !important;
    font-size: 12px;
    font-weight: bold;
    text-align: center;
    margin-top: -8px;
}
.loginform 
{
	font-family:Verdana; 
}

.left
{
	float:left;
}

.formmenu 
{
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #E6E6E6;
    margin-bottom: 10px;
    padding: 10px;
}
.tableh1 {
    background-image: url("images/tile_back1.gif");
    border-bottom: 1px solid #CED2D6;
    border-left: 1px solid #FFFFFF;
    border-right: 1px solid #FFFFFF;
    color: #606F79;
    font-size: 10px;
    height: 18px;
    padding: 8px 12px 8px 8px;
    font-family: verdana;
    font-weight: bold;
}

textarea {
     border: 1px solid #A5ACB2;
    font-family: verdana;
    font-size: 12px;
    padding: 5px;
    width: 71%;
}

.style1
{
	width:16%;
}

.style2
{
	width:84%;
}

#tddischarge label
{
	 display: inline;
    margin-top: 3px;
    position: absolute;
}

.hide
{
	display:none;
}
.vis
{
	display:table-row
}

#trdt label
{
	display:inline;
	color:#056735;
	font-weight:bold;
}

</style>

<script type="text/javascript">
    function ShowRow()
    {

        var status = document.getElementById("ddStatus").value;
        
        if(status == "1")
        {
            document.getElementById("trdd").className = "vis";
            document.getElementById("trdt").className = "vis";
            document.getElementById("trtreat").className = "vis";
        }      
        else
        {
            document.getElementById("trdd").className = "hide";
            document.getElementById("trdt").className = "hide";  
            document.getElementById("trtreat").className = "hide";                  
        }
    }

    function DateDiffInDays(dt1, dt2) {
        var millisecondsPerDay = 1000 * 60 * 60 * 24;
        var millisBetween = dt2.getTime() - dt1.getTime();
        var days = millisBetween / millisecondsPerDay;

        return days;
    }

    function DateDiffMoreThanThree(sender, args) {
        debugger;
        var dtPassed = args.Value.split('/')
        var DateVal = dtPassed[1] + "/" + dtPassed[0] + "/" + dtPassed[2];
        var dt1 = new Date((new Date().getMonth() + 1) + "/" + new Date().getDate() + "/" + new Date().getFullYear());

        var dt2 = new Date(DateVal);

        var days = DateDiffInDays(dt1, dt2);
        if (days > 3) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }


    }
</script>

</head>
<body style="background-color:#FFFFFF;">
    <form id="form1" runat="server">
    <div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
          <table style="width:100%; background-color:#FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                        <h2>Edit Addmission Details</h2>
                        
                        <div class="formmenu">
                            <div class="loginform">
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel" style="height:auto;border:1.5px solid #E2E2E2;">
                                    <div style="padding:15px; height:auto;">
                                        <table style="width: 100%; border: 1px solid #EFEFEF;" cellspacing="5px" cellpadding="2px">
                                            <tr>
                                                 <td class="style1">Addmission Date :</td>
                                                 <td class="style2">
                                                       <asp:TextBox ID="txtDOA" runat="server" CssClass="required field" 
                                                            Width="70px" ></asp:TextBox>
                                                        <cc1:CalendarExtender ID="txtDOA_CalendarExtender" runat="server" 
                                                            Format="dd/MM/yyyy" PopupButtonID="imgDOA" 
                                                            TargetControlID="txtDOA">
                                                        </cc1:CalendarExtender>
                                                        <asp:Image ID="imgDOA" runat="server" ImageUrl="~/images/calimg.gif" 
                                                            style="position: absolute; width:20px; height:20px;" />
                                                        <asp:RequiredFieldValidator ID="valDOC" runat="server" 
                                                            ControlToValidate="txtDOA" CssClass="reqPos" 
                                                            ErrorMessage="Addmission Date is Required."><span class="error" 
                                                            style="margin-left:21px;">Addmission Date is Required.</span>
                                                        </asp:RequiredFieldValidator>
                                                        
                                                        <asp:RegularExpressionValidator ID="regDOA" runat="server" 
                                                            ControlToValidate="txtDOA" ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" >
                                                            <span class="error" style="margin-left:21px;">Please enter date in dd/mm/yyyy</span>    
                                                        </asp:RegularExpressionValidator>    
                                                 </td>
                                             </tr>
                                            <tr>
                                                 <td class="style1">Addmission Time :</td>
                                                 <td class="style2">
                                                     <MKB:TimeSelector ID="txtTOA" runat="server">
                                                     </MKB:TimeSelector>
                                                 </td>
                                             </tr>                                             
                                            <tr>
                                                 <td class="style1">Ref By :</td>
                                                 <td class="style2">
                                                     <asp:TextBox ID="txtRefBy" runat="server" Width="250px"></asp:TextBox>
                                                 </td>
                                             </tr>  
                                              <tr>
                                                 <td class="style1">Prepared By :</td>
                                                 <td class="style2">
                                                     <asp:TextBox ID="txtPrepared" runat="server" Width="250px"></asp:TextBox>
                                                 </td>
                                             </tr>  
                                              <tr>
                                                 <td class="style1">Checked By :</td>
                                                 <td class="style2">
                                                     <asp:TextBox ID="txtChecked" runat="server" Width="250px"></asp:TextBox>
                                                 </td>
                                             </tr>                                             
                                            <tr>
                                                 <td class="style1">Discharge Patient :</td>
                                                 <td class="style2">
                                                     <asp:DropDownList ID="ddStatus" runat="server" Width="60px" CssClass="required field" onchange="ShowRow()">
                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                     </asp:DropDownList>
                                                 </td>
                                             </tr>
                                            <tr id = "trtreat" class="hide">
                                                 <td class="style1">Treatment :</td>
                                                 <td class="style2">
                                                     <asp:TextBox ID="txttreatment" runat="server" Width="250px" TextMode="MultiLine"></asp:TextBox>
                                                 </td>
                                             </tr>
                                            <tr id = "trdd" class="hide">
                                                 <td class="style1">Discharge Date :</td>
                                                 <td class="style2">
                                                    <asp:TextBox ID="txtDOD" runat="server" CssClass="required field" 
                                                             Width="70px"></asp:TextBox>
                                                        <cc1:CalendarExtender ID="txtDOD_CalendarExtender" runat="server" 
                                                            Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgDOD" 
                                                            TargetControlID="txtDOD">
                                                        </cc1:CalendarExtender>
                                                        <asp:Image ID="imgDOD" runat="server" ImageUrl="~/images/calimg.gif" 
                                                            style="position: absolute; width:20px; height:20px;" />
                                                        
                                                        <asp:RegularExpressionValidator ID="regValDOD" runat="server" Display="Dynamic" 
                                                            ControlToValidate="txtDOD" ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" >
                                                            <span class="error" style="margin-left:21px;">Please enter date in dd/mm/yyyy</span>    
                                                        </asp:RegularExpressionValidator>   
                                                        <asp:CustomValidator ID="cvDOD" runat="server" ControlToValidate="txtDOD"
                                                            ClientValidationFunction="DateDiffMoreThanThree"  Display="Dynamic">
                                                             <span class="error" style="margin-left:21px;">You can enter only 3 days greater date than today.</span>
                                                        </asp:CustomValidator>
                                                 </td>
                                             </tr>
                                            <tr id = "trdt" class="hide"> 
                                                 <td class="style1" style="vertical-align:top;">Discharge Time :</td>
                                                 <td class="style2">
                                                     <MKB:TimeSelector ID="txtTOD" runat="server">
                                                     </MKB:TimeSelector>
                                                      <div style="margin-left: -6px;padding: 2px;">  
                                                     <asp:CheckBox ID="chkGenBill" runat="server" Text="Generate Bill"/>
                                                     </div>
                                                 </td>
                                             </tr>
                                             
                                            <tr>
                                                 <td class="style1">Total Paid Amount :</td>
                                                 <td class="style2">
                                                     <asp:TextBox ID="txtDepositAmt" runat="server" CssClass="required field" Width="120px" Enabled="false"> </asp:TextBox>
                                                     <asp:RequiredFieldValidator ID="reqDepAmt" runat="server" 
                                                            ControlToValidate="txtDepositAmt" CssClass="reqPos" 
                                                            ErrorMessage="Please specify Ward."><span class="error">Please enter Deposit 
                                                        Amount.</span></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="regDepAmt" runat="server" 
                                                            ControlToValidate="txtDepositAmt" Error Message="Please Enter Only Numbers" 
                                                            ValidationExpression="^(-)?\d+(\.\d\d)?$" ValidationGroup="check">
                                                        <span class="error">Please enter Only Numbers.</span>
                                                        </asp:RegularExpressionValidator>
                                                     
                                                 </td>
                                             </tr>

                                              <tr style="display:none;">
                                               
                                                  <td class="style1">Payment Mode *</td>
                                                 <td class="style2">
                                                     <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="160px"
                                    CssClass="field">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                    <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                    <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                      <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                    <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                </asp:DropDownList>

                                    <br />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                        Display="Dynamic" ErrorMessage="Please specify Payment Mode" ValidationGroup="check">
                                                        <span class="error" >Please specify Payment Mode</span></asp:RequiredFieldValidator>
                                                </td>
                                                </tr>

                                            <tr>
                                                 <td class="style1">&nbsp;</td>
                                                 <td class="style2">
                                                     <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" 
                                                         onclick="btnSave_Click" style="margin-top: 2px;" Text="Save" />
                                                 </td>
                                             </tr>
                                            </table>           
                                    </div>
                                </div>
                            <div class="clr"></div>
                        </div>               
                        </div>
                    </div>
            </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
