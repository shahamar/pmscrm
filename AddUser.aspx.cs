﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class AddUser : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string userrole;
    string strSQL;
    int uId = 0;
    int status;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["status"] != null)
            status = Convert.ToInt32(Request.QueryString["status"]);

        if (Request.QueryString["uId"] != null)
            uId = Convert.ToInt32(Request.QueryString["uId"].ToString());

        if (!IsPostBack)
        {
           // txtDOB.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            if (status == 1)
                lblMessage.Text = "User Details saved successfully";

            BindUserRole();
            FillData();
            
        }
    }
    public void BindUserRole()
    {
        ddRegFees.Items.Clear();
        ddRegFees.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT * FROM tblUserType", "UserTypeId", "UserType", ddRegFees);
    }
    private void FillData()
    {
        if (uId != 0)
        {

            TR_PWD.Visible = false;
            TR_PWDTXT.Visible = false;
            //txtLoginID.ReadOnly = true;

            TR_ShowPWD.Visible = false;
            TR_ShowPWDtxt.Visible = false;

            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "Select * from tblUser Where uId= " + uId + " ";
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader sqldr = SqlCmd.ExecuteReader();
            while (sqldr.Read())
            {
                txtfname.Text = sqldr["uFname"].ToString();
                txtmname.Text = sqldr["uMname"].ToString();
                txtlname.Text = sqldr["uLname"].ToString();
                txttele.Text = sqldr["uContact"].ToString();
                ddRegFees.SelectedValue = sqldr["uRole"].ToString();
                txtLoginID.Text = sqldr["uName"].ToString();
                txtPassword.Text = sqldr["uPwd"].ToString();
                string pwd = "";
                pwd = EncryptionTest.base64Decode(sqldr["uPwd"].ToString());
                txtshowpwd.Text = pwd;

                if (sqldr["uDOB"].ToString() != "NULL" || sqldr["uDOB"].ToString() != "")
                {
                    txtDOB.Text = sqldr["uDOB"] != DBNull.Value ? string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(sqldr["uDOB"].ToString())) : "";
                }

            }

            sqldr.Close();
            sqldr.Dispose();
            con.Close();
            con.Dispose();
        }
        else
        {
            TR_ShowPWD.Visible = false;
            TR_ShowPWDtxt.Visible = false;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string dob;
            if (txtDOB.Text != "")
            {
                string[] _dob = txtDOB.Text.ToString().Split('/');
                dob = _dob[2] + "/" + _dob[1] + "/" + _dob[0];
            }
            else
            {
                dob = "";

            }

            //string[] _dob = txtDOB.Text.ToString().Split('/');
            //string dob = _dob[2] + "/" + _dob[1] + "/" + _dob[0];

            bool doesexists = false;
            if (uId == 0)
                doesexists = gen.doesExist("Select * From tblUser Where uName='" + txtLoginID.Text.Trim() + " '");
            if (doesexists == false)
            {

                if (uId == 0)
                    strSQL = "InsUserDetails";
                else
                    strSQL = "UptUserDetails";

                SqlCmd = new SqlCommand(strSQL, con);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@uFname", txtfname.Text.ToString());
                SqlCmd.Parameters.AddWithValue("@uMname", txtmname.Text.ToString());
                SqlCmd.Parameters.AddWithValue("@uLname", txtlname.Text.ToString());
                SqlCmd.Parameters.AddWithValue("@uContact", txttele.Text.ToString());
                SqlCmd.Parameters.AddWithValue("@uCreatedBy", userid);
                SqlCmd.Parameters.AddWithValue("@uRole", ddRegFees.SelectedValue);
                SqlCmd.Parameters.AddWithValue("@uDOB", dob);
                //SqlCmd.Parameters.AddWithValue("@uName", txtLoginID.Text.Trim());
                SqlCmd.Parameters.AddWithValue("@uName", txtLoginID.Text.ToString());
                SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
                if (uId == 0)
                {

                    string pwd = "";
                    pwd = EncryptionTest.base64Encode(txtPassword.Text.Trim());

                    
                    SqlCmd.Parameters.AddWithValue("@uPwd", pwd);
                    SqlCmd.Parameters.Add("@uId", SqlDbType.Int).Direction = ParameterDirection.Output;

                }
                else
                {
                    SqlCmd.Parameters.AddWithValue("@uId", uId);
                }
                if (con.State == ConnectionState.Closed)
                    con.Open();
                int reslt;
                SqlCmd.ExecuteNonQuery();
                reslt = (int)SqlCmd.Parameters["@Error"].Value;
                if (reslt == 0)
                {
                    if (uId == 0)
                    {
                        uId = (int)SqlCmd.Parameters["@uId"].Value;
                        //Response.Redirect("User Access.aspx");
                       // Response.Redirect("MenuAccess.aspx?username=" + txtLoginID.Text.ToString() + "&userrole=" + ddRegFees.SelectedItem.Text + "&_userid=" + uId.ToString() + "&mode=new");
                        Response.Redirect("AddUser.aspx?status=1");
                    }
                    else
                    {
                        // Response.Redirect("User_Access.aspx");
                        //Response.Redirect("MenuAccess.aspx?username=" + txtLoginID.Text.ToString() + "&userrole=" + ddRegFees.SelectedItem.Text + "&_userid=" + uId.ToString() + "&mode=edt");
                        Response.Redirect("AddUser.aspx?status=1");
                    }
                }

                SqlCmd.Dispose();
                con.Close();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Login ID already exists. Please try with different login name.');", true);
            }
        }
    }
    protected void Chk1_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk1.Checked == true)
        {
            TR_ShowPWDtxt.Visible = true;
        }
        else
        {
            TR_ShowPWDtxt.Visible = false; 
        }
    }
}
