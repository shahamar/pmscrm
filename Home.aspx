﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="Home.aspx.cs" Inherits="Home" Title="Home-Welcome to Patient Management Systems" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <%--<meta http-equiv="refresh" content="10;" />--%>
    <style type="text/css">
        .blanket {
            background-color: #111;
            opacity: 0.65;
            position: absolute;
            z-index: 9001; /*ooveeerrrr nine thoussaaaannnd*/
            top: 0px;
            left: 0px;
            width: 100%;
        }

        .popUpDiv {
            position: absolute;
            background-color: #eeeeee;
            width: 70%;
            height: auto;
            z-index: 9002; /*ooveeerrrr nine thoussaaaannnd*/
            -moz-box-shadow: inset 0 0 5px #585858;
            -webkit-box-shadow: inset 0 0 5px #585858;
        }

        .fltrt {
            float: right;
            margin-right: 10px
        }

        .tblbox {
            height: 700px;
            width: 100%;
            margin-top: 20px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <table style="width: 100%;" cellspacing="5px" cellpadding="0px">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UP1" runat="server">
                        <ContentTemplate>
                            <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="60000"></asp:Timer>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>


                    <div class="login-area margin_ten_right margin">
                        <%--<h2>Welcome to Patient Management Systems</h2>--%>
                        <div class="formmenu">
                            <div class="loginform">
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel tblbox">
                                    <table style="width: 100%;" cellspacing="5px" cellpadding="0px">
                                        <tr>
                                            <td id="gvcol" style="width: 100%;">
                                                <asp:GridView ID="GVD_BedOccStatus" runat="server" ShowHeader="false" CssClass="mGrid" Visible="false"></asp:GridView>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                    <tr>
                                                        <td width="40%" align="left" valign="top">
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="0" class="table">
                                                                <tr>
                                                                    <td height="35" align="center" valign="middle" class="dashboardheader">
                                                                        <span>Appointment </span>
                                                                        <span class="fltrt"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="250" valign="top" align="left">
                                                                        <div class="data">

                                                                            <asp:UpdatePanel ID="UP2" runat="server">
                                                                                <ContentTemplate>

                                                                                    <asp:GridView ID="GVD_NewPat" runat="server" CssClass="mGrid" AutoGenerateColumns="false" OnRowCommand="GVD_NewPat_RowCommand">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="SNO" HeaderText="SN." ItemStyle-HorizontalAlign="center" ItemStyle-Width="10%" />
                                                                                            <%--<asp:BoundField DataField="pdName" HeaderText="Patient Name" />--%>
                                                                                            <asp:TemplateField ItemStyle-Width="45%" ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnkNewCasepdName" runat="server" Text='<%# Eval("pdName")%>'
                                                                                                        CommandArgument='<%# Eval("pdId") + ";" +Eval("fdId")%>'
                                                                                                        CommandName="NewCaseFol" Style="text-decoration: none; font-weight: normal;"
                                                                                                        ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("FStatus").ToString())%>'></asp:LinkButton>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <%-- <asp:BoundField DataField="pdPatientICardNo" HeaderText="Card No" />--%>
                                                                                            <%-- <asp:BoundField DataField="pdMob" HeaderText="Mobile No."  ItemStyle-HorizontalAlign="center" />--%>


                                                                                            <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="center" HeaderText="Mobile No.">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblpbMob" runat="server" Text='<%# Eval("pdMob")%>'
                                                                                                        ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("NewCaseFormStat").ToString())%>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>


                                                                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                                                                                                <ItemTemplate>
                                                                                                    <%# Eval("PrintBill") %>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>

                                                                                            <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="btnNewCase" runat="server" CommandName="View" CommandArgument='<%# Eval("adid") %>'
                                                                                                        ImageUrl="~/images/view.ico" ToolTip="View Details" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>

                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                        <td width="40%" align="left" valign="top">
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="0" class="table">
                                                                <tr>
                                                                    <td height="35" align="center" valign="middle" class="dashboardheader">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left" width="40%">
                                                                                    <span>
                                                                                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onclick" CssClass="textbutton b_submit" Text="Search" Style="margin-top: 2px;" />
                                                                                        <asp:TextBox ID="txtSearch" runat="server" Width="40%" CssClass="field"></asp:TextBox></span>
                                                                                </td>
                                                                                <td align="center">
                                                                                    <span>OPD</span>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="fltrt">
                                                                                        <a href="SearchPatient.aspx?AppType=C" class='example7'>
                                                                                            <asp:ImageButton ID="img_OPD" Width="60px" Style="vertical-align: middle;" runat="server"
                                                                                                ImageUrl="~/images/add-button-blue-th.png" ToolTip="OPD App." />
                                                                                        </a>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="250" valign="top" style="text-align: center; background: #fff;">
                                                                        <div class="data">

                                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                <ContentTemplate>


                                                                                    <asp:GridView ID="GVD_OLDPat" runat="server" CssClass="mGrid" EmptyDataText="No Record Found..." AutoGenerateColumns="false"
                                                                                        OnRowCommand="GVD_OLDPat_RowCommand">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="SNO" HeaderText="SN." ItemStyle-HorizontalAlign="center" />
                                                                                            <%-- <asp:BoundField DataField="pdName" HeaderText="Patient Name" />--%>
                                                                                            <%-- <asp:BoundField DataField="pdPatientICardNo" HeaderText="Card No" />--%>
                                                                                            <asp:TemplateField ItemStyle-Width="65%" ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnkOPDpdName" runat="server" Text='<%# Eval("pdName")%>'
                                                                                                        CommandArgument='<%# Eval("pdId") + ";" +Eval("fdId") %>'
                                                                                                        CommandName="ODPFol" Style="text-decoration: none; font-weight: normal;"
                                                                                                        ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("FStatus").ToString())%>'></asp:LinkButton>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField DataField="pdMob" HeaderText="Mobile No." Visible="false" />
                                                                                            <asp:BoundField DataField="App_Time" HeaderText="Time" ItemStyle-HorizontalAlign="center" />
                                                                                            <%-- <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                                                                    <ItemTemplate>
                                                                         <%# Eval("PrintBill") %>
                                                                    </ItemTemplate>
                                                                 </asp:TemplateField> --%>


                                                                                            <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center" Visible="true" HeaderText="Dr. Name">
                                                                                                <ItemTemplate>
                                                                                                    <%# Eval("uShortName") %>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>

                                                                                            <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblFCStat" runat="server" Text='<%# Eval("pdName")%>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>

                                                                                        </Columns>
                                                                                    </asp:GridView>

                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                        <td width="20%" align="left" valign="top">
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="0" class="table">
                                                                <tr>
                                                                    <td height="35" align="center" valign="middle" class="dashboardheader">
                                                                        <span class="style4">Today</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="250" align="right" valign="top">
                                                                        <div class="data1">
                                                                            <table width="100%" style="text-align: center;">

                                                                                <br />
                                                                                <br />

                                                                                <tr style="height: 120px">
                                                                                    <td align="center" valign="top">

                                                                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                                                            <ContentTemplate>

                                                                                                <table cellpadding="7px">

                                                                                                    <tr>
                                                                                                        <td>New Case</td>
                                                                                                        <td>:-</td>
                                                                                                        <td>
                                                                                                            <asp:Label ID="lblTotalNewCaseCount" runat="server" Text="Label"></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="color: #2c982c;">Completed</td>
                                                                                                        <td style="color: #2c982c;">:-</td>
                                                                                                        <td style="color: #2c982c;">
                                                                                                            <asp:Label ID="lblTotalCompNewCase" runat="server" Text="Label"></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="color: #FF0000;">Pending</td>
                                                                                                        <td style="color: #FF0000;">:-</td>
                                                                                                        <td style="color: #FF0000;">
                                                                                                            <asp:Label ID="lblTotalNewCasePending" runat="server" Text="Label"></asp:Label></td>
                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <br />
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td>OPD
                                                                                                        </td>

                                                                                                        <td>:- 
                                                                                                        </td>

                                                                                                        <td>
                                                                                                            <asp:Label ID="lblTotalOPDCount" runat="server" Text="Label"></asp:Label>
                                                                                                        </td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td style="color: #2c982c;">Completed 
                                                                                                        </td>

                                                                                                        <td style="color: #2c982c;">:- 
                                                                                                        </td>

                                                                                                        <td style="color: #2c982c;">
                                                                                                            <asp:Label ID="lblTotalOPDCompleted" runat="server" Text="Label"></asp:Label>
                                                                                                        </td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td style="color: #FF0000;">Pending
                                                                                                        </td>

                                                                                                        <td style="color: #FF0000;">:- 
                                                                                                        </td>

                                                                                                        <td style="color: #FF0000;">
                                                                                                            <asp:Label ID="lblTotalOPDPending" runat="server" Text="Label"></asp:Label>
                                                                                                        </td>

                                                                                                    </tr>
                                                                                                    <%--                <tr>
                  <td style="color: #FF0000;">
                        UpComing OPD
                  </td>
                  <td style="color:#FF0000;">
                :- 
                </td>
                 <td style="color: #FF0000;">
                 <asp:Label ID="lblUpcomingOPD" runat="server" Text="Label">
                 </asp:Label>
                 </td>
                 </tr>--%>
                                                                                                </table>

                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>

                                                                                    </td>
                                                                                </tr>

                                                                            </table>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>

                                                    <tr>
                                                        <td width="40%" align="left" valign="top">
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="0" class="table">
                                                                <tr>
                                                                    <td height="35" align="center" valign="middle" class="dashboardheader">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <span>
                                                                                        <asp:ImageButton IS="img_QCR" Width="65px" runat="server" Style="vertical-align: left; padding-left: 2px;" ImageUrl="~/images/QuickReg.png" ToolTip="Quick Registration" OnClick="img_QuickR_Click" /></span>
                                                                                </td>
                                                                                <td align="center">
                                                                                    <span>Non-Appointment</span>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="fltrt">
                                                                                        <asp:ImageButton ID="img_NewCase" Width="60px" runat="server" Style="vertical-align: middle;"
                                                                                            ImageUrl="~/images/add-button-blue-th.png" ToolTip="Add New Case" OnClick="img_NewCase_Click" />
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="250" align="left" valign="top">
                                                                        <div class="data">

                                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                                <ContentTemplate>

                                                                                    <asp:GridView ID="GVD_NonApp" runat="server" CssClass="mGrid" AutoGenerateColumns="false" OnRowCommand="GVD_NonApp_RowCommand">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="SNO" HeaderText="SN." ItemStyle-HorizontalAlign="center" ItemStyle-Width="10%" />
                                                                                            <%--<asp:BoundField DataField="pdName" HeaderText="Patient Name" />--%>
                                                                                            <asp:TemplateField ItemStyle-Width="45%" ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                                                                                <ItemTemplate>
                                                                                                    <asp:LinkButton ID="lnkNewCasepdName" runat="server" Text='<%# Eval("pdName")%>'
                                                                                                        CommandArgument='<%# Eval("pdId") + ";" +Eval("fdId")%>'
                                                                                                        CommandName="NewCaseFol" Style="text-decoration: none; font-weight: normal;"
                                                                                                        ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("FStatus").ToString())%>'></asp:LinkButton>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <%-- <asp:BoundField DataField="pdPatientICardNo" HeaderText="Card No" />--%>
                                                                                            <%-- <asp:BoundField DataField="pdMob" HeaderText="Mobile No."  ItemStyle-HorizontalAlign="center" />--%>

                                                                                            <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="center" HeaderText="Mobile No.">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblpbMob" runat="server" Text='<%# Eval("pdMob")%>'
                                                                                                        ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("NewCaseFormStat").ToString())%>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>

                                                                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                                                                                                <ItemTemplate>
                                                                                                    <%# Eval("PrintBill") %>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>

                                                                                            <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="btnNewCase" runat="server" CommandName="View" CommandArgument='<%# Eval("adid") %>'
                                                                                                        ImageUrl="~/images/view.ico" ToolTip="View Details" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>

                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                        <td width="40%" align="left" valign="top">
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="0" class="table">
                                                                <tr>
                                                                    <td height="35" align="center" valign="middle" class="dashboardheader">
                                                                        <span>Tele-Appointment <%--OPD Appoinment--%>
                                                                            <span class="fltrt">
                                                                                <%--    <a href="SearchPatient.aspx" class='example7'>--%>
                                                                                <asp:ImageButton ID="ImageButton2" Width="60px" Style="" runat="server" Visible="false"
                                                                                    ImageUrl="~/images/add-button-blue-th.png" ToolTip="OPD App." />

                                                                                <asp:ImageButton ID="img_btnNewApp" Width="60px" Style="vertical-align: middle;"
                                                                                    runat="server" ImageUrl="~/images/add-button-blue-th.png"
                                                                                    ToolTip="New Case App." OnClick="img_btnNewApp_Click" />

                                                                                <%-- </a>--%>
                                                                            </span>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="250" align="left" valign="top">
                                                                        <div class="data">

                                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                                                <ContentTemplate>

                                                                                    <asp:GridView ID="GVD_OldPatApp" runat="server" CssClass="mGrid" AutoGenerateColumns="false" Visible="false"
                                                                                        OnRowCommand="GVD_OldPatApp_RowCommand">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="SNO" HeaderText="SN." ItemStyle-HorizontalAlign="center" />
                                                                                            <asp:BoundField DataField="pdName" HeaderText="Patient Name" ItemStyle-HorizontalAlign="center" />
                                                                                            <%--<asp:BoundField DataField="pdPatientICardNo" HeaderText="Card No" />--%>
                                                                                            <asp:BoundField DataField="pdMob" HeaderText="Mobile No." ItemStyle-HorizontalAlign="center" />
                                                                                            <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="btnOldPatAppConfirm" runat="server" CommandName="Confirm"
                                                                                                        CommandArgument='<%# Eval("adid") %>'
                                                                                                        ImageUrl="~/images/button_ok.png" ToolTip="Confirm Appoinmet" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>

                                                                                    <asp:GridView ID="GVD_NewPatApp" runat="server" CssClass="mGrid" AutoGenerateColumns="false"
                                                                                        OnRowCommand="GVD_NewPatApp_RowCommand">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="SNO" HeaderText="SN." ItemStyle-HorizontalAlign="center" />
                                                                                            <asp:BoundField DataField="pdName" HeaderText="Patient Name" ItemStyle-HorizontalAlign="center" />
                                                                                            <%-- <asp:BoundField DataField="pdPatientICardNo" HeaderText="Card No" Visible="false"/>--%>
                                                                                            <asp:BoundField DataField="pdMob" HeaderText="Mobile No." ItemStyle-HorizontalAlign="center" />
                                                                                            <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="center">
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="btnNewPatAppConfirm" runat="server" CommandName="Confirm"
                                                                                                        CommandArgument='<%# Eval("adid") %>'
                                                                                                        ImageUrl="~/images/button_ok.png" ToolTip="Confirm Appoinmet" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>


                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>

                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                        <td width="20%" align="left" valign="top">
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="0" class="table">
                                                                <tr>
                                                                    <td height="35" align="center" valign="middle" class="dashboardheader">
                                                                        <span>Unoccupied Bed</span></td>
                                                                </tr>

                                                                <tr>
                                                                    <td height="250" align="left" valign="top">
                                                                        <div class="data1">
                                                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                                                <ContentTemplate>


                                                                                    <asp:GridView ID="GVD_BedOcc" runat="server" CssClass="mGrid" AutoGenerateColumns="false" ShowHeader="false">
                                                                                        <Columns>
                                                                                            <asp:BoundField DataField="WardTypeName" HeaderText="Ward Type" />
                                                                                            <asp:BoundField DataField="TotalBed" HeaderText="Total Bed" />
                                                                                        </Columns>
                                                                                    </asp:GridView>

                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>



                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;" colspan="2">
                                                <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                                    <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label></font>
                                            </td>
                                        </tr>
                                        <!--<tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>-->
                                        <%--//---Added By Nikita On 10thMarch 2015------------------------------------------------------%>
                                        <tr>
                                            <td style="text-align: center;" colspan="2">
                                                <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></font>
                                            </td>
                                        </tr>
                                        <%--//-------------------------------------------------------------------------------------------%>
                                    </table>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>



                </td>
            </tr>
        </table>


        <table>
            <tr>
                <td>

                    <asp:UpdatePanel ID="UPBL" runat="server">
                        <ContentTemplate>

                            <asp:Panel ID="pnlBirthList" runat="server" Visible="False">

                                <div id="blackout" runat="server" class="blanket" style="height: 2000px;"></div>
                                <div id="whiteout" class="popUpDiv" runat="server" style="top: 75px; left: 15%;">
                                    <div style="position: relative; top: -10px; left: 7px; float: right">
                                        <asp:ImageButton ID="imgCloseButton" runat="server" ImageUrl="~/images/FancyClose.png" OnClick="imgCloseButton_Click" />
                                    </div>

                                    <table style="width: 100%;">
                                        <tr>
                                            <td><font style="font-size: 24px; color: #F27400;">
                                                <center>Users BirthDay List</center>
                                            </font></td>
                                        </tr>

                                        <tr>
                                            <td id="Td3" align="center" style="width: 100%; text-align: center;">
                                                <center>
                                                    <asp:GridView ID="gridUsersBirthList" Width="800px" runat="server" AutoGenerateColumns="false" CssClass="mGrid" AllowPaging="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="uid" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbluid" runat="server" Text='<% #Eval("uId") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="User Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Mobile No." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbluContact" runat="server" Text='<%# Eval("uContact")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </center>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <font style="font-size: 24px; color: #F27400;">
                                                    <center>Doctors BirthDay List</center>
                                                </font>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td id="Td2" align="center" style="width: 100%; text-align: center;">
                                                <center>
                                                    <asp:GridView ID="gridDoctorsBirthList" Width="800px" runat="server" AutoGenerateColumns="false" CssClass="mGrid" AllowPaging="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="pdid" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldid" runat="server" Text='<% #Eval("did") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Doctor Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Mobile No." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldMobileNo" runat="server" Text='<%# Eval("dMobileNo")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </center>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <font style="font-size: 24px; color: #F27400;">
                                                    <center>IPD Patient BirthDay List</center>
                                                </font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="Td1" align="center" style="width: 100%; text-align: center;">
                                                <center>
                                                    <asp:GridView ID="gridPatientBirthList" Width="800px" runat="server" AutoGenerateColumns="false" CssClass="mGrid" AllowPaging="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="pdid" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpdid" runat="server" Text='<% #Eval("pdid") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Patient Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Case Paper No." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCasePaperNo" runat="server" Text='<%# Eval("CasePaperNo")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Mobile No." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpdMob" runat="server" Text='<%# Eval("pdMob")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>


                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>



    </center>
</asp:Content>

