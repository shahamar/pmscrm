﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterHome1.master" CodeFile="SendSMS.aspx.cs" Inherits="SendSMS" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 100%; text-align: center">
                    <h2>
                        <asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h2>
                    <%--<div class="formmenu">
                    <div class="loginform">--%>
                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                        <table style="width: 100%; background: #f2f2f2" cellspacing="0px" cellpadding="5px">
                            <tr>
                                <td>
                                    <table style="width: 100%;" cellspacing="5px" cellpadding="5px">
                                        <tr>
                                            <td width="17%" align="left">
                                                <strong>Patients :&nbsp;</strong>
                                                <asp:CheckBox ID="chk_patient" runat="server" OnCheckedChanged="chk_patient_CheckedChanged" AutoPostBack="true" />
                                            </td>
                                            <td width="16%" align="left">
                                                <strong>Doctors :&nbsp;</strong>
                                                <asp:CheckBox ID="chk_doctors" runat="server" OnCheckedChanged="chk_doctors_CheckedChanged" ValidationGroup="1" AutoPostBack="true" />
                                            </td>
                                           <td width="423px" height="65px" align="left">
                                               <asp:TextBox ID="txt_message" runat="server" CssClass="textbox" TextMode="MultiLine" width="423px" height="65px" onkeypress="return this.value.length<=159"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_message"
                                                    Display="Dynamic" ErrorMessage="Message is required." ValidationGroup="Send">
                                                    <span class="error">Message is required.</span>
                                                </asp:RequiredFieldValidator>
                                            </td>
                                            <td width="10%" align="left">
                                                <asp:Button ID="btnsendsms" runat="server" Text="Send SMS" CssClass="textbutton b_submit left" OnClick="btnsendsms_Click"
                                                    ValidationGroup="Send" />
                                            </td>
                                            <td width="42%"></td>
                                              <td style="color: Red; text-align: right;">
                                                            <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                        </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td id="gvcol_patient" runat="server" style="width: 100%; text-align: center; background: #fff">
                                      <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                    CssClass="mGrid"  PageSize="20" ShowHeader="true" Width="100%" EmptyDataText="No records found..."
                                                     OnPageIndexChanging="GRV1_PageIndexChanging">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" Visible="false">
                                                            <ItemTemplate>
                                                                <%# Eval("pdPatientICardNo")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                                            <ItemTemplate>
                                                                <%# Eval("pdName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="center" HeaderText="Gender">
                                                            <ItemTemplate>
                                                                <%# Eval("pdSex")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderText="DOC">
                                                            <ItemTemplate>
                                                                <%# Eval("pdDOC", "{0:dd-MMM-yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="center" HeaderText="Case Paper No.">
                                                            <ItemTemplate>
                                                                <%# Eval("pdCasePaperNo")%>  <%# Eval("pdRCasePaperNo")%> 
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                       
                                                        <asp:TemplateField ItemStyle-Width="19%" ItemStyle-HorizontalAlign="center" HeaderText="Mobile No.">
                                                            <ItemTemplate>
                                                                <%# Eval("pdMob")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                </td>
                                 <td id="gvcol_doctor" runat="server" style="width: 100%; text-align: center; background: #fff">
                                       <asp:GridView ID="GRV_doctors" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                CssClass="mGrid" EmptyDataText="No Records Found." PagerStyle-CssClass="pgr"
                                                                PageSize="20" ShowHeader="true" Width="100%"
                                                                OnPageIndexChanging="GRV_doctors_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Full Name">
                                                                        <ItemTemplate>
                                                                            <%# Eval("FullName")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
								                                    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderText="DOB">
                                                                        <ItemTemplate>
                                                                            <%# Eval("DOB", "{0:dd-MMM-yyyy}")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderText="DOJ">
                                                                        <ItemTemplate>
                                                                            <%# Eval("dDOJ", "{0:dd-MMM-yyyy}")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Type">
                                                                        <ItemTemplate>
                                                                            <%# Eval("dType")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderText="Mobile No.">
                                                                        <ItemTemplate>
                                                                            <%# Eval("dMobileNo")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("did") %>'
                                                                                CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                            <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("did") %>'
                                                                                CommandName="del" ImageUrl="Images/delete.ico" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete ?');" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></font>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <%-- <div class="clr">
                        </div>
                    </div>
                </div>--%>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
