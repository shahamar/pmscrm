﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="VoucherReport.aspx.cs" Inherits="VoucherReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <link rel="stylesheet" type="text/css" href="css/radiotabstrip.css" />
    <style>
        #tbsearch td {
            text-align: left;
            vertical-align: middle;
        }

        #tbsearch label {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }

        #tbhead th {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }

        #gvcol div {
            margin-top: -10px;
        }

        .head1 {
            width: 10%;
        }

        .head2 {
            width: 18%;
        }

        .head3 {
            width: 9%;
        }

        .head4 {
            width: 10%;
        }

        .head4i {
            width: 18%;
        }

        .head5 {
            width: 10%;
        }

        .head6 {
            width: 10%;
        }

        .head7 {
            width: 18%;
        }

        .head8 {
            width: 5%;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>

    <%--<asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>--%>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>Report</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <ul class="quicktabs_tabs quicktabs-style-excel">
                                <li class="qtab-Demo first" id="li_1">
                                    <asp:LinkButton ID="lnkOpd" runat="server" CssClass="qt_tab active" PostBackUrl="~/OPDReport.aspx">OPD</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_2">
                                    <asp:LinkButton ID="lnk3dccg" runat="server" CssClass="qt_tab active" PostBackUrl="~/3DCCGReport.aspx">3DCCG</asp:LinkButton></li>
                                <li class="qtab-HTML " id="li_3">
                                    <asp:LinkButton ID="lnlipd" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDReport.aspx" Visible="False">IPD</asp:LinkButton></li>
                                <li class="qtab-HTML" id="li_4">
                                    <asp:LinkButton ID="lnkConsulting" runat="server" CssClass="qt_tab active" PostBackUrl="~/ConsultingReport.aspx">Consulting</asp:LinkButton></li>
                                <li class="qtab-HTML" id="li_5">
                                    <asp:LinkButton ID="lnkPft" runat="server" CssClass="qt_tab active" PostBackUrl="~/PFTReport.aspx">PFT</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_6">
                                    <asp:LinkButton ID="LnkReconsulting" runat="server" CssClass="qt_tab active" PostBackUrl="~/ReConsulting.aspx">Reconsulting</asp:LinkButton></li>
                                <li class="qtab-HTML" id="li_7">
                                    <asp:LinkButton ID="LnkECG" runat="server" CssClass="qt_tab active" PostBackUrl="~/ECGReport.aspx">ECG</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_8">
                                    <asp:LinkButton ID="LikCourier" runat="server" CssClass="qt_tab active" PostBackUrl="~/CourierReport.aspx">Courier</asp:LinkButton></li>
                                <li class="qtab-HTML" id="li_9">
                                    <asp:LinkButton ID="LnkAdvance" runat="server" CssClass="qt_tab active" PostBackUrl="~/BalanceReport.aspx">Advance</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_10">
                                    <asp:LinkButton ID="LnkBalance" runat="server" CssClass="qt_tab active" PostBackUrl="~/BalanceReport.aspx" Style="display: none;">Balance</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_11">
                                    <asp:LinkButton ID="LnkPnt" runat="server" CssClass="qt_tab active" PostBackUrl="~/DeletedPatientReport.aspx" Style="display: none;">Deleted Patient</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_12">
                                    <asp:LinkButton ID="LnkDietn" runat="server" CssClass="qt_tab active" PostBackUrl="~/DieticianReport.aspx">Dietitian</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_13">
                                    <asp:LinkButton ID="LnkDelIPD" runat="server" CssClass="qt_tab active" PostBackUrl="~/DeletedIPDReport.aspx" Style="display: none;">Deleted IPD</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML  active last" id="li_14">
                                    <asp:LinkButton ID="LnkVoucher" runat="server" CssClass="qt_tab active" Enabled="false">Voucher</asp:LinkButton>
                                </li>
                                 <li class="qtab-HTML last" id="li_15">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="qt_tab active" PostBackUrl="~/EECPReports.aspx">EECP</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML last" id="li_16">
                                    <asp:LinkButton ID="LnkTotal" runat="server" CssClass="qt_tab active" PostBackUrl="~/GetTotalInvoiceAmountofAllReport.aspx">Total Report</asp:LinkButton>
                                </li>
                            </ul>
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto; border: 1.5px solid #E2E2E2;">

                                <table width="100%" cellspacing="0px" cellpadding="2px">
                                    <tr>
                                        <td>
                                            <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                <tr>
                                                    <td class="tableh1">
                                                        <table id="tbsearch" style="width: 98%;">
                                                            <tr>
                                                                <td>Voucher Date From
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtfrm" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                        runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute; width: 20px; height: 20px;" />
                                                                </td>
                                                                <td>To
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtto" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                        Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute; width: 20px; height: 20px;" />
                                                                </td>
                                                                <td>Prepared By : &nbsp;
                                                                              
                                                                </td>

                                                                <td>
                                                                    <asp:DropDownList ID="DDL_User" runat="server" class="required field" Width="150px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>

                                                            <tr>

                                                                <td colspan="8">

                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 2%">
                                                                                <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                                                                    OnClientClick="aspnetForm.target ='_self';" OnClick="btnView_Click" />
                                                                            </td>
                                                                            <td style="width: 2%">
                                                                                <div id="export" runat="server" style="float: right; display: none">
                                                                                    <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" OnClick="imgexport_Click" />
                                                                                </div>
                                                                            </td>
                                                                            <td style="color: Red; text-align: right;">
                                                                                <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Panel ID="pnlHead" runat="server" Visible="false">
                                    <div id="rptHead" runat="server" visible="false">
                                        <table width="100%" id="rounded-corner-report">
                                            <tr>
                                                <td align="center">Aditya Homoeopathic Hospital & Healing Center<br />
                                                    Pimpri Gaon, Pune- 411 017
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="font-weight: bold">Voucher Report
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="gvcol">
                                                    <asp:GridView runat="server" ID="GRV1" AutoGenerateColumns="false" CssClass="mGrid" AllowPaging="True" AllowSorting="true" PagerStyle-CssClass="pgr" PageSize="20"
                                                        Width="98%" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="Prepared By" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblUser" Text='<%#Eval("UserName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Sr.No" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%#Container.DataItemIndex + 1%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%#Eval("Name")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Voucher Date" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%# Eval("Date","{0:dd/MM/yyyy}")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CasePaperNo" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%#Eval("pdCasePaperNo")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Return Amount" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblPayment" Text='<%#Eval("ReturmAmt")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%#Eval("Source")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                                                                <ItemTemplate>
                                                                    <%# Eval("PrintBill") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="10">
                                                    <table width="100%" cellspacing="0" border="1" class="mGrid">
                                                        <tr>
                                                            <td style="width: 59%">
                                                                <%--<asp:Label ID="Label1" Font="Bold" runat="server" Text="Sub Payment"></asp:Label></b>--%>
                                                                <b>Sub Payment</b>
                                                            </td>

                                                            <td style="width: 20%">
                                                                <b>
                                                                    <asp:Label ID="lblSubTotal" runat="server" Text="0.00"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 59%">
                                                                <b>Total Payment</b>
                                                            </td>

                                                            <td style="width: 20%">
                                                                <b>
                                                                    <asp:Label ID="lblGrandTotal" runat="server" Text="0.00"></asp:Label></b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

