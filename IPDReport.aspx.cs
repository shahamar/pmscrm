﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public partial class IPDReport : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    string uid = "0";
    //string ipdId = "0";
    int Page_no = 0, Page_size = 20;
    string tempUser = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //StrSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 41";
        //bool exists = gen.doesExist(StrSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();
            BindGridView();
        }
    }

    protected void FillData()
    {
        DDL_User.Items.Insert(0, new ListItem("Select", "%"));
        gen.FillDropDownList("Select uId , uFname +' '+ uMname +' '+ uLname As DNAME from tblUser Where IsDelete = 0 and uRole = 2", "uId", "DNAME", DDL_User);
        //DDL_User.SelectedValue = uid;

    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    private void BindGridView()
    {
        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        string FormDate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        string ToDate = todate[2] + "/" + todate[1] + "/" + todate[0];
        StrSQL = "Sp_GetIpdReport";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        //SqlCmd.Parameters.AddWithValue("@ipdId", ipdId);
        SqlCmd.Parameters.AddWithValue("@frmdate", FormDate);
        SqlCmd.Parameters.AddWithValue("@todate", ToDate);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@User", DDL_User.SelectedValue);
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                pnlHead.Visible = true;
                rptHead.Visible = true;
                GRV1.DataSource = dt;
                GRV1.DataBind();
                decimal Total = 0;

                foreach (GridViewRow row in GRV1.Rows)
                {

                    Label lblsub = (Label)row.FindControl("lblPayment");
                    Total += Decimal.Parse(lblsub.Text.ToString());

                }
                decimal tot = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tot += decimal.Parse(dt.Rows[i]["TotAmt"].ToString());
                }

                lblGrandTotal.Text = tot.ToString();
                lblSubTotal.Text = Total.ToString();
                export.Style.Add("display", "block");
                createpaging(dt.Rows.Count, GRV1.PageCount);
                decimal subtot = Decimal.Parse(lblSubTotal.Text);
                lblSubTotal.Text = subtot.ToString();
                Label1.Visible = true;
                lblSubTotal.Visible = true;
            }
            else
            {
                GRV1.DataSource = null;
                GRV1.DataBind();

                pnlHead.Visible = false;
                rptHead.Visible = false;
                export.Style.Add("display", "none");
                lblGridHeader.Text = "No record found";
            }
        }
    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGridView();
    }

    protected void GRV1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (((Label)e.Row.FindControl("lblUser")).Text == tempUser)
            {
                e.Row.Cells[0].Text = string.Empty;
            }
            else
            {
                tempUser = ((Label)e.Row.FindControl("lblUser")).Text;
            }

        }
    }
    
    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        Label1.Visible = false;
        lblSubTotal.Visible = false;
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "IPDReport.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRV1.AllowPaging = false;
        this.BindGridView();
        GRV1.HeaderRow.Style.Add("background-color", "#FFFFFF");
        pnlHead.RenderControl(htw);
        //GRV1.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGridView();
        //ScriptManager1.RegisterPostBackControl(this.imgexport);
    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string ipdId = e.CommandArgument.ToString();
        if (e.CommandName == "Save")
        {
            Session["IpdID"] = ipdId;
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "sp_SaveBill";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Connection = conn;
            SqlCmd.Parameters.AddWithValue("@ipdId", ipdId);
            SqlCmd.Parameters.AddWithValue("@finyear", gen.GetCurrentFinYear());
            SqlCmd.Parameters.AddWithValue("@CreatedBy", uid.ToString());
            SqlCmd.Parameters.AddWithValue("@OtherCharges", 0);
            SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            SqlCmd.ExecuteNonQuery();
            SqlCmd.Dispose();
            conn.Close();
            Session["IpdID"] = ipdId;
            String js = "window.open('IPDReportBill.aspx', '_blank');";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Open IPDReportBill.aspx", js, true);
        }
    }
    private void RedirectPage()
    {
        string.Format("<script>window.open({0},'_blank');</script>", "IPDReportBill.aspx");
    }

}
