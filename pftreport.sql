USE [PMS]
GO

/****** Object:  StoredProcedure [dbo].[sp_PftReport]    Script Date: 04/16/2013 11:27:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_PftReport]      
@frmDate SMALLDATETIME      
,@toDate SMALLDATETIME      
AS      
BEGIN      
SELECT    
    itdTestDate  
   ,pdInitial + ' ' + pdFname + ' ' + pdMname +' ' + pdLname 'Name'   
   ,'PFT' AS 'test'    
   ,itdcharges   
   ,ROW_NUMBER()OVER(PARTITION by  itdTestDate order by itdTestDate asc)as 'rank'  
FROM      
  tblIpdTestDetails itd    
  inner  join    
  tblIpdMaster i ON itd.itdipdId = i.ipdId     
  inner  join    
  tblPatientDetails pd ON i.ipdpdId = pd.pdID   
WHERE    
 itdtmId = 3 and itd.itdTestDate between @frmDate and @toDate
 order by itdTestDate
 End
GO


