﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SeminarPatientList : System.Web.UI.Page
{
    genral gen = new genral();
    bool rslt = false;
    SqlCommand SqlCmd;
    string StrSQL;
    SqlDataAdapter dapt;
    string strSQL = "";
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string pdId = "0";
    string uid = "0";
    string ipdid = "0";
    int Page_no = 0, Page_size = 20;
    object frm = null;
    object to = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");
        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindGVR();
        }
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    private void Bind_Report()
    {

        string Name, CasePaperNo;

        if (txtname.Text == "")
        {
            Name = "%";
        }
        else
        {
            Name = "%" + txtname.Text + "%";
        }


        if (txtcasepaperno.Text == "")
        {
            CasePaperNo = "%";
        }
        else
        {
            CasePaperNo = "%" + txtcasepaperno.Text + "%";
        }




        string[] _frm;
        string[] _to;


        if (txtfrm.Text != "")
        {
            if (txtfrm.Text.Contains("/"))
            {
                _frm = txtfrm.Text.ToString().Split('/');
                _to = txtto.Text.ToString().Split('/');

                frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
                to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";
            }

            if (txtfrm.Text.Contains("-"))
            {
                _frm = txtfrm.Text.ToString().Split('-');
                _to = txtto.Text.ToString().Split('-');

                frm = _frm[2] + "-" + _frm[1] + "-" + _frm[0] + " 00:00:00";
                to = _to[2] + "-" + _to[1] + "-" + _to[0] + " 23:55:00";
            }
        }

        StrSQL = "Proc_GetSeminarPatientList";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FrmDate", ToDBNull(frm));
        SqlCmd.Parameters.AddWithValue("@ToDate", ToDBNull(to));
        SqlCmd.Parameters.AddWithValue("@Name", Name);
        SqlCmd.Parameters.AddWithValue("@CasePaperNo", CasePaperNo);

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {

                GRV1.DataSource = dt;
                GRV1.DataBind();
                GRV1.PageSize = Page_size;
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
            else
            {
                GRV1.DataSource = null;
                GRV1.DataBind();

            }
            conn.Close();
            dr.Dispose();
        }
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }
    public void BindGVR()
    {
        Bind_Report();
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    public static object ToDBNull(object value)
    {
        if (null != value)
            return value;
        return DBNull.Value;
    }
}