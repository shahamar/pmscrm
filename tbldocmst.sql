USE [PMS]
GO

/****** Object:  Table [dbo].[tblDoctorMaster]    Script Date: 04/17/2013 15:49:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblDoctorMaster](
	[did] [int] IDENTITY(1,1) NOT NULL,
	[dFName] [varchar](100) NULL,
	[dMName] [varchar](100) NULL,
	[dLName] [varchar](100) NULL,
	[dContactNo] [varchar](20) NULL,
	[dMobileNo] [varchar](20) NULL,
	[dCreatedBy] [int] NULL,
	[dCratedDate] [datetime] NULL,
	[dDOJ] [smalldatetime] NULL,
	[dAddressLine1] [varchar](100) NULL,
	[dAddressLine2] [varchar](100) NULL,
	[dType] [varchar](50) NULL,
	[dEmailId] [varchar](60) NULL,
PRIMARY KEY CLUSTERED 
(
	[did] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblDoctorMaster] ADD  DEFAULT (getdate()) FOR [dCratedDate]
GO


