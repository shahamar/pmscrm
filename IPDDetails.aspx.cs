﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;


public partial class IPDDetails : System.Web.UI.Page
{
    SqlDataReader Sqldr;
    SqlCommand SqlCmd;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string strSQL = "";
    string strSQL1 = "";
    genral gen = new genral();
    string urole,msg;
    //string pdId = "0";
    string uid = "0";
    //added by nikita on 12th march 2015------------------------------
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-----------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");
        
        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 51";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            FillData();
        }

    }
    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "sel")
        {
            Session["ipdid"] = e.CommandArgument.ToString();
            Response.Redirect("IPDViewAks.aspx?DISPROW=ALL", false);
        }

        if (e.CommandName == "del")
        {
            strSQL = "UPDATE tblIpdMaster SET IsDelete = 1 WHERE ipdId = " + e.CommandArgument;
            gen.executeQuery(strSQL);

            strSQL1 = "UPDATE tblPatientsWardDetails SET WIsDelete = 1 WHERE wipdId = " + e.CommandArgument;
            gen.executeQuery(strSQL1);

            FillData();
        }
    }

    private void FillData()
    {
        strSQL = "Select *,Case ipdIsDischarge When 1 THEN '<img src=''images/bill.ico'' onclick= />' ELSE '' END 'Bill'From dbo.tblIpdMaster where IsDelete = 0 AND ipdpdID=" + pdId;
        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        if (msg == "1")
            GRV1.Columns[4].Visible = false;
        else
            GRV1.Columns[4].Visible = true;

        strSQL = "Select *,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName' From dbo.tblPatientDetails where pdID=" + pdId;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            lblPatientName.Text = Sqldr["pdName"].ToString();
            lblPatientId.Text = Sqldr["pdPatientICardNo"].ToString();
            lblAge.Text = Sqldr["pdAge"].ToString();
            lblCasePaperNo.Text = Sqldr["pdCasePaperNo"].ToString();
            lblCassetteNo.Text = Sqldr["pdCassetteNo"].ToString();
            lblemail.Text = Sqldr["pdemail"].ToString();
            lblGender.Text = Sqldr["pdSex"].ToString();
            lblmob.Text = Sqldr["pdMob"].ToString();
            lbltele.Text = Sqldr["pdTele"].ToString();
        }
        Sqldr.Close();
        SqlCmd.Dispose();
    }
}
