﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Drawing;

public partial class TodaysPatientList : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string RepType = "";
    string frm, to;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (Request.QueryString["status"] != null)
        {
            int status = Convert.ToInt32(Request.QueryString["status"]);
            if (status == 1)
                lblMessage.Text = "You are not authorized to view this form";
        }

        if (Request.QueryString["ListType"] != null)
        {
            RepType = Request.QueryString["ListType"].ToString();

            if (RepType == "NewCase")
            {
                lblHeader.Text = "New Patient List";
            }

            else if (RepType == "OPDCase")
            {
                lblHeader.Text = "OPD Patient List";
            }

        }

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            Bind_All_Patient_NEW_OPD();
        }

    }

    private void Bind_All_Patient_NEW_OPD()
    {
        string[] _frm;
        string[] _to;


        if (txtfrm.Text.Contains("/"))
        {
            _frm = txtfrm.Text.ToString().Split('/');
            _to = txtto.Text.ToString().Split('/');

            frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";
        }

        if (txtfrm.Text.Contains("-"))
        {
            _frm = txtfrm.Text.ToString().Split('-');
            _to = txtto.Text.ToString().Split('-');

            frm = _frm[2] + "-" + _frm[1] + "-" + _frm[0] + " 00:00:00";
            to = _to[2] + "-" + _to[1] + "-" + _to[0] + " 23:55:00";
        }

        StrSQL = "PatientListCompletionStatus";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;

        if (RepType == "NewCase")
        {

            SqlCmd.Parameters.AddWithValue("@Flag", "Bind_All_New_Patient");

        }
        else if (RepType == "OPDCase")
        {

            SqlCmd.Parameters.AddWithValue("@Flag", "Bind_All_OPD_Patient");

        }

        SqlCmd.Parameters.AddWithValue("@FrmDate", frm);
        SqlCmd.Parameters.AddWithValue("@ToDate", to);
        SqlCmd.Parameters.AddWithValue("@APType", ddlApType.SelectedValue);

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {

                if (RepType == "NewCase")
                {
                    //GVD_PatientList.Columns[10].Visible = false; //lastfollowupdate
                    //GVD_PatientList.Columns[4].Visible = false; //contactno
                    //GVD_PatientList.Columns[13].Visible = false; //time

                    GVD_PatientList1.DataSource = dt;
                    GVD_PatientList1.DataBind();

                    GVD_PatientList1.Visible = true;
                    GVD_PatientList.Visible = false;
                }
                else if (RepType == "OPDCase")
                {
                    //GVD_PatientList.Columns[2].Visible = false; //age
                    //GVD_PatientList.Columns[3].Visible = false; //gender
                    //GVD_PatientList.Columns[6].Visible = false; //consltingdate
                    //GVD_PatientList.Columns[7].Visible = false; //doctorname

                    GVD_PatientList.DataSource = dt;
                    GVD_PatientList.DataBind();

                    GVD_PatientList.Visible = true;
                    GVD_PatientList1.Visible = false;
                }
            }
            else
            {
                if (RepType == "NewCase")
                {
                    GVD_PatientList1.DataSource = null;
                    GVD_PatientList1.DataBind();
                }
                else if (RepType == "OPDCase")
                {
                    GVD_PatientList.DataSource = null;
                    GVD_PatientList.DataBind();
                }
            }
            conn.Close();
            dr.Dispose();
        }
    }

    protected void GVD_PatientList_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "CaseFol")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                string fdID = arg[1];

                Session["ipdid"] = null;
                Session["CasePatientID"] = pdID;

                if (RepType == "NewCase")
                {
                    //Response.Redirect("ConsultPatient.aspx", false);

                    int cnt = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from vwConsultingHistoryNewAks Where fdPatientId=" + pdID).ToString());

                    if (cnt >= 1)
                    {
                        if (fdID != "0")
                        {
                            Response.Redirect("FollowupDetail.aspx?Stat=NO&fdId=" + fdID);
                        }
                        else
                        {
                            Response.Redirect("FollowupDetail.aspx?fdId=" + fdID);
                        }


                    }
                    else
                    {
                        Response.Redirect("ConsultPatient.aspx", false);
                    }

                }
                else if (RepType == "OPDCase")
                {
                    if (fdID != "0")
                    {
                        Response.Redirect("FollowupDetail.aspx?Stat=NO&fdId=" + fdID);
                    }
                    else
                    {
                        Response.Redirect("FollowupDetail.aspx?fdId=" + fdID);
                    }
                }

            }
            catch
            { }
        }

        if (e.CommandName == "Cancel")
        {
            try
            {

                SqlCmd = new SqlCommand();
                SqlCmd.CommandText = "proc_ConfirmAppointmentDetails";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlCmd.Parameters.AddWithValue("@adId", e.CommandArgument);
                SqlCmd.Parameters.AddWithValue("@ConfirmBy", uid);
                SqlCmd.Parameters.AddWithValue("@pdfinyear", gen.GetCurrentFinYear());
                SqlCmd.Parameters.AddWithValue("@Flag", "Delete");
                SqlCmd.ExecuteNonQuery();
                con.Close();
                SqlCmd.Dispose();
                Bind_All_Patient_NEW_OPD();

            }
            catch
            { }
        }
        if (e.CommandName == "OPDT")
        {

            string pdID = (e.CommandArgument).ToString();

            Session["ipdid"] = null;
            Session["CasePatientID"] = pdID;

            Response.Redirect("OPDTestDetails.aspx");
        }


    }

    protected void GVD_PatientList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        string pdid = "";
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
        {
            if (RepType == "NewCase")
            {
                pdid = (string)this.GVD_PatientList1.DataKeys[e.Row.RowIndex]["pdId"].ToString();
            }
            else if (RepType == "OPDCase")
            {
                pdid = (string)this.GVD_PatientList.DataKeys[e.Row.RowIndex]["pdId"].ToString();
            }

            Label lblkco = (Label)e.Row.FindControl("lblkco");
            Label lblPatType = (Label)e.Row.FindControl("lblPatType");
            Label lblCaseStat = (Label)e.Row.FindControl("lblCaseStat");
            LinkButton btnLnk = (LinkButton)e.Row.FindControl("lnkNewCasepdName");
            ImageButton btnOldPatAppCancel = (ImageButton)e.Row.FindControl("btnOldPatAppCancel");

            if (File.Exists(Server.MapPath("Files/KCO/kco_" + pdid)))
            {
                lblkco.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + pdid).ToString());
            }
            else
            {
                lblkco.Text = "";
            }

            if (lblPatType.Text == ".")
            {
                lblPatType.ForeColor = Color.DarkRed;
            }
            if (lblkco.Text == "")
            {
                if (lblCaseStat.Text == "Y")
                {
                    foreach (TableCell cell in e.Row.Cells)
                    {
                        cell.ForeColor = Color.Purple;
                    }
                    lblCaseStat.Text = "Completed";
                    btnLnk.ForeColor = Color.Purple;
                }
                else
                {
                    foreach (TableCell cell in e.Row.Cells)
                    {
                        cell.ForeColor = Color.Purple;
                    }
                    lblCaseStat.Text = "Pending";
                    btnLnk.ForeColor = Color.Purple;
                }
            }
            else if (lblCaseStat.Text == "N")
            {
                btnOldPatAppCancel.Visible = true;
                foreach (TableCell cell in e.Row.Cells)
                {
                    cell.ForeColor = Color.Red;
                }
                lblCaseStat.Text = "Pending";
                btnLnk.ForeColor = Color.Red;
            }
            else
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    cell.ForeColor = Color.Green;
                }
                lblCaseStat.Text = "Completed";
                btnLnk.ForeColor = Color.Green;
            }
        }
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        Bind_All_Patient_NEW_OPD();
    }

    protected void GVD_PatientList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

    }

    protected void GVD_PatientList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (RepType == "NewCase")
        {
            GVD_PatientList1.PageIndex = e.NewPageIndex;
            Bind_All_Patient_NEW_OPD();
        }
        else if (RepType == "OPDCase")
        {
            GVD_PatientList.PageIndex = e.NewPageIndex;
            Bind_All_Patient_NEW_OPD();
        }
    }

}