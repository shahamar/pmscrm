﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterHome1.master" CodeFile="ManageUserAccess.aspx.cs" Inherits="ManageUserAccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />


    <head>
        <title>Manage User Access</title>

    </head>

    <%--<table width="1000" cellpadding="0" cellspacing="0">
    <tr>
      <td width="85" align="left" valign="middle" class="style8">Information</td>
      <td width="11" align="center" valign="middle"><img src="images/arrow.png" width="10" /></td>
      <td width="95" align="center" valign="middle" class="style8">User Access</td>
     
     <td width="850" align="center" valign="middle"></td>
    </tr>
  </table>--%>


    <style type="text/css">
        .mGrid1 {
            width: 100%;
            background-color: #fff;
            border: solid 1px #525252;
            border-collapse: collapse;
        }

            .mGrid1 td {
                padding: 5px;
                border: solid 1px #c1c1c1;
                color: Black;
                text-transform: uppercase;
            }

            .mGrid1 th {
                padding: 8px 5px;
                color: #595130;
                background: #e9f7bc;
                border-left: solid 1px #525252;
                font-size: 0.9em;
                text-transform: uppercase;
                font-size: 12px;
            }

            .mGrid1 .alt {
                background: #fcfcfc url(grd_alt.png) repeat-x top;
            }

            .mGrid1 .pgr {
                background: url(../images/nav-back.gif) repeat-x top;
            }

                .mGrid1 .pgr table {
                    margin: 5px 0;
                }

                .mGrid1 .pgr td {
                    border-width: 0;
                    padding: 0 6px;
                    border-left: solid 1px #666;
                    font-weight: bold;
                    color: #fff;
                    line-height: 12px;
                }

                .mGrid1 .pgr a {
                    color: #C1C1C1;
                    text-decoration: none;
                }

                    .mGrid1 .pgr a:hover {
                        color: #000;
                        text-decoration: none;
                    }
    </style>
    <style type="text/css">
        .b_submit input[type="submit"]:disabled {
            color: Black;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <center>
        <div class="formmenu">
            <div class="loginform">
                <ul class="quicktabs_tabs quicktabs-style-excel">
                    <li class="qtab-Demo first" id="li_1"><a class="qt_tab active" href="UserAccess.aspx">User Access Master</a> </li>
                    <li class="qtab-HTML active last" id="li_9"><a class="qt_tab active" href="javascript:void(0)">Manage User Access</a> </li>

                </ul>
                <div class="container box" id="main">

                    <div class="form" id="div_form">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" valign="top">
                                    <table width="98%" cellpadding="0" cellspacing="0" class="form">
                                        <br />
                                        <tr>
                                            <td colspan="6"></td>
                                        </tr>
                                        <tr>
                                            <td width="8%" valign="top" valign="middle" class="form-group">
                                                <label for="adate" class="req">Select User :</label>
                                            </td>
                                            <td width="20%">
                                                <asp:DropDownList ID="ddlUsers" runat="server" CssClass="field" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            
                                                <td align="left" colspan="4" valign="top">
                                                            <asp:Button ID="btnSearch" runat="server" Text="Search"
                                                                CssClass="textbutton b_submit" OnClick="btnSearch_Click" />
                                                        </td>
                                            
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="left" valign="top" style="font-weight: bold; color:red">
                                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="6" align="left" valign="top"><span style="margin-left: 100px;">

                                                <asp:GridView ID="gridUser" CssClass="mGrid1" runat="server" AllowPaging="true" CellPadding="5" AutoGenerateColumns="false" CellSpacing="100" OnRowCommand="gridUser_RowCommand" OnPageIndexChanging="gridUser_PageIndexChanging" EmptyDataText="No records found..." PageSize="10">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Sr. No.">
                                                            <ItemTemplate>
                                                                <%# Container.DataItemIndex + 1 %>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="2%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="User Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkUName" runat="server" Text='<%# Eval("UName")%>' CommandArgument='<%# Eval("uRole") %>'
                                                                    CommandName="Access" Style="text-decoration: none; color: Blue; font-weight: normal;"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="User Type" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblutype" runat="server" Text='<% #Eval("UserType") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="NMobile No." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lboMob" runat="server" Text='<% #Eval("uContact") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="DOB" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDOB" runat="server" Text='<% #Eval("uDOB") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="left" valign="middle">&nbsp;</td>
                                        </tr>
                                        <tr id="tr1" runat="server" visible="false">
                                            <td colspan="6">
                                                <table style="width: 100%;" cellpadding="5px">
                                                    <tr>
                                                        <td>
                                                            <center>
                                                                <strong>Access Rights List</strong>
                                                            </center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="left" valign="top" style="font-weight: bold; color:red">
                                                <asp:Label ID="lblMsg2" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="left" valign="top"><span style="margin-left: 100px;">
                                                <div id="divAccess" runat="server" visible="false">
                                                    <asp:GridView ID="grvRights" CssClass="mGrid1" runat="server" AllowPaging="true" EmptyDataText="No records found..." PageSize="10" CellPadding="5" AutoGenerateColumns="false" CellSpacing="100" OnPageIndexChanging="grvRights_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Sr. No.">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex + 1 %>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="4%" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Menu Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMenuName" runat="server" Text='<% #Eval("MenuType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Display Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDisplayName" runat="server" Text='<% #Eval("DisplayName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    </div>
                                                    </span>
                                                
                                            </td>
                                        </tr>
                                        <!--/end container box1-->

                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div style="height: 313px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </center>
</asp:Content>
