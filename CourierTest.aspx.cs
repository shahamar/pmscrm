﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CourierTest : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL, strSQLAks, strSQL;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string Uroll = "0";
    string msg = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (Session["urole"] != null)
            Uroll = Session["urole"].ToString();

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            BindCourierDetailsGrid();
            FillData();
        }
    }
    private void BindCourierDetailsGrid()
    {
        object Frmdate = null;
        object Todate = null;

        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        Todate = todate[2] + "/" + todate[1] + "/" + todate[0];
        //string[] frmdate = txtfrm.Text.ToString().Split('-');
        //string[] todate = txtto.Text.ToString().Split('-');
        //Frmdate = frmdate[2] + "-" + frmdate[1] + "-" + frmdate[0];
        //Todate = todate[2] + "-" + todate[1] + "-" + todate[0];

        StrSQL = "Proc_GetPatientCourierDetails";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FrmDate", Frmdate);
        SqlCmd.Parameters.AddWithValue("@ToDate", Todate);
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_Courier.DataSource = dt;
                GVD_Courier.DataBind();
            }
            else
            {
                GVD_Courier.DataSource = null;
                GVD_Courier.DataBind();
            }
            conn.Close();
            dr.Dispose();

        }
    }

    protected void GVD_Courier_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "PdName")
        {
            string[] arg = new string[4];
            arg = e.CommandArgument.ToString().Split('_');

            string pdid = arg[0];
            string fdid = arg[1];
            string pdName = arg[2];
            string pdCasePaperNo = arg[3];
            string Address = arg[4];
            string Mob = arg[5];
            string Email = arg[6];
            txtPdName.Text = pdName;
            txtCasePNo.Text = pdCasePaperNo;
            txtReceiverName.Text = pdName;
            txtAddress.Text = Address;
            txtMobile.Text = Mob;
            txtEmail.Text = Email;

            ViewState["pdid"] = pdid;
            ViewState["fdid"] = fdid;
        }
    }

    protected void GVD_Courier_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVD_Courier.PageIndex = e.NewPageIndex;
        BindCourierDetailsGrid();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string finyear = gen.GetCurrentFinYear();
        string cmdReceiptNo = "";
        object o = gen.executeScalar("Select CONVERT(VARCHAR,ISNull(MAX(CAST(SubString(cmdReceiptNo,1,len(cmdReceiptNo)-4) As INT)),0)+1)+'" + finyear + "' From dbo.tblCourierMedicinsDetails where cmdCreatedDate > '2016-04-01 00:00:00.001'");
        cmdReceiptNo = o.ToString();


        strSQL = "Proc_InsCourierDetails";

        string[] _CDate = txtCourierDate.Text.ToString().Split('/');
        string CDate = _CDate[2] + "/" + _CDate[1] + "/" + _CDate[0];

        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@fdPatientId", Convert.ToInt32(ViewState["pdid"]));
        SqlCmd.Parameters.AddWithValue("@fdId", Convert.ToInt32(ViewState["fdid"]));
        SqlCmd.Parameters.AddWithValue("@cmdName", txtReceiverName.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdAddress", txtAddress.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdMobileNo", txtMobile.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdEmailId", txtEmail.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdDate", CDate);
        SqlCmd.Parameters.AddWithValue("@cmId", DDCourier.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@cmdDocketNo", txtDocketNo.Text.ToString());
        if (txtCharges.Text != "")
        {
            SqlCmd.Parameters.AddWithValue("@cmdCharges", txtCharges.Text.ToString());
        }
        else
        {
            SqlCmd.Parameters.AddWithValue("@cmdCharges", 0);
        }
        SqlCmd.Parameters.AddWithValue("@fdCreatedBy", uid);
        SqlCmd.Parameters.AddWithValue("@cmdReceiptNo", cmdReceiptNo);

        SqlCmd.Parameters.AddWithValue("@CourierPaymentMode", DDL_Pay_Mode.SelectedValue);



        SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;

        if (con.State == ConnectionState.Closed)
            con.Open();

        SqlCmd.ExecuteNonQuery();
        int result;
        result = (int)SqlCmd.Parameters["@Error"].Value;
        if (result == 0)
        {
            if (txtMobile.Text.Trim() != "" && txtDocketNo.Text.Trim() != "")
            {
                String smsstring;

                SqlCmd = new SqlCommand();
                smsstring = "" + txtReceiverName.Text.ToString() + " Your medicine has been dispatched from aditya homoeopathic hospital on " + txtCourierDate.Text.ToString() + " via " + DDCourier.SelectedItem.Text + " courier with tracking (CN) number " + txtDocketNo.Text + "... For any queries contact nontariff number 33004444... visit us at www.drnikam.com";
                decimal TemplateId = 1207160931676032697;
                genral.GetResponse(txtMobile.Text.Trim(), smsstring, TemplateId);
                //genral.GetResponse("9821479720", smsstring);

                SqlCmd.CommandText = "sp_insSmsTransaction";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = con;
                SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                SqlCmd.Parameters.AddWithValue("@MobileNo", txtMobile.Text.Trim());
                SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                SqlCmd.Parameters.AddWithValue("@SenderID", uid);
                SqlCmd.Parameters.AddWithValue("@Pdid", Convert.ToInt32(ViewState["pdid"]));
                SqlCmd.Parameters.AddWithValue("@Sources", "Followup");
                SqlCmd.ExecuteNonQuery();
            }
            //if (chkIsReconsulting.Checked == true)
            //{
            //    CrystalReportViewer2.Visible = true;
            //    string AksConn = ConfigurationManager.AppSettings["constring"];
            //    bindReport(AksConn, pdId);
            //}

            //if (fdId == "0")
            //{
            //    //Response.Redirect("ProfarmaOfCase.aspx", false);
            //    Response.Redirect("Home.aspx?MSG=Follow-up details saved successfully");
            //}
            //else
            //{
            //    //Response.Redirect("FollowupHistory.aspx", false);
            //    Response.Redirect("Home.aspx?MSG=Follow-up details Modify successfully");
            //}

            //ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('Courier Details Saved Successfully..');</script>");
            Response.Redirect("Home.aspx?MSG=Courier Details Saved Successfully");

        }
        //string script = "alert('Records Saved Successfully....');";
        //ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
        //return;

        SqlCmd.Dispose();
        con.Close();
    }

    public void FillData()
    {

        DDCourier.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select cmID,cmCourierName From tblCourierMaster", "cmID", "cmCourierName", DDCourier);
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindCourierDetailsGrid();
        //Bind_FollowUpPaymentDetails_ExtraDoses();
    }
}