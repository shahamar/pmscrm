﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EECPReports : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    string uid = "0";
    string tempDate = "";
    string tempDoctor = "";
    int Page_no = 0, Page_size = 20;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }
        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();
            BindGridView();
        }

        if (!(Session["Report"] as string == "{CrystalDecisions.CrystalReports.Engine.ReportDocument}"))
        {
            CrystalReportViewer2.ReportSource = (ReportDocument)Session["Report"];
        }

    }

    protected void FillData()
    {
        DDL_Doctor.Items.Insert(0, new ListItem("Select", "%"));
        gen.FillDropDownList("Select uId , uFname +' '+ uMname +' '+ uLname As DNAME from tblUser Where IsDelete = 0 and uRole = 2 ", "uId", "DNAME", DDL_Doctor);
        //DDL_Doctor.SelectedValue = uid;

    }
    private void BindGridView()
    {
        //lblSubTotal.Text = "0.00";
        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];
        StrSQL = "Proc_GetEECPReports";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@frmDate", Frmdate);
        SqlCmd.Parameters.AddWithValue("@toDate", Todate);
        SqlCmd.Parameters.AddWithValue("@Doctor", DDL_Doctor.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            //SqlCmd.Dispose();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                pnlHead.Visible = true;
                rptHead.Visible = true;
                GRV1.DataSource = dt;
                GRV1.DataBind();
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
            else
            {
                pnlHead.Visible = false;
                rptHead.Visible = false;
                export.Style.Add("display", "none");
                lblGridHeader.Text = "No record found";
            }
        }
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGridView();
        ScriptManager1.RegisterPostBackControl(this.imgexport);
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

}