﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserLogedInReport : System.Web.UI.Page
{
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            BindGrid(0);
            Session["flag"] = 0;
        }
    }

    private void BindGrid(int flag)
    {
        //string[] frmdate = txtfrm.Text.ToString().Split('/');

        //string Logindate = frmdate[2] + "-" + frmdate[1] + "-" + frmdate[0];

        strSQL = "Proc_GetUserLogedinreport";
        SqlCommand SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            SqlCmd.Parameters.AddWithValue("@date", txtfrm.Text);
            SqlCmd.Parameters.AddWithValue("@flag", flag);
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GRV.DataSource = dt;
                GRV.DataBind();
            }
            else
            {
                GRV.DataSource = null;
                GRV.DataBind();
            }
            con.Close();
            dr.Dispose();
        }
    }
    protected void GRV_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        Session["flag"] = 1;
        BindGrid(1);
    }

    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "UserLogedinReport_'"+DateTime.Now.ToString("dd-MM-yyyy")+"'.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRV.AllowPaging = false;
        this.BindGrid(Convert.ToInt32(Session["flag"]));
        GRV.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
}