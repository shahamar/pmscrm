﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;    

/// <summary>
/// Summary description for AutoCompleteService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class AutoCompleteService : System.Web.Services.WebService {

    public AutoCompleteService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public List<string> GetPatientDetails(string username)
    {
        List<string> result = new List<string>();
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ToString()))
        {
            using (SqlCommand cmd = new SqlCommand("Select PName +' - '+ WardTypeName +' - '+ wmWardNo +' - '+ pdCasePaperNo AS PName from VW_GetWardNBedStatus_Test where PName LIKE '" + username + "%' or WardTypeName LIKE '" + username + "%' or wmWardNo LIKE '" + username + "%' or pdCasePaperNo LIKE '" + username + "%' ", con))
            {
                con.Open();
                ///cmd.Parameters.AddWithValue("@SearchText", username);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(dr["PName"].ToString());
                }
                return result;
            }
        }
    }
}
