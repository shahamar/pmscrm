﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucene.Net.Index;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Store;
using System.IO;
using Lucene.Net.Search;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for Index
/// </summary>
public class Index
{
    //genral gen=new genral();
    public static void AppendIndex(string IndexPath, string Contents, string ParentId, string Title, string SectionId, string Id, string indexId)
    {
        string strIndexDir = IndexPath;
        Lucene.Net.Store.Directory indexDir = Lucene.Net.Store.FSDirectory.Open(new System.IO.DirectoryInfo(strIndexDir));
        Lucene.Net.Analysis.Analyzer std = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30); //Version parameter is used for backward compatibility. Stop words can also be passed to avoid indexing certain words
        IndexWriter idxw = new IndexWriter(indexDir, std, System.IO.Directory.GetFiles(IndexPath).Length == 0 ? true : false, IndexWriter.MaxFieldLength.UNLIMITED); //Create an Index writer object.
        Lucene.Net.Documents.Document doc = new Lucene.Net.Documents.Document();
        Lucene.Net.Documents.Field indexidText = new Lucene.Net.Documents.Field("indexid", indexId, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(indexidText);
        Lucene.Net.Documents.Field idText = new Lucene.Net.Documents.Field("id", Id, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(idText);
        Lucene.Net.Documents.Field fldText = new Lucene.Net.Documents.Field("text", Contents, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(fldText);
        Lucene.Net.Documents.Field parentid = new Lucene.Net.Documents.Field("parentid", ParentId, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(parentid);
        Lucene.Net.Documents.Field title = new Lucene.Net.Documents.Field("title", Title, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(title);
        Lucene.Net.Documents.Field sectionid = new Lucene.Net.Documents.Field("sectionid", SectionId, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(sectionid);
        try
        {
            //write the document to the index
            idxw.AddDocument(doc);
            //optimize and close the writer
            //  idxw.Optimize();
        }
        catch
        {

        }
        finally
        {
            idxw.Close();
        }
    }
    public static void ModifyIndex(string IndexPath, string Contents, string Title, string ParentId, string SectionId, string Id, string indexId)
    {
        Lucene.Net.Util.Version LuceneVersion = Lucene.Net.Util.Version.LUCENE_30;
        var IndexLocationPath = IndexPath; // Set to your location
        var directoryInfo = new DirectoryInfo(IndexLocationPath);
        var directory = FSDirectory.Open(directoryInfo);
        var writer = new IndexWriter(directory,
                    new StandardAnalyzer(LuceneVersion),
                    false, // Don't create index
                    IndexWriter.MaxFieldLength.LIMITED);
        Lucene.Net.Documents.Document doc = new Lucene.Net.Documents.Document();
        Lucene.Net.Documents.Field indexidText = new Lucene.Net.Documents.Field("indexid", indexId, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(indexidText);
        Lucene.Net.Documents.Field idText = new Lucene.Net.Documents.Field("id", Id, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(idText);
        Lucene.Net.Documents.Field fldText = new Lucene.Net.Documents.Field("text", Contents, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(fldText);
        Lucene.Net.Documents.Field title = new Lucene.Net.Documents.Field("title", Title, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(title);
        Lucene.Net.Documents.Field parentid = new Lucene.Net.Documents.Field("parentid", ParentId, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(parentid);
        Lucene.Net.Documents.Field sectionid = new Lucene.Net.Documents.Field("sectionid", SectionId, Lucene.Net.Documents.Field.Store.YES, Lucene.Net.Documents.Field.Index.NOT_ANALYZED, Lucene.Net.Documents.Field.TermVector.YES);
        doc.Add(sectionid);
        try
        {
            writer.UpdateDocument(new Term("indexid", indexId), doc);
            //  writer.Optimize(); // Should be done with low load only ...
        }
        catch { }
        finally { writer.Close(); }
    }
    public static void DeleteChildIndex(string IndexPath, string indexId)
    {
        Lucene.Net.Util.Version LuceneVersion = Lucene.Net.Util.Version.LUCENE_30;
        var IndexLocationPath = IndexPath; // Set to your location
        var directoryInfo = new DirectoryInfo(IndexLocationPath);
        var directory = FSDirectory.Open(directoryInfo);
        var writer = new IndexWriter(directory,
                    new StandardAnalyzer(LuceneVersion),
                    false, // Don't create index
                    IndexWriter.MaxFieldLength.LIMITED);
        try
        {
            writer.DeleteDocuments(new Term("indexid", indexId));
            //    writer.Optimize(); // Should be done with low load only ...
        }
        catch { }
        finally
        {
            writer.Close();
        }
    }
    public static void DeleteParentIndex(string IndexPath, string ParentId, string SectionId)
    {
        Lucene.Net.Util.Version LuceneVersion = Lucene.Net.Util.Version.LUCENE_30;
        var IndexLocationPath = IndexPath; // Set to your location
        var directoryInfo = new DirectoryInfo(IndexLocationPath);
        var directory = FSDirectory.Open(directoryInfo);
        var writer = new IndexWriter(directory,
                    new StandardAnalyzer(LuceneVersion),
                    false, // Don't create index
                    IndexWriter.MaxFieldLength.LIMITED);

        //   writer.DeleteDocuments(new Term("parentid", ParentId));

        List<Term> t = new List<Term>();
        t.Add(new Term("parentid", ParentId));
        t.Add(new Term("sectionid", SectionId));

        try
        {
            writer.DeleteDocuments(t.ToArray());
            //   writer.Optimize(); // Should be done with low load only ...
        }
        catch { }
        finally { writer.Close(); }
    }

    public static void InsSearchIndex(string IndexId, string SectionId, string ParentId, string desc, out int indxId)
    {
        genral gen = new genral();
        SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@IndexId",genral.ToDBNull(IndexId)),
                new SqlParameter("@SectionId",SectionId),
                new SqlParameter("@PatientId",ParentId),
                new SqlParameter("@sdescription",desc)
            };
        string _out = "@output";
        indxId = gen.ExecuteSp_Out_Para("proc_InsSearchIndex", param, _out);
    }

    public static DataTable GetIndexId(int SectionId, int patientId)
    {
        SqlDataReader dr;
        SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
        con.Open();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "proc_GetIndexId";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.AddWithValue("@SectionId", SectionId);
        cmd.Parameters.AddWithValue("@PatientId", patientId);
        dr = cmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        con.Close();
        return dt;
    }

    public static int DeleteSearchIndex(string IndexId)
    {
        genral gen = new genral();
        SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@IndexId",IndexId)
            };
         bool result = gen.executeNonQuery("Proc_DelSearchindex", param);
        return result==false?-1:1;
    }

    public static bool InsKCO(int pdid)
    {
        bool result = false;
        genral gen = new genral();
        if (!gen.doesExist("Select * From tblKco Where kpdid=" + pdid.ToString()))
        {
            string query = "Insert Into tblKco(kpdid,kword,kcreatedby)VALUES(" + pdid.ToString() + ",'',1)";
            result = gen.executeQuery(query);
        }
        return result;
    }

    public static bool InsChiefCO(int pdid)
    {
        bool result = false;
        genral gen = new genral();
        if (!gen.doesExist("Select * From tblChiefCO Where cpdid=" + pdid.ToString()))
        {
            string query = "Insert Into tblChiefCO(cpdid,cword,ccreaatedby)VALUES(" + pdid.ToString() + ",'',1)";
            result = gen.executeQuery(query);
        }
        return result;
    }

    public static bool InsFamilyHo(int pdid)
    {
        bool result = false;
        genral gen = new genral();
        if (!gen.doesExist("Select * From tblFamilyHo Where fhpdid=" + pdid.ToString()))
        {
            string query = "Insert Into tblFamilyHo(fhpdid,fhword,fhcreatedby)VALUES(" + pdid.ToString() + ",'',1)";
            result = gen.executeQuery(query);
        }
        return result;
    }

    public static bool InsInvestigation(int pdid)
    {
        bool result = false;
        genral gen = new genral();
        if (!gen.doesExist("Select * From tblInvestigation Where ivpdid=" + pdid.ToString()))
        {
            string query = "Insert Into tblInvestigation(ivpdid,ivword,ivcreatedby)VALUES(" + pdid.ToString() + ",'',1)";
            result = gen.executeQuery(query);
        }
        return result;
    }

    public static bool InsAF(int pdid)
    {
        bool result = false;
        genral gen = new genral();
        if (!gen.doesExist("Select * From tblAF Where afpdid=" + pdid.ToString()))
        {
            string query = "Insert Into tblAF(afpdid,afword,afcreatedby)VALUES(" + pdid.ToString() + ",'',1)";
            result = gen.executeQuery(query);
        }
        return result;
    }

    public static bool InsMind(int pdid)
    {
        bool result = false;
        genral gen = new genral();
        if (!gen.doesExist("Select * From tblMind Where mpdid=" + pdid.ToString()))
        {
            string query = "Insert Into tblMind(mpdid,mword,mcreatedby)VALUES(" + pdid.ToString() + ",'',1)";
            result = gen.executeQuery(query);
        }
        return result;
    }

    public static bool InsPastHO(int pdid)
    {
        bool result = false;
        genral gen = new genral();
        if (!gen.doesExist("Select * From tblPastHO Where phpdid=" + pdid.ToString()))
        {
            string query = "Insert Into tblPastHO(phpdid,phword,phcreatedby)VALUES(" + pdid.ToString() + ",'',1)";
            result = gen.executeQuery(query);
        }
        return result;
    }

    public static bool InsPhysicalGenerals(int pdid)
    {
        bool result = false;
        genral gen = new genral();
        if (!gen.doesExist("Select * From tblPhysicalGenerals Where pdpdid=" + pdid.ToString()))
        {
            string query = "Insert Into tblPhysicalGenerals(pdpdid,pgword,pgcreatedby)VALUES(" + pdid.ToString() + ",'',1)";
            result = gen.executeQuery(query);
        }
        return result;
    }

    public static bool InsThermal(int pdid)
    {
        bool result = false;
        genral gen = new genral();
        if (!gen.doesExist("Select * From tblThermal Where tpdid=" + pdid.ToString()))
        {
            string query = "Insert Into tblThermal(tpdid,tword,tcreatedby)VALUES(" + pdid.ToString() + ",'',1)";
            result = gen.executeQuery(query);
        }
        return result;
    }       

}