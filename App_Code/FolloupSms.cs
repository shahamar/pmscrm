﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for FolloupSms
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class FolloupSms : System.Web.Services.WebService {

    public FolloupSms () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public void SmsFolloup()
    {
        PatientFolloupSms();
    }

    private void PatientFolloupSms()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
        con.Open();
        DateTime TodayDate = DateTime.Today;
        string CurrentDate = TodayDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        SqlCommand cmd = new SqlCommand("select DATEDIFF ( d, fdDate , fdnextfollowupDate ) as Date_Diff,fdnextfollowupDate,CONVERT(DATE,DATEADD(Day, - 5, fdnextfollowupDate)) AS 'ReminderDate',isnull(PD.pdInitial,'') +' '+ isnull(PD.pdFname,'')+' '+ isnull(PD.pdMname,'') +' '+ isnull(PD.pdLname,'') As 'pdName',PD.pdMob from tblFollowUpDatails FU inner Join tblPatientDetails PD ON FU.fdPatientId=PD.pdID where fdnextfollowupDate !=''", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet dtCopy = new DataSet();
        da.Fill(dtCopy);
        con.Close();
        DataTable dt = new DataTable();
        dt = dtCopy.Tables[0];
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i <= dtCopy.Tables[0].Rows.Count - 1; i++)
            {
                if (Convert.ToInt32(dt.Rows[i]["Date_Diff"]) >= 15)
                {
                    string FDate = dt.Rows[i]["fdnextfollowupDate"].ToString();
                    DateTime followdate = Convert.ToDateTime(FDate);
                    string FollowupDate = followdate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);                  
                    string RDate = dt.Rows[i]["ReminderDate"].ToString();
                    DateTime date = Convert.ToDateTime(RDate);
                    string ReminderDate = date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    if (ReminderDate == CurrentDate)
                    {
                        //DateTime Date1 = DATEADD(Day, -30, Date);
                        string Name = dt.Rows[i]["pdName"].ToString();
                        string MobNo = dt.Rows[i]["pdMob"].ToString();
                        decimal TemplateId = 1207160931653621645;
                        string FolloupSms = "Dear "+ Name + ", This is a reminder you have an Appointment with Aditya Homoeopathic Hospital and Healing Center on "+ FollowupDate+".";
                        genral.GetResponse(MobNo, FolloupSms, TemplateId);
                    }
                }
            }
            //genral.GetResponse("8268429946", "Dear Sanjay Yadav, This is a reminder you have an Appointment with JDS Multi Super-Speciality Homoeopathic Hospital and Research Center on " + Convert.ToDateTime(dt.Rows[0]["fdnextfollowupDate"].ToString()).ToShortDateString() + "");
            //genral.GetResponse("9833821300", "Dear Saurabh Raut, This is a reminder you have an Appointment with JDS Multi Super-Speciality Homoeopathic Hospital and Research Center on " + Convert.ToDateTime(dt.Rows[0]["fdnextfollowupDate"].ToString()).ToShortDateString() + "");
        }
    }
    
}
