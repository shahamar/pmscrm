using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Net;


/// <summary>
/// Summary description for genral
/// </summary>
public class genral
{

    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);

    public genral()
    {

    }

    public DataTable getDataTable(string query)
    {
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;

        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;

        }
        finally
        {
            con.Close();
        }
    }

    public DataTable getDataTable(SqlCommand cmd)
    {
        try
        {

            cmd.Connection = con;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }

    }

    public bool doesExist(string query)
    {
        bool ret = false;
        try
        {
            con.Open();
            SqlCommand sc = new SqlCommand(query, con);
            SqlDataReader dr = sc.ExecuteReader();
            if (dr.Read())
            {
                ret = true;

            }
            else
            {
                ret = false;
            }
            con.Close();
            dr.Close();

            return ret;

        }
        catch (Exception ex)
        {
            con.Close();
            throw ex;

        }
        finally
        {
            con.Close();
        }
    }

    ///<summary>
    /// To Fill the DropDown. Must have 0th column as value and 1st column as id. 
    ///</summary>


    public void FillDropDown(DropDownList ddl, DataTable dt)
    {
        foreach (DataRow drow in dt.Rows)
        {
            ddl.Items.Add(new ListItem(drow[1].ToString(), drow[0].ToString()));
        }
    }

    public void FillDropDownList(string query, string idfield, string valuefield, DropDownList ddl)
    {
        DataTable dt = this.getDataTable(query);

        foreach (DataRow drow in dt.Rows)
        {
            ddl.Items.Add(new ListItem(drow[valuefield].ToString(), drow[idfield].ToString()));
        }
    }

    public void FillDropDown(DropDownList ddl, DataTable dt, string idfield, string valuefield)
    {
        foreach (DataRow drow in dt.Rows)
        {
            ddl.Items.Add(new ListItem(drow[valuefield].ToString(), drow[idfield].ToString()));
        }
    }

    public object executeScalar(string query)
    {
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            Object o = cmd.ExecuteScalar();
            con.Close();
            return o;
        }
        catch (Exception ex)
        {
            string a = ex.Message;
            return null;
        }
    }

    public bool executeQuery(string query)
    {
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            con.Close();
            return true;

            /* OleDbConnection con = new OleDbConnection(System.Configuration.ConfigurationManager.AppSettings["connection"]);
               con.Open();
               OleDbCommand cmd = new OleDbCommand(query, con);
               cmd.ExecuteNonQuery();
               con.Close();
               return true;
            */
        }
        catch (Exception ex)
        {
            throw ex;
            string a = ex.Message;
            return false;
        }
    }

    public String executeQueryThrowException(string query)
    {
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
            con.Close();
            return "success";

            /* OleDbConnection con = new OleDbConnection(System.Configuration.ConfigurationManager.AppSettings["connection"]);
               con.Open();
               OleDbCommand cmd = new OleDbCommand(query, con);
               cmd.ExecuteNonQuery();
               con.Close();
               return true;
            */
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    public object ExecuteAndReturnValue(string query)
    {
        object o = null;
        SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
        con.Open();
        SqlCommand sqlcmd = new SqlCommand(query, con);
        SqlDataReader reader = sqlcmd.ExecuteReader();
        if (reader.Read())
        {
            o = reader[0];
        }

        reader.Close();
        con.Close();
        sqlcmd.Dispose();

        return o;



    }

    public string GetCurrentFinYear()
    {
        string cFinYear = "";
        string dnow = "";
        string dnow1 = "";

        int cMonth = DateTime.Now.Month;
        if (cMonth <= 3)
        {

            dnow = DateTime.Now.AddYears(-1).ToString("yy");
            dnow1 = DateTime.Now.ToString("yy");

        }
        else
        {

            dnow = DateTime.Now.ToString("yy");
            dnow1 = DateTime.Now.AddYears(1).ToString("yy");

        }
        cFinYear = dnow + dnow1;
        return cFinYear;

    }

    public string getdateAks(string date)
    {
        string[] dt = date.Split('/');
        if (dt.Length == 3)
            return dt[1] + "/" + dt[0] + "/" + dt[2];
        else
            return "";
    }

    public string getdate(string inputdate, char seperator)
    {
        string[] _date = inputdate.Split(seperator);
        string outputdate = _date[2] + "/" + _date[1] + "/" + _date[0];

        return outputdate;
    }

    public string getreversedate(string inputdate, char seperator)
    {
        string[] _date = inputdate.Split(seperator);
        string outputdate = _date[1] + "/" + _date[0] + "/" + _date[2];
        return outputdate;
    }

    public string[] getTime(string inputtime)
    {
        string hh, mm, ss, ampm;
        string[] _time1 = inputtime.Split(' ');
        string[] _time2 = _time1[0].Split(':');
        ampm = _time1[1];
        hh = _time2[0];
        mm = _time2[1];
        ss = _time2[2];

        string[] outputtime = { hh, mm, ss, ampm };

        return outputtime;


    }

    public bool executeNonQuery(string spname, SqlParameter[] param)
    {
        return true; ;
    }


    public IEnumerable<string> FindInFiles(string directoryArg, string containsTextArg, bool ignoreCaseArg, System.IO.SearchOption searchSubDirsArg, params string[] fileWildcardsArg)
    {
        List<String> files = new List<string>(); // This List accumulates files found.

        foreach (string fileWildcard in fileWildcardsArg)
        {
            string[] xFiles = System.IO.Directory.GetFiles(directoryArg, fileWildcard, searchSubDirsArg);

            foreach (string x in xFiles)
            {
                if (!files.Contains(x)) // If file not already found...
                {
                    // See if the file contains the search text.
                    // Assume a null search string matches any file.
                    // Use ToLower to perform a case-insensitive search.
                    bool containsText =
                        containsTextArg.Length == 0 ||
                        ignoreCaseArg ?
                        System.IO.File.ReadAllText(x).ToLower().Contains(containsTextArg.ToLower()) :
                        System.IO.File.ReadAllText(x).Contains(containsTextArg);

                    if (containsText)
                    {
                        files.Add(x); // This file is a keeper. Add it to the list.
                    } // if
                } // if
            } // foreach file
        }//foreach wildcard 
        return files;
    }


    public int ExecuteSp_Out_Para(string spname, SqlParameter[] param, string _out)
    {
        SqlCommand cmd = new SqlCommand(spname, con);
        cmd.CommandType = CommandType.StoredProcedure;
        foreach (SqlParameter sqlParam in param)
        {
            cmd.Parameters.Add(sqlParam);
        }

        cmd.Parameters.Add(_out, SqlDbType.Int).Direction = ParameterDirection.Output;

        if (con.State == ConnectionState.Closed)
            con.Open();
        cmd.ExecuteNonQuery();
        int id = Convert.ToInt32((cmd.Parameters[_out].Value.ToString()));
        return id;
    }

    public int ExecuteSp(string spname, SqlParameter[] param)
    {
        SqlCommand cmd = new SqlCommand(spname, con);
        cmd.CommandType = CommandType.StoredProcedure;
        foreach (SqlParameter sqlParam in param)
        {
            cmd.Parameters.Add(sqlParam);
        }

        if (con.State == ConnectionState.Closed)
            con.Open();
        int i = cmd.ExecuteNonQuery();

        return i;
    }

    public static object ToDBNull(object value)
    {
        if (null != value)
            return value;
        return DBNull.Value;
    }

    //function to convrert date into 'mm/dd/yyyy'
    public static string getdate(string date)
    {
        string[] dt = date.Split('/');
        if (dt.Length == 3)
            return dt[1] + "/" + dt[0] + "/" + dt[2];
        else
            return "";
    }


    public static string GetResponse(string celnos, string msg, decimal TemplateId)
    {
        //string URL = "http://dndopen.dove-sms.com/TransSMS/SMSAPI.jsp?username=adityahosp&password=aditya&sendername=DRNIKM&mobileno=" + celnos + "&message= " + msg + "";
        //string URL = "http://admint.dove-sms.com/WebSMS/SMSAPI.jsp?username=adityahosp&password=aditya&sendername=DRNIKM&mobileno=" + celnos + "&message= " + msg + "";
        //string sURL = "http://ocs-sms.com/submitsms.jsp?user=adityaho&key=fa69f9a31cXX&mobile=" + celnos + "&message=" + msg + "&senderid=DRNIKM&accusage=1&MT=4";
      
        //string URL = "http://smspanel.ocs.net.in/sendsms.jsp?user=adityaho&password=adityaho&mobiles=" + celnos + "&sms=" + value2 + "&senderid=DRNIKM";
        string value2 = msg.Replace("&", "AND");
        //////string URL = "http://174.143.34.193/SendSMS/SingleSMS.aspx?usr=saurabh.raut@ocs.net.in&pass=drnikam&msisdn=" + celnos + "&msg=" + value2 + "&sid=DRNIKM" + "&mt=4";
        string URL = "http://ocs-sms.com/submitsms.jsp?user=adityaho&key=fa69f9a31cXX&mobile=" + celnos + "&message=" + msg + "&senderid=DRNIKM&accusage=1&entityid=1201159897232502035&tempid=" + TemplateId;
        //"http://174.143.34.193/SendSMS/SingleSMS.aspx?usr=saurabh.raut@ocs.net.in&pass=drnikam&msisdn=" + celnos + "&msg=" + value2 + "&sid=DRNIKM" + "&mt=4";
       

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
        request.MaximumAutomaticRedirections = 4;
        request.Credentials = CredentialCache.DefaultCredentials;
        try
        {
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
            string sResponse = readStream.ReadToEnd();
            response.Close();
            readStream.Close();
            return sResponse;
        }
        catch
        {
            return "";
        }
    }

    public  DataTable UserAccess(int UserId, string PageUrl)
    {
        SqlCommand cmd = new SqlCommand("Proc_GetCheckUserAccessForMaster", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@UserId", UserId);
        cmd.Parameters.AddWithValue("@Text", PageUrl);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }


}
