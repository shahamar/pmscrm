﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="ManageRemedy.aspx.cs" Inherits="ManageRemedy" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />

    <style>
        #tbsearch td
        {
            text-align: left;
            vertical-align: middle;
        }
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        #gvcol div
        {
            margin-top: -10px;
        }
        .head1
        {
            width: 15%;
        }
        .head2
        {
            width: 20%;
        }
        .head3
        {
            width: 10%;
        }
        .head4
        {
            width: 10%;
        }
        .head5
        {
            width: 15%;
        }
        .head5i
        {
            width: 15%;
        }
        .head6
        {
            width: 10%;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div>
  <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Remedy Master</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo first" id="li_1"><a class="qt_tab active" href="RemedyMaster.aspx">
                                            Remedy Master</a> </li>
                                        <li class="qtab-HTML active last" id="li_9"><a class="qt_tab active" href="javascript:void(0)">
                                            Remedy Test</a> </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto;">
                                        <div style="padding: 15px; height: auto;">
                                            <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                <tr>
                                                    <td colspan="2" style="text-align: center; color: Red;">
                                                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: right; color: Red;">
                                                        <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                            <tr>
                                                                <th class="head1">
                                                                    Remedy Name
                                                                </th>
                                                                
                                                                  <th class="head6">
                                                                    Action
                                                                </th>


                                                            </tr>
                                                            <tr>
                                                                <th class="head1">
                                                                    <asp:TextBox ID="txtRemedy" runat="server" class="field" Width="137px"></asp:TextBox>
                                                                </th>
                                                              
                                                                <th class="head6">
                                                                    <div style="display: block; width: 68px; margin-left: 14px;">
                                                                        <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none; color: #FFF;
                                                                            text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click">
                                                                <img src="images/filter.png" style="border:0px;"/>
                                                                Filter</asp:LinkButton>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gvcol" colspan="2">
                                                            <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" ShowHeader="false" Width="100%"
                                                                OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%# Eval("RMNAME")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                   
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("RMId") %>'
                                                                                CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                            <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("RMId") %>'
                                                                                CommandName="del" ImageUrl="Images/delete.ico" OnClientClick="return confirm('Are you sure you want to delete this?');"
                                                                                ToolTip="Delete" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                      
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
</div>
</asp:Content>

