﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SendSMS : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string uid;
    string userid;
    int Page_no = 0, Page_size = 20;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");
        if (!IsPostBack)
        {
            //BindGVR();
        }
    }
    public void BindGVR()
    {
        try
        {
            strSQL = "Select pd.pdId, pdPatientICardNo,p.pdName, pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,IsNull(pdOccupation,'') pdOccupation,CASE  WHEN pdIsRegistrationFeesPaid In (1,3) THEN '<a href=''ConsultingBill.aspx?id='+CAST(pd.pdID AS VARCHAR)+'''& target=_blank><img src=''images/print1.png'' /></a>' else '' END AS 'PrintBill',pdCassetteNo, Case When pdRCasePaperNo is NULL Then '' when pdRCasePaperNo = '0' Then '' else +' - '+ pdRCasePaperNo end 'pdRCasePaperNo',ISNULL(pdMob,'') AS 'pdMob' From dbo.tblPatientDetails pd left join vwGetPatient p on pd.pdID=p.pdid where IsCancel = 0 Order by pdDOC desc";

            DataTable dt = gen.getDataTable(strSQL);
            if (dt.Rows.Count > 0)
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
            else
            {
                GRV1.EmptyDataText.ToString();
                GRV1.DataBind();
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        BindGVR();
    }
    protected void GRV_doctors_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV_doctors.PageIndex = e.NewPageIndex;
        BindGVR_Doctors();
    }
    protected void chk_patient_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_patient.Checked)
        {
            chk_doctors.Checked = false;
            gvcol_doctor.Visible = false;
            gvcol_patient.Visible = true;
            BindGVR();
        }
    }

    protected void chk_doctors_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_doctors.Checked)
        {
            chk_patient.Checked = false;
            gvcol_patient.Visible = false;
            gvcol_doctor.Visible = true;
            BindGVR_Doctors();
        }
    }

    public void BindGVR_Doctors()
    {
        try
        {
            strSQL = "Select did, dFName+' '+ dMName +' '+ dLName As 'FullName',DOB,dContactNo,dMobileNo,dDOJ,dType,dEmailId From tblDoctorMaster where 1=1";

            DataTable dt = gen.getDataTable(strSQL);
            GRV_doctors.DataSource = dt;
            GRV_doctors.DataBind();
            createpaging(dt.Rows.Count, GRV_doctors.PageCount);
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnsendsms_Click(object sender, EventArgs e)
    {
        if (chk_patient.Checked)
        {
            strSQL = "Select pd.pdId, pdPatientICardNo,p.pdName, pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,IsNull(pdOccupation,'') pdOccupation,CASE  WHEN pdIsRegistrationFeesPaid In (1,3) THEN '<a href=''ConsultingBill.aspx?id='+CAST(pd.pdID AS VARCHAR)+'''& target=_blank><img src=''images/print1.png'' /></a>' else '' END AS 'PrintBill',pdCassetteNo, Case When pdRCasePaperNo is NULL Then '' when pdRCasePaperNo = '0' Then '' else +' - '+ pdRCasePaperNo end 'pdRCasePaperNo',ISNULL(pdMob,'') AS 'pdMob' From dbo.tblPatientDetails pd left join vwGetPatient p on pd.pdID=p.pdid where IsCancel = 0 Order by pdDOC desc";

            DataTable dt = gen.getDataTable(strSQL);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count - 1; i++)
                {
                    string Name = dt.Rows[i]["pdName"].ToString();
                    string MobileNo = dt.Rows[i]["pdMob"].ToString();
                    if (MobileNo != "" || MobileNo != null)
                    {
                        //string patientMsg = "Dear " + Name + ", " + txt_message.Text.ToString();
                        //genral.GetResponse(MobileNo, patientMsg);
                    }
                }

            }
            //genral.GetResponse("8268429946", txt_message.Text.ToString());
        }
        else if (chk_doctors.Checked)
        {
            strSQL = "Select did, dFName+' '+ dMName +' '+ dLName As 'FullName',DOB,dContactNo,dMobileNo,dDOJ,dType,dEmailId From tblDoctorMaster where 1=1";

            DataTable dt = gen.getDataTable(strSQL);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count - 1; i++)
                {
                    string DName = dt.Rows[i]["FullName"].ToString();
                    string DMobileNo = dt.Rows[i]["dMobileNo"].ToString();
                    if (DMobileNo != "" || DMobileNo != null)
                    {
                        //string DpatientMsg = "Dear " + DName + ", " + txt_message.Text.ToString();
                        //genral.GetResponse(DMobileNo, DpatientMsg);
                    }
                }
            }
            //genral.GetResponse("8268429946", txt_message.Text.ToString());
        }
        else
        {
            Response.Write("<script>alert('Please select atleast one checkbox.');</script>");
        }
    }
}