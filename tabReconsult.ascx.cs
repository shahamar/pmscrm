﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class tabReconsult : System.Web.UI.UserControl
{
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string strSQL = "";
    SqlDataReader Sqldr;
    SqlCommand SqlCmd;
    genral gen = new genral();
    public int fdId
    {
        get
        {
            if (ViewState["fdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["fdId"].ToString());
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["fdId"] != null)
                ViewState["fdId"] = Session["fdId"].ToString();

            FillData();
        }
    }

    private void FillData()
    {
        strSQL = "SELECT * FROM [dbo].[vwConsultingHistory]  where fdId = " + fdId;
        DataTable dt = gen.getDataTable(strSQL);
        if (dt.Rows.Count > 0)
        {
            lblAccute.Text = dt.Rows[0]["fdReconsulting"].ToString();
            lblCassetteNo.Text = dt.Rows[0]["fdCasseteNo"].ToString();
            lblDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(dt.Rows[0]["fdDate"].ToString()));
            lblDose.Text = dt.Rows[0]["fdDoseType"].ToString();
            lblRemedy.Text = dt.Rows[0]["fdRemedy"].ToString();
            lblRemedyPeriod.Text = dt.Rows[0]["fdRemedyPeriod"].ToString();
        }
    }
}