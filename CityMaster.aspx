﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="CityMaster.aspx.cs" Inherits="CityMaster" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                City Master</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">
                                            City Master</a> </li>
                                        <li class="qtab-HTML last" id="li_9"><a class="qt_tab active" href="ManageCityMaster.aspx">
                                            Manage City</a> </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto;">
                                        <table style="width: 100%; padding-left: 20px" cellspacing="0px" cellpadding="0px">
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    State *
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="DDLState" runat="server" CssClass="field required">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="DDLState"
                                                        Display="Dynamic" ErrorMessage="State is required." ValidationGroup="val">
                                                        <span class="error">State is required.</span></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                          
                                          
                                            <tr>
                                                <td>
                                                   City :
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txt_City" runat="server" CssClass="field required"
                                                        Style="float: left;"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>


                                          
                                          
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                        Style="margin-top: 2px;" Text="Save" ValidationGroup="val" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

