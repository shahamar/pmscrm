﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;

public partial class AF : System.Web.UI.Page
{
    //string pdId = "";
    genral gen = new genral();
    string uid;
    string strSQL,msg;
    //added by nikita on 12th march 2015------------------------------
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-----------------------------------------------------------------

    //added by nikita on 8th april 2015--------------------------------
    public int fdId
    {
        get
        {
            if (ViewState["fdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["fdId"].ToString());
        }
    }
    //-----------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 26";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            //---Added By Nikita On 30thMarch 2015----------------------------------------------------
            if (msg == "1")
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }
            //-----------------------------------------------------------------------------------------

            //added by nikita on 8th april 2015--------------------------------------------------------
            if (Session["fdId"] != null)
                ViewState["fdId"] = Session["fdId"].ToString();
            //-----------------------------------------------------------------------------------------

            FillAF();
        }
    }
    public void FillAF()
    {
        if (fdId == 0)
        {
            if (File.Exists(Server.MapPath("Files/AF/af_" + pdId)))
            {
                kco.Content = File.ReadAllText(Server.MapPath("Files/AF/af_" + pdId).ToString());
            }
           // txtKco.Text = Convert.ToString(gen.executeScalar("Select afword from tblAf Where afpdid=" + pdId));
        }
        if (fdId != 0)
        {
            div1.Style.Add("display", "block");
            if (File.Exists(Server.MapPath("ReconsultingFiles/AF/af_" + fdId)))
            {
                kco.Content = File.ReadAllText(Server.MapPath("ReconsultingFiles/AF/af_" + fdId).ToString());
            }
        }
        else
        {
            div1.Style.Add("display", "none");
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
       // int inxId;
        if (fdId == 0)
        {
            //if (kco.Content.ToString() != "")
            //{
                if (File.Exists(Server.MapPath("Files/AF/af_" + pdId)))
                {
                    File.Delete(Server.MapPath("Files/AF/af_" + pdId));
                }
                File.WriteAllText(Server.MapPath("Files/AF/af_" + pdId), kco.Content.ToString());
                savekco();

                ////---------------Added by Apeksha-----------------------------
                //DataTable dt = Index.GetIndexId(8, pdId);
                //foreach (DataRow dr in dt.Rows)
                //{
                //    Index.DeleteChildIndex(Server.MapPath("~/App_Data/index"), dr["IndexId"].ToString());
                //    Index.DeleteSearchIndex(dr["IndexId"].ToString());
                //}
                //Index.InsSearchIndex(null, "8", pdId.ToString(), "", out inxId);
                //Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/AF/af_" + pdId))), pdId.ToString(), "", "8", "", inxId.ToString());
                ////-------------------------------------------------------------


                Response.Redirect("Thermal.aspx");
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('There is no text in editor');", true);
            //}
        }
        else
        {
            //if (kco.Content.ToString() != "")
            //{
                if (File.Exists(Server.MapPath("ReconsultingFiles/AF/af_" + fdId)))
                {
                    File.Delete(Server.MapPath("ReconsultingFiles/AF/af_" + fdId));
                }
                File.WriteAllText(Server.MapPath("ReconsultingFiles/AF/af_" + fdId), kco.Content.ToString());
                
                //DataTable dt = Index.GetIndexId(17, fdId);
                //foreach (DataRow dr in dt.Rows)
                //{
                //    Index.DeleteChildIndex(Server.MapPath("~/App_Data/index"), dr["IndexId"].ToString());
                //    Index.DeleteSearchIndex(dr["IndexId"].ToString());
                //}
                //Index.InsSearchIndex(null, "17", fdId.ToString(), "", out inxId);
                //Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("ReconsultingFiles/AF/af_" + fdId))), fdId.ToString(), "", "17", "", inxId.ToString());
              
                Response.Redirect("Thermal.aspx");
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('There is no text in editor');", true);
            //}
        }
    }
    protected void timaf_Tick(object sender, EventArgs e)
    {
        if (kco.Content.ToString() != "")
        {
            if (File.Exists(Server.MapPath("Files/AF/af_" + pdId)))
            {
                File.Delete(Server.MapPath("Files/AF/af_" + pdId));
            }
            File.WriteAllText(Server.MapPath("Files/AF/af_" + pdId), kco.Content.ToString());
        }
    }

    private void savekco()
    {
        try
        {
            string str = System.Text.RegularExpressions.Regex.Replace(kco.Content.ToString(), @"(<(.|\n)*?>|&nbsp;)", string.Empty);

            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@word",str),
                new SqlParameter("@pdid",pdId),
                new SqlParameter("@createdby",uid),
                new SqlParameter("@type",8)//8 for AF 
            };

            int id = gen.ExecuteSp("sp_KeyWords", param);
        }
        catch (Exception ex)
        {

        }
    }
}
