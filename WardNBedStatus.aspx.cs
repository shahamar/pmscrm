﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Collections.Generic;
using System.Drawing;
using AjaxControlToolkit;

public partial class WardNBedStatus : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlCommand SqlCmd1;
    string StrSQL;
    string StrSQL1;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn1 = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    genral gen1 = new genral();
    string uid = "0";
    DataTable dt = new DataTable();
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (!IsPostBack)
        {
            
            BindWardNBedStatusGridAks();
            BindWaitingListGrid();
        }

    }

    private void BindCommonSubGrid(GridView grdWard, Label lblwardTypeId, string Type)
    {
        StrSQL1 = "Proc_GetWardNBedStatus_Test";
        SqlCmd1 = new SqlCommand(StrSQL1, conn1);
        SqlDataReader dr1;
        SqlCmd1.CommandType = CommandType.StoredProcedure;
        SqlCmd1.Parameters.AddWithValue("@Flag", Type);
        SqlCmd1.Parameters.AddWithValue("@ID", lblwardTypeId.Text);
        if (conn1.State == ConnectionState.Open)
        {
            conn1.Close();
        }

        if (conn1.State == ConnectionState.Closed)
        {
            conn1.Open();
            dr1 = SqlCmd1.ExecuteReader();
            DataTable dt1 = new DataTable();
            dt1.Load(dr1);
            if (dt1.Rows.Count > 0)
            {
                grdWard.DataSource = dt1;
                grdWard.DataBind();
            }
            conn1.Close();
            dr1.Dispose();

        }
    }

    private void BindWardNBedStatusGridAks()
    {
        StrSQL = "Proc_GetWardNBedStatus_Test";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Flag", "WardType");
        SqlCmd.Parameters.AddWithValue("@ID", "");
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVDWardNBedStatus.DataSource = dt;
                GVDWardNBedStatus.DataBind();
            }
            conn.Close();
            dr.Dispose();

        }
    }

    private void BindWaitingListGrid()
    {

        StrSQL = "Proc_GetWaitingPatientList";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            //DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                export.Style.Add("display", "block");
                GRVWaitingList.DataSource = dt;
                GRVWaitingList.DataBind();
            }
            conn.Close();
            dr.Dispose();

        }

    }

    protected void GVDWardNBedStatus_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
        {
            GridView grdWard = (GridView)e.Row.FindControl("Grd_Ward");
            Label lblwardTypeID = (Label)e.Row.FindControl("lblWardTypeID");
            BindCommonSubGrid(grdWard, lblwardTypeID, "Ward");

        }
    }

    protected void Grd_Ward_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
        {
            GridView grdBed1 = (GridView)e.Row.FindControl("Grd_Bed1");
            Label lblWard1 = (Label)e.Row.FindControl("lblWard1");
            BindCommonSubGrid(grdBed1, lblWard1, "Bed");

            GridView grdBed2 = (GridView)e.Row.FindControl("Grd_Bed2");
            Label lblWard2 = (Label)e.Row.FindControl("lblWard2");
            BindCommonSubGrid(grdBed2, lblWard2, "Bed");
        }
    }

    protected void Grd_Bed1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "IPDDetails")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                string ipdid = arg[1];

                Session["CasePatientID"] = pdID;
                Session["ipdid"] = ipdid;
                //Response.Redirect("IPDView.aspx", false);
                Response.Redirect("AddIPDRound.aspx", false);

            }
            catch
            { }
        }
    }

    protected void Grd_Bed2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "IPDDetails")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                string ipdid = arg[1];

                Session["CasePatientID"] = pdID;
                Session["ipdid"] = ipdid;
                //Response.Redirect("IPDView.aspx", false);
                Response.Redirect("AddIPDRound.aspx", false);
            }
            catch
            { }
        }
    }

    protected void btn_CaseSearch_Click(object sender, EventArgs e)
    {
        string pdID = "";
        string IPDID = "";
        pdID = gen.executeScalar("SELECT COALESCE( (SELECT PDID FROM tblPatientDetails WHERE IsCancel = 0 And pdcasepaperno = '" + txtCasePaperno.Text + "'), '')").ToString();

        // pdID = gen.executeScalar("Select PDID from tblPatientDetails Where pdcasepaperno = '" + txtCasePaperno.Text + "'").ToString();

        if (pdID != "" && pdID != "0")
        {
            IPDID = gen.executeScalar("SELECT COALESCE( (Select ipdId from tblIpdMaster Where ipdDischargeDate is null And IsDelete = 0 And ipdpdId = '" + pdID + "'), '')").ToString();
            // IPDID = gen.executeScalar("Select ipdId from tblIpdMaster Where ipdDischargeDate is null And IsDelete = 0 And ipdpdId = '" + pdID + "'").ToString();

            if (IPDID != "0" && IPDID != "")
            {
                Session["ipdid"] = IPDID;
                Session["CasePatientID"] = pdID;
                Response.Redirect("IPDViewAks.aspx?DISPROW=ALL", false);
            }
            else
            {
                if (pdID != "")
                {
                    Session["ipdid"] = null;
                    Session["CasePatientID"] = pdID;
                    Response.Redirect("IPD.aspx", false);
                }
            }
        }
    }

    protected void btn_PatSearch_Click(object sender, EventArgs e)
    {
        string pdID = "";
        string IPDID = "";

        int memID = 0;
        if (hdnInsId.Value.Trim() != "")
            memID = hdnInsId.Value == "" ? 0 : Convert.ToInt32(hdnInsId.Value);

        pdID = Convert.ToString(memID);

        if (pdID != "" && pdID != "0")
        {
            IPDID = gen.executeScalar("SELECT COALESCE( (Select ipdId from tblIpdMaster Where ipdDischargeDate is null And IsDelete = 0 And ipdpdId = '" + pdID + "'), '')").ToString();
            // IPDID = gen.executeScalar("Select ipdId from tblIpdMaster Where ipdDischargeDate is null And IsDelete = 0 And ipdpdId = '" + pdID + "'").ToString();

            if (IPDID != "0" && IPDID != "")
            {
                Session["ipdid"] = IPDID;
                Session["CasePatientID"] = pdID;
                Response.Redirect("IPDViewAks.aspx?DISPROW=ALL", false);
            }
            else
            {
                if (pdID != "")
                {
                    Session["ipdid"] = null;
                    Session["CasePatientID"] = pdID;
                    Response.Redirect("IPD.aspx", false);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Invalid Patient Name')", true);
                }
            }
        }
    }



    protected void GRVWaitingList_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        try
        {
            lblMsg.Text = "";

            if (e.CommandName == "IPD")
            {
                //int wID = Convert.ToInt32(e.CommandArgument);
                //if (wID != null)
                //{
                GridViewRow gvRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                DropDownList ddWard = (DropDownList)gvRow.FindControl("ddWard");
                HiddenField hdnWard = (HiddenField)gvRow.FindControl("hdnWard");
                HiddenField hdnwID = (HiddenField)gvRow.FindControl("hdnwID");
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                Session["CasePatientID"] = pdID;
                Session["WaitingPatientID"] = hdnwID.Value;


                Session["Ward"] = hdnWard.Value;

                int pdId = Convert.ToInt32(e.CommandArgument);
                int cnt = Convert.ToInt32(gen.executeScalar("SELECT COALESCE( (Select ipdId from tblIpdMaster Where ipdDischargeDate is null And IsDelete = 0 And ipdpdId = '" + pdID + "'),'')").ToString());

                if (cnt > 0)
                {

                    Response.Redirect("IPDViewAks.aspx?DISPROW=ALL", false);
                }
                else
                {
                    Response.Redirect("IPD.aspx", false);
                }
                //}
            }


            if (e.CommandName == "done")
            {
                int wID = Convert.ToInt32(e.CommandArgument);
                if (wID != null)
                {
                    GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);

                    DropDownList ddWard = (DropDownList)gvRow.FindControl("ddWard");
                    ImageButton btnDone = (ImageButton)gvRow.FindControl("btnDone");
                    ImageButton btnEdit = (ImageButton)gvRow.FindControl("btnEdit");
                    HiddenField hdnWard = (HiddenField)gvRow.FindControl("hdnWard");
                    Label lblResult = (Label)gvRow.FindControl("lblResult");
                    TextBox txtMob = (TextBox)gvRow.FindControl("txtMob");
                    Label txtpdid = (Label)gvRow.FindControl("txtpdid");

                    TextBox txtDOW = (TextBox)gvRow.FindControl("txtDOW");
                    CalendarExtender txtDOW_CalenderExtender = (CalendarExtender)gvRow.FindControl("txtDOW_CalenderExtender");

                    Session["Ward"] = ddWard.SelectedValue;
                    hdnWard.Value = ddWard.SelectedValue;

                    if (ddWard.SelectedValue == "")
                    {
                        lblResult.Visible = true;
                        lblResult.Text = "Compulsory";
                        lblResult.ForeColor = Color.DarkRed;
                        return;
                    }
                    else
                    {
                        lblResult.Visible = true;
                        lblResult.Text = "";
                    }

                    txtDOW_CalenderExtender.Enabled = false;
                    btnDone.Visible = ddWard.Enabled = txtDOW.Enabled = txtMob.Enabled = false;
                    btnEdit.Visible = true;

                    
                    string[] _dow = txtDOW.Text.ToString().Split('/');
                    string dow = _dow[2] + "/" + _dow[1] + "/" + _dow[0];

                    SqlCmd = new SqlCommand();
                    SqlCmd.CommandText = "Proc_UpdatePaitientWaitingList";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.AddWithValue("@wID", wID);
                    SqlCmd.Parameters.AddWithValue("@wWardTypeId", Session["Ward"]);
                    SqlCmd.Parameters.AddWithValue("@wCreatedBy", uid);
                    SqlCmd.Parameters.AddWithValue("@wBookingDate", dow);
                    SqlCmd.Parameters.AddWithValue("@pdID", txtpdid.Text.ToString());
                    SqlCmd.Parameters.AddWithValue("@Mob",txtMob.Text.ToString());
                    SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
                    SqlCmd.Connection = conn;
                    if (conn.State == ConnectionState.Closed)
                        conn.Open();
                    
                    SqlCmd.ExecuteNonQuery();
                    conn.Close();
                    lblMsg.Text = "Record Updated Successfully...!";
                    Response.Redirect("WardNBedStatus.aspx", true);

                }

            }
            if (e.CommandName == "del")
            {
                int wID = Convert.ToInt32(e.CommandArgument);
                if (wID != null)
                {
                    SqlCmd = new SqlCommand();

                    SqlCmd.CommandText = "Proc_DeletePaitientWaitingList";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.AddWithValue("@wID", wID);
                    SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
                    SqlCmd.Connection = conn;
                    if (conn.State == ConnectionState.Closed)
                        conn.Open();

                    SqlCmd.ExecuteNonQuery();
                    conn.Close();
                    Response.Redirect("WardNBedStatus.aspx", true);
                }

            }

            if (e.CommandName == "editRow")
            {
                int wID = Convert.ToInt32(e.CommandArgument);
                if (wID != null)
                {
                    GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    DropDownList ddWard = (DropDownList)gvRow.FindControl("ddWard");
                    ImageButton btnDone = (ImageButton)gvRow.FindControl("btnDone");
                    ImageButton btnEdit = (ImageButton)gvRow.FindControl("btnEdit");
                    TextBox txtMob = (TextBox)gvRow.FindControl("txtMob");
                    TextBox txtDOW = (TextBox)gvRow.FindControl("txtDOW");
                    CalendarExtender txtDOW_CalenderExtender = (CalendarExtender)gvRow.FindControl("txtDOW_CalenderExtender");

                    txtDOW_CalenderExtender.Enabled = true;
                    btnDone.Visible = ddWard.Enabled = txtDOW.Enabled = txtMob.Enabled = true;
                    btnEdit.Visible = false;
                }
                
            }
        }
        catch (Exception ex)
        {


        }

    }

    protected void GRVWaitingList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
        {
            DropDownList ddWard = (DropDownList)e.Row.FindControl("ddWard");
            HiddenField hdnWard = (HiddenField)e.Row.FindControl("hdnWard");
            ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
            ImageButton btnDone = (ImageButton)e.Row.FindControl("btnDone");
            TextBox txtMob = (TextBox)e.Row.FindControl("txtMob");
            TextBox txtDOW = (TextBox)e.Row.FindControl("txtDOW");
            CalendarExtender txtDOW_CalenderExtender = (CalendarExtender)e.Row.FindControl("txtDOW_CalenderExtender");

            ddWard.DataSource = dt;
            ddWard.Items.Clear();

            ddWard.Items.Insert(0, new ListItem("Select", ""));

            gen.FillDropDownList("SELECT Distinct WardTypeID As wmWardTypeId , WardTypeName As wmType FROM tblWardTypeMaster Where IsDelete = 0", "wmWardTypeId", "wmType", ddWard);

            ddWard.SelectedValue = hdnWard.Value;
            if (ddWard.Text != "")
            {
                txtDOW_CalenderExtender.Enabled = false;
                ddWard.Enabled = txtDOW.Enabled = txtMob.Enabled = btnDone.Visible = false;
                btnEdit.Visible = true;
            }
            else
            {
                txtDOW_CalenderExtender.Enabled = true;
                ddWard.Enabled = txtDOW.Enabled = txtMob.Enabled = btnDone.Visible = true;
                btnEdit.Visible = false;
            }

        }


    }
    protected void GRVWaitingList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRVWaitingList.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindWaitingListGrid();
    }
    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "WaitingPatientList.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRVWaitingList.AllowPaging = false;
        this.BindWaitingListGrid();
        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Convert.ToString(Session["update"]);
    }

    [WebMethod]
    public static List<string> GetPatientDetails(string PName)
    {
        List<string> PNameResult = new List<string>();
        using (SqlConnection con = new SqlConnection((ConfigurationManager.AppSettings["constring"])))
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "Select pd.pdID,pdName,pdCasePaperNo,pdName+' - '+pdCasePaperNo Name ,wmWardNo,bmNoOfBads from dbo.tblPatientDetails pd LEFT JOIN vwGetPatient p on pd.pdID=p.pdid Inner Join tblIpdMaster IM On IM.ipdpdId = pd.pdID Inner Join tblPatientsWardDetails pwd On pwd.wipdId = IM.ipdId Left Join tblWardMaster WM On pwd.wNo = WM.wmID Left Join tblBadMaster BM On WM.wmID = BM.wmID And pwd.wBedNo = BM.bmID where IsCancel=0 And ipdDischargeDate is Null And ipdIsDischarge = 0 And wOutDate is Null And IM.IsDelete = 0 And IsNull(pwd.WIsDelete,0) = 0 and pdName LIKE REPLACE('%" + PName + "%', ' ' , '%') or pdCasePaperNo LIKE REPLACE('%" + PName + "%', ' ' , '%')";
                cmd.Connection = con;
                con.Open();
                //cmd.Parameters.AddWithValue("@SearchPatient", PName);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    //PNameResult.Add(dr["PName"].ToString());
                    //while (dr.Read())
                    //{
                    PNameResult.Add(string.Format("{0}${1}${2}${3}${4}${5}", dr["pdId"].ToString(), dr["pdName"].ToString(), dr["pdCasePaperNo"].ToString(), dr["Name"].ToString(), dr["wmWardNo"].ToString(), dr["bmNoOfBads"].ToString()));
                    //}
                }
                con.Close();
                return PNameResult;
            }
        }
    }
}
