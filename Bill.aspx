﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Bill.aspx.cs" Inherits="Bill" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Bill</title>
    <script type="text/javascript">
        function PrintDiv()
        {             
            w=window.open();
            w.document.write(document.getElementById("pnlBill").innerHTML);
            w.print();
            w.close();
            close();
        }
    </script>

    
<style type="text/css">
.address
{
	font-family: Calibri;
	font-size: 18px;
	border-bottom: solid 2px;
	text-align: center;
}
.Date
{
	font-family: Calibri;
	font-size: 12px;
	font-weight: bold;
	border-bottom: solid 2px;
	text-align: right;
}
.details
{
	font-family: Calibri;
	font-size: 14px;
	border-bottom: solid 2px;
	text-align: left;
}
.maintable
{
	border: solid 1px;
	border-radius: 5px;
	padding: 0px;
	font-family: Calibri;	
}
.reportname
{
	text-align: center;
	font-weight: bold;
	font-family: Calibri;
	font-size:22px;
	<%--padding-bottom:5px--%>
}
.reportname1
{
	text-align: center;
	border-bottom: solid thin;
	font-weight: bold;
	font-family: Calibri;	
	padding-bottom:5px
}
.label{	border-bottom: solid thin;}
</style>


</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align:center;">
        <table width="60%" align="center">
            <tr>
                <td align="right">
                    <img src="images/print.png" style="cursor:pointer;" onclick="PrintDiv()"/>
                </td>
            </tr>
        </table>
    </div>
    
    <div style="text-align:center;">
        <asp:Panel ID="pnlBill" runat="server">
        <style type="text/css">
            
            .sec1td1
            {
            	text-align:left;
            	width:15%;
                border-left: 1px solid #000000;
                border-top: 1px solid #000000;
                padding-left:10px;                
            }
            
            .sec1td2
            {
            	text-align:left;
            	width:85%;
                border-left: 1px solid #000000;
                border-top: 1px solid #000000;            	
                border-right: 1px solid #000000;
            }
            .recno
	    {
		width:20%;
		border-left: 1px solid #000000;
                
	    }
	    
            .col1
            {
            	text-align:left;
            	width:15%;            	      
                border-left: 1px solid #000000;
                border-top: 1px solid #000000;            	      	
            }
            
            .col2
            {
            	text-align:left;
            	width:30%;
                border-left: 1px solid #000000;
                border-top: 1px solid #000000;            	
		border-right: 1px solid #000000;
                
            }
             .age
	    {
		text-align:left;
            	width:12%;
                border-left: 1px solid #000000;
                border-top: 1px solid #000000;
            }
            .col3
            {
            	width:10%;
            	text-align:left;
                border-left: 1px solid #000000;
                border-top: 1px solid #000000;            	
            }
            
              .col4
            {
            	width:35%;
            	text-align:left;
                border-left: 1px solid #000000;
                border-top: 1px solid #000000;
            }
            
             .col5
            {
            	text-align:left;
            	width:15%;
                border-left: 1px solid #000000;
                border-top: 1px solid #000000; 
                border-right: 1px solid #000000;           	
            }
            
             .col6
            {
            	text-align:left;
            	width:25%;
                border-top: 1px solid #000000; 
                border-right: 1px solid #000000;           	
            }
            
            
            .head1
            {
            	width:10%;
            	text-align:right;
            	padding-right:5px;
            	border-top: 1px solid #000000; 
                border-right: 1px solid #000000;
                border-left: 1px solid #000000;
            }
            
            .head2
            {
            	width:51%;
            	text-align:left;
            	padding-left:2px;
            	border-top: 1px solid #000000; 
                border-right: 1px solid #000000;
            }            
            .head3
            {
            	width:13%;
            	text-align:right;
            	padding-right:2px;
            	border-top: 1px solid #000000; 
                border-right: 1px solid #000000;
                padding-right:10px;                
            }
            .head4
            {
            	width:13%;
            	text-align:right;
            	padding-right:2px;
            	border-top: 1px solid #000000; 
                border-right: 1px solid #000000;
                padding-right:10px;
               
            }
            .head5
            {
            	width:13%;
            	text-align:right;
            	padding-right:2px;
            	border-top: 1px solid #000000; 
                border-right: 1px solid #000000;
                padding-right:10px;
              
            }
            
            .head6
            {
            	text-align:left;
            	padding-left:2px;
            	border-top: 0px solid #000000; 
                border-right: 0px solid #000000;
                border-left: 1px solid #000000;
                vertical-align:bottom;
                padding-bottom:10px;
            
            }
            
              .headMy
            {
            	text-align:left;
            	padding-left:2px;
            	border-top: 1px solid #000000; 
                border-right: 1px solid #000000;
                border-left: 0px solid #000000;
                vertical-align:bottom;
                padding-bottom:10px;
            
            }
            
            .collast
            {
            	border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                font-family: Times New Roman;
                font-size: 14px;
                height: 66px;
                padding-bottom: 10px;
                padding-left: 10px;
                vertical-align: bottom;
                text-align:left;
            }
               .headAks1
            {
            	width:20%;
            	text-align:left;
            	padding-right:2px;
                border-bottom: 1px solid #000000; 
            }
              .headAks2
            {
            	width:13%;
            	text-align:right;
            	padding-right:2px;
                border-right: 1px solid #000000; 
            }
        
        </style>
                
        <table width="100%" align="center">
            <tr>
                <td align="center">
                    
                    <asp:Repeater ID="rptParent" runat="server" 
                        onitemdatabound="rptParent_ItemDataBound">
                        <ItemTemplate>
                            <table width="100%" align="center" cellspacing="0px" cellpadding="0px" >
                            
                                <tr>
                                                   <td align="center" style="border-left:1px solid #000000; border-right:1px solid #000000; border-top:1px solid #000000;"><font style="font-size:18px">
                                                   <table>
                                                   <tr>
                                                     <td>
                                                        <img src="images/Aditya_logo_final.png" width="65px" height="50px" />
                                                    </td>
                                                    <td style="padding-left:20px">
                                                        <b style="font-size:18px;"> Aditya Homoeopathic Hospital & Healing Center</b><br />
                                                        <span style="font-size:14px">DR. AMARSINHA D. NIKAM (M.D.Homoeopath)</span>
                                                    </td> 
                                                   </tr>
                                                   </table>
                                                   </td> 
                                                </tr> 
                                                <tr>
                                                	<td style="border-bottom:1px solid"></td>
                                                </tr> 
                                                
                                                <tr>
                                                    <td align="center" style="border-left:1px solid #000000; border-right:1px solid #000000;>
                                                    	<span style="font-size:13px"><strong>Address :</strong>  
                                                    	Old Kate Pimple Road,Near Shivaji Chawk,Pimprigaon,Pune – 411017 
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="border-left:1px solid #000000; border-right:1px solid #000000;">
                                                    	<span style="font-size:13px"><strong>Tel :</strong> 020-27412197 / 8806061061&nbsp;&nbsp;&nbsp;&nbsp;
                                                    	<strong>Website :</strong> www.drnikam.com</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td style="border-bottom:1px solid"></td>
                                                </tr>  
                                               							
                                                <tr>
						     <td>
						    <table width="100%" cellspacing="0px" cellpadding="2px" style="color: black; border: 0px solid black; font-size:14px;">
							<tbody>
								<tr>
									<td class="recno">Rec No. : <font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "bmNo")%></td>
									<td class="reportname" style="border-right:1px solid #000000; text-align:center;" width="80%"><b>IPD - Invoice</b></td>
								</tr>
							</tbody>
						    </table>
							</td>
                                                </tr>

                                <tr>
                                      <td>
                                        <table width="100%" id="tblsec1" cellspacing="0px" cellpadding="2px" style="color: black; border: 0px solid black; font-size:14px;">
                                            <tr>
                                                <%--<td class="col1">Rec. No :</td>
                                                 <td class="col2" colspan="5"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "bmNo")%> </font></td>
                                                 <td class="col5">Date :</td>
                                                <td class="col6"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "PrintDate", "{0:dd-MMM-yyyy}")%></font> </td>--%>
                                            </tr>
                                            
                                            <tr>
                                                <td class="col1">Pt Name :</td>
                                                <td class="col4"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "Name")%> </font></td>
                                                <td class="col3">Age :</td>
                                                <td class="age"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "pdAge")%></font> </td>
                                                <td class="col5">Case No. :</td>
                                                <td class="col6"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "pdCasePaperNo")%></font> </td>
                                            </tr>
                                            
                                              <tr>
                                                <td class="col1">Admit Dt:</td>
                                                <td class="col4"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "ipdAdmissionDate", "{0:dd-MMM-yyyy}")%></font></td>
                                                <td class="col3">Disch Dt :</td>
                                                <td class="age"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "ipdDischargeDate", "{0:dd-MMM-yyyy}")%> </font></td>
                                                <td class="col5">Total Days : </td>
                                                <td class="col6"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "TotalDays")%></font></td>
                                            </tr>

                                            <tr style="display:none;">
                                                <td class="sec1td1">Address :</td>
                                                <td class="sec1td2" colspan="3"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "Address")%> </font></td>
                                           </tr>
                                            
                                            <tr style="display:none;">
                                               <td class="col1">Sex :</td>
                                                <td class="col5"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "pdSex")%></font> </td>
                                            </tr>
                                            
                                             <tr>
                                                <td class="col1">Diagnosis :</td>
                                                <td class="sec1td2" colspan="5"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "ipdFinalDiagnosis")%> </font></td>
                                            </tr>
                                               
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family:Times New Roman;" align="center">
                                        <asp:Repeater ID="rptChild" runat="server">
                                               <HeaderTemplate>
                                                    <table width="100%" cellspacing="0px" cellpadding="2px" style="color: black; font-size:14px;">
                                                    <tr>
                                                        <th class="head1">Sr. No.</th>
                                                        <th class="head2">Pariculars</th>
                                                        <th class="head3">Rate</th>
                                                        <th class="head4">Unit/Days</th>
                                                        <th class="head5">Amount</th>
                                                    </tr>
                                               </HeaderTemplate>
                                                    
                                               <ItemTemplate>
                                                    <tr>
                                                        <td class="head1"><%# DataBinder.Eval(Container.DataItem, "bId")%></td>
                                                        <td class="head2"><%# DataBinder.Eval(Container.DataItem, "Particulars")%></td>
                                                        <td class="head3"><%# DataBinder.Eval(Container.DataItem, "BaseCharge")%></td>
                                                        <td class="head4"><%# DataBinder.Eval(Container.DataItem, "Units")%></td>
                                                        <td class="head5"><%# DataBinder.Eval(Container.DataItem, "TotAmt")%></td>
                                                    </tr>
                                               </ItemTemplate> 
                                               
                                               <FooterTemplate>
                                                    </table>    
                                               </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family:Times New Roman;" align="center">
                                        <table width="100%" cellspacing="0px" cellpadding="2px" style="color: black; font-size:14px;">
                                         
                                            <tr>
                                              <td class="headMy" style="border-left: 1px solid;" colspan="4">
                                        Amount in words :
                                        
                                        <%--<asp:Label ID="lblPaidAmt" runat="server" CssClass="label"></asp:Label>--%>
                                        <%# Eval("AmtWord")%> Only
                                                    
                                    </td>
                                                
                                             
                                             
                                                <td class="head4">Other :</td>
                                                <td class="head5"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "OtherCharges")%></font></td>
                                                
                                            </tr>
                                            
                                             <tr>
                                              <td class="head6" style="border-bottom:1px solid; width:29%"> 
                                              Prepared By : <%# DataBinder.Eval(Container.DataItem, "ipdPreparedBy")%>
                                                </td>

                                                   <td class="headAks1" colspan="1">&nbsp;
                                                Paid By <%# DataBinder.Eval(Container.DataItem, "Payment_Mode")%>
                                                </td>

                                                <td style="border-bottom:1px solid; border-Right:1px solid; width:25%;" colspan="2">
                                                Checked By : <%# DataBinder.Eval(Container.DataItem, "ipdCheckeddBy")%>
                                           <%--     </td>
                                               <td  style="border-bottom:1px solid;">
                                                </td>
                                               <td class="headAks2"  style="border-bottom:1px solid;">&nbsp;</td>--%>
                                                <td class="head4" style="border-bottom:1px solid;">Total :</td>
                                                <td class="head5" style="border-bottom:1px solid;"><%# DataBinder.Eval(Container.DataItem, "bmTotalAmt")%></td>
                                            </tr>
                                            
                                            
                                            <tr id="tr_Adv" runat="server">
                                               <td  class="head6">
                                                </td>
                                                <td>
                                                </td>
                                                  <td>
                                                </td>
                                                <td class="headAks2">&nbsp;</td>
                                                  <td class="head4">Advance :</td>
                                                <td class="head5"><%# DataBinder.Eval(Container.DataItem, "bmAdvance")%></td>
                                                
                                            </tr>
                                          
                                            <tr  id="tr_Bal" runat="server">
                                               <td  class="head6" style="border-bottom:1px solid;">
                                                </td>
                                                <td style="border-bottom:1px solid;">
                                                </td>
                                                 <td  style="border-bottom:1px solid;">
                                                </td>
                                                <td class="headAks2" style="border-bottom:1px solid #000000;">&nbsp;</td>
                                                <td class="head4" style="border-bottom:1px solid #000000;">Balance :</td>
                                                <td class="head5" style="border-bottom:1px solid #000000;"><font style="color:Black;"><%# DataBinder.Eval(Container.DataItem, "bmBalance")%></font></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                               
                            </table>    
                        </ItemTemplate>
                    </asp:Repeater>
                    
                </td>
            </tr>

            <tr id="rd_AdvTitle" runat="server">
            <td align="center">
            <br />
           <b> Advanced Details </b>
         
            </td>
            </tr>

              <tr id="rd_AdvData" runat="server">
                                        <td style="text-align: center;">
                                      
                                            <asp:Panel ID="Panel3" runat="server">
                                                <asp:GridView ID="GRD_Adv" runat="server" AllowPaging="False" AllowSorting="true" AutoGenerateColumns="False"
                                                    CssClass="mGrid" Width="100%">
                                                    <Columns>

                                                      <asp:TemplateField HeaderText="Dep. Date" ItemStyle-HorizontalAlign="center" ItemStyle-Width="20%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDepDate" runat="server" Text='<%# Eval("DepDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDepAmt" runat="server" Text='<%# Eval("DepAmt")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Payment Mode" ItemStyle-HorizontalAlign="center" ItemStyle-Width="30%">
                                                            <ItemTemplate>
                                                            <asp:Label ID="lblPayment_Mode" runat="server" Text='<%# Eval("Payment_Mode")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
								
                                                     		<asp:TemplateField HeaderText="Prepared by" ItemStyle-HorizontalAlign="center" ItemStyle-Width="30%">
                                                            <ItemTemplate>
                                                            <asp:Label ID="lblPreparedBy" runat="server" Text='<%# Eval("uName")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                           
                                        </td>
                                    </tr>

        </table>
        
        </asp:Panel>
    </div>
    </form>
</body>
</html>
