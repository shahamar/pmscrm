﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterHome1.master" CodeFile="CourierDetails.aspx.cs" Inherits="CourierDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<script type="text/javascript" src="js/custom-form-elements.js"></script>
<script type="text/javascript">
    function callscript() {
        alert("ok");
        Custom.init;
        alert("ok");
    }

</script>
<style type="text/css">
#tbsearch td { text-align: left; vertical-align: middle;}
#tbsearch label { display: inline; margin-top: 3px; position: absolute;}
#tbhead th
{
    text-align: left;
    background: url(images/nav-back.gif) repeat-x top;
    color: #FFFFFF;
    padding-left: 2px;
    padding-top: 3px;
}
#gvcol div {  margin-top: -10px;}
.head1 { width: 25%;}
.head2 { width: 10%;}
.head3 { width: 25%;}
.head4 { width: 10%;}
.head5 { width: 10%;}
.head6 { width: 10%;}
.head7 { width: 10%;}

.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}
</style>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <script type="text/javascript">
        function callscript() {
            alert("ok");
            Custom.init;
            alert("ok");
        }

    </script>
    <style type="text/css">
        #tbsearch td
        {
            text-align: left;
            vertical-align: middle;
        }
        
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        
        #gvcol div
        {
            margin-top: -10px;
        }
        
        .head1
        {
            width: 30%;
        }
        
        .head2
        {
            width: 30%;
        }
        
        .head3
        {
            width: 15%;
        }
        
        .head4
        {
            width: 15%;
        }
        
        .head5
        {
            width: 10%;
        }
        
        .head5i
        {
            width: 15%;
        }
        
        .head6
        {
            width: 10%;
        }
    </style>
</asp:Content>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Courier Details</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">Add Courier
                                            Details</a> </li>
                                        <li class="qtab-HTML last" id="li_9"><a class="qt_tab active" href="CourierView.aspx">
                                            View Courier Details</a> </li>
                                    </ul>
                                     <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                    <table style="width: 100%; background: #f2f2f2" cellspacing="0px" cellpadding="5px" border="0px">
                                    <tr>
                                        <td>
                                            <table style="width: 100%;" cellspacing="0px" cellpadding="0px">
                                                <tr>
                                                    <td><strong>From :&nbsp;</strong>
                                                        <asp:TextBox ID="txtfrm" runat="server" CssClass="field textbox"></asp:TextBox>
                                                        <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                            runat="server" Enabled="True" TargetControlID="txtfrm">
                                                        </cc1:CalendarExtender>
                                                        &nbsp;
                                                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                    </td>
                                                    <td><strong>To :&nbsp;</strong>
                                                        <asp:TextBox ID="txtto" runat="server" CssClass="field textbox"></asp:TextBox>
                                                        <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                            Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                        </cc1:CalendarExtender>
                                                        &nbsp;
                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="Td2" style="text-align: center; background: #fff; border: 0px solid;">
                                            <%--<font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                                <asp:Label ID="Label2" runat="server" Text="Extra Doses"></asp:Label></font>--%>
                                            <br />
                                            <asp:GridView ID="GVD_Courier" Width="100%" runat="server"
                                                AutoGenerateColumns="False"
                                                PageSize="5" OnRowCommand="GVD_Courier_RowCommand" AllowPaging="True"
                                                DataKeyNames="fdId" CssClass="mGrid"
                                                OnPageIndexChanging="GVD_Courier_PageIndexChanging"
                                                CellPadding="0" ForeColor="#333333">
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr. No.">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:BoundField DataField="SNO" HeaderText="Sr No." HeaderStyle-Font-Size="Small" />--%>
                                                    <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkNewCasepdName" runat="server" Text='<%# Eval("pdName")%>'
                                                                CommandArgument='<%# Eval("pdtext")%>'
                                                                CommandName="PdName" Style="text-decoration: none; font-weight: normal;"
                                                                ForeColor="Blue"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="pdCasePaperNo" HeaderText="Case Paper No." HeaderStyle-Font-Size="Small" />
                                                    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderText="DATE">
                                                        <ItemTemplate>
                                                            <%# Eval("fdDate", "{0:dd-MMM-yyyy}")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center" HeaderText="Pharmacy Done BY" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDoctors" runat="server" Text='<%# Eval("PharmacyDoneBY")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="RMNAME" HeaderText="Remedy" HeaderStyle-Font-Size="Small" />
                                                    <asp:BoundField DataField="RemedyPeriod" HeaderText="Remedy Period" HeaderStyle-Font-Size="Small" />
                                                    <asp:BoundField DataField="DoseType" HeaderText="Dose" HeaderStyle-Font-Size="Small" />
                                                </Columns>
                                                <EditRowStyle BackColor="#2461BF" />
                                                <HeaderStyle BackColor="#e9f7bc" Font-Bold="True" ForeColor="Black" />
                                                <PagerStyle BackColor="#e9f7bc" ForeColor="Black" HorizontalAlign="left" />
                                                <RowStyle BackColor="#F0F0F0" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                                <table style="width: 100%; background: #f2f2f2" cellspacing="0px" cellpadding="5px" border="0px">
                                    <tr>
                                        <td>Patient Name
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPdName" runat="server" CssClass="field textbox1" MaxLength="50" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td>Case Paper No.
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCasePNo" runat="server" CssClass="field textbox1" MaxLength="20" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Receiver Name
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtReceiverName" TextMode="MultiLine" Width="257px" runat="server"></asp:TextBox>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Address
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="field textbox1" TextMode="MultiLine" Width="255px" MaxLength="100"></asp:TextBox>
                                        </td>
                                        <td>Mobile No.
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMobile" runat="server" CssClass="field textbox1" MaxLength="10" onkeypress="return isNumberKey(event,this)"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="field textbox1"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regEmailID1" runat="server" ControlToValidate="txtEmail"
                                                CssClass="field required" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
                                                ValidationGroup="val" Display="Dynamic">
                                                    <span class="error">Please enter valid email Id</span>
                                            </asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="reg_EmailId1" runat="server" ControlToValidate="txtEmail"
                                                Display="Dynamic" CssClass="field required" ValidationExpression="^[\s\S]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Max 50 characters allowed</span>
                                            </asp:RegularExpressionValidator>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Courier Name
                                        </td>
                                        <td style="padding-left:8px">
                                            <%--<asp:TextBox ID="txtCourierName" runat="server" CssClass="field textbox1" MaxLength="100"></asp:TextBox>--%>
                                            <asp:DropDownList ID="DDCourier" runat="server" CssClass="field" Width="260px" Height="30px">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rcDDCourier" runat="server" Display="Dynamic" ControlToValidate="DDCourier"
                                              ValidationGroup="val" CssClass="reqPos" ErrorMessage="Courier Name Is Required."><span class="error">Courier Name Is Required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>Courier Date
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCourierDate" runat="server" Width="200px" Height="20px" CssClass="field"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtDOC_CalendarExtender" runat="server" Enabled="True"
                                                Format="dd/MM/yyyy" PopupButtonID="imgDOC" TargetControlID="txtCourierDate">
                                            </cc1:CalendarExtender>
                                            &nbsp;<asp:Image ID="imgDOC" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit; width: 18px; height: 18px; top: 5px; left: -5px;" />
                                            <asp:RequiredFieldValidator ID="req_DOC" runat="server" ControlToValidate="txtCourierDate"
                                                ValidationGroup="val" Display="Dynamic" ErrorMessage="Courier date is required!" CssClass="field required">
                                                    <span class="error">Courier date is required!</span>
                                            </asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regDOC" runat="server" CssClass="field required" ValidationGroup="val" Display="Dynamic"
                                                ControlToValidate="txtCourierDate" ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                                                    <span class="error">Enter valid date!</span></asp:RegularExpressionValidator>
                                            <asp:CustomValidator ID="cvDOC" runat="server" Display="Dynamic" ControlToValidate="txtCourierDate" ValidationGroup="val"
                                                CssClass="field required" ClientValidationFunction="CallDateFun" ErrorMessage="Please enter valid date!">
                                                    <span class="error">Please enter valid date!</span>
                                            </asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Docket No.
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDocketNo" runat="server" CssClass="field textbox1"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="txtDocketNo"
                                              ValidationGroup="val" CssClass="reqPos" ErrorMessage="Docket No. Is Required."><span class="error">Docket No. Is Required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>Charges
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCharges" runat="server" CssClass="field textbox1"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Payment Mode*
                                        </td>
                                        <td style="padding-left:8px">
                                            <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="260px" Height="30px" CssClass="field">
                                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                                <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                <%--<asp:ListItem Text="Other" Value="Other"></asp:ListItem>--%>
                                                <asp:ListItem Text="Free" Value="Free"></asp:ListItem>
                                                <asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ControlToValidate="DDL_Pay_Mode"
                                              ValidationGroup="val" CssClass="reqPos" ErrorMessage="Payment Mode Is Required."><span class="error">Payment Mode Is Required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                Style="margin-top: 2px;" Text="Submit" ValidationGroup="val" />
                                        </td>
                                    </tr>
                                </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
