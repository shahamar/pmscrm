﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;


public partial class SearchPatient : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string pdId = "0";
    string uid = "0";
    string ipdid = "0";
    string AppType = "";
    int Page_no = 0, Page_size = 10;
    string frm, to;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //if (Session["CasePatientID"] != null)
        //    pdId = Session["CasePatientID"].ToString();
        //else
        //    Response.Redirect("Home.aspx?flag=1");

        if (Request.QueryString["AppType"] != null)
        {
            AppType = Request.QueryString["AppType"].ToString();
            Session["AppType"] = AppType;
        }
        else
        {
            Session["AppType"] = "N";
        }



        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            Session["CasePatientID"] = null;
            Session["msg"] = null;
            Session["fdId"] = null;
            //BindGVR();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }

    public void BindGVR()
    {
        string[] _frm;
        string[] _to;

        if (txtfrm.Text.Contains("/"))
        {
            _frm = txtfrm.Text.ToString().Split('/');
            _to = txtto.Text.ToString().Split('/');

            frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";
        }

        if (txtfrm.Text.Contains("-"))
        {
            _frm = txtfrm.Text.ToString().Split('-');
            _to = txtto.Text.ToString().Split('-');

            frm = _frm[2] + "-" + _frm[1] + "-" + _frm[0] + " 00:00:00";
            to = _to[2] + "-" + _to[1] + "-" + _to[0] + " 23:55:00";
        }



        //strSQL = "Select pdId, pdPatientICardNo,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,pdCassetteNo,ISNULL(pdOccupation,'') pdOccupation,pdAdd1+' '+pdAdd2+' '+pdAdd3+' '+pdAdd4 As pdAdd,pdRCasePaperNo,CASE WHEN  kpdid IS NULL THEN 'NO' ELSE 'YES' END 'flag' From dbo.tblPatientDetails pd LEFT JOIN tblKco k ON k.kpdId = pd.pdID where IsCancel = 0";
        strSQL = "Select pd.pdId, pdPatientICardNo,p.pdName, pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,pdCassetteNo,ISNULL(pdOccupation,'') pdOccupation,pdAdd1 + ' ' + IIF(pdAdd2='Test','',pdAdd2) + ' ' + IIF(pdAdd3='Test','',pdAdd3) + ' ' + IIF(pdAdd4='Test','',pdAdd4) AS pdAdd,pdRCasePaperNo,CASE WHEN  kpdid IS NULL THEN 'NO' ELSE 'YES' END 'flag', ReconsultingNo From dbo.tblPatientDetails pd Left Join tblReconsultingDetails RD on RD.RecPatientId = pd.pdID LEFT JOIN tblKco k ON k.kpdId = pd.pdID left join vwGetPatient p on pd.pdID=p.pdid where IsCancel = 0";
        if (chkConsultingPeriod.Checked == true)
            strSQL += " AND pdDOC BETWEEN '" + frm + "' and '" + to + "'";

        if (txtAdd.Text != "")
            strSQL += " AND (pdAdd1+' '+pdAdd2+' '+pdAdd3+' '+pdAdd4) Like '%" + txtAdd.Text.ToString() + "%'";

        if (txtcasepaperno.Text.ToString() != "")
            strSQL += " AND pdCasePaperNo Like '%" + txtcasepaperno.Text.ToString() + "%'";

        if (txtMobno.Text.ToString() != "")
            strSQL += " AND pdTele + pdMob ='" + txtMobno.Text.ToString() + "'";

        if (txtname.Text.ToString() != "")
        {
            //strSQL += " AND pdFname+' '+ pdMname +' '+ pdLname like '%" + txtname.Text.Trim() + "%'";
            strSQL += " AND p.pdName like REPLACE('%" + txtname.Text.Trim() + "%', ' ' , '%') ";
        }

        if (txtReconsultingNo.Text.ToString() != "")
            strSQL += " And ReconsultingNo Like '%" + txtReconsultingNo.Text.ToString() + "%'";

        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.PageSize = Page_size;
        GRV1.DataBind();
        createpaging(dt.Rows.Count, GRV1.PageCount);
    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "case")
        {
            string pdID = e.CommandArgument.ToString();
            Session["ipdid"] = null;
            Session["CasePatientID"] = pdID;
            Response.Redirect("ProfarmaOfCase.aspx", false);
        }

        if (e.CommandName == "app")
        {
            string pdID = e.CommandArgument.ToString();
            GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            DropDownList ddlDoctor = (DropDownList)gvRow.FindControl("ddldoctors");
            int DUID = Convert.ToInt32(ddlDoctor.SelectedValue);
            int cnt = Convert.ToInt32(gen.executeScalar("Select Count(adid) As TotalApp From tblAppointmentDetails Where Pat_Type = 'O' And App_Cancel = 0 And CONVERT(date,AppointmentDate) = CONVERT(date, getdate()) And PatientID =" + pdID).ToString());

            if (cnt > 0)
            {
                string script = "alert('Appointment Already Generated For The Today ...!!!!');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                return;
            }

            Session["CasePatientID"] = pdID;
            //Session["DoctorID"] = DUID;
            Response.Redirect("OldPatientAppointment.aspx?DID=" + DUID, false);
        }

        if (e.CommandName == "ADDTODAYOPD")
        {
            GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            DropDownList ddlDoctor = (DropDownList)gvRow.FindControl("ddldoctors");
            int DUID = Convert.ToInt32(ddlDoctor.SelectedValue);
            int PID = Convert.ToInt32(e.CommandArgument);
            string[] _doc = txtfrm.Text.ToString().Split('/');
            string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

            int cnt = Convert.ToInt32(gen.executeScalar("Select Count(adid) As TotalApp From tblAppointmentDetails Where Pat_Type = 'O' And App_Cancel = 0 And CONVERT(date,AppointmentDate) = CONVERT(date, getdate()) And PatientID =" + PID).ToString());

            if (cnt > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Appointment Already Generated For The Today ...!!!!')", true);
                return;
            }
            else
            {

                strSQL = "InsAppoinmentDetails";

                SqlCmd = new SqlCommand(strSQL, con);
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlCmd.Parameters.AddWithValue("@PatientID", PID);
                SqlCmd.Parameters.AddWithValue("@AppointmentDate", doc);
                SqlCmd.Parameters.AddWithValue("@adCreatedBy", uid);
                SqlCmd.Parameters.AddWithValue("@App_Type", "CALL");

                SqlCmd.Parameters.AddWithValue("@pdInitial", "");
                SqlCmd.Parameters.AddWithValue("@pdFname", "");
                SqlCmd.Parameters.AddWithValue("@pdMname", "");
                SqlCmd.Parameters.AddWithValue("@pdLname", "");

                SqlCmd.Parameters.AddWithValue("@pdAdd1", "");
                SqlCmd.Parameters.AddWithValue("@pdAdd2", "");
                SqlCmd.Parameters.AddWithValue("@pdAdd3", "");
                SqlCmd.Parameters.AddWithValue("@pdAdd4", "");
                SqlCmd.Parameters.AddWithValue("@pdTele", "");
                SqlCmd.Parameters.AddWithValue("@pdMob", "");
                SqlCmd.Parameters.AddWithValue("@pdemail", "");

                SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
                SqlCmd.Parameters.Add("@adid", SqlDbType.Int).Direction = ParameterDirection.Output;
                SqlCmd.Parameters.AddWithValue("@pdWeight", "");
                SqlCmd.Parameters.AddWithValue("@duid", DUID);
                SqlCmd.Parameters.AddWithValue("@Pat_Type", "CO");

                if (con.State == ConnectionState.Closed)
                    con.Open();
                int reslt;
                SqlCmd.ExecuteNonQuery();
                reslt = (int)SqlCmd.Parameters["@Error"].Value;

                if (reslt == 0)
                {
                    // Response.Redirect("Home.aspx?MSG=Patient Appoinment Saved successfully");
                    Response.Redirect("Home.aspx");
                }
                SqlCmd.Dispose();
                con.Close();
            }
        }

        if (e.CommandName == "AddToWaitingList")
        {
            int PID = Convert.ToInt32(e.CommandArgument);
            int wpdId = Convert.ToInt32(gen.executeScalar("Select Count(wpdId) From tblWaitingPatientList Where wpdId = '" + PID + "' and wstatus = 'N' ").ToString());
            if (wpdId > 0)
            {
                string script = "alert('This Patient is Already Exist in WaitingList Kindly select other Patient....!!!!');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                return;
            }

            strSQL = "InsPaitientWaitingList";
            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@wpdId", PID);
            SqlCmd.Parameters.AddWithValue("@wstatus", "N");
            SqlCmd.Parameters.AddWithValue("@wCreatedBy", uid);
            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            int reslt;
            SqlCmd.ExecuteNonQuery();
            reslt = (int)SqlCmd.Parameters["@Error"].Value;
            if (reslt == 0)
            {
                Response.Redirect("WardNBedStatus.aspx");
            }
            SqlCmd.Dispose();
            con.Close();
        }
        if (e.CommandName == "AddTodaysList")
        {
            GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            TextBox txtDate = (TextBox)gvRow.FindControl("txtDate");
            CheckBox chkExtraCases = (CheckBox)gvRow.FindControl("chkExtraCases");
            Label lblResult = (Label)gvRow.FindControl("lblResult");
            int Pid = Convert.ToInt32(e.CommandArgument);
            int Tid = Convert.ToInt32(gen.executeScalar("Select Count(Tid) From tblTodaysList Where IsDelete = 0 and TpdId = '" + Pid + "'").ToString());
            if (Tid > 0)
            {
                string script = "alert('This Patient is Already Exist in Todays List Kindly Select other Patient.....!!!!');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                return;
            }

            int TCount = Convert.ToInt32(gen.executeQuery("Select CONVERT(VARCHAR(50), COUNT(TCreatedDate), 101) from tblTodaysList where IsDelete = 0 And CONVERT(VARCHAR(50), TCreatedDate, 101) = CONVERT(VARCHAR(50), " + txtDate.Text.ToString() + ", 101) And TSource = 'Walkin' and TExtraCase = 0 group by CONVERT(VARCHAR(50), TCreatedDate, 101)"));
            if (TCount >= 13)
            {
                string script = "alert('Appointment is full.....!!!!');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                return;
            }

            if (txtDate.Text == "")
            {
                lblResult.Visible = true;
                lblResult.Text = "Required";
                lblResult.ForeColor = Color.DarkRed;
                return;
            }
            else
            {
                lblResult.Visible = true;
                lblResult.Text = "";
            }

            int check = 0;
            if (chkExtraCases.Checked == true)
            {
                check = 1;
            }

            string[] _date = txtDate.Text.ToString().Split('/');
            string date = _date[2] + "/" + _date[1] + "/" + _date[0];

            strSQL = "InsTodaysList";
            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@TpdID", Pid);
            SqlCmd.Parameters.AddWithValue("@TCreatedBy", uid);
            SqlCmd.Parameters.AddWithValue("@TCreatedDate", date);
            SqlCmd.Parameters.AddWithValue("@TExtraCase", check);
            SqlCmd.Parameters.AddWithValue("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            int result;
            SqlCmd.ExecuteNonQuery();
            result = (int)SqlCmd.Parameters["@Error"].Value;
            if (result == 0)
            {
                Response.Redirect("TodaysList.aspx");
            }
            SqlCmd.Dispose();
            con.Close();
        }
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }

    protected void GRV1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GRV1.EditIndex = -1;
        BindGVR();
    }
    protected void GRV1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRV1.EditIndex = e.NewEditIndex;
        BindGVR();
    }
    protected void GRV1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GRV1.Rows[e.RowIndex];
        HiddenField hidpdId = (HiddenField)row.Cells[3].FindControl("hidcpno");

        TextBox _txtcpno = (TextBox)row.Cells[3].FindControl("txtcpno");
        TextBox _txtcno = (TextBox)row.Cells[4].FindControl("txtcno");
        TextBox _txtoccup = (TextBox)row.Cells[5].FindControl("txtoccup");


        strSQL = "Update tblPatientDetails SET pdCasePaperNo=@pdCasePaperNo,pdCassetteNo=@pdCassetteNo, pdOccupation =@pdOccupation";
        strSQL += " where pdId = " + hidpdId.Value.ToString() + "";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.Parameters.AddWithValue("@pdCasePaperNo", _txtcpno.Text);
        SqlCmd.Parameters.AddWithValue("@pdCassetteNo", _txtcno.Text);
        SqlCmd.Parameters.AddWithValue("@pdOccupation", _txtoccup.Text);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd.ExecuteNonQuery();
        con.Close();
        GRV1.EditIndex = -1;
        BindGVR();
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
        {
            string pdid = (string)this.GRV1.DataKeys[e.Row.RowIndex]["pdID"].ToString();
            Repeater rptReconsult = (Repeater)e.Row.FindControl("rptReconsult");
            Label lblkco = (Label)e.Row.FindControl("lblkco");
            TextBox txtDate = (TextBox)e.Row.FindControl("txtDate");
            txtDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            DropDownList ddlDoctor = (e.Row.FindControl("ddldoctors") as DropDownList);
            ddlDoctor.Items.Clear();
            //ddlDoctor.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT * FROM tblUser where uShortName is not null", "uId", "uFname", ddlDoctor);
            ddlDoctor.SelectedValue = "35";

            strSQL = "SELECT fdId,fdDate,CAST(fdId AS VARCHAR) + '-' + CAST(fdPatientId AS VARCHAR) 'fdFlag' FROM tblFollowUpDatails WHERE fdIsReconsulting = 1 AND fdPatientId = " + pdid;
            DataTable dt = gen.getDataTable(strSQL);
            if (dt.Rows.Count > 0)
            {
                rptReconsult.DataSource = dt;
                rptReconsult.DataBind();
            }

            if (File.Exists(Server.MapPath("Files/KCO/kco_" + pdid)))
            {
                lblkco.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + pdid).ToString());
            }
            else
            {
                lblkco.Text = "No KCO Found";
            }
        }
    }

    protected void rptReconsult_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "watch")
        {
            string arg = e.CommandArgument.ToString();
            string[] _arg = arg.Split('-');

            string fdid = _arg[0].ToString();
            string fdpdid = _arg[1].ToString();

            Session["fdId"] = fdid;
            Session["CasePatientID"] = fdpdid;
            Response.Redirect("ProfarmaOfCase.aspx", false);
        }
    }
}