USE [PMS]
GO

/****** Object:  StoredProcedure [dbo].[sp_DccgReport]    Script Date: 04/17/2013 15:44:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--[dbo].[sp_DccgReport] '2012-04-01' '2013-04-17'
CREATE PROCEDURE [dbo].[sp_DccgReport]
	 @frmDate SMALLDATETIME
	,@toDate SMALLDATETIME
AS
BEGIN 
      DECLARE @TempDccg TABLE
      (
         itdTestDate SMALLDATETIME
         ,Name VARCHAR(200)
         ,tmBaseCharges DECIMAL(18,2)
      )
      
INSERT INTO @TempDccg
	SELECT 
		   itd.itdTestDate
		  ,pd.pdInitial + ' ' + pd.pdFname + ' ' + pd.pdMname +' ' + pd.pdLname 'Name'
		  ,itd.itdcharges
	  --,ROW_NUMBER()OVER(PARTITION by  itdTestDate order by itdTestDate asc)as 'rank'
	FROM 
		 tblIpdTestDetails itd 
		 inner join 
		 tblTestMaster tm ON itd.itdtmId=tm.tmId 
		 inner  join 
		 tblPatientDetails pd ON pd.pdID=itd.itdipdId 
		 WHERE  tm.tmId=2	AND itd.itdTestDate between @frmDate  AND @toDate
		 ORDER BY itd.itdTestDate
	 
INSERT INTO @TempDccg
	  SELECT  ptd.ptdFromDate
			 ,pd.pdInitial + ' ' + pd.pdFname + ' ' + pd.pdMname +' ' + pd.pdLname 'Name'
			 ,ptd.ptdCharges
		  FROM tblPatientTestDetails ptd 
		  INNER JOIN 
		  tblTestMaster tm ON	ptd.ptdTMId=tm.tmId 
		  INNER JOIN 
		  tblPatientDetails pd ON PD.pdID=ptd.ptdId
      
    SELECT itdTestDate,Name,'B/H' as 'test',tmBaseCharges,ROW_NUMBER()OVER(PARTITION by  itdTestDate order by itdTestDate asc)as 'rank' FROM @TempDccg 
   
END



	
GO


