﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="IPDDieticianReport.aspx.cs" Inherits="IPDDieticianReport" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />

<style>
.AksDisplay {display:none;}
.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}
.mGrid {background-color: #eee;}
</style>
<script type="text/javascript">

    function ConfirmPrint(FormDep) {


        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Do you want to Print?")) {

            confirm_value.value = "Yes";

            if (FormDep == "COMM_Print")
            { OPDPrintFunction(); }

            else {
                confirm_value.value = "No";
            }
        }
        document.forms[0].appendChild(confirm_value);
    }


    function OPDPrintFunction() {


        setTimeout('DelayOPDPrintFunction()', 200);

    }

    function DelayOPDPrintFunction() {
        var myWindow = window.open("PrintReport.aspx");
        myWindow.focus();
        myWindow.print();
    }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
 <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <div class="login-area margin_ten_right" style="width: 100%; text-align:center">
                <h2><asp:Label ID="lblHeader" runat="server" Text="Dietician Report"></asp:Label></h2>
              
                       <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                          <table style="width:100%; font-size:14px; background:#f2f2f2;" cellspacing="0px" cellpadding="5px">
                             
                        	  <tr>
                             	<td id="gvcol" style="width:100%; text-align: center;background:#fff">       
                                       
                                    <asp:GridView ID="GVD_PatientList"  Width="100%" runat="server" CssClass="mGrid" AutoGenerateColumns="false"  
                                    PageSize="20" AllowPaging="false" OnRowDataBound="GVD_PatientList_RowDataBound"  OnRowCommand="GVD_PatientList_RowCommand" 
                                    DataKeyNames="pdID" OnPageIndexChanging="GVD_PatientList_PageIndexChanging">
					<AlternatingRowStyle BackColor="White" />
                                    <Columns>    
                                                                    
                                     <asp:BoundField DataField="SNO" HeaderText="S.N." HeaderStyle-Font-Size="Small"/>

                                      <asp:TemplateField HeaderText="DOA" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lbldate" Text='<%#Eval("DOA", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                       <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderText="Patient Name" HeaderStyle-width="27%" HeaderStyle-Font-Size="Small">
                                           <ItemTemplate>
                                                <asp:LinkButton ID="lnkpdName" runat="server" Text='<%# Eval("pdName")%>' CommandArgument='<%# Eval("ipdId")%>'
                                                    CommandName="IPDDetails" Style="text-decoration: none; font-weight: normal;"></asp:LinkButton>
                                            </ItemTemplate>
                                         </asp:TemplateField>
					<asp:BoundField DataField="wmWardNo" HeaderText="Ward No." HeaderStyle-Font-Size="Small"/>

                                             <asp:BoundField DataField="bmNoOfBads" HeaderText="Bed No." HeaderStyle-Font-Size="Small"/>
                              
                                        <asp:TemplateField HeaderText="KCO" Visible="false" HeaderStyle-Font-Size="Small">
                                               <ItemTemplate>
                                               <asp:Label ID="lblkco" runat="server"></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField>


                                         <asp:TemplateField HeaderText="Diet" HeaderStyle-Font-Size="Small">
                                               <ItemTemplate>
                                               <asp:Label ID="lblDiet" runat="server" Text='<%# Eval("Diet")%>'></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField>
                                   
					<asp:TemplateField HeaderText="Diet Date" HeaderStyle-Font-Size="Small">
						<ItemTemplate>
							<asp:Label ID="lblDietDate" runat="server" Text='<%#Eval("DietDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

                                            <asp:TemplateField HeaderText="Diet Given" HeaderStyle-Font-Size="Small">
                                               <ItemTemplate>
                                               <asp:Label ID="lblDietGiven" runat="server" Text='<%# Eval("DietGiven")%>'></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField>

					   <asp:TemplateField HeaderText="Given Date" HeaderStyle-Font-Size="Small">
						<ItemTemplate>
							<asp:Label ID="lblDietGivenDate" runat="server" Text='<%#Eval("DietGivenDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					    <asp:TemplateField HeaderText="Print" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnPrint" runat="server" CommandName="Print" CommandArgument='<%# Eval("ipdId") %>'
                                                        ImageUrl="~/images/print1.png" ToolTip="Print Diatician Form" OnClientClick="ConfirmPrint('COMM_Print');"/>
                                                </ItemTemplate> 
                                             </asp:TemplateField>

                                     </Columns>
                                    </asp:GridView>

                                     
                                </td>                           
                        	  </tr>                       
                              <tr>
                                <td style="text-align: center;">
                                    <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></font>
                                </td>
                              </tr>
                    	  </table>
                        </div>
                     
            </div>
        </td>
    </tr>
    
</table>
<asp:Panel BorderStyle="Outset"  BorderWidth="5px" BorderColor="#C4D4B6" ID="PNLWorkSheet" runat="server" Visible="false">                                   
	<asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right"  ImageUrl="~/Images/FancyClose.png" />
    <asp:Panel ID="pnl1" runat="server" ScrollBars="Auto" Width="945px">
        <CR:CrystalReportViewer ID="CrystalReportViewer2" runat="server"  AutoDataBind="True" Height="500px" EnableParameterPrompt="False" 
            ReuseParameterValuesOnRefresh="True" ToolPanelView="None" GroupTreeImagesFolderUrl="" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
           	EnableDatabaseLogonPrompt="False" />
        <CR:CrystalReportSource ID="CrystalReportSource2" runat="server">
           <Report FileName="~/Reports/OpdReportAks.rpt"></Report>
        </CR:CrystalReportSource>
	</asp:Panel>
	</asp:Panel>

</asp:Content>

