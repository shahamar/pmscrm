﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="tabReconsult.ascx.cs"
    Inherits="tabReconsult" %>
<style type="text/css">
    .tabdiv
    {
        border-width: 2px 2px 0px;
        border-style: solid solid none;
        border-color: #E2E2E2;
        position: inherit;
        height: auto;
    }
    
    #tabtbl
    {
        color: #000;
        font-size: 12px;
        font-weight: lighter;
        padding: 2px;
        background-color: #F1F1F1;
    }
    
    .alt1
    {
        width: 65px;
        font-weight:bold;
    }
    .alt2
    {
        width: 125px;
    }
</style>
<script type="text/javascript">
    function goBack() {
        //alert("ok")
        window.history.go(-1)
    }
</script>

<div class="tabdiv">
    <table id="tabtbl" style="width:100%;" cellpadding="2px" cellspacing="0px" border="1px">
        <tr>
            <td class="alt1">
                Followup Date :</td>
            <td class="alt2">
                <asp:Label ID="lblDate" runat="server"></asp:Label>
            </td>
            <td class="alt1">
                Reconsulting :</td>
            <td class="alt2">
                <asp:Label ID="lblAccute" runat="server"></asp:Label>
            </td>
            <td class="alt1">
                Cassette No. :</td>
            <td class="alt2">
               <asp:Label ID="lblCassetteNo" runat="server"></asp:Label>
             </td>
        </tr>
        <tr>
            <td class="alt1">
                Remedy :</td>
            <td class="alt2">
                <asp:Label ID="lblRemedy" runat="server"></asp:Label>
            </td>
            <td class="alt1">
                Remedy Period :</td>
            <td class="alt2">
                <asp:Label ID="lblRemedyPeriod" runat="server"></asp:Label>
            </td>
            <td class="alt1">
                Dose :</td>
            <td class="alt2">
                <asp:Label ID="lblDose" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</div>
