﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class GenerateBill : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    //string pdId = "0";
    string uid = "0";
    //string ipdid = "0";
    //added by nikita on 12th march 2015------------------------------
    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }
    
    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (!IsPostBack)
        {
            //added by nikita on 12th march 2015------------------------------
            if (Session["BilledPatientID"] != null)
                ViewState["pdId"] = Session["BilledPatientID"].ToString();

            if (Session["BilledipdId"] != null)
                ViewState["ipdid"] = Session["BilledipdId"].ToString();
            else
                Response.Redirect("ManageBill.aspx?flag=1");
            //---------------------------------------------------------------------
            FillData();
        }

    }

    public void FillData()
    {
        SqlCmd = new SqlCommand();
        SqlCmd.CommandText = "sp_GenerateBill";
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Connection = con;
        SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
        dapt = new SqlDataAdapter(SqlCmd);
        DataTable dt = new DataTable();
        dapt.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GRV1.DataSource = dt;
            GRV1.DataBind();
        }

        decimal OtherCharge = 0;
        decimal TotalCharge = 0;
        OtherCharge = Convert.ToDecimal(gen.executeScalar("SELECT COALESCE( (SELECT OtherCharges FROM tblBillMaster WHERE bmipdId = '" + ipdid + "'), '0')").ToString());
        TotalCharge = Convert.ToDecimal(gen.executeScalar("SELECT COALESCE( (SELECT (  Case When ReturmAmt > 0 Then (ReturmAmt - (-(IsNull(bmTotalAmt,0) - IsNull(bmAdvance,0)))) else IsNull(bmTotalAmt,0) - IsNull(bmAdvance,0) end  ) As BalanceAmt FROM tblBillMaster BM Left Join tblVoucherDetails VD On VD.VIPDID = BM.bmipdId  And IsDelete = 0 WHERE bmipdId = '" + ipdid + "'), '0')").ToString());

        txtOtherCharges.Text = Convert.ToString(OtherCharge);
        lblTotal.Text =  Convert.ToString(TotalCharge+OtherCharge);

        string Balance = "";
        Balance = BalanceStatusForVoucher();

        if (Balance == "YES")
        {
            btn_Voucher.Visible = true;
        }
        else
        {
            btn_Voucher.Visible = false;
        }
       


    }
    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        decimal otherCharges = 0;

        otherCharges = txtOtherCharges.Text.Trim() == "" ? 0 : decimal.Parse(txtOtherCharges.Text.Trim());
        SqlCmd = new SqlCommand();
        SqlCmd.CommandText = "sp_SaveBill";
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Connection = con;
        SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
        SqlCmd.Parameters.AddWithValue("@finyear", gen.GetCurrentFinYear());
        SqlCmd.Parameters.AddWithValue("@CreatedBy", uid.ToString());
        SqlCmd.Parameters.AddWithValue("@OtherCharges", otherCharges);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd.ExecuteNonQuery();
        SqlCmd.Clone();
        con.Close();
        Session["BillPrintIpdId"] = ipdid;
        Response.Redirect("Bill.aspx", false);
    }

    protected void btn_FinalBillPrint_Click(object sender, EventArgs e)
    {

        string Balance = "";
        Balance = BalanceStatus();


        if (Balance == "NO")
        {
            decimal otherCharges = 0;

            otherCharges = txtOtherCharges.Text.Trim() == "" ? 0 : decimal.Parse(txtOtherCharges.Text.Trim());
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "sp_SaveBill";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Connection = con;
            SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
            SqlCmd.Parameters.AddWithValue("@finyear", gen.GetCurrentFinYear());
            SqlCmd.Parameters.AddWithValue("@CreatedBy", uid.ToString());
            SqlCmd.Parameters.AddWithValue("@OtherCharges", otherCharges);
            SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd.ExecuteNonQuery();
            SqlCmd.Clone();
            con.Close();
            Session["BillPrintIpdId"] = ipdid;
            Response.Redirect("Bill.aspx?V=N", false);
        }
        else
        {
            string script = "alert('Kindly First Pay All Bill Amount...!!!!');";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
            return;
        }
        
    }

    protected void btn_Voucher_Click(object sender, EventArgs e)
    {
        Session["ipdid"] = ipdid;
        Session["CasePatientID"] = PdID;
        Response.Redirect("Voucher.aspx");
    }

    public string BalanceStatus()
    {
        string BalanceStatus;
        BalanceStatus = "";
        decimal BalanceAmt = 0;

        foreach (GridViewRow rw in GRV1.Rows)
        {
            Label lblGridTotal = (Label)rw.FindControl("lblGridTotal");
            Label lblUnits = (Label)rw.FindControl("lblUnits");
            if (lblUnits.Text == "Balance")
            {
                BalanceAmt = lblGridTotal.Text == "" ? 0 : Convert.ToDecimal(lblGridTotal.Text);
            }
        }

        if (BalanceAmt > 0)
        {
            BalanceStatus = "YES";
        }
        else
        {
            BalanceStatus = "NO";
        }

        return BalanceStatus;
    }

    public string BalanceStatusForVoucher()
    {
        string BalanceStatus;
        BalanceStatus = "";
        decimal BalanceAmt = 0;

        foreach (GridViewRow rw in GRV1.Rows)
        {
            Label lblGridTotal = (Label)rw.FindControl("lblGridTotal");
            Label lblUnits = (Label)rw.FindControl("lblUnits");
            if (lblUnits.Text == "Balance")
            {
                BalanceAmt = lblGridTotal.Text == "" ? 0 : Convert.ToDecimal(lblGridTotal.Text);
            }
        }

        if (BalanceAmt < 0)
        {
            BalanceStatus = "YES";
        }
        else
        {
            BalanceStatus = "NO";
        }

        return BalanceStatus;
    }

    protected void txtOtherCharges_TextChanged(object sender, EventArgs e)
    {
        decimal total = 0;

        foreach (GridViewRow rw in GRV1.Rows)
        {
            Label lblGridTotal = (Label)rw.FindControl("lblGridTotal");
            Label lblUnits = (Label)rw.FindControl("lblUnits");

            if (lblUnits.Text == "Total")
            {
                total = decimal.Parse(lblGridTotal.Text);
            }
            
            if (lblUnits.Text == "Balance")
            {
                total = decimal.Parse(lblGridTotal.Text);
            }
        }

        total += decimal.Parse(txtOtherCharges.Text.Trim());

        lblTotal.Text = total.ToString();
    }
}