﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Net;

public partial class Login : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "CallValidator;", true);

        if (Session["userid"] != null)
        {
            Response.Redirect("Home.aspx", false);
        }

    }

    protected void btnSignIn_Click(object sender, EventArgs e)
    {
        try
        {
            string _pwd = EncryptionTest.base64Encode(txtpwd.Text.ToString());
            strSQL = "select * from tblUser where uName=@uName and uPwd=@uPwd and IsDelete = 0";
            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.Parameters.AddWithValue("@uName",txtuname.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@uPwd", _pwd);
            if (con.State == ConnectionState.Closed)
                con.Open();
            Sqldr = SqlCmd.ExecuteReader();
            if (Sqldr.HasRows == true)
            {
                while (Sqldr.Read())
                {
                    Session["userid"] = Sqldr["uId"].ToString();  //dt.Rows[0]["uId"].ToString();
                    Session["urole"] = Sqldr["uRole"].ToString();
                    Session["uName"] = Sqldr["uFname"].ToString();
                    Session["username"] = Sqldr["uName"].ToString();

                    
                }
                //New logic
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST  
                                                     // Get the IP  
                string IPAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();
                string strSQL1 = "INSERT INTO tblUserLogOffInfoHistory(User_ID,CreatedBy,CreatedDate,IpAddress) VALUES(" + Session["userid"] + "," + Session["userid"] + ",GETDATE(),'" + IPAddress + "')";
                SqlCommand SqlCmd1 = new SqlCommand(strSQL1, con);
                SqlCmd1.ExecuteNonQuery();

                Response.Redirect("Home.aspx", false);
            }
            
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('Invalid User Name or Password');</script>");
            }

            con.Close();

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "<script>alert('"+ex.ToString()+"');</script>");
        }
    }
    protected void btnforgotpass_Click(object sender, EventArgs e)
    {
        Response.Redirect("ForgotPassword.aspx");
    }
}
