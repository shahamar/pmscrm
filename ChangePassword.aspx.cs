﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;


public partial class ChangePassword : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string userrole;
    string strSQL;
    int uId = 0;
    string smsstring;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 50";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string oldpwd = EncryptionTest.base64Encode(txtOldPassword.Text.Trim());
        
        bool doesexist = gen.doesExist("Select * From tblUser Where uId="+userid+" AND uPwd='"+oldpwd+"'");

        if(doesexist == true)
        {
            string pwd = "";
            pwd = EncryptionTest.base64Encode(txtNewPassword.Text.Trim());
            strSQL = "Update tblUser SET uPwd='" + pwd + "' where uId=" + userid + "";
            gen.executeQuery(strSQL);

            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "select (isnull(uFname,'')+' '+isnull(uMname,'')+' '+isnull(uLname,'')) Name,uName,uContact,uPwd from tblUser Where IsDelete=0 and uId=" + userid + "";
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader sqldr = SqlCmd.ExecuteReader();
            while (sqldr.Read())
            {
                if (sqldr["uContact"].ToString()!="" || sqldr["uContact"].ToString()!=null)
                {
                    string MobileNo = sqldr["uContact"].ToString();
                    string NewPassWord = EncryptionTest.base64Decode(sqldr["uPwd"].ToString().Trim());
                    string Name = sqldr["Name"].ToString();
                    string UName = sqldr["uName"].ToString();
                    decimal TemplateId = 1207160931694518997;
                    smsstring = "Dear " + Name + ",  We received a request for password change for username: " + UName + " and your new password is: " + NewPassWord + " Aditya Homoeopathic Hospital and Healing center";
                    string str21 = genral.GetResponse(MobileNo, smsstring, TemplateId);
                }
            }



                Response.Redirect("Logout.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Please enter correct old password.');", true);
        }
    }
}
