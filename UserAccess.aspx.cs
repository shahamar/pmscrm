﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;
using System.Net.Mail;
using System.IO;
using System.Text;

public partial class UserAccess : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    DataTable dt;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    int userid = 0;
    string strSQL = "";
    int aId = 0;
    genral gen = new genral();
    protected void Page_Load(object sender, EventArgs e)
    {
        //ddlUsers.Focus();
        ddlUsersRole.Focus();
        if (Session["userid"] != null)
            userid = Convert.ToInt32(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?msg=You are not logged in. Please login to proceed further");


        if (Session["userid"] == null)
            Response.Redirect("Login.aspx");
        if (!IsPostBack)
        {
            //BindUser();
            BindUserRole();
            BindMenu();
        }

    }

    public void BindMenu()
    {
        SqlDataAdapter da = new SqlDataAdapter("Proc_FillMenuTypeUsersAccess", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            ddlmenutype.DataSource = dt;
            ddlmenutype.DataTextField = "MenuType";
            ddlmenutype.DataValueField = "MenuTypeID";
            ddlmenutype.DataBind();
            ddlmenutype.Items.Insert(0, new ListItem("Select MenuType", ""));
        }
    }

    protected void ddlmenutype_OnSelectedIndexChanged(object sender, System.EventArgs e)
    {
        //displaySubmit();
        if (ddlUsersRole.SelectedValue != "" && ddlmenutype.SelectedValue != "")
        {
            bindgrd(Convert.ToInt32(ddlmenutype.SelectedValue), Convert.ToInt32(ddlUsersRole.SelectedValue));
            
        }
    }

    public void bindgrd(int MenutypeId, int UId)
    {
        SqlCommand cmd = new SqlCommand("Proc_FillGridMenuUsersAccess", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Menutype", MenutypeId);
        cmd.Parameters.AddWithValue("@User_Role", UId);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            gridUserAccess.DataSource = dt;
            gridUserAccess.DataBind();
        }
        else
        {
            gridUserAccess.DataSource = null;
            gridUserAccess.DataBind();
        }
    }
    protected void save2_Click(object sender, EventArgs e)
    {

        if (con.State == ConnectionState.Closed)
            con.Open();

        strSQL = "Proc_DeleteUserAccessBeforeInsert";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@User_Role", Convert.ToInt32(ddlUsersRole.SelectedValue));
        SqlCmd.Parameters.AddWithValue("@MenuType", Convert.ToInt32(ddlmenutype.SelectedValue));
        SqlCmd.ExecuteNonQuery();
        SqlCmd.Dispose();
        con.Close();

        foreach (GridViewRow g1 in gridUserAccess.Rows)
        {
            string menuId = (g1.FindControl("lblMenuId") as Label).Text;
            CheckBox chkstat = g1.FindControl("chkstat") as CheckBox;

            if (chkstat.Checked == true)
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                strSQL = "Proc_InsertUserAccess";
                SqlCmd = new SqlCommand(strSQL, con);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@User_Role", Convert.ToInt32(ddlUsersRole.SelectedValue.Trim()));
                SqlCmd.Parameters.AddWithValue("@Menu_ID", menuId.Trim());
                SqlCmd.Parameters.AddWithValue("@IsDisabled", 0);
                //SqlCmd.Parameters.AddWithValue("@MenuType", Convert.ToInt32(ddlmenutype.SelectedValue));

                SqlCmd.ExecuteNonQuery();
                SqlCmd.Dispose();
                con.Close();

            }
        }
        string script = "alert('Records saved successfully..');";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);

        return;
        //Response.Redirect("UserAccess.aspx");
        //Context.Items["msg"] = "Data Save Successfully";
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }

    protected void ddlUsersRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUsersRole.SelectedValue != "" && ddlmenutype.SelectedValue != "")
        {
            bindgrd(Convert.ToInt32(ddlmenutype.SelectedValue), Convert.ToInt32(ddlUsersRole.SelectedValue));

        }
    }
    public void BindUserRole()
    {
        ddlUsersRole.Items.Clear();
        ddlUsersRole.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT * FROM tblUserType", "UserTypeId", "UserType", ddlUsersRole);
    }
}