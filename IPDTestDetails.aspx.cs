﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

public partial class IPDTestDetails : System.Web.UI.Page
{
    public static List<string> list = new List<string>();
    genral gen = new genral();
    DataTable dt = new DataTable();
    DataRow dr;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlCommand SqlCmd;
    string strSQL="";
    string urole,msg;
    string pdId = "";
    string uid = "";
    //string ipdid = "";
    //added by nikita on 12th march 2015------------------------------
    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }
    //-------------------------------------------------------------
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            //added by nikita on 12th march 2015------------------------------
            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            else
                Response.Redirect("IPDView.aspx");
            //-------------------------------------------------------------  

            //---Added By Nikita On 30thMarch 2015----------------------------------------------------
            if (msg == "1")
            {
                btnAdd.Enabled = false;
                btnRemove.Enabled = false;
                _btnSave.Enabled = false;
            }
            else
            {
                btnAdd.Enabled = true;
                btnRemove.Enabled = true;
                _btnSave.Enabled = true;
            }
            //-----------------------------------------------------------------------------------------

            Session["SelectedDates"] = null;
            list.Clear();
            FillData();
            txtHours.Text = "0";
        }
    }

    public void FillData()
    {
        ddTests.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select * from dbo.tblTestMaster", "tmId", "tmTestName", ddTests);
    }

    public bool searchdate(string date, List<string> dates)
    {

        var query = from o in dates
                    where Convert.ToDateTime(o) == Convert.ToDateTime(date)
                    select o;
        if (query.ToList().Count == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    protected void Cal_SelectionChanged(object sender, EventArgs e)
    {
        string selection = Calendar1.SelectedDate.ToString();

        if (Session["SelectedDates"] != null)
        {
            List<string> selectedList = (List<string>)Session["SelectedDates"];
            foreach (string s in selectedList)
            {
                Calendar1.SelectedDates.Add(Convert.ToDateTime(s));
            }
            if (searchdate(selection, selectedList))
            {
                Calendar1.SelectedDates.Remove(Convert.ToDateTime(selection));
            }

            list.Clear();
        }
    }
    protected void Calendar1_PreRender(object sender, EventArgs e)
    {
        if (Calendar1.SelectedDates.Count == 1)
        {
            foreach (string dt in list)
            {
                if (searchdate(dt, list) && list.Count == 1)
                {
                    Calendar1.SelectedDates.Clear();
                    list.Clear();
                    break;
                }
            }
        }
    }
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        if (e.Day.IsSelected == true)
        {
            list.Add(e.Day.Date.ToString("MM/dd/yyyy"));
        }
        Session["SelectedDates"] = list;
        if (list.Count != 0)
        {
            //TextBox1.Text = "";
            //foreach (string s in list)
            //{
            //    //Response.Write(s + "<BR/>");
            //    TextBox1.Text = TextBox1.Text + "," + s;
            //}
            //TextBox1.Text = TextBox1.Text.Substring(1, TextBox1.Text.Length - 1);
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (ddTests.SelectedValue == "")
        {
            cvTest.IsValid = false;
            return;
        }

        if (Calendar1.SelectedDates.Count == 0)
        {
            cvCaltest.IsValid = false;
            return;
        }
        
        int integer;
        if (Int32.TryParse(txtHours.Text, out integer) == false)
        {
            cvHrs.IsValid = false;
            return;
        }
        else
        {
            if (ddTests.SelectedItem.Text == "Monitor")
            {
                int value = int.Parse(txtHours.Text.Trim());
                if (value <= 0)
                {
                    cvHours.IsValid = false;
                    return;
                }
            }
            List<string> selectedList = (List<string>)Session["SelectedDates"];
            int daycnt = selectedList.Count;
            int tothrs = daycnt * 24;
            int enterhrs = Convert.ToInt32(txtHours.Text);
            if (enterhrs > tothrs)
            {
                cvHrs.IsValid = false;
                return;
            }
        }



        pnlfeat.Visible = true;
        if (ViewState["CurrentData"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentData"];
            int count = dt.Rows.Count;
            BindGrid(count);
        }
        else
        {
            BindGrid(1);
        }                
    }

    private void BindGrid(int rowcount)
    {

        dt.Columns.Add(new System.Data.DataColumn("tmTestName", typeof(String)));
        dt.Columns.Add(new System.Data.DataColumn("itdTmid", typeof(Int32)));
        dt.Columns.Add(new System.Data.DataColumn("tmDate", typeof(String)));
        dt.Columns.Add(new System.Data.DataColumn("tmHours", typeof(Int32)));
        //dt.Columns.Add(new System.Data.DataColumn("tmBaseCharges", typeof(Int32)));
        dt.Columns.Add(new System.Data.DataColumn("Payment_Mode", typeof(String)));

        if (ViewState["CurrentData"] != null)
        {
            for (int i = 0; i < rowcount + 1; i++)
            {
                dt = (DataTable)ViewState["CurrentData"];
                if (dt.Rows.Count > 0)
                {
                    dr = dt.NewRow();
                    dr[0] = dt.Rows[0][0].ToString();
                    dr[1] = Convert.ToInt32(dt.Rows[0][1].ToString());
                    dr[2] = dt.Rows[0][2].ToString();
                    dr[3] = Convert.ToInt32(dt.Rows[0][3].ToString());
                    //dr[4] = Convert.ToInt32(dt.Rows[0][4].ToString());
                    //dr[4] = dt.Rows[0][4].ToString();
                }
            }
            dr = dt.NewRow();
            dr[0] = ddTests.SelectedItem.Text.ToString();
            dr[1] = ddTests.SelectedValue.ToString();
            dr[2] = getconductedDates();
            dr[3] = txtHours.Text;
            dr[4] = DDL_Pay_Mode.SelectedValue;
            dt.Rows.Add(dr);

        }
        else
        {
            dr = dt.NewRow();
            dr[0] = ddTests.SelectedItem.Text.ToString();
            dr[1] = ddTests.SelectedValue.ToString();
            dr[2] = getconductedDates();
            dr[3] = txtHours.Text;
            dr[4] = DDL_Pay_Mode.SelectedValue;
            dt.Rows.Add(dr);

        }

        // If ViewState has a data then use the value as the DataSource
        if (ViewState["CurrentData"] != null)
        {
            GRV1.DataSource = (DataTable)ViewState["CurrentData"];
            GRV1.DataBind();
        }
        else
        {
            // Bind GridView with the initial data assocaited in the DataTable
            GRV1.DataSource = dt;
            GRV1.DataBind();

        }
        // Store the DataTable in ViewState to retain the values
        ViewState["CurrentData"] = dt;
        Session["SelectedDates"] = null;
        list.Clear();
        Calendar1.SelectedDates.Clear();
        ddTests.SelectedValue = "";
        txtHours.Text = "0";
    }

    public string getconductedDates()
    {
        string cdates = "";
        if (Session["SelectedDates"] != null)
        {
            List<string> newList = (List<string>)Session["SelectedDates"];
            List<string> newDistinctList = list.Distinct().ToList();

            foreach (string s in newDistinctList)
            {
                //Response.Write(s + "<BR/>");
                cdates = cdates + "," + gen.getreversedate(s, '/');
                
            }
            if (newList.Count != 0)
            {
                cdates = cdates.Substring(1, cdates.Length - 1);
            }
        }
        return cdates;
    }

    protected void btnRemove_Click(object sender, EventArgs e)
    {
        dt = new DataTable();
        if (ViewState["CurrentData"] != null)
        {
            dt = (DataTable)ViewState["CurrentData"];
        }

        int i = 0;
        if (dt.Rows.Count > 0)
        {
            foreach (GridViewRow grvrow in GRV1.Rows)
            {

                HtmlInputCheckBox chk = (HtmlInputCheckBox)grvrow.FindControl("cbAC");

                if (chk.Checked == true)
                {
                    dt.Rows.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }
        }
        GRV1.DataSource = dt;
        GRV1.DataBind();
        ViewState["CurrentData"] = dt;
        if (dt.Rows.Count == 0)
        {
            pnlfeat.Visible = false;
        }
    }
    protected void _btnSave_Click(object sender, EventArgs e)
    {
        string finyear = gen.GetCurrentFinYear();
        string itdReceiptNo = "";
        if (con.State == ConnectionState.Closed)
            con.Open();

        foreach (GridViewRow grvrow in GRV1.Rows)
        {
            HiddenField hdntmid = (grvrow.FindControl("hdntmId") as HiddenField);
            Label lbltmDate = (grvrow.FindControl("lbltmDate") as Label);
            Label lbltmHours = (grvrow.FindControl("lbltmHours") as Label);
            Label lblPayment_Mode = (grvrow.FindControl("lblPayment_Mode") as Label);
                                    
            string[] dates = new string[]{};
            string[] hrs = new string[]{};

            int hours = Convert.ToInt32(lbltmHours.Text);

            string _hours = "";
            
            if (lbltmDate.Text.Contains(","))
            {
                dates = lbltmDate.Text.Split(',');
                if (hours > 0)
                {
                    
                    int mod = hours % 24;
                    int div = hours / 24;
                    if (mod == 0)
                    {
                        for (int i = 0; i < dates.Length; i++)
                        {
                            _hours += ",24";
                        }
                        _hours = _hours.Substring(1, (_hours.Length - 1));

                        hrs = _hours.Split(',');
                    }
                    else
                    {
                        for (int i = 0; i < div; i++)
                        {
                            _hours += ",24";
                        }
                        _hours = _hours + "," + mod.ToString();
                        _hours = _hours.Substring(1, (_hours.Length - 1));
                        hrs = _hours.Split(',');
                    }
                }
                else
                {
                    for (int i = 0; i < dates.Length; i++)
                    {
                        _hours += ",0";
                    }

                    _hours = _hours.Substring(1, (_hours.Length - 1));
                    hrs = _hours.Split(',');
                }
            }
            else
            {
                dates = new string[1];
                hrs = new string[1];  
                dates[0] = lbltmDate.Text;
                hrs[0] = lbltmHours.Text;
                
            }

            for (int i = 0; i < dates.Length; i++)
            {
                object o = gen.executeScalar("Select CONVERT(VARCHAR,ISNull(MAX(CAST(SubString(itdReceiptNo,1,len(itdReceiptNo)-4) As INT)),0)+1)+'" + finyear + "' From dbo.tblIpdTestDetails");
                itdReceiptNo = o.ToString();

                strSQL = "InsIPDTest";
                SqlCmd = new SqlCommand(strSQL, con);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@itdTmid", hdntmid.Value);
                SqlCmd.Parameters.AddWithValue("@itdipdId", ipdid);
                SqlCmd.Parameters.AddWithValue("@itdtestdate", gen.getdate(dates[i], '/'));
                SqlCmd.Parameters.AddWithValue("@itdHours", hrs[i].ToString());
                SqlCmd.Parameters.AddWithValue("@CreatedBy", uid);
                SqlCmd.Parameters.AddWithValue("@itdReceiptNo", itdReceiptNo);
                SqlCmd.Parameters.AddWithValue("@Payment_Mode", lblPayment_Mode.Text);
                SqlCmd.ExecuteNonQuery();
            }

        }
        SqlCmd.Dispose();
        con.Close();

        Response.Redirect("IPDView.aspx", false);
    }

    //protected void txtHours_TextChanged(object sender, EventArgs e)
    //{
    //    DataTable dt = gen.getDataTable("SELECT tmIsHourlyBase From dbo.tblTestMaster Where tmId ='" + ddTests.SelectedValue + "'");
    //    DataTable dt1 = new DataTable();
    //    if (dt.Rows[0][0].ToString() == "False")
    //    {
    //        dt1 = gen.getDataTable("SELECT tmBaseCharges From dbo.tblTestMaster Where tmId ='" + ddTests.SelectedValue + "'");
    //    }
    //    else {
    //        dt1 = gen.getDataTable("SELECT tmBaseCharges/24*"+ txtHours.Text + " From dbo.tblTestMaster Where tmId ='" + ddTests.SelectedValue + "'");
    //    }
    //    txtCharges.Text = dt1.Rows[0][0].ToString();
    //}
}
