﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="Voucher.aspx.cs" Inherits="Voucher" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="asp" Namespace="Saplin.Controls" Assembly="DropDownCheckBoxes" %>
<%@ Register Src="tabipd.ascx" TagName="Tab" TagPrefix="tab1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table align="center" cellpadding="5" cellspacing="5" style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 100%;" cellpadding="5" cellspacing="5" >
                    <h2>Generate Voucher</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto; border: 1.5px solid #E2E2E2;">
                               
                               <table style="width: 100%;" cellpadding="5" cellspacing="5" >
                                    
                                    <tr>
                                        <td colspan="6">
                                            <tab1:Tab ID="tabcon" runat="server" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="6" style="text-align: center; color: Red;">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>

                                        <td>
                                           Voucher Date :
                                        </td>
                                        <td>
                                          <asp:TextBox ID="txt_VoucherDate" runat="server" Enabled="false" Width="80px" style="padding-left:6px"></asp:TextBox>
                                        </td>

                                           <td>
                                           Enter Return Amount :
                                        </td>
                                        <td>
                                          <asp:TextBox ID="txt_Amt" runat="server" Width="80px" style="padding-left:6px"></asp:TextBox>
					  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_Amt" Display="Dynamic" ErrorMessage="Please Enter Refund Amount." ValidationGroup="val">
						<span class="error">Please Enter Refund Amount.</span>
					  </asp:RequiredFieldValidator>
                                        </td>

                                            <td>
                                           Being :
                                        </td>
                                        <td>
                                          <asp:TextBox ID="txt_Being" runat="server" Width="150px" style="padding-left:6px" Text="" Placeholder="Refund excess amount."></asp:TextBox>
					  <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" ControlToValidate="txt_Being" Display="Dynamic" ErrorMessage="Please Enter the Narration." ValidationGroup="val">
						<span class="error">Please Enter the Narration.</span>
					  </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan="6" align="center">
                                            <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" Text="Save"
                                                OnClick="btnSave_Click" ValidationGroup="val" />
                                        </td>
                                    </tr>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

</asp:Content>

