﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="AddInvestigation.aspx.cs" Inherits="AddInvestigation" Title="Investigation Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <style type="text/css">
        #tbsearch td
        {
            text-align: left;
            vertical-align: middle;
        }
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        #gvcol div
        {
            margin-top: -10px;
        }
        .col1
        {
            width: 25%;
        }
        .col2
        {
            width: 75%;
        }
    </style>
    <%--<script type="text/javascript" language="javascript">
        function toggleSelection(source) {
            //alert("toggleSelection");
            $("#ctl00_ContentPlaceHolder1_GRV1 input[id*='cbAC']").each(function (index) {
                $(this).attr('checked', source.checked);
            });
        }

        function toggleSelection1(source) {
            //alert("toggleSelection");
            $("#ctl00_ContentPlaceHolder1_GRVSelected input[id*='cbSelAC']").each(function (index) {
                $(this).attr('checked', source.checked);
            });
        } 
   
    </script>--%>
    <%--<script type="text/javascript" language="javascript">
        function ChkValidate() {
            var isValid = false;
            var gridView = document.getElementById("ctl00_ContentPlaceHolder1_GRV1");
            for (var i = 1; i < gridView.rows.length; i++) {
                var inputs = gridView.rows[i].getElementsByTagName('input');
                if (inputs != null) {
                    if (inputs[0].type == "checkbox") {
                        if (inputs[0].checked) {
                            isValid = true;
                            return true;
                        }
                    }
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>--%>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        Investigation Report</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <ul class="quicktabs_tabs quicktabs-style-excel">
                                <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">
                                    Add Investigation</a> </li>
                                <li class="qtab-HTML last" id="li_9"><a class="qt_tab active" href="ManageInvestigation.aspx">
                                    Manage Investigation</a> </li>
                            </ul>
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto;">
                                <table style="width: 100%; padding-left: 20px" cellspacing="0px" cellpadding="0px">
                                    <tr>
                                        <td colspan="4" style="text-align: center; color: Red;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="text-align: center; color: Red;">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Visit Time <strong style="color: Red;">*</strong>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlVisitTime" runat="server" CssClass="field required">
                                                <asp:ListItem Text="Select Visit Time" Value=""></asp:ListItem>
                                                <asp:ListItem Text="09:00 AM" Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="12:00 PM" Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="04:00 PM" Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="10:00 PM" Value="10:00 PM"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="ddlVisitTime"
                                                Display="Dynamic" ErrorMessage="Visit Time is required." ValidationGroup="val">
                                                        <span class="error">Visit Time is required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            Ward Type : <strong style="color: Red;">*</strong>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DDWardType" runat="server" CssClass="field required" AutoPostBack="True"
                                                OnSelectedIndexChanged="DDWardType_SelectedIndexChanged">
                                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                <asp:ListItem Text="General" Value="General"></asp:ListItem>
                                                <asp:ListItem Text="Semi-Special" Value="Semi-Special"></asp:ListItem>
                                                <asp:ListItem Text="Special" Value="Special"></asp:ListItem>
                                                <asp:ListItem Text="Deluxe" Value="Deluxe"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DDWardType"
                                                Display="Dynamic" ErrorMessage="Ward Type is required." ValidationGroup="val">
                                                        <span class="error">Ward Type is required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ward No. : <strong style="color: Red;">*</strong>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UP1" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddWardNo" runat="server" class="required field" OnSelectedIndexChanged="ddWardNo_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddWardNo"
                                                Display="Dynamic" ErrorMessage="Please specify Ward No." ValidationGroup="val">
                                                        <span class="error">Please specify Ward No.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            Patient Name. : <strong style="color: Red;">*</strong>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddPatientName" runat="server" class="required field" OnSelectedIndexChanged="ddPatientName_SelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddPatientName"
                                                Display="Dynamic" ErrorMessage="Please specify Patient Name." ValidationGroup="val">
                                                        <span class="error">Please specify Patient Name.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:Button ID="btnView" runat="server" CssClass="textbutton b_submit" Style="margin-top: 2px;
                                                display: none;" Text="View" OnClick="btnView_Click" CausesValidation="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <table style="width: 100%; padding-left: 20px" cellspacing="2px" cellpadding="2px">
                                            <tr>
                                                <td class="col1">
                                                    Bed No.
                                                </td>
                                                <td class="col2">
                                                    <asp:Label ID="lblBedNo" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col1">
                                                    Systolic
                                                </td>
                                                <td class="col2">
                                                    <asp:TextBox ID="txtSys" runat="server" Width="200px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col1">
                                                    Diastolic
                                                </td>
                                                <td class="col2">
                                                    <asp:TextBox ID="txtDis" runat="server" Width="200px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col1" valign="top">
                                                    Fresh Complaint
                                                </td>
                                                <td class="col2">
                                                    <asp:TextBox ID="txtFreshComplaint" runat="server" TextMode="MultiLine" Width="300px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col1" valign="top">
                                                    Medicine Details
                                                </td>
                                                <td class="col2">
                                                    <asp:TextBox ID="txtMedicineDetails" runat="server" TextMode="MultiLine" Width="300px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col1" valign="top">
                                                    Latest Symptom
                                                </td>
                                                <td class="col2">
                                                    <asp:TextBox ID="txtLatestSymptom" runat="server" TextMode="MultiLine" Width="300px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col1" valign="top">
                                                    Reference to a Third Party Doctor
                                                </td>
                                                <td class="col2">
                                                    <asp:TextBox ID="txtReferencetoaThirdPartyDoctor" runat="server" TextMode="MultiLine"
                                                        Width="300px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col1" valign="top">
                                                    We can shift to another hospital
                                                </td>
                                                <td class="col2">
                                                    <asp:TextBox ID="txtWecanshifttoanotherhospital" runat="server" TextMode="MultiLine"
                                                        Width="300px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <table style="width: 100%; padding-left: 20px" cellspacing="2px" cellpadding="2px">
                                    <tr>
                                        <td class="col1">
                                            Blood Test
                                        </td>
                                        <td class="col2">
                                            <asp:FileUpload ID="fu_BloodTest" runat="server"></asp:FileUpload>
                                            <asp:RegularExpressionValidator ID="regfu_BloodTest" runat="server" ControlToValidate="fu_BloodTest"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF|.jpg|.JPG|.jpeg|.JPEG)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf,jpg and jpeg files are allowed.</span></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col1">
                                            Urine Test
                                        </td>
                                        <td class="col2">
                                            <asp:FileUpload ID="fu_UrineTest" runat="server"></asp:FileUpload>
                                            <asp:RegularExpressionValidator ID="regfu_UrineTest" runat="server" ControlToValidate="fu_UrineTest"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF|.jpg|.JPG|.jpeg|.JPEG)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf,jpg and jpeg files are allowed.</span></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col1">
                                            Monitor
                                        </td>
                                        <td class="col2">
                                            <asp:FileUpload ID="fu_Monitor" runat="server"></asp:FileUpload>
                                            <asp:RegularExpressionValidator ID="regfu_Monitor" runat="server" ControlToValidate="fu_Monitor"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF|.jpg|.JPG|.jpeg|.JPEG)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf,jpg and jpeg files are allowed.</span></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col1">
                                        <br /><br />
                                        </td>
                                        <td class="col2"><br />
                                            <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" Text="Save"
                                                OnClick="btnSave_Click" ValidationGroup="val" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
