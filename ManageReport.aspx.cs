﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Search.Highlight;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;
using System.IO;
using Lucene.Net.Index;
 
public partial class ManageReport : System.Web.UI.Page
{
    genral gen = new genral();
    bool rslt = false;
    SqlCommand SqlCmd;
    string StrSQL;
    SqlDataAdapter dapt;
    string strSQL = "";
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string pdId = "0";
    string uid = "0";
    string ipdid = "0";
    int Page_no = 0, Page_size = 20;
    object frm = null;
    object to = null;

    //--------------------------------------
    string title = "";
    private int toItem;

    /// <summary>
    /// Total items returned by search.
    /// </summary>
    private int total;

    /// <summary>
    /// Time it took to make the search.
    /// </summary>
    private TimeSpan duration;
    private int startAt;

    /// <summary>
    /// First item on page (user format).
    /// </summary>
    private int fromItem;

    /// <summary>
    /// How many items can be showed on one page.
    /// </summary>
    private readonly int maxResults = 10;
    protected DataTable Results = new DataTable();
    protected DataTable dt = new DataTable();
    //---------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 38";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");

        //-----------------------------------------------------------------------------------------
        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        }
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "report")
        {
            Session["rptPdId"] = e.CommandArgument.ToString();
            Response.Redirect("PatientReport.aspx",false);
        }

        if (e.CommandName == "case")
        {
            string pdID = e.CommandArgument.ToString();
            Session["ipdid"] = null;
            Session["CasePatientID"] = pdID;
            Response.Redirect("FollowupDetail.aspx", false);
        }
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }

    protected string GetFilterCasePid()
    {
        string pid="";

        var fileWildcardsArg = new[] { "" };
        var directoryArg = Server.MapPath("Files/");

        if (optTabs.SelectedValue == "1")
        {
            fileWildcardsArg = new[] { "kco_*" };
            directoryArg = Server.MapPath("Files/KCO/");
            rslt = true; 
        }
        if (optTabs.SelectedValue == "2")
        {
            fileWildcardsArg = new[] { "inv_*" };
            directoryArg = Server.MapPath("Files/Investigation/");
            rslt = true;
        }
        if (optTabs.SelectedValue == "3")
        {
            fileWildcardsArg = new[] { "chief_*" };
            directoryArg = Server.MapPath("Files/ChiefCo/");
            rslt = true;
        }
        if (optTabs.SelectedValue == "4")
        {
            fileWildcardsArg = new[] {"pho_*"};
            directoryArg = Server.MapPath("Files/PastHo/");
            rslt = true;
        }
        if (optTabs.SelectedValue == "5")
        {
            fileWildcardsArg = new[] { "fho_*" };
            directoryArg = Server.MapPath("Files/FamilyHo/");
            rslt = true;
        }
        if (optTabs.SelectedValue == "6")
        {
            fileWildcardsArg = new[] { "phy_*" };
            directoryArg = Server.MapPath("Files/PhysicalGenerals/");
            rslt = true;
        }
        if (optTabs.SelectedValue == "7")
        {
            fileWildcardsArg = new[] { "mnd_*" };
            directoryArg = Server.MapPath("Files/Mind/");
            rslt = true;
        }
        if (optTabs.SelectedValue == "8")
        {
            fileWildcardsArg = new[] { "af_*" };
            directoryArg = Server.MapPath("Files/AF/");
            rslt = true;
        }
        if (optTabs.SelectedValue == "9")
        {
            fileWildcardsArg = new[] { "thermal_*" };
            directoryArg = Server.MapPath("Files/Thermal/");
            rslt = true;
        }

        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = txtSearchCase.Text.ToString();
        var ignoreCaseArg = true;

        if (rslt == true && txtSearchCase.Text != "")
        {
            IEnumerable<string> files = gen.FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);   
            foreach (string fls in files)
            {
                int startindex = fls.IndexOf('_') + 1;
                int length = fls.Length - fls.IndexOf('_') - 1;
                pid += fls.Substring(startindex, length) + ",";
            }
            if(pid.Length > 1)
                pid = pid.Substring(0, pid.Length - 1);  
        }

        return pid;    
    }

    public void BindGVR()
    {
       // string[] _frm = txtfrm.Text.ToString().Split('/');
       // string[] _to = txtto.Text.ToString().Split('/');

       // string frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
       // string to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";

       // //strSQL = "Select pdId, pdPatientICardNo,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,pdCassetteNo,ISNULL(pdOccupation,'') pdOccupation,pdAdd1+' '+pdAdd2+' '+pdAdd3+' '+pdAdd4 As pdAdd,'' as 'sample',u.uName From dbo.tblPatientDetails pd INNER JOIN tblUser u ON u.uId = pd.pdCreatedBy Left Join dbo.tblFollowUpDatails FUD On FUD.fdPatientId = pd.pdId Left Join tblRemedyMaster RM On RM.RMID = FUD.fdRemedy ";
       // strSQL = "Select pd.pdId, pdPatientICardNo,p.pdName,pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,pdCassetteNo,ISNULL(pdOccupation,'') pdOccupation,pdAdd1+' '+pdAdd2+' '+pdAdd3+' '+pdAdd4 As pdAdd,'' as 'sample',u.uName From dbo.tblPatientDetails pd INNER JOIN tblUser u ON u.uId = pd.pdCreatedBy Left Join dbo.tblFollowUpDatails FUD On FUD.fdPatientId = pd.pdId Left Join tblRemedyMaster RM On RM.RMID = FUD.fdRemedy left join vwGetPatient p on pd.pdID=p.pdid ";

       ///* if (txtSearchCase.Text != "")
       // {

       //     if (optTabs.SelectedValue == "1")
       //     {
       //         strSQL += " inner join tblKco k ON pd.pdId = k.kpdid where kword Like '%" + txtSearchCase.Text.ToString() + "%'";                    
       //     }
       //     if (optTabs.SelectedValue == "2")
       //     {
       //         strSQL += " inner join tblInvestigation iv ON pd.pdId = iv.ivpdid where ivword Like '%" + txtSearchCase.Text.ToString() + "%'";
       //     }
       //     if (optTabs.SelectedValue == "3")
       //     {
       //         strSQL += " inner join tblChiefCo c ON pd.pdId = c.cpdid where cword Like '%" + txtSearchCase.Text.ToString() + "%'";
       //     }
       //     if (optTabs.SelectedValue == "4")
       //     {
       //         strSQL += " inner join tblPastHo ph ON pd.pdId = iv.phpdid where phword Like '%" + txtSearchCase.Text.ToString() + "%'";
       //     }
       //     if (optTabs.SelectedValue == "5")
       //     {
       //         strSQL += " inner join tblFamilyHo fh ON pd.pdId = fh.fhpdid where fhword Like '%" + txtSearchCase.Text.ToString() + "%'";
       //     }
       //     if (optTabs.SelectedValue == "6")
       //     {
       //         strSQL += " inner join tblPhysicalGenerals pg ON pd.pdId = pg.pdpdid where pgword Like '%" + txtSearchCase.Text.ToString() + "%'";
       //     }
       //     if (optTabs.SelectedValue == "7")
       //     {
       //         strSQL += " inner join tblMind m ON pd.pdId = m.mpdid where mword Like '%" + txtSearchCase.Text.ToString() + "%'";
       //     }
       //     if (optTabs.SelectedValue == "8")
       //     {
       //         strSQL += " inner join tblAF a ON pd.pdId = a.afpdid where afword Like '%" + txtSearchCase.Text.ToString() + "%'";
       //     }
       //     if (optTabs.SelectedValue == "9")
       //     {
       //         strSQL += " inner join tblThermal t ON pd.pdId = t.tpdid where tword Like '%" + txtSearchCase.Text.ToString() + "%'";
       //     }

       //     strSQL += " AND IsCancel = 0";
       // }
       // else*/
       // //{
       //     strSQL += " where IsCancel = 0";
       //// }

        
       // if (chkConsultingPeriod.Checked == true)
       //     strSQL += " AND pdDOC BETWEEN '" + frm + "' and '" + to + "'";

       // if (txtAdd.Text.ToString() != "")
       //     strSQL += " AND pdAdd1+' '+pdAdd2+' '+pdAdd3+' '+pdAdd4 Like '%" + txtAdd.Text.ToString() + "%'";

       // if (txtcasepaperno.Text.ToString() != "")
       //     strSQL += " AND pdCasePaperNo Like '%" + txtcasepaperno.Text.ToString() + "%'";

       // if (txtcassetteno.Text.ToString() != "")
       //     strSQL += " AND pdCassetteNo Like '%" + txtcassetteno.Text.ToString() + "%'";

       // if (txtname.Text.ToString() != "")
       // {
       //     //strSQL += " AND pdFname+' '+ pdMname +' '+ pdLname like '%" + txtname.Text.ToString() + "%'";
       //     strSQL += " AND p.pdName like REPLACE('%" + txtname.Text.Trim() + "%', ' ' , '%') ";
       // }

       // if (txtOccupation.Text.ToString() != "")
       //     strSQL += " AND pdOccupation Like '%" + txtOccupation.Text.ToString() + "%'";

       // if (optTabs.SelectedValue == "10")
       // {
       //     strSQL += " AND RM.RMNAME Like REPLACE('%" + txtSearchCase.Text.ToString() + "%', ' ' , '%') ";
       // }

       // strSQL += " group by pd.pdId, pdPatientICardNo,p.pdName,pdSex, pdDOC,pdTele,pdMob,pdCasePaperNo,pdCassetteNo,pdOccupation,pdAdd1,pdAdd2,pdAdd3,pdAdd4 ,u.uName";

       // dt = gen.getDataTable(strSQL);
 

       // if (txtSearchCase.Text != "")
       // {
       //     if (optTabs.SelectedValue == "1" || optTabs.SelectedValue == "2" || optTabs.SelectedValue == "3" || optTabs.SelectedValue == "4" || optTabs.SelectedValue == "5" || optTabs.SelectedValue == "6" || optTabs.SelectedValue == "7" || optTabs.SelectedValue == "8" || optTabs.SelectedValue == "9")
       //     {
       //         this.BindSearchContent();
       //         DataBind();
       //     }

       //     else
       //     {
       //         GRV1.DataSource = dt;
       //         GRV1.PageSize = Page_size;
       //         GRV1.DataBind();
       //         createpaging(dt.Rows.Count, GRV1.PageCount);
       //     }
       // }
       // else
       // {
       //     GRV1.DataSource = dt;
       //     GRV1.PageSize = Page_size;
       //     GRV1.DataBind();
       //     createpaging(dt.Rows.Count, GRV1.PageCount);
       // }

        Bind_Report();


    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }
    /////--------------------Search Result-------------------------------------------
    private void BindSearchContent()
    {
        DateTime start = DateTime.Now;

        // create the searcher
        // index is placed in "index" subdirectory
        string indexDirectory = Server.MapPath("~/App_Data/index");

        var analyzer = new StandardAnalyzer(Version.LUCENE_30);

        IndexSearcher searcher = new IndexSearcher(FSDirectory.Open(indexDirectory));

        // parse the query, "text" is the default field to search
        var parser = new QueryParser(Version.LUCENE_30, "text", analyzer);

        string title = "";
        title = txtSearchCase.Text.ToString().Replace("*", "").Replace("?", "");

        Query query = parser.Parse(title);

        // create the result DataTable
        // this.Results.Columns.Add("title", typeof(string));
        this.Results.Columns.Add("parentid", typeof(int));
        this.Results.Columns.Add("sectionId", typeof(int));
        this.Results.Columns.Add("sample", typeof(string));
        this.Results.Columns.Add("path", typeof(string));

        // search
        TopDocs hits = searcher.Search(query, 200);

        this.total = hits.TotalHits;

        // create highlighter
        IFormatter formatter = new SimpleHTMLFormatter("<span style=\"font-weight:bold;\">", "</span>");
        SimpleFragmenter fragmenter = new SimpleFragmenter(80);
        QueryScorer scorer = new QueryScorer(query);
        Highlighter highlighter = new Highlighter(formatter, scorer);
        highlighter.TextFragmenter = fragmenter;


        // initialize startAt
        int startAt = InitStartAt();

        // how many items we should show - less than defined at the end of the results
        int resultsCount = Math.Min(total, this.maxResults + this.startAt);

       // int resultsCount = total;
        //ScriptManager.RegisterStartupScript(this.Page, GetType(), "GetCount", "alert(" + hits.ScoreDocs.Length + ")", true);
       // int i = 0;
        //try
        //{

            for (int i = startAt; i < hits.ScoreDocs.Length; i++)
            {
                // get the document from index

                Document doc = searcher.Doc(hits.ScoreDocs[i].Doc);

                TokenStream stream = analyzer.TokenStream("", new StringReader(doc.Get("text")));
                String sample = highlighter.GetBestFragments(stream, doc.Get("text"), 2, "...");

                // create a new row with the result data
                DataRow row = this.Results.NewRow();
                //row["title"] = doc.Get("title");
                row["parentid"] = doc.Get("parentid");
                row["sectionId"] = doc.Get("sectionid");
                row["sample"] = sample;
                this.Results.Rows.Add(row);
            }
            DataTable dtRslt = new DataTable();
            dtRslt.Columns.Add("pdId", typeof(int));
            dtRslt.Columns.Add("pdName", typeof(string));
            dtRslt.Columns.Add("pdSex", typeof(string));
            dtRslt.Columns.Add("pdDOC", typeof(string));
            dtRslt.Columns.Add("pdAdd", typeof(string));
            dtRslt.Columns.Add("pdCasePaperNo", typeof(string));
            dtRslt.Columns.Add("pdCassetteNo", typeof(string));
            dtRslt.Columns.Add("pdOccupation", typeof(string));
            dtRslt.Columns.Add("sample", typeof(string));
            dtRslt.Columns.Add("uName", typeof(string));

            var rslt = from datatabl1 in dt.AsEnumerable()
                       join datatable2 in Results.AsEnumerable()
                           on datatabl1.Field<int>("pdId") equals datatable2.Field<int>("parentid")
                       where datatable2.Field<int>("sectionId") == int.Parse(optTabs.SelectedValue) // into Main
                       select new { datatabl1, datatable2 };

            foreach (var dr in rslt)
            {
                DataRow newRow = dtRslt.NewRow();
                // Now fill the row with the value from the query t1
                newRow["pdId"] = dr.datatabl1.Field<int>("pdId");
                newRow["pdName"] = dr.datatabl1.Field<string>("pdName");
                newRow["pdSex"] = dr.datatabl1.Field<string>("pdSex");
                newRow["pdDOC"] = dr.datatabl1.Field<DateTime>("pdDOC");
                newRow["pdAdd"] = dr.datatabl1.Field<string>("pdAdd");
                newRow["pdCasePaperNo"] = dr.datatabl1.Field<string>("pdCasePaperNo");
                newRow["pdCassetteNo"] = dr.datatabl1.Field<string>("pdCassetteNo");
                newRow["pdOccupation"] = dr.datatabl1.Field<string>("pdOccupation");
                newRow["sample"] = dr.datatable2.Field<string>("sample");
                newRow["uName"] = dr.datatabl1.Field<string>("uName");
                //.... Continue with all the columns from t1 in the same way
                // Now fill the row with the value from the query t2
                // In the same way as above
                //When all columns have been filled in then add the row to the table
                dtRslt.Rows.Add(newRow);
            }
            // if (dtRslt.Rows.Count > 0)
            {
                GRV1.DataSource = dtRslt;
                GRV1.DataBind();
                createpaging(dtRslt.Rows.Count, GRV1.PageCount);
            }
            //else
            //{
            //    GRV1.EmptyDataText.ToString();
            //    GRV1.DataBind();
            //}
            //Repeater1.DataSource = dtRslt;
            //Repeater1.DataBind();
        //}
        //catch 
        //{
            
        //}
        //finally
        //{
        //    searcher.Dispose();
        //}
    }

    private int InitStartAt()
    {
        try
        {
            int sa = Convert.ToInt32(this.Request.Params["start"]);

            // too small starting item, return first page
            if (sa < 0)
                return 0;

            // too big starting item, return last page
            if (sa >= total - 1)
            {
                return LastPageStartsAt;
            }

            return sa;
        }
        catch
        {
            return 0;
        }
    }

    private int LastPageStartsAt
    {
        get
        {
            return PageCount * maxResults;
        }
    }

    private int PageCount
    {
        get
        {
            return (total - 1) / maxResults; // floor
        }
    }

    private void Bind_Report()
    {

        string SearchType, SearchText, Name, Address, CasePaperNo, CassetteNo, Occupation = "";
        
        SearchType = optTabs.SelectedValue;

        if (txtSearchCase.Text == "")
        {
            SearchText = "%";
        }
        else
        {
            SearchText = "%" + txtSearchCase.Text + "%" ;
        }

        if (txtname.Text == "")
        {
            Name = "%";
        }
        else
        {
            Name = "%" + txtname.Text + "%" ;
        }

        if (txtAdd.Text == "")
        {
            Address = "%";
        }
        else
        {
            Address = "%" +  txtAdd.Text + "%";
        }

        if (txtcasepaperno.Text == "")
        {
            CasePaperNo = "%";
        }
        else
        {
            CasePaperNo = "%" + txtcasepaperno.Text + "%";
        }

        if (txtcassetteno.Text == "")
        {
            CassetteNo = "%";
        }
        else
        {
            CassetteNo = "%" + txtcassetteno.Text + "%";
        }

        if (txtOccupation.Text == "")
        {
            Occupation = "%";
        }
        else
        {
            Occupation = "%" + txtOccupation.Text + "%";
        }


        string[] _frm;
        string[] _to;

        if (chkConsultingPeriod.Checked == true)
        {
             if (txtfrm.Text != "")
        {
            if (txtfrm.Text.Contains("/"))
            {
                _frm = txtfrm.Text.ToString().Split('/');
                _to = txtto.Text.ToString().Split('/');

                frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
                to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";
            }

            if (txtfrm.Text.Contains("-"))
            {
                _frm = txtfrm.Text.ToString().Split('-');
                _to = txtto.Text.ToString().Split('-');

                frm = _frm[2] + "-" + _frm[1] + "-" + _frm[0] + " 00:00:00";
                to = _to[2] + "-" + _to[1] + "-" + _to[0] + " 23:55:00";
            }
        }
        }
       

        StrSQL = "Proc_GetSearchRemedyReport";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FrmDate", ToDBNull(frm));
        SqlCmd.Parameters.AddWithValue("@ToDate", ToDBNull(to));
        SqlCmd.Parameters.AddWithValue("@SearchType", SearchType);
        SqlCmd.Parameters.AddWithValue("@SearchText", SearchText);
        SqlCmd.Parameters.AddWithValue("@Name", Name);
        SqlCmd.Parameters.AddWithValue("@Address", Address);
        SqlCmd.Parameters.AddWithValue("@CasePaperNo", CasePaperNo);
        SqlCmd.Parameters.AddWithValue("@CassetteNo", CassetteNo);
        SqlCmd.Parameters.AddWithValue("@Occupation", Occupation);

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {

                GRV1.DataSource = dt;
                GRV1.DataBind();
                GRV1.PageSize = Page_size;
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
            else
            {
                GRV1.DataSource = null;
                GRV1.DataBind();
               
            }
            conn.Close();
            dr.Dispose();
        }
    }

    public static object ToDBNull(object value)
    {
        if (null != value)
            return value;
        return DBNull.Value;
    }



}