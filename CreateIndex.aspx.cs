﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;

public partial class CreateIndex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        string str = "proc_GetDataToUpdateIndex";
        SqlCommand cmd = new SqlCommand(str, new SqlConnection(ConfigurationManager.AppSettings["constring"]));
        SqlDataAdapter ad = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        ad.Fill(dt);

        int indexid = 0;
        int iId;
        foreach (DataRow dr in dt.Rows)
        {
            indexid++;
            switch (int.Parse(dr["secId"].ToString()))
            {
                case 1:
                    if (File.Exists(Server.MapPath("Files/KCO/kco_" + dr["CasePatientID"].ToString())))
                    {
                        string pdid = dr["CasePatientID"].ToString();
                        string s = Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/KCO/kco_" + dr["CasePatientID"].ToString())));
                        Index.AppendIndex(Server.MapPath("~/App_Data/index"), s, dr["CasePatientID"].ToString(), "", dr["secId"].ToString(), "", indexid.ToString());
                        Index.InsSearchIndex(indexid.ToString(), dr["secId"].ToString(), dr["CasePatientID"].ToString(), "", out iId);
                    }
                    break;
                case 2:
                    if (File.Exists(Server.MapPath("Files/Investigation/inv_" + dr["CasePatientID"].ToString())))
                    {
                        Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/Investigation/inv_" + dr["CasePatientID"].ToString()))), dr["CasePatientID"].ToString(), "", dr["secId"].ToString(), "", indexid.ToString());
                        Index.InsSearchIndex(indexid.ToString(), dr["secId"].ToString(), dr["CasePatientID"].ToString(), "", out iId);
                    }
                    break;
                case 3:
                    if (File.Exists(Server.MapPath("Files/ChiefCo/chief_" + dr["CasePatientID"].ToString())))
                    {
                        Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/ChiefCo/chief_" + dr["CasePatientID"].ToString()))), dr["CasePatientID"].ToString(), "", dr["secId"].ToString(), "", indexid.ToString());
                        Index.InsSearchIndex(indexid.ToString(), dr["secId"].ToString(), dr["CasePatientID"].ToString(), "", out iId);
                    }
                    break;
                case 4:
                    if (File.Exists(Server.MapPath("Files/PastHo/pho_" + dr["CasePatientID"].ToString())))
                    {
                        Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/PastHo/pho_" + dr["CasePatientID"].ToString()))), dr["CasePatientID"].ToString(), "", dr["secId"].ToString(), "", indexid.ToString());
                        Index.InsSearchIndex(indexid.ToString(), dr["secId"].ToString(), dr["CasePatientID"].ToString(), "", out iId);
                    }
                    break;
                case 5:
                    if (File.Exists(Server.MapPath("Files/FamilyHo/fho_" + dr["CasePatientID"].ToString())))
                    {
                        Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/FamilyHo/fho_" + dr["CasePatientID"].ToString()))), dr["CasePatientID"].ToString(), "", dr["secId"].ToString(), "", indexid.ToString());
                        Index.InsSearchIndex(indexid.ToString(), dr["secId"].ToString(), dr["CasePatientID"].ToString(), "", out iId);
                    }
                    break;
                case 6:
                    if (File.Exists(Server.MapPath("Files/PhysicalGenerals/phy_" + dr["CasePatientID"].ToString())))
                    {
                        Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/PhysicalGenerals/phy_" + dr["CasePatientID"].ToString()))), dr["CasePatientID"].ToString(), "", dr["secId"].ToString(), "", indexid.ToString());
                        Index.InsSearchIndex(indexid.ToString(), dr["secId"].ToString(), dr["CasePatientID"].ToString(), "", out iId);
                    }
                    break;
                case 7:
                    if (File.Exists(Server.MapPath("Files/Mind/mnd_" + dr["CasePatientID"].ToString())))
                    {
                        Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/Mind/mnd_" + dr["CasePatientID"].ToString()))), dr["CasePatientID"].ToString(), "", dr["secId"].ToString(), "", indexid.ToString());
                        Index.InsSearchIndex(indexid.ToString(), dr["secId"].ToString(), dr["CasePatientID"].ToString(), "", out iId);
                    }
                    break;
                case 8:
                    if (File.Exists(Server.MapPath("Files/AF/af_" + dr["CasePatientID"].ToString())))
                    {
                        Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/AF/af_" + dr["CasePatientID"].ToString()))), dr["CasePatientID"].ToString(), "", dr["secId"].ToString(), "", indexid.ToString());
                        Index.InsSearchIndex(indexid.ToString(), dr["secId"].ToString(), dr["CasePatientID"].ToString(), "", out iId);
                    }
                    break;
                case 9:
                    if (File.Exists(Server.MapPath("Files/Thermal/thermal_" + dr["CasePatientID"].ToString())))
                    {
                        Index.AppendIndex(Server.MapPath("~/App_Data/index"), Util.HTMLToString(File.ReadAllText(Server.MapPath("Files/Thermal/thermal_" + dr["CasePatientID"].ToString()))), dr["CasePatientID"].ToString(), "", dr["secId"].ToString(), "", indexid.ToString());
                        Index.InsSearchIndex(indexid.ToString(), dr["secId"].ToString(), dr["CasePatientID"].ToString(), "", out iId);
                    }
                    break;
            }
        }
        Response.Write("Success!");
    }
    protected void btn_updatekco_Click(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "kco_*" };
        var directoryArg = Server.MapPath("Files/KCO/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            string pdId = fls.Split('_')[1];
            Index.InsKCO(int.Parse(pdId));
        }
    }


    static IEnumerable<string> FindInFiles(string directoryArg, string containsTextArg, bool ignoreCaseArg, System.IO.SearchOption searchSubDirsArg, params string[] fileWildcardsArg)
    {
        List<String> files = new List<string>(); // This List accumulates files found.

        foreach (string fileWildcard in fileWildcardsArg)
        {
            string[] xFiles = System.IO.Directory.GetFiles(directoryArg, fileWildcard, searchSubDirsArg);

            foreach (string x in xFiles)
            {
                if (!files.Contains(x)) // If file not already found...
                {
                    // See if the file contains the search text.
                    // Assume a null search string matches any file.
                    // Use ToLower to perform a case-insensitive search.
                    bool containsText =
                        containsTextArg.Length == 0 ||
                        ignoreCaseArg ?
                        System.IO.File.ReadAllText(x).ToLower().Contains(containsTextArg.ToLower()) :
                        System.IO.File.ReadAllText(x).Contains(containsTextArg);

                    if (containsText)
                    {
                        files.Add(x); // This file is a keeper. Add it to the list.
                    } // if
                } // if
            } // foreach file
        }//foreach wildcard 
        return files;
    }

    protected void btn_updatechief_Click(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "chief_*" };
        var directoryArg = Server.MapPath("Files/ChiefCo/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            string pdId = fls.Split('_')[1];
            Index.InsChiefCO(int.Parse(pdId));
        }
    }
    protected void btn_updatefho_Click(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "fho_*" };
        var directoryArg = Server.MapPath("Files/FamilyHo/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            string pdId = fls.Split('_')[1];
            Index.InsFamilyHo(int.Parse(pdId));
        }
    }
    protected void btn_updateinvestigation_Click(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "inv_*" };
        var directoryArg = Server.MapPath("Files/Investigation/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            string pdId = fls.Split('_')[1];
            Index.InsInvestigation(int.Parse(pdId));
        }
    }
    protected void btn_AF_Click(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "af_*" };
        var directoryArg = Server.MapPath("Files/AF/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            string pdId = fls.Split('_')[1];
            Index.InsAF(int.Parse(pdId));
        }
    }
    protected void btn_mind_Click(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "mnd_*" };
        var directoryArg = Server.MapPath("Files/Mind/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            string pdId = fls.Split('_')[1];
            Index.InsMind(int.Parse(pdId));
        }
    }
    protected void btn_pastho_Click(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "pho_*" };
        var directoryArg = Server.MapPath("Files/PastHo/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            string pdId = fls.Split('_')[1];
            Index.InsPastHO(int.Parse(pdId));
        }
    }
    protected void btn_physicalgenerals_Click(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "phy_*" };
        var directoryArg = Server.MapPath("Files/PhysicalGenerals/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            string pdId = fls.Split('_')[1];
            Index.InsPhysicalGenerals(int.Parse(pdId));
        }
    }
    protected void btn_thermal_Click(object sender, EventArgs e)
    {
        var fileWildcardsArg = new[] { "thermal_*" };
        var directoryArg = Server.MapPath("Files/Thermal/");
        var searchSubDirsArg = System.IO.SearchOption.AllDirectories;
        var containsTextArg = "";
        var ignoreCaseArg = true;

        IEnumerable<string> files = FindInFiles(directoryArg, containsTextArg, ignoreCaseArg, searchSubDirsArg, fileWildcardsArg);

        foreach (string fls in files)
        {
            string pdId = fls.Split('_')[1];
            Index.InsThermal(int.Parse(pdId));
        }
    }
}