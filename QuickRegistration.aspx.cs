﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class QuickRegistration : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string strSQL;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");
        if (!IsPostBack)
        {
            //BindAutoCasePaperNo();
        }
    }


    public void BindAutoCasePaperNo()
    {

        DataTable dt = new DataTable();

        strSQL = "GetAutoIncCasePaperNo";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;

        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(SqlCmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            //txtCasePaperNo.Text = ds.Tables[0].Rows[0]["CasePaperAutoNo"].ToString();
            //dr = SqlCmd.ExecuteReader();

            //dt.Load(dr);

            con.Close();
        }
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (txtmob.Text != "")
        {
            String smsstring;
            decimal TemplateId=1207160931643569223;
            smsstring = "Welcome to Aditya Homoeopathic Hospital and Healing center. Kindly Visit http://drnikam.com/OnlinePatient/PatientRegistration.aspx And Enter Details to Fill Patient Registration form. ";
            string str21 = genral.GetResponse(txtmob.Text.Trim(), smsstring, TemplateId);
            txtmob.Text = string.Empty;
        }
    }
}