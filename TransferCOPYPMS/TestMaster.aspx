﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="TestMaster.aspx.cs" Inherits="TestMaster" Title="Test Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <script type="text/javascript">
        function Validate() {

            var fname = document.getElementById("<%= txtTestName.ClientID %>").value;
            var lname = document.getElementById("<%= txtCharges.ClientID %>").value;

            var Err = "";

            if (fname == "") {
                Err += "Please Fill the Test Name";
                document.getElementById("<%= txtTestName.ClientID %>").className = "field input-validation-error";
            }
            else {
                document.getElementById("<%= txtTestName.ClientID %>").className = "field";
            }

            if (lname == "") {
                Err += "Please Fill the Base Charge";
                document.getElementById("<%= txtCharges.ClientID %>").className = "field input-validation-error";
            }
            else {
                document.getElementById("<%= txtCharges.ClientID %>").className = "field";
            }

            if (Err != "") {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Test Master</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">
                                            Test Master</a> </li>
                                        <li class="qtab-HTML last" id="li_9"><a class="qt_tab active" href="ManageTest.aspx">
                                            Manage Test</a> </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto;">
                                        <table style="width: 100%; padding-left: 20px" cellspacing="0px" cellpadding="0px">
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Test Name*
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtTestName" runat="server" CssClass="field required" MaxLength="50"
                                                        Style="float: left;"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="txtTestName"
                                                        Display="Dynamic" ErrorMessage="Test Name is required." ValidationGroup="val">
                                                <span class="error">Test Name is required.</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="reg_TestName" runat="server" ControlToValidate="txtTestName"
                                                        CssClass="field required" ValidationExpression="^[\s\S]{0,100}$" ValidationGroup="val">
                                                        <span class="error">Max 100 characters allowed</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Base Charges*
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtCharges" runat="server" CssClass="field required" MaxLength="50"
                                                        Style="float: left;"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valCharges" runat="server" ControlToValidate="txtCharges"
                                                        Display="Dynamic" ErrorMessage="Base Charge is required." ValidationGroup="val">
                                                        <span class="error">Base Charge is required.</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regCounsultingAmt" runat="server" ControlToValidate="txtCharges"
                                                        ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$" ValidationGroup="val">
                                                        <span class="error">Please enter Only Numbers.</span>
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Start Date*
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtStartDate" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgDOC" TargetControlID="txtStartDate">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="imgDOC" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                        width: 20px; height: 20px;" />
                                                    <asp:RequiredFieldValidator ID="req_DOC" runat="server" ControlToValidate="txtStartDate" ValidationGroup="val"
                                                        Display="Dynamic" ErrorMessage="Start date is required!" CssClass="field required">
                                                        <span class="error">Start date is required!</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regDOC" runat="server" CssClass="field required" ValidationGroup="val"
                                                        Display="Dynamic" ControlToValidate="txtStartDate" 
                                                        ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                                                        <span class="error">Please Enter valid date!</span></asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="cvDOC" runat="server" Display="Dynamic" ControlToValidate="txtStartDate" ValidationGroup="val"
                                                        CssClass="field required" ClientValidationFunction="CallDateFun" ErrorMessage="Please enter valid date!">
                                                        <span class="error">Please enter valid date!</span></asp:CustomValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                        Style="margin-top: 2px;" Text="Save" ValidationGroup="val" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
