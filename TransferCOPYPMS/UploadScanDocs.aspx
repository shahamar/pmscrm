﻿<%@ Page Title="Upload Scan Documents" Language="C#" MasterPageFile="~/MasterHome1.master"
    AutoEventWireup="true" CodeFile="UploadScanDocs.aspx.cs" Inherits="UploadScanDocs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Src="tab.ascx" TagName="Tab" TagPrefix="tab1" %>
<%@ Register Src="~/tabReconsult.ascx" TagName="tabR" TagPrefix="TabR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sp_counter').text("0");
            setHeartbeat();
        });

        function setHeartbeat() {
            //alert("ok");
            //setTimeout("heartbeat()", 10000); // about to 2 minutes
            setTimeout("heartbeat()", 300000); // about to 5 minutes
        }

        function heartbeat() {
            $.get(
                '<%= ResolveUrl("Handler/SessionHeartbeat.ashx") %>',
                null,
                function () {
                    //                    //$("#heartbeat").show().fadeOut(1000); // just a little "red flash" in the corner :)
                    //alert('<%= Session["heartbeat"] %>');                    
                    //                    var cnt = $('#sp_counter').text();
                    //                    cnt = parseInt(cnt) + 5;
                    //                    $('#sp_counter').text(cnt);
                    //                    alert("ok")
                    //                    

                },
                "json"
            );
            setHeartbeat();
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <table style="width: 100%; background-color: #FFFFFF;">
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="login-area margin_ten_right" style="width: 98%;">
                        <h2>
                            Upload Scanned Documents</h2>
                        <div class="formmenu">
                            <div class="loginform">
                                <tab1:Tab ID="tabKCO" runat="server"></tab1:Tab>
                                <div id="div1" runat="server" style="display: none;">
                                    <TabR:tabR ID="tabRKCO" runat="server" />
                                </div>
                                <ul class="quicktabs_tabs quicktabs-style-excel" style="border-left: 2px solid #E2E2E2;
                                    border-right: 2px solid #E2E2E2;">
                                    <li class="qtab-Demo first" id="li_1">
                                        <asp:LinkButton ID="lnkKCO" runat="server" CssClass="qt_tab active" Enabled="False">K/C/O</asp:LinkButton>
                                    </li>
                                    <li class="qtab-HTML" id="li_2">
                                        <asp:LinkButton ID="lnkInvestigation" runat="server" CssClass="qt_tab active" PostBackUrl="~/Investigation.aspx">Investigation</asp:LinkButton>
                                    </li>
                                    <li class="qtab-HTML" id="li_3">
                                        <asp:LinkButton ID="lnkChiefCo" runat="server" CssClass="qt_tab active" PostBackUrl="~/ChiefCO.aspx">Chief C/O</asp:LinkButton>
                                    </li>
                                    <li class="qtab-HTML" id="li_4">
                                        <asp:LinkButton ID="lnkPastHo" runat="server" CssClass="qt_tab active" PostBackUrl="~/PastHO.aspx">Past H/o</asp:LinkButton>
                                    </li>
                                    <li class="qtab-HTML" id="li_5">
                                        <asp:LinkButton ID="lnkFamilyHo" runat="server" CssClass="qt_tab active" PostBackUrl="~/FamilyHo.aspx">Family H/o</asp:LinkButton>
                                    </li>
                                    <li class="qtab-HTML" id="li_6">
                                        <asp:LinkButton ID="lnkPhysicalgenerals" runat="server" CssClass="qt_tab active"
                                            PostBackUrl="~/PhysicalGenerals.aspx">Physical Generals</asp:LinkButton>
                                    </li>
                                    <li class="qtab-HTML" id="li_7">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="qt_tab active" PostBackUrl="~/Mind.aspx">Mind</asp:LinkButton>
                                    </li>
                                    <li class="qtab-HTML" id="li_8">
                                        <asp:LinkButton ID="lnkAf" runat="server" CssClass="qt_tab active" PostBackUrl="~/AF.aspx">A/F</asp:LinkButton>
                                    </li>
                                    <li class="qtab-HTML" id="li_9">
                                        <asp:LinkButton ID="lnkThermal" runat="server" CssClass="qt_tab active" PostBackUrl="~/Thermal.aspx">Thermal</asp:LinkButton>
                                    </li>
                                    <li class="qtab-HTML active last" id="li_10">
                                        <asp:LinkButton ID="lnkUploadDoc" runat="server" CssClass="qt-tab active" PostBackUrl="~/UploadScanDocs.aspx">Upload Docs</asp:LinkButton>
                                    </li>
                                </ul>
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                    <table style="width: 100%; padding-left: 20px" cellspacing="2px" cellpadding="2px">
                                        <tr>
                                            <td colspan="2" style="text-align: center; color: Red;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center; color: Red;">
                                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                K/C/O
                                            </td>
                                            <td>
                                                Investigation
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:FileUpload ID="fu_KCO" runat="server" />
                                                <asp:RegularExpressionValidator ID="regKCO" runat="server" ControlToValidate="fu_KCO"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf files are allowed.</span></asp:RegularExpressionValidator>
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="fu_Inv" runat="server" />
                                                <asp:RegularExpressionValidator ID="regInv" runat="server" ControlToValidate="fu_Inv"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf files are allowed.</span></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Chief C/O
                                            </td>
                                            <td>
                                                Past H/O
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:FileUpload ID="fu_ChiefCO" runat="server" />
                                                <asp:RegularExpressionValidator ID="regChief" runat="server" ControlToValidate="fu_ChiefCO"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf files are allowed.</span></asp:RegularExpressionValidator>
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="fu_PastHO" runat="server" />
                                                <asp:RegularExpressionValidator ID="regPAst" runat="server" ControlToValidate="fu_PastHO"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf files are allowed.</span></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Family H/O
                                            </td>
                                            <td>
                                                Physical Generals
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:FileUpload ID="fu_FamilyHO" runat="server" />
                                                <asp:RegularExpressionValidator ID="regFamilyHO" runat="server" ControlToValidate="fu_FamilyHO"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf files are allowed.</span></asp:RegularExpressionValidator>
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="fu_Physical" runat="server" />
                                                <asp:RegularExpressionValidator ID="regPhysical" runat="server" ControlToValidate="fu_Physical"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf files are allowed.</span></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Mind
                                            </td>
                                            <td>
                                                A/F
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:FileUpload ID="fu_Mind" runat="server" />
                                                <asp:RegularExpressionValidator ID="regMind" runat="server" ControlToValidate="fu_Mind"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf files are allowed.</span></asp:RegularExpressionValidator>
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="fu_AF" runat="server" />
                                                <asp:RegularExpressionValidator ID="regAF" runat="server" ControlToValidate="fu_AF"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf files are allowed.</span></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Thermal
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:FileUpload ID="fu_Thermal" runat="server" />
                                                <asp:RegularExpressionValidator ID="regThermal" runat="server" ControlToValidate="fu_Thermal"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf files are allowed.</span></asp:RegularExpressionValidator>
                                                
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                    Style="margin-top: 2px;" Text="Save" ValidationGroup="val" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clr">
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
