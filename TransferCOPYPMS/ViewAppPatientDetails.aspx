﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewAppPatientDetails.aspx.cs" Inherits="ViewAppPatientDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc1" %>

<%@ Register Src="tab.ascx" TagName="Tab" TagPrefix="tab1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Patient Details</title>
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style type="text/css">
.fancy-green .ajax__tab_header
{
	background: url(images/green_bg_Tab.gif) repeat-x;
	cursor:pointer;
}
.fancy-green .ajax__tab_hover .ajax__tab_outer, .fancy-green .ajax__tab_active .ajax__tab_outer
{
	background: url(images/green_left_Tab.gif) no-repeat left top;
}
.fancy-green .ajax__tab_hover .ajax__tab_inner, .fancy-green .ajax__tab_active .ajax__tab_inner
{
	background: url(images/green_right_Tab.gif) no-repeat right top;
}
.fancy .ajax__tab_header
{
	font-size: 13px;
	font-weight: bold;
	color: #000;
	font-family: sans-serif;
}
.fancy .ajax__tab_active .ajax__tab_outer, .fancy .ajax__tab_header .ajax__tab_outer, .fancy .ajax__tab_hover .ajax__tab_outer
{
	height: 46px;
}
.fancy .ajax__tab_active .ajax__tab_inner, .fancy .ajax__tab_header .ajax__tab_inner, .fancy .ajax__tab_hover .ajax__tab_inner
{
	height: 46px;
	margin-left: 16px; /* offset the width of the left image */
}
.fancy .ajax__tab_active .ajax__tab_tab, .fancy .ajax__tab_hover .ajax__tab_tab, .fancy .ajax__tab_header .ajax__tab_tab
{
	margin: 16px 16px 0px 0px;
}
.fancy .ajax__tab_hover .ajax__tab_tab, .fancy .ajax__tab_active .ajax__tab_tab
{
	color: #fff;
}
.fancy .ajax__tab_body
{
	font-family: Arial;
	font-size: 10pt;
	border-top: 0;
	border:1px solid #999999;
	padding: 8px;
	background-color: #ffffff;
}
.tabdiv
{
	border-width: 2px 2px 0px; 
	border-style: solid solid none; 
	border-color: #E2E2E2; 
	position: inherit; 
	height: auto;
}

#tabtbl
{
	color: #000;
    font-size: 12px;
    font-weight: lighter;
    padding: 2px;
    background-color:#F1F1F1
}

.alt1 
{
	width:65px;
    }
.alt2 {
    width:125px;
}

</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width:100%; background-color:#FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        Patient Details</h2>
                    <div class="formmenu">
                        <div class="loginform">
                           <table id="tabtbl" style="width:100%;" cellpadding="2px" cellspacing="0px" border="1px">
        <tr>
            <td colspan="6">
                <div style="float:right;">
                    <asp:LinkButton ID="lnkHistory" runat="server" onclick="lnkHistory_Click">History</asp:LinkButton>
                 </div>
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Patient Name :</td>
            <td class="alt2">
                <asp:Label ID="lblPatientName" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Age :</td>
            <td class="alt2">
                <asp:Label ID="lblAge" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Gender :</td>
            <td class="alt2">
                <asp:Label ID="lblGender" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Tele :</td>
            <td class="alt2">
                <asp:Label ID="lbltele" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Mobile :</td>
            <td class="alt2">
                <asp:Label ID="lblmob" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Email :</td>
            <td class="alt2">
                <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Patient ID :</td>
            <td class="alt2">
                <asp:Label ID="lblPatientId" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Case Paper No :</td>
            <td class="alt2">
                <asp:Label ID="lblCasePaperNo" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Cassette No :</td>
            <td class="alt2">
                <asp:Label ID="lblCassetteNo" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="alt1">Occupation :</td>
            <td class="alt2"><asp:Label ID="lblOccupation" runat="server" Text=""></asp:Label></td>
            <td class="alt1">Doctor Name :</td>
            <td class="alt2"><asp:Label ID="lblDoctorName" runat="server"></asp:Label></td>
            <td class="alt1"></td>
            <td class="alt2"></td>
        </tr>
    </table>
                           
                            <cc2:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></cc2:ToolkitScriptManager>
                            <cc2:TabContainer ID="TabContainer1" runat="server" CssClass="fancy fancy-green" 
                                        ActiveTabIndex="6">
                             <cc2:TabPanel ID="Page1" runat="server" >
                                <HeaderTemplate>K/C/O</HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlKCO" runat="server">
                                            <asp:Label ID="lblkco" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                            </cc2:TabPanel>
                             <cc2:TabPanel ID="Investigation" runat="server" >
                                <HeaderTemplate>Investigation</HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlInv" runat="server">
                                            <asp:Label ID="lblInvestigation" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                             </cc2:TabPanel>
                             <cc2:TabPanel ID="ChiefCO" runat="server" >
                                <HeaderTemplate>Chief C/O</HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlCC" runat="server">
                                            <asp:Label ID="lblchiefco" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                             </cc2:TabPanel>
                             <cc2:TabPanel ID="PastHO" runat="server" >
                                <HeaderTemplate>Past H/o</HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlHO" runat="server">
                                            <asp:Label ID="lblpastho" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                             </cc2:TabPanel>                             
                             <cc2:TabPanel ID="FHO" runat="server" >
                                <HeaderTemplate>Family H/o</HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlFHO" runat="server">
                                            <asp:Label ID="lblfamilyho" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                             </cc2:TabPanel>
                             <cc2:TabPanel ID="PhysicalGenerals" runat="server" >
                                <HeaderTemplate>Physical Generals</HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlPG" runat="server">
                                            <asp:Label ID="lblpg" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                             </cc2:TabPanel>
                             <cc2:TabPanel ID="Mind" runat="server" >
                                <HeaderTemplate>Mind</HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlMind" runat="server">
                                            <asp:Label ID="lblmind" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                             </cc2:TabPanel>
                             <cc2:TabPanel ID="AF" runat="server" >
                                <HeaderTemplate>A/F</HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlAF" runat="server">
                                            <asp:Label ID="lblAF" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                             </cc2:TabPanel>
                             <cc2:TabPanel ID="Thermal" runat="server" >
                                <HeaderTemplate>Thermal</HeaderTemplate>
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlThermal" runat="server">
                                            <asp:Label ID="lblThermal" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                             </cc2:TabPanel>   
                            </cc2:TabContainer>

                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
