﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class ViewAppointment : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string pdId = "0";
    string uid = "0";
    string ipdid = "0";
    int Page_no = 0, Page_size = 10;
    bool IsExport = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 49";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            Session["CasePatientID"] = null;
            BindGVR();
        }
    }
    public void BindGVR()
    {
        string[] _frm = txtfrm.Text.ToString().Split('/');
        string[] _to = txtto.Text.ToString().Split('/');

        string frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
        string to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";

        //strSQL = "Select pdId, pdPatientICardNo,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,pdCassetteNo,ISNULL(pdOccupation,'') pdOccupation,pdAdd1+' '+pdAdd2+' '+pdAdd3+' '+pdAdd4 As pdAdd From dbo.tblPatientDetails where IsCancel = 0";
        
        //if (chkConsultingPeriod.Checked == true)
        //    strSQL += " AND pdDOC BETWEEN '" + frm + "' and '" + to + "'";

        //if (txtAdd.Text != "")
        //    strSQL += " AND (pdAdd1+' '+pdAdd2+' '+pdAdd3+' '+pdAdd4) Like '%" + txtAdd.Text.ToString() + "%'";

        //if (txtcasepaperno.Text.ToString() != "")
        //    strSQL += " AND pdCasePaperNo Like '%" + txtcasepaperno.Text.ToString() + "%'";

        //if (txtcassetteno.Text.ToString() != "")
        //    strSQL += " AND pdCassetteNo Like '%" + txtcassetteno.Text.ToString() + "%'";

        //if (txtname.Text.ToString() != "")
        //    strSQL += " AND pdFname+' '+ pdMname +' '+ pdLname like '%" + txtname.Text.ToString() + "%'";

        //if (txtOccupation.Text.ToString() != "")
        //    strSQL += " AND pdOccupation Like '%" + txtOccupation.Text.ToString() + "%'";

        string type = "";
        if(chkApType.Checked)
            type=ddlApType.SelectedValue;
        else
            type="0";

        SqlCmd = new SqlCommand();
        SqlCmd.CommandText = "GetTodayAppointment";
        SqlCmd.Parameters.AddWithValue("@FrmDate", frm);
        SqlCmd.Parameters.AddWithValue("@ToDate", to);
        SqlCmd.Parameters.AddWithValue("@pdCasePaperNo", txtcasepaperno.Text.Trim());
        SqlCmd.Parameters.AddWithValue("@pdCassetteNo", txtcassetteno.Text.Trim());
        SqlCmd.Parameters.AddWithValue("@pdName", txtname.Text.Trim());
        SqlCmd.Parameters.AddWithValue("@pdOccupation", txtOccupation.Text.Trim());
        SqlCmd.Parameters.AddWithValue("@APType", type);

        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Connection = con;
        SqlDataAdapter dapt = new SqlDataAdapter(SqlCmd);
        DataSet ds = new DataSet();
        con.Open();
        dapt.Fill(ds, "Table");
        con.Close();

        GRV1.DataSource = ds.Tables[0];
        //GRV1.DataSource = ds.Tables[0];
        pnlHead.Visible = true;
        //DataTable dt = gen.getDataTable(strSQL);
        //GRV1.DataSource = dt;
        GRV1.PageSize = Page_size;
        GRV1.DataBind();
        createpaging(ds.Tables[0].Rows.Count, GRV1.PageCount);
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int pdID = Convert.ToInt32(e.CommandArgument);
        if (e.CommandName == "history")
        {
            Session["rptPdId"] = pdID;
            Response.Redirect("PatientReport.aspx");
            //Response.OpenNewDamnPage("PatientReport.aspx");
        }
        if (e.CommandName == "del")
        {
            try
            {
                SqlCmd = new SqlCommand();
                SqlCmd.CommandText = "proc_DeleteAppointmentDetails";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlCmd.Parameters.AddWithValue("@adId", e.CommandArgument);
                SqlCmd.Parameters.AddWithValue("@deletedBy", uid);
                SqlCmd.ExecuteNonQuery();
                con.Close();
                SqlCmd.Dispose();
            }
            catch
            { }
        }
        BindGVR();
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
        ScriptManager1.RegisterPostBackControl(this.imgexport);
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }

    protected void GRV1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        
    }
    protected void GRV1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        
    }
    protected void GRV1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (IsExport == true)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkpdName = (LinkButton)e.Row.FindControl("lnkpdName");
                Literal lit = new Literal();
                lit.Text = lnkpdName.Text;
                e.Row.Cells[0].Controls.Add(lit);
                e.Row.Cells[0].Controls.Remove(lnkpdName);
            }
        }
    }
    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        IsExport = true;
        GRV1.AllowPaging = false;
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Appointment.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRV1.ShowHeader = true;
        this.BindGVR();
        GRV1.Columns[5].Visible = false;
        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
}