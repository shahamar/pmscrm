﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class ManagePatients : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string uid;
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx", false);

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 12";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["flag"] == "1")
            lblMessage.Text = "Patient details updated successfully";

        if (Request.QueryString["flag"] == "2")
            lblMessage.Text = "Patient details deleted successfully";

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            BindGVR();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }

    public void BindGVR()
    {
        try
        {
            string[] _frm = txtfrm.Text.ToString().Split('/');
            string[] _to = txtto.Text.ToString().Split('/');

            string frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            string to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";

            //strSQL = "Select pdId, pdPatientICardNo,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,IsNull(pdOccupation,'') pdOccupation,CASE pdIsRegistrationFeesPaid WHEN 1 THEN '<a href=''ConsultingBill.aspx?id='+CAST(pdID AS VARCHAR)+'''& target=_blank><img src=''images/print1.png'' /></a>' WHEN 0 THEN '' END AS 'PrintBill',pdCassetteNo,ISNULL(pdRCasePaperNo,'') 'pdRCasePaperNo' From dbo.tblPatientDetails where IsCancel = 0";

            //strSQL = "Select pdId, pdPatientICardNo,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,IsNull(pdOccupation,'') pdOccupation,CASE  WHEN pdIsRegistrationFeesPaid In (1,3) THEN '<a href=''ConsultingBill.aspx?id='+CAST(pdID AS VARCHAR)+'''& target=_blank><img src=''images/print1.png'' /></a>' else '' END AS 'PrintBill',pdCassetteNo, Case When pdRCasePaperNo is NULL Then '' when pdRCasePaperNo = '0' Then '' else +' - '+ pdRCasePaperNo end 'pdRCasePaperNo',ISNULL(pdMob,'') AS 'pdMob' From dbo.tblPatientDetails where IsCancel = 0";

            strSQL = "Select pd.pdId, pdPatientICardNo,p.pdName, pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,IsNull(pdOccupation,'') pdOccupation,CASE  WHEN pdIsRegistrationFeesPaid In (1,3) THEN '<a href=''ConsultingBill.aspx?id='+CAST(pd.pdID AS VARCHAR)+'''& target=_blank><img src=''images/print1.png'' /></a>' else '' END AS 'PrintBill',pdCassetteNo, Case When pdRCasePaperNo is NULL Then '' when pdRCasePaperNo = '0' Then '' else +' - '+ pdRCasePaperNo end 'pdRCasePaperNo',ISNULL(pdMob,'') AS 'pdMob' From dbo.tblPatientDetails pd left join vwGetPatient p on pd.pdID=p.pdid where IsCancel = 0";


            if (chkConsultingPeriod.Checked == true)
                strSQL += " AND pdDOC BETWEEN '" + frm + "' and '" + to + "'";
            if (txtCardNo.Text.ToString() != "")
                strSQL += " AND pdPatientICardNo Like '%" + txtCardNo.Text.ToString() + "%'";

            if (txtcasepaperno.Text.ToString() != "")
                strSQL += " AND pdCasePaperNo Like '%" + txtcasepaperno.Text.ToString() + "%'";

            if (txtcassetteno.Text.ToString() != "")
                strSQL += " AND pdCassetteNo Like '%" + txtcassetteno.Text.ToString() + "%'";

            if (txtname.Text.ToString() != "")
            {
                //strSQL += " AND pdInitial+' '+ pdFname +' '+ pdMname +' '+ pdLname like '%" + txtname.Text.ToString() + "%'";
                strSQL += " AND p.pdName like REPLACE('%" + txtname.Text.Trim() + "%', ' ' , '%') ";
            }

            if (txtOccupation.Text != "")
                strSQL += " AND pdMob Like '%" + txtOccupation.Text + "%'";

            strSQL += " Order by pdId desc";

            DataTable dt = gen.getDataTable(strSQL);
            if (dt.Rows.Count > 0)
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
            else
            {
                GRV1.EmptyDataText.ToString();
                GRV1.DataBind();
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string pdId = e.CommandArgument.ToString();
        if (e.CommandName == "edt")
        {
            Session["edtPdId"] = pdId;
            Response.Redirect("AddNewPatient.aspx");
        }
        if (e.CommandName == "_cancel")
        {
            int aId = Convert.ToInt32(e.CommandArgument);
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeletePatient";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@pdId", pdId);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();

            //gen.executeQuery("Update tblPatientDetails Set IsCancel = 1 Where pdID=" + pdId);
            gen.executeQuery("Update tblAppointmentDetails Set App_Cancel = 1 , adModifiedBy = " + uid + " , adModifiedDate = getDate() Where CONVERT(date, AppointmentDate) = CONVERT(date, getdate())   And PatientID = " + pdId);
            Response.Redirect("ManagePatients.aspx?flag=2", false);
        }
        if (e.CommandName == "_Reconsulting")
        {
            Session["CasePatientID"] = pdId;
            Response.Redirect("AddNewReconsulting.aspx");
        }
        BindGVR();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }

    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }



    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        BindGVR();
    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }
}
