﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="SetAppointment.aspx.cs" Inherits="SetAppointment" Title="Set Appointment" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width:100%; background-color:#FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                        <h2>Set Appointment</h2>
                        <div class="formmenu">
                            <div class="loginform">
                                <ul class="quicktabs_tabs quicktabs-style-excel">
                                <li class="qtab-Demo active first" id="li_1">
                                    <a href="NewAppointment.aspx" style="text-decoration:none;"> For New Patient</a>
                                </li>
                                
                                <li class="qtab-HTML last" id="li_9">
                                    <a href="SearchPatient.aspx" class='example7'> For Old Patient</a>
                                </li>
                                
                                </ul>
                                
                        </div>
                        <div style="text-align:center; font-family:Verdana; font-size:medium; font-weight:bold; margin-top:10px;">
                            <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
        </ContentTemplate>
    </asp:UpdatePanel> 

</asp:Content>

