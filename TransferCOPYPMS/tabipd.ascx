﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="tabipd.ascx.cs" Inherits="tabipd" %>
<style>
.tabdiv
{
	border-width: 2px 2px 0px; 
	border-style: solid solid none; 
	border-color: #E2E2E2; 
	position: inherit; 
	height: auto;
}

#tabtbl
{
	color: #000;
    font-size: 12px;
    font-weight: lighter;
    padding: 2px;
    background-color:#F1F1F1
}

.alt1 
{
	width:65px;
    font-weight:bold;
    }
.alt2 {
    width:125px;
}

</style>

<div class="tabdiv">

 <table id="tabtbl" style="width:100%;" cellpadding="2px" cellspacing="0px" border="1px">
        <tr>
            <td colspan="8">
                <div style="float:right;">&nbsp;
                <% if (Session["ipdid"] != null)
                    {%>                           
                <asp:LinkButton ID="lnkTest" runat="server" PostBackUrl="IPDTestDetails.aspx">Add IPD Test</asp:LinkButton>
                &nbsp;|&nbsp;
                <%
                }
                %>	
                 <a href="AddIPDRound.aspx">IPD Round</a>
                &nbsp;|&nbsp;      
                 <a href="IPDViewAks.aspx?DISPROW=ALL">IPD View</a>
                &nbsp;|&nbsp;  
                
                <a href="IPDDetails.aspx" class="example7">IPD Details</a>
                &nbsp;|&nbsp;   
                
                  <a href="IPDPaper.aspx" target="_blank">IPD Paper</a>
                &nbsp;|&nbsp; 
                             
                <a href="ProfarmaOfCase.aspx">My Case</a></div> 
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Pat. Name :</td>
            <td class="alt2">
                <asp:Label ID="lblPatientName" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Age :</td>
            <td class="alt2">
                <asp:Label ID="lblAge" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Gender :</td>
            <td class="alt2">
                <asp:Label ID="lblGender" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Contact No. :</td>
            <td class="alt2">
                <asp:Label ID="lblmob" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
          <td class="alt1">
                Address :</td>
            <td class="alt2" colspan="3">
                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
            </td>

                <td class="alt1">
                City :</td>
            <td class="alt2">
                <asp:Label ID="lblemail" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
            </td>

            <td class="alt1">
                State :</td>
            <td class="alt2">
                <asp:Label ID="lbltele" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Label ID="lblState" runat="server" Text=""></asp:Label>
            </td>
            
        
            
        </tr>
        <tr>
            <td class="alt1">
                Weight :</td>
            <td class="alt2">
                <asp:Label ID="lblWeight" runat="server" Text=""></asp:Label>
                <asp:Label ID="lblPatientId" runat="server" Text="" Visible="false"></asp:Label>
            </td>
            <td class="alt1">
                Case Paper No :</td>
            <td class="alt2">
                <a href="ViewSelectedScanDoc.aspx?CPNO=<%=lblCasePaperNo.Text %>" target="_blank">
                <asp:Label ID="lblCasePaperNo" runat="server" Text=""></asp:Label>
                 </a>
            </td>
            <td class="alt1">
               <%-- CD No :--%>
               Doctor Name :
           </td>
            <td class="alt2">
                <asp:Label ID="lblCassetteNo" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Label ID="lblDoctorName" runat="server"></asp:Label>
            </td>
            <td class="alt1">
                <%--Cassette No :--%>
                Cons. Date:
                </td>
            <td class="alt2">
                <asp:Label ID="lblConsDate" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        
        <tr style="display:none">
            <td class="alt1">Occupation :</td>
            <td class="alt2"><asp:Label ID="lblOccupation" runat="server" Text=""></asp:Label></td>
            <td class="alt1">Doctor Name :</td>
            <td class="alt2"></td>
            <td class="alt1"> </td>
            <td class="alt2"> </td>
            <td class="alt1"> </td>
            <td class="alt2"> </td>
        </tr>

    </table>

   <%-- <table>
    <tr>
    <td>
    <br />
   </td>
    </tr>
    </table>--%>

    <%--  <hr />--%>

      <div style="height:10px;">
      
      </div>

   <table id="tabtbl" style="width:100%;" cellpadding="2px" cellspacing="0px" border="1px">  

       <tr>
            <td class="alt1">Ward Type :</td>
            <td class="alt2"><asp:Label ID="lblWardType" runat="server" Text=""></asp:Label></td>
            <td class="alt1">Ward :</td>
            <td class="alt2"><asp:Label ID="lblWard" runat="server"></asp:Label></td>
            <td class="alt1">Bed : </td>
            <td class="alt2"><asp:Label ID="lblBed" runat="server"></asp:Label></td>
             <td class="alt1">Adm. Date : </td>
            <td class="alt2"><asp:Label ID="lblAdmDate" runat="server"></asp:Label></td>
        </tr>

        <tr>
            <td class="alt1">Adm. Time :</td>
            <td class="alt2"><asp:Label ID="lblAdmTime" runat="server" Text=""></asp:Label></td>
            <td class="alt1">Total Bill :</td>
            <td class="alt2"><asp:Label ID="lblBill" runat="server"></asp:Label></td>
            <td class="alt1">Deposit : </td>
            <td class="alt2"><asp:Label ID="lblDisc" runat="server"></asp:Label></td>
            <td class="alt1">Balance </td>
            <td class="alt2"><asp:Label ID="lblBalance" runat="server"></asp:Label>
            <div style="float:right;">
               <asp:Label ID="lblVoucherAmt" runat="server" ForeColor="Red" Visible="false"></asp:Label>
            </div>
         
            </td>
        </tr>

   </table>

 <%--  <hr />--%>

</div>