﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class IPDViewAks : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    string StrSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole, msg;
    //string pdId = "";
    string uid = "";
    string DISPROW = "";
    object frm = null;
    object to = null;

    //string ipdid = "";
    //added by nikita on 12th march 2015------------------------------

    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------



        if (Request.QueryString["DISPROW"] != null)
            DISPROW = Request.QueryString["DISPROW"].ToString();

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            else
            {
                Session["ipdid"] = Convert.ToInt32(gen.executeScalar("Select ipdId From tblIpdMaster Where ipdpdId =" + PdID).ToString());
                ViewState["ipdid"] = Session["ipdid"];
            }

            //---Added By Nikita On 30thMarch 2015----------------------------------------------------
            if (msg == "1")
            {
                btnPrint.Enabled = false;
                btnPrintIPD.Visible = false;
            }
            else
            {
                btnPrint.Enabled = true;
                btnPrintIPD.Visible = true;
            }
            //-----------------------------------------------------------------------------------------
            txtDOA.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtDOV.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();


        }
        SetTabMenu();

        if (!(Session["Report"] as string == "{CrystalDecisions.CrystalReports.Engine.ReportDocument}"))
        {
            CrystalReportViewer2.ReportSource = (ReportDocument)Session["Report"];
        }

    }

    public void FillData()
    {
        strSQL = "Select LEFT(ipdAdmissionTime,LEN(ipdAdmissionTime)-5) +' '+ Right(ipdAdmissionTime,LEN(ipdAdmissionTime)-7) As 'ipdAdmissionTime'  , DBO.GetTotalDepositAmountAks(" + ipdid + ") As DepAmt , * From tblIpdMaster Where IsDelete = 0 AND ipdpdId =" + PdID;
        //ipdId=" + ipdid;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            if (Sqldr["ipdProvisionalDiagnosis"] != DBNull.Value && Sqldr["ipdProvisionalDiagnosis"].ToString() != "")
                lblProvisionalDiagnosis.Text = "<font class='fontin'>Provisional Diagnosis :</font><br /><div class='divin'>" + Sqldr["ipdProvisionalDiagnosis"].ToString().Replace("\n", "<br />") + "</div>";
            else
                lblProvisionalDiagnosis.Text = "<font class='fontin'>Provisional Diagnosis :</font><br /><div class='divin'><font style='color:Red;'>Not Mentioned</font></div>";

            if (Sqldr["ipdFinalDiagnosis"] != DBNull.Value && Sqldr["ipdFinalDiagnosis"].ToString() != "")
                lblFinalDiagnosis.Text = "<font class='fontin'>Final Diagnosis :</font><br /><div class='divin'>" + Sqldr["ipdFinalDiagnosis"].ToString().Replace("\n", "<br />") + "</div>";
            else
                lblFinalDiagnosis.Text = "<font class='fontin'>Final Diagnosis :</font><br /><div class='divin'><font style='color:Red;'>Not Mentioned</font></div>";

            if (Sqldr["ReconsultingDiagnosis"] != DBNull.Value && Sqldr["ReconsultingDiagnosis"].ToString() != "")
                lblReconsultingDiagnosis.Text = "<font class='fontin'>Reconsulting Diagnosis :</font><br /><div class='divin'>" + Sqldr["ReconsultingDiagnosis"].ToString().Replace("\n", "<br />") + "</div>";
            else
                lblReconsultingDiagnosis.Text = "<font class='fontin'>Reconsulting Diagnosis :</font><br /><div class='divin'><font style='color:Red;'>Not Mentioned</font></div>";

            if (Sqldr["ipdOperation"] != DBNull.Value && Sqldr["ipdOperation"].ToString() != "")
                lblOperation.Text = "<font class='fontin'>Operation :</font><br /> <div class='divin'>" + Sqldr["ipdOperation"].ToString().Replace("\n", "<br />") + "</div>";
            else
                lblOperation.Text = "<font class='fontin'>Operation :</font><br /> <div class='divin'><font style='color:Red;'>Not Mentioned</font></div>";

            if (Sqldr["ipdAdmissionDate"] != DBNull.Value && Sqldr["ipdAdmissionDate"].ToString() != "")
                lblDOA.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["ipdAdmissionDate"].ToString()));
            else
                lblDOA.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdAdmissionTime"] != DBNull.Value && Sqldr["ipdAdmissionTime"].ToString() != "")
                lblTime.Text = Sqldr["ipdAdmissionTime"].ToString();
            else
                lblTime.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdRefBy"] != DBNull.Value && Sqldr["ipdRefBy"].ToString() != "")
                lblRefBy.Text = Sqldr["ipdRefBy"].ToString();
            else
                lblRefBy.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdCheckeddBy"] != DBNull.Value && Sqldr["ipdCheckeddBy"].ToString() != "")
                lblChecked.Text = Sqldr["ipdCheckeddBy"].ToString();
            else
                lblChecked.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdPreparedBy"] != DBNull.Value && Sqldr["ipdPreparedBy"].ToString() != "")
                lblPrepared.Text = Sqldr["ipdPreparedBy"].ToString();
            else
                lblPrepared.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdTreatment"] != DBNull.Value && Sqldr["ipdTreatment"].ToString() != "")
                lblTreatment.Text = Sqldr["ipdTreatment"].ToString();
            else
                lblTreatment.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdDischargeDate"] != DBNull.Value && Sqldr["ipdDischargeDate"].ToString() != "")
                lblDichargeDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["ipdDischargeDate"].ToString()));
            else
                lblDichargeDate.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["ipdDischargeTime"] != DBNull.Value && Sqldr["ipdDischargeTime"].ToString() != "")
                lblDischargeTime.Text = Sqldr["ipdDischargeTime"].ToString();
            else
                lblDischargeTime.Text = "<font style='color:Red;'>Not Mentioned</font>";

            if (Sqldr["DepAmt"] != DBNull.Value && Sqldr["DepAmt"].ToString() != "")
                lblDepAmt.Text = Sqldr["DepAmt"].ToString();
            else
                lblDepAmt.Text = "<font style='color:Red;'>Not Mentioned</font>";

            DDL_Diet.SelectedValue = Sqldr["Diet"].ToString();

            if (Sqldr["DietDate"] != DBNull.Value && Sqldr["DietDate"].ToString() != "")
                txt_DietDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["DietDate"].ToString()));

            DDL_DietGiven.SelectedValue = Sqldr["DietGiven"].ToString();

            if (Sqldr["DietGivenDate"] != DBNull.Value && Sqldr["DietGivenDate"].ToString() != "")
                txt_DietGivenDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["DietGivenDate"].ToString()));

            txt_DietCharge.Text = Sqldr["DietCharge"].ToString();

        }
        Sqldr.Close();
        Sqldr.Dispose();
        //***********************************************************************************************************************************
        FillWardDetails();

        //if (gen.doesExist("Select * From tblIpdMaster Where ipdId=" + ipdid + " AND ipdIsDischarge = 1") == true)
        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (msg == "1")
            divshiftward.Visible = false;
        else
        {
            if (gen.doesExist("Select * From tblIpdMaster Where IsDelete = 0 AND ipdId=" + ipdid + "") == true)
                divshiftward.Visible = true;
        }

        if (msg == "1")
            divrelativward.Visible = false;
        else
        {
            if (gen.doesExist("Select * From tblIpdMaster Where IsDelete = 0 AND ipdId=" + ipdid + " AND ipdIsDischarge = 1") == true)
                divrelativward.Visible = false;
            else
                divrelativward.Visible = true;
        }

        if (msg == "1")
        {
            div_print.Style.Add("display", "none");
            div_Addmission.Style.Add("display", "none");
            div_Diagnosis.Style.Add("display", "none");
            div_Remedy.Style.Add("display", "none");
        }
        else
        {
            div_print.Style.Add("display", "block");
            div_Addmission.Style.Add("display", "block");
            div_Diagnosis.Style.Add("display", "block");
            div_Remedy.Style.Add("display", "block");
        }
        //-----------------------------------------------------------------------------------------
        //***********************************************************************************************************************************
        //strSQL = "Select * From tblPatientsWardDetails Where wipdId=" + ipdid;
        //SqlCmd = new SqlCommand(strSQL, con);
        //Sqldr = SqlCmd.ExecuteReader();

        //***********************************************************************************************************************************
        FillTestDetails();
        FillRemedyDetails();
        FillRelativeWardDetails();
        SetInitialRow();
        BindExtraParticular();
        BindIPDRound();
        BindIPDAdvanced();
        BindVoucherDetails();
    }

    public void FillTestDetails()
    {
        strSQL = "sp_getIPDTestDetails";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@itdipdId", ipdid);
        dapt = new SqlDataAdapter(SqlCmd);
        DataTable dt = new DataTable();
        dapt.Fill(dt);
        GRV1.DataSource = dt;
        GRV1.DataBind();

        if (msg == "1")
            GRV1.Columns[3].Visible = false;
        else
            GRV1.Columns[3].Visible = true;

        if (dt.Rows.Count > 0)
        {
            pnlfeat.Visible = true;
            divInv.Visible = false;
            btnPrint.Visible = true;
        }
        else
        {
            pnlfeat.Visible = false;
            divInv.Visible = true;
            btnPrint.Visible = false;
        }
        dt.Dispose();
    }

    public void SetTabMenu()
    {


        li_lnkRound.Attributes["class"] = "qtab-Demo first";

        if (DISPROW == "VD")
        {

            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = false;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML active first";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = true;
        }

        if (DISPROW == "MR")
        {

            lnkIpdRoundDetails.Enabled = false;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML active first";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = true;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = false;
        }


        if (DISPROW == "EP")
        {
            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = false;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML active first";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = true;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = false;
        }

        if (DISPROW == "RD")
        {
            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = false;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML active first";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = true;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = false;
        }

        if (DISPROW == "DG")
        {
            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = false;
            li_lnkDiag.Attributes["class"] = "qtab-HTML active first";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = true;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = false;
        }

        if (DISPROW == "AD")
        {
            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = false;
            li_lnkAdm.Attributes["class"] = "qtab-HTML active first";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = true;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = false;
        }

        if (DISPROW == "ADV")
        {
            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = false;
            li_lnkAdv.Attributes["class"] = "qtab-HTML active first";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = true;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = false;
        }

        if (DISPROW == "WD")
        {
            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = false;
            li_lnkWard.Attributes["class"] = "qtab-HTML active first";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = true;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = false;
        }



        if (DISPROW == "RTB")
        {
            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = false;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML active first";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = false;
        }

        if (DISPROW == "IN")
        {
            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = false;
            li_lnkInv.Attributes["class"] = "qtab-HTML active first";

            lnkDiet.Enabled = true;
            li_lnkDiet.Attributes["class"] = "qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = true;
            Row_Diet.Visible = false;
            Row_Voucher.Visible = false;
        }

        if (DISPROW == "DT")
        {
            lnkIpdRoundDetails.Enabled = true;
            li_IpdRoundDetails.Attributes["class"] = "qtab-HTML";

            lnkExtraP.Enabled = true;
            li_lnkExtraP.Attributes["class"] = "qtab-HTML";

            lnkRemedies.Enabled = true;
            li_lnkRemedies.Attributes["class"] = "qtab-HTML";

            lnkDiag.Enabled = true;
            li_lnkDiag.Attributes["class"] = "qtab-HTML";

            lnkAdm.Enabled = true;
            li_lnkAdm.Attributes["class"] = "qtab-HTML";

            lnkAdv.Enabled = true;
            li_lnkAdv.Attributes["class"] = "qtab-HTML";

            lnkWard.Enabled = true;
            li_lnkWard.Attributes["class"] = "qtab-HTML";

            lnkRelBed.Enabled = true;
            li_lnkRelBed.Attributes["class"] = "qtab-HTML";

            lnkInv.Enabled = true;
            li_lnkInv.Attributes["class"] = "qtab-HTML";

            lnkDiet.Enabled = false;
            li_lnkDiet.Attributes["class"] = "qtab-HTML active first qtab-Demo last";

            lnkVoucher.Enabled = true;
            li_lnkVoucher.Attributes["class"] = "qtab-HTML";

            Row_ManageRound.Visible = false;
            Row_Extra.Visible = false;
            Row_Remedies.Visible = false;
            Row_Diagnosis.Visible = false;
            Row_Adm.Visible = false;
            Row_Advanced.Visible = false;
            Row_Ward.Visible = false;
            Row_Relatives.Visible = false;
            Row_Inv.Visible = false;
            Row_Diet.Visible = true;
            Row_Voucher.Visible = false;
        }

        if (DISPROW == "ALL")
        {
            AksRow.Visible = false;
        }


    }

    protected void GRV2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GRV2.EditIndex = -1;
        FillWardDetails();
    }

    protected void GRV2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRV2.EditIndex = e.NewEditIndex;
        FillWardDetails();
    }

    protected void GRV2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GRV2.Rows[e.RowIndex];
        HiddenField hidwId = (HiddenField)row.FindControl("hdnwId");

        DropDownList ddwType = (DropDownList)row.FindControl("ddwType");
        DropDownList ddWardNo = (DropDownList)row.FindControl("ddWardNo");
        DropDownList ddBedNo = (DropDownList)row.FindControl("ddBedNo");

        //  TextBox txtWardNo = (TextBox)row.FindControl("txtWardNo");
        // TextBox txtBedNo = (TextBox)row.FindControl("txtBedNo");

        TextBox InDate = (TextBox)row.FindControl("txtInDate");
        TextBox OutDate = (TextBox)row.FindControl("txtOutDate");

        object InDateAks = InDate.Text == "" ? null : InDate.Text.Trim();
        object OutDateAks = OutDate.Text == "" ? null : OutDate.Text.Trim();


        //strSQL = "Update tblPatientsWardDetails SET wType=" + ddwType.SelectedValue + ",wNo = '" + txtWardNo.Text + "',wBedNo='" + txtBedNo.Text + "' ,wInDate='" + InDate.Text.Trim() + "',wOutDate='" + OutDate.Text.Trim() + "'";
        //strSQL += " where wId = " + hidwId.Value + "";
        //gen.executeQuery(strSQL);
        //GRV2.EditIndex = -1;

        SqlCmd = new SqlCommand();
        SqlCmd.CommandText = "proc_EditPatientWardDetails";
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@wId", hidwId.Value);
        SqlCmd.Parameters.AddWithValue("@wType", ddwType.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@wNo", ddWardNo.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@wBedNo", ddBedNo.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@wInDate", InDateAks);
        SqlCmd.Parameters.AddWithValue("@wOutDate", OutDateAks);
        SqlCmd.Parameters.AddWithValue("@modifiedBy", uid);
        SqlCmd.Connection = con;
        if (con.State == ConnectionState.Closed)
            con.Open();

        SqlCmd.ExecuteNonQuery();
        con.Close();
        SqlCmd.Dispose();
        GRV2.EditIndex = -1;
        FillWardDetails();
    }

    protected void GRV2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int wId = int.Parse(e.CommandArgument.ToString());

        if (e.CommandName == "delete")
        {
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeletePatientWardDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@wId", wId);
            SqlCmd.Parameters.AddWithValue("@DeletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            FillWardDetails();
	    Response.Redirect("IPDViewAks.aspx?DISPROW=WD", false);

        }
    }

    public void FillWardDetails()
    {
        //strSQL = "Select *,Case wType When 1 Then 'General' When 2 Then 'Semi Special' When 3 Then 'Special' When 4 Then 'Delux'  END _wType From tblPatientsWardDetails Where wipdId=" + ipdid;
        strSQL = "Select * From VW_GetPatientWardDetails Where wipdId=" + ipdid;
        //strSQL += " AND wId = (SELECT MAX(wId) FROM dbo.tblPatientsWardDetails Where wipdId=" + ipdid + ")";

        DataTable _dt = gen.getDataTable(strSQL);
        GRV2.DataSource = _dt;
        GRV2.DataBind();

        if (_dt.Rows.Count > 1)
        {
            GRV2.Columns[7].Visible = true;
        }
        else
        {
            GRV2.Columns[7].Visible = false;
        }
        if (msg == "1")
            GRV2.Columns[6].Visible = false;
        else
            GRV2.Columns[6].Visible = true;
        _dt.Dispose();
    }

    public void FillRelativeWardDetails()
    {
        strSQL = "Select *,Case rwType When 1 Then 'General' When 2 Then 'Semi Special' END _wType From tblRelativeWardDetails Where ipdId=" + ipdid;


        DataTable _dt = gen.getDataTable(strSQL);
        GRVRelative.DataSource = _dt;
        GRVRelative.DataBind();
        if (msg == "1")
            GRVRelative.Columns[5].Visible = false;
        else
            GRVRelative.Columns[5].Visible = true;

        _dt.Dispose();
    }

    public void FillRemedyDetails()
    {
        strSQL = "Select RM.RMNAME , IR.irRemedy , IR.irId , IR.irStartDate  From tblIpdRemedy IR Inner Join tblRemedyMaster RM On IR.irRemedy = RM.RMID Where iripdId=" + ipdid;
        //strSQL += " AND wId = (SELECT MAX(wId) FROM dbo.tblPatientsWardDetails Where wipdId=" + ipdid + ")";
        DataTable _dt = gen.getDataTable(strSQL);
        GRV3.DataSource = _dt;
        GRV3.DataBind();
        if (msg == "1")
            GRV3.Columns[2].Visible = false;
        else
            GRV3.Columns[2].Visible = true;

        if (_dt.Rows.Count >= 1)
        {
            pnlremedy.Visible = true;
            divRemedy.Visible = false;
        }
        else
        {
            pnlremedy.Visible = false;
            divRemedy.Visible = true;
        }
        _dt.Dispose();


    }

    protected void GRV2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && GRV2.EditIndex == e.Row.RowIndex)
        {
            DropDownList ddwType = (DropDownList)e.Row.FindControl("ddwType");
            DropDownList ddWardNo = (DropDownList)e.Row.FindControl("ddWardNo");
            DropDownList ddBedNo = (DropDownList)e.Row.FindControl("ddBedNo");
            TextBox txtBedNo = (TextBox)e.Row.FindControl("txtBedNo");

            ddwType.Items.Clear();
            ddwType.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT Distinct WardTypeID As wmWardTypeId , WardTypeName FROM tblWardTypeMaster Where IsDelete = 0", "wmWardTypeId", "WardTypeName", ddwType);
            ddwType.Items.FindByValue((e.Row.FindControl("hdnwType") as HiddenField).Value).Selected = true;

            ddWardNo.Items.Clear();
            ddWardNo.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + ddwType.SelectedValue + "' And IsDelete = 0 ", "wmID", "wmWardNo", ddWardNo);
            ddWardNo.Items.FindByValue((e.Row.FindControl("hdnWardNo") as HiddenField).Value).Selected = true;

            string BID = (e.Row.FindControl("hdnBedNo") as HiddenField).Value;
            string BNo = txtBedNo.Text;

            ddBedNo.Items.Clear();
            ddBedNo.Items.Insert(0, new ListItem("Select", ""));
            ddBedNo.Items.Insert(1, new ListItem(BNo, BID));

            //gen.FillDropDownList("SELECT bmNoOfBads , bmID FROM tblBadMaster WHERE wmID='" + ddWardNo.SelectedValue + "' And IsDelete = 0 ", "bmID", "bmNoOfBads", ddBedNo);
            strSQL = "sp_GetBed_New";
            SqlCmd = new SqlCommand(strSQL, con);
            SqlDataReader dr;
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@wmID", ddWardNo.SelectedValue);

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
                dr = SqlCmd.ExecuteReader();
                SqlCmd.Dispose();
                DataTable dt = new DataTable();
                dt.Load(dr);
                foreach (DataRow drow in dt.Rows)
                {
                    ddBedNo.Items.Add(new ListItem(drow["bmNoOfBads"].ToString(), drow["bmid"].ToString()));
                }
            }
            con.Close();
            ddBedNo.Items.FindByValue((e.Row.FindControl("hdnBedNo") as HiddenField).Value).Selected = true;

        }
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string itdId = e.CommandArgument.ToString();
        if (e.CommandName == "del")
        {
            //gen.executeQuery("Delete From tblIpdTestDetails Where itdId = " + itdId + "");
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteIPDTestDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@itdId", itdId);
            SqlCmd.Parameters.AddWithValue("@deletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            FillTestDetails();
        }
    }

    protected void GRV3_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GRV3.EditIndex = -1;
        FillRemedyDetails();
    }

    protected void GRV3_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRV3.EditIndex = e.NewEditIndex;
        FillRemedyDetails();
    }

    protected void GRV3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GRV3.Rows[e.RowIndex];
        HiddenField hdnirId = (HiddenField)row.FindControl("hdnirId");
        DropDownList DDL_RemList = (DropDownList)row.FindControl("DDL_RemList");

        TextBox StartDate = (TextBox)row.FindControl("txtStartDate");

        strSQL = "Update tblIpdRemedy SET irRemedy = '" + DDL_RemList.SelectedValue + "',irStartDate='" + StartDate.Text.Trim() + "'  Where irId = " + hdnirId.Value + "";
        gen.executeQuery(strSQL);
        GRV3.EditIndex = -1;
        FillRemedyDetails();
    }

    protected void GRV3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && GRV3.EditIndex == e.Row.RowIndex)
        {
            DropDownList DDL_RemList = (DropDownList)e.Row.FindControl("DDL_RemList");
            Label lblirRemID = (Label)e.Row.FindControl("lblirRemID");

            DDL_RemList.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("Select * From tblRemedyMaster where isDelete = 0 AND RMNAME != '0' ORDER BY REPLACE(RMNAME,' ','') ASC ", "RMID", "RMNAME", DDL_RemList);
            DDL_RemList.Items.FindByValue((e.Row.FindControl("lblirRemID") as Label).Text).Selected = true;
        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        string ptdId = "";
        string newptdId = "";
        string fptdId = "";

        foreach (GridViewRow grvrow in GRV1.Rows)
        {
            HtmlInputCheckBox chk = (HtmlInputCheckBox)grvrow.FindControl("cbAC");
            if (chk.Checked == true)
            {
                ptdId = chk.Value;
                newptdId += ',' + ptdId;
            }

        }
        if (newptdId != "")
        {
            fptdId = newptdId.Substring(1);
            Response.Redirect("IPDBillFormat.aspx?Flag=Test&id=" + fptdId + "");
        }
    }
    protected void GRVRelative_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GRVRelative.EditIndex = -1;
        FillRelativeWardDetails();
    }

    protected void GRVRelative_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && GRVRelative.EditIndex == e.Row.RowIndex)
        {
            DropDownList ddrwType = (DropDownList)e.Row.FindControl("ddrwType");
            HiddenField hdnrwType = (HiddenField)e.Row.FindControl("hdnrwType");

            ddrwType.SelectedValue = hdnrwType.Value;
        }

    }

    protected void GRVRelative_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRVRelative.EditIndex = e.NewEditIndex;
        FillRelativeWardDetails();

    }

    protected void GRVRelative_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //GridViewRow row = GRVRelative.Rows[e.RowIndex];
        //HiddenField hidrwId = (HiddenField)row.Cells[0].FindControl("hdnrwId");
        //DropDownList ddrwType = (DropDownList)row.Cells[0].FindControl("ddrwType");
        //TextBox txtrWardNo = (TextBox)row.Cells[1].FindControl("txtrWardNo");
        //TextBox txtrBedNo = (TextBox)row.Cells[2].FindControl("txtrBedNo");

        //strSQL = "Update tblRelativeWardDetails SET rwType=" + ddrwType.SelectedValue + ",rwNo = '" + txtrWardNo.Text + "',rwBedNo='" + txtrBedNo.Text + "'";
        //strSQL += " where rwId = " + hidrwId.Value + "";
        //gen.executeQuery(strSQL);
        //GRVRelative.EditIndex = -1;
        //FillRelativeWardDetails();
    }

    protected void GRVRelative_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            GRVRelative.EditIndex = -1;
        }

        if (e.CommandName == "del")
        {
            int rwid = Convert.ToInt32(e.CommandArgument);
            //gen.executeQuery("Delete From tblRelativeWardDetails where rwId=" + rwid + "");
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteRelativeWardDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@rwid", rwid);
            SqlCmd.Parameters.AddWithValue("@deletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            FillRelativeWardDetails();
        }

        if (e.CommandName == "update")
        {

            foreach (GridViewRow row in GRVRelative.Rows)
            {

                GRVRelative.EditIndex = -1;
                HiddenField hidrwId = (HiddenField)row.Cells[0].FindControl("hdnrwId");
                if (hidrwId != null)
                {
                    DropDownList ddrwType = (DropDownList)row.Cells[0].FindControl("ddrwType");
                    TextBox txtrWardNo = (TextBox)row.Cells[1].FindControl("txtrWardNo");
                    TextBox txtrBedNo = (TextBox)row.Cells[2].FindControl("txtrBedNo");
                    TextBox doi = (TextBox)row.Cells[3].FindControl("txtInDate");
                    TextBox doO = (TextBox)row.Cells[4].FindControl("txtOutDate");


                    strSQL = "Update tblRelativeWardDetails SET rwType=" + ddrwType.SelectedValue + ",rwNo = '" + txtrWardNo.Text + "',rwBedNo='" + txtrBedNo.Text + "', rwInDate='" + doi.Text.Trim() + "',rwOutDate='" + doO.Text.Trim() + "'";
                    strSQL += " where rwId = " + hidrwId.Value + "";
                    gen.executeQuery(strSQL);
                    GRVRelative.EditIndex = -1;
                    FillRelativeWardDetails();
                }
            }
        }

    }
    protected void btnPrintIPD_Click(object sender, EventArgs e)
    {
        Response.Redirect("IPDPaper.aspx", false);
    }

    protected void ddwType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddwType = (DropDownList)sender;
        GridViewRow gvr = (GridViewRow)ddwType.NamingContainer;
        DropDownList ddWardNo = (DropDownList)gvr.FindControl("ddWardNo");

        ddWardNo.Items.Clear();
        ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + ddwType.SelectedValue + "' And IsDelete = 0 ", "wmID", "wmWardNo", ddWardNo);
    }

    protected void ddWardNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddWardNo = (DropDownList)sender;
        GridViewRow gvr = (GridViewRow)ddWardNo.NamingContainer;
        DropDownList ddBedNo = (DropDownList)gvr.FindControl("ddBedNo");

        //ddBedNo.Items.Clear();
        //ddBedNo.Items.Insert(0, new ListItem("Select", ""));
        //gen.FillDropDownList("SELECT bmNoOfBads , bmID FROM tblBadMaster WHERE wmID='" + ddWardNo.SelectedValue + "' And IsDelete = 0 ", "bmID", "bmNoOfBads", ddBedNo);

        ddBedNo.Items.Clear();
        ddBedNo.Items.Insert(0, new ListItem("Select", ""));
        strSQL = "sp_GetBed_New";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@wmID", ddWardNo.SelectedValue);

        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            dr = SqlCmd.ExecuteReader();
            SqlCmd.Dispose();
            DataTable dt = new DataTable();
            dt.Load(dr);
            foreach (DataRow drow in dt.Rows)
            {
                ddBedNo.Items.Add(new ListItem(drow["bmNoOfBads"].ToString(), drow["bmid"].ToString()));
            }
        }
        con.Close();

    }

    private void SetInitialRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;

        //Define the Columns
        dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dt.Columns.Add(new DataColumn("ExtParticular", typeof(string)));
        dt.Columns.Add(new DataColumn("Charge", typeof(string)));
        dt.Columns.Add(new DataColumn("Quantity", typeof(string)));
        //dt.Columns.IsReadOnly = false;
        dt.Columns.Add(new DataColumn("GivenDate", typeof(string)));
        dt.Columns.Add(new DataColumn("ReturnDate", typeof(string)));
        dt.Columns.Add(new DataColumn("Days", typeof(string)));
        dt.Columns.Add(new DataColumn("TotalCharge", typeof(string)));

        //Add a Dummy Data on Initial Load
        dr = dt.NewRow();
        //dr["RowNumber"] = 1;
        dr["ExtParticular"] = "";
        dr["Charge"] = "0";
        dr["Quantity"] = "0";
        dr["GivenDate"] = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        dr["ReturnDate"] = ""; // string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        dr["Days"] = "0";
        dr["TotalCharge"] = "0";

        dt.Rows.Add(dr);

        ViewState["CurrentTable"] = dt;
        GVD_EP.DataSource = dt;
        GVD_EP.DataBind();
    }

    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["CurrentTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DropDownList DDL_ExtP = (DropDownList)GVD_EP.Rows[rowIndex].Cells[1].FindControl("DDL_ExtP");
                    Label lbl_ExtP = (Label)GVD_EP.Rows[rowIndex].Cells[1].FindControl("lbl_ExtP");
                    TextBox txt_Charge = (TextBox)GVD_EP.Rows[rowIndex].Cells[2].FindControl("txt_Charge");
                    TextBox txt_Quantity = (TextBox)GVD_EP.Rows[rowIndex].Cells[3].FindControl("txt_Quantity");
                    TextBox txt_GivenDate = (TextBox)GVD_EP.Rows[rowIndex].Cells[4].FindControl("txt_GivenDate");
                    TextBox txt_ReturnDate = (TextBox)GVD_EP.Rows[rowIndex].Cells[5].FindControl("txt_ReturnDate");
                    TextBox txt_Days = (TextBox)GVD_EP.Rows[rowIndex].Cells[6].FindControl("txt_Days");
                    TextBox txt_TotalCharge = (TextBox)GVD_EP.Rows[rowIndex].Cells[7].FindControl("txt_TotalCharge");

                    DDL_ExtP.SelectedValue = dt.Rows[i]["ExtParticular"].ToString();
                    lbl_ExtP.Text = dt.Rows[i]["ExtParticular"].ToString();
                    txt_Charge.Text = dt.Rows[i]["Charge"].ToString() == "" ? "0" : dt.Rows[i]["Charge"].ToString();
                    txt_Quantity.Text = dt.Rows[i]["Quantity"].ToString() == "" ? "0" : dt.Rows[i]["Quantity"].ToString();
                    txt_GivenDate.Text = dt.Rows[i]["GivenDate"].ToString() == "" ? string.Format("{0:dd/MM/yyyy}", DateTime.Now) : dt.Rows[i]["GivenDate"].ToString();
                    txt_ReturnDate.Text = dt.Rows[i]["ReturnDate"].ToString() == "" ? "" : dt.Rows[i]["ReturnDate"].ToString();
                    txt_Days.Text = dt.Rows[i]["Days"].ToString() == "" ? "0" : dt.Rows[i]["Days"].ToString();
                    txt_TotalCharge.Text = dt.Rows[i]["TotalCharge"].ToString() == "" ? "0" : dt.Rows[i]["TotalCharge"].ToString();

                    rowIndex++;
                }
            }
        }
    }

    private void AddNewRowToGrid()
    {
        int rowIndex = 0;

        if (ViewState["CurrentTable"] != null)
        {
            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

            foreach (DataColumn col in dtCurrentTable.Columns)
                col.ReadOnly = false;

            DataRow drCurrentRow = null;

            if (dtCurrentTable.Rows.Count > 0)
            {

                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                {

                    DropDownList DDL_ExtP = (DropDownList)GVD_EP.Rows[rowIndex].Cells[1].FindControl("DDL_ExtP");
                    Label lbl_ExtP = (Label)GVD_EP.Rows[rowIndex].Cells[1].FindControl("lbl_ExtP");

                    TextBox txt_Charge = (TextBox)GVD_EP.Rows[rowIndex].Cells[2].FindControl("txt_Charge");
                    TextBox txt_Quantity = (TextBox)GVD_EP.Rows[rowIndex].Cells[3].FindControl("txt_Quantity");

                    TextBox txt_GivenDate = (TextBox)GVD_EP.Rows[rowIndex].Cells[4].FindControl("txt_GivenDate");
                    TextBox txt_ReturnDate = (TextBox)GVD_EP.Rows[rowIndex].Cells[5].FindControl("txt_ReturnDate");

                    TextBox txt_Days = (TextBox)GVD_EP.Rows[rowIndex].Cells[6].FindControl("txt_Days");
                    TextBox txt_TotalCharge = (TextBox)GVD_EP.Rows[rowIndex].Cells[7].FindControl("txt_TotalCharge");


                    drCurrentRow = dtCurrentTable.NewRow();
                    //drCurrentRow["RowNumber"] = i + 1;


                    dtCurrentTable.Rows[i - 1]["ExtParticular"] = lbl_ExtP.Text; // DDL_ExtP.SelectedValue;
                    dtCurrentTable.Rows[i - 1]["Charge"] = txt_Charge.Text;
                    dtCurrentTable.Rows[i - 1]["Quantity"] = txt_Quantity.Text;

                    dtCurrentTable.Rows[i - 1]["GivenDate"] = txt_GivenDate.Text;

                    if (txt_ReturnDate.Text != "")
                        dtCurrentTable.Rows[i - 1]["ReturnDate"] = txt_ReturnDate.Text;

                    dtCurrentTable.Rows[i - 1]["Days"] = txt_Days.Text;
                    dtCurrentTable.Rows[i - 1]["TotalCharge"] = txt_TotalCharge.Text;

                    rowIndex++;
                }

                dtCurrentTable.Rows.Add(drCurrentRow);
                ViewState["CurrentTable"] = dtCurrentTable;

                GVD_EP.DataSource = dtCurrentTable;
                GVD_EP.DataBind();

            }
        }
        else
        {
            Response.Write("ViewState is null");
        }


        SetPreviousData();
    }

    private void AddNewRowToGridNew()
    {
        int rowIndex = 0;

        if (ViewState["CurrentTable"] != null)
        {
            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

            foreach (DataColumn col in dtCurrentTable.Columns)
                col.ReadOnly = false;

            DataRow drCurrentRow = null;

            if (dtCurrentTable.Rows.Count > 0)
            {

                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                {

                    DropDownList DDL_ExtP = (DropDownList)GVD_EP.Rows[rowIndex].Cells[1].FindControl("DDL_ExtP");
                    Label lbl_ExtP = (Label)GVD_EP.Rows[rowIndex].Cells[1].FindControl("lbl_ExtP");

                    TextBox txt_Charge = (TextBox)GVD_EP.Rows[rowIndex].Cells[2].FindControl("txt_Charge");
                    TextBox txt_Quantity = (TextBox)GVD_EP.Rows[rowIndex].Cells[3].FindControl("txt_Quantity");

                    TextBox txt_GivenDate = (TextBox)GVD_EP.Rows[rowIndex].Cells[4].FindControl("txt_GivenDate");
                    TextBox txt_ReturnDate = (TextBox)GVD_EP.Rows[rowIndex].Cells[5].FindControl("txt_ReturnDate");

                    TextBox txt_Days = (TextBox)GVD_EP.Rows[rowIndex].Cells[6].FindControl("txt_Days");
                    TextBox txt_TotalCharge = (TextBox)GVD_EP.Rows[rowIndex].Cells[7].FindControl("txt_TotalCharge");


                    drCurrentRow = dtCurrentTable.NewRow();
                    //drCurrentRow["RowNumber"] = i + 1;


                    dtCurrentTable.Rows[i - 1]["ExtParticular"] = lbl_ExtP.Text; // DDL_ExtP.SelectedValue;
                    dtCurrentTable.Rows[i - 1]["Charge"] = txt_Charge.Text;
                    dtCurrentTable.Rows[i - 1]["Quantity"] = txt_Quantity.Text;

                    dtCurrentTable.Rows[i - 1]["GivenDate"] = txt_GivenDate.Text;

                    if (txt_ReturnDate.Text != "")
                        dtCurrentTable.Rows[i - 1]["ReturnDate"] = txt_ReturnDate.Text;

                    dtCurrentTable.Rows[i - 1]["Days"] = txt_Days.Text;
                    dtCurrentTable.Rows[i - 1]["TotalCharge"] = txt_TotalCharge.Text;

                    rowIndex++;
                }

                ViewState["CurrentTable"] = dtCurrentTable;

                GVD_EP.DataSource = dtCurrentTable;
                GVD_EP.DataBind();

            }
        }
        else
        {
            Response.Write("ViewState is null");
        }

        SetPreviousData();
    }


    protected void lnk_emp_rowAdd_Click(object sender, EventArgs e)
    {
        AddNewRowToGrid();
    }

    protected void GVD_EP_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string returnDate = "";
            DropDownList DDL_ExtP = (DropDownList)e.Row.FindControl("DDL_ExtP");
            string lbl_ExtP = (e.Row.FindControl("lbl_ExtP") as Label).Text;

            DDL_ExtP.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("Select EP_ID , EP_NAME  from tblExtraParticularMaster Where EP_IsDelete = 0", "EP_ID", "EP_NAME", DDL_ExtP);
            DDL_ExtP.Items.FindByValue(lbl_ExtP).Selected = true;

            TextBox txt_ReturnDate = (TextBox)e.Row.FindControl("txt_ReturnDate");
            TextBox txt_Charge = (TextBox)e.Row.FindControl("txt_Charge");
            TextBox txt_Quantity = (TextBox)e.Row.FindControl("txt_Quantity");
            TextBox txt_GivenDate = (TextBox)e.Row.FindControl("txt_GivenDate");
            TextBox txt_Days = (TextBox)e.Row.FindControl("txt_Days");
            TextBox txt_TotalCharge = (TextBox)e.Row.FindControl("txt_TotalCharge");

            if (txt_ReturnDate.Text == "" && txt_Quantity.Text != "" && txt_Charge.Text != "")
            {
               
                returnDate = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                IFormatProvider theCultureInfo = new System.Globalization.CultureInfo("en-GB", true);
                DateTime GivenDT = DateTime.ParseExact(txt_GivenDate.Text, "dd/MM/yyyy", theCultureInfo);
                DateTime ReturnDT = DateTime.ParseExact(returnDate, "dd/MM/yyyy", theCultureInfo);
                double TotalCharge = ((ReturnDT - GivenDT).TotalDays);
                txt_Days.Text = Convert.ToString(TotalCharge + 1);
                txt_TotalCharge.Text = Convert.ToString((Convert.ToInt32(txt_Charge.Text) * Convert.ToInt32(txt_Quantity.Text)) * (TotalCharge + 1));

            }

          
        }
    }

    protected void GVD_EP_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Cancel")
        {
            try
            {

                int index = Convert.ToInt16(e.CommandArgument);
                DataTable dtCurrentTableMY = (DataTable)ViewState["CurrentTable"];
                dtCurrentTableMY.Rows[index].Delete();
                //dtCurrentTableMY.AcceptChanges();
                ViewState["CurrentTable"] = dtCurrentTableMY;
                GVD_EP.DataSource = null;
                GVD_EP.DataSource = dtCurrentTableMY;
                GVD_EP.DataBind();
            }
            catch
            { }


        }
    }

    protected void DDL_ExtP_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList DDL_ExtP = (DropDownList)sender;
        GridViewRow gvr = (GridViewRow)DDL_ExtP.NamingContainer;
        TextBox txt_Charge = (TextBox)gvr.FindControl("txt_Charge");
        TextBox txt_Quantity = (TextBox)gvr.FindControl("txt_Quantity");
        Label lbl_ExtP = (Label)gvr.FindControl("lbl_ExtP");

        strSQL = "Exec  Proc_GetExtraParticularDetails " + DDL_ExtP.SelectedValue + " , Details";
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            txt_Charge.Text = Sqldr["Charge"].ToString();
            txt_Quantity.Text = "1";
            lbl_ExtP.Text = DDL_ExtP.SelectedValue;
        }
        Sqldr.Close();
        SqlCmd.Dispose();

        AddNewRowToGridNew();

    }

    protected void GVD_EP_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

    }

    private string xmlString()
    {
        string xmlstring = "<ROOT>";

        foreach (GridViewRow grvrow in GVD_EP.Rows)
        {
            DropDownList DDL_ExtP = (DropDownList)grvrow.FindControl("DDL_ExtP");
            TextBox txt_Charge = (TextBox)grvrow.FindControl("txt_Charge");
            TextBox txt_Quantity = (TextBox)grvrow.FindControl("txt_Quantity");

            TextBox txt_GivenDate = (TextBox)grvrow.FindControl("txt_GivenDate");
            TextBox txt_ReturnDate = (TextBox)grvrow.FindControl("txt_ReturnDate");

            object from_date = txt_GivenDate.Text == "" ? null : gen.getdateAks(txt_GivenDate.Text.Trim());
            object to_date = txt_ReturnDate.Text == "" ? null : gen.getdateAks(txt_ReturnDate.Text.Trim());

            TextBox txt_TotalCharge = (TextBox)grvrow.FindControl("txt_TotalCharge");

            if (DDL_ExtP.SelectedValue.Trim() != "" && txt_Charge.Text.Trim() != "" && txt_Quantity.Text.Trim() != "" && txt_GivenDate.Text.Trim() != "")
                xmlstring = xmlstring + "<OCS Pat_EPID='" + DDL_ExtP.SelectedValue.Trim() + "' Pat_EPCharge='" + txt_Charge.Text + "' Pat_EPQuantity='" + txt_Quantity.Text + "' Pat_EPGivenDate='" + from_date + "' Pat_EPReturnDate='" + to_date + "' Pat_EPTotalCharge='" + txt_TotalCharge.Text + "' />";
        }
        xmlstring = xmlstring + "</ROOT>";
        return xmlstring;
    }

    protected void btn_SaveExtraP_Click(object sender, EventArgs e)
    {

        try
        {
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_InsertExtraParticularDetail";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@Pat_IPDID", ipdid);
            SqlCmd.Parameters.AddWithValue("@Pat_EPCreatedBy", uid);
            SqlCmd.Parameters.AddWithValue("@xmlRepresentString", xmlString());
            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            int reslt;
            reslt = (int)SqlCmd.Parameters["@Error"].Value;
            con.Close();
            if (reslt == 0)
            {

            }
            else
            {


            }
        }
        catch
        {

        }


    }

    private void BindExtraParticular()
    {

        strSQL = "GetExtraParticularPatientWise";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Pat_EPID", ipdid);
        SqlCmd.Parameters.AddWithValue("@Flag", "Details");

        if (con.State == ConnectionState.Closed)
            con.Open();


        dr = SqlCmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        if (dt.Rows.Count > 0)
        {
            GVD_EP.DataSource = dt;
            GVD_EP.DataBind();
            ViewState["CurrentTable"] = dt;
        }
        con.Close();
        dr.Dispose();

    }

    private void BindIPDRound()
    {

        strSQL = "GetRoundDetailsPatientWise";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Pat_IPDID_ID", ipdid);
        SqlCmd.Parameters.AddWithValue("@Flag", "View");

        if (con.State == ConnectionState.Closed)
            con.Open();


        dr = SqlCmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        if (dt.Rows.Count > 0)
        {
            GVD_ManageRound.DataSource = dt;
            GVD_ManageRound.DataBind();

        }
        con.Close();
        dr.Dispose();

    }

    protected void txt_Quantity_TextChanged(object sender, EventArgs e)
    {
        TextBox txt_Quantity = (TextBox)sender;
        GridViewRow gvr = (GridViewRow)txt_Quantity.NamingContainer;
        TextBox txt_Charge = (TextBox)gvr.FindControl("txt_Charge");
        TextBox txt_TotalCharge = (TextBox)gvr.FindControl("txt_TotalCharge");
        TextBox txt_GivenDate = (TextBox)gvr.FindControl("txt_GivenDate");
        TextBox txt_ReturnDate = (TextBox)gvr.FindControl("txt_ReturnDate");
        TextBox txt_Days = (TextBox)gvr.FindControl("txt_Days");

        if (txt_ReturnDate.Text != "" && txt_Quantity.Text != "" && txt_Charge.Text != "")
        {
            //GivenDT = DateTime.Parse(txt_GivenDate.Text).Date;
            //ReturnDT = DateTime.Parse(txt_ReturnDate.Text).Date;
            //DayCount = ReturnDT - GivenDT;
            //TotalDays = DayCount.Days;
            //txt_TotalCharge.Text = Convert.ToString((Convert.ToInt32(txt_Charge.Text) * Convert.ToInt32(txt_Quantity.Text)) * TotalDays);

            //DateTime FromYear = Convert.ToDateTime(txt_GivenDate.Text).Date;
            //DateTime ToYear = Convert.ToDateTime(txt_ReturnDate.Text).Date;
            //TimeSpan objTimeSpan = ToYear - FromYear;
            //double Days = Convert.ToDouble(objTimeSpan.TotalDays);
            //txt_TotalCharge.Text = Convert.ToString((Convert.ToInt32(txt_Charge.Text) * Convert.ToInt32(txt_Quantity.Text)) * Days);

            IFormatProvider theCultureInfo = new System.Globalization.CultureInfo("en-GB", true);
            DateTime GivenDT = DateTime.ParseExact(txt_GivenDate.Text, "dd/MM/yyyy", theCultureInfo);
            DateTime ReturnDT = DateTime.ParseExact(txt_ReturnDate.Text, "dd/MM/yyyy", theCultureInfo);
            double TotalCharge = ((ReturnDT - GivenDT).TotalDays);
            txt_Days.Text = Convert.ToString(TotalCharge + 1);
            txt_TotalCharge.Text = Convert.ToString((Convert.ToInt32(txt_Charge.Text) * Convert.ToInt32(txt_Quantity.Text)) * (TotalCharge + 1));

        }
    }

    protected void txt_ReturnDate_TextChanged(object sender, EventArgs e)
    {
        string returnDate = "";
        TextBox txt_ReturnDate = (TextBox)sender;
        GridViewRow gvr = (GridViewRow)txt_ReturnDate.NamingContainer;
        TextBox txt_Charge = (TextBox)gvr.FindControl("txt_Charge");
        TextBox txt_TotalCharge = (TextBox)gvr.FindControl("txt_TotalCharge");
        TextBox txt_GivenDate = (TextBox)gvr.FindControl("txt_GivenDate");
        TextBox txt_Quantity = (TextBox)gvr.FindControl("txt_Quantity");
        TextBox txt_Days = (TextBox)gvr.FindControl("txt_Days");

        //DateTime GivenDT;
        //DateTime ReturnDT;
        //TimeSpan DayCount;
        //int TotalDays = 0;
        //int TotalCharge = 0;

        if (txt_ReturnDate.Text != "" && txt_Quantity.Text != "" && txt_Charge.Text != "")
        {
            //GivenDT = DateTime.Parse(txt_GivenDate.Text).Date;
            //ReturnDT = DateTime.Parse(txt_ReturnDate.Text).Date;
            //DayCount = ReturnDT - GivenDT;
            //TotalDays = DayCount.Days;
            //txt_TotalCharge.Text = Convert.ToString((Convert.ToInt32(txt_Charge.Text) * Convert.ToInt32(txt_Quantity.Text)) * TotalDays);

            //DateTime FromYear = Convert.ToDateTime(txt_GivenDate.Text).Date;
            //DateTime ToYear = Convert.ToDateTime(txt_ReturnDate.Text).Date;
            //TimeSpan objTimeSpan = ToYear - FromYear;
            //double Days = Convert.ToDouble(objTimeSpan.TotalDays);
            //txt_TotalCharge.Text = Convert.ToString((Convert.ToInt32(txt_Charge.Text) * Convert.ToInt32(txt_Quantity.Text)) * Days);

            IFormatProvider theCultureInfo = new System.Globalization.CultureInfo("en-GB", true);
            DateTime GivenDT = DateTime.ParseExact(txt_GivenDate.Text, "dd/MM/yyyy", theCultureInfo);
            DateTime ReturnDT = DateTime.ParseExact(txt_ReturnDate.Text, "dd/MM/yyyy", theCultureInfo);
            double TotalCharge = ((ReturnDT - GivenDT).TotalDays);
            txt_Days.Text = Convert.ToString(TotalCharge + 1);
            txt_TotalCharge.Text = Convert.ToString((Convert.ToInt32(txt_Charge.Text) * Convert.ToInt32(txt_Quantity.Text)) * (TotalCharge + 1));

        }
        if (txt_ReturnDate.Text == "" && txt_Quantity.Text != "" && txt_Charge.Text != "")
        {
            returnDate = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            IFormatProvider theCultureInfo = new System.Globalization.CultureInfo("en-GB", true);
            DateTime GivenDT = DateTime.ParseExact(txt_GivenDate.Text, "dd/MM/yyyy", theCultureInfo);
            DateTime ReturnDT = DateTime.ParseExact(returnDate, "dd/MM/yyyy", theCultureInfo);
            double TotalCharge = ((ReturnDT - GivenDT).TotalDays);
            txt_Days.Text = Convert.ToString(TotalCharge + 1);
            txt_TotalCharge.Text = Convert.ToString((Convert.ToInt32(txt_Charge.Text) * Convert.ToInt32(txt_Quantity.Text)) * (TotalCharge + 1));
        }
    }

    protected void GVD_ManageRound_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "edit")
        {
            int InvId = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("AddIPDRound.aspx?Id=" + InvId);
        }
        if (e.CommandName == "del")
        {
            int InvId = Convert.ToInt32(e.CommandArgument);
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteIPDRoundDetail";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@InvestID", InvId);
            SqlCmd.Parameters.AddWithValue("@deletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            BindIPDRound();
        }

    }

    protected void btnAdvSave_Click(object sender, EventArgs e)
    {
        string finyear = gen.GetCurrentFinYear();
        int AksStat = 0;

        string doa = "";
        if (txtDOA.Text != "")
            doa = gen.getdate(txtDOA.Text.ToString(), '/');

        string Flag = "";
        if (lblDepID.Text == "0")
        {
            Flag = "Save";
        }
        else
        {
            Flag = "Update";
        }

        strSQL = "ManageAdvancedDetailsAks";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@DepID", lblDepID.Text);
        SqlCmd.Parameters.AddWithValue("@DOA", doa);
        SqlCmd.Parameters.AddWithValue("@DepositAmt", txtDepositAmt.Text);
        SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
        SqlCmd.Parameters.AddWithValue("@CreatedBy", uid);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@finyear", finyear);
        SqlCmd.Parameters.AddWithValue("@Flag", Flag);

        if (con.State == ConnectionState.Closed)
            con.Open();
        AksStat = SqlCmd.ExecuteNonQuery();
        con.Close();
        SqlCmd.Dispose();

        if (AksStat > 0)
        {
            //lblDepID.Text = "0";
            //DDL_Pay_Mode.SelectedValue = "";
            //txtDepositAmt.Text = "";
            //txtDOA.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            //BindIPDAdvanced();
            Response.Redirect("IPDViewAks.aspx?DISPROW=ADV", false);
        }

    }

    private void BindIPDAdvanced()
    {
        string finyear = gen.GetCurrentFinYear();
        strSQL = "ManageAdvancedDetailsAks";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@DepID", lblDepID.Text);
        SqlCmd.Parameters.AddWithValue("@DOA", "");
        SqlCmd.Parameters.AddWithValue("@DepositAmt", 0);
        SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
        SqlCmd.Parameters.AddWithValue("@CreatedBy", 0);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", "");
        SqlCmd.Parameters.AddWithValue("@finyear", finyear);
        SqlCmd.Parameters.AddWithValue("@Flag", "GetDep");

        if (con.State == ConnectionState.Closed)
            con.Open();


        dr = SqlCmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        if (dt.Rows.Count > 0)
        {
            GRD_Adv.DataSource = dt;
            GRD_Adv.DataBind();
        }
        con.Close();
        dr.Dispose();

    }

    protected void GRD_Adv_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRD_Adv.EditIndex = e.NewEditIndex;
    }

    protected void GRD_Adv_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "edit")
        {
            int Depid = Convert.ToInt32(e.CommandArgument);
            lblDepID.Text = Convert.ToString(Depid);
            string finyear = gen.GetCurrentFinYear();

            string doa = "";
            if (txtDOA.Text != "")
                doa = gen.getdate(txtDOA.Text.ToString(), '/');

            strSQL = "ManageAdvancedDetailsAks";
            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@DepID", lblDepID.Text);
            SqlCmd.Parameters.AddWithValue("@DOA", doa);
            SqlCmd.Parameters.AddWithValue("@DepositAmt", 0);
            SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
            SqlCmd.Parameters.AddWithValue("@CreatedBy", uid);
            SqlCmd.Parameters.AddWithValue("@Payment_Mode", "");
            SqlCmd.Parameters.AddWithValue("@finyear", finyear);
            SqlCmd.Parameters.AddWithValue("@Flag", "GetDep");

            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd.ExecuteNonQuery();
            Sqldr = SqlCmd.ExecuteReader();
            while (Sqldr.Read())
            {
                txtDepositAmt.Text = Sqldr["DepAmt"].ToString();

                if (Sqldr["DepDate"].ToString() != "NULL" || Sqldr["DepDate"].ToString() != "")
                {
                    txtDOA.Text = Sqldr["DepDate"] != DBNull.Value ? string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["DepDate"].ToString())) : "";
                }
                if (Sqldr["Payment_Mode"] != DBNull.Value || Sqldr["Payment_Mode"].ToString() != "")
                {
                    DDL_Pay_Mode.SelectedValue = Sqldr["Payment_Mode"].ToString();
                }
            }

            con.Close();
            SqlCmd.Dispose();
            con.Dispose();



        }
        if (e.CommandName == "del")
        {
            int AksStat = 0;
            int Depid = Convert.ToInt32(e.CommandArgument);
            lblDepID.Text = Convert.ToString(Depid);

            string doa = "";
            if (txtDOA.Text != "")
                doa = gen.getdate(txtDOA.Text.ToString(), '/');

            strSQL = "ManageAdvancedDetailsAks";
            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@DepID", lblDepID.Text);
            SqlCmd.Parameters.AddWithValue("@DOA", doa);
            SqlCmd.Parameters.AddWithValue("@DepositAmt", 0);
            SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
            SqlCmd.Parameters.AddWithValue("@CreatedBy", uid);
            SqlCmd.Parameters.AddWithValue("@Payment_Mode", "");
            SqlCmd.Parameters.AddWithValue("@finyear", "");
            SqlCmd.Parameters.AddWithValue("@Flag", "Delete");

            if (con.State == ConnectionState.Closed)
                con.Open();
            AksStat = SqlCmd.ExecuteNonQuery();
            con.Close();
            SqlCmd.Dispose();

            if (AksStat > 0)
            {
                //lblDepID.Text = "0";
                //DDL_Pay_Mode.SelectedValue = "";
                //txtDepositAmt.Text = "";
                //txtDOA.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                //BindIPDAdvanced();
                Response.Redirect("IPDViewAks.aspx?DISPROW=ADV", false);
            }

        }

    }

    protected void btn_Diet_Click(object sender, EventArgs e)
    {
        string[] _frm;
        string[] _to;

        if (txt_DietDate.Text != "")
        {
            if (txt_DietDate.Text.Contains("/"))
            {
                _frm = txt_DietDate.Text.ToString().Split('/');
                frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            }

            if (txt_DietDate.Text.Contains("-"))
            {
                _frm = txt_DietDate.Text.ToString().Split('-');
                frm = _frm[2] + "-" + _frm[1] + "-" + _frm[0] + " 00:00:00";
            }
        }


        if (txt_DietGivenDate.Text != "")
        {
            if (txt_DietGivenDate.Text.Contains("/"))
            {

                _to = txt_DietGivenDate.Text.ToString().Split('/');
                to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";
            }

            if (txt_DietDate.Text.Contains("-"))
            {

                _to = txt_DietGivenDate.Text.ToString().Split('-');
                to = _to[2] + "-" + _to[1] + "-" + _to[0] + " 23:55:00";
            }
        }




        strSQL = "Proc_InsertDietDetails";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
        SqlCmd.Parameters.AddWithValue("@Diet", DDL_Diet.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@DietDate", ToDBNull(frm));
        SqlCmd.Parameters.AddWithValue("@DietGiven", DDL_DietGiven.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@DietGivenDate", ToDBNull(to));
        SqlCmd.Parameters.AddWithValue("@DietCharge", txt_DietCharge.Text);
        SqlCmd.Parameters.AddWithValue("@DoneBy", uid);

        if (con.State == ConnectionState.Closed)
            con.Open();
        int i = SqlCmd.ExecuteNonQuery();
        con.Close();
        SqlCmd.Dispose();
        con.Dispose();

        if (i > 0)
        {
            lbl_Result.Text = "Diet Details Saved Successfully";
            lbl_Result.ForeColor = System.Drawing.Color.Green;
        }
        else
        {
            lbl_Result.Text = "Diet Details Not Saved....!!!!";
            lbl_Result.ForeColor = System.Drawing.Color.Red;
        }
    }

    public static object ToDBNull(object value)
    {
        if (null != value)
            return value;
        return DBNull.Value;
    }

    protected void btn_PrintDietForm_Click(object sender, EventArgs e)
    {
        CrystalReportViewer2.Visible = true;
        string AksConn = ConfigurationManager.AppSettings["constring"];
        bindReport(AksConn, ipdid);
    }

    public void bindReport(string AksConn, int ipdid)
    {
        DataTable dt = new DataTable();
        ReportDocument cryRpt = new ReportDocument();
        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = default(CrystalDecisions.CrystalReports.Engine.Tables);
        System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

        builder.ConnectionString = AksConn;

        string AksServer = builder["Server"] as string;
        string AksDatabase = builder["Database"] as string;
        string AksUsername = builder["Uid"] as string;
        string AksPassword = builder["Pwd"] as string;


        dt = GetReport(ipdid);

        cryRpt.Load(Server.MapPath("~/Reports/DiaticianForm.rpt"));

        CrTables = cryRpt.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        cryRpt.SetDataSource(dt);
        cryRpt.Refresh();

        string KCO = "";
        if (File.Exists(Server.MapPath("Files/KCO/kco_" + PdID)))
        {
            KCO = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + PdID).ToString());
        }

        KCO = KCO.Replace("<br />", "");
        KCO = KCO.Replace("\r", " ");
        KCO = KCO.Replace("&amp;", " ");
        //KCO = KCO.Replace("\n", " ");

        cryRpt.SetParameterValue("KCO", KCO);
        //cryRpt.SetParameterValue("ToDate", "");

        CrystalReportViewer2.ReportSourceID = "CrystalReportSource2";
        CrystalReportViewer2.ReportSource = cryRpt;

        System.IO.Stream oStream = null;
        byte[] byteArray = null;
        oStream = cryRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        byteArray = new byte[oStream.Length];

        Session.Add("COMMPrintingData", byteArray);
        Session.Add("PrintingEventForm", "COMM_Print");

        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
        Session["byteArray"] = byteArray;
        cryRpt.Close();
        cryRpt.Dispose();


    }

    public DataTable GetReport(int ipdid)
    {
        DataTable dt = new DataTable();

        StrSQL = "Proc_GetIPDDieticianForm";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@ipdid", ipdid);
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();

            dt.Load(dr);

        }

        return dt;

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Convert.ToString(Session["update"]);
    }

    protected void btnVoucherSave_Click(object sender, EventArgs e)
    {
        string finyear = gen.GetCurrentFinYear();
        int AksStat = 0;

        string dov = "";
        if (txtDOV.Text != "")
            dov = gen.getdate(txtDOV.Text.ToString(), '/');

        string Flag = "";
        if (lblVouID.Text == "0")
        {
            Flag = "Save";
        }
        else
        {
            Flag = "Update";
        }

        strSQL = "proc_InsVoucherDetails_IPDViewAks";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@VoucherID", lblVouID.Text);
        SqlCmd.Parameters.AddWithValue("@VoucherDate", dov);
        SqlCmd.Parameters.AddWithValue("@ReturnAmt", txtReturnAmt.Text);
        SqlCmd.Parameters.AddWithValue("@IPDID", ipdid);
        SqlCmd.Parameters.AddWithValue("@CreatedBy", uid);
        //SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@FinYear", finyear);
        SqlCmd.Parameters.AddWithValue("@Being", txtBeing.Text);
        SqlCmd.Parameters.AddWithValue("@Flag", Flag);

        if (con.State == ConnectionState.Closed)
            con.Open();
        AksStat = SqlCmd.ExecuteNonQuery();
        con.Close();
        SqlCmd.Dispose();

        if (AksStat > 0)
        {
            //lblDepID.Text = "0";
            //DDL_Pay_Mode.SelectedValue = "";
            //txtDepositAmt.Text = "";
            //txtDOA.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            //BindIPDAdvanced();
            Response.Redirect("IPDViewAks.aspx?DISPROW=VD", false);
        }
    }

    public void BindVoucherDetails()
    {
        string finyear = gen.GetCurrentFinYear();
        strSQL = "proc_InsVoucherDetails_IPDViewAks";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@VoucherID", lblVouID.Text);
        SqlCmd.Parameters.AddWithValue("@VoucherDate", "");
        SqlCmd.Parameters.AddWithValue("@ReturnAmt", 0);
        SqlCmd.Parameters.AddWithValue("@IPDID", ipdid);
        SqlCmd.Parameters.AddWithValue("@CreatedBy", 0);
        //SqlCmd.Parameters.AddWithValue("@Payment_Mode", "");
        SqlCmd.Parameters.AddWithValue("@FinYear", finyear);
        SqlCmd.Parameters.AddWithValue("@Being", "");
        SqlCmd.Parameters.AddWithValue("@Flag", "GetVou");

        if (con.State == ConnectionState.Closed)
            con.Open();


        dr = SqlCmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        if (dt.Rows.Count > 0)
        {
            GRV_Voucher.DataSource = dt;
            GRV_Voucher.DataBind();
        }
        con.Close();
        dr.Dispose();
    }

    protected void GRD_Vou_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRV_Voucher.EditIndex = e.NewEditIndex;
    }

    protected void GRV_Voucher_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            string finyear = gen.GetCurrentFinYear();
            strSQL = "proc_InsVoucherDetails_IPDViewAks";
            SqlCmd = new SqlCommand(strSQL, con);
            SqlDataReader dr;
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@VoucherID", lblVouID.Text);
            SqlCmd.Parameters.AddWithValue("@VoucherDate", "");
            SqlCmd.Parameters.AddWithValue("@ReturnAmt", 0);
            SqlCmd.Parameters.AddWithValue("@IPDID", ipdid);
            SqlCmd.Parameters.AddWithValue("@CreatedBy", 0);
            //SqlCmd.Parameters.AddWithValue("@Payment_Mode", "");
            SqlCmd.Parameters.AddWithValue("@FinYear", finyear);
            SqlCmd.Parameters.AddWithValue("@Being", "");
            SqlCmd.Parameters.AddWithValue("@Flag", "GetVou");

            if (con.State == ConnectionState.Closed)
                con.Open();

            dr = SqlCmd.ExecuteReader();
            while (dr.Read())
            {
                txtReturnAmt.Text = Sqldr["ReturnAmt"].ToString();
                txtDOV.Text = Sqldr["VoucherDate"].ToString();
                txtBeing.Text = Sqldr["Being"].ToString();
            }
            //int VoucherID = Convert.ToInt32(e.CommandArgument);
            //lblVouID.Text = Convert.ToString(VoucherID);
            //strSQL = "Select D.* , u.uName   from tblVoucherDetails D INNER JOIN tblUser u ON u.[uId] = D.CreatedBy Where VIPDID = "+ ipdid +" And VoucherID ="+ VoucherID +" And D.IsDelete = 0";
            //    SqlCmd.CommandType = CommandType.Text;
            //    SqlDataReader Sqldr;
            //    if (con.State == ConnectionState.Closed)
            //        con.Open();
            //    SqlCmd = new SqlCommand(strSQL, con);
            //    Sqldr = SqlCmd.ExecuteReader();
            //    while (Sqldr.Read())
            //    {

            //        txtReturnAmt.Text = Sqldr["ReturnAmt"].ToString();
            //        //if (txtReturnAmt.Text.ToString().Contains("-"))
            //        //{
            //        //    txtReturnAmt.Text = txtReturnAmt.Text.Replace("-", "");
            //        //}
            //        txtDOV.Text = Sqldr["VoucherDate"].ToString();
            //        txtBeing.Text = Sqldr["Being"].ToString();
            //    }
            //    Sqldr.Close();
            //    SqlCmd.Dispose();
            //    con.Close();
        }
        if (e.CommandName == "del")
        {
            int AksStat = 0;
            int VoucherID = Convert.ToInt32(e.CommandArgument);
            lblVouID.Text = Convert.ToString(VoucherID);

            string dov = "";
            if (txtDOV.Text != "")
                dov = gen.getdate(txtDOV.Text.ToString(), '/');

            strSQL = "proc_InsVoucherDetails_IPDViewAks";
            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@VoucherID", lblVouID.Text);
            SqlCmd.Parameters.AddWithValue("@VoucherDate", dov);
            SqlCmd.Parameters.AddWithValue("@ReturnAmt", 0);
            SqlCmd.Parameters.AddWithValue("@IPDID", ipdid);
            SqlCmd.Parameters.AddWithValue("@CreatedBy", uid);
            //SqlCmd.Parameters.AddWithValue("@Payment_Mode", "");
            SqlCmd.Parameters.AddWithValue("@FinYear", "");
            SqlCmd.Parameters.AddWithValue("@Being", "");
            SqlCmd.Parameters.AddWithValue("@Flag", "Delete");

            if (con.State == ConnectionState.Closed)
                con.Open();
            AksStat = SqlCmd.ExecuteNonQuery();
            con.Close();
            SqlCmd.Dispose();

            if (AksStat > 0)
            {
                //lblDepID.Text = "0";
                //DDL_Pay_Mode.SelectedValue = "";
                //txtDepositAmt.Text = "";
                //txtDOA.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                //BindIPDAdvanced();
                Response.Redirect("IPDViewAks.aspx?DISPROW=VD", false);
            }

        }
    }

    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }
    }
}