﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class Confirm_Appoinment : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string pdId = "0";
    string uid = "0";
    string ipdid = "0";
    int Page_no = 0, Page_size = 10;
    bool IsExport = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 49";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["PatType"] != null)
            ddlApType.SelectedValue = Request.QueryString["PatType"].ToString();
            //ddlApType.Enabled = false;

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            Session["CasePatientID"] = null;
            BindGVR();
        }
    }

    public void BindGVR()
    {
        string[] _frm = txtfrm.Text.ToString().Split('/');
        string[] _to = txtto.Text.ToString().Split('/');

        string frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
        string to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";

        string type = "";
        if (chkApType.Checked)
            type = ddlApType.SelectedValue;
        else
            type = "0";

        string MobNo = "%";
        if (txtmobno.Text != "")
        {
            MobNo = txtmobno.Text;
        }

        SqlCmd = new SqlCommand();
        SqlCmd.CommandText = "GetTodayAppointmentForConfirm";
        SqlCmd.Parameters.AddWithValue("@FrmDate", frm);
        SqlCmd.Parameters.AddWithValue("@ToDate", to);
        SqlCmd.Parameters.AddWithValue("@pdName", txtname.Text.Trim());
        SqlCmd.Parameters.AddWithValue("@pdMobNo", MobNo);
        SqlCmd.Parameters.AddWithValue("@APType", type);
        

        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Connection = con;
        SqlDataAdapter dapt = new SqlDataAdapter(SqlCmd);
        DataSet ds = new DataSet();
        con.Open();
        dapt.Fill(ds, "Table");
        con.Close();

        if (ds.Tables[0].Rows.Count > 0)
        {
            GRV1.DataSource = ds.Tables[0];
            pnlHead.Visible = true;
            GRV1.PageSize = Page_size;
            GRV1.DataBind();
            createpaging(ds.Tables[0].Rows.Count, GRV1.PageCount);
        }
        else
        {
            GRV1.DataSource = null;
            pnlHead.Visible = true;
            //GRV1.PageSize = Page_size;
            //GRV1.DataBind();
            createpaging(ds.Tables[0].Rows.Count, GRV1.PageCount);
        }

       
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "Confirm")
        {
            try
            {
                if (ddlApType.SelectedValue == "2")
                {
                    SqlCmd = new SqlCommand();
                    SqlCmd.CommandText = "proc_ConfirmAppointmentDetails";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Connection = con;
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    SqlCmd.Parameters.AddWithValue("@adId", e.CommandArgument);
                    SqlCmd.Parameters.AddWithValue("@ConfirmBy", uid);
                    SqlCmd.Parameters.AddWithValue("@pdfinyear", gen.GetCurrentFinYear());
                    SqlCmd.Parameters.AddWithValue("@Flag", "Confirm");
                    SqlCmd.ExecuteNonQuery();
                    con.Close();
                    SqlCmd.Dispose();
                    Response.Redirect("Confirm_Appoinment.aspx?PatType=1");
                }
                else
                {
                    Response.Redirect("AddNewPatient.aspx?ADID=" + e.CommandArgument);
                }

             }
            catch
            { }
        }

        if (e.CommandName == "Delete")
        {
            try
            {
                
                    SqlCmd = new SqlCommand();
                    SqlCmd.CommandText = "proc_ConfirmAppointmentDetails";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Connection = con;
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    SqlCmd.Parameters.AddWithValue("@adId", e.CommandArgument);
                    SqlCmd.Parameters.AddWithValue("@ConfirmBy", uid);
                    SqlCmd.Parameters.AddWithValue("@pdfinyear", gen.GetCurrentFinYear());
                    SqlCmd.Parameters.AddWithValue("@Flag", "Delete");
                    SqlCmd.ExecuteNonQuery();
                    con.Close();
                    SqlCmd.Dispose();
                    Response.Redirect("Confirm_Appoinment.aspx?PatType=1");
               
            }
            catch
            { }
        }

       // BindGVR();
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
       
    }
   
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (IsExport == true)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkpdName = (LinkButton)e.Row.FindControl("lnkpdName");
                Literal lit = new Literal();
                lit.Text = lnkpdName.Text;
                e.Row.Cells[0].Controls.Add(lit);
                e.Row.Cells[0].Controls.Remove(lnkpdName);
            }
        }
    }
    
   
}