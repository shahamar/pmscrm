﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class OPDReport : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    string uid = "0";
    string tempDate = "";
    string tempDoctor = "";
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //StrSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 43";
        //bool exists = gen.doesExist(StrSQL);
        //if (exists == false)
           // Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();
            BindGridView();
        }

        if (!(Session["Report"] as string == "{CrystalDecisions.CrystalReports.Engine.ReportDocument}"))
        {
            CrystalReportViewer2.ReportSource = (ReportDocument)Session["Report"];
        }


    }

    protected void FillData()
    {
        DDL_Doctor.Items.Insert(0, new ListItem("Select", "%"));
        gen.FillDropDownList("Select uId , uFname +' '+ uMname +' '+ uLname As DNAME from tblUser Where IsDelete = 0 and uRole = 2 ", "uId", "DNAME", DDL_Doctor);
        //DDL_Doctor.SelectedValue = uid;

    }

    private void BindGridView()    
    {
        lblSubTotal.Text = "0.00";
        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];
        StrSQL = "Sp_GetOPDReport";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@frmDate", Frmdate);
        SqlCmd.Parameters.AddWithValue("@toDate", Todate);
        SqlCmd.Parameters.AddWithValue("@Doctor", DDL_Doctor.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            //SqlCmd.Dispose();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {    
                pnlHead.Visible = true;
                rptHead.Visible = true;
                GRV1.DataSource = dt;
                GRV1.DataBind();

                decimal Total = 0;
               
                foreach (GridViewRow row in GRV1.Rows)
                {
                    
                    Label lblsub = (Label)row.FindControl("lblPayment");
                    if (lblsub.Text == "")
                    {
                        Total += 0;
                        lblsub.Text = "0";
                    }
                    else
                    {
                        Total += Decimal.Parse(lblsub.Text.ToString());
                    }
                    
                }
                decimal tot = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string  total = "0";
                    decimal temp=0;
                    total=(dt.Rows[i]["fdCharges"].ToString());
                        
                    temp =total.ToString() ==""?Decimal.Parse("0.00"):Decimal.Parse(total.ToString());
                    tot += temp;
                }

		decimal tt=0;
		for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tt += decimal.Parse(dt.Rows[i]["PreviousBalance"].ToString());
                }

		lblBalTotal.Text = tt.ToString();
                lblGrandTotal.Text = tot.ToString();
                lblSubTotal.Text = Total.ToString();
                export.Style.Add("display", "block");
                createpaging(dt.Rows.Count, GRV1.PageCount);
                
                decimal subtot = decimal.Parse(lblSubTotal.Text);
                lblSubTotal.Text = subtot.ToString();
                Label1.Visible = true;
                lblSubTotal.Visible = true;
            }
                 

            else
            {
                pnlHead.Visible = false;
                rptHead.Visible = false;
                export.Style.Add("display", "none");
                lblGridHeader.Text = "No record found";
            }
        }
    }

    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        Label1.Visible = false;
        lblSubTotal.Visible = false;
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "OPDReport.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRV1.AllowPaging = false;
        this.BindGridView();
        GRV1.Columns[9].Visible = false;
        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();

       
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGridView();
        ScriptManager1.RegisterPostBackControl(this.imgexport);
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        CrystalReportViewer2.Visible = true;
        string AksConn = ConfigurationManager.AppSettings["constring"];
        bindReport(AksConn);
    }

    public void bindReport(string AksConn)
    {
        DataTable dt = new DataTable();
        ReportDocument cryRpt = new ReportDocument();
        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = default(CrystalDecisions.CrystalReports.Engine.Tables);
        System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

        builder.ConnectionString = AksConn;

        string AksServer = builder["Server"] as string;
        string AksDatabase = builder["Database"] as string;
        string AksUsername = builder["Uid"] as string;
        string AksPassword = builder["Pwd"] as string;


        dt = GetOPDReport();
        cryRpt.Load(Server.MapPath("~/Reports/OpdReportAks.rpt"));



        CrTables = cryRpt.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        cryRpt.SetDataSource(dt);
        cryRpt.Refresh();

        cryRpt.SetParameterValue("FromDate", txtfrm.Text);
        cryRpt.SetParameterValue("ToDate", txtto.Text);

        CrystalReportViewer2.ReportSourceID = "CrystalReportSource2";
        CrystalReportViewer2.ReportSource = cryRpt;

        System.IO.Stream oStream = null;
        byte[] byteArray = null;
        oStream = cryRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        byteArray = new byte[oStream.Length];

        Session.Add("COMMPrintingData", byteArray);
        Session.Add("PrintingEventForm", "COMM_Print");

        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
        Session["byteArray"] = byteArray;
        cryRpt.Close();
        cryRpt.Dispose();


    }

    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (((Label)e.Row.FindControl("lbldate")).Text == tempDate)
            {
                e.Row.Cells[1].Text = string.Empty;
            }
            else
            {
                tempDate = ((Label)e.Row.FindControl("lbldate")).Text;
            }

            if (((Label)e.Row.FindControl("lblDoctor")).Text == tempDoctor)
            {
                e.Row.Cells[0].Text = string.Empty;
            }
            else
            {
                tempDoctor = ((Label)e.Row.FindControl("lblDoctor")).Text;
            }

        }
    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGridView();
    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }



    public DataTable GetOPDReport()
    {
        DataTable dt = new DataTable();
        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];
        StrSQL = "Sp_GetOPDReport";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@frmDate", Frmdate);
        SqlCmd.Parameters.AddWithValue("@toDate", Todate);
        SqlCmd.Parameters.AddWithValue("@Doctor", DDL_Doctor.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);


        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();

            dt.Load(dr);

        }

        return dt;


    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Convert.ToString(Session["update"]);
    }


}
