<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterHome1.master" CodeFile="StockManager.aspx.cs" Inherits="StockManager" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
 <script type="text/javascript" src="js/custom-form-elements.js"></script>

    <style type="text/css">
        .style1
        {
            width: 274px;
        }
    </style>
<style>
        ._tableh1
        {
            background-image: url(    "images/tile_back1.gif" );
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
            height: 18px;
            padding: 8px 12px 8px 8px;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
<asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
<center>
	<table Style="Width:100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
		<tr>
			<td></td>
		</tr>
		<tr>
			<td>
				<div class="login-area margin_ten_right" style="width: 100%;">
                    		  	<span style="text-align:center;">
                        		<h2>Stock Manager</h2>
                        		</span></br>
                        		<div class="formmenu">
                            			<div class="loginform">
						<ul class="quicktabs_tabs quicktabs-style-excel">
                                    			<li class="qtab-Demo active first" id="li_1">
                                    				<a class="qt_tab active" href="javascript:void(0)">Add New Stock</a> 
                                    			</li>
                                    			<li class="qtab-HTML last" id="li_9">
                                    				<a class="qt_tab active" href="ManageStock.aspx">Manage Stock</a> 
                                   			</li>
                                		</ul>
                                		<div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
						<table style="width:100%; background:#f2f2f2; padding-left:20px; padding-bottom:10px; padding-top:10px;" cellspacing="0px" cellpadding="5px">
                                       		<tr>
							<td Colspan="6" style="text-align: center; color: Red;">
							<asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
							<td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
                                            		<td Width="12%">
								Company Name :
							</td>
							<td align="left">
								<asp:DropDownList ID="DDLCompany" Width="190px" CssClass="field" runat="server"></asp:DropDownList>
								<%--<asp:TextBox ID="txtCompanyName" runat="server" CssClass="field" Width="80%"></asp:TextBox>--%>
								<div style="margin-bottom: -6px; float: right;" id="div_Remedy" runat="server">
                            						<table border="0" cellpadding="0" cellspacing="0">
                                					<tr>
                                    						<td>
                                        						<a href="CompanyMaster.aspx" class="example7"><img src="Images/add.png" /></a>
                                    						</td>
                                    						<td>
                                        						&nbsp;&nbsp;
                                    						</td>
                                					</tr>
                            						</table>
                        					</div>
								<%--<a href="CompanyMaster.aspx" target="_self"><img src="Images/add.png" /></a>--%>
							</td>
							<td>
								Invoice Date :
							</td>
							<td>
								<asp:TextBox ID="txtReceivedDate" runat="server" PlaceHolder="DD/MM/YYYY" CssClass="field"  Width="50%"></asp:TextBox>
								<cc1:CalendarExtender ID="CalendarExtender" runat="server" TargetControlID="txtReceivedDate" Format="dd/MM/yyyy" PopupButtonID="imgDOR"></cc1:CalendarExtender>
								<asp:Image ID="imgDOR" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit;width: 18px; height: 18px; top: 5px; left: -5px;" />
							</td>
							<td>
								Invoice No. :
							</td>
							<td align="left">
								<asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="field"  Width="50%"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>	
							<td>
								Product :
							</td>
							<td>
								<asp:TextBox ID="txtProduct" runat="server" CssClass="field"  Width="80%"></asp:TextBox>
							</td>
							<td>
								Qty :
							</td>
							<td>
								<asp:TextBox ID="txtQty" runat="server" CssClass="field" Width="50%"></asp:TextBox>
							</td>
							<td>
								Unit Per Rate :
							</td>
							<td>
								<asp:TextBox ID="txtUnit" runat="server" CssClass="field" Width="50%"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>
								CGST :
							</td>
							<td>
								<asp:TextBox ID="txtCGST" runat="server" CssClass="field" Width="50%"></asp:TextBox>
							</td>
							<td>
								SGST :
							</td>
							<td>
								<asp:TextBox ID="txtSGST" runat="server" CssClass="field" Width="50%"></asp:TextBox>
							</td>
							<td>
								Grand Total :
							</td>
							<td>
								<asp:TextBox ID="txtGrandTot" runat="server" CssClass="field" Width="50%"></asp:TextBox>
							</td>
							
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>
								Total Price :
							</td>
							<td>
								<asp:TextBox ID="txtTotalPrice" runat="server" CssClass="field" Width="80%"></asp:TextBox>
							</td>
							<td>
								Product Description :
							</td>
							<td>
								<asp:TextBox ID="txtProDescription" runat="server" CssClass="field" TextMode="MultiLine" Width="80%"></asp:TextBox>
							</td>
							
							<td>
							</td>
							<td>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr ID="TR_Bal" runat="server">
							<td>
								Issue Stock :
							</td>
							<td>
								<asp:TextBox ID="txtIssueStock" runat="server" CssClass="field" Width="80%"></asp:TextBox>
							</td>
							<td>
								Received Stock :
							</td>
							<td>
								<asp:TextBox ID="txtReceivedStock" runat="server" CssClass="field" Width="50%"></asp:TextBox>
							</td>
							<td>
								Balance Stock :
							</td>
							<td>
								<asp:TextBox ID="txtBalanceStock" runat="server" CssClass="field" Width="50%"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td align="center" Colspan="6">
								<asp:Button ID="btn_Save" Text="Save" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click" Style="margin-top:5px;margin-right:20px;"/>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						</table>
						</div>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</center>
</asp:Content>