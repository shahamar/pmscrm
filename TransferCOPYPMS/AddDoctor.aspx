﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="AddDoctor.aspx.cs" Inherits="AddDoctor" Title="Add Doctor Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />

    <script type="text/javascript">
        function Validate()
        {
            var fname = document.getElementById("<%= txtfname.ClientID %>").value;
            
            var Err = "";
                        
            if(fname == "")
            {
                Err += "Please Fill the First Name";
                document.getElementById("<%= txtfname.ClientID %>").className = "field input-validation-error";
            }
            else
            {
                document.getElementById("<%= txtfname.ClientID %>").className = "field";
            }
            
            if(Err != "")
            {
                return false;   
            }
        }

        function DisableButtons() {
            
            var inputs = document.getElementsByTagName("INPUT");
            for (var i in inputs) {
                if (inputs[i].type == "button" || inputs[i].type == "submit") {
                    inputs[i].disabled = true;
                }
            }
        }

        window.onbeforeunload = DisableButtons;
    </script>
    <style type="text/css">
        .b_submit input[type="submit"]:disabled{color:Black;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Add Doctor</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">
                                            Add New Doctor</a> </li>
                                        <li class="qtab-HTML last" id="li_9"><a class="qt_tab active" href="ManageDoctor.aspx">
                                            Manage Doctors</a> </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto;">
                                        <table style="width: 100%; padding-left: 20px" cellspacing="0px" cellpadding="0px">
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    First Name*
                                                </td>
                                                <td>
                                                    Middle Name
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtfname" runat="server" CssClass="field required" MaxLength="50"
                                                        Style="float: left;"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="txtfname"
                                                        Display="Dynamic" ErrorMessage="First Name is required." ValidationGroup="val">
                                                        <span class="error">First Name is required.</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regFname" runat="server" CssClass="field required"
                                                        Display="Dynamic" ErrorMessage="Please enter valid name" ValidationExpression="^[a-zA-Z .\s]{3,100}$"
                                                        ControlToValidate="txtfname" ValidationGroup="val">
                                                             <span class="error">Please enter valid name!</span>
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtmname" runat="server" CssClass="field" MaxLength="50"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regmname" runat="server" CssClass="field required"
                                                        ControlToValidate="txtmname" Display="Dynamic" ErrorMessage="Please enter valid name"
                                                        ValidationExpression="^[a-zA-Z .\s]{0,100}$" ValidationGroup="val">
                                                        <span class="error">Please enter valid name!</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;Last Name
                                                </td>
                                                <td>
                                                    Date Of Joining*
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtlname" runat="server" CssClass="field" MaxLength="50" Style="float: left;"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="reg_lname" runat="server" CssClass="field required"
                                                        ControlToValidate="txtlname" Display="Dynamic" ErrorMessage="Please enter valid name"
                                                        ValidationExpression="^[a-zA-Z .\s]{0,100}$" ValidationGroup="val">
                                                        <span class="error">Please enter valid name!</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDOJ" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txtDOJ_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgDOJ" TargetControlID="txtDOJ">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="imgDOJ" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit;
                                                        width: 20px; height: 20px; top: 5px; left: -5px;" />
                                                    <asp:RequiredFieldValidator ID="req_DOC" runat="server" ControlToValidate="txtDOJ" ValidationGroup="val"
                                                        Display="Dynamic" ErrorMessage="Joining date is required!" CssClass="field required">
                                                        <span class="error">Joining date is required!</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regDOC" runat="server" CssClass="field required" ValidationGroup="val"
                                                        Display="Dynamic" ControlToValidate="txtDOJ" ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                                                        <span class="error">Enter valid date!</span></asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="cvDOC" runat="server" Display="Dynamic" ControlToValidate="txtDOJ" ValidationGroup="val"
                                                        CssClass="field required" ClientValidationFunction="CallDateFun" ErrorMessage="Please enter valid date!">
                                                        <span class="error">Please enter valid date!</span></asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Address Line1
                                                </td>
                                                <td>
                                                    Address Line2
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="required field" MaxLength="50"
                                                        Style="float: left;" Width="300px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regAddress1" runat="server" Display="Dynamic"
                                                        CssClass="field required" ControlToValidate="txtAddress1" ErrorMessage="Please enter valid address!"
                                                        ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,100}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid address!</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="field" MaxLength="50" Width="300px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regAddress2" runat="server" Display="Dynamic"
                                                        CssClass="field required" ControlToValidate="txtAddress2" ErrorMessage="Please enter valid address!"
                                                        ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,100}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid address!</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Contact No
                                                </td>
                                                <td>
                                                    Mobile No.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="images/phone.png" />&nbsp;<asp:TextBox ID="txttele" runat="server" CssClass="field"
                                                        MaxLength="20" Width="130px"></asp:TextBox>
                                                    &nbsp;
                                                    <asp:RegularExpressionValidator ID="regtele" runat="server" Display="Dynamic" CssClass="field required"
                                                        ControlToValidate="txttele" ErrorMessage="Please enter valid telephone number!"
                                                        ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid telephone number!</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <img src="images/mobile_phone.png" />
                                                    <asp:TextBox ID="txtmob" runat="server" CssClass="field" MaxLength="20" Width="130px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regmob" runat="server" Display="Dynamic" CssClass="field required"
                                                        Style="padding-left: 140px;" ControlToValidate="txtmob" ErrorMessage="Please enter valid mobile number!"
                                                        ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid mobile number!</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Type*
                                                </td>
                                                <td>
                                                    EmailId
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="DDType" runat="server">
                                                        <asp:ListItem Text="SELECT" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="VISITING" Value="VISITING"></asp:ListItem>
                                                      
                                                        <asp:ListItem Text="OLD" Value="OLD"></asp:ListItem>
                                                       <asp:ListItem Text="CURRENT" Value="CURRENT"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="reqtype" runat="server" CssClass="field required"
                                                        ControlToValidate="DDType" Display="Dynamic" ValidationGroup="val">
                                                        <span class="error">Type is required!</span></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmailid" runat="server" CssClass="field" MaxLength="50"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regEmailID1" runat="server" ControlToValidate="txtEmailid"
                                                        CssClass="field required" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
                                                        ValidationGroup="val">
                                                        <span class="error">Please enter valid email Id!</span>
                                                    </asp:RegularExpressionValidator>
                                                    <asp:RegularExpressionValidator ID="reg_EmailId1" runat="server" ControlToValidate="txtEmailid"
                                                        CssClass="field required" ValidationExpression="^[\s\S]{0,50}$" ValidationGroup="val">
                                                        <span class="error">Max 60 characters allowed!</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>

                                              <tr>
                                                <td>
                                                    Date Of Birth
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>

                                             <tr>
                                                <td>
                                                    <asp:TextBox ID="txtDOB" runat="server" placeholder="DD/MM/YYYY" CssClass="field" Width="100px"></asp:TextBox>

                                                 <%--   <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgDOJ" TargetControlID="txtDOB">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit;
                                                        width: 20px; height: 20px; top: 5px; left: -5px;" />
                                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDOB" ValidationGroup="val"
                                                        Display="Dynamic" ErrorMessage="Joining date is required!" CssClass="field required">
                                                        <span class="error">DOB is required!</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="field required" ValidationGroup="val"
                                                        Display="Dynamic" ControlToValidate="txtDOB" ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                                                        <span class="error">Enter valid date!</span></asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="CustomValidator1" runat="server" Display="Dynamic" ControlToValidate="txtDOB" ValidationGroup="val"
                                                        CssClass="field required" ClientValidationFunction="CallDateFun" ErrorMessage="Please enter valid date!">
                                                        <span class="error">Please enter valid date!</span></asp:CustomValidator>--%>

                                                           <cc1:CalendarExtender ID="ce1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                        PopupButtonID="imgDOB" TargetControlID="txtDOB">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="imgDOB" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit;
                                                        width: 18px; height: 18px; top: 5px; left: -5px;" />
                                                          <cc1:MaskedEditExtender ID="meeMonthYear" runat="server"
                                                             TargetControlID="txtDOB" Mask="99/99/9999"  MaskType="Date" 
                                                             InputDirection="RightToLeft"></cc1:MaskedEditExtender>

                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                        Style="margin-top: 2px;" Text="Save" ValidationGroup="val" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
