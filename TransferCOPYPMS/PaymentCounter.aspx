﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="PaymentCounter.aspx.cs" Inherits="PaymentCounter" %>

  <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<style>
.AksDisplay {display:none;}
.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}
.mGrid { 
    width: 100%; 
    background-color: #eee; 
    margin: 5px 0 10px 0; 
    border: solid 1px #525252; 
    border-collapse:collapse; 
    
}
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <div class="login-area margin_ten_right" style="width: 100%; text-align:center">
                <h2>Cash Counter</h2>
              	
                       <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">

                        <%--  <asp:UpdatePanel ID="UP3" runat="server">
                           <ContentTemplate>--%>

                          <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">

                        

                              <tr>
                                <td>    
                              		<table style="width:100%;" cellspacing="0px" cellpadding="0px">
                              			<tr>

                              				<td><strong>From :&nbsp;</strong>
                                                <asp:TextBox ID="txtfrm" runat="server" CssClass="field textbox"></asp:TextBox>
                                                <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                    runat="server" Enabled="True" TargetControlID="txtfrm">
                                                </cc1:CalendarExtender>&nbsp;
                                                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"  />
                                            </td>
                                            <td style="padding-left:15px;"><strong>To :&nbsp;</strong>
                                                <asp:TextBox ID="txtto" runat="server" CssClass="field textbox"></asp:TextBox>
                                                <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                    Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                </cc1:CalendarExtender>&nbsp;
                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"  />
                            				</td>
						<td style="padding-left:15px;">
                                                                   Search By CPN : &nbsp;
                                                                    </td>             
                                                                            <td>
                                                                                <asp:TextBox ID="txtSearchCPN" runat="server"></asp:TextBox>
                                                                                <%-- <asp:DropDownList ID="DDL_Doctor" runat="server" class="required field"  Width="150px">
                                                                                </asp:DropDownList>--%>
                                                                        	</td>
                                                                               <td>
                                                                   Payment Mode : &nbsp;
                                                                    </td>            
                                                                        	<td>
                                                                                 <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="80px" CssClass="field">
										    <asp:ListItem Text="All" Value="%"></asp:ListItem>
                                                                                    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                                                    <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                                                    <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                                                    <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                                                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
											<asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
											</asp:DropDownList>
                                                                        	</td>
                                              <td>Search Name :</td>
                                              <td><asp:TextBox ID="txtSearch" runat="server" CssClass="field"></asp:TextBox></td>
                                            <td>
                                               <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" />
                                            </td>
                                           
                          				</tr>
                          			</table>
                          		</td>
                          	  </tr>

                                <tr>
                                <td style="text-align: center;">
                                    <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></font>
                                </td>
                              </tr>

                        	  <tr>
                             	<td id="gvcol" style="text-align: center;background:#fff">                              
                                    <asp:GridView ID="GVD_FollowupPaymentDetails" runat="server" CssClass="mGrid" AutoGenerateColumns="false"  
                                    PageSize="20" OnRowCommand="GVD_FollowupPaymentDetails_RowCommand"  AllowPaging="true" onrowdatabound="GVD_FollowupPaymentDetails_RowDataBound" 
                                    DataKeyNames="fdId" OnPageIndexChanging="GVD_FollowupPaymentDetails_PageIndexChanging">
					<AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                     <asp:BoundField DataField="SNO" HeaderText="Sr No." />
                                       
                                     <asp:BoundField DataField="PNAME" HeaderText="Patient Name" />
                                       
                                     <asp:BoundField DataField="pdCasePaperNo" HeaderText="Case Paper No." />

                                       <asp:TemplateField HeaderText="Charges Paid">
                                              <ItemTemplate>
                                                  <%-- <asp:TextBox ID="txt_ChargesPaid" Width="80px" runat="server" Text='<%# Eval("PaidAmount")%>'></asp:TextBox> --%>
                                                   <asp:TextBox ID="txt_ChargesPaid" Width="80px" runat="server" Text='<%# Eval("TotalAmt")%>'></asp:TextBox>
                                                   <asp:Label ID="lbl_Stat" runat="server" Visible="false" Text='<%# Eval("Stat")%>'></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>
						<asp:BoundField DataField="TotalAmt" HeaderText="Total Amount" /> 

                                             <asp:TemplateField HeaderText="Payment Mode">
                                              <ItemTemplate>
                                              <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="80px" CssClass="field">
                                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                <%--<asp:ListItem Text="Other" Value="Other"></asp:ListItem>--%>
                                                <asp:ListItem Text="Free" Value="Free"></asp:ListItem>
						<asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
                                                </asp:DropDownList>
                                                  <asp:Label ID="lbl_Pay_Mode" runat="server" Visible="false" Text='<%# Eval("Payment_Mode")%>'></asp:Label>
                                                  <asp:Label ID="lblResult" runat="server" Visible="false"></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>

                                          <asp:TemplateField HeaderStyle-Width="7%" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                              <ItemTemplate>

                                                    <asp:ImageButton ID="btnPayAmount" runat="server" CommandName="Payment"
                                                    CommandArgument='<%# Eval("fdId") %>' ImageUrl="~/images/button_ok.png" ToolTip="Pay Amount"/>

                                                     <asp:ImageButton ID="btnEdit" runat="server" CommandName="Allow"
                                                    CommandArgument='<%# Eval("fdId") %>' ImageUrl="~/images/edit.ico" ToolTip="Edit Amount"/>

                                                     <%# Eval("PrintBill") %>
                                                 
                                              </ItemTemplate>    
                                          </asp:TemplateField>

                                          
                                      <asp:BoundField DataField="RemedyPeriod" HeaderText="Remedy Period" />

                                        <asp:BoundField DataField="DoseType" HeaderText="Dose Type" />
    
                                        <asp:BoundField DataField="AddEmg" HeaderText="Add Emg" />

                                           <asp:BoundField DataField="ExtraDos" HeaderText="Extra doses" />

                                          <asp:BoundField DataField="Charges" HeaderText="Remedy Charge" />

                                           <asp:BoundField DataField="PreviousBalance" HeaderText="Previous Balance" />

                                                                                                                           

                                     </Columns>
                                   
                                    </asp:GridView>
                                </td>                           
                        	  </tr>                       
                              
                    	  </table>

                           <%-- </ContentTemplate>
                          </asp:UpdatePanel>--%>
                        </div>
                     
            </div>
        </td>
    </tr>
    
</table>
</asp:Content>

