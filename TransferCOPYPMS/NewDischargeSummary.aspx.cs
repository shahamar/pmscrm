﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class NewDischargeSummary : System.Web.UI.Page
{
    string strSQL = "";
    genral gen = new genral();
    string idID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
            idID = Request.QueryString["id"].ToString();

        FillData();

    }



    protected void FillData()
    {
        object o = null;
        string PdID = "";

        strSQL = "select pdID from tblIpdMaster i  inner join tblPatientDetails p on i.ipdpdId =p.pdID ";
        strSQL += " Where ipdId  = " + idID + "";
        DataTable dt1 = gen.getDataTable(strSQL);

        PdID = dt1.Rows[0]["pdID"].ToString();

        if (File.Exists(Server.MapPath("Files/Investigation/inv_" + PdID)))
        {
            lblInv.Text = File.ReadAllText(Server.MapPath("Files/Investigation/inv_" + PdID).ToString());
        }

        if (File.Exists(Server.MapPath("Files/ChiefCo/chief_" + PdID)))
        {
            lblChiefCo.Text = File.ReadAllText(Server.MapPath("Files/ChiefCo/chief_" + PdID).ToString());
            
        }
        lblChiefCo.Text = lblChiefCo.Text.Replace("<br />", " ");

        strSQL = "select distinct(ipdId),pdID,CONVERT(VARCHAR(10), ipdAdmissionDate, 105) AS DOA,CONVERT(VARCHAR(10), ipdDischargeDate, 105) AS DOC,pdReceiptNo,isnull(pdAge,'NA')pdAge,pdSex,isnull(ipdRefBy,'NA')ipdRefBy,pdInitial +' '+pdFname +' '+pdMname +' '+ pdLname AS 'PName', ";
        strSQL += "ipdFinalDiagnosis, [dbo].[TestList](ipdId) as 'Investigation',p.pdCasePaperNo,ipdTreatment as 'Treatment','" + lblChiefCo.Text + "' as 'ChiefCo' ";
        strSQL += " from tblIpdMaster i inner join tblPatientDetails p on i.ipdpdId =p.pdID left join tblIpdTestDetails itd on itd.itdipdId = i.ipdId left join tblTestMaster t on t.tmId = itd.itdtmId   left join tblFollowUpDatails f on f.fdpatientID=p.pdID";
        strSQL += " Where ipdId  = " + idID + "";
        DataTable dt = gen.getDataTable(strSQL);
        rptParent.DataSource = dt;
        rptParent.DataBind();        

    }

}
