﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;

public partial class OnlineWorkshop : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string Uroll = "0";
    int Page_no = 0, Page_size = 20;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (Session["urole"] != null)
            Uroll = Session["urole"].ToString();

        if(!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindOnlineWorkshop();
        }
    }
    private void BindOnlineWorkshop()
    {
        lblSubTotal.Text = "0.00";
        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];

        StrSQL = "GetOnlineWorkshopDetails";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FrmDate", Frmdate);
        SqlCmd.Parameters.AddWithValue("@ToDate", Todate);
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();


                decimal Total = 0;

                foreach (GridViewRow row in GRV1.Rows)
                {

                    Label lblsub = (Label)row.FindControl("lblPayment");
                    if (lblsub.Text == "")
                    {
                        Total += 0;
                        lblsub.Text = "0";
                    }
                    else
                    {
                        Total += Decimal.Parse(lblsub.Text.ToString());
                    }

                }
                decimal tot = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string total = "0";
                    decimal temp = 0;
                    total = (dt.Rows[i]["PaidAmt"].ToString());

                    temp = total.ToString() == "" ? Decimal.Parse("{0:0.00}") : Decimal.Parse(total.ToString());
                    tot += temp;
                }

                
                lblGrandTotal.Text = tot.ToString("f2");
                lblSubTotal.Text = Total.ToString();
                
                decimal subtot = decimal.Parse(lblSubTotal.Text);
                lblSubTotal.Text = subtot.ToString();
                Label1.Visible = true;
                lblSubTotal.Visible = true;
            }
            else
            {
                GRV1.DataSource = null;
                GRV1.DataBind();
            }
            conn.Close();
            dr.Dispose();

        }
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindOnlineWorkshop();
    }
    protected void GRV_Workshop_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindOnlineWorkshop();
    }
    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //string WrId = e.CommandArgument.ToString();
        //if (e.CommandName == "_ADDPayment")
        //{
        //    Session["WrId"] = WrId;
        //    Response.Redirect("WorkshopAddPayment.aspx?WrId=" + WrId);
        //}
    }
}