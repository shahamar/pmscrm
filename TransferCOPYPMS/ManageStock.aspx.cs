using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;


public partial class ManageStock : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlCommand SqlCmd;
    SqlDataReader Sqldr;
    int Page_no = 0, Page_size = 10;
    string strSQL;
    string uid;
    
	protected void Page_load(object sender, EventArgs e)
	{
        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }
        strSQL = "Select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 18";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");

        if (Request.QueryString["flag"] == "3")
        {
            lblMessage.Text = "Stock Deleted Successfully...!";
        }

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindGridView();
        }
	}
    private void BindGridView()
    {
        try
        {
            string[] _frm = txtfrm.Text.ToString().Split('/');
            string[] _to = txtto.Text.ToString().Split('/');

            string frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            string to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";


            strSQL = "Select * from tblStockManage sm Inner join tblCompanyMaster cm on sm.StkCompanyName = cm.Cid where StkIsDelete = 0";

            if (chkStockPeriod.Checked == true)
            //if (txtfrm.Text.ToString() != "" && txtto.Text.ToString() != "")
            {
                strSQL += " AND StkDOR BETWEEN '" + frm + "' and '" + to + "'";
            }

            if (txtCompanyName.Text.ToString() != "")
            {
                strSQL += "AND Cname Like '%" + txtCompanyName.Text.ToString() + "%'";
            }
            if (txtProduct.Text.ToString() != "")
            {
                strSQL += "AND StkProductName Like '%" + txtProduct.Text.ToString() + "%'";
            }
            strSQL += " Order by StkCreatedDate DESC";

            DataTable dt = gen.getDataTable(strSQL);
            if (dt.Rows.Count > 0)
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
            else
            {
                GRV1.DataSource = null;
                GRV1.DataBind();
            }
        }
        catch(Exception e)
        {
        }
    }
    
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGridView();
    }

    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGridView();
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string Pid = e.CommandArgument.ToString();
        if (e.CommandName == "edt")
        {
            Response.Redirect("StockManager.aspx?Pid=" + e.CommandArgument);
        }
        if (e.CommandName == "del")
        {
            gen.executeQuery("Update tblStockManage Set StkIsDelete = 1, StkIsDeletedBy = " + uid + "Where StkPid=" + Pid);
            Response.Redirect("ManageStock.aspx?flag=3", false);
        }
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGridView();
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        //lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;
    }
}