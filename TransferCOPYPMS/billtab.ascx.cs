﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient; 

public partial class billtab : System.Web.UI.UserControl
{
    string strSQL = "";
    SqlDataReader Sqldr;
    SqlCommand SqlCmd;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);

    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["BilledPatientID"] != null)
            {
                ViewState["pdId"] = Session["BilledPatientID"].ToString();
            }
            if (Session["BilledipdId"] != null)
            {
                ViewState["ipdid"] = Session["BilledipdId"].ToString();
            }
            else
            {
                Response.Redirect("ManageBill.aspx");
            }

            FillData();
        }
    }


    private void FillData()
    {
        strSQL = "Select *,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName',(select (dFName+' '+dMName+' '+dLName) AS DName From tblDoctorMaster WHERE did=dbo.tblPatientDetails.did)as DoctorName From dbo.tblPatientDetails where pdID=" + PdID;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            lblPatientName.Text = Sqldr["pdName"].ToString();
            lblPatientId.Text = Sqldr["pdPatientICardNo"].ToString();
            lblAge.Text = Sqldr["pdAge"].ToString();
            lblCasePaperNo.Text = Sqldr["pdCasePaperNo"].ToString();
            lblCassetteNo.Text = Sqldr["pdCassetteNo"].ToString();
            lblemail.Text = Sqldr["pdemail"].ToString();
            lblGender.Text = Sqldr["pdSex"].ToString();
            lblmob.Text = Sqldr["pdMob"].ToString();
            lbltele.Text = Sqldr["pdTele"].ToString();
            lblDoctorName.Text = Sqldr["DoctorName"].ToString();
            if (Sqldr["pdOccupation"] != DBNull.Value)
            {
                lblOccupation.Text = Sqldr["pdOccupation"].ToString();
            }

        }
        Sqldr.Close();
        SqlCmd.Dispose();
    }
}
