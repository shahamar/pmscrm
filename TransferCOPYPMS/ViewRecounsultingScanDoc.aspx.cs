﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewRecounsultingScanDoc : System.Web.UI.Page
{
    string RecNO = "";
    private string path = @"c:\PDF_Files\";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["RecNO"] != null)
        {
            RecNO = Request.QueryString["RecNO"].ToString();
            RecNO = RecNO.Replace("/", "_");
        }

        string str = "";
        str = "~/UploadReconsultingScanDocs/" + RecNO + ".PDF#toolbar=0";
        //Response.Redirect("995_46.PDF#toolbar=0");
        Response.Redirect(str);
    }
}