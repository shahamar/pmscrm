USE [PMS]
GO

/****** Object:  StoredProcedure [dbo].[sp_PftReport]    Script Date: 04/17/2013 15:42:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--[dbo].[sp_PftReport]'2013-03-01', '2013-04-17'
CREATE PROCEDURE [dbo].[sp_PftReport]      
@frmDate SMALLDATETIME      
,@toDate SMALLDATETIME      
AS      
BEGIN      
	DECLARE @TempDccg TABLE
		  (
			 itdTestDate SMALLDATETIME
			 ,Name VARCHAR(200)
			 ,itdcharges DECIMAL(18,2)
		  )
INSERT INTO @TempDccg
	SELECT    
		itdTestDate  
	   ,pdInitial + ' ' + pdFname + ' ' + pdMname +' ' + pdLname 'Name'   
   	   ,itdcharges   
	   --,ROW_NUMBER()OVER(PARTITION by  itdTestDate order by itdTestDate asc)as 'rank'  
	FROM      
	  tblIpdTestDetails itd    
	  inner  join    
	  tblIpdMaster i ON itd.itdipdId = i.ipdId     
	  inner  join    
	  tblPatientDetails pd ON i.ipdpdId = pd.pdID   
	WHERE    
	 itdtmId = 3 and itd.itdTestDate between @frmDate and @toDate
	 order by itdTestDate
	 
INSERT INTO @TempDccg
	  SELECT  ptd.ptdFromDate
			 ,pd.pdInitial + ' ' + pd.pdFname + ' ' + pd.pdMname +' ' + pd.pdLname 'Name'
			 ,ptd.ptdCharges
	  FROM 
		  tblPatientTestDetails ptd 
		  INNER JOIN 
		  tblTestMaster tm ON	ptd.ptdTMId=tm.tmId 
		  INNER JOIN 
		  tblPatientDetails pd ON PD.pdID=ptd.ptdId
		  
	  SELECT itdTestDate,Name,'PFT' AS 'test',itdcharges,ROW_NUMBER()OVER(PARTITION by  itdTestDate order by itdTestDate asc)as 'rank' FROM @TempDccg 
 End
GO


