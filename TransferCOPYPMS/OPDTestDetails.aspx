﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="OPDTestDetails.aspx.cs" Inherits="OPDTestDetails" Title="Test Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="Date.ascx" TagName="Date" TagPrefix="uc1" %>
<%@ Register Src="tab1.ascx" TagName="Tab" TagPrefix="tab1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <link href="cal/popcalendar.css" type="text/css" rel="stylesheet" />

    <script src="js/jquery-1.4.4.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript" src="js/actb.js"></script>

    <script type="text/javascript">
function CalcTotal()
 {

   var grid = document.getElementById("<%= GRV1.ClientID %>");
   var _totamt = 0.00,_charge=0.00;
   
  // alert("grid.rows.length="+grid.rows.length);
     
     for(i=1;i<grid.rows.length;i++)
   {
     var chk =false;
     //----------- check box ----------
     cell = grid.rows[i].cells[0];
     for(j=0;j<cell.childNodes.length;j++)
     {
        
        if(cell.childNodes[j].nodeName=="INPUT" && cell.childNodes[j].type=="checkbox")
        {       
            chk = cell.childNodes[j].checked ;      
        }
     }  
     
     if(chk==true)
     {
     
     //----------- Quantity ----------
     cell = grid.rows[i].cells[2];
     for(j=0;j<cell.childNodes.length;j++)
     {
        //if(cell.childNodes[j].nodeName=="INPUT")
      
        if(cell.childNodes[j].nodeName=="SPAN")
        {
            
            _charge = parseFloat(cell.childNodes[j].innerHTML);
            _totamt = _totamt + _charge;       
        }
     }
    }
   //alert("GTotal="+GTotal); 
   else
   {
      cell = grid.rows[i].cells[2];
      for(j=0;j<cell.childNodes.length;j++)
     {
        //if(cell.childNodes[j].nodeName=="INPUT")
      
        if(cell.childNodes[j].nodeName=="SPAN")
        {
            _charge = parseFloat(cell.childNodes[j].innerHTML);
        }
     } 
     
   }   
   }//chk==true    
   
   document.getElementById("<%= lblTotal.ClientID %>").innerHTML = _totamt;
 }

    </script>

    <script type="text/javascript" language="javascript">
    function toggleSelection(source) {
    //alert("toggleSelection");
        $("#ctl00_ContentPlaceHolder1_GRV1 input[id*='chkrow']").each(function (index) {
            $(this).attr('checked', source.checked);
        });
    } 
    
      function toggleSelection1(source) {
    //alert("toggleSelection");
        $("#ctl00_ContentPlaceHolder1_GRVSelected input[id*='cbSelAC']").each(function (index) {
            $(this).attr('checked', source.checked);
        });
    } 
    
    </script>

    <style>
        ._tableh1
        {
            background-image: url(    "images/tile_back1.gif" );
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
            height: 18px;
            padding: 8px 12px 8px 8px;
            text-align: center;
        }
    </style>

    <script type="text/javascript" language="javascript">
    function toggleSelection(source) {
    //alert("toggleSelection");
        $("#ctl00_ContentPlaceHolder1_GRV2 input[id*='cbAC']").each(function (index) {
            $(this).attr('checked', source.checked);
        });
    } 
    
      function toggleSelection1(source) {
    //alert("toggleSelection");
        $("#ctl00_ContentPlaceHolder1_GRVSelected input[id*='cbSelAC']").each(function (index) {
            $(this).attr('checked', source.checked);
        });
    } 
    
    </script>

    <script type="text/javascript" language="javascript">
function ChkValidate() {
var isValid = false;
var gridView = document.getElementById("ctl00_ContentPlaceHolder1_GRV2");
for (var i = 1; i < gridView.rows.length; i++) {
var inputs = gridView.rows[i].getElementsByTagName('input');
if (inputs != null) {
if (inputs[0].type == "checkbox") {
if (inputs[0].checked) {
isValid = true;
return true;
}
}
}
}
alert("Please select atleast one checkbox for Bill printing");
return false;
}
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                &nbsp;Patient Test Details</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto; border: 1.5px solid #E2E2E2;">
                                        <table style="width: 100%;" cellspacing="0px" cellpadding="2px">
                                            <tr>
                                                <td colspan="2">
                                                    <tab1:Tab ID="tabcon" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align:center;">
                                                    <asp:Label ID="lblMessage" runat="server" style="color:Red;"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="false"
                                                        OnPageIndexChanging="gridOPDTest_PageIndexChanging" CssClass="mGrid" Width="50%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Right" HeaderText="" ItemStyle-HorizontalAlign="Right"
                                                                ItemStyle-Width="5%">
                                                                <HeaderTemplate>
                                                                    <input id="cbAll" runat="server" onclick="toggleSelection(this)" type="checkbox"
                                                                        value="checked" onchange="CalcTotal()" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <input id="chkrow" runat="server" type="checkbox" onchange="CalcTotal()" value='<%# Eval("tmId") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="50%">
                                                                <ItemTemplate>
                                                                    <%# Eval("tmTestName")%>
                                                                    <asp:HiddenField ID="hidtmId" runat="server" Value='<%# Eval("tmId") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test Charges" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBaseCharges" runat="server" Text='<%# Eval("tmBaseCharges")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25%">
                                                                <ItemTemplate>
                                                                    <uc1:Date ID="tDate" runat="server" CalendarDate='<%# Bind("tDate") %>' DateFormat="dd/MM/yyyy" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div>
                                                        <table rules="all" cellspacing="0" border="1" style="width: 50%; border-collapse: collapse;"
                                                            class="mGrid">
                                                            <tr style="height: 10px;">
                                                                <td align="right" style="width: 5%;">
                                                                </td>
                                                                <td align="left" style="width: 50%;">
                                                                    Total
                                                                </td>
                                                                <td align="right" style="width: 20%;">
                                                                    <asp:Label ID="lblTotal" runat="server" Text="0.00"></asp:Label>
                                                                </td>
                                                                <td align="right" style="width: 25%;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>

                                             <tr>
                                              <td colspan="2">
                                                 Payment Mode *
                                              
                                                      <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="160px"
                                    CssClass="field">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                    <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                    <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                      <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                    <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                    <asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
                                </asp:DropDownList>

                                    

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                        Display="Dynamic" ErrorMessage="Please specify Payment Mode" ValidationGroup="check">
                                                        <span class="error" >Please specify Payment Mode</span></asp:RequiredFieldValidator>

                             
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                        Style="margin-top: 2px;" Text="Save" ValidationGroup="check" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Panel ID="pnlhis" runat="server">
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="_tableh1">
                                                                    HISTORY OF TESTS
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:GridView ID="GRV2" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                        CssClass="mGrid" Width="55%" OnRowCommand="GRV2_RowCommand" OnRowCancelingEdit="GRV2_RowCancelingEdit"
                                                        OnRowEditing="GRV2_RowEditing" OnRowUpdating="GRV2_RowUpdating">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <input id="cbAll" runat="server" onclick="toggleSelection(this)" type="checkbox" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <input id="cbAC" runat="server" type="checkbox" value='<%# Eval("ptdId") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="40%">
                                                                <ItemTemplate>
                                                                    <%# Eval("tmTestName")%>
                                                                    <asp:HiddenField ID="hidptdId" runat="server" Value='<%# Eval("ptdId") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test Charges" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBaseCharges2" runat="server" Text='<%# Eval("ptdCharges")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtBaseCharges2" runat="server" Text='<%# Eval("ptdCharges") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Test Date" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ltDate2" runat="server" Text='<%# Eval("ptdFromDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <uc1:Date ID="tDate2" runat="server" CalendarDate='<%# Bind("ptdFromDate") %>' DateFormat="dd/MM/yyyy" />
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("ptdId") %>'
                                                                        CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                    <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("ptdId") %>'
                                                                        CommandName="del" ImageUrl="Images/delete.ico" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete ?');" />
                                                                    <%--<a href="BillFormat.aspx?ptdID=<%# Eval("ptdId") %>" target="_blank">
                                                                        <img src="images/print1.png" /></a>--%>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:ImageButton ID="btnEditCancel" runat="server" CommandArgument='<%# Eval("ptdId") %>'
                                                                        CommandName="cancel" ImageUrl="images/cancel.ico" ToolTip="Cancel" />
                                                                    <asp:ImageButton ID="btnUpdate" runat="server" CommandArgument='<%# Eval("ptdId") %>'
                                                                        CommandName="update" ImageUrl="images/update.ico" ToolTip="Update" />
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>


                                           


                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnPrint" runat="server" Text="Print Bill" CssClass="textbutton b_submit"
                                                        OnClientClick="return ChkValidate()" onclick="btnPrint_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
