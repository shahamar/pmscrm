﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="ManageBill.aspx.cs" Inherits="ManageBill" Title="Manage Bill" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<style>
#tbsearch td { text-align: left; vertical-align: middle;}
#tbsearch label
{
    display: inline;
    margin-top: 3px;
    position: absolute;
}
#tbhead th
{
    text-align: left;
    background: url(images/nav-back.gif) repeat-x top;
    color: #FFFFFF;
    padding-left: 2px;
    padding-top: 3px;
}
#gvcol div{ margin-top: -10px;}
.head1{ width: 10%;}
.head2{ width: 23%;}
.head3{ width: 8%;}
.head4{ width: 13%;}
.head5{ width: 13%;}
.head5i{ width: 12%;}
.head5ii{ width: 14%;}
.head6{ width: 7%;}
.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}

.mGrid1 { 
    width: 100%; 
    background-color: #fff; 
    border: solid 1px #525252; 
    border-collapse:collapse; 
    
}
.mGrid1 td { 
    padding: 5px; 
    border: solid 1px #c1c1c1; 
    color: Black; 
	text-transform:uppercase    
}
.mGrid1 th { 
    padding: 8px 5px; 
    color: #595130;	
    background: #e9f7bc;
    border-left: solid 1px #525252; 
    font-size: 0.9em; 
	text-transform:uppercase;
	font-size:12px;
    
}
.mGrid1 .alt { background: #fcfcfc url(grd_alt.png) repeat-x top; }
.mGrid1 .pgr { background: url(../images/nav-back.gif) repeat-x top; }
.mGrid1 .pgr table { margin: 5px 0; }
.mGrid1 .pgr td { 
    border-width: 0; 
    padding: 0 6px; 
    border-left: solid 1px #666; 
    font-weight: bold; 
    color: #fff; 
    line-height: 12px; 
 }   
.mGrid1 .pgr a { color:#C1C1C1; text-decoration: none; }
.mGrid1 .pgr a:hover { color: #000; text-decoration: none; }
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
        <div class="login-area margin_ten_right" style="width: 100%; text-align:center">
            <h2>Manage Bills</h2>
            <div class="formmenu">
                <div class="loginform">
                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                        <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">
                            <tr>
                                <td>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <table style="width:100%;" cellspacing="5px" cellpadding="5px">
                                                    <tr>
                                                        <td style="width: 15%">
                                                            <asp:CheckBox ID="chkConsultingPeriod" runat="server" Text="&nbsp;<strong> Discharge Period :</strong>" />
                                                        </td>
                                                        <td style="width: 12%">
                                                            <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox" Width="75px"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                runat="server" Enabled="True" TargetControlID="txtfrm">
                                                            </cc1:CalendarExtender>&nbsp;
                                                            <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                        </td>
                                                        <td style="width: 4%"><strong>To :</strong></td>
                                                        <td style="width: 12%">
                                                            <asp:TextBox ID="txtto" runat="server" CssClass="textbox" Width="75px"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                            </cc1:CalendarExtender>&nbsp;
                                                            <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblGridHeader" runat="server" style="float:right; color:Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="mGrid" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                    <tr style="border: 1px solid #fff;">
                                                        <th class="head1">SR No</th>
                                                        <th class="head2">Name</th>
                                                        <th class="head3">Gender</th>
                                                        <th class="head5ii">Addmission Date</th>
                                                        <th class="head4"> Discharge Date</th>
                                                        <th class="head5">Case Paper No</th>
                                                        <th class="head5i" style="display:none">Cassette No</th>
                                                        <th class="head6">Action</th>
                                                    </tr> 
                                                    <tr>
                                                        <th class="head1">
                                                            <asp:TextBox ID="txtCardNo" runat="server" class="field" Width="90px" Visible="false"></asp:TextBox>
                                                        </th>
                                                        <th class="head2">
                                                            <asp:TextBox ID="txtname" runat="server" class="field" Width="250px"></asp:TextBox>
                                                        </th>
                                                        <th class="head3"></th>
                                                        <th class="head5ii"></th>
                                                        <th class="head4"></th>
                                                        <th class="head5">
                                                            <asp:TextBox ID="txtcasepaperno" runat="server" class="field" Width="125px"></asp:TextBox>
                                                        </th>
                                                        <th class="head5i"  style="display:none">
                                                            <asp:TextBox ID="txtcassetteno" runat="server" class="field" Width="125px"></asp:TextBox>
                                                        </th>
                                                        <th class="head6">
                                                            <div style="display: block;">
                                                                <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none; color: #FFF;
                                                                text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click">
                                                        		<img src="images/filter.png" style="border:0px;"/>Filter</asp:LinkButton>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="gvcol">
                                                <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                    CssClass="mGrid1" PageSize="20" ShowHeader="false" Width="100%"
                                                    OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("pdPatientICardNo")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="23%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("pdName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("pdSex")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="14%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("ipdAdmissionDate", "{0:dd-MMM-yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("ipdDischargeDate", "{0:dd-MMM-yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <%# Eval("pdCasePaperNo")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="center" Visible="false">
                                                            <ItemTemplate>
                                                                <%# Eval("pdCassetteNo")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgGenBill" runat="server" CommandArgument='<%# Eval("comargsId") %>'
                                                                    CommandName="bill" ImageUrl="Images/bill.ico" ToolTip="Select" />
                                                                <%# Eval("PrintBill") %>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </td>
</tr>
</table>
</asp:Content>
