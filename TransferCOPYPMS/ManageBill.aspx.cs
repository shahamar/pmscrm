﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class ManageBill : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole = "";
    string uid = "";
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx", false);

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 37";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {

            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            BindGVR();
        }
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }

    public void BindGVR()
    {
        try
        {
            string[] _frm = txtfrm.Text.ToString().Split('/');
            string[] _to = txtto.Text.ToString().Split('/');

            string frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            string to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";

            //strSQL = "Select pdID,Row_Number()over (order by ipdDischargeDate Desc) As pdPatientICardNo,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact'";
            //strSQL += " ,pdCasePaperNo,pdCassetteNo,IsNull(pdOccupation,'') pdOccupation,ipdAdmissionDate,ipdAdmissionTime,ipdDepositAmount,ipdDischargeDate ";
            //strSQL += ",ipdDischargeTime,ipdFinalDiagnosis,ipdIsDischarge,ipdProvisionalDiagnosis,CONVERT(VARCHAR,ipdId)+'-'+CONVERT(VARCHAR,pdId) As 'comargsId','<a title=''Print Discharge Summary'' href=''DischargeSummery.aspx?id='+CAST(ipdId AS VARCHAR)+'''& target=_blank><img src=''images/print1.png'' /></a>' AS 'PrintBill' ";
            //strSQL += " From tblPatientDetails pd INNER JOIN tblIpdMaster ipd ON pd.pdID = ipd.ipdpdId Where ipdIsDischarge = 1 AND IsCancel = 0 AND ipd.IsDelete = 0";

            strSQL = "Select pd.pdID,Row_Number()over (order by ipdDischargeDate Desc) As pdPatientICardNo,p.pdName, pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact'";
            strSQL += " ,pdCasePaperNo,pdCassetteNo,IsNull(pdOccupation,'') pdOccupation,ipdAdmissionDate,ipdAdmissionTime,ipdDepositAmount,ipdDischargeDate ";
            strSQL += ",ipdDischargeTime,ipdFinalDiagnosis,ipdIsDischarge,ipdProvisionalDiagnosis,CONVERT(VARCHAR,ipdId)+'-'+CONVERT(VARCHAR,pd.pdId) As 'comargsId','<a title=''Print Discharge Summary'' href=''NewDischargeSummary.aspx?id='+CAST(ipdId AS VARCHAR)+'''& target=_blank><img src=''images/print1.png'' /></a>' AS 'PrintBill' ";
            strSQL += " From tblPatientDetails pd INNER JOIN tblIpdMaster ipd ON pd.pdID = ipd.ipdpdId left join vwGetPatient p on pd.pdID=p.pdid Where ipdIsDischarge = 1 AND IsCancel = 0 AND ipd.IsDelete = 0";

            if (chkConsultingPeriod.Checked == true)
                strSQL += " AND ipdDischargeDate BETWEEN '" + frm + "' and '" + to + "'";
            if (txtCardNo.Text.ToString() != "")
                strSQL += " AND pdPatientICardNo Like '%" + txtCardNo.Text.ToString() + "%'";

            if (txtcasepaperno.Text.ToString() != "")
                strSQL += " AND pdCasePaperNo Like '%" + txtcasepaperno.Text.ToString() + "%'";

            if (txtcassetteno.Text.ToString() != "")
                strSQL += " AND pdCassetteNo Like '%" + txtcassetteno.Text.ToString() + "%'";

            if (txtname.Text.ToString() != "")
            {
                //strSQL += " AND pdFname+' '+ pdMname +' '+ pdLname like '%" + txtname.Text.ToString() + "%'";
                strSQL += " AND p.pdName like REPLACE('%" + txtname.Text.ToString() + "%', ' ' , '%')";
            }

            strSQL += " ORDER BY ipdDischargeDate DESC";

            DataTable dt = gen.getDataTable(strSQL);
            GRV1.DataSource = dt;
            GRV1.DataBind();
            createpaging(dt.Rows.Count, GRV1.PageCount);
        }
        catch (Exception ex)
        {

        }
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        BindGVR();
    }
    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "bill")
        {
            string comargsId = e.CommandArgument.ToString();
            string[] arr = comargsId.Split('-');
            string ipdid = arr[0];
            string pdid = arr[1];

            Session["BilledPatientID"] = pdid;
            Session["BilledipdId"] = ipdid;
            Response.Redirect("GenerateBill.aspx", false);
        }
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;
    }
}