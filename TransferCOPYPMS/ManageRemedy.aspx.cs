﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ManageRemedy : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    int userid;
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 9";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["flag"] == "1")
            lblMessage.Text = "Remedy details updated successfully";

        if (Request.QueryString["flag"] == "2")
            lblMessage.Text = "Remedy details deleted successfully";

        if (!IsPostBack)
        {
            BindGVR();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }
    public void BindGVR()
    {


        strSQL = "Select * From tblRemedyMaster where isDelete = 0 AND RMNAME != '0' ";

        if (txtRemedy.Text.ToString() != "")
            strSQL += " AND RMNAME Like '%" + txtRemedy.Text.ToString() + "%'";

        strSQL += " ORDER BY REPLACE(RMNAME,' ','') ASC ";
        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        createpaging(dt.Rows.Count, GRV1.PageCount);
    }
    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string pdId = e.CommandArgument.ToString();
        if (e.CommandName == "edt")
        {
            int rmId = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("RemedyMaster.aspx?RMId=" + rmId);
        }
        if (e.CommandName == "del")
        {
            int rmId = Convert.ToInt32(e.CommandArgument);
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "DeleteRemedyMaster";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@RMId", rmId);
            SqlCmd.Parameters.AddWithValue("@deletedBy", userid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            Response.Redirect("ManageRemedy.aspx?flag=2", false);
        }
        BindGVR();
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

}