﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;

public partial class tab : System.Web.UI.UserControl
{
    string pdId = "0";
    string strSQL = "";
    SqlDataReader Sqldr;
    SqlCommand SqlCmd;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    string msg;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CasePatientID"] != null)
        {
            pdId = Session["CasePatientID"].ToString();
        }
        else
        {
            Response.Redirect("Home.aspx?flag=1");
        }

        if (!IsPostBack)
        {
            FillData();                
        }
    }

    private void FillData()
    {
        //strSQL = "Select *,CONVERT(VARCHAR(10), pdDOB, 105) AS 'Birth',pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName' ,(select (dFName+' '+dMName+' '+dLName) AS DName From tblDoctorMaster WHERE did=dbo.tblPatientDetails.did)as DoctorName From dbo.tblPatientDetails where pdID=" + pdId;
        strSQL = "Select CONVERT(VARCHAR(10), pdDOB, 105) AS 'Birth',pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName',IsNull(DM.dFName,DMH.dFName) + ' ' + IsNull(DM.dMName,DMH.dMName) + ' ' + isNull(DM.dLName,DMH.dLName) AS DoctorName , * From tblPatientDetails PD Left Join tblDoctorMaster DM On PD.did = DM.did Left Join tblDoctorHistoryDetails DMH On PD.did = DMH.did where PD.pdID=" + pdId;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            lblPatientName.Text = Sqldr["pdName"].ToString();
            lblPatientId.Text = Sqldr["pdPatientICardNo"].ToString();
            lblAge.Text = Sqldr["pdAge"].ToString();
            lblCasePaperNo.Text = Sqldr["pdCasePaperNo"].ToString();
            lblCassetteNo.Text = Sqldr["pdCassetteNo"].ToString();
            lblemail.Text = Sqldr["pdemail"].ToString();
            lblGender.Text = Sqldr["pdSex"].ToString();
            lblmob.Text = Sqldr["pdMob"].ToString();
            lbltele.Text = Sqldr["pdTele"].ToString();
            lblDoctorName.Text = Sqldr["DoctorName"].ToString();
            if (Sqldr["pdOccupation"] != DBNull.Value)
            {
                lblOccupation.Text = Sqldr["pdOccupation"].ToString();
            }
            lblDOB.Text = Sqldr["Birth"].ToString();
            lblBlood.Text = Sqldr["pdBloodGroup"].ToString();
        }
        Sqldr.Close();
        SqlCmd.Dispose();

       // int cnt = Convert.ToInt32(gen.executeScalar("Select Count(*) From tblFollowUpDatails Where fdPatientId=" + pdId).ToString());
        int cnt = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from vwConsultingHistoryNewAks Where fdPatientId=" + pdId).ToString());

        if (cnt > 0)
        {
            lnkrdirect.Text = "Follow Up";
            lnkrdirect.PostBackUrl = "FollowupDetail.aspx";
            lnkHistory.Enabled = true;
            lnkHistory.PostBackUrl = "FollowupHistory.aspx";
        }
        else
        {
            lnkrdirect.Text = "Consult";
            lnkrdirect.PostBackUrl = "ConsultPatient.aspx";
            lnkHistory.Enabled = false;
            lnkHistory.ForeColor = Color.Black;

        }

        lblIPDStatus.Text = gen.executeScalar("Select [dbo].[IsIPDStatus](" + pdId + ")").ToString(); 
    }


}
