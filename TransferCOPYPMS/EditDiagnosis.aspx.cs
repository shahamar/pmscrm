﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;


public partial class EditDiagnosis : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    //string pdId = "";
    string uid = "";
    //string ipdid = "";
    //added by nikita on 12th march 2015------------------------------
    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            else
            {
                Session["ipdid"] = gen.executeScalar("Select ipdId From tblIpdMaster Where ipdpdId =" + PdID).ToString();
                ViewState["ipdid"] = Session["ipdid"];
            }

            FillData();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        strSQL = "EditDiagnosis";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@ProvisionalDiagnosis", txtProvisionalDiagnosis.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@FinalDiagnosis", txtFinalDiagnosis.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@Operation", txtOperation.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
        SqlCmd.Parameters.AddWithValue("@ReconsultingDiagnosis", txtReconsultingDiagnosis.Text.ToString());

        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd.ExecuteNonQuery();
        con.Close();
        SqlCmd.Dispose();
        con.Dispose();
        Response.Redirect("IPDView.aspx", false);
    }

    public void FillData()
    {
        strSQL = "Select * From tblIpdMaster Where ipdId=" + ipdid;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            txtProvisionalDiagnosis.Text = Sqldr["ipdProvisionalDiagnosis"].ToString();
            txtFinalDiagnosis.Text = Sqldr["ipdFinalDiagnosis"].ToString();
            txtOperation.Text = Sqldr["ipdOperation"].ToString();
            txtReconsultingDiagnosis.Text = Sqldr["ReconsultingDiagnosis"].ToString();
        }

        Sqldr.Close();
        Sqldr.Dispose();
    }
}
