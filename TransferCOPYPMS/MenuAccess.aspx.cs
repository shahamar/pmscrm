﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class MenuAccess : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string strSQL;
    DataTable dt;
    DataSet ds;
    string mode;
    int error;
    SqlDataAdapter dapt;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["userrole"] != null)
            lblurole.Text = Request.QueryString["userrole"].ToString();

        if (Request.QueryString["username"] != null)
            lbluname.Text = Request.QueryString["username"].ToString();

        if (Request.QueryString["_userid"] != null)
            hiduid.Value = Request.QueryString["_userid"].ToString();

        if (Request.QueryString["mode"] != null)
            mode = Request.QueryString["mode"].ToString();

        if (!IsPostBack)
        {
            bindDataTable();
            buildNodes(null, null);
            FillCheckItems();
        }
    }

    private void bindDataTable()
    {
        dt = new DataTable();
        SqlCmd = new SqlCommand();
        SqlCmd.CommandText = "Select * from Menu";
        SqlCmd.CommandType = CommandType.Text;
        SqlCmd.Connection = con;
        SqlDataAdapter dapt = new SqlDataAdapter(SqlCmd);
        
        dapt.Fill(dt);
    }

    private void buildNodes(TreeNode n, int? parentid)
    {
        //dt = new DataTable();
        DataRow[] drCategories;
        if (parentid == null)
            drCategories = dt.Select("NodeLevel=0"); //This will get the Root Node
        else
            drCategories = dt.Select("ParentID='" + parentid + "'");

        foreach (DataRow dr in drCategories)
        {                       
            TreeNode t = new TreeNode();
            t.Text = dr["Text"].ToString();
            t.Value = dr["Menu_ID"].ToString();

            buildNodes(t, Convert.ToInt32(dr["Menu_ID"].ToString()));//calling the function recursively
            if (n == null)//if it is a root node
                trvwMenuAccess.Nodes.Add(t);
            else
                n.ChildNodes.Add(t);
        }
    }
    public string getXmlString()
    {

        string xmlString = "<ROOT>";

        foreach (TreeNode node in trvwMenuAccess.Nodes)
        {
            if (node.ChildNodes.Count > 0)
            {
                foreach (TreeNode lnd in node.ChildNodes)
                {
                    //if (lnd.Checked == true)
                    //{
                    if (lnd.ChildNodes.Count > 0)
                    {
                        foreach (TreeNode tn in lnd.ChildNodes)
                        {
                            if (tn.Checked == true)
                            {
                                xmlString = xmlString + "<menu Menu_ID='" + tn.Value.ToString() + "'/>";
                            }
                        }
                    }
                    else if (lnd.Checked)
                        xmlString = xmlString + "<menu Menu_ID='" + lnd.Value.ToString() + "'/>";
                    // }
                }
            }
            else if (node.Checked)
                xmlString = xmlString + "<menu Menu_ID='" + node.Value.ToString() + "'/>";

        }
        //foreach (TreeNode nd in trvwMenuAccess. CheckedNodes)
        //    xmlString = xmlString + "<menu Menu_ID='" + nd.Value.ToString() + "'/>";


        xmlString = xmlString + "</ROOT>";

        return xmlString;

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string xmlString = getXmlString();
        SqlCmd = new SqlCommand();
        SqlCmd.CommandText = "sp_UpdateMenuPrivilage";
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Connection = con;
        SqlCmd.Parameters.AddWithValue("@userid", Convert.ToInt32(hiduid.Value.ToString()));
        SqlCmd.Parameters.AddWithValue("@xmlString", xmlString);
        SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd.ExecuteNonQuery();

        error = Convert.ToInt32(SqlCmd.Parameters["@Error"].Value.ToString());

        if (error == 0)
        {
            if (mode == "new")
                Response.Redirect("AddUser.aspx?status=1");
            else
                Response.Redirect("ManageUser.aspx?status=2");
        }
    }

    private void FillCheckItems()
    {
        if (hiduid.Value.ToString() != "")
        {
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "sp_tblMenuUserSel";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Connection = con;
            SqlCmd.Parameters.AddWithValue("@userid", Convert.ToInt32(hiduid.Value.ToString()));
            dapt = new SqlDataAdapter(SqlCmd);
            if (con.State == ConnectionState.Closed)
                con.Open();
            ds = new DataSet();
            dapt.Fill(ds, "Table");

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        foreach (TreeNode nd in trvwMenuAccess.Nodes)
                        {
                            if (nd.ChildNodes.Count > 0)
                            {
                                foreach (TreeNode cnd in nd.ChildNodes)
                                {
                                    if (cnd.ChildNodes.Count > 0)
                                    {
                                        foreach (TreeNode lnd in cnd.ChildNodes)
                                        {
                                            if (lnd.Value.ToString() == ds.Tables[0].Rows[i]["Menu_ID"].ToString())
                                                lnd.Checked = true;
                                        }
                                    }
                                    else
                                    {
                                        if (cnd.Value.ToString() == ds.Tables[0].Rows[i]["Menu_ID"].ToString())
                                            cnd.Checked = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}