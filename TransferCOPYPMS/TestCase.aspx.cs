﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class TestCase : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlCommand cmd;
    SqlDataAdapter da;
    SqlDataReader dr;
    genral gen = new genral();
    string query="";
    string pdId = "";
    string kco = "";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnview_Click(object sender, EventArgs e)
    {

        if (con.State == ConnectionState.Closed)
            con.Open();

        //query = "Select pdId,_KCO FROM (Select pdId,kco,CONVERT(VARCHAR(8000),KCO) _KCO";
        //query += " From dbo.tblOldPatientData opd INNER JOIN dbo.tblPatientDetails pd ON opd.Case_Paper_No = pd.pdCasePaperNo";
        //query += " Where CONVERT(VARCHAR(8000),KCO) <> '') A";
        //query += " Where _KCO NOT Like '%C/C%'"; // ONLY KCO    

        query = "Select pdCasePaperNo,pdId,_KCO FROM (Select pdCasePaperNo,pdId,kco,REPLACE(CONVERT(VARCHAR(8000),KCO),'C/C','~') _KCO";
        query += " From dbo.tblOldPatientData opd INNER JOIN dbo.tblPatientDetails pd ON opd.Case_Paper_No = pd.pdCasePaperNo";
        query += " Where CONVERT(VARCHAR(8000),KCO) <> '') A";
        query += " Where _KCO Like '%~%'";
        

        cmd = new SqlCommand(query, con);
        dr = cmd.ExecuteReader();
                
        while (dr.Read())
        {
            pdId = dr["pdId"].ToString();
            kco = dr["_KCO"].ToString();

            string[] _kco = kco.Split('~');
            string kco1 = _kco[0];
            string chief = _kco[1];
            chief = chief.Replace("\n", "<br />");
            kco1 = kco1.Replace("\n", "<br />");

            //kco = kco.Replace("\n", "<br />");
    
            if (File.Exists(Server.MapPath("Files/KCO/kco_" + pdId)))
            {
                File.Delete(Server.MapPath("Files/KCO/kco_" + pdId));
                
            }
            File.WriteAllText(Server.MapPath("Files/KCO/kco_" + pdId), kco1);

            //Response.Redirect("Investigation.aspx");

            if (File.Exists(Server.MapPath("Files/ChiefCo/chief_" + pdId)))
            {
                File.Delete(Server.MapPath("Files/ChiefCo/chief_" + pdId));

            }
            File.WriteAllText(Server.MapPath("Files/ChiefCo/chief_" + pdId), chief);


        }
        con.Close();
        dr.Close();
        dr.Dispose();
    }
    protected void delete_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();

        query = "Select pdId From tblPatientDetails";


                cmd = new SqlCommand(query, con);
        dr = cmd.ExecuteReader();

        while (dr.Read())
        {
            pdId = dr["pdId"].ToString();

            if (File.Exists(Server.MapPath("Files/KCO/kco_" + pdId)))
            {
                File.Delete(Server.MapPath("Files/KCO/kco_" + pdId));
            }

            if (File.Exists(Server.MapPath("Files/ChiefCo/chief_" + pdId)))
            {
                File.Delete(Server.MapPath("Files/ChiefCo/chief_" + pdId));
            }
            
        }
    }
}
