﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class ViewAppPatientDetails : System.Web.UI.Page
{
    int pdID = 0;
    string uid = "";
    genral gen = new genral();
    string strSQL = "";
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlCommand SqlCmd;
    SqlDataReader Sqldr;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["pdID"] != null)
            pdID = Convert.ToInt32(Request.QueryString["pdID"]);

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 49";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            Fill();
        }
    }
    public void Fill()
    {
        try
        {
            if (con.State == ConnectionState.Closed)
                con.Open();

            strSQL = "SELECT *,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName',(select (dFName+' '+dMName+' '+dLName) AS DName From tblDoctorMaster WHERE did=dbo.tblPatientDetails.did)as DoctorName FROM tblPatientDetails WHERE pdID = " + pdID + "";
            SqlCmd = new SqlCommand(strSQL, con);
            Sqldr = SqlCmd.ExecuteReader();
            while (Sqldr.Read())
            {
                //if (File.Exists(Server.MapPath("Files/KCO/kco_" + pdID)))
                //{
                //    lblkco.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + pdID).ToString());
                //}
                
                //if (File.Exists(Server.MapPath("Files/Investigation/inv_" + pdID)))
                //{
                //    lblInvestigation.Text = File.ReadAllText(Server.MapPath("Files/Investigation/inv_" + pdID).ToString());
                //}
                
                //if (File.Exists(Server.MapPath("Files/ChiefCo/cc_" + pdID)))
                //{
                //    lblchiefco.Text = File.ReadAllText(Server.MapPath("Files/ChiefCo/cc_" + pdID).ToString());
                //}
                
                //if (File.Exists(Server.MapPath("Files/PastHo/pho_" + pdID)))
                //{
                //    lblpastho.Text = File.ReadAllText(Server.MapPath("Files/PastHo/pho_" + pdID).ToString());
                //}
                
                //if (File.Exists(Server.MapPath("Files/FamilyHo/fho_" + pdID)))
                //{
                //    lblfamilyho.Text = File.ReadAllText(Server.MapPath("Files/FamilyHo/fho_" + pdID).ToString());
                //}
                
                //if (File.Exists(Server.MapPath("Files/PhysicalGenerals/phy_" + pdID)))
                //{
                //    lblpg.Text = File.ReadAllText(Server.MapPath("Files/PhysicalGenerals/phy_" + pdID).ToString());
                //}
                
                //if (File.Exists(Server.MapPath("Files/Mind/mnd_" + pdID)))
                //{
                //    lblmind.Text = File.ReadAllText(Server.MapPath("Files/Mind/mnd_" + pdID).ToString());
                //}
                
                //if (File.Exists(Server.MapPath("Files/AF/af_" + pdID)))
                //{
                //    lblAF.Text = File.ReadAllText(Server.MapPath("Files/AF/af_" + pdID).ToString());
                //}
                
                //if (File.Exists(Server.MapPath("Files/Thermal/thermal_" + pdID)))
                //{
                //    lblThermal.Text = File.ReadAllText(Server.MapPath("Files/Thermal/thermal_" + pdID).ToString());
                //}


                if (File.Exists(Server.MapPath("Files/KCO/kco_" + pdID)))
                {
                    lblkco.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + pdID).ToString());
                    
                }
                if (File.Exists(Server.MapPath("Files/Investigation/inv_" + pdID)))
                {
                    lblInvestigation.Text = File.ReadAllText(Server.MapPath("Files/Investigation/inv_" + pdID).ToString());
                    
                }
                if (File.Exists(Server.MapPath("Files/ChiefCo/chief_" + pdID)))
                {
                    lblchiefco.Text = File.ReadAllText(Server.MapPath("Files/ChiefCo/chief_" + pdID).ToString());
                    
                }
                if (File.Exists(Server.MapPath("Files/PastHo/pho_" + pdID)))
                {
                    lblpastho.Text = File.ReadAllText(Server.MapPath("Files/PastHo/pho_" + pdID).ToString());
                    
                }
                if (File.Exists(Server.MapPath("Files/FamilyHo/fho_" + pdID)))
                {
                    lblfamilyho.Text = File.ReadAllText(Server.MapPath("Files/FamilyHo/fho_" + pdID).ToString());
                    
                }
                if (File.Exists(Server.MapPath("Files/Mind/mnd_" + pdID)))
                {
                    lblmind.Text = File.ReadAllText(Server.MapPath("Files/Mind/mnd_" + pdID).ToString());
                    
                }
                if (File.Exists(Server.MapPath("Files/AF/af_" + pdID)))
                {
                    lblAF.Text = File.ReadAllText(Server.MapPath("Files/AF/af_" + pdID).ToString());
                    
                }

                if (File.Exists(Server.MapPath("Files/Thermal/thermal_" + pdID)))
                {
                    lblThermal.Text = File.ReadAllText(Server.MapPath("Files/Thermal/thermal_" + pdID).ToString());
                    
                }



                lblPatientName.Text = Sqldr["pdName"].ToString();
                lblPatientId.Text = Sqldr["pdPatientICardNo"].ToString();
                lblAge.Text = Sqldr["pdAge"].ToString();
                lblCasePaperNo.Text = Sqldr["pdCasePaperNo"].ToString();
                lblCassetteNo.Text = Sqldr["pdCassetteNo"].ToString();
                lblemail.Text = Sqldr["pdemail"].ToString();
                lblGender.Text = Sqldr["pdSex"].ToString();
                lblmob.Text = Sqldr["pdMob"].ToString();
                lbltele.Text = Sqldr["pdTele"].ToString();
                lblDoctorName.Text = Sqldr["DoctorName"].ToString();
                if (Sqldr["pdOccupation"] != DBNull.Value)
                {
                    lblOccupation.Text = Sqldr["pdOccupation"].ToString();
                }
                
            }
        }
        catch (Exception ex)
        {
            
        }
    }
    protected void lnkHistory_Click(object sender, EventArgs e)
    {
        Session["rptPdId"] = pdID;
        Response.Redirect("PatientReport.aspx");
    }
}
