﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="UpdateCourierDetails.aspx.cs" Inherits="UpdateCourierDetails" Title="Update Courier Details" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<script type="text/javascript">
        function Validate()
        {
            
              
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width:100%; background-color:#FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                        <h2>Courier Details</h2>
                        <div class="formmenu">
                                              
                            
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel" style="height:auto;">
                            <table style="width: 100%;padding-left:20px" cellspacing="0px" cellpadding="0px">
                                    <tr>
                                        <td colspan="2" style="text-align:center; color:Red;">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align:center; color:Red;">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblmsg" runat= "server" Visible="false"></asp:Label>
                                            </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Receiver Name*</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                             
                                    <asp:TextBox id="txtCName" runat ="server" Width="400px"></asp:TextBox>
                                    
                                            <asp:RequiredFieldValidator ID="valFname" runat="server" 
                                                ControlToValidate="txtCName" Display="Dynamic" 
                                                ErrorMessage="Courier Name is required."><span class="error">Courier Name is 
                                            required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            
                                            &nbsp;</td>
                                    </tr>
                                    <%--<tr>
                                        <td>
                                            Date Of Birth*
                                         </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtDOB" runat="server" CssClass="field" Width="70px"
                                            onblur="CalculateAge(this)" onchange="CalculateAge(this)"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtDOB_CalendarExtender" runat="server" 
                                                Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgDOB" 
                                                TargetControlID="txtDOB">
                                            </cc1:CalendarExtender>
                                            <asp:Image ID="imgDOB" runat="server" 
                                                ImageUrl="~/images/calimg.gif" style="position: absolute; width:20px; height:20px;"/>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>--%>
                                    <tr>
                                        <td>
                                            Address*</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                    <td>
                                          <asp:TextBox ID="txtCAddress" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
                                          </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            Mobile No*</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <asp:TextBox id="txtCMobileNo" runat ="server" Width="200px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="txtCMobileNo" Display="Dynamic" 
                                                ErrorMessage="Mobile No is required."><span class="error">Mobile No is 
                                            required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            Email ID</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                           
                                    <asp:TextBox id="txtcEmailID" runat ="server" Width="200px"></asp:TextBox>
                                            
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                  
                                    
                                   <tr>
                                        <td>
                                            Courier Name</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                              <asp:DropDownList ID="DDCourier" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                         </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                    
                                    
                                     <tr>
                                        <td>
                                            Courier Date</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:TextBox ID="txtCourierDate" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgcfollow" 
                                                TargetControlID="txtCourierDate">
                                            </cc1:CalendarExtender>
                                            <asp:Image ID="imgcfollow" runat="server" ImageUrl="~/images/calimg.gif" 
                                                style="position: absolute; width:20px; height:20px;" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                    
                                     <tr>
                                        <td>
                                              DocketNo</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <asp:TextBox id="txtCDocketNo" runat ="server" Width="200px"></asp:TextBox>
                                             <asp:HiddenField ID="hidDocketNo" runat="server" />
                                         </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                    
                                    
                                     <tr>
                                        <td>
                                              Charges</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                         <asp:TextBox id="txtCCharges" runat ="server" Width="200px" Enabled="false"></asp:TextBox>
                                         </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" 
                                                onclick="btnSave_Click" style="margin-top: 2px;" Text="Save" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                
                            </div>    
                                
                                 
                                <div class="clr"></div>
                                </div>               
                         </div>
            </td>
        </tr>
        
        <tr>
            <td>
            </td>
        </tr>
    </table>
        </ContentTemplate>
    </asp:UpdatePanel> 

</asp:Content>



