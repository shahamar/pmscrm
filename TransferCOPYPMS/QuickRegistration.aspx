﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="QuickRegistration.aspx.cs" Inherits="QuickRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

  <center>
        <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <div class="login-area margin_ten_right" style="width: 100%;">
                      	<span style="text-align:center;">
                        	<h2>Patient Registration</h2>
                        </span>
                        <div class="formmenu">
                            <div class="loginform">

                             <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">

                                <table style="width:100%; background:#f2f2f2; padding-left:20px" cellspacing="0px" cellpadding="5px">

                                        <tr>
                                            <td colspan="5" style="text-align: center; color: Red;">
                                               <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>

                                          <tr>
                                            <td>First Name *</td>
                                            <td>Middle Name</td>
                                            <td>Last Name*</td>
                                          
                                       </tr>



                                        <tr>

                                          <td>
                                          
                                                <asp:TextBox ID="txtfname" runat="server" CssClass="field required" MaxLength="50" 
                                                Style="float: left; text-transform:uppercase;"></asp:TextBox> <br />                                                 
                                                <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="txtfname"
                                                    Display="Dynamic" ErrorMessage="First Name is required." ValidationGroup="val">
                                                    <span class="error">Please specify First Name.</span>
                                                </asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                            <asp:TextBox ID="txtmname" runat="server" CssClass="field" MaxLength="50" Style="text-transform:uppercase;"></asp:TextBox>
                                            </td>
                                            <td>
                                             <asp:TextBox ID="txtlname" runat="server" CssClass="field" MaxLength="50" Style="float: left;  text-transform:uppercase;">
                                                </asp:TextBox><br /><br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtlname"
                                                    Display="Dynamic" ErrorMessage="Last Name is required." ValidationGroup="val">
                                                    <span class="error">Please specify Last Name.</span>
                                                </asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="reg_age" runat="server" CssClass="field required"
                                                    ControlToValidate="txtlname" Display="Dynamic" ErrorMessage="Please enter valid Last Name"
                                                    ValidationExpression="^[a-zA-Z0-9 .\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid Last Name!</span>
                                                </asp:RegularExpressionValidator>
                                            </td>

                                        </tr>

                                       <tr>
                                            <td>Case Paper No*</td>
                                            <td>Mobile No.*</td>
                                            <td></td>
                                            
                                       </tr>

                                       <tr>
                                       <td>
                                       <asp:TextBox ID="txtCasePaperNo" runat="server" CssClass="field" MaxLength="20"  Style="text-transform:uppercase;">
                                                </asp:TextBox><br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCasePaperNo"
                                                    ValidationGroup="val" Display="Dynamic" ErrorMessage="Case Paper No!" CssClass="field required">
                                                    <span class="error">Please specify Consulting Case Paper No</span>
                                                </asp:RequiredFieldValidator>
                                       </td>

                                       <td>
                                         <asp:TextBox ID="txtmob" runat="server" CssClass="field" MaxLength="10" Width="140px"></asp:TextBox>&nbsp;
                                               <img src="images/mobile_phone.png" /><br />
                                               <asp:RegularExpressionValidator ID="regtele" runat="server" Display="Dynamic" CssClass="field required"
                                                   ControlToValidate="txtmob" ErrorMessage="Please enter valid telephone number!"
                                                   ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val">
                                                   <span class="error">Please enter valid telephone number!</span>
                                               </asp:RegularExpressionValidator>
    
                                               <asp:RegularExpressionValidator ID="regmob" runat="server" Display="Dynamic" CssClass="field required"
                                                   Style="padding-left: 140px;" ControlToValidate="txtmob" ErrorMessage="Please enter valid mobile number!"
                                                   ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val">
                                                   <span class="error">Please enter valid mobile number!</span>
                                               </asp:RegularExpressionValidator>
    
                                               <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtmob"
                                                   Display="Dynamic" ErrorMessage="Please specify Contact No." ValidationGroup="val">
                                                   <span class="error" >Please specify Contact No.</span>
                                               </asp:RequiredFieldValidator>
                                       </td>
                                        <td></td>
                                       </tr>

                                       <tr>
                                       <td colspan="3" align="center">
                                         <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click" 
                                          Style="margin-top: 2px;" Text="Save" ValidationGroup="val" />
                                       </td>
                                       </tr>

                                </table>

                             </div>


                             <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            
        </table>
    </center>


</asp:Content>

