﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditDiagnosis.aspx.cs" Inherits="EditDiagnosis" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Diagnosis</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
    
    
    <style type="text/css">
    
    .b_submit {
    background: url("images/b_submit.gif") no-repeat scroll 0 0 transparent;
    border: 0 none;
    display: inline-block;
    font-family: arial;
    vertical-align: middle;
    width: 67px;
    height:32px; 
}

.textbutton 
{
    color: #FFFFFF !important;
    font-size: 12px;
    font-weight: bold;
    text-align: center;
    margin-top: -8px;
}
.loginform 
{
	font-family:Verdana; 
}

.left
{
	float:left;
}

.formmenu 
{
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #E6E6E6;
    margin-bottom: 10px;
    padding: 10px;
}
.tableh1 {
    background-image: url("images/tile_back1.gif");
    border-bottom: 1px solid #CED2D6;
    border-left: 1px solid #FFFFFF;
    border-right: 1px solid #FFFFFF;
    color: #606F79;
    font-size: 10px;
    height: 18px;
    padding: 8px 12px 8px 8px;
    font-family: verdana;
    font-weight: bold;
}

textarea {
     border: 1px solid #A5ACB2;
    font-family: verdana;
    font-size: 12px;
    padding: 5px;
    width: 71%;
}

    </style>
</head>
<body style="background-color:#FFFFFF;">
    <form id="form1" runat="server">
    <div>
        <table style="width:100%; background-color:#FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                        <h2>Edit Diagnosis</h2>
                        
                        <div class="formmenu">
                            <div class="loginform">
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel" style="height:auto;border:1.5px solid #E2E2E2;">
                                    <div style="padding:15px; height:auto;">
                                        <table style="width: 100%; border: 1px solid #EFEFEF;">
                                            <tr>
                                                 <td>
                                                               
                                                     Provisional Diagnosis :</td>
                                             </tr>   
                                            <tr>
                                                 <td>
                                                               
                                            <asp:TextBox ID="txtProvisionalDiagnosis" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                               
                                                 </td>
                                             </tr>   
                                            <tr>
                                                 <td>
                                                               
                                                     Final Diagnosis :</td>
                                             </tr>   
                                            <tr>
                                                 <td>
                                                               
                                            <asp:TextBox ID="txtFinalDiagnosis" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                               
                                                 </td>
                                             </tr>   
                                              <tr>
                                                 <td>
                                                               
                                                     Reconsulting Diagnosis :</td>
                                             </tr>   
                                            <tr>
                                                 <td>
                                                               
                                            <asp:TextBox ID="txtReconsultingDiagnosis" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                               
                                                 </td>
                                             </tr>   
                                            <tr>
                                                 <td>
                                                               
                                                     Operation :</td>
                                             </tr>   
                                            <tr>
                                                 <td>
                                                               
                                            <asp:TextBox ID="txtOperation" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                               
                                                 </td>
                                             </tr>   
                                            <tr>
                                                 <td>
                                                               
                                                     <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" 
                                                         onclick="btnSave_Click" style="margin-top: 2px;" Text="Save" />
                                                 </td>
                                             </tr>   
                                            </table>           
                                    </div>
                                </div>
                            <div class="clr"></div>
                        </div>               
                        </div>
                    </div>
            </td>
        </tr>
        </table>    
    </div>
    </form>
</body>
</html>
