﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="FollowupHistory.aspx.cs" Inherits="FollowupHistory" Title="History" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="tab1.ascx" TagName="Tab" TagPrefix="tab1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <style type="text/css">
        ._tableh1
        {
            background-image: url("images/tile_back1.gif");
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
            height: 18px;
            padding: 8px 12px 8px 8px;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        Patient&#39;s Consulting History</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto; border: 1.5px solid #E2E2E2;">
                                <table style="width: 100%;" cellspacing="0px" cellpadding="2px">
                                    <tr>
                                        <td colspan="2">
                                            <tab1:Tab ID="tabcon" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" Width="100%" OnRowCommand="GRV1_RowCommand"
                                                OnRowDataBound="GRV1_RowDataBound" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderText="Date">
                                                        <ItemTemplate>
                                                            <%# Eval("fdDate","{0:dd-MMM-yyyy}")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderText="Reconsulting/Acute">
                                                        <ItemTemplate>
                                                            <%# Eval("fdReconsulting")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Left" HeaderText="Casstte No">
                                                        <ItemTemplate>
                                                            <%# Eval("fdCasseteNo")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Remedy">
                                                        <ItemTemplate>
                                                            <%# Eval("fdRemedy")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Remedy Period">
                                                        <ItemTemplate>
                                                            <%# Eval("fdRemedyPeriod")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%" HeaderText="Dose">
                                                        <ItemTemplate>
                                                            <%# Eval("fdDoseType")%>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

						    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%" HeaderText="Add Emg">
							<ItemTemplate>
								<%# Eval("fdAddEmgAmt") %>
								<ItemStyle HorizontalAlign="Center" />
							</ItemTemplate>
						    </asp:TemplateField>
	
						    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%" HeaderText="Extra Dose">
							<ItemTemplate>
								<%# Eval("ExtraDosAmt") %>
								<ItemStyle HorizontalAlign="Center" />
							</ItemTemplate>
						    </asp:TemplateField>

                                                       <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%" HeaderText="Rec. No" Visible="false">
                                                        <ItemTemplate>
                                                            <%# Eval("ReconsultingNo")%>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField> --%>

                                                     <asp:TemplateField ItemStyle-Width="0%" ItemStyle-HorizontalAlign="Left" HeaderText="" Visible="false">
                                                        <ItemTemplate>
                                                             <asp:label runat="server" ID="lblISRec" Text='<%#Eval("fdIsReconsulting")%>' Visible="false"></asp:label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%" HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("Flag") %>'
                                                                CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                            <asp:ImageButton ID="btnCase" runat="server" CausesValidation="false" CommandArgument='<%# Eval("fdId") %>'
                                                                CommandName="case" ToolTip="Add Reconsulting Details" ImageUrl="~/images/document_add.png"
                                                                Style='<%# "display:" + DataBinder.Eval(Container.DataItem, "DEL") + ";" %>' />
							    <asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("fdId") %>'
                                                                        CausesValidation="false" CommandName="del" ToolTip="Delete" ImageUrl="~/images/delete.ico"
                                                                        Style='<%# "display:" + DataBinder.Eval(Container.DataItem, "DEL") + ";" %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Panel ID="pnlremedy" runat="server" Visible="False">
                                                <table width="100%">
                                                    <tr>
                                                        <td class="_tableh1">
                                                            HISTORY OF IPD REMEDY
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:GridView ID="GRV3" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                                CssClass="mGrid" Width="60%">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Remedy" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="50%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblirRemedy" runat="server" Text='<%# Eval("irRemedy")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblirStartDate" runat="server" Text='<%# Eval("irStartDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--<asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Left" 
                                                                        ItemStyle-Width="20%">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblirEndDate" runat="server" Text='<%# Eval("irEndDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
