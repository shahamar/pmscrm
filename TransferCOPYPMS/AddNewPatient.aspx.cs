﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
//using System.Windows.Forms;

public partial class AddNewPatient : System.Web.UI.Page
{
    SqlCommand SqlCmd, SqlCmd1;
    SqlDataAdapter da;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection con1 = new SqlConnection(ConfigurationManager.AppSettings["constringOnline"]);
    string userid;
    string userrole;
    string strSQL, StrSQL, strSQL1 ;
    int ADID = 0;
    int OLID = 0;
    //int pdID = 0;
    //added by nikita on 12th march 2015-------------------------
    
    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //----------------------------------------------------------------
    SqlDataReader Sqldr;
    string flag;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["flag"] != null)
            flag = Request.QueryString["flag"].ToString();

        if (flag == "new")
            Session["edtPdId"] = null;

        if (Session["IcardNo"] != null)
        {
            lblMessage.Text = "Patient Registered With ID CARD No." + Session["IcardNo"].ToString() + "";
            Session["IcardNo"] = null;
        }

        if (Request.QueryString["ADID"] != null)
            ADID = Convert.ToInt32(Request.QueryString["ADID"]);

        if (Request.QueryString["OLID"] != null)
            OLID = Convert.ToInt32(Request.QueryString["OLID"]);

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 11";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();
     

        
        if (!IsPostBack)
        {
            //---Added By Nikita On 12th March 2015----------------------------------------------------
            if (Session["edtPdId"] != null)
                ViewState["pdId"] = Session["edtPdId"];
            //-----------------------------------------------------------------------
            if (Request.QueryString["pid"] != null)
            {
                string callfun = string.Format("openinnewTab({0});", Request.QueryString["pid"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message",callfun, true);
            }
            txtDOC.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
           // txtDOB.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            //FillData();
            BindAutoCasePaperNo();
            this.FillData();

        }

        if (!(Session["Report"] as string == "{CrystalDecisions.CrystalReports.Engine.ReportDocument}"))
        {
            CrystalReportViewer2.ReportSource = (ReportDocument)Session["Report"];
        }

    }

    public void BindDoctor()
    {
        DDDoctor.Items.Clear();
        DDDoctor.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT * FROM vwGetDoctor", "did", "FullName", DDDoctor);
    }


    public void BindAutoCasePaperNo()
    {

        DataTable dt = new DataTable();

        StrSQL = "GetAutoIncCasePaperNo";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(SqlCmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            txtCasePaperNo.Text = ds.Tables[0].Rows[0]["CasePaperAutoNo"].ToString();

            conn.Close();
        }

        //return dt;

        //strSQL = "Exec GetAutoIncCasePaperNo";
        // if (con.State == ConnectionState.Closed)
        //    con.Open();
        //SqlCmd = new SqlCommand(strSQL, con);
        //Sqldr = SqlCmd.ExecuteReader();

        //while (Sqldr.Read())
        //{
        //    //td_r.Style.Add("display", "block");
        //    //td_R1.Style.Add("display", "block");
        //    txtCasePaperNo.Text= Sqldr["AutoNo"].ToString(); 
        //}
        //Sqldr.Close();
    }

    public void BindState()
    {
        DDL_State.Items.Clear();
        DDL_State.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT StateID , Upper(State) As State FROM tblStateMaster Where IsDelete = 0 Order By State", "StateID", "State", DDL_State);
        DDL_State.SelectedValue = "3";
    }

    public void BindCity()
    {
        DDL_City.Items.Clear();
        DDL_City.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT CityID , Upper(City) As City  FROM tblCityMaster Where StateID = '" + DDL_State.SelectedValue + "' And IsDelete = 0 Order By City", "CityID", "City", DDL_City);
        //DDL_City.SelectedValue = "1103";
    }
    protected void btnVoucher_Click(object sender, EventArgs e)
    {
        if (PdID != 0)
        {
            Session["CasePatientID"] = PdID;
            Response.Redirect("Voucher.aspx?SC=Consulting&PdID=" + PdID);
        }
        else
        {
            Response.Redirect("Home.aspx");
        }
    }
    public void FillData()
    {
        BindDoctor();
        BindState();
        BindCity();

        if (PdID != 0)
        {
            strSQL = "Select Cast(pdConsultingAmount As decimal(18,0)) As pdConsultingAmountAks , ISNULL(DM.did,DMH.did) As NDID,IsNull(DM.dFName,DMH.dFName) + ' ' + IsNull(DM.dMName,DMH.dMName) + ' ' + isNull(DM.dLName,DMH.dLName) AS FullName , * From tblPatientDetails PD Left Join tblDoctorMaster DM On PD.did = DM.did Left Join tblDoctorHistoryDetails DMH On PD.did = DMH.did Where pdID=" + PdID;
        }
        else if (OLID != 0)
        {
            strSQL = "Exec GetTodayOnlinePatientRegistrationDetails " + OLID;
        }
        else
        {
            strSQL = "Exec GetTodayAppointmentForRegistrationConfirm " + ADID;
        }
       

        //else if (ADID != 0)
        //{
        //    strSQL = "Exec GetTodayAppointmentForRegistrationConfirm " + ADID;
        //}
        //else if (OLID != 0)
        //{
        //    strSQL = "Exec GetTodayOnlinePatientRegistrationDetails " + OLID;
        //}

        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();

        while (Sqldr.Read())
        {
            //td_r.Style.Add("display", "block");
            //td_R1.Style.Add("display", "block");
            ViewState["pdId"] = Sqldr["pdID"].ToString(); 
            ddintial.SelectedValue = Sqldr["pdInitial"].ToString();
            txtfname.Text = Sqldr["pdFname"].ToString();
            txtlname.Text = Sqldr["pdLname"].ToString();
            txtmname.Text = Sqldr["pdMname"].ToString();
            txtmob.Text = Sqldr["pdMob"].ToString();
            txttele.Text = Sqldr["pdTele"].ToString();
            txtAddress1.Text = Sqldr["pdAdd1"].ToString();
            txtAddress2.Text = Sqldr["pdAdd2"].ToString();
            txtAddress3.Text = Sqldr["pdAdd3"].ToString();
            txtAddress4.Text = Sqldr["pdAdd4"].ToString();
            txtage.Text = Sqldr["pdAge"].ToString();
            if (PdID != 0)
            {
                txtCasePaperNo.Text = Sqldr["pdCasePaperNo"].ToString();
            }
            else
            {
                //txtCasePaperNo.Text = "";
            }
            txt_ReconsultingNo.Text = Sqldr["PDReconsultingNo"].ToString();
            txtCassetteNo.Text = Sqldr["pdCassetteNo"].ToString();
            txtDOC.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["pdDOC"].ToString()));
            txtemail.Text = Sqldr["pdemail"].ToString();
            ddRegFees.SelectedValue = Sqldr["pdIsRegistrationFeesPaid"].ToString();
            ddSex.SelectedValue = Sqldr["pdSex"].ToString();
            DDDoctor.SelectedValue = Sqldr["did"].ToString();

            if (DDDoctor.SelectedValue == "")
            {
                DDDoctor.Items.Insert(1, new ListItem(Sqldr["FullName"].ToString(), Sqldr["NDID"].ToString()));
                DDDoctor.SelectedValue = Sqldr["NDID"].ToString();
            }

            txtCounsultingAmt.Text = Sqldr["pdConsultingAmountAks"].ToString();
            if (Sqldr["pdDOB"].ToString() != "NULL" || Sqldr["pdDOB"].ToString() != "")
            {
                txtDOB.Text = Sqldr["pdDOB"]!=DBNull.Value?string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["pdDOB"].ToString())):"";
            }
            DDL_BloodGroup.SelectedValue = Sqldr["pdBloodGroup"].ToString();
            txtWeight.Text = Sqldr["pdWeight"].ToString();
            txtHeight.Text = Sqldr["pdHeight"].ToString();
            txt_RelNm.Text = Sqldr["Rel_Nm"].ToString();
            txt_Relation.Text = Sqldr["Relationship"].ToString();
            DDL_PatType.SelectedValue = Sqldr["Pat_Type"].ToString();
            txtOccupation.Text = Sqldr["pdOccupation"].ToString();
            DDL_Country.SelectedValue = Sqldr["pdCountry"].ToString();
            if (Sqldr["pdstate"] != DBNull.Value || Sqldr["pdstate"].ToString().Trim() != "")
            {
                DDL_State.SelectedValue = Sqldr["pdstate"].ToString();
            }

            BindCity();

            if (Sqldr["pdcity"] != DBNull.Value || Sqldr["pdcity"].ToString() != "")
            {
                DDL_City.SelectedValue = Sqldr["pdcity"].ToString();
            }

            txt_Pin.Text = Sqldr["pdPin"].ToString();


            if (Sqldr["Payment_Mode"] != DBNull.Value || Sqldr["Payment_Mode"].ToString() != "")
            {
                DDL_Pay_Mode.SelectedValue = Sqldr["Payment_Mode"].ToString();
            }
            txtCardnm.Text = Sqldr["Bank_Name"].ToString();
            DDL_CardType.SelectedValue = Sqldr["Bank_Branch"].ToString();
            txtCardno.Text = Sqldr["Cheque_No"].ToString();
            if (Sqldr["Cheque_Date"].ToString() != "NULL" || Sqldr["Cheque_Date"].ToString() != "")
            {
                txtChqdt.Text = Sqldr["Cheque_Date"] != DBNull.Value ? string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["Cheque_Date"].ToString())) : "";
            }

            if (Sqldr["pdAgeClassification"].ToString() == "Kid")
            {
                chk_kids.Checked = true;
            }
            else if (Sqldr["pdAgeClassification"].ToString() == "Teen")
            {
                chk_teen.Checked = true;
            }
            else if (Sqldr["pdAgeClassification"].ToString() == "Adult")
            {
                chk_adult.Checked = true;
            }
            if (DDL_Pay_Mode.SelectedIndex == 2)
            {
                tr_Bank.Style.Add("display", "none");
                tr_Cheque.Style.Add("display", "none");
            }
            else
            {
                tr_Bank.Style.Add("display", "none");
                tr_Cheque.Style.Add("display", "none");
            }

        }
        Sqldr.Close();
    }

    //private void txtmob_KeyPress(object sender, KeyPressEventArgs e)
    //{
    //    if (txtmob.Text.Length >= 10)
    //    {
    //        MessageBox.Show("Plz enter 10 digits...");
    //    }
    //}

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            //if (ddRegFees.SelectedIndex == 0)
            //{
            //    string script = "alert('Kindly Select Registration Fee ...!!!!');";
            //    ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
            //    return;
            //}

            if (PdID == 0)
            {
                int CasePapercnt = Convert.ToInt32(gen.executeScalar("Select Count(pdCasePaperNo) As pdCasePaperNo from tblPatientDetails  Where IsCancel = 0  And pdCasePaperNo = '" + txtCasePaperNo.Text.ToString() + "'").ToString());
                if (CasePapercnt > 0)
                {
                    string script = "alert('Case Paper No. Already Generated Kindly Enter New Case Paper No. ...!!!!');";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                    return;
                }

                if (OLID == 0)
                {
                    int CasePapercnt1 = Convert.ToInt32(gen.executeScalar("Select Count(QCasePaperNo) As pdCasePaperNo from tblQuickRegistration  Where QCasePaperNo = '" + txtCasePaperNo.Text.ToString() + "'").ToString());
                    if (CasePapercnt1 > 0)
                    {
                        string script = "alert('Case Paper No. Already Generated In Quick Registration Kindly Enter New Case Paper No. ...!!!!');";
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                        return;
                    }
                }
            }

           // SaveOnlinePatientRegistration();
                 

            string dob;
            if (PdID == 0)
                strSQL = "InsPatientDetails";
            else
                strSQL = "UpdPatientDetails";

            string[] _doc = txtDOC.Text.ToString().Split('/');
            string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

            if (txtDOB.Text != "")
            {
                string[] _dob = txtDOB.Text.ToString().Split('/');
                dob = _dob[2] + "/" + _dob[1] + "/" + _dob[0];
            }
            else
            {
                dob = "";

            }

            string CChqdt;

            if (txtChqdt.Text != "")
            {
                string[] _CheckDt = txtChqdt.Text.ToString().Split('/');
                CChqdt = _CheckDt[2] + "/" + _CheckDt[1] + "/" + _CheckDt[0];
            }
            else
            {
                CChqdt = "";
            }
            
            int NewCaseFormStat = 0;
            if (chk_PrintConsBill.Checked == true)
            {
                NewCaseFormStat = 1;
            }

            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@pdInitial", ddintial.SelectedValue.Trim());
            SqlCmd.Parameters.AddWithValue("@pdFname", txtfname.Text.Trim().ToUpper());
            SqlCmd.Parameters.AddWithValue("@pdMname", txtmname.Text.Trim().ToUpper());
            SqlCmd.Parameters.AddWithValue("@pdLname", txtlname.Text.Trim().ToUpper());
            SqlCmd.Parameters.AddWithValue("@pdSex", ddSex.SelectedValue.ToString());
            SqlCmd.Parameters.AddWithValue("@pdAgeInMonth", txt_agein_month.Text.Trim());
            SqlCmd.Parameters.AddWithValue("@pdAge", txtage.Text.Trim());
            SqlCmd.Parameters.AddWithValue("@pdDOC", doc);
            SqlCmd.Parameters.AddWithValue("@pdAdd1", txtAddress1.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdAdd2", txtAddress2.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdAdd3", txtAddress3.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdAdd4", txtAddress4.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdTele", txttele.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdMob", txtmob.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdemail", txtemail.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdfinyear", gen.GetCurrentFinYear());
            SqlCmd.Parameters.AddWithValue("@pdcreatedby", userid);
            SqlCmd.Parameters.AddWithValue("@pdCasePaperNo", txtCasePaperNo.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdCassetteNo", txtCassetteNo.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdRegFees", ddRegFees.SelectedValue.ToString());
            SqlCmd.Parameters.AddWithValue("@did", DDDoctor.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@pdDOB", dob);
            SqlCmd.Parameters.AddWithValue("@pdConsultingAmount", txtCounsultingAmt.Text.ToString());

            SqlCmd.Parameters.AddWithValue("@pdBloodGroup", DDL_BloodGroup.SelectedValue.ToString().ToUpper());
            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.AddWithValue("@pdWeight", txtWeight.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdHeight", txtHeight.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@adId", ADID);
            SqlCmd.Parameters.AddWithValue("@Rel_Nm", txt_RelNm.Text.ToString().ToUpper());
            SqlCmd.Parameters.AddWithValue("@Relationship", txt_Relation.Text.ToString().ToUpper());
            SqlCmd.Parameters.AddWithValue("@Pat_Type", DDL_PatType.SelectedValue);

            SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@Bank_Name", txtCardnm.Text.ToString().ToUpper());
            SqlCmd.Parameters.AddWithValue("@Bank_Branch", DDL_CardType.SelectedValue.ToString());
            SqlCmd.Parameters.AddWithValue("@Cheque_No", txtCardno.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@Cheque_Date", CChqdt);

            SqlCmd.Parameters.AddWithValue("@pdState", DDL_State.SelectedValue.ToString());
            SqlCmd.Parameters.AddWithValue("@pdcity", DDL_City.SelectedValue.ToString());
            SqlCmd.Parameters.AddWithValue("@pdpin", txt_Pin.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdOccupation", txtOccupation.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@pdCountry", DDL_Country.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@NewCaseFormStat", NewCaseFormStat);
            SqlCmd.Parameters.AddWithValue("@PDReconsultingNo", txt_ReconsultingNo.Text);
            SqlCmd.Parameters.AddWithValue("@OLID", OLID);
            if (chk_kids.Checked == true)
            {
                SqlCmd.Parameters.AddWithValue("@pdAgeClassification", "Kid");
            }
            else if (chk_teen.Checked == true)
            {
                SqlCmd.Parameters.AddWithValue("@pdAgeClassification", "Teen");
            }
            else if (chk_adult.Checked == true)
            {
                SqlCmd.Parameters.AddWithValue("@pdAgeClassification", "Adult");
            }
            else
            {
                SqlCmd.Parameters.AddWithValue("@pdAgeClassification", "");
            }

            if (PdID == 0)
            {
                SqlCmd.Parameters.Add("@pdId", SqlDbType.Int).Direction = ParameterDirection.Output;
                SqlCmd.Parameters.Add("@pdIcardNo", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
            }
            else
            {
                SqlCmd.Parameters.AddWithValue("@pdId", PdID);
                SqlCmd.Parameters.AddWithValue("@pdRCasePaperNo", txtRCasePaperNo.Text.Trim());
            }
            if (con.State == ConnectionState.Closed)
                con.Open();
            int reslt;
            SqlCmd.ExecuteNonQuery();
            reslt = (int)SqlCmd.Parameters["@Error"].Value;

            if (reslt == 0)
            {
                if (PdID == 0)
                {

                    string cId = "";

                    Session["IcardNo"] = SqlCmd.Parameters["@pdIcardNo"].Value.ToString();
                    cId = SqlCmd.Parameters["@pdId"].Value.ToString();
                    txtPID.Text = cId;

                    if (txtmob.Text != "")
                    {
                        String smsstring;

                        SqlCmd = new SqlCommand();
                        decimal TemplateId = 1207160931633599674;
                        //smsstring = "Aditya homoeopathic hospital and healing centre... Your case paper no. is " + txtCasePaperNo.Text.ToString() + "... Registration date: " + txtDOC.Text.ToString() + "... For any queries contact 020-27412197, 020-27412549... visit us at www.drnikam.com";
                        smsstring = "Welcome to Aditya Homoeopathic Hospital and Healing center. Your case no. " + txtCasePaperNo.Text.ToString() + ". \n \n Contact :- 020-27412197, 8806061061 . amar@ahhhc.com drnikam.com";
                        string str21 = genral.GetResponse(txtmob.Text.Trim(), smsstring, TemplateId);

                        SqlCmd.CommandText = "sp_InsSMSTransaction";
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Connection = con;
                        SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                        SqlCmd.Parameters.AddWithValue("@MobileNo", txtmob.Text.Trim());
                        SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                        SqlCmd.Parameters.AddWithValue("@SenderID", userid);
                        SqlCmd.Parameters.AddWithValue("@Pdid", PdID);
                        SqlCmd.Parameters.AddWithValue("@Sources", "Registration");
                        SqlCmd.ExecuteNonQuery();
                    }

                    if (ADID != 0)
                    {
                        //Response.Redirect("AddNewPatient.aspx?msg=Patient Appoinment Confirmed successfully");
                        if (chk_PrintConsBill.Checked == true && Chk_ConsultingBill.Checked == true && chk_AcuteBill.Checked == true)
                        {
                            CrystalReportViewer2.Visible = true;
                            string AksConn = ConfigurationManager.AppSettings["constring"];
                            bindReport(AksConn, PdID);

                            Session["edtPdId"] = null;
                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);

                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                        }
                        else if (chk_PrintConsBill.Checked == true)
                        {
                            CrystalReportViewer2.Visible = true;
                            string AksConn = ConfigurationManager.AppSettings["constring"];
                            bindReport(AksConn, PdID);

                            Session["edtPdId"] = null;
                            Response.Redirect("Home.aspx");                            
                        }
                        else if (Chk_ConsultingBill.Checked == true)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);                            
                        }
                        else if (chk_AcuteBill.Checked == true)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                        }
                        else if (chk_RecForm.Checked == true)
                        {
                            CrystalReportViewer2.Visible = true;
                            string AksConn = ConfigurationManager.AppSettings["constring"];
                            bindReportForRec(AksConn, PdID);
                        }
                        else
                        {
                            // Response.Redirect("Home.aspx?MSG=Patient Appoinment Confirmed successfully");
                            Session["edtPdId"] = null;
                            Response.Redirect("Home.aspx");
                        }

                    }
                    else
                    {
                        if (ddRegFees.SelectedValue.ToString() == "1")
                        {
                            //Response.Redirect("AddNewPatient.aspx?msg=Patient details saved successfully");
                            if (chk_PrintConsBill.Checked == true && Chk_ConsultingBill.Checked == true && chk_AcuteBill.Checked == true)
                            {
                                CrystalReportViewer2.Visible = true;
                                string AksConn = ConfigurationManager.AppSettings["constring"];
                                bindReport(AksConn, PdID);

                                Session["edtPdId"] = null;
                                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
    "click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);

                                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                            }
                            else if (chk_PrintConsBill.Checked == true)
                            {
                                CrystalReportViewer2.Visible = true;
                                string AksConn = ConfigurationManager.AppSettings["constring"];
                                bindReport(AksConn,PdID);

                                Session["edtPdId"] = null;
                                Response.Redirect("Home.aspx");
                            }
                            else if (Chk_ConsultingBill.Checked == true)
                            {
                                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
    "click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                            }
                            else if (chk_AcuteBill.Checked == true)
                            {
                                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
    "click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                            }
                            else if (chk_RecForm.Checked == true)
                            {
                                CrystalReportViewer2.Visible = true;
                                string AksConn = ConfigurationManager.AppSettings["constring"];
                                bindReportForRec(AksConn, PdID);
                            }
                            else
                            {
                                // Response.Redirect("Home.aspx?MSG=Patient details saved successfully");
                                Session["edtPdId"] = null;
                                Response.Redirect("Home.aspx");
                            }
                        }
                        else
                        {
                            //Response.Redirect("AddNewPatient.aspx?msg=Patient details saved successfully");
                            if (chk_PrintConsBill.Checked == true && Chk_ConsultingBill.Checked == true && chk_AcuteBill.Checked == true)
                            {
                                CrystalReportViewer2.Visible = true;
                                string AksConn = ConfigurationManager.AppSettings["constring"];
                                bindReport(AksConn, PdID);

                                Session["edtPdId"] = null;
                                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
    "click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);

                                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                            }
                            else if (chk_PrintConsBill.Checked == true)
                            {
                                CrystalReportViewer2.Visible = true;
                                string AksConn = ConfigurationManager.AppSettings["constring"];
                                bindReport(AksConn,PdID);
                                Session["edtPdId"] = null;
                                Response.Redirect("Home.aspx");
                            }
                            else if (Chk_ConsultingBill.Checked == true)
                            {
                                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
    "click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                            }
                            else if (chk_AcuteBill.Checked == true)
                            {
                                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
    "click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                            }
                            else if (chk_RecForm.Checked == true)
                            {
                                CrystalReportViewer2.Visible = true;
                                string AksConn = ConfigurationManager.AppSettings["constring"];
                                bindReportForRec(AksConn, PdID);
                            }
                            else
                            {
                                //Response.Redirect("Home.aspx?MSG=Patient details saved successfully");
                                Session["edtPdId"] = null;
                                Response.Redirect("Home.aspx");
                            }
                        }
                    }


                }
                else
                {
                    // string smsstring = "Aditya homoeopathic hospital and healing centre... Your case paper no. is " + txtCasePaperNo.Text.ToString() + "... Registration date: " + txtDOC.Text.ToString() + "... For any queries contact 020-27412197, 020-27412549... visit us at www.drnikam.com";
                    string smsstring = "Welcome to Aditya Homoeopathic Hospital and Healing center. \nYour case no. is " + txtCasePaperNo.Text.ToString() + ". \n \nContact :- 020-27412197, 8806061061 . \ndr.amar@ahhhc.com \ndrnikam.com ";
                   // string str21 = genral.GetResponse(txtmob.Text.Trim(), smsstring);
                    if (ADID != 0)
                    {
                        //Response.Redirect("AddNewPatient.aspx?msg=Patient Appoinment Confirmed successfully");
                        if (chk_PrintConsBill.Checked == true && Chk_ConsultingBill.Checked == true && chk_AcuteBill.Checked == true)
                        {
                            CrystalReportViewer2.Visible = true;
                            string AksConn = ConfigurationManager.AppSettings["constring"];
                            bindReport(AksConn, PdID);

                            Session["edtPdId"] = null;
                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);

                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                        }
                        else if (chk_PrintConsBill.Checked == true)
                        {

                            CrystalReportViewer2.Visible = true;
                            string AksConn = ConfigurationManager.AppSettings["constring"];
                            bindReport(AksConn,PdID);

                            Session["edtPdId"] = null;
                        }
                        else if (Chk_ConsultingBill.Checked == true)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                        }
                        else if (chk_AcuteBill.Checked == true)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                        }
                        else if (chk_RecForm.Checked == true)
                        {
                            CrystalReportViewer2.Visible = true;
                            string AksConn = ConfigurationManager.AppSettings["constring"];
                            bindReportForRec(AksConn, PdID);
                        }
                        else
                        {
                            // Response.Redirect("Home.aspx?MSG=Patient Appoinment Confirmed successfully");
                            Session["edtPdId"] = null;
                            Response.Redirect("Home.aspx");
                        }
                    }
                    else
                    {
                        if (chk_PrintConsBill.Checked == true && Chk_ConsultingBill.Checked == true && chk_AcuteBill.Checked == true)
                        {
                            CrystalReportViewer2.Visible = true;
                            string AksConn = ConfigurationManager.AppSettings["constring"];
                            bindReport(AksConn, PdID);

                            Session["edtPdId"] = null;
                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);

                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                        }
                        else if (chk_PrintConsBill.Checked == true)
                        {
                            CrystalReportViewer2.Visible = true;
                            string AksConn = ConfigurationManager.AppSettings["constring"];
                            bindReport(AksConn,PdID);

                            Session["edtPdId"] = null;
                            Response.Redirect("AddNewPatient.aspx?MSG=Patient Details Update successfully");
                            //         ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
                            //"click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                        }
                        else if (Chk_ConsultingBill.Checked == true)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('ConsultingBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                        }
                        else if (chk_AcuteBill.Checked == true)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page),
"click", @"<script>window.open('AcuteBill.aspx?id=" + PdID + "','_newtab');</script>", false);
                        }
                        else if (chk_RecForm.Checked == true)
                        {
                            CrystalReportViewer2.Visible = true;
                            string AksConn = ConfigurationManager.AppSettings["constring"];
                            bindReportForRec(AksConn, PdID);
                        }
                        else
                        {
                            Session["edtPdId"] = null;
                            Response.Redirect("ManagePatients.aspx?flag=1");
                        }

                    }


                }
            }

            SqlCmd.Dispose();
            con.Close();
        }
 }

    protected void ddRegFees_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddRegFees.SelectedValue == "1")
        {
            txtCounsultingAmt.Enabled = true;
            txtCounsultingAmt.Text = "1500";
            txt_RelNm.Visible = false;
            txt_Relation.Visible = false;
            lblRelNm.Visible = false;
            lblRelation.Visible = false;

        }
        else if (ddRegFees.SelectedValue == "3")
        {
            txtCounsultingAmt.Enabled = true;
            txtCounsultingAmt.Text = "2000";
            txt_RelNm.Visible = true;
            txt_Relation.Visible = true;
            lblRelNm.Visible = true;
            lblRelation.Visible = true;
        }
        else
        {
            txtCounsultingAmt.Enabled = true;
            txtCounsultingAmt.Text = "0";
            txt_RelNm.Visible = false;
            txt_Relation.Visible = false;
            lblRelNm.Visible = false;
            lblRelation.Visible = false;
        }
      }

    //protected void DDL_Pay_Mode_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (DDL_Pay_Mode.SelectedIndex == 2)
    //    {
    //        tr_Bank.Style.Add("display", "none");
    //        tr_Cheque.Style.Add("display", "none");
    //    }
    //    else
    //    {
    //        tr_Bank.Style.Add("display", "none");
    //        tr_Cheque.Style.Add("display", "none");
    //    }
    //}

    protected void DDL_Country_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (DDL_Country.SelectedValue != "INDIA")
        {
            DDL_State.SelectedValue = "42";

            DDL_City.Items.Clear();
            DDL_City.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT CityID , Upper(City) As City FROM tblCityMaster Where StateID = '" + DDL_State.SelectedValue + "' And IsDelete = 0 ", "CityID", "City", DDL_City);
            DDL_City.SelectedValue = "1687";
        }
        else
        {
            DDL_State.Items.Clear();
            DDL_State.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT StateID , Upper(State) As State FROM tblStateMaster Where IsDelete = 0 ", "StateID", "State", DDL_State);
            DDL_State.SelectedValue = "3";

            DDL_City.Items.Clear();
            DDL_City.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT CityID , Upper(City) As City  FROM tblCityMaster Where StateID = '" + DDL_State.SelectedValue + "' And IsDelete = 0 ", "CityID", "City", DDL_City);
            //DDL_City.SelectedValue = "1103";
        }

       
    }


    protected void DDL_State_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDL_City.Items.Clear();
        DDL_City.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT CityID , Upper(City) As City FROM tblCityMaster Where StateID = '" + DDL_State.SelectedValue + "' And IsDelete = 0 ", "CityID", "City", DDL_City);
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtCounsultingAmt.Enabled = true;
        txtCounsultingAmt.Text = "1500";
        txt_RelNm.Visible = true;
        txt_Relation.Visible = true;
        lblRelNm.Visible = true;
        lblRelation.Visible = true;
    }

    public void bindReport(string AksConn , int pdid)
    {
        DataTable dt = new DataTable();
        ReportDocument cryRpt = new ReportDocument();
        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = default(CrystalDecisions.CrystalReports.Engine.Tables);
        System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

        builder.ConnectionString = AksConn;

        string AksServer = builder["Server"] as string;
        string AksDatabase = builder["Database"] as string;
        string AksUsername = builder["Uid"] as string;
        string AksPassword = builder["Pwd"] as string;


        dt = GetReport(pdid);

         cryRpt.Load(Server.MapPath("~/Reports/NewCasePrintReport.rpt"));
       
        CrTables = cryRpt.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        cryRpt.SetDataSource(dt);
        cryRpt.Refresh();

        //cryRpt.SetParameterValue("FromDate", "");
        //cryRpt.SetParameterValue("ToDate", "");

        CrystalReportViewer2.ReportSourceID = "CrystalReportSource2";
        CrystalReportViewer2.ReportSource = cryRpt;

        System.IO.Stream oStream = null;
        byte[] byteArray = null;
        oStream = cryRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        byteArray = new byte[oStream.Length];

        Session.Add("COMMPrintingData", byteArray);
        Session.Add("PrintingEventForm", "COMM_Print");

        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
        Session["byteArray"] = byteArray;
        cryRpt.Close();
        cryRpt.Dispose();


    }

    public DataTable GetReport(int pdid)
    {
        DataTable dt = new DataTable();

        StrSQL = "Proc_GetNewCaseNReconsultingData";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@pdID", pdid);
        SqlCmd.Parameters.AddWithValue("@Flag", "NewCase");
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();

            dt.Load(dr);

        }

        return dt;

     }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Convert.ToString(Session["update"]);
    }

    public void bindReportForRec(string AksConn, int pdid)
    {
        DataTable dt = new DataTable();
        ReportDocument cryRpt = new ReportDocument();
        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = default(CrystalDecisions.CrystalReports.Engine.Tables);
        System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

        builder.ConnectionString = AksConn;

        string AksServer = builder["Server"] as string;
        string AksDatabase = builder["Database"] as string;
        string AksUsername = builder["Uid"] as string;
        string AksPassword = builder["Pwd"] as string;


        dt = GetReportForRec(pdid);

        cryRpt.Load(Server.MapPath("~/Reports/ReconsultingPrintReport.rpt"));

        CrTables = cryRpt.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        cryRpt.SetDataSource(dt);
        cryRpt.Refresh();

        //cryRpt.SetParameterValue("FromDate", "");
        //cryRpt.SetParameterValue("ToDate", "");

        CrystalReportViewer2.ReportSourceID = "CrystalReportSource2";
        CrystalReportViewer2.ReportSource = cryRpt;

        System.IO.Stream oStream = null;
        byte[] byteArray = null;
        oStream = cryRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        byteArray = new byte[oStream.Length];

        Session.Add("COMMPrintingData", byteArray);
        Session.Add("PrintingEventForm", "COMM_Print");

        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
        Session["byteArray"] = byteArray;
        cryRpt.Close();
        cryRpt.Dispose();


    }

    public DataTable GetReportForRec(int pdid)
    {
        DataTable dt = new DataTable();

        StrSQL = "Proc_GetNewCaseNReconsultingData";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@pdID", pdid);
        SqlCmd.Parameters.AddWithValue("@Flag", "ReconsultingForm");
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();

            dt.Load(dr);

        }

        return dt;

    }

    protected void btnQR_Click(object sender, EventArgs e)
    {
        Response.Redirect("QuickRegistration.aspx");
    }

    protected void btnReconsulting_Click(object sender, EventArgs e)
    {
        if (PdID != 0)
        {
            Session["CasePatientID"] = PdID;
            Response.Redirect("AddNewReconsulting.aspx");
        }
        else
        {
            Response.Redirect("Home.aspx");
        }
    }
    //public void SaveOnlinePatientRegistration()
    //{
    //    string dob;
    //    if (PdID == 0)
    //        strSQL1 = "InsPatientRegistration";
    //    else
    //        strSQL1 = "UpdPatientRegistration";

    //    string[] _doc = txtDOC.Text.ToString().Split('/');
    //    string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

    //    if (txtDOB.Text != "")
    //    {
    //        string[] _dob = txtDOB.Text.ToString().Split('/');
    //        dob = _dob[2] + "/" + _dob[1] + "/" + _dob[0];
    //    }
    //    else
    //    {
    //        dob = "";

    //    }

    //    string CChqdt;

    //    if (txtChqdt.Text != "")
    //    {
    //        string[] _CheckDt = txtChqdt.Text.ToString().Split('/');
    //        CChqdt = _CheckDt[2] + "/" + _CheckDt[1] + "/" + _CheckDt[0];
    //    }
    //    else
    //    {
    //        CChqdt = "";
    //    }

    //    int NewCaseFormStat = 0;
    //    if (chk_PrintConsBill.Checked == true)
    //    {
    //        NewCaseFormStat = 1;
    //    }

    //    SqlCmd1 = new SqlCommand(strSQL1, con1);
    //    SqlCmd1.CommandType = CommandType.StoredProcedure;
    //    SqlCmd1.Parameters.AddWithValue("@pdInitial", ddintial.SelectedValue.Trim());
    //    SqlCmd1.Parameters.AddWithValue("@pdFname", txtfname.Text.Trim().ToUpper());
    //    SqlCmd1.Parameters.AddWithValue("@pdMname", txtmname.Text.Trim().ToUpper());
    //    SqlCmd1.Parameters.AddWithValue("@pdLname", txtlname.Text.Trim().ToUpper());
    //    SqlCmd1.Parameters.AddWithValue("@pdSex", ddSex.SelectedValue.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdAge", txtage.Text.Trim());
    //    SqlCmd1.Parameters.AddWithValue("@pdDOC", doc);
    //    SqlCmd1.Parameters.AddWithValue("@pdAdd1", txtAddress1.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdAdd2", txtAddress2.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdAdd3", txtAddress3.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdAdd4", txtAddress4.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdTele", txttele.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdMob", txtmob.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdemail", txtemail.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdfinyear", gen.GetCurrentFinYear());
    //    SqlCmd1.Parameters.AddWithValue("@pdcreatedby", userid);
    //    SqlCmd1.Parameters.AddWithValue("@pdCasePaperNo", txtCasePaperNo.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdCassetteNo", txtCassetteNo.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdRegFees", ddRegFees.SelectedValue.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@did", DDDoctor.SelectedValue);
    //    SqlCmd1.Parameters.AddWithValue("@pdDOB", dob);
    //    SqlCmd1.Parameters.AddWithValue("@pdConsultingAmount", txtCounsultingAmt.Text.ToString());

    //    SqlCmd1.Parameters.AddWithValue("@pdBloodGroup", DDL_BloodGroup.SelectedValue.ToString().ToUpper());
    //    SqlCmd1.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
    //    SqlCmd1.Parameters.AddWithValue("@pdWeight", txtWeight.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@adId", ADID);
    //    SqlCmd1.Parameters.AddWithValue("@Rel_Nm", txt_RelNm.Text.ToString().ToUpper());
    //    SqlCmd1.Parameters.AddWithValue("@Relationship", txt_Relation.Text.ToString().ToUpper());
    //    SqlCmd1.Parameters.AddWithValue("@Pat_Type", DDL_PatType.SelectedValue);

    //    SqlCmd1.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
    //    SqlCmd1.Parameters.AddWithValue("@Bank_Name", txtCardnm.Text.ToString().ToUpper());
    //    SqlCmd1.Parameters.AddWithValue("@Bank_Branch", DDL_CardType.SelectedValue.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@Cheque_No", txtCardno.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@Cheque_Date", CChqdt);

    //    SqlCmd1.Parameters.AddWithValue("@pdState", DDL_State.SelectedValue.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdcity", DDL_City.SelectedValue.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdpin", txt_Pin.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdOccupation", txtOccupation.Text.ToString());
    //    SqlCmd1.Parameters.AddWithValue("@pdCountry", DDL_Country.SelectedValue);
    //    SqlCmd1.Parameters.AddWithValue("@NewCaseFormStat", NewCaseFormStat);
    //    SqlCmd1.Parameters.AddWithValue("@PDReconsultingNo", txt_ReconsultingNo.Text);

    //    if (PdID == 0)
    //    {
    //        SqlCmd1.Parameters.Add("@pdId", SqlDbType.Int).Direction = ParameterDirection.Output;
    //        SqlCmd1.Parameters.Add("@pdIcardNo", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
    //    }
    //    else
    //    {
    //        SqlCmd1.Parameters.AddWithValue("@pdId", PdID);
    //        SqlCmd1.Parameters.AddWithValue("@pdRCasePaperNo", txtRCasePaperNo.Text.Trim());
    //    }
    //    if (con1.State == ConnectionState.Closed)
    //        con1.Open();
       
    //    SqlCmd1.ExecuteNonQuery();

    //    SqlCmd1.Dispose();
    //    con1.Close();


    //}
        protected void chk_kids_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_kids.Checked==true)
        {
            chk_teen.Checked = false;
            chk_adult.Checked = false;
        }
    }

    protected void chk_teen_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_teen.Checked == true)
        {
            chk_kids.Checked = false;
            chk_adult.Checked = false;
        }
    }

    protected void chk_adult_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_adult.Checked == true)
        {
            chk_teen.Checked = false;
            chk_kids.Checked = false;
        }
    }
    protected void chk_AcuteBill_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_ConsultingBill.Checked)
        {
            Chk_ConsultingBill.Checked = false;
        }
        if (chk_AcuteBill.Checked)
        {
            Chk_ConsultingBill.Checked = false;
        }
    }

    protected void Chk_ConsultingBill_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_ConsultingBill.Checked)
        {
            chk_AcuteBill.Checked = false;
        }
        if (chk_AcuteBill.Checked)
        {
            Chk_ConsultingBill.Checked = false;
        }
    }

    protected void txtDOB_TextChanged(object sender, EventArgs e)
    {
        SqlCmd = new SqlCommand();
        SqlCmd.CommandText = "Proc_GetAge";
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Age", txtDOB.Text);
        da = new SqlDataAdapter(SqlCmd);
        SqlCmd.Connection = con;
        con.Open();
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            txtage.Text = dt.Rows[0]["Year"].ToString();
            txt_agein_month.Text = dt.Rows[0]["Month"].ToString();
        }
        // SqlCmd.ExecuteNonQuery();
        con.Close();
    }

}