﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class EditAddmissionDetails : System.Web.UI.Page
{
    SqlCommand SqlCmd, SqlCmd2;
    SqlDataReader Sqldr, Sqldr2, dr;
    string strSQL = "";
    string strSQL2 = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection con1 = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection con2 = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    //string pdId = "";
    string uid = "";
    //string ipdid = "";
    //added by nikita on 12th march 2015------------------------------
    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-------------------------------------------------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();

            txtDOD.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();
            //BindUser();
        }
    }
    //protected void BindUser()
    //{
    //    strSQL = "Select * From tblUser where uid = " + uid;
    //    SqlCmd = new SqlCommand(strSQL, con);
    //    SqlCmd.CommandType = CommandType.Text;
    //    SqlDataReader dr;
    //    if(con.State == ConnectionState.Closed)
    //        con.Open();
    //        dr = SqlCmd.ExecuteReader();
    //        while (dr.Read())
    //        {
    //            txtPrepared.Text = Sqldr["uFname"].ToString();
    //        }
    //        con.Close();
    //        dr.Dispose();
    //}
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string doa = gen.getdate(txtDOA.Text.ToString(),'/');
        string toa = txtTOA.Hour +":" + txtTOA.Minute + ":" + txtTOA.Second + " "+txtTOA.AmPm;
        string flag = "0";
        string dod = "9999/01/31";
        string tod = txtTOD.Hour + ":" + txtTOD.Minute + ":" + txtTOD.Second + " " + txtTOD.AmPm;
        if (txtDOD.Text != "")
        {
            flag = "1";
            dod = gen.getdate(txtDOD.Text.ToString(), '/');
        }
        
        strSQL = "EditAddmissionDetails";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@DOA", doa);
        SqlCmd.Parameters.AddWithValue("@TOA", toa);
        SqlCmd.Parameters.AddWithValue("@DOD", dod);
        SqlCmd.Parameters.AddWithValue("@TOD", tod);
        SqlCmd.Parameters.AddWithValue("@DepositAmt", txtDepositAmt.Text);
        SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
        SqlCmd.Parameters.AddWithValue("@Flag", ddStatus.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@RefBy", txtRefBy.Text);
        SqlCmd.Parameters.AddWithValue("@Treatment", txttreatment.Text);
        SqlCmd.Parameters.AddWithValue("@PreparedBy", txtPrepared.Text);
        SqlCmd.Parameters.AddWithValue("@CheckedBy", txtChecked.Text);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@ipdDischargeBy", uid);

        if (con.State == ConnectionState.Closed)
            con.Open();
        int i = SqlCmd.ExecuteNonQuery();
        con.Close();
        SqlCmd.Dispose();
        con.Dispose();

        if (i > 0)
        {
            if (txtDOD.Text != "" && ddStatus.SelectedValue == "1")
            {

                decimal BalanceAmt = 0;
                strSQL2 = "Exec  sp_GenerateBill_CurrentBill " + ipdid;
                if (con2.State == ConnectionState.Closed)
                    con2.Open();
                SqlCmd2 = new SqlCommand(strSQL2, con2);
                Sqldr2 = SqlCmd2.ExecuteReader();
                while (Sqldr2.Read())
                {
                    BalanceAmt = Convert.ToDecimal(Sqldr2["CurrentBillAmount"]);
                }
                Sqldr2.Close();
                SqlCmd2.Dispose();

                if (BalanceAmt <= 0)
                {
                    AddAppointmentOfDischargePatient();
                }
             }
        }

        if (chkGenBill.Checked == true)
        {
            Session["BilledipdId"] = ipdid;
            Session["BilledPatientID"] = PdID;
            Response.Redirect("GenerateBill.aspx");
        }
        else
        {
            Response.Redirect("IPDView.aspx", false);
        }
    }

    public void FillData()
    {
        strSQL = "Select DBO.GetTotalDepositAmountAks(" + ipdid + ") As DepAmt , * From tblIpdMaster Where ipdId=" + ipdid;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            txtDOA.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["ipdAdmissionDate"].ToString()));

            string[] time = gen.getTime(Sqldr["ipdAdmissionTime"].ToString());
            if (time[3] == "AM")
                txtTOA.SetTime(Convert.ToInt32(time[0]), Convert.ToInt32(time[1]), Convert.ToInt32(time[2]), MKB.TimePicker.TimeSelector.AmPmSpec.AM);
            else
                txtTOA.SetTime(Convert.ToInt32(time[0]), Convert.ToInt32(time[1]), Convert.ToInt32(time[2]), MKB.TimePicker.TimeSelector.AmPmSpec.PM);

            if (Sqldr["ipdDischargeDate"] != DBNull.Value)
            {
                txtDOD.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["ipdDischargeDate"].ToString()));
            }

            if (Sqldr["ipdIsDischarge"] != DBNull.Value)
            {
                ddStatus.SelectedValue = Sqldr["ipdIsDischarge"].ToString();    
            }

            if (Sqldr["ipdDischargeTime"] != DBNull.Value)
            {
                string[] time1 = gen.getTime(Sqldr["ipdDischargeTime"].ToString());
                if (time1[3] == "AM")
                    txtTOD.SetTime(Convert.ToInt32(time1[0]), Convert.ToInt32(time1[1]), Convert.ToInt32(time1[2]), MKB.TimePicker.TimeSelector.AmPmSpec.AM);
                else
                    txtTOD.SetTime(Convert.ToInt32(time1[0]), Convert.ToInt32(time1[1]), Convert.ToInt32(time1[2]), MKB.TimePicker.TimeSelector.AmPmSpec.PM);
            }
            txtDepositAmt.Text = Sqldr["DepAmt"].ToString();

            if (Sqldr["ipdRefBy"] != DBNull.Value)
            {
                txtRefBy.Text = Sqldr["ipdRefBy"].ToString();
            }
            if (Sqldr["ipdPreparedBy"] != DBNull.Value)
            {
                txtPrepared.Text = Sqldr["ipdPreparedBy"].ToString();
            }
            if (Sqldr["ipdCheckeddBy"] != DBNull.Value)
            {
                txtChecked.Text = Sqldr["ipdCheckeddBy"].ToString();
            }
            if (Sqldr["ipdTreatment"] != DBNull.Value)
            {
                txttreatment.Text = Sqldr["ipdTreatment"].ToString();
            }
        }

        Sqldr.Close();
        Sqldr.Dispose();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "ShowRow();", true);
    }

    public void AddAppointmentOfDischargePatient()
    {
        int PID = PdID;
        string[] _doc = txtDOD.Text.ToString().Split('/');
        string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

        int cnt = Convert.ToInt32(gen.executeScalar("Select Count(adid) As TotalApp From tblAppointmentDetails Where Pat_Type = 'O' And App_Cancel = 0 And CONVERT(date,AppointmentDate) = CONVERT(date, getdate()) And PatientID =" + PID).ToString());

        if (cnt > 0)
        {
        }
        else
        {
            strSQL = "InsAppoinmentDetails";

            SqlCmd = new SqlCommand(strSQL, con1);
            SqlCmd.CommandType = CommandType.StoredProcedure;

            SqlCmd.Parameters.AddWithValue("@PatientID", PID);
            SqlCmd.Parameters.AddWithValue("@AppointmentDate", doc);
            SqlCmd.Parameters.AddWithValue("@adCreatedBy", uid);
            SqlCmd.Parameters.AddWithValue("@App_Type", "CALL");

            SqlCmd.Parameters.AddWithValue("@pdInitial", "");
            SqlCmd.Parameters.AddWithValue("@pdFname", "");
            SqlCmd.Parameters.AddWithValue("@pdMname", "");
            SqlCmd.Parameters.AddWithValue("@pdLname", "");

            SqlCmd.Parameters.AddWithValue("@pdAdd1", "");
            SqlCmd.Parameters.AddWithValue("@pdAdd2", "");
            SqlCmd.Parameters.AddWithValue("@pdAdd3", "");
            SqlCmd.Parameters.AddWithValue("@pdAdd4", "");
            SqlCmd.Parameters.AddWithValue("@pdTele", "");
            SqlCmd.Parameters.AddWithValue("@pdMob", "");
            SqlCmd.Parameters.AddWithValue("@pdemail", "");

            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.Add("@adid", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.AddWithValue("@pdWeight", "");
            SqlCmd.Parameters.AddWithValue("@Pat_Type", "CO");

            if (con1.State == ConnectionState.Closed)
                con1.Open();
            int reslt;
            SqlCmd.ExecuteNonQuery();
            reslt = (int)SqlCmd.Parameters["@Error"].Value;

            if (reslt == 0)
            {
                // Response.Redirect("Home.aspx?MSG=Patient Appoinment Saved successfully");
                // Response.Redirect("Home.aspx");
            }
            SqlCmd.Dispose();
            con1.Close();
        }

       
    }

}
