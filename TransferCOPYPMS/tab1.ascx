﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="tab1.ascx.cs" Inherits="tab1" %>
<style type="text/css">
    .tabdiv
    {
        border-width: 2px 2px 0px;
        border-style: solid solid none;
        border-color: #E2E2E2;
        position: inherit;
        height: auto;
    }
    
    #tabtbl
    {
        color: #000;
        font-size: 12px;
        font-weight: lighter;
        padding: 2px;
        background-color: #F1F1F1;
    }
    
    .alt1
    {
        width: 65px;
        font-weight: bold;
    }
    .alt2
    {
        width: 125px;
    }
</style>
<script type="text/javascript">
    function goBack() {
        //alert("ok")
        window.history.go(-1)
    }
</script>
<script type="text/javascript">

    function PatientClick() {
        $("#<%=btnPatientDetails.ClientID %>").click();
    }
</script>
<div class="tabdiv">
    <table id="tabtbl" style="width: 100%;" cellpadding="2px" cellspacing="0px" border="1px">
        <tr>
            <td colspan="6">
                <div style="float: right;">
                    &nbsp;
                    <asp:LinkButton ID="lnkTest" runat="server" PostBackUrl="~/OPDTestDetails.aspx">OPD Test</asp:LinkButton>
                    &nbsp;|&nbsp;
                    <%--<a href="" onclick="parent.history.back(); return false;">Back</a>&nbsp;|&nbsp;--%>
                    <a href="ProfarmaOfCase.aspx">Back To My Case</a>
                    <asp:Label ID="lblIPDStatus" runat="server" Text=""></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Patient Name :
            </td>
            <td class="alt2">
                <a onclick="PatientClick()" href="#">
                    <asp:Label ID="lblPatientName" runat="server" Text=""></asp:Label>
                </a>
                <asp:Button ID="btnPatientDetails" runat="server" Text="Button" OnClick="btnPatientDetails_Click"
                    Style="display: none;" />
            </td>
            <td class="alt1">
                Age :
            </td>
            <td class="alt2">
                <asp:Label ID="lblAge" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Gender :
            </td>
            <td class="alt2">
                <asp:Label ID="lblGender" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Mobile :
            </td>
            <td class="alt2">
                <asp:Label ID="lblmob" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Email :
            </td>
            <td class="alt2">
                <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                City :
            </td>
            <td class="alt2">
                <asp:Label ID="lbltele" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="alt1">
                Address :
            </td>
            <td class="alt2" colspan="3">
                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                Consulting Date :
            </td>
            <td class="alt2">
                <asp:Label ID="lblConsDate" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="alt1">
                <%-- Patient ID :--%>
                Case Paper No :
            </td>
            <td class="alt2">
                <asp:Label ID="lblPatientId" runat="server" Text="" Visible="false"></asp:Label>
                <a href="ViewSelectedScanDoc.aspx?CPNO=<%=lblCasePaperNo.Text %>" target="_blank">
                    <asp:Label ID="lblCasePaperNo" runat="server" Text=""></asp:Label>
                </a>, <a href="ViewRecounsultingScanDoc.aspx?RecNO=<%=lblRecNO.Text %>" target="_blank">
                    <asp:Label ID="lblRecNO" runat="server" Text=""></asp:Label>
                </a>
            </td>
            <td class="alt1">
                Occupation :
            </td>
            <td class="alt2">
                <asp:Label ID="lblOccupation" runat="server" Text=""></asp:Label>
            </td>
            <td class="alt1">
                <%-- Cassette No :--%>
                Doctor Name :
            </td>
            <td class="alt2">
                <asp:Label ID="lblCassetteNo" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Label ID="lblDoctorName" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</div>
