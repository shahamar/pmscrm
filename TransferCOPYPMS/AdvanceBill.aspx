﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdvanceBill.aspx.cs" Inherits="AdvanceBill" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Advance Details</title>

<style type="text/css">
.address
{
	font-family: Calibri;
	font-size: 18px;
	border-bottom: solid 2px;
	text-align: center;
}
.Date
{
	font-family: Calibri;
	font-size: 12px;
	font-weight: bold;
	border-bottom: solid 2px;
	text-align: right;
}
.details
{
	font-family: Calibri;
	font-size: 14px;
	border-bottom: solid 2px;
	text-align: left;
}
.col1
{
	text-align: left;
	width: 30%;
}
.col2
{
	text-align: center;
	width: 100%;
	font-weight: bold;
	margin-left: 100px;
}
.maintable
{
	border: solid 2px;
	border-radius: 5px;
	padding: 0px;
	font-family: Calibri;	
}
.reportname
{
	text-align: center;
	border-bottom: solid thin;
	font-weight: bold;
	font-family: Calibri;
	font-size:16px;
	padding-bottom:5px
}
.reportname1
{
	text-align: center;
	border-bottom: solid thin;
	font-weight: bold;
	font-family: Calibri;	
	padding-bottom:5px
}
.label{	border-bottom: solid thin;}
</style>
</head>

<body>
    <form id="form1" runat="server">   
    <div style="text-align: center; height: 80%">
        <asp:Panel ID="pnlBill" runat="server" Height="50%">
            <div>
                <center>
                    <asp:Repeater ID="rptBill" runat="server">
                        <ItemTemplate>
                            <table width="100%" cellpadding="0" cellspacing="5" border="0" class="maintable" style="font-size:14px">
                            	<tbody>
                                	<tr>
                                    	<td>
                                        	<table width="100%" cellpadding="0" cellspacing="5" border="0">    
                                                <tr>
                                                    <td align="right" width="10%">
                                                        <img src="images/Aditya_logo_final.png" />
                                                    </td>
                                                    <td align="left" width="90%" style="padding-left:20px">
                                                        <b style="font-size:18px;"> Aditya Homoeopathic Hospital & Healing Center</b><br />
                                                        <span style="font-size:14px">DR. AMARSINHA D. NIKAM (M.D.Homoeopath</span>
                                                    </td> 
                                                </tr> 
                                                <tr>
                                                	<td style="border-bottom:2px solid" colspan="2"></td>
                                                </tr> 
                                                
                                                <tr>
                                                    <td align="center" colspan="2">
                                                    	<span style="font-size:13px"><strong>Address :</strong>  
                                                    	Near Dakshinmukhi Hanuman Mandir, S/No. 309/1/2, Shivaji Chowk, Pimpri Pune - 411017, Maharashatra, India.
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                    	<span style="font-size:13px"><strong>Tel :</strong> 020-27412197 / 8806061061&nbsp;&nbsp;&nbsp;&nbsp;
                                                    	<strong>Website :</strong> www.drnikam.com</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td style="border-bottom:2px solid" colspan="2"></td>
                                                </tr>  
                                               							
                                                <tr>
                                                    <td class="reportname" colspan="2">Advance Details</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td class="reportname1" colspan="2">
                                                        <div style="float: left;">Receipt No. :
                                                            <%--<asp:Label ID="lblRcptNo" runat="server"></asp:Label>--%>
                                                            <%# Eval("ReceiptNo")%>
                                                        </div>
                                                        <div style="float: right;">Date :
                                                            <%--<asp:Label ID="lblDate" runat="server"></asp:Label>--%>
                                                           <%# Eval("Date","{0:dd/MM/yyyy}") %>
                                                        </div>
                                                    </td>
                                                </tr>                                                          
                                                                                
                                                <tr>
                                                    <td align="left" colspan="2">                                     
                                                        <table width="100%" cellpadding="0" cellspacing="5">
                                                            <tr>
                                                                <td align="left">Shree/Smt. :
                                                                    <%# Eval("Name")%>
                                                                </td>
                                                                <td align="center"><%--<strong></strong>--%>
                                                                      Age :
                                                                    <%# Eval("pdAge")%>
                                                                </td>
                                                                <td align="right"><%--<strong></strong>--%>
                                                                    Case Paper No. :
                                                                    <%# Eval("pdCasePaperNo")%>
                                                                </td>
                                                            </tr>
                                                       </table>
                                                    </td>
                                                </tr>
                                                
                                               
                                                <tr style="display:none;">
                                                    <td align="left" colspan="2">
                                                        <strong>Total Amount :</strong>
                                                       
                                                        <%# Eval("TotalCharges")%>
                                                       
                                                    </td>
                                                </tr>     
                                                
                                                   <tr>
                                                    <td align="left" colspan="2">
                                                    Advance Given :
                                                      
                                                        <%# Eval("AdvanceCharges")%>
                                                      
                                                    </td>
                                                   </tr>     

                                                     
                                                  <tr>
                                                    <td class="col1" colspan="2">
                                                    <br />

                                                        <div class="maintable" style="width: 15%;">
                                                            <strong>Rs.</strong>
                                                            <%# Eval("ipdDepositAmount")%>
                                                        </div>
                                                    </td>
                                                </tr>
                                                         
                                                                
                                                <tr>
                                                    <td class="col1">Prepared By :&nbsp;  <%# Eval("UNAME")%> 
                                                        <strong style="padding-left:90px ; font-weight:500;"><%# Eval("Payment_Mode")%></strong>
                                                    </td>
                                                    <td style="float:right; text-align:center">
                                                        <b>Aditya Homoeopathic Hospital <br /> & Healing Center</b>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                </center>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
