﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="IPD.aspx.cs" Inherits="IPD" Title="IPD Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="tabipd.ascx" TagName="Tab" TagPrefix="tab1" %>
<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker" TagPrefix="MKB" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script src="js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" type="text/css" href="css/screen.css" />
    <style>
        .reqPos
        {
            position: absolute;
        }
        
        ._tableh1
        {
            background-image: url("images/tile_back1.gif");
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
            height: 18px;
            padding: 8px 12px 8px 8px;
            text-align: center;
        }
        
        #tblInner td
        {
            width: 50%;
        }
        
        #tblInner1 td
        {
            width: 50%;
        }
    </style>
    <script type="text/javascript">
			// <![CDATA[
        jQuery.noConflict();
        jQuery(document).ready(function () {
            jQuery('textarea').elastic();
            jQuery('textarea').trigger('update');
        });	
			// ]]>
		</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                IPD Details</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <%--<ul class="quicktabs_tabs quicktabs-style-excel">
                                <li class="qtab-Demo active first" id="li_1">
                                    <a class="qt_tab active" href="javascript:void(0)">Add New Patients</a>
                                </li>
                                
                                <li class="qtab-HTML last" id="li_9">
                                    <a class="qt_tab active" href="ManagePatients.aspx">Manage Patients</a>
                                </li>
                                
                                </ul>--%>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto; border: 1.5px solid #E2E2E2;">
                                        <table style="width: 100%;" cellspacing="0px" cellpadding="2px">
                                            <tr>
                                                <td colspan="2">
                                                    <tab1:Tab ID="tabcon" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    Provisional Diagnosis
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                 <asp:TextBox ID="txtProvisionalDiagnosis" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    Final Diagnosis
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txtFinalDiagnosis" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    Reconsulting Diagnosis
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txtReconsultingDiagnosis" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    Operation
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txtOperation" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table id="tblInner" width="100%" cellspacing="0px" cellpadding="2px">
                                                        <tr>
                                                            <td>
                                                                Admission Date
                                                            </td>
                                                            <td>
                                                                Admission Time
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtDOA" runat="server" CssClass="required field" Width="70px"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtDOA_CalendarExtender" runat="server" Enabled="True"
                                                                    Format="dd/MM/yyyy" PopupButtonID="imgDOA" TargetControlID="txtDOA">
                                                                </cc1:CalendarExtender>
                                                                <asp:Image ID="imgDOA" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                    width: 20px; height: 20px;" />
                                                                <asp:RequiredFieldValidator ID="valDOC" runat="server" ControlToValidate="txtDOA"
                                                                    CssClass="reqPos" ErrorMessage="Addmission Date is Required."><span class="error" 
                                                            style="margin-left:21px;">Addmission Date is Required.</span>
                                                                </asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="regDOA" runat="server" ControlToValidate="txtDOA"
                                                                    ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}">
                                                            <span class="error" style="margin-left:21px;">Please enter date in dd/mm/yyyy</span>    
                                                                </asp:RegularExpressionValidator>
                                                            </td>
                                                            <td>
                                                                <MKB:TimeSelector runat="server" ID="txtTOA">
                                                                </MKB:TimeSelector>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Deposit Amount
                                                            </td>
                                                            <td>
                                                                Ref By
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtDepositAmt" runat="server" CssClass="required field"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="reqDepAmt" runat="server" ControlToValidate="txtDepositAmt"
                                                                    CssClass="reqPos" ErrorMessage="Please specify Ward."><span class="error">Please enter Deposit 
                                                        Amount.</span></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="regDepAmt" runat="server" ControlToValidate="txtDepositAmt"
                                                                    ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                                                    ValidationGroup="check">
                                                        <span class="error">Please enter Only Numbers.</span>
                                                                </asp:RegularExpressionValidator>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtRefBy" runat="server" Width="250px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table style="width: 100%;" id="tblInner1" width="100%" cellspacing="0px" cellpadding="2px">
                                                        <tr>
                                                            <td>
                                                                Ward Type
                                                            </td>
                                                            <td>
                                                                Ward No
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="ddWard" runat="server" Width="150px" class="required field"
                                                                    OnSelectedIndexChanged="ddWard_SelectedIndexChanged" AutoPostBack="True">
                                                                   <%-- <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                                    <asp:ListItem Text="General" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Semi-Special" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Special" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="Delux" Value="4"></asp:ListItem>--%>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="valward" runat="server" ControlToValidate="ddWard"
                                                                    CssClass="reqPos" ErrorMessage="Please specify Ward Type."><span class="error" 
                                                            style="margin-left:21px;">Please specify Ward Type.</span>
                                                                </asp:RequiredFieldValidator>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddWardNo" runat="server" Width="150px" class="required field"
                                                                    OnSelectedIndexChanged="ddWardNo_SelectedIndexChanged" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddWardNo"
                                                                    CssClass="reqPos" ErrorMessage="Please specify Ward Type."><span class="error" 
                                                            style="margin-left:21px;">Please specify Ward No.</span>
                                                                </asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Bed No
                                                            </td>
                                                            <td>
                                                                Remedy
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="ddBedNo" runat="server" class="required field" Width="150px">
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddBedNo"
                                                                    CssClass="reqPos" ErrorMessage="Please specify bed No."><span class="error" 
                                                            style="margin-left:21px;">Please specify BedS No.</span>
                                                                </asp:RequiredFieldValidator>
                                                            </td>
                                                            <td>
                                                               <%-- <asp:TextBox ID="txtRemedy" runat="server" CssClass="required field"></asp:TextBox>--%>
                                                                 <asp:DropDownList ID="DDL_RemList" runat="server" Width="170px">
                                                                 </asp:DropDownList>
								<asp:RequiredFieldValidator ID="valRemedy" runat="server" ControlToValidate="DDL_RemList"
                                                                    CssClass="reqPos" ErrorMessage="Please specify Remedy."><span class="error" 
                                                            	    style="margin-left:21px;">Please specify Remedy.</span>
                                                                </asp:RequiredFieldValidator>

                                                            </td>
                                                        </tr>

                                                        <tr>
                                                           <td colspan="2"> Payment Mode * </td>
                                                        </tr>

                                                         <tr>
                                               
                                               
                                                 <td  colspan="2">
                                                     <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="160px"
                                    CssClass="field">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                    <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                    <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                      <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                    <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                    <%--<asp:ListItem Text="Other" Value="Other"></asp:ListItem>--%>
                                </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="reqDepPay_mode" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                                    CssClass="reqPos" ErrorMessage="Please specify Payment Mode."><span class="error">Please Select Payment Mode</span>
						    </asp:RequiredFieldValidator>
                                                </td>
                                                </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                        Style="margin-top: 2px;" Text="Save" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>