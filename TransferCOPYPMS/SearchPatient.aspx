﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchPatient.aspx.cs" Inherits="SearchPatient" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Patient</title>
    <style type="text/css">
        .ui-menu {
            padding: 5px;
            position: absolute;
            width: auto;
        }
    </style>
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/screen.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
    <script src="js/jquery.browser.js" type="text/javascript"></script>
    <script src="js/jquery.tooltip.js" type="text/javascript"></script>
    <style type="text/css">
        .b_submit {
            background: url("images/b_submit.gif") no-repeat scroll 0 0 transparent;
            border: 0 none;
            display: inline-block;
            font-family: arial;
            vertical-align: middle;
            width: 67px;
            height: 32px;
        }

        .textbutton {
            color: #FFFFFF !important;
            font-size: 12px;
            font-weight: bold;
            text-align: center;
            margin-top: -8px;
        }

        .loginform {
            font-family: Verdana;
        }

        .left {
            float: left;
        }

        .formmenu {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #E6E6E6;
            margin-bottom: 10px;
            padding: 10px;
        }

        .tableh1 {
            background-image: url("images/tile_back1.gif");
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-size: 10px;
            height: 18px;
            padding: 8px 12px 8px 8px;
            font-family: verdana;
            font-weight: bold;
        }
    </style>
    <style type="text/css">
        #tbsearch td {
            text-align: left;
            vertical-align: middle;
        }

        #tbsearch label {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }

        #tbhead th {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }

        #gvcol div {
            margin-top: -10px;
        }

        .head1 {
            width: 10%;
            font-size: small;
        }

        .head2 {
            width: 10%;
            font-size: small;
        }

        .head4 {
            width: 10%;
            font-size: small;
        }

        .head4i {
            width: 14%;
        }


        .head5 {
            width: 10%;
            font-size: small;
        }

        .head5i {
            width: 11%;
            font-size: small;
        }

        .head5ii {
            width: 15%;
            font-size: small;
        }

        .head6 {
            width: 10%;
            font-size: small;
        }
         .head7 {
            width: 5%;
            font-size: small;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $(".rerun")
			.button()
            //			.click(function () {
            //			    alert("Running the last action");
            //			})
			.next()
				.button({
				    text: false,
				    icons: {
				        primary: "ui-icon-triangle-1-s"
				    }
				})
				.click(function () {
				    var menu = $(this).parent().next().show().position({
				        my: "right top",
				        at: "right bottom",
				        of: this
				    });
				    $(document).one("click", function () {
				        menu.hide();
				    });
				    return false;
				})
				.parent()
					.buttonset()
					.next()
						.hide()
						.menu();
        });

        $(document).ready(function () {
            $('span.ui-button-text').css("padding", "0.1em");
            $('li.ui-menu-item').css("padding", "3px 1px  5px 2px");
            $('li.ui-menu-item').css("text-align", "left");
        })
    </script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            InitializeToolTip();
        });
        function InitializeToolTip() {

            $(".gridViewToolTip").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {
                    return $($(this).next().html());
                },
                showURL: false
            });
        }
    </script>
</head>
<body style="background-color: #FFFFFF;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel ID="UP1" runat="server">        <ContentTemplate>--%>
    <div>
        <table style="width: 100%; background-color: #FFFFFF;">
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="login-area margin_ten_right123" style="width: 98%; text-align:center;">
                        <h2>Search Patients</h2>
                       <%-- <div class="formmenu">
                            <div class="loginform">
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                    style="height: auto; border: 1.5px solid #E2E2E2;">
                                    <div style="padding: 15px; height: auto;">--%>
                                        <table style="width: 100%; border: 1px solid #EFEFEF;">
                                            <tr>
                                                <td class="tableh1">
                                                    <table id="tbsearch" style="width: 100%;">
                                                        <tr>
                                                            <td style="width: 16%">
                                                                <asp:CheckBox ID="chkConsultingPeriod" runat="server" Text="Consulting Period :" />
                                                            </td>
                                                            <td style="width: 13%">
                                                                <asp:TextBox ID="txtfrm" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                    runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                </cc1:CalendarExtender>
                                                                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                    width: 20px; height: 20px;" />
                                                            </td>
                                                            <td style="width: 2%">
                                                                To
                                                            </td>
                                                            <td style="width: 13%">
                                                                <asp:TextBox ID="txtto" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                    Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                </cc1:CalendarExtender>
                                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                    width: 20px; height: 20px;" />
                                                            </td>
                                                            <td style="width: 8%">
                                                                <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                                                    OnClick="btnView_Click" />
                                                            </td>
                                                            <td style="width: 48%; color: Red; text-align: right;">
                                                                <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 100%;
                                                        border: 1px solid #C1C1C1;">
                                                        <tr>
                                                            <th class="head2">
                                                                Name
                                                            </th>
                                                            <th class="head4">
                                                                DOC
                                                            </th>
                                                            <th class="head4i">
                                                                Address
                                                            </th>
                                                            <th class="head5">
                                                                Case Paper No
                                                            </th>
							    <th class="head6">
								Reconsulting No.
                                                            </th>
                                                            <th class="head5i">
                                                                Mobile No.
                                                            </th>

                                                            <th class="head1" style="display:none";>
                                                                KCO
                                                            </th>
							    <th class="head7">
                                    Doctor
                                                            </th>
                                                            <th class="head7">
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th class="head2">
                                                                <asp:TextBox ID="txtname" runat="server" class="field" Width="150px"></asp:TextBox>
                                                            </th>
                                                            <th class="head4">
                                                            </th>
                                                            <th class="head4i">
                                                                <asp:TextBox ID="txtAdd" runat="server" class="field" Width="150px"></asp:TextBox>
                                                            </th>
                                                            <th class="head5">
                                                                <asp:TextBox ID="txtcasepaperno" runat="server" class="field" Width="100px"></asp:TextBox>
                                                            </th>
							    <th class="head6">
                                                                <asp:TextBox ID="txtReconsultingNo" runat="server" class="field" Width="100px"></asp:TextBox>
                                                            </th>
                                                            <th class="head5i">
                                                                <asp:TextBox ID="txtMobno" runat="server" class="field" Width="100px"></asp:TextBox>
                                                            </th>
                                                            <th class="head1" style="display:none";>
                                                            </th>
                                                            <th class="head7">
                                                            </th>
                                                             <th class="head7">
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="gvcol">
                                                    <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                        CssClass="mGrid" PagerStyle-CssClass="pgr" ShowHeader="false" Width="100%" OnRowCommand="GRV1_RowCommand"
                                                        OnRowCancelingEdit="GRV1_RowCancelingEdit" OnRowEditing="GRV1_RowEditing" OnRowUpdating="GRV1_RowUpdating"
                                                        DataKeyNames="pdID" OnPageIndexChanging="GRV1_PageIndexChanging" EmptyDataText="No Records Found."
                                                        OnRowDataBound="GRV1_RowDataBound">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="18%" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>

                                                                <% if (Session["AppType"].ToString() == "View")
                                                                    {%>	
                                                                    <asp:LinkButton ID="lnkpdName" runat="server" Text='<%# Eval("pdName")%>' CommandArgument='<%# Eval("pdID") %>'
                                                                        CommandName="case" Style="text-decoration: none; color: Blue; font-weight: normal;"></asp:LinkButton>
                                                                         <%
                                                                             }
                                                                         %>

                                                                        <% else
                                                                            {%>	
                                                                         <asp:Label ID="lnkpdNameAks" ForeColor="DarkBlue" runat="server" Text='<%# Eval("pdName")%>'></asp:Label>
                                                                        <%
                                                                            }
                                                                         %>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-Width="11%" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <%# Eval("pdDOC", "{0:dd-MMM-yy}")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <%# Eval("pdAdd")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcpno" runat="server" Text='<%# Eval("pdCasePaperNo")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtcpno" runat="server" Text='<%# Eval("pdCasePaperNo") %>' Width="100px"></asp:TextBox>
                                                                    <asp:HiddenField ID="hidcpno" runat="server" Value='<%# Eval("pdID") %>' />
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
							    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
								<ItemTemplate>
								    <%# Eval("ReconsultingNo")%>
								</ItemTemplate>
							    </asp:TemplateField>
							    <asp:TemplateField>
								<ItemTemplate>	
									<%# Eval("pdContact")%>
								</ItemTemplate>
							    </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
								<ItemTemplate>	
									<%--<asp:Label ID="lbldoctor" runat="server" Text='<%# Eval("Country") %>' Visible = "false" />--%>
                <asp:DropDownList ID="ddldoctors" runat="server">
                </asp:DropDownList>
								</ItemTemplate>
							    </asp:TemplateField>


                                                            <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcno" runat="server" Text='<%# Eval("pdCassetteNo")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtcno" runat="server" Text='<%# Eval("pdCassetteNo") %>' Width="100px"></asp:TextBox>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField ItemStyle-Width="10%" HeaderText="KCO" Visible="false">
                                                                <ItemTemplate>
                                                                    <a class="example80 gridViewToolTip" href="#">KCO_<%# Eval("pdID") %>
                                                                    </a>
                                                                    <div id="tooltip" style="display: none;">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblkco" runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <div style="margin-top: 5px; float: left;">
                                                                        <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("pdID") %>'
                                                                            CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" Visible="false" />
                                                                        <%--<asp:ImageButton ID="btnCase" runat="server" CommandArgument='<%# Eval("pdID") %>' CommandName="case" ImageUrl="Images/medical_bag.png" ToolTip="Case" />--%>
                                                                      
                                                                      <% if (Session["AppType"].ToString() == "N")
                                                                          {%>	
                                                                      
                                                                        <asp:ImageButton ID="genAppoinment" runat="server" CommandArgument='<%# Eval("pdID") %>'
                                                                            CommandName="app" ImageUrl="Images/appointment.png" ToolTip="Set Appointment" />
                                                                             <%
                                                                                 }
                                                                                %>


                                                                                   <% if (Session["AppType"].ToString() == "C")
                                                                                       {%>	
                                                                              <asp:ImageButton ID="imgbtn_RegOPD" runat="server" CommandArgument='<%# Eval("pdID") %>'
                                                                            CommandName="ADDTODAYOPD" ImageUrl="Images/button_ok.png" ToolTip="Add Todays OPD" />
                                                                             <%
                                                                                 }
                                                                                %>
                                                                                <% if (Session["AppType"].ToString() == "W")
                                                                                    {%>	
                                                                              <asp:ImageButton ID="image_waitingList" runat="server" CommandArgument='<%# Eval("pdID") %>'
                                                                            CommandName="AddToWaitingList" ImageUrl="Images/add.png" ToolTip="Add Waiting List" />
                                                                             <%
                                                                                 }
                                                                                %>
										 <% if (Session["AppType"].ToString() == "T")
                                             {%>	<asp:TextBox ID="txtDate" runat="server" Width="50%" placeholder="DD/MM/YYYY" CssClass="field"></asp:TextBox>
										<cc1:CalendarExtender ID="txtDate_CalenderExtender" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgDate" TargetControlID="txtDate"></cc1:CalendarExtender>
												<asp:Image ID="imgDate" runat="server" ImageUrl="~/images/calimg.gif" Style="position:inherit;width: 18px; height: 18px; top: 5px; left: -5px;" />
                                                                              <asp:ImageButton ID="image_TodaysList" runat="server" CommandArgument='<%# Eval("pdID") %>'
                                                                            CommandName="AddTodaysList" ImageUrl="Images/button_ok.png" ToolTip="Add Waiting List" />
										<asp:CheckBox ID="chkExtraCases" runat="server" />
										<asp:Label ID="lblResult" runat="server" Visible="false"></asp:Label>
                                                                             <%
                                                                                 }
                                                                                %>
										
                                                                    </div>
                                                                    <div style="float: right; margin-top: 9px; margin-right: 10px;">
                                                                        <asp:Repeater ID="rptReconsult" runat="server" OnItemCommand="rptReconsult_ItemCommand">
                                                                            <HeaderTemplate>
                                                                                <div class="ui-buttonset">
                                                                                    <button id="rerun" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left rerun"
                                                                                        role="button" style="display: none;" onclick="javascript:void(0)">
                                                                                        <span class="ui-button-text">Action</span></button>
                                                                                    <button id="select" class="ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-right"
                                                                                        role="button" title="Select an action" onclick="javascript:void(0)">
                                                                                        <span class="ui-button-icon-primary ui-icon ui-icon-triangle-1-s"></span><span class="ui-button-text">
                                                                                            Select</span></button>
                                                                                </div>
                                                                                <ul style="display: none; top: 34px; left: 115.7px; z-index: 1;" id="ui-id-1" class="ui-menu ui-widget ui-widget-content"
                                                                                    role="menu" tabindex="0" aria-activedescendant="ui-id-2">
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <li class="ui-menu-item" id="ui-id-2" tabindex="-1" role="menuitem">
                                                                                    <asp:LinkButton ID="lnk_btn_Task" runat="server" CommandArgument='<%# Eval("fdFlag") %>'
                                                                                        CommandName="watch" ToolTip="Add Reconsulting Details" Text='<%# Eval("fdDate","{0:dd/MM/yyyy}") %>'>
                                                                                                    <img src="images/view.png" title="View Project" style="margin-bottom:-4px;"/> 
                                                                                                    <span style="font-size:10px;">View</span>
                                                                                    </asp:LinkButton>
                                                                                </li>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </ul>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:ImageButton ID="btnEditCancel" runat="server" CommandArgument='<%# Eval("pdID") %>'
                                                                        CommandName="cancel" ImageUrl="images/cancel.ico" ToolTip="Cancel" />
                                                                    <asp:ImageButton ID="btnSave" runat="server" CommandArgument='<%# Eval("pdID") %>'
                                                                        CommandName="update" ImageUrl="images/update.ico" ToolTip="Update" />
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    <%--</div>
                                </div>
                                <div class="clr">
                                </div>
                            </div>
                        </div>--%>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <%--</ContentTemplate></asp:UpdatePanel>--%>
    </form>
</body>
</html>
