﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="ConsultPatient.aspx.cs" Inherits="ConsultPatient" Title="Consult Patient" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="tab1.ascx" TagName="Tab" TagPrefix="tab1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <script type="text/javascript">
        function CalculateRemedyCharges() {
            var period = document.getElementById("<%= ddRemedyPeriod.ClientID %>").value;
            var dose = document.getElementById("<%= ddDoseType.ClientID %>").value;
            var charge = 0
            var Newperiod = period.split('-');
            var IDS = Newperiod[0];
            var DAY = Newperiod[1];



            if (Newperiod != "") {
                SetFollowUp(DAY);
            }

            if (IDS.trim() == "7" || IDS.trim() == "8" || IDS.trim() == "9" || IDS.trim() == "10" || IDS.trim() == "11" || IDS.trim() == "12") {

                if (dose != "") {
                    if (dose == "1") {
                        charge = 10 * DAY
                    }
                    if (dose == "2") {
                        charge = 20 * DAY
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 50 * DAY
                    }
                }
            }

            else if (IDS.trim() == "13") {
                if (dose != "") {
                    if (dose == "1" || dose == "2" || dose == "3" || dose == "6") {
                        charge = 0
                    }

                }
            }

            else if (IDS.trim() == "1") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 80
                    }
                    if (dose == "2") {
                        charge = 150
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 400
                    }
                }
            }

            else if (IDS.trim() == "2") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 160
                    }
                    if (dose == "2") {
                        charge = 300
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 800
                    }
                }
            }

            else if (IDS.trim() == "3") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 240
                    }
                    if (dose == "2") {
                        charge = 450
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 1200
                    }
                }
            }


            else if (IDS.trim() == "4") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 300
                    }
                    if (dose == "2") {
                        charge = 600
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 1600
                    }
                }
            }


            else if (IDS.trim() == "5") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 600
                    }
                    if (dose == "2") {
                        charge = 1200
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 3200
                    }
                }
            }

            else if (IDS.trim() == "6") {
                if (dose != "") {
                    if (dose == "1") {
                        charge = 900
                    }
                    if (dose == "2") {
                        charge = 1800
                    }
                    if (dose == "3" || dose == "5") {
                        charge = 4800
                    }
                }
            }

            if (charge >= 0) {
                document.getElementById("<%= txtCharges.ClientID %>").value = charge;
            }

        }

        function SetFollowUp(days) {
            var doc = document.getElementById("<%= txtDOC.ClientID %>").value;
            //alert(doc);
            if (doc != "") {
                _doc = doc.split('/');
                if (_doc.length == 3) {
                    datedoc = new Date(_doc[2], _doc[1] * 1 - 1, _doc[0]);
                    followdate = new Date(datedoc.getTime() + days * 24 * 60 * 60 * 1000);
                    var _day = followdate.getDate()

                    if (_day.toString().length == 1) {
                        _day = "0" + _day;
                    }
                    var _month = parseFloat(followdate.getMonth()) + 1;

                    if (_month.toString().length == 1) {
                        _month = "0" + _month;
                    }
                    var _year = followdate.getFullYear();

                    document.getElementById("<%= txtNfollow.ClientID %>").value = _day + "/" + _month + "/" + _year;

                    //alert(followdate.getDate()+"\n"+followdate.getMonth()+"\n"+followdate.getFullYear());
                }
            }
        }
    </script>
    <script type="text/javascript" language="javascript">
        function DisplayCurrior() {
            if (document.getElementById("<%= chkIsCourier.ClientID%>").checked == true) {
                document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'block';
            }
            else {
                document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'none';
            }
        }

        function OnModeChange() {
            var com;
            com = document.getElementById("<%= chkIsCourier.ClientID%>").checked;
            if (com == true) {

                document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'block';

                var charges;
                charges = document.getElementById("<%= txtCharges.ClientID %>").value;
                var date;
                date = document.getElementById("<%= txtDOC.ClientID %>").value;
                //           alert(charges);
                //           alert(date);

                document.getElementById("<%= txtCCharges.ClientID %>").value = charges;
                document.getElementById("<%= txtCourierDate.ClientID%>").value = date;


                ValidatorEnable(document.getElementById("<%= rcname.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcaddress.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rccharge.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDate.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDocketNo.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDDCourier.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcMobileNo.ClientID %>"), true);

            }

            else {

                document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'none';

                ValidatorEnable(document.getElementById("<%= rcname.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcaddress.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rccharge.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDate.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDocketNo.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDDCourier.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcMobileNo.ClientID %>"), false);
            }
        }

        function OnModeChangeOnline() {
            var com;
            com = document.getElementById("<%= chkIsCourier.ClientID%>").checked;
            if (com == true) {

                document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'block';

                var charges;
                charges = document.getElementById("<%= txtCharges.ClientID %>").value;
                var date;
                date = document.getElementById("<%= txtDOC.ClientID %>").value;
                //           alert(charges);
                //           alert(date);

                document.getElementById("<%= txtCCharges.ClientID %>").value = charges;
                document.getElementById("<%= txtCourierDate.ClientID%>").value = date;


                ValidatorEnable(document.getElementById("<%= rcname.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcaddress.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rccharge.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDate.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDocketNo.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcDDCourier.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= rcMobileNo.ClientID %>"), true);

            }

            else {

                document.getElementById("<%= pnlcourier.ClientID%>").style.display = 'none';

                ValidatorEnable(document.getElementById("<%= rcname.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcaddress.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rccharge.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDate.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDocketNo.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcDDCourier.ClientID %>"), false);
                ValidatorEnable(document.getElementById("<%= rcMobileNo.ClientID %>"), false);
            }
        }         
    </script>
    <style>
        .reqPos
        {
            position: absolute;
        }
    </style>
    <script type="text/javascript">
        function addDays(myDate, days) {
            return new Date(myDate.getTime() + days * 24 * 60 * 60 * 1000);
        }
    </script>
    <script language="javascript" type="text/javascript">
        function checkCollectionDateAks(sender, args) {
            var x = new Date('2016-09-13');
            if (sender._selectedDate < x) {
                alert("You Can Not select A Date Less Than To 2016-09-13 Date...!!!");
                sender._selectedDate = new Date();
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Consult Patient</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <%--<ul class="quicktabs_tabs quicktabs-style-excel">
                                <li class="qtab-Demo active first" id="li_1">
                                    <a class="qt_tab active" href="javascript:void(0)">Add New Patients</a>
                                </li>
                                
                                <li class="qtab-HTML last" id="li_9">
                                    <a class="qt_tab active" href="ManagePatients.aspx">Manage Patients</a>
                                </li>
                                
                                </ul>--%>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto; border: 1.5px solid #E2E2E2;">
                                        <table style="width: 100%;" cellspacing="0px" cellpadding="2px">
                                            <tr>
                                                <td colspan="2">
                                                    <tab1:Tab ID="tabcon" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <%--<tr>
                                        <td>
                                            Date Of Birth*
                                         </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtDOB" runat="server" CssClass="field" Width="70px"
                                            onblur="CalculateAge(this)" onchange="CalculateAge(this)"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtDOB_CalendarExtender" runat="server" 
                                                Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgDOB" 
                                                TargetControlID="txtDOB">
                                            </cc1:CalendarExtender>
                                            <asp:Image ID="imgDOB" runat="server" 
                                                ImageUrl="~/images/calimg.gif" style="position: absolute; width:20px; height:20px;"/>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>--%>
                                            <tr>
                                                <td>
                                                    Consulting Date*
                                                </td>
                                                <td>
                                                    Remedy*
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtDOC" runat="server" CssClass="required field" Width="70px" onchange="CalculateRemedyCharges()"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txtDOC_CalendarExtender" runat="server" Enabled="True"
                                                        OnClientDateSelectionChanged="checkCollectionDateAks" Format="dd/MM/yyyy" PopupButtonID="imgDOC"
                                                        TargetControlID="txtDOC">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="imgDOC" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                        width: 20px; height: 20px;" />
                                                    <asp:RequiredFieldValidator ID="valDOC" runat="server" ControlToValidate="txtDOC"
                                                        ErrorMessage="Date Of Consulting is Required." CssClass="reqPos">Date Of Consulting is Required.</span>
                                                    </asp:RequiredFieldValidator>
                                                    <br />
                                                    <%--  <asp:RangeValidator runat="server" ForeColor="Red" id="rngDate" Display="Dynamic" controltovalidate="txtDOC" minimumvalue="13/09/2016"  maximumvalue="31/12/2200"
                                                    errormessage="Please Select Date Greater Than 13-09-2016" />--%>
                                                </td>
                                                <td>
                                                    <%-- <asp:TextBox ID="txtRemedy" runat="server" Width="250px" CssClass="required field"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valRemedy" runat="server" ControlToValidate="txtRemedy"
                                                        ErrorMessage="Remedy is Required." CssClass="reqPos"><span class="error">Remedy is Required.</span>
                                                    </asp:RequiredFieldValidator>--%>
                                                    <asp:DropDownList ID="DDL_RemList" runat="server" Width="170px" CssClass="field">
                                                    </asp:DropDownList>
						    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ControlToValidate="DDL_RemList" ValidationGroup="check" ErrorMessage="Please Select Remedy."><span class="error">Please Select Remedy.</span></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Remedy Period *
                                                </td>
                                                <td>
                                                    Dose Type *
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddRemedyPeriod" runat="server" CssClass="required field" Width="130px"
                                                        onchange="CalculateRemedyCharges()" AutoPostBack="True" OnSelectedIndexChanged="ddRemedyPeriod_SelectedIndexChanged">
                                                        <%--  <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="1 Day" Value="8"></asp:ListItem>
                                                        <asp:ListItem Text="2 Days" Value="9"></asp:ListItem>
                                                        <asp:ListItem Text="3 Days" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="4 Days" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="5 Days" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="6 Days" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="7 Days" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="8 Days" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="9 Days" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="10 Days" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="11 Days" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="12 Days" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="13 Days" Value="20"></asp:ListItem>
                                                        <asp:ListItem Text="14 Days" Value="21"></asp:ListItem>
                                                        <asp:ListItem Text="15 Days" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="1 Month" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="2 Month" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="3 Month" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="6 Month" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="IPD" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="Free" Value="11"></asp:ListItem>--%>
                                                    </asp:DropDownList>
						    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="ddRemedyPeriod" ValidationGroup="check" ErrorMessage="Please Select Remedy Period."><span class="error">Please Select Remedy Period.</span></asp:RequiredFieldValidator>--%>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddDoseType" runat="server" CssClass="required field" Width="170px"
                                                        onchange="CalculateRemedyCharges()">
							<%--<asp:ListItem Text="-Select-" Value=""></asp:ListItem>--%>
                                                        <asp:ListItem Text="Hourly(10 Times/Day)" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="Double(6 Times/Day)" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Single(3 Times/Day)" Value="1"></asp:ListItem>
							<asp:ListItem Text="Add Emg" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="Extra Doses" Value="5"></asp:ListItem>
                                                        <%-- <asp:ListItem Text="Liquid" Value="4"></asp:ListItem>--%>
                                                        <asp:ListItem Text="Liquid" Value="5"></asp:ListItem>
                                                        <%-- <asp:ListItem Text="Single" Value="6"></asp:ListItem> --%>
                                                    </asp:DropDownList>
						    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ControlToValidate="ddDoseType" ValidationGroup="check" ErrorMessage="Please Select Dose Type."><span class="error">Please Select Dose Type.</span></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Remedy Charges*
                                                </td>
                                                <td>
                                                    Consulting Charges*
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtCharges" runat="server" CssClass="field" Width="120px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valRemedyCharge" runat="server" ControlToValidate="txtCharges"
                                                        ErrorMessage="Remedy Charge is Required." CssClass="reqPos"><span class="error">Remedy Charge is Required.</span>
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regremcharge" runat="server" ControlToValidate="txtCharges"
                                                        ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                                        ValidationGroup="check">
                                            <span class="error">Please enter Only Numbers.</span>
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCOnCharges" runat="server" Enabled="false" Width="160px" CssClass="field"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valccharge" runat="server" ControlToValidate="txtCOnCharges"
                                                        ErrorMessage="Consulting Charge is Required." CssClass="reqPos"><span class="error">Consulting Charge 
                                            is Required is Required.</span>
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regconcharge" runat="server" ControlToValidate="txtCOnCharges"
                                                        ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                                        ValidationGroup="check">
                                            <span class="error">Please enter Only Numbers.</span>
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Next Follow Up
                                                </td>
                                                <td>
                                                    Patient Observation
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtNfollow" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txtNfollow_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgNfollow" TargetControlID="txtNfollow">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="imgNfollow" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                        width: 20px; height: 20px;" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="DDL_PatObservation" runat="server" CssClass="required field"
                                                        Width="170px">
                                                        <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Good" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Not Good" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="High" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="Low" Value="4"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Additional Emergency
                                                </td>
                                                <td>
                                                    Extra doses
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="DDL_AddEmg" runat="server" Width="170px" CssClass="field">
                                                        <asp:ListItem Text="Select" Value="0" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                                        <asp:ListItem Text="200" Value="200"></asp:ListItem>
                                                        <asp:ListItem Text="300" Value="300"></asp:ListItem>
                                                        <asp:ListItem Text="400" Value="400"></asp:ListItem>
                                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                                        <asp:ListItem Text="600" Value="600"></asp:ListItem>
                                                        <asp:ListItem Text="700" Value="700"></asp:ListItem>
                                                        <asp:ListItem Text="800" Value="800"></asp:ListItem>
                                                        <asp:ListItem Text="900" Value="900"></asp:ListItem>
                                                        <asp:ListItem Text="1000" Value="1000"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="DDL_ExtranDos" runat="server" Width="170px" CssClass="field">
                                                        <asp:ListItem Text="Select" Value="0" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="5-100" Value="100"></asp:ListItem>
                                                        <asp:ListItem Text="10-200" Value="200"></asp:ListItem>
                                                        <asp:ListItem Text="15-300" Value="300"></asp:ListItem>
							<asp:ListItem Text="20-400" Value="400"></asp:ListItem>
				    			<asp:ListItem Text="25-500" Value="500"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Remark
                                                    <%--FP Cassette No--%>
                                                </td>
                                                <td>
                                                    <%--Payment Mode *--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtRemark" CssClass="field" runat="server" TextMode="MultiLine"
                                                        Width="250px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="170px" CssClass="field"
                                                        OnSelectedIndexChanged="DDL_Pay_Mode_SelectedIndexChanged" Visible="false">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                        <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                                        <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                        <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                        <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                        <%--<asp:ListItem Text="Other" Value="Other"></asp:ListItem>--%>
                                                        <asp:ListItem Text="Free" Value="Free"></asp:ListItem>
							<asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                        Visible="false" Display="Dynamic" ErrorMessage="Please specify Payment Mode"
                                                        ValidationGroup="check">
                                                        <span class="error" >Please specify Payment Mode</span></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr id="tr_Bank" runat="server" style="display: none;">
                                                <td>
                                                    Card Name
                                                </td>
                                                <td>
                                                    Card Type
                                                </td>
                                            </tr>
                                            <tr id="tr_Cheque" runat="server" style="display: none;">
                                                <td>
                                                    <asp:TextBox ID="txtCardnm" runat="server" CssClass="field" Width="170px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="DDL_CardType" runat="server" Width="160px" CssClass="field"
                                                        AutoPostBack="true">
                                                        <asp:ListItem Text="Debit" Value="Debit" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Credit" Value="Credit"></asp:ListItem>
                                                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="tr_Bank1" runat="server" style="display: none;">
                                                <td>
                                                    Reference No.
                                                </td>
                                                <td style="display: none;">
                                                    Cheque Date
                                                </td>
                                            </tr>
                                            <tr id="tr_Cheque1" runat="server" style="display: none;">
                                                <td>
                                                    <asp:TextBox ID="txtCardno" runat="server" CssClass="field" MaxLength="6" Width="170px"></asp:TextBox>
                                                </td>
                                                <td class="last">
                                                    <asp:TextBox ID="txtChqdt" Visible="false" runat="server" CssClass="field" Width="200px"
                                                        placeholder="DD/MM/YYYY"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                        TargetControlID="txtChqdt">
                                                    </cc1:CalendarExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="chkIsCourier" type="checkbox" value="ConsFlag" runat="server" onclick="javascript:OnModeChange();" />
                                                    <label>
                                                        Is Courier</label>
                                                </td>
                                                <td>
                                                    <input id="chk_ExtraDoses" type="checkbox" value="ConsFlag" runat="server" onchange="CalculateRemedyCharges();" />
                                                    <label>
                                                        Is Extra Doses
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                        ValidationGroup="check" Style="margin-top: 2px;" Text="Save" /><br />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Panel ID="pnlcourier" runat="server" Style="display: none;">
                                                        <table cellpadding="0" cellspacing="0" width="100%" border="1px">
                                                            <tr>
                                                                <td>
                                                                    Receiver Name
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCName" runat="server" Width="400px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rcname" runat="server" ControlToValidate="txtCName"
                                                                        CssClass="reqPos" ErrorMessage="Receiver Name Is Required."><span class="error">Receiver Name Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Address
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCAddress" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rcaddress" runat="server" Enabled="false" ControlToValidate="txtCAddress"
                                                                        CssClass="reqPos" ErrorMessage="Address Is Required."><span class="error">Address Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    MoblieNo
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCMobileNo" runat="server" Width="200px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rcMobileNo" runat="server" Enabled="false" ControlToValidate="txtCMobileNo"
                                                                        CssClass="reqPos" ErrorMessage="Mobile No Is Required."><span class="error">Mobile No Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    EmailID
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtcEmailID" runat="server" Width="200px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Courier Name
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DDCourier" runat="server" Width="200px">
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="rcDDCourier" runat="server" Enabled="false" ControlToValidate="DDCourier"
                                                                        CssClass="reqPos" ErrorMessage="Courier Name Is Required."><span class="error">Courier Name Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Courier Date
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:TextBox ID="txtCourierDate" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy"
                                                                        PopupButtonID="imgcfollow" TargetControlID="txtCourierDate">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:Image ID="imgcfollow" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                        width: 20px; height: 20px;" />
                                                                    <asp:RequiredFieldValidator ID="rcDate" runat="server" ControlToValidate="txtCourierDate"
                                                                        CssClass="reqPos" ErrorMessage="Date Is Required." Enabled="false"><span class="error">Date Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    DocketNo
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCDocketNo" runat="server" Width="200px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rcDocketNo" runat="server" Enabled="false" ControlToValidate="txtCDocketNo"
                                                                        CssClass="reqPos" ErrorMessage="Docket No Is Required."><span class="error">Docket No Is Required.</span>
                                                                    </asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Charges
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCCharges" runat="server" Width="200px"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="rccharge" runat="server" ControlToValidate="txtCCharges"
                                                                        ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                                                        ValidationGroup="check">
                                                                        <span class="error">Please enter Only Numbers.</span>
                                                                    </asp:RegularExpressionValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Payment Mode *
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DDL_CourierPaymentMode" runat="server" Width="200px" CssClass="field">
                                                                        <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                                        <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                                                        <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                                        <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                                        <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                                        <%--<asp:ListItem Text="Other" Value="Other"></asp:ListItem>--%>
									<asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
