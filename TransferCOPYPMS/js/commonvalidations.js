﻿//function to validate date in dd//mm/yyyy format


function IsValidDate(Day, Mn, Yr)
 {

    //alert(Day + "\n" + Mn + "\n" + Yr);
    var DateVal = Mn + "/" + Day + "/" + Yr;
    var dt = new Date(DateVal);

    if (dt.getDate() != Day) {
        //alert('Invalid Date');
        return (false);
    }
    else if (dt.getMonth() != Mn - 1) {
        //this is for the purpose JavaScript starts the month from 0
        //alert('Invalid Date');
        return (false);
    }
    else if (dt.getFullYear() != Yr) {
        //alert('Invalid Date');
        return (false);
    }

    return (true);
}

//Calling IsValidDate from Custome Validation 
function CallDateFun(sender, args) {

    var dt = args.Value.split('/')
    
    if (IsValidDate(dt[0], dt[1], dt[2]))
        args.IsValid = true;
    else
        args.IsValid = false;
}


//Function to check valid time
function CallTimeFun(sender,args){
    var _t1 = args.Value.split(' ');    
    var _t2 = _t1[0].split(':');
    
    var hh = parseInt(_t2[0]);
    var mm = parseInt(_t2[1]);
    var ss = parseInt(_t2[2]);
    
    if(hh <= 12 && mm <= 59 && ss <= 59)
    {
        args.IsValid = true;
    }
    else
    {
        args.IsValid = false;
    }
    
}


//Function to get difference in two dates

function dateDiff(dt1, dt2)
{
    /*
     * setup 'empty' return object
     */
    var ret = {days:0, months:0, years:0};

    /*
     * If the dates are equal, return the 'empty' object
     */
    if (dt1 == dt2) return ret;

    /*
     * ensure dt2 > dt1
     */
    /*if (dt1 > dt2)
    {
        var dtmp = dt2;
        dt2 = dt1;
        dt1 = dtmp;
    }*/

    /*
     * First get the number of full years
     */

    var year1 = dt1.getFullYear();
    var year2 = dt2.getFullYear();

    var month1 = dt1.getMonth();
    var month2 = dt2.getMonth();

    var day1 = dt1.getDate();
    var day2 = dt2.getDate();

    /*
     * Set initial values bearing in mind the months or days may be negative
     */

    ret['years'] = year2 - year1;
    ret['months'] = month2 - month1;
    ret['days'] = day2 - day1;

    /*
     * Now we deal with the negatives
     */

    /*
     * First if the day difference is negative
     * eg dt2 = 13 oct, dt1 = 25 sept
     */
    if (ret['days'] < 0)
    {
        /*
         * Use temporary dates to get the number of days remaining in the month
         */
        var dtmp1 = new Date(dt1.getFullYear(), dt1.getMonth() + 1, 1, 0, 0, -1);

        var numDays = dtmp1.getDate();

        ret['months'] -= 1;
        ret['days'] += numDays;

    }

    /*
     * Now if the month difference is negative
     */
    if (ret['months'] < 0)
    {
        ret['months'] += 12;
        ret['years'] -= 1;
    }

    alert(ret['days']+"\n"+ret['months']+"\n"+ret['years'] +"\n")
    return ret;
}


//function to get date difference in days
function DateDiffInDays(dt1,dt2)
{
    var millisecondsPerDay = 1000 * 60 * 60 * 24;
    var millisBetween = dt2.getTime() - dt1.getTime();
    var days = millisBetween / millisecondsPerDay;
    
    return days;
}


//function to check datediff less than 3 than todays date

function DateDiffLessThanThree(sender, args)
{
    var dtPassed = args.Value.split('/')
    var DateVal = dtPassed[1] + "/" + dtPassed[0] + "/" + dtPassed[2];
    var dt1 = new Date(DateVal);
    
    var dt2 = new Date((new Date().getMonth()+1)+"/"+new Date().getDate()+"/"+ new Date().getFullYear());
    
    var days = DateDiffInDays(dt1,dt2);
    if(days < 0 || days > 3)
    {
        args.IsValid = false;
    }
    else
    {
        args.IsValid =  true;
    }
}

function FutureDate(sender, args)
{
    var dtPassed = args.Value.split('/')
    var DateVal = dtPassed[1] + "/" + dtPassed[0] + "/" + dtPassed[2];
    var dt1 = new Date(DateVal);
    
    var dt2 = new Date((new Date().getMonth()+1)+"/"+new Date().getDate()+"/"+ new Date().getFullYear());
    
    var days = DateDiffInDays(dt2,dt1)
    if(days <= 0)
    {
        args.IsValid = false;
    }     
    else 
    {
        args.IsValid = true;
    }
}
function FutureDateGrater(sender, args)
{
    var dtPassed = args.Value.split('/')
    var DateVal = dtPassed[1] + "/" + dtPassed[0] + "/" + dtPassed[2];
    var dt1 = new Date(DateVal);
    
    var dt2 = new Date((new Date().getMonth()+1)+"/"+new Date().getDate()+"/"+ new Date().getFullYear());
    
    var days = DateDiffInDays(dt2,dt1)
    if(days > 0)
    {
        args.IsValid = false;
    }     
    else 
    {
        args.IsValid = true;
    }
}
//function to check less than given date 
function DateLessThanGivenDate(dt1,dt2)
{
    var _dt1 = dt1.split('/')
    var _dt2 = dt2.split('/')
    
    var dt1_val = new Date(_dt1[1]+"/"+_dt1[0]+"/"+_dt1[2]);
    var dt2_val = new Date(_dt2[1]+"/"+_dt2[0]+"/"+_dt2[2]);
    
    if(dt1_val > dt2_val)
    {
        return false;
    }
    
}


//Function to Validate Max Length

function MaxLengthValTwenty(sender, args) {
    if (args.Value.length > 20) {
        args.IsValid = false;
    }
}

function MaxLengthValFifty(sender, args) {
    if (args.Value.length > 50) {
        args.IsValid = false;
    }
}

function MaxLengthValTwoHundred(sender, args) {
    if (args.Value.length > 200) {
        args.IsValid = false;
    }
}

function MaxLengthValFiveHundred(sender, args) {
    if (args.Value.length > 500) {
        args.IsValid = false;
    }
}

function MaxLengthValHundred(sender, args) {
    if (args.Value.length > 100) {
        args.IsValid = false;
    }
}

function MaxLengthValTen(sender, args) {
    if (args.Value.length > 10) {
        args.IsValid = false;
    }
}

function MaxLengthValFifteen(sender, args) {
    if (args.Value.length > 15) {
        args.IsValid = false;
    }
}

function MaxLengthValThousand(sender, args) {
    if (args.Value.length > 1000) {
        args.IsValid = false;
    }
}

//Function to validate Max-Min Length
function MaxFifteenMinTen(sender,args){
    if(args.Value.length > 15 || args.Value.length < 10){
        args.IsValid = false;
    }

}

function MaxFiftyMinTen(sender,args){
    if(args.Value.length > 50 || args.Value.length < 10){
        args.IsValid = false;
    }

}

function Max13Min10(sender,args){
    if(args.Value.length < 10|| args.Value.length > 13){
        args.IsValid = false;
    }
}

function Max15Min7(sender,args){
    if(args.Value.length < 7 || args.Value.length > 15){
        args.IsValid = false;
    }
}




//---------------------------Validation To Check Non numeric only----------------------------------------------------

function IsNumeric(sender, args)
{
    if(!isNaN(args.Value))
    {
        args.IsValid = false;
    }
    
}


//---------------------------Validation To Check File Extension----------------------------------------------------

function UploadFileCheck(source, arguments) {
    var sFile = arguments.Value;
    arguments.IsValid = 
       ((sFile.endsWith('.pdf')) ||
        (sFile.endsWith('.PDF')));
}

function UploadImageCheck(source, arguments) {
    var sFile = arguments.Value;
    arguments.IsValid = 
       ((sFile.endsWith('.png')) ||
        (sFile.endsWith('.PNG')));
}
//-------------------------------------------------------------------------------------------------------------------
function SmartPager(currentSection, TotalSection, size, howmanypages, currentpage) {
    
    for (var i = 1; i <= howmanypages; i++) {
        document.getElementById(i.toString()).className = "leftflt hide padleft_pager";
    }
    var divid = (currentSection * size) - (size - 1);
    for (var i = 1; i <= size; i++) {
        if(i==1){
            document.getElementById(divid.toString()).className = "leftflt show padleft_pager pgrleftborder";
        }
        else{
            document.getElementById(divid.toString()).className = "leftflt show padleft_pager pgrborder";
        }
        divid++;
        if(divid > howmanypages){
            break;
        }
    }
    var currdiv = document.getElementById(currentpage.toString())
    currdiv.className += " " + "activepgr";         
}

function AlphaPager(currentletter)
{

    for(var i = 65; i<=90;i++)
    {
        var chr = String.fromCharCode(i);
        
        if(chr == 'A'){
            document.getElementById(chr).className = "leftflt padleft_pager pgrleftborder";
        }      
        else{
            document.getElementById(chr).className = "leftflt padleft_pager pgrborder";            
        } 
    }
    document.getElementById('All').className = "leftflt padleft_pager pgrborder";
    var currdiv = document.getElementById(currentletter.toString())
    currdiv.className += " " + "activepgr";             
}


/*-------------Reg Exp for Number and symbol ---------------------------*/


    function SpecialName(sender,args)
    {
        alert("ok");
        var regNum = "/\d+/";
        var regSpecChar = "/\W/" 
        if(!regNum.test(args.Value))
        {
            alert("ok");
            args.IsValid = false;
        }
        if(!regSpecChar.test(args.Value))
        {
            alert("ok");
            argsIsValid = false;
        }
    }           
    
/*-------------Reg Exp for Number and symbol Ends here---------------------------*/    
    
    function DateDiffIsGretterThanZero(sender, args)
    {
       //alert ("test");
       now = new Date()
       dob = args.Value.split('/');
       born = new Date(dob[2], dob[1] * 1 - 1, dob[0]);
       age = Math.floor((now.getTime() - born.getTime()) / (365.25 * 24 * 60 * 60 * 1000));
       //alert(age);    
       if (age >= 0)
       {
         args.IsValid = true;
       }
       else
       {
         args.IsValid = false ;
       }
    }
    
    function NumSymbolCheck(sender, args)
    {
        var cnt  = 0; 
        for(var i = 0; i <= args.Value.length; i++)
        {
            //var pchar = args.Value.charAt(i);
            var pcode = args.Value.charCodeAt(i);
               
            //alert(cnt +"\n"+ pchar +"\n"+ pcode);

            if(pcode >= 32 && pcode <= 64)
            {
                cnt++;
            }
        }
        
        if(cnt == args.Value.length)
        {
            args.IsValid = false;
        }
        
    }



//Positioning Autocomplete Extender below target control

        function resetPosition(object, args)
        {

	        var tb = object._element;

	        var tbposition = findPositionWithScrolling(tb);
        	

	        var xposition = tbposition[0];

	        var yposition = tbposition[1] + 25;	// 25 = textbox height + a few pixels spacing

	        var ex = object._completionListElement;

	        if (ex)
	        {
		        $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
	        }
        }

        function findPositionWithScrolling( oElement )
        {

            if( typeof( oElement.offsetParent ) != 'undefined' )
            {

	            var originalElement = oElement;
	            for( var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent )
	            {

		            posX += oElement.offsetLeft;

		            posY += oElement.offsetTop;

		            if( oElement != originalElement && oElement != document.body && oElement != document.documentElement )
		            {
			            posX -= oElement.scrollLeft;
			            posY -= oElement.scrollTop;

		            }

	            }

	            return [ posX, posY ];
            } else {

            return [ oElement.x, oElement.y ];
            }
        }

    
        function OnClientPopulating(sender, e) {
        sender._element.className = "roundedbox loading";
        }
        function OnClientCompleted(sender, e) {
        sender._element.className = "roundedbox";
        }    