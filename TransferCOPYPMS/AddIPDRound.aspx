﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="AddIPDRound.aspx.cs" Inherits="AddIPDRound" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="asp" Namespace="Saplin.Controls" Assembly="DropDownCheckBoxes" %>
<%@ Register Src="tabipd.ascx" TagName="Tab" TagPrefix="tab1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
 <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
   <link rel="Stylesheet" type="text/css" href="css/CustomDDStyles.css" />

   <style type="text/css">
        .reqPos
        {
            position: absolute;
        }
        
        ._tableh1
        {
            background-image: url("images/tile_back1.gif");
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
            height: 18px;
            padding: 8px 12px 8px 8px;
            text-align: center;
        }
        
        #rowcon label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
            color: #056735;
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
        }
    </style>


    <style type="text/css">
          .colmiddle
        {
            font-family: Times New Roman;
            border-left: 1px solid #000000;
            border-right: 1px solid #000000;
            border-top: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }
       fieldset
        {
            padding: 0.5em 1em;
            border: solid Red 1px;
            width: 95%;
        }
        legend
        {
            font-weight: bold;
            color: #056735;
            font-family: Verdana;
        }
    </style>

      <script type="text/javascript">
          function CheckRangeForColor() {

              var PR = document.getElementById("<%= txt_PulseRt.ClientID %>").value;
              var SBP = document.getElementById("<%= txl_Bp.ClientID %>").value;
              var DBP = document.getElementById("<%= txt_Bp_Dia.ClientID %>").value;
              var Temp = document.getElementById("<%= txt_GlucTest.ClientID %>").value;
              var RR = document.getElementById("<%= txt_RR.ClientID %>").value;

             

              if (PR != "") {
                  if (PR > 90 || PR < 60) {
                      document.getElementById("<%= txt_PulseRt.ClientID %>").style.color = "#FF0000";
                    // document.getElementById("<%= DDL_INDC.ClientID %>").value = "2";
                  }
                  else {
                      document.getElementById("<%= txt_PulseRt.ClientID %>").style.color = "#000000";
                  }
              }

              if (SBP != "") {
                  if (SBP > 150 || SBP < 100) {
                      document.getElementById("<%= txl_Bp.ClientID %>").style.color = "#FF0000";
                      //document.getElementById("<%= DDL_INDC.ClientID %>").value = "2";
                  }
                  else {
                      document.getElementById("<%= txl_Bp.ClientID %>").style.color = "#000000";
                  }
              }

              if (DBP != "") {
                  if (DBP > 90 || DBP < 70) {
                      document.getElementById("<%= txt_Bp_Dia.ClientID %>").style.color = "#FF0000";
                     // document.getElementById("<%= DDL_INDC.ClientID %>").value = "2";
                  }
                  else {
                      document.getElementById("<%= txt_Bp_Dia.ClientID %>").style.color = "#000000";
                  }
              }

              if (Temp != "") {
                  if (Temp > 99) {
                      document.getElementById("<%= txt_GlucTest.ClientID %>").style.color = "#FF0000";
                    //  document.getElementById("<%= DDL_INDC.ClientID %>").value = "2";
                  }
                  else {
                      document.getElementById("<%= txt_GlucTest.ClientID %>").style.color = "#000000";
                  }
              }

              if (RR != "") {
                  if (RR > 20 || RR < 12) {
                      document.getElementById("<%= txt_RR.ClientID %>").style.color = "#FF0000";
                    //  document.getElementById("<%= DDL_INDC.ClientID %>").value = "2";
                  }
                  else {
                      document.getElementById("<%= txt_RR.ClientID %>").style.color = "#000000";
                  }
              }
             
          }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table align="center" cellpadding="5" cellspacing="5" style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 100%;" cellpadding="5" cellspacing="5" >
                    <h2>Doctors Round</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto; border: 1.5px solid #E2E2E2;">
                               
                               <table style="width: 100%;" cellpadding="5" cellspacing="5" >
                                    
                                    <tr>
                                        <td colspan="5">
                                            <tab1:Tab ID="tabcon" runat="server" />
                                        </td>
                                    </tr>

                                    <tr>
                                    <td  colspan="5">
                                      <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-HTML active first" id="li_lnkRound" runat="server">
                                            <asp:LinkButton ID="lnkRound" runat="server" CssClass="qt_tab active" Enabled="false">IPD Round</asp:LinkButton>
                                        </li>

                                          <li class="qtab-HTML" id="li_IpdRoundDetails" runat="server">
                                           <asp:LinkButton ID="lnkIpdRoundDetails" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=MR">Manage Round</asp:LinkButton></li>
                                     
                                        <li class="qtab-HTML" id="li_lnkExtraP" runat="server">
                                            <asp:LinkButton ID="lnkExtraP" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=EP">Extra Particulars</asp:LinkButton></li>
                                        
                                        <li class="qtab-HTML" id="li_lnkRemedies" runat="server">
                                            <asp:LinkButton ID="lnkRemedies" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=RD">Remedies Details</asp:LinkButton></li>
                                       
                                        <li class="qtab-HTML " id="li_lnkDiag" runat="server">
                                            <asp:LinkButton ID="lnkDiag" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=DG">Diagnosis</asp:LinkButton></li>
                                       
                                        <li class="qtab-HTML" id="li_lnkAdm"  runat="server">
                                            <asp:LinkButton ID="lnkAdm" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=AD">Addmission</asp:LinkButton></li>

                                             <li class="qtab-HTML" id="li_lnkAdv"  runat="server">
                                           <asp:LinkButton ID="lnkAdv" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=ADV">Advanced</asp:LinkButton></li>

                                        <li class="qtab-HTML" id="li_lnkWard"  runat="server">
                                         <asp:LinkButton ID="lnkWard" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=WD">Ward</asp:LinkButton>
                                           </li>
                                      
                                        <li class="qtab-HTML" id="li_lnkRelBed" runat="server" style="display:none;">
                                            <asp:LinkButton ID="lnkRelBed" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=RTB">Relative Bed</asp:LinkButton></li>  
                                   
                                      <li  class="qtab-HTML" id="li_lnkInv" runat="server">
                                            <asp:LinkButton ID="lnkInv" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=IN">Investigation</asp:LinkButton></li>  

                                    <li  class="qtab-HTML" id="li_lnkDiet" runat="server">
                                    <asp:LinkButton ID="lnkDiet" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=DT">Diet Details</asp:LinkButton></li>  

					<li class="qtab-Demo last" id="li_lnkVoucher" runat="server">
						<asp:LinkButton id="lnkVoucher" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=VD">Voucher</asp:LinkButton></li>
                                    </ul>
                                    </td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="5" style="text-align: center; color: Red;">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>

                                        <td width="7.5%">
                                           Inv. Date :
                                        </td>
                                        <td width="15%">
                                          <asp:TextBox ID="txt_InvDate" runat="server" Enabled="false" Width="80px" style="padding-left:6px"></asp:TextBox>
                                        </td>

                                       <td width="8%">
                                            Visit Time :<strong style="color: Red;">*</strong>
                                        </td>
                                        <td width="15%">
                                          <%--  <asp:DropDownList ID="ddlVisitTime" runat="server" CssClass="field required" Width="180px">
                                                <asp:ListItem Text="Select Visit Time" Value=""></asp:ListItem>
                                                <asp:ListItem Text="09:00 AM" Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Text="12:00 PM" Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="04:00 PM" Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Text="10:00 PM" Value="10:00 PM"></asp:ListItem>
                                            </asp:DropDownList>--%>
                                       <asp:TextBox ID="txt_Vtime" runat="server" Enabled="false" Width="80px" style="padding-left:6px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="txt_Vtime"
                                                Display="Dynamic" ErrorMessage="Visit Time is required." ValidationGroup="val">
                                                        <span class="error">Visit Time is required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>

                     <td  width="45%" rowspan="7" valign="top" class="colmiddle" align="left" style="font-family: Verdana; font-size: 12px;">

                            
                             <h2 style="text-align: center;margin-top: -20px;z-index: 999999;border-color: #fff">
                            <span class="_tableh1"> Patient Details </span>
                             </h2>
                                                
                     <div style="overflow-y: scroll; height:300px;">

                        <div runat="server" id="divkco" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>K/C/O</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblkco" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>

                        <div runat="server" id="divinv" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Investigation</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblInv" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divchief" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Chief C/O</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblChiefCo" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divpho" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Past HO</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblpho" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divfho" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Family HO</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblfho" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divphy" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Physical Generals</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblphy" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divmnd" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Mind</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblMind" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divAF" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>A/F</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblAF" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                        <div runat="server" id="divThermal" style="display: none; padding-top: 10px;">
                            <fieldset>
                                <legend>Thermal</legend>
                                <div style="text-align: left;">
                                    <asp:Label ID="lblThermal" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>


                                                </div>

                                                </td>

                                    </tr>

                                  
                                    <tr>
                                        <td valign="top"> Doctor :</td>
                                        <td valign="top">
                                            <asp:DropDownList ID="DDL_Doctor" Enabled="false" runat="server" class="required field"  Width="180px">
                                            </asp:DropDownList>
                                        </td>

                                          <td valign="top">Remedy :</td>
                                    <td valign="top">
                                        <asp:DropDownList ID="DDL_Remedy" runat="server" class="required field"  Width="180px">
                                        </asp:DropDownList>
                                                
                                    </td>

                                          </tr>
                                  
                                    
                                  
                                  
                                    <tr>
                                                <td valign="top">Old Comp.:</td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txt_FinalDign" runat="server" TextMode="MultiLine" Width="180px">
                                                    </asp:TextBox>
                                                </td>

                                                <td valign="top">New Comp. : </td>
                                                <td valign="top">
                                                    <asp:TextBox ID="txtFreshComplaint" runat="server" TextMode="MultiLine" Width="180px">
                                                    </asp:TextBox>
                                                </td>

                                                  </tr>
                                  
                                    
                                  
                                 
                                    <tr>
                                      
                                         <td valign="top">Pulse Rate :</td>
                                        <td valign="top">
                                           <asp:TextBox ID="txt_PulseRt" runat="server" Width="50px" onchange="CheckRangeForColor()"></asp:TextBox> /MIN.
                                        </td>

                                         <td valign="top"> B.P :</td>
                                        <td valign="top">
                                           <asp:TextBox ID="txl_Bp" runat="server" Width="50px"  onchange="CheckRangeForColor()"></asp:TextBox> /
                                           <asp:TextBox ID="txt_Bp_Dia" runat="server" Width="50px" onchange="CheckRangeForColor()"></asp:TextBox> mm hg.
                                        </td>

                                          </tr>
                                  
                                    <tr>
                                       <td valign="top"> Temp. :</td>
                                        <td valign="top">
                                           <asp:TextBox ID="txt_GlucTest" runat="server" Width="50px" onchange="CheckRangeForColor()"></asp:TextBox> *F
                                        </td>
                                     
                                      <td valign="top"> RR :</td>
                                        <td valign="top">
                                           <asp:TextBox ID="txt_RR" runat="server" Width="50px" onchange="CheckRangeForColor()"></asp:TextBox> MIN.
                                        </td>


                                       </tr>
                                  
                                 
                                      <tr>

                                       <td valign="top"> Sug. Inv. :</td>
                                        <td valign="top">
                                           <asp:TextBox ID="txtLatestSymptom" runat="server" Width="100px" Visible="false">
                                                    </asp:TextBox>

                                                  
                                                 <asp:DropDownCheckBoxes ID="DDL_SuggInv" runat="server" UseButtons="True"
                                                    AddJQueryReference="True"  UseSelectAllNode="True" >
                                                    <%--<Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />--%>
                                                    <Texts SelectBoxCaption="Select Investigation" />
                                              </asp:DropDownCheckBoxes>
                                          
                                                     
                                        </td>

                                           <td valign="top">Status :</td>
                                              <td valign="top">
                                                <%--  <asp:RadioButtonList ID="RBL_INDC" runat="server" RepeatColumns="3"   Width="250px"  CssClass="RadioButton"
                                                      RepeatDirection="Horizontal">
                                                      <asp:ListItem Value="1" Text="Increase" Selected="True"></asp:ListItem>
                                                      <asp:ListItem Value="2" Text="Decrease"></asp:ListItem>
                                                  </asp:RadioButtonList>   --%>
                                                  
                                                <asp:DropDownList ID="DDL_INDC" runat="server" CssClass="field required" Width="100px">
                                             <asp:ListItem Value="1" Text="Severe"></asp:ListItem>
                                               <asp:ListItem Value="2" Text="Moderate"></asp:ListItem>
                                               <asp:ListItem Value="3" Text="Mild"></asp:ListItem>
                                               <asp:ListItem Value="4" Text="Relived"></asp:ListItem>
                                            </asp:DropDownList>
                                                                                      
                                                
                                              </td>

                                    </tr>

                                   
                                    <tr>
                                       
                                        <td colspan="4" align="center">
                                            <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" Text="Save"
                                                OnClick="btnSave_Click" ValidationGroup="val" />
                                        </td>
                                    </tr>


                                </table>
                               
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <table style="width: 100%" cellpadding="5" cellspacing="5">
                                         <tr style="display:none">
                                                <td>Systolic : </td>
                                                <td>
                                                    <asp:TextBox ID="txtSys" runat="server" Width="200px"></asp:TextBox>
                                                </td>

                                                 <td> Diastolic :</td>
                                                <td>
                                                    <asp:TextBox ID="txtDis" runat="server" Width="200px"></asp:TextBox>
                                                </td>
                                            </tr>
                                          
                                         
                                            <tr style="display:none">
                                                <td valign="top">Medicine Details : </td>
                                                <td>
                                                    <asp:TextBox ID="txtMedicineDetails" runat="server" TextMode="MultiLine" Width="300px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="display:none">
                                                <td valign="top">Latest Symptom :</td>
                                                <td>
                                                   
                                                    <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Width="300px">
                                                    </asp:TextBox>

                                                </td>
                                            </tr>
                                            <tr style="display:none">
                                                <td valign="top">Reference to a Third Party Doctor :</td>
                                                <td>
                                                    <asp:TextBox ID="txtReferencetoaThirdPartyDoctor" runat="server" TextMode="MultiLine"
                                                        Width="300px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>

                                              <tr style="display:none">
                                              <td>
                                              We can shift to another hospital :
                                              </td>
                                              <td>
                                                <asp:TextBox ID="txtWecanshifttoanotherhospital" runat="server" TextMode="MultiLine"
                                                        Width="200px">
                                                    </asp:TextBox>
                                              </td>
                                              </tr>
                                            

                                            <tr style="display:none">
                                               <td>Blood Test :</td>
                                        <td>
                                            <asp:FileUpload ID="fu_BloodTest" runat="server"></asp:FileUpload>
                                            <asp:RegularExpressionValidator ID="regfu_BloodTest" runat="server" ControlToValidate="fu_BloodTest"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF|.jpg|.JPG|.jpeg|.JPEG)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf, jpg and jpeg files are allowed.</span></asp:RegularExpressionValidator>
                                        </td>

                                              </tr>

                                              <tr style="display:none">
                                            
  <td>Urine Test :</td>
                                        <td>
                                            <asp:FileUpload ID="fu_UrineTest" runat="server"></asp:FileUpload>
                                            <asp:RegularExpressionValidator ID="regfu_UrineTest" runat="server" ControlToValidate="fu_UrineTest"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF|.jpg|.JPG|.jpeg|.JPEG)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf, jpg and jpeg files are allowed.</span></asp:RegularExpressionValidator>
                                        </td>
                                              </tr>

                                              <tr style="display:none">
                                            
   <td> Monitor :</td>
                                        <td style="margin-top:5px">
                                            <asp:FileUpload ID="fu_Monitor" runat="server"></asp:FileUpload>
                                            <asp:RegularExpressionValidator ID="regfu_Monitor" runat="server" ControlToValidate="fu_Monitor"
                                                    ValidationExpression="[a-zA-Z0-9].*\b(.pdf|.PDF|.jpg|.JPG|.jpeg|.JPEG)\b" CssClass="field required" ValidationGroup="val">
                                                <span class="error">Only pdf, jpg and jpeg files are allowed.</span></asp:RegularExpressionValidator>
                                        </td>

                                              </tr>


                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

