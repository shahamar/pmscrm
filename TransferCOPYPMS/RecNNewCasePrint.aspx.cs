﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class RecNNewCasePrint : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    string uid = "0";
    string tempDate = "";
    string tempDoctor = "";
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }

        if (!(Session["Report"] as string == "{CrystalDecisions.CrystalReports.Engine.ReportDocument}"))
        {
            CrystalReportViewer2.ReportSource = (ReportDocument)Session["Report"];
        }

    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        CrystalReportViewer2.Visible = true;
        string AksConn = ConfigurationManager.AppSettings["constring"];
        bindReport(AksConn);
    }

    public void bindReport(string AksConn)
    {
        DataTable dt = new DataTable();
        ReportDocument cryRpt = new ReportDocument();
        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = default(CrystalDecisions.CrystalReports.Engine.Tables);
        System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

        builder.ConnectionString = AksConn;

        string AksServer = builder["Server"] as string;
        string AksDatabase = builder["Database"] as string;
        string AksUsername = builder["Uid"] as string;
        string AksPassword = builder["Pwd"] as string;


        dt = GetReport();

        if (DDL_ReportType.SelectedValue == "1")
        {
            cryRpt.Load(Server.MapPath("~/Reports/NewCasePrintReport.rpt"));
        }
        else
        {
            cryRpt.Load(Server.MapPath("~/Reports/ReconsultingPrintReport.rpt"));
        }
       



        CrTables = cryRpt.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        cryRpt.SetDataSource(dt);
        cryRpt.Refresh();

        //cryRpt.SetParameterValue("FromDate", "");
        //cryRpt.SetParameterValue("ToDate", "");

        CrystalReportViewer2.ReportSourceID = "CrystalReportSource2";
        CrystalReportViewer2.ReportSource = cryRpt;

        System.IO.Stream oStream = null;
        byte[] byteArray = null;
        oStream = cryRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        byteArray = new byte[oStream.Length];

        Session.Add("COMMPrintingData", byteArray);
        Session.Add("PrintingEventForm", "COMM_Print");

        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
        Session["byteArray"] = byteArray;
        cryRpt.Close();
        cryRpt.Dispose();


    }

    public DataTable GetReport()
    {
        DataTable dt = new DataTable();

        StrSQL = "Proc_GetNewCaseNReconsultingData";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
       
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();

            dt.Load(dr);

        }

        return dt;


    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Convert.ToString(Session["update"]);
    }

}