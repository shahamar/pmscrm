﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;

public partial class UploadScanDocs : System.Web.UI.Page
{
    genral gen = new genral();
    string uid;
    string strSQL;
    string msg;
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    public int fdId
    {
        get
        {
            if (ViewState["fdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["fdId"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");
        //-----------------------------------------------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 26";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");


        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["flag"] != null)
            lblMessage.Text = Request.QueryString["flag"].ToString();
        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            if (msg == "1")
            {
                btnSave.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
            }

            if (Session["fdId"] != null)
                ViewState["fdId"] = Session["fdId"].ToString();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string strFileName = "", strPath = "", strExt = "";
        if (pdId != 0)
        {
            if (fu_AF.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_AF.FileName);
                strFileName = "af_" + pdId + strExt;
                strPath = "UploadMyCaseFiles/AF/" + strFileName;
                fu_AF.SaveAs(Server.MapPath(strPath));
            }

            if (fu_ChiefCO.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_ChiefCO.FileName);
                strFileName = "chief_" + pdId + strExt;
                strPath = "UploadMyCaseFiles/ChiefCo/" + strFileName;
                fu_ChiefCO.SaveAs(Server.MapPath(strPath));
            }
            if (fu_FamilyHO.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_FamilyHO.FileName);
                strFileName = "fho_" + pdId + strExt;
                strPath = "UploadMyCaseFiles/FamilyHo/" + strFileName;
                fu_FamilyHO.SaveAs(Server.MapPath(strPath));
            }
            if (fu_Inv.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_Inv.FileName);
                strFileName = "inv_" + pdId + strExt;
                strPath = "UploadMyCaseFiles/Investigation/" + strFileName;
                fu_Inv.SaveAs(Server.MapPath(strPath));
            }
            if (fu_KCO.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_KCO.FileName);
                strFileName = "kco_" + pdId + strExt;
                strPath = "UploadMyCaseFiles/KCO/" + strFileName;
                fu_KCO.SaveAs(Server.MapPath(strPath));
            }
            if (fu_Mind.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_Mind.FileName);
                strFileName = "mnd_" + pdId + strExt;
                strPath = "UploadMyCaseFiles/Mind/" + strFileName;
                fu_Mind.SaveAs(Server.MapPath(strPath));
            }
            if (fu_PastHO.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_PastHO.FileName);
                strFileName = "pho_" + pdId + strExt;
                strPath = "UploadMyCaseFiles/PastHo/" + strFileName;
                fu_PastHO.SaveAs(Server.MapPath(strPath));
            }
            if (fu_Physical.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_Physical.FileName);
                strFileName = "phy_" + pdId + strExt;
                strPath = "UploadMyCaseFiles/PhysicalGenerals/" + strFileName;
                fu_Physical.SaveAs(Server.MapPath(strPath));
            }
            if (fu_Thermal.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_Thermal.FileName);
                strFileName = "thermal_" + pdId + strExt;
                strPath = "UploadMyCaseFiles/Thermal/" + strFileName;
                fu_Thermal.SaveAs(Server.MapPath(strPath));
            }
        }
        if (fdId != 0)
        {
            if (fu_AF.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_AF.FileName);
                strFileName = "af_" + pdId + strExt;
                strPath = "UploadReconsultingFiles/AF/" + strFileName;
                fu_AF.SaveAs(Server.MapPath(strPath));
            }

            if (fu_ChiefCO.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_ChiefCO.FileName);
                strFileName = "chief_" + pdId + strExt;
                strPath = "UploadReconsultingFiles/ChiefCo/" + strFileName;
                fu_ChiefCO.SaveAs(Server.MapPath(strPath));
            }
            if (fu_FamilyHO.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_FamilyHO.FileName);
                strFileName = "fho_" + pdId + strExt;
                strPath = "UploadReconsultingFiles/FamilyHo/" + strFileName;
                fu_FamilyHO.SaveAs(Server.MapPath(strPath));
            }
            if (fu_Inv.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_Inv.FileName);
                strFileName = "inv_" + pdId + strExt;
                strPath = "UploadReconsultingFiles/Investigation/" + strFileName;
                fu_Inv.SaveAs(Server.MapPath(strPath));
            }
            if (fu_KCO.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_KCO.FileName);
                strFileName = "kco_" + pdId + strExt;
                strPath = "UploadReconsultingFiles/KCO/" + strFileName;
                fu_KCO.SaveAs(Server.MapPath(strPath));
            }
            if (fu_Mind.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_Mind.FileName);
                strFileName = "mnd_" + pdId + strExt;
                strPath = "UploadReconsultingFiles/Mind/" + strFileName;
                fu_Mind.SaveAs(Server.MapPath(strPath));
            }
            if (fu_PastHO.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_PastHO.FileName);
                strFileName = "pho_" + pdId + strExt;
                strPath = "UploadReconsultingFiles/PastHo/" + strFileName;
                fu_PastHO.SaveAs(Server.MapPath(strPath));
            }
            if (fu_Physical.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_Physical.FileName);
                strFileName = "phy_" + pdId + strExt;
                strPath = "UploadReconsultingFiles/PhysicalGenerals/" + strFileName;
                fu_Physical.SaveAs(Server.MapPath(strPath));
            }
            if (fu_Thermal.HasFile)
            {
                strExt = System.IO.Path.GetExtension(fu_Thermal.FileName);
                strFileName = "thermal_" + pdId + strExt;
                strPath = "UploadReconsultingFiles/Thermal/" + strFileName;
                fu_Thermal.SaveAs(Server.MapPath(strPath));
            }
        }
        Response.Redirect("UploadScanDocs.aspx?flag=Documents uploaded successfully");
    }
}