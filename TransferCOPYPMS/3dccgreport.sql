USE [PMS]
GO

/****** Object:  StoredProcedure [dbo].[sp_DccgReport]    Script Date: 04/16/2013 11:26:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_DccgReport]
	 @frmDate SMALLDATETIME
	,@toDate SMALLDATETIME
AS
BEGIN 
SELECT 
	   itd.itdTestDate
	  ,pd.pdInitial + ' ' + pd.pdFname + ' ' + pd.pdMname +' ' + pd.pdLname 'Name'
	  ,'B/H' as 'test'
	  ,tm.tmBaseCharges
	  ,ROW_NUMBER()OVER(PARTITION by  itdTestDate order by itdTestDate asc)as 'rank'
FROM 
	 tblIpdTestDetails itd left  join tblTestMaster tm 
ON 
	 itd.itdtmId=tm.tmId left  join tblPatientDetails pd
ON
	 pd.pdID=itd.itdipdId
WHERE
	 tm.tmId=2	AND itd.itdTestDate BETWEEN @frmDate AND @toDate

END


GO


