﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class ManageBadMaster : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    int userid = 0;
    genral gen = new genral();
    string strSQL = "";
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 21";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["flag"] == "2")
            lblMessage.Text = "Bed details deleted Successfully";

        if (Request.QueryString["flag"] == "1")
            lblMessage.Text = "Bed details updated Successfully";

        if (!IsPostBack)
        {
            BindGVR();
        }
    }

    protected void BindGVR()
    {
        strSQL = "SELECT WardTypeName , wmWardNo ,bmNoOfBads , bmID FROM tblBadMaster BM INNER JOIN tblWardMaster WM ON WM.wmID = BM.wmID Inner Join tblWardTypeMaster WTM On WTM.WardTypeID = WM.wmWardTypeId Where BM.IsDelete = 0 order by WM.wmID";
        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        createpaging(dt.Rows.Count, GRV1.PageCount);
    }
    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "edt")
        {
            int aId = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("BadMaster.aspx?Id=" + aId);
        }
        if (e.CommandName == "del")
        {
            int aId = Convert.ToInt32(e.CommandArgument);
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteBedDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@bmId", aId);
            SqlCmd.Parameters.AddWithValue("@deletedBy", userid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            Response.Redirect("ManageBadMaster.aspx?flag=2", false);
        }
        BindGVR();
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
}