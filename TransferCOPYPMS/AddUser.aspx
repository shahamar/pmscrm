﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="AddUser.aspx.cs" Inherits="AddUser" Title="Add User Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<script type="text/javascript">
    function Validate() {
        var fname = document.getElementById("<%= txtfname.ClientID %>").value;
        var password = document.getElementById("<%= txtPassword.ClientID %>").value;
        var confpwd = document.getElementById("<%= txtConfirmPwd.ClientID %>").value;
        var loginid = document.getElementById("<%= txtLoginID.ClientID %>").value;

        var Err = "";

        if (fname == "") {
            Err += "Please Fill the First Name";
            document.getElementById("<%= txtfname.ClientID %>").className = "field input-validation-error";
        }
        else {
            document.getElementById("<%= txtfname.ClientID %>").className = "field";
        }

        if (loginid == "") {
            Err += "Please Fill the LoginID";
            document.getElementById("<%= txtLoginID.ClientID %>").className = "field input-validation-error";
        }
        else {
            document.getElementById("<%= txtLoginID.ClientID %>").className = "field";
        }

        if (password == "") {
            Err += "Please Fill the Password";
            document.getElementById("<%= txtPassword.ClientID %>").className = "field input-validation-error";
        }
        else {
            document.getElementById("<%= txtPassword.ClientID %>").className = "field";
        }

        if (confpwd == "") {
            Err += "Please Fill Confirm Password";
            document.getElementById("<%= txtConfirmPwd.ClientID %>").classname = "field input-validation-error";
        }
        else {
            document.getElementById("<%= txtConfirmPwd.ClientID %>").classname = "field";
        }

        if (Err != "") {
            return false;
        }
    }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 100%;">
                            <h2 align="center">Add User</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">
                                            Add New User</a> 
                                        </li>
                                        <li class="qtab-HTML last" id="li_9"><a class="qt_tab active" href="ManageUser.aspx">
                                            Manage Users</a> 
                                        </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                        <table style="width:100%; background:#f2f2f2; padding-left:100px" cellspacing="0px" cellpadding="5px" >
                                            <tr>
                                                <td style="text-align: center; color: Red;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center; color: Red;">
                                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>First Name*</td>
                                                <td>Middle Name</td>
                                                <td>Last Name</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtfname" runat="server" CssClass="field required" MaxLength="50"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="txtfname"
                                                        Display="Dynamic" ErrorMessage="First Name is required." ValidationGroup="val"><br />
                                                        <span class="error">First Name is required.</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regFname" runat="server" CssClass="field required"
                                                        Display="Dynamic" ErrorMessage="Please enter valid name" ValidationExpression="^[a-zA-Z .\s]{0,50}$"
                                                        ControlToValidate="txtfname" ValidationGroup="val"><br />
                                                        <span class="error">Please enter valid name!</span>
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtmname" runat="server" CssClass="field" MaxLength="50"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regmname" runat="server" CssClass="field required"
                                                    ControlToValidate="txtmname" Display="Dynamic" ErrorMessage="Please enter valid name"
                                                    ValidationExpression="^[a-zA-Z .\s]{0,50}$" ValidationGroup="val"><br />
                                                    <span class="error">Please enter valid name!</span></asp:RegularExpressionValidator>
                                                </td>
                                                 <td>
                                                    <asp:TextBox ID="txtlname" runat="server" CssClass="field" MaxLength="50"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="reg_lname" runat="server" CssClass="field required"
                                                    ControlToValidate="txtlname" Display="Dynamic" ErrorMessage="Please enter valid name"
                                                    ValidationExpression="^[a-zA-Z .\s]{0,50}$" ValidationGroup="val"><br />
                                                    <span class="error">Please enter valid name!</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                           
                                            <tr id="TR_PWD" runat="server">
                                            	<td>Password*</td>
                                                <td id="cfmpwd" runat="server" Colspan="2">Confirm Password*</td>
						
                                            </tr>
                                            <tr id="TR_PWDTXT" runat="server">
                                            	<td>
                                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="field" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valPassword" runat="server" ControlToValidate="txtPassword"
                                                        Display="Dynamic" ErrorMessage="Password is required." ValidationGroup="val"><br />
                                                        <span class="error">Password is required.</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regPassword" runat="server" ControlToValidate="txtPassword"
                                                        CssClass="field required" ValidationExpression="^[\s\S]{0,100}$" ValidationGroup="val"><br />
                                                        <span class="error">Max 100 characters allowed</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td Colspan="2">
                                                    <asp:TextBox ID="txtConfirmPwd" runat="server" CssClass="field" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valConfPwd" runat="server" ControlToValidate="txtConfirmPwd"
                                                        Display="Dynamic" ErrorMessage="Confirm Password is required." ValidationGroup="val"><br />
                                                        <span class="error">Confirm Password is required.</span></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator runat="server" ControlToValidate="txtConfirmPwd" ControlToCompare="txtPassword"
                                                        ErrorMessage="Passwords do not match." ID="Comparevalidator1" Operator="Equal"
                                                        ValidationGroup="val" Type="String"><span class="error">Passwords do not match.</span></asp:CompareValidator>
                                                    <asp:RegularExpressionValidator ID="regConfirmPwd" runat="server" ControlToValidate="txtConfirmPwd"
                                                        CssClass="field required" ValidationExpression="^[\s\S]{0,100}$" ValidationGroup="val"><br />
                                                        <span class="error">Max 100 characters allowed</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Contact No</td>
                                                <td>User Role*</td>
						<td>Date Of Birth</td>
					    </tr>
                                            <tr>
                                                <td>
                                                    <img src="images/phone.png" />&nbsp;<asp:TextBox ID="txttele" runat="server" CssClass="field"
                                                        MaxLength="20" Width="130px"></asp:TextBox>&nbsp;
                                                    <asp:RegularExpressionValidator ID="regtele" runat="server" Display="Dynamic" CssClass="field required"
                                                        ControlToValidate="txttele" ErrorMessage="Please enter valid contact number!"
                                                        ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val"><br />
                                                    <span class="error">Please enter valid contact number!</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddRegFees" runat="server" CssClass="required field" Width="120px">
                                                        <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="ADMIN" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="RECEPTIONIST" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="OPERATOR" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="PHARMACIST" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="DOCTOR" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="SISTER" Value="6"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="reqRegFees" runat="server" ControlToValidate="ddRegFees"
                                                        ValidationGroup="val" Display="Dynamic" ErrorMessage="Please Select User Role."><br />
                                                        <span class="error">Please Select User Role.</span></asp:RequiredFieldValidator>
						</td>
						<td>
							<asp:TextBox ID="txtDOB" runat="server" placeholder="DD/MM/YYYY" CssClass="field" Width="100px"></asp:TextBox>
    
                                                   <%-- <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgDOJ" TargetControlID="txtDOB">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit;
                                                        width: 20px; height: 20px; top: 5px; left: -5px;" />
                                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDOB" ValidationGroup="val"
                                                        Display="Dynamic" ErrorMessage="Joining date is required!" CssClass="field required">
                                                        <span class="error">DOB is required!</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="field required" ValidationGroup="val"
                                                        Display="Dynamic" ControlToValidate="txtDOB" ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                                                        <span class="error">Enter valid date!</span></asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="CustomValidator1" runat="server" Display="Dynamic" ControlToValidate="txtDOB" ValidationGroup="val"
                                                        CssClass="field required" ClientValidationFunction="CallDateFun" ErrorMessage="Please enter valid date!">
                                                        <span class="error">Please enter valid date!</span></asp:CustomValidator>--%>
    
    
                                                    <cc1:CalendarExtender ID="ce1" runat="server" Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgDOB" 
                                                    TargetControlID="txtDOB">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="imgDOB" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit;
                                                    width: 18px; height: 18px; top: 5px; left: -5px;" />
                                                    <cc1:MaskedEditExtender ID="meeMonthYear" runat="server" TargetControlID="txtDOB" Mask="99/99/9999"  
                                                     MaskType="Date" InputDirection="RightToLeft"></cc1:MaskedEditExtender>    
						</td>
                                            </tr>
					    <tr>
						<td>Login ID*</td>
					    </tr>
					    <tr>
						<td>
                                                    <asp:TextBox ID="txtLoginID" runat="server" CssClass="field" width="50%"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valLoginID" runat="server" ControlToValidate="txtLoginID"
                                                        Display="Dynamic" ErrorMessage="Login ID is required." ValidationGroup="val"><br />
                                                        <span class="error">Login ID is required.</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="reg_LoginID" runat="server" ControlToValidate="txtLoginID"
                                                        CssClass="field required" ValidationExpression="^[\s\S]{0,50}$" ValidationGroup="val"><br />
                                                        <span class="error">Max 50 characters allowed</span></asp:RegularExpressionValidator>
                                                </td>					
					    </tr>
                                            <tr>
                                            	<td>&nbsp;</td>
                                            </tr> 
                                            <tr id="TR_ShowPWD" runat="server">
                                            <td>                                                                                                 
                                                  <asp:CheckBox ID="Chk1" runat="server" Text="Show My Password" 
                                                      AutoPostBack="true" oncheckedchanged="Chk1_CheckedChanged"/>
                                            </td>
                                            </tr>   
                                            <tr id="TR_ShowPWDtxt" runat="server">
                                            <td>
                                                <asp:TextBox ID="txtshowpwd" runat="server" CssClass="field"></asp:TextBox>
                                            </td>
                                            </tr>                                          
                                            <tr>
                                                <td colspan="3" align="center">
                                                    <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                    Style="margin-top: 2px;" Text="Save" ValidationGroup="val" />
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                            	<td>&nbsp;</td>
                                            </tr>                                            
                                        </table>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
<script type="text/javascript">
    $(function () {
        $("#chkShowPassword").bind("click", function () {
            var txtPassword = $("#txtPassword");
            if ($(this).is(":checked")) {
                txtPassword.after('<input onchange = "PasswordChanged(this);" id = "txt_' + txtPassword.attr("id") + '" type = "text" value = "' + txtPassword.val() + '" />');
                txtPassword.hide();
            } else {
                txtPassword.val(txtPassword.next().val());
                txtPassword.next().remove();
                txtPassword.show();
            }
        });
    });
    function PasswordChanged(txt) {
        $(txt).prev().val($(txt).val());
    }
</script>
</asp:Content>
