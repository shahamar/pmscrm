﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="ViewAppointment.aspx.cs" Inherits="ViewAppointment" Title="Today's Appointment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <style type="text/css">
        .b_submit
        {
            background: url("images/b_submit.gif") no-repeat scroll 0 0 transparent;
            border: 0 none;
            display: inline-block;
            font-family: arial;
            vertical-align: middle;
            width: 67px;
            height: 32px;
        }
        
        .textbutton
        {
            color: #FFFFFF !important;
            font-size: 12px;
            font-weight: bold;
            text-align: center;
            margin-top: -8px;
        }
        .loginform
        {
            font-family: Verdana;
        }
        
        .left
        {
            float: left;
        }
        
        .formmenu
        {
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #E6E6E6;
            margin-bottom: 10px;
            padding: 10px;
        }
        .tableh1
        {
            background-image: url("images/tile_back1.gif");
            border-bottom: 1px solid #CED2D6;
            border-left: 1px solid #FFFFFF;
            border-right: 1px solid #FFFFFF;
            color: #606F79;
            font-size: 10px;
            height: 18px;
            padding: 8px 12px 8px 8px;
            font-family: verdana;
            font-weight: bold;
            background-repeat: repeat-x;
        }
    </style>
    <style type="text/css">
        #tbsearch td
        {
            text-align: left;
            vertical-align: middle;
        }
        
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        
        #gvcol div
        {
            margin-top: -10px;
        }
        
        .head1
        {
            width: 9%;
            font-size: small;
        }
        
        .head2
        {
            width: 18%;
            font-size: small;
        }
        
        .head4
        {
            width: 10%;
            font-size: small;
        }
        .head4i
        {
            width: 20%;
        }
        
        
        .head5
        {
            width: 12%;
            font-size: small;
        }
        
        .head5i
        {
            width: 12%;
            font-size: small;
        }
        
        .head5ii
        {
            width: 20%;
            font-size: small;
        }
        
        .head6
        {
            width: 8%;
            font-size: small;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div>
                <table style="width: 100%; background-color: #FFFFFF;">
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="login-area margin_ten_right" style="width: 98%;">
                                <h2>
                                    Today's Appointment</h2>
                                <div class="formmenu">
                                    <div class="loginform">
                                        <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                            style="height: auto; border: 1.5px solid #E2E2E2;">
                                            <div style="padding: 15px; height: auto;">
                                                <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                    <tr>
                                                        <td colspan="3" class="tableh1">
                                                            <table id="tbsearch" style="width: 100%;">
                                                                <tr>
                                                                    <td style="width: 16%">
                                                                        <asp:CheckBox ID="chkConsultingPeriod" runat="server" Checked="true" Text="Appointment Period :" />
                                                                    </td>
                                                                    <td style="width: 13%">
                                                                        <asp:TextBox ID="txtfrm" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                        <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                            runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                            width: 20px; height: 20px;" />
                                                                    </td>
                                                                    <td style="width: 2%">
                                                                        To
                                                                    </td>
                                                                    <td style="width: 13%">
                                                                        <asp:TextBox ID="txtto" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                        <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                            Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                            width: 20px; height: 20px;" />
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                                                            OnClick="btnView_Click" />
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <div id="export" runat="server" style="float: right; display: block">
                                                                            <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" OnClick="imgexport_Click" />
                                                                        </div>
                                                                    </td>
                                                                    <td style="width: 40%; color: Red; text-align: right;">
                                                                        <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 16%">
                                                                        <asp:CheckBox ID="chkApType" runat="server" Checked="true" Text="Appointment Type :" />
                                                                    </td>
                                                                    <td style="width: 13%">
                                                                        <asp:DropDownList ID="ddlApType" runat="server">
                                                                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                                                            <asp:ListItem Text="New" Value="1"></asp:ListItem>
                                                                            <asp:ListItem Text="Follow Up" Value="2"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 100%;
                                                                border: 1px solid #C1C1C1;">
                                                                <tr>
                                                                    <th class="head2">
                                                                        Name
                                                                    </th>
                                                                    <th class="head4">
                                                                        DOC
                                                                    </th>
                                                                    <%--<th class="head4i">
                                                                Address
                                                            </th>--%>
                                                                    <th class="head5">
                                                                        Case Paper No
                                                                    </th>
                                                                    <th class="head5i">
                                                                        Cassette No
                                                                    </th>
                                                                    <th class="head5ii">
                                                                        Occupation
                                                                    </th>
                                                                    <th class="head6">
                                                                        View Details
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="head2">
                                                                        <asp:TextBox ID="txtname" runat="server" class="field" Width="150px"></asp:TextBox>
                                                                    </th>
                                                                    <th class="head4">
                                                                    </th>
                                                                    <%--<th class="head4i">
                                                                <asp:TextBox ID="txtAdd" runat="server" class="field" Width="150px"></asp:TextBox>
                                                            </th>--%>
                                                                    <th class="head5">
                                                                        <asp:TextBox ID="txtcasepaperno" runat="server" class="field" Width="100px"></asp:TextBox>
                                                                    </th>
                                                                    <th class="head5i">
                                                                        <asp:TextBox ID="txtcassetteno" runat="server" class="field" Width="100px"></asp:TextBox>
                                                                    </th>
                                                                    <th class="head5ii">
                                                                        <asp:TextBox ID="txtOccupation" runat="server" class="field" Width="180px"></asp:TextBox>
                                                                    </th>
                                                                    <th class="head6">
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <tr>
                                                            <td id="gvcol" colspan="3">
                                                                <asp:Panel ID="pnlHead" runat="server" Visible="false">
                                                                <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                    CssClass="mGrid" PagerStyle-CssClass="pgr" ShowHeader="false" Width="100%" OnRowCommand="GRV1_RowCommand"
                                                                    OnRowCancelingEdit="GRV1_RowCancelingEdit" OnRowEditing="GRV1_RowEditing" OnRowUpdating="GRV1_RowUpdating"
                                                                    OnPageIndexChanging="GRV1_PageIndexChanging" EmptyDataText="No Records Found." OnRowDataBound="GRV1_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="22%" ItemStyle-HorizontalAlign="Left" HeaderText="Patient Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkpdName" runat="server" Text='<%# Eval("pdName")%>' CommandArgument='<%# Eval("pdID") %>'
                                                                                    CommandName="history" Style="text-decoration: none; color: Blue; font-weight: normal;"
                                                                                    OnClientClick="aspnetForm.target ='_blank';"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="Left" HeaderText="DOC">
                                                                            <ItemTemplate>
                                                                                <%# Eval("apDate", "{0:dd-MMM-yyyy}")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Case Paper No">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblcpno" runat="server" Text='<%# Eval("pdCasePaperNo")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Cassette No">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblcno" runat="server" Text='<%# Eval("pdCassetteNo")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left" HeaderText="Occupation">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbloccup" runat="server" Text='<%# Eval("pdOccupation")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <%--<asp:ImageButton ID="btnView" runat="server" CommandArgument='<%# Eval("pdID") %>' CommandName="vw" ImageUrl="Images/view.ico" ToolTip="View" />--%>
                                                                                <a href="ViewAppPatientDetails.aspx?pdID=<%# Eval("pdID") %>" target="_blank" style="float:left; padding-left:10px;">
                                                                                    <img src="images/view.ico" runat="server" /></a>
                                                                                
                                                                                <asp:ImageButton ID="btnDelete" runat="server" CommandName="del" CommandArgument='<%# Eval("adid") %>'
                                                                                    ImageUrl="~/images/delete.ico" ToolTip="Delete" style='<%# "display:" + DataBinder.Eval(Container.DataItem, "Disp") + ";" %>'/>
                                                                                  
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>