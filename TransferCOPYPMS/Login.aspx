﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
   <%-- <link rel="stylesheet" type="text/css" href="css/style.css" />--%>
    <script type="text/javascript">
    
        function Validate()
        {
            var uname = document.getElementById("<%= txtuname.ClientID %>").value;
            var pwd = document.getElementById("<%= txtpwd.ClientID %>").value;
            var Err = "";            
            if(uname == "")
            {
                Err += "Please enter user name";
                document.getElementById("<%= txtuname.ClientID %>").className = "field input-validation-error";
            }
            else
            {
                document.getElementById("<%= txtuname.ClientID %>").className = "field";
            }
            
            if(pwd == "")
            {
                Err += "Please enter password";
                document.getElementById("<%= txtpwd.ClientID %>").className = "field input-validation-error";
            }
            else
            {
                document.getElementById("<%= txtpwd.ClientID %>").className = "field";
            }
            
            if(Err != "")
            {
                return false;   
            }  
        }
        
    </script>
    
    <script type="text/javascript">
	
		if (top.location!= self.location) {
			top.location = self.location.href
		}
	
</script>
<style type="text/css">
body{
	margin:0;
}	
.style5 {	
	font-size: 27px;
	font-family: Calibri,Arial,sans-serif,Tahoma;
	color: #fff;
	font-weight:bold;
}
.NFButton {
    background: #333;
    border: medium none;
    color: #ffffff;
    cursor: pointer;
    font-weight: bold;
    height: 33px;
    margin-top: -2px;
    padding: 0 15px;
    vertical-align: middle;
    width: auto;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
  
  <center>
         <table width="100%" border="0" cellspacing="0" cellpadding="0">

    <tr>
      <td align="center" valign="middle" bgcolor="#768607">
      <table width="1000px" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="250px" align="center" valign="middle" height="90px">
         <img src="images/logo.png" style="margin-left:20px" alt="Aditya" width="300px" border="0" />
         </td>
        </tr>
      </table></td>
    </tr>
   
     <tr>
      <td bgcolor="#f27400"><div style="height:5px;">&nbsp;</div></td>
    </tr>
    <tr>
      <td height="180">&nbsp;</td>
    </tr>


   <tr>
   <td  align="center" valign="top">
   <div class="main-body">
      <table width="400" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="400" align="center" valign="top" class="p1" style="background-image: url(images/pattern.png), url(images/login_bkg.png);"><table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td align="center" valign="top"><span class="style5">CRM LOGIN</span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center" valign="top"><label>
                  <asp:TextBox ID="txtuname" runat="server" style="width:250px; padding-left:10px; height:30px; background-color:#FFFFFF; color:#333; border-radius:4px;"></asp:TextBox>
                </label></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center" valign="top"><label>
                  <asp:TextBox ID="txtpwd" runat="server" TextMode="Password" style="width:250px; padding-left:10px; height:30px; background-color:#FFFFFF; color:#333; border-radius:4px;"></asp:TextBox>
                  </label>
                    <br />
                  <%--<a href="#" style="text-decoration:none;"><span style="color:#FFFFFF; font-family: Calibri,Arial,sans-serif,Tahoma; margin-left:175px; font-size:13px;">Forgot password</span></a>--%>
                  </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                <asp:Button ID="btnSignIn" runat="server" Text="Login" style="height:30px; width:105px;" class="NFButton"
                   onclick="btnSignIn_Click" OnClientClick="return Validate()"/>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
          </table></td>
        </tr>
      </table>
   </div>
   </td>
   </tr>
    
     <tr valign="bottom">
      <td style="height:180px;"></td>
    </tr>

    <tr valign="bottom">
      <td align="center" bgcolor="#768607">
      <span class="style5">PMS : CRM | Powered by </span><a href="http://www.ocs.net.in" class="style4" style="text-decoration:none;">OCS</a></td>
    </tr>
  </table>
        </center>


    </form>
</body>
</html>
