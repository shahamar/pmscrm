﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="CourierView.aspx.cs" Inherits="CourierView" Title="Courier View" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<script type="text/javascript" src="js/custom-form-elements.js"></script>
<script type="text/javascript">
    function callscript() {
        alert("ok");
        Custom.init;
        alert("ok");
    }

</script>
<style type="text/css">
#tbsearch td { text-align: left; vertical-align: middle;}
#tbsearch label { display: inline; margin-top: 3px; position: absolute;}
#tbhead th
{
    text-align: left;
    background: url(images/nav-back.gif) repeat-x top;
    color: #FFFFFF;
    padding-left: 2px;
    padding-top: 3px;
}
#gvcol div {  margin-top: -10px;}
.head1 { width: 25%;}
.head2 { width: 10%;}
.head3 { width: 25%;}
.head4 { width: 10%;}
.head5 { width: 10%;}
.head6 { width: 10%;}
.head7 { width: 10%;}

.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right"  style="width: 100%;" cellspacing="0px" cellpadding="5px">
                            <span style="text-align:center;"><h2>View Courier</h2></span>
                            <div class="formmenu">
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                        <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">
                                            <tr>
                                                <td colspan="3" style="text-align:center; color:Red;">
                                                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table style="width:100%;" cellspacing="5px" cellpadding="5px">
                                                        <tr>
                                                            <td style="width: 13%">
                                                                <asp:CheckBox ID="chkCourierDate" runat="server" Text="&nbsp;<strong>Courier Period :</strong>" />
                                                            </td>
                                                            <td style="width: 12%">
                                                                <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                    runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                </cc1:CalendarExtender>&nbsp;
                                                                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"/>
                                                            </td>
                                                            <td style="width: 4%"><strong>To :</strong></td>
                                                            <td style="width: 13%">
                                                                <asp:TextBox ID="txtto" runat="server" CssClass="textbox"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                    Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                </cc1:CalendarExtender>&nbsp;
                                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" />
                                                            </td>
                                                            <td><asp:Label ID="lblGridHeader" runat="server" style="float:right; color:Red;"></asp:Label></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table class="mGrid" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                        <tr style="border: 1px solid #fff;">
                                                            <th class="head1">Receiver Name</th>
                                                            <th class="head2">Mobile No</th>
                                                            <th class="head3">Courier Name</th>
                                                            <th class="head4">Courier Date</th>
                                                            <th class="head5">Docket No</th>
                                                            <th class="head6">Courier Charge</th>
                                                            <th class="head7">Action</th>
                                                        </tr>
                                                        <tr>
                                                            <th class="head1">
                                                                <asp:TextBox ID="txtTest" runat="server" class="field" Width="250px"></asp:TextBox>
                                                            </th>
                                                            <th class="head2"></th>
                                                            <th class="head3"></th>
                                                            <th class="head4"></th>
                                                            <th class="head5"></th>
                                                            <th class="head6"></th>
                                                            <th class="head7">
                                                                <div style="display: block; width: 68px; margin-left: 14px;">
                                                                    <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none; color: #FFF;
                                                                    text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click">
                                                                	<img src="images/filter.png" style="border:0px;"/> Filter</asp:LinkButton>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td id="gvcol" colspan="3">
                                                        <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                            CssClass="mGrid" PageSize="20" ShowHeader="false" Width="100%"
                                                            OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="center">
                                                                    <ItemTemplate>
                                                                        <%# Eval("cmdName")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center">
                                                                    <ItemTemplate>
                                                                        <%# Eval("cmdMobileNo")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="25%" ItemStyle-HorizontalAlign="center">
                                                                    <ItemTemplate>
                                                                        <%# Eval("cmCourierName")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center">
                                                                    <ItemTemplate>
                                                                        <%# Eval("cmdDate", "{0:dd-MMM-yyyy}")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center">
                                                                    <ItemTemplate>
                                                                        <%# Eval("cmdDocketNo")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center">
                                                                    <ItemTemplate>
                                                                        <%# Eval("cmdCharges")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("cmdID") %>'
                                                                            CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                        <asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("cmdID") %>'
                                                                            CommandName="del" ImageUrl="~/images/delete.ico" ToolTip="Delete"
                                                                            OnClientClick="return confirm('Are you sure you want to delete this record ?');" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>                                             
                                        </table>                                 
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
