﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class PatientReport : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataReader SqlDr;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    //string pdId = "";
    string strSQL = "";
    int cnt = 0;
    int userid;
    //added by nikita on 12th march 2015------------------------------
    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Session["userid"] == null)
            Response.Redirect("Login.aspx",true);
        
        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 38";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            if (Session["rptPdId"] != null)
                ViewState["pdId"] = Session["rptPdId"].ToString();

            FillData();              
        }
    }

    public void FillData()
    {
        strSQL = "Select pdemail,pdAge,pdId, pdPatientICardNo,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', pdSex, pdDOC,pdTele +'/'+pdMob As 'pdContact',pdCasePaperNo,pdCassetteNo,ISNULL(pdOccupation,'') pdOccupation,(select (dFName+' '+dMName+' '+dLName) AS DName From tblDoctorMaster WHERE did=dbo.tblPatientDetails.did)as DoctorName From dbo.tblPatientDetails where pdId =" + PdID;
        SqlCmd = new SqlCommand(strSQL, con);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlDr = SqlCmd.ExecuteReader();
        while (SqlDr.Read())
        {
            lblName.Text = SqlDr["pdName"].ToString();
            lblPatientId.Text = SqlDr["pdPatientICardNo"].ToString();
            lblGender.Text = SqlDr["pdSex"].ToString();
            lblAge.Text = SqlDr["pdAge"].ToString();
            lblCasePaperNo.Text = SqlDr["pdCasePaperNo"].ToString();
            lblCassetteNo.Text = SqlDr["pdCassetteNo"].ToString();
            lblOccupation.Text = SqlDr["pdOccupation"].ToString();
            lblContact.Text = SqlDr["pdContact"].ToString();
            lblDoctorName.Text = SqlDr["DoctorName"].ToString();
            if (SqlDr["pdemail"] != DBNull.Value)
                lblContact.Text += "<br />" + SqlDr["pdemail"].ToString();

        }
        SqlDr.Close();
        con.Close();

        if (File.Exists(Server.MapPath("Files/KCO/kco_" + PdID)))
        {
            lblkco.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + PdID).ToString());
            divkco.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/Investigation/inv_" + PdID)))
        {
            lblInv.Text = File.ReadAllText(Server.MapPath("Files/Investigation/inv_" + PdID).ToString());
            divinv.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/ChiefCo/chief_" + PdID)))
        {
            lblChiefCo.Text = File.ReadAllText(Server.MapPath("Files/ChiefCo/chief_" + PdID).ToString());
            divchief.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/PastHo/pho_" + PdID)))
        {
            lblpho.Text = File.ReadAllText(Server.MapPath("Files/PastHo/pho_" + PdID).ToString());
            divpho.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/FamilyHo/fho_" + PdID)))
        {
            lblfho.Text = File.ReadAllText(Server.MapPath("Files/FamilyHo/fho_" + PdID).ToString());
            divfho.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/Mind/mnd_" + PdID)))
        {
            lblMind.Text = File.ReadAllText(Server.MapPath("Files/Mind/mnd_" + PdID).ToString());
            divmnd.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/AF/af_" + PdID)))
        {
            lblAF.Text = File.ReadAllText(Server.MapPath("Files/AF/af_" + PdID).ToString());
            divAF.Style.Add("display", "block");
        }

        if (File.Exists(Server.MapPath("Files/Thermal/thermal_" + PdID)))
        {
            lblThermal.Text = File.ReadAllText(Server.MapPath("Files/Thermal/thermal_" + PdID).ToString());
            divThermal.Style.Add("display", "block");
        }

        FillFollowupHistory();
        FillHistory();
    }

    public void FillFollowupHistory()
    {
        strSQL = "Select * from vwConsultingHistoryNewAks Where fdPatientId=" + PdID;
        DataTable dt = gen.getDataTable(strSQL);
        if (dt.Rows.Count > 0)
        {
            GRV1.DataSource = dt;
            GRV1.DataBind();
            divFollowupHistory.Style.Add("display", "block");
        }
    }

    public void FillHistory()
    {
        strSQL = "Select ptd.ptdId,ptd.ptdTMId,ptd.ptdpdId,ptd.ptdFromDate,ptd.ptdToDate,ptd.ptdCharges,tm.tmTestName ";
        strSQL += " FROM dbo.tblPatientTestDetails ptd INNER JOIN dbo.tblTestMaster tm ON ptd.ptdTMId = tm.tmId Where ptdpdId=" + PdID;

        DataTable dt = gen.getDataTable(strSQL);
        GRV2.DataSource = dt;
        GRV2.DataBind();
        if (dt.Rows.Count > 0)
        {
            divOpdTestDetails.Style.Add("display", "block");
        }
    }

    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            cnt = cnt + 1;
            if (cnt == 1)
            {
                e.Row.BackColor = System.Drawing.Color.FromName("#FDFA00");
            }
        }
    }
   
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        FillFollowupHistory();
    }

}