﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class UnOccupiedBed : System.Web.UI.Page
{
    SqlCommand cmd;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string uid;
    int Page_no = 0, Page_size = 20;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx", false);

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 28";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            FillData();
        }
    }

    protected void FillData()
    {
        ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT wmWardNo,wmID FROM tblWardMaster", "wmID", "wmWardNo", ddWardNo);
    }

    public void BindGVR()
    {
        try
        {
            cmd = new SqlCommand();
           // cmd.CommandText = "sp_GetFreeBed";
            cmd.CommandText = "sp_GetFreeBed_New";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@wmID", ddWardNo.SelectedValue);

            DataTable dt = gen.getDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
            else
            {
                GRV1.EmptyDataText.ToString();
                GRV1.DataBind();
            }
        }
        catch
        {
        }
    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;
    }
}