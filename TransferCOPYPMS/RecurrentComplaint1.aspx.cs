﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class RecurrentComplaint : System.Web.UI.Page
{
    string pdId = "";
    genral gen = new genral();
    string uid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Session["pdId"] != null)
            pdId = Session["pdId"].ToString();
        else
            Response.Redirect("ManagePatients.aspx?flag=1");

        if (!IsPostBack)
        {
            FillRecurrentComplaint();
        }
    }

    public void FillRecurrentComplaint()
    {
        if (File.Exists(Server.MapPath("Files/RecurrentComplaint/rec_" + pdId)))
        {
            kco.Content = File.ReadAllText(Server.MapPath("Files/RecurrentComplaint/rec_" + pdId).ToString());
        }
    }


    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (File.Exists(Server.MapPath("Files/RecurrentComplaint/rec_" + pdId)))
        {
            File.Delete(Server.MapPath("Files/RecurrentComplaint/rec_" + pdId));
        }
        File.WriteAllText(Server.MapPath("Files/RecurrentComplaint/rec_" + pdId), kco.Content.ToString());

        Response.Redirect("PastHO.aspx");
    }
}
