﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public partial class AwaitingCourierReq : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL, strSQLAks, strSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string Uroll = "0";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (Session["urole"] != null)
            Uroll = Session["urole"].ToString();


        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindOnlinePatientGrid();
        }
    }

    private void BindOnlinePatientGrid()
    {
        object Frmdate = null;
        object Todate = null;

        if (chkDT.Checked == true)
        {
            string[] frmdate = txtfrm.Text.ToString().Split('/');
            string[] todate = txtto.Text.ToString().Split('/');
            Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
            Todate = todate[2] + "/" + todate[1] + "/" + todate[0];
        }

        StrSQL = "GetOnlineTreatementPatientDetails";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FrmDate", Frmdate);
        SqlCmd.Parameters.AddWithValue("@ToDate", Todate);
        SqlCmd.Parameters.AddWithValue("@Flag", "ACR");
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_OnLinePat.DataSource = dt;
                GVD_OnLinePat.DataBind();
            }
            else
            {
                GVD_OnLinePat.DataSource = null;
                GVD_OnLinePat.DataBind();
            }
            conn.Close();
            dr.Dispose();

        }
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindOnlinePatientGrid();
    }

    protected void GVD_OnLinePat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      
        if (e.CommandName == "NewCaseFol")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                string fdID = arg[1];
                string OLID = arg[2];

                Session["ipdid"] = null;
                Session["CasePatientID"] = pdID;

                int cnt = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from vwConsultingHistoryNewAks Where fdPatientId=" + pdID).ToString());


                if (cnt >= 1)
                {
                    Response.Redirect("FollowupDetail.aspx?SC=ACR&fdId=" + fdID + "&OLID=" + OLID);
                }
                else
                {
                    Response.Redirect("ConsultPatient.aspx?fdId=" + fdID + "&OLID=" + OLID);
                }


            }
            catch
            { }
        }

    }

}