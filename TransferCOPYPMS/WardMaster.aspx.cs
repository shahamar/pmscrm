﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class WardMaster : System.Web.UI.Page
{
    SqlCommand Sqlcmd;
    genral gen = new genral();
    int wmID = 0;
    SqlConnection com = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string strSQL = "";
    int userid = 0;
    int i = 0;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["Id"] != null)
            wmID = int.Parse(Request.QueryString["Id"].ToString());

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 17";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();

        if (!IsPostBack)
        {
            FillData();
        }

    }

    protected void FillData()
    {

        DDWardType.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT Distinct WardTypeID As wmWardTypeId , WardTypeName As wmType FROM tblWardTypeMaster Where IsDelete = 0", "wmWardTypeId", "wmType", DDWardType);

        if (wmID != 0)
        {
            SqlCommand SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "SELECT * FROM tblWardMaster WHERE wmID=" + wmID + "";
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = com;
            if (com.State == ConnectionState.Closed)
                com.Open();
            SqlDataReader dr = SqlCmd.ExecuteReader();
            while (dr.Read())
            {
                txtWardNo.Text = dr["wmWardNo"].ToString();
                DDWardType.SelectedValue = dr["wmWardTypeId"].ToString();
                txt_WardRate.Text = dr["wmWardRate"].ToString();
                txt_OrderNo.Text = dr["OrderNo"].ToString();
                lblOldRate.Text = dr["wmWardRate"].ToString();
            }
            dr.Close();
            dr.Dispose();
            com.Close();
            com.Dispose();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (com.State == ConnectionState.Closed)
                com.Open();

            if (wmID == 0)
                strSQL = "INSERT INTO tblWardMaster(wmWardNo,wmCreatedBy,wmWardTypeId,wmWardRate,OrderNo) VALUES (@wmWardNo,@wmCreatedBy,@wmWardTypeId,@wmWardRate,@OrderNo)";
            else
                strSQL = "UPDATE tblWardMaster SET wmWardNo=@wmWardNo,wmWardTypeId=@wmWardTypeId,wmWardRate=@wmWardRate,OrderNo=@OrderNo WHERE wmID=" + wmID + "";
            SqlCommand Sqlcmd = new SqlCommand(strSQL, com);
            Sqlcmd.CommandType = CommandType.Text;
            Sqlcmd.Parameters.AddWithValue("@wmWardNo", txtWardNo.Text.Trim());
            Sqlcmd.Parameters.AddWithValue("@wmCreatedBy", userid);
            Sqlcmd.Parameters.AddWithValue("@wmWardTypeId", Convert.ToInt32(DDWardType.SelectedValue));
            Sqlcmd.Parameters.AddWithValue("@wmWardRate", Convert.ToDecimal(txt_WardRate.Text));
            Sqlcmd.Parameters.AddWithValue("@OrderNo", Convert.ToDecimal(txt_OrderNo.Text));
            i = Sqlcmd.ExecuteNonQuery();
            Sqlcmd.Dispose();
            com.Close();

            if (i > 0)
            {

                if (wmID != 0)
                {

                    if (txt_WardRate.Text != lblOldRate.Text)
                    {
                        if (com.State == ConnectionState.Closed)
                            com.Open();

                        strSQL = "INSERT INTO tblWardHistoryDetailsAks(HWardID,HWardCharge,HWardCreatedBy) VALUES (@HWardID,@HWardCharge,@HWardCreatedBy)";
                        SqlCommand Sqlcmd1 = new SqlCommand(strSQL, com);
                        Sqlcmd1.CommandType = CommandType.Text;
                        Sqlcmd1.Parameters.AddWithValue("@HWardID", wmID);
                        Sqlcmd1.Parameters.AddWithValue("@HWardCreatedBy", userid);
                        Sqlcmd1.Parameters.AddWithValue("@HWardCharge", Convert.ToDecimal(lblOldRate.Text));
                        Sqlcmd1.ExecuteNonQuery();
                        Sqlcmd1.Dispose();
                        com.Close();
                    }

                }

                if (wmID == 0)
                    Response.Redirect("WardMaster.aspx?msg=Ward details saved successfully");
                else
                    Response.Redirect("ManageWardMaster.aspx?flag=1");
            }
            else
            {
                lblMessage.Text = "Ward details Not saved...!!!";
            }

           

            Sqlcmd.Dispose();
            com.Close();
        }
    }
}
