﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="ManageIPD.aspx.cs" Inherits="ManageIPD" Title="Manage IPD" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />

<style>
#tbsearch td
{
	text-align:left;
	vertical-align: middle;
}

#tbsearch label
{
	  display: inline;
    margin-top: 3px;
    position: absolute;
}

#tbhead th
{
	text-align:left;
	background: url(images/nav-back.gif) repeat-x top;
	color:#FFFFFF;
	padding-left:2px;
	padding-top:3px; 
}

#gvcol div
{
	margin-top:-10px;
}

.head1
{
	width:15%;
}

.head2
{
	width:20%;
}   

.head3
{
	width:10%
}

.head4
{
	width:10%
}

.head5
{
	width:15%
}

.head5i
{
	width:15%
}

.head6
{
	width:10%
}
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width:100%; background-color:#FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                        <h2>Manage IPD</h2>
                        
                        <div class="formmenu">
                            <div class="loginform">                            
                                <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel" style="height:auto;">
                                    <div style="padding:15px; height:auto;">
                                        <table style="width: 100%; border: 1px solid #EFEFEF;">
                                            <tr>
                                                <td colspan="3" class="tableh1">
                                                    <table id="tbsearch" style="width:98%;">
                                                        <tr>
                                                            <td style="width:14%">
                                                                <asp:CheckBox ID="chkAddDate" runat="server" 
                                                                    Text="Addmission Date :" />
                                                            </td>
                                                            <td style="width:12%">
                                                                <asp:TextBox ID="txtfrm" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm" runat="server" 
                                                                    Enabled="True" TargetControlID="txtfrm">
                                                                </cc1:CalendarExtender>
                                                                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" 
                                                style="position: absolute; width:20px; height:20px;" />
                                                                </td>
                                                            <td style="width:2%">
                                                                To </td>
                                                            <td style="width:12%">
                                                                
                                                                <asp:TextBox ID="txtto" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" 
                                                                    TargetControlID="txtto" Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                </cc1:CalendarExtender>
                                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" 
                                                style="position: absolute; width:20px; height:20px;" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnView" runat="server" Text="View" 
                                                                    CssClass="textbutton b_submit left" onclick="btnView_Click"/>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                 <td colspan="3">
                                                    <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                        <tr>
                                                            <th class="head1">
                                                                Card No
                                                            </th>
                                                            
                                                            <th class="head2">
                                                                Name
                                                            </th>
                                                            
                                                            <th class="head3">
                                                                Gender
                                                            </th>
                                                            
                                                            <th class="head4">
                                                                DOA
                                                            </th>
                                                            
                                                            <th class="head5">
                                                                Case Paper No
                                                            </th>
                                                            
                                                            <th class="head5i">
                                                                Cassette No
                                                            </th>
                                                            
                                                            <th class="head6">
                                                                Action
                                                            </th>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <th class="head1">
                                                                <asp:TextBox ID="txtCardNo" runat="server" class="field" Width="137px"></asp:TextBox>    
                                                            </th>
                                                            
                                                            <th class="head2">
                                                                <asp:TextBox ID="txtname" runat="server" class="field" Width="183px"></asp:TextBox>
                                                            </th>
                                                            
                                                            <th class="head3">
                                                            </th>
                                                            
                                                            <th class="head4">
                                                                
                                                            </th>
                                                            
                                                            <th class="head5">
                                                                <asp:TextBox ID="txtcasepaperno" runat="server" class="field" Width="137px"></asp:TextBox>
                                                            </th>
                                                            
                                                            <th class="head5i">
                                                                <asp:TextBox ID="txtcassetteno" runat="server" class="field" Width="137px"></asp:TextBox>
                                                            </th>
                                                            
                                                            <th class="head6">
                                                                <div style="display: block; width: 68px; margin-left: 14px;">
                                                                <asp:LinkButton ID="lnkfilter" runat="server" 
                                                                    style="text-decoration:none;color:#FFF; text-align:center; margin-left:2px;" 
                                                                    onclick="lnkfilter_Click">
                                                                <img src="images/filter.png" style="border:0px;"/>
                                                                Filter</asp:LinkButton>
                                                                 </div>   
                                                            </th>
                                                        </tr>
                                                    </table>
                                                 </td>
                                            <tr>
                                                 <td id="gvcol" colspan="3">
                                                    <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" 
                                    AutoGenerateColumns="False" CssClass="mGrid" 
                                    PagerStyle-CssClass="pgr" PageSize="20" ShowHeader="false" Width="100%" 
                                                         onrowcommand="GRV1_RowCommand">
                                    <Columns>
                                        
                                        <asp:TemplateField ItemStyle-Width="15%" 
                                                ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("pdPatientICardNo")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    
                                        <asp:TemplateField ItemStyle-Width="20%" 
                                                ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("pdName")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField ItemStyle-Width="10%"  
                                                ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("pdSex")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField ItemStyle-Width="10%" 
                                                ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("pdDOC", "{0:dd-MMM-yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField ItemStyle-Width="15%" 
                                                ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                               <%# Eval("pdCasePaperNo")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                       <asp:TemplateField ItemStyle-Width="15%" 
                                                ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                               <%# Eval("pdCassetteNo")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                        
                                                                                
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("pdID") %>' CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>           
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>           
                                    </div>
                                </div>
                            <div class="clr"></div>
                        </div>               
                        </div>
                    </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
        </ContentTemplate>
    </asp:UpdatePanel>
        

</asp:Content>

