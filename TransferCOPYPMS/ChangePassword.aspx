﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" Title="Change Password" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 100%; text-align:center">
                    <h2>Change Password</h2>
                    <div class="formmenu">
                        <div class="loginform">              
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">
                                    <tr>
                                        <td colspan="2" style="text-align:center; color:Red;">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Old Password*</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtOldPassword" runat="server" CssClass="field required" MaxLength="50" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valoldpwd" runat="server" ControlToValidate="txtOldPassword" Display="Dynamic" 
                                                ErrorMessage="Old Password is required.">
                                                <span class="error">Old Password is required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>New Password*</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtNewPassword" runat="server" CssClass="field required" MaxLength="50" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valNewPwd" runat="server" ControlToValidate="txtNewPassword" Display="Dynamic" 
                                                ErrorMessage="New Password is required.">
                                                <span class="error">New Password is required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Confirm Password*</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtConfirmPwd" runat="server" CssClass="field required" style="margin-left:142px" MaxLength="50" 
                                            TextMode="Password"></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="valConfPwd" runat="server" ControlToValidate="txtConfirmPwd" Display="Dynamic" 
                                                ErrorMessage="Confirm Password is required.">
                                                <span class="error">Confirm Password is required.</span>
                                            </asp:RequiredFieldValidator>
                                            <asp:CompareValidator runat="server" ControlToValidate="txtConfirmPwd" ControlToCompare="txtNewPassword" 
                                            ErrorMessage="Passwords do not match." id="Comparevalidator1" Operator="Equal" Type="String">
                                            <span class="error">Passwords do not match.</span></asp:CompareValidator>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" onclick="btnSave_Click" style="margin-top: 2px;" Text="Save" />
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                    	<td>&nbsp;</td>
                                   	</tr>                                    
                                </table>                                
                            </div>   
                        <div class="clr"></div>
                        </div>               
                    </div>
                </div>
            </td>
        </tr>        
    </table>
</asp:Content>

