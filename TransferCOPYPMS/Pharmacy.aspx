﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="Pharmacy.aspx.cs" Inherits="Pharmacy" %>

  <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<%--<meta http-equiv="refresh" content="10;" />--%>
<style>
.AksDisplay {display:none;}
.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}
.mGrid { 
    width: 100%; 
    background-color: #eee; 
    margin: 5px 0 10px 0; 
    border: solid 1px #525252; 
    border-collapse:collapse; 
    font-size:18px;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server"> 
        <ContentTemplate> 
<table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">

    <tr>
            <td></td>
	<%--	<td> 
              <asp:UpdatePanel ID="UP1" runat="server"> 
    <ContentTemplate> 
            <asp:timer id="Timer1" runat="server" OnTick="Timer1_Tick" Interval="20000"></asp:timer>
  </ContentTemplate> 
    </asp:UpdatePanel> 
             </td>--%>
        </tr>
    <tr>
        <td>
            <div class="login-area margin_ten_right" style="width: 100%; text-align:center">
                <h2 style="color:#768607;font-size:28px">Pharmacy</h2>
              	
                       <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                          <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px" border="0px">

                        

                              <tr>
                                <td>    
                              		<table style="width:100%;" cellspacing="0px" cellpadding="0px">
                              			<tr>

                              				<td><strong>From :&nbsp;</strong>
                                                <asp:TextBox ID="txtfrm" runat="server" CssClass="field textbox"></asp:TextBox>
                                                <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                    runat="server" Enabled="True" TargetControlID="txtfrm">
                                                </cc1:CalendarExtender>&nbsp;
                                                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"  />
                                            </td>
                                            <td><strong>To :&nbsp;</strong>
                                                <asp:TextBox ID="txtto" runat="server" CssClass="field textbox"></asp:TextBox>
                                                <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                    Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                </cc1:CalendarExtender>&nbsp;
                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"  />
                            				</td>
                                            <td>
                                               <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" />
                                            </td>
                                           
                          				</tr>
                          			</table>
                          		</td>
                          	  </tr>


                                <tr>
                                <td style="text-align: center;">
                                    <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></font>
                                </td>
                              </tr>

                              
                              
                            	  <tr>
                             	<td id="gvcol" style="width:100%; text-align: center;background:#fff;border:0px solid;">

                                <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                    <asp:Label ID="Label1" runat="server" Text="Non Extra Doses"></asp:Label></font>

                       <br />

				     <asp:timer id="Timer1" runat="server" OnTick="Timer1_Tick" Interval="20000"></asp:timer>                              
                                    <asp:GridView ID="GVD_FollowupPaymentDetails"  Width="100%" runat="server" 
                                         AutoGenerateColumns="False" 
                                    PageSize="20" OnRowCommand="GVD_FollowupPaymentDetails_RowCommand"  AllowPaging="True"
                                    DataKeyNames="fdId" CssClass="mGrid"
                                        OnPageIndexChanging="GVD_FollowupPaymentDetails_PageIndexChanging" 
                                        CellPadding="4" ForeColor="#333333">
                                        <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                     <asp:BoundField DataField="SNO" HeaderText="Sr No." HeaderStyle-Font-Size="Small"/>
					
				     <asp:BoundField DataField="App_Time" HeaderText="Time" HeaderStyle-Font-size="Small" />
                                       
                                     <asp:BoundField DataField="PNAME" HeaderText="Patient Name" HeaderStyle-Font-Size="Small"/>
                                       
                                     <asp:BoundField DataField="pdCasePaperNo" HeaderText="Case Paper No." HeaderStyle-Font-Size="Small"/>

                                      <asp:BoundField DataField="RMNAME" HeaderText="Remedy" HeaderStyle-Font-Size="Small"/>

                                      <asp:BoundField DataField="RemedyPeriod" HeaderText="Remedy Period" HeaderStyle-Font-Size="Small"/>
					
					 <asp:TemplateField HeaderText="Dose Type" HeaderStyle-Font-Size="Small">
                                               <ItemTemplate>
                                               <asp:Label ID="lblDoseType" runat="server" Text='<%# Eval("DoseType")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("FStatus").ToString())%>'></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField> 

                                     <%-- <asp:BoundField DataField="DoseType" HeaderText="Dose Type" HeaderStyle-Font-Size="Small"/> --%>
    
                                      <%-- <asp:BoundField DataField="AddEmg" HeaderText="Add Emg" HeaderStyle-Font-Size="Small" />

                                       <asp:BoundField DataField="ExtraDos" HeaderText="Extra doses" HeaderStyle-Font-Size="Small" />  --%>
						
					<asp:TemplateField HeaderText="Add Emg" HeaderStyle-Font-Size="Small">
                                               <ItemTemplate>
                                               <asp:Label ID="lblAddEmg" runat="server" Text='<%# Eval("AddEmg")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("AStatus").ToString())%>'></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField>
			
					<asp:TemplateField HeaderText="Extra Doses" HeaderStyle-Font-Size="Small">
                                               <ItemTemplate>
                                               <asp:Label ID="lblExtraDos" runat="server" Text='<%# Eval("ExtraDos")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("EStatus").ToString())%>'></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField>

                                          <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center" HeaderText="Done" HeaderStyle-Font-Size="Small" >
                                              <ItemTemplate>
                                                    <asp:ImageButton ID="btnPayAmount" runat="server" CommandName="Pharmacy"
                                                    CommandArgument='<%# Eval("fdId") %>' ImageUrl="~/images/button_ok.png" ToolTip="Done"/>
                                          </ItemTemplate>    

<HeaderStyle Width="5%"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
                                          
</asp:TemplateField>
                                     </Columns>
                                        <EditRowStyle BackColor="#2461BF" />
                                        <HeaderStyle BackColor="#e9f7bc" Font-Bold="True" ForeColor="Black" />
                                        <PagerStyle BackColor="#e9f7bc" ForeColor="Black" HorizontalAlign="left" />
                                        <RowStyle BackColor="#F0F0F0" />
                                    </asp:GridView>
                                </td>                           
                        	  </tr>           
                              

                           
                              <tr>
                             	<td id="Td2" style="width:100%; text-align: center;background:#fff; border:0px solid;">
				     <asp:timer id="Timer2" runat="server" OnTick="Timer2_Tick" Interval="20000"></asp:timer>  
                     
                       <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                    <asp:Label ID="Label2" runat="server" Text="Extra Doses"></asp:Label></font>

                       <br />
                                                 
                                    <asp:GridView ID="GVD_PharmacyExtraDoses"  Width="100%" runat="server" 
                                         AutoGenerateColumns="False" 
                                    PageSize="20" OnRowCommand="GVD_PharmacyExtraDoses_RowCommand"  AllowPaging="True"
                                    DataKeyNames="fdId" CssClass="mGrid"
                                        OnPageIndexChanging="GVD_PharmacyExtraDoses_PageIndexChanging" 
                                        CellPadding="4" ForeColor="#333333">
                                        <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                     <asp:BoundField DataField="SNO" HeaderText="Sr No." HeaderStyle-Font-Size="Small"/>

					<asp:BoundField DataField="App_Time" HeaderText="App Time" HeaderStyle-Font-Size="Small" />
                                       
                                     <asp:BoundField DataField="PNAME" HeaderText="Patient Name" HeaderStyle-Font-Size="Small"/>
                                       
                                     <asp:BoundField DataField="pdCasePaperNo" HeaderText="Case Paper No." HeaderStyle-Font-Size="Small"/>

                                      <asp:BoundField DataField="RMNAME" HeaderText="Remedy" HeaderStyle-Font-Size="Small"/>

                                 <asp:TemplateField HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center" HeaderText="Extra Doses" HeaderStyle-Font-Size="Small" >
                                      <ItemTemplate>
                                          <asp:CheckBox ID="chk_ExtraDoses1" runat="server"  Checked='<%# Convert.ToBoolean(Eval("ExtraDoses1")) %>' 
                                          OnCheckedChanged="chk_ExtraDoses1_CheckedChanged" AutoPostBack="true"  />
                                          <asp:Label ID="lbl_ExtraDoses1" runat="server" Text='<%# Eval("fdId") %>' CssClass="AksDisplay"></asp:Label>


                                          <asp:CheckBox ID="chk_ExtraDoses2" runat="server" Checked='<%# Convert.ToBoolean(Eval("ExtraDoses2")) %>'  
                                          OnCheckedChanged="chk_ExtraDoses2_CheckedChanged" AutoPostBack="true"   />
                                           <asp:Label ID="lbl_ExtraDoses2" runat="server" Text='<%# Eval("fdId") %>' CssClass="AksDisplay"></asp:Label>

                                           <asp:CheckBox ID="chk_ExtraDoses3" runat="server" Checked='<%# Convert.ToBoolean(Eval("ExtraDoses3")) %>' 
                                           OnCheckedChanged="chk_ExtraDoses3_CheckedChanged" AutoPostBack="true"   />
                                             <asp:Label ID="lbl_ExtraDoses3" runat="server" Text='<%# Eval("fdId") %>' CssClass="AksDisplay"></asp:Label>

                                           <asp:CheckBox ID="chk_ExtraDoses4" runat="server" Checked='<%# Convert.ToBoolean(Eval("ExtraDoses4")) %>'
                                           OnCheckedChanged="chk_ExtraDoses4_CheckedChanged" AutoPostBack="true"    />
                                             <asp:Label ID="lbl_ExtraDoses4" runat="server" Text='<%# Eval("fdId") %>' CssClass="AksDisplay"></asp:Label>

                                           <asp:CheckBox ID="chk_ExtraDoses5" runat="server" Checked='<%# Convert.ToBoolean(Eval("ExtraDoses5")) %>'
                                           OnCheckedChanged="chk_ExtraDoses5_CheckedChanged" AutoPostBack="true"    />
                                             <asp:Label ID="lbl_ExtraDoses5" runat="server" Text='<%# Eval("fdId") %>' CssClass="AksDisplay"></asp:Label>

                                      </ItemTemplate>    

                                  </asp:TemplateField>

                                     <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center" HeaderText="Done" HeaderStyle-Font-Size="Small" >
                                      <ItemTemplate>

                                       <asp:ImageButton ID="btnPayAmount" runat="server" CommandName="Pharmacy" Visible='<%# (Convert.ToBoolean(Eval("ViewStat"))) %>'
                                        CommandArgument='<%# Eval("fdId") %>' ImageUrl="~/images/button_ok.png" ToolTip="Done"/>
                                      </ItemTemplate>    

                                    <HeaderStyle Width="5%"></HeaderStyle>

                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                          
                                    </asp:TemplateField>


                                     </Columns>
                                        <EditRowStyle BackColor="#2461BF" />
                                        <HeaderStyle BackColor="#e9f7bc" Font-Bold="True" ForeColor="Black" />
                                        <PagerStyle BackColor="#e9f7bc" ForeColor="Black" HorizontalAlign="left" />
                                        <RowStyle BackColor="#F0F0F0" />
                                    </asp:GridView>
                                </td>                           
                        	  </tr>       
                              
                                        
                                          
                              
                    	  </table>
                        </div>
                     
            </div>
        </td>
    </tr>
    
</table>

 </ContentTemplate> 
    </asp:UpdatePanel> 

</asp:Content>

