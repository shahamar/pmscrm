﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="ManageUser.aspx.cs" Inherits="ManageUser" Title="Manage User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<script type="text/javascript" src="js/custom-form-elements.js"></script>
<style type="text/css">
#tbsearch td
{
    text-align: left;
    vertical-align: middle;
}

#tbsearch label
{
    display: inline;
    margin-top: 3px;
    position: absolute;
}

#tbhead th
{
    text-align: left;
    background: url(images/nav-back.gif) repeat-x top;
    color: #FFFFFF;
    padding-left: 2px;
    padding-top: 3px;
}

#gvcol div
{
    margin-top: -10px;
}

.head1
{
    width: 15%;
}

.head2
{
    width: 20%;
}

.head3
{
    width: 10%;
}

.head4 { width: 10%;}
.head5 { width: 15%;}

.head5i
{
    width: 15%;
}

.head6
{
    width: 10%;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Manage User</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo first" id="li_1"><a class="qt_tab active" href="AddUser.aspx">Add
                                            New User</a> </li>
                                        <li class="qtab-HTML active last" id="li_9"><a class="qt_tab active" href="javascript:void(0)">
                                            Manage Users</a> </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto;">
                                        <div style="padding: 15px; height: auto;">
                                            <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                <tr>
                                                    <td colspan="3" style="text-align: center; color: Red;">
                                                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" style="text-align:right; color:Red;">
                                                        <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                            <tr>
                                                                <th class="head1">
                                                                  Name
                                                                </th>
                                                                <th class="head2">
                                                                    User Roll
                                                                </th>
                                                                
                                                                <th class="head3">
                                                                    Contact No.
                                                                </th>
								<th>
									Birth Date
								</th>
                                                                 <th class="head6">
                                                                 Action
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th class="head1">
                                                                    <asp:TextBox ID="txtUName" runat="server" Width="130px"></asp:TextBox>
                                                                </th>
                                                                <th class="head2">
                                                                </th>
                                                                <th class="head3">
                                                                </th>
								<th width="10%"></th>
                                                                <th class="head6">
                                                                    <div style="display: block; width: 68px; margin-left: 14px;">
                                                                        <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none; color: #FFF;
                                                                            text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click">
                                                                <img src="images/filter.png" style="border:0px;"/>
                                                                Filter</asp:LinkButton>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <tr>
                                                        <td id="gvcol" colspan="3">
                                                            <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="50" ShowHeader="false" Width="100%"
                                                                OnRowCommand="GRV1_RowCommand" DataKeyNames="_uRole,uName,uId" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%# Eval("FullName")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                             <%# Eval("_uRole")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%# Eval("uContact")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
								    <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
									<ItemTemplate>
										<%# Eval ("BirthDate")%>
									</ItemTemplate>
								    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                        <%--<% if (Session["urole"] == "1")
                                                                           { %>--%>
                                                                            <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'
                                                                                CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                            <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'
                                                                                CommandName="del" ImageUrl="Images/delete.ico" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete ?');" />
                                                                            <asp:ImageButton ID="btnAccess" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'
                                                                                CommandName="acc" ToolTip="Manage Menu Access" ImageUrl="~/images/user_access.png" />
                                                                            <%--<% } %>--%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;
                                                            
                                                        </td>
                                                        <td>&nbsp;
                                                            
                                                        </td>
                                                        <td>&nbsp;
                                                            
                                                        </td>
                                                    </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
