using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class CompanyManager : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlCommand sqlcmd;
    genral gen = new genral();
    int Page_no = 0, Page_size = 10;
    string strsql, strSQL;
    int cid = 0;
    int uid = 0;
    string flag = "";

	protected void Page_Load(object sender, EventArgs e)
	{
        if (Session["userid"] != null)
        {
            uid = int.Parse(Session["userid"].ToString());
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }
        if (Request.QueryString["cid"] != null)
        {
            cid = int.Parse(Request.QueryString["cid"].ToString());
        }

        if (Request.QueryString["flag"] == "1")
            lblMessage.Text = "Company Saved Successfully...!";
        
        if (Request.QueryString["flag"] == "2")
            lblMessage.Text = "Company Updated Successfully...!";

        if (Request.QueryString["flag"] == "3")
            lblMessage.Text = "Company Deleted Successfully...!";

        strsql = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 17";
        bool exists = gen.doesExist(strsql);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");


		if(!IsPostBack)
		{
            BindGVR();
		}
	}

    protected void BindGVR()
    {
        strSQL = "SELECT * FROM tblCompanyMaster Where IsDelete = 0 ";
        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        createpaging(dt.Rows.Count, GRV1.PageCount);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (cid == 0)
            {
                strsql = "Insert into tblCompanyMaster(Cname, CCreatedBy) values (@Cname, @CCreatedBy)";
            }
            else
            {
                strsql = "Update tblCompanyMaster set Cname = @Cname, ModifiedBy = @CCreatedBy , ModifiedDate = getDate() WHERE Cid =" + cid ;
            }

            sqlcmd = new SqlCommand(strsql, con);
            sqlcmd.CommandType = CommandType.Text;
            sqlcmd.Parameters.AddWithValue("@Cname", txtCompany.Text.Trim());
            sqlcmd.Parameters.AddWithValue("@CCreatedBy", uid );
            sqlcmd.ExecuteNonQuery();

            if (cid == 0)
            {
                Response.Redirect("StockManager.aspx");
            }
            else
            {
                Response.Redirect("CompanyMaster.aspx?flag=2");
            }
            con.Close();
        }
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int cid = Convert.ToInt32(e.CommandArgument);
        if (e.CommandName == "edt")
        {
                strsql = "select Cname from tblCompanyMaster where IsDelete = 0 And Cid = " + cid;
		        sqlcmd = new SqlCommand(strsql, con);
                sqlcmd.CommandType = CommandType.Text;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlDataReader sqldr = sqlcmd.ExecuteReader();
                while (sqldr.Read())
                {
                    txtCompany.Text = sqldr["Cname"].ToString();
                }
                sqldr.Dispose();
                sqlcmd.Dispose();
                con.Close();
        }
        if (e.CommandName == "del")
        {
            try
            {
                gen.executeQuery("Update tblCompanyMaster Set IsDelete = 1 Where Cid =" + cid);
                Response.Redirect("CompanyMaster.aspx?flag=3", false);
            }
            catch
            {
            }
        }
        BindGVR();
    }
    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }
}