﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class VoucherReceipt : System.Web.UI.Page
{
    genral gen = new genral();
    string vid = "";


    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["PdID"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["PdID"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
            vid = Request.QueryString["id"].ToString();

        if (!IsPostBack)
        {

            //if (Session["CasePatientID"] != null)
            //    ViewState["PdID"] = Session["CasePatientID"].ToString();


            //if (Request.QueryString["id"] != null)
            //{
            //    ViewState["ipdid"] = Request.QueryString["id"];
            //}
            //else
            //{
            //    if (Session["ipdid"] != null)
            //        ViewState["ipdid"] = Session["ipdid"].ToString();
            //    else
            //    {
            //        //Session["ipdid"] = Convert.ToInt32(gen.executeScalar("Select ipdId From tblIpdMaster Where ipdpdId =" + PdID).ToString() + " And IsDelete = 0");
            //        //ViewState["ipdid"] = Session["ipdid"];
            //    }
            //}
            FillData();
        }
    }

    protected void FillData()
    {
        SqlCommand sqlcmd = new SqlCommand();
        sqlcmd.CommandText = "sp_GetVoucherDetails";
        sqlcmd.CommandType = CommandType.StoredProcedure;
        //sqlcmd.Parameters.AddWithValue("@ipdid", ipdid);
        //sqlcmd.Parameters.AddWithValue("@pdID", PdID);
	    sqlcmd.Parameters.AddWithValue("@VoucherID", vid);//Shweta
        DataTable dt = new DataTable();
        dt = gen.getDataTable(sqlcmd);
        if (dt.Rows.Count > 0)
        {
            rptBill.DataSource = dt;
            rptBill.DataBind();
        }
    }

}