﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="AF.aspx.cs" Inherits="AF" Title="A/F" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="cc1" %>
<%@ Register Src="tab.ascx" TagName="Tab" TagPrefix="tab1" %>
<%@ Register Src="~/tabReconsult.ascx" TagName="tabR" TagPrefix="TabR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sp_counter').text("0");
            setHeartbeat();
        });

        function setHeartbeat() {
            //alert("ok");
            setTimeout("heartbeat()", 300000); // about to 5 minutes
        }

        function heartbeat() {
            $.get(
                '<%= ResolveUrl("Handler/SessionHeartbeat.ashx") %>',
                null,
                function () {

                },
                "json"
            );
            setHeartbeat();
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Timer ID="timaf" runat="server" Interval="15000" OnTick="timaf_Tick">
    </asp:Timer>
    <asp:UpdatePanel runat="server" ID="UP1">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="timaf" EventName="Tick" />
        </Triggers>
    </asp:UpdatePanel>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        Profarma Of Case Taking-AF</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <tab1:Tab ID="tabKCO" runat="server"></tab1:Tab>
                            <div id="div1" runat="server" style="display: none;">
                                <TabR:tabR ID="tabRKCO" runat="server" />
                            </div>
                            <ul class="quicktabs_tabs quicktabs-style-excel" style="border-left: 2px solid #E2E2E2;
                                border-right: 2px solid #E2E2E2;">
                                <li class="qtab-Demo first" id="li_1">
                                    <asp:LinkButton ID="lnkKCO" runat="server" CssClass="qt_tab active" PostBackUrl="~/ProfarmaOfCase.aspx">K/C/O</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_2">
                                    <asp:LinkButton ID="lnkInvestigation" runat="server" CssClass="qt_tab active" PostBackUrl="Investigation.aspx">Investigation</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_3">
                                    <asp:LinkButton ID="lnkChiefCo" runat="server" CssClass="qt_tab active" PostBackUrl="~/ChiefCO.aspx">Chief C/O</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_4">
                                    <asp:LinkButton ID="lnkPastHo" runat="server" CssClass="qt_tab active" PostBackUrl="~/PastHO.aspx">Past H/o</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_5">
                                    <asp:LinkButton ID="lnkFamilyHo" runat="server" CssClass="qt_tab active" PostBackUrl="~/FamilyHo.aspx">Family H/o</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_6">
                                    <asp:LinkButton ID="lnkPhysicalgenerals" runat="server" CssClass="qt_tab active"
                                        PostBackUrl="~/PhysicalGenerals.aspx">Physical Generals</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_7">
                                    <asp:LinkButton ID="lnkMind" runat="server" CssClass="qt_tab active" PostBackUrl="~/Mind.aspx">Mind</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML active" id="li_8">
                                    <asp:LinkButton ID="lnkAf" runat="server" CssClass="qt_tab active" Enabled="False">A/F</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_9">
                                    <asp:LinkButton ID="lnkThermal" runat="server" CssClass="qt_tab active" Enabled="True"
                                        PostBackUrl="~/Thermal.aspx">Thermal</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML last" id="li_10">
                                    <asp:LinkButton ID="lnkUploadDoc" runat="server" CssClass="qt-tab active" PostBackUrl="~/UploadScanDocs.aspx">Upload Docs</asp:LinkButton>
                                </li>
                            </ul>
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                <div style="height: 400px;">
                                    <cc1:Editor ID="kco" runat="server" Height="400px" />
                                </div>
                                <div class="kwordcontdiv">
                                    <div style="text-align: center; width: 36%; float: left; margin-top: 10px;">
                                        Keywords To Search (seperated by comma) :
                                    </div>
                                    <div style="text-align: right; width: 64%; float: right;">
                                        <asp:TextBox ID="txtKco" runat="server" TextMode="MultiLine" Width="600px" Height="30px"> </asp:TextBox>
                                    </div>
                                </div>
                                <div class="savecontdiv">
                                    <asp:Button ID="btnNext" runat="server" CssClass="textbutton b_submit" Style="margin-top: 0px;"
                                        Text="Save" OnClick="btnNext_Click" />
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
