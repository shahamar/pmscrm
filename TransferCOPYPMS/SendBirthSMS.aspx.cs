﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;


public partial class SendBirthSMS : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string strSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (!IsPostBack)
        {
            BirthDayPatientList();
        }

    }

    private void BirthDayPatientList()
    {
        strSQL = "Proc_FillBirthPatientList";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                gridBirthList.DataSource = dt;
                gridBirthList.DataBind();
            }
            con.Close();
            dr.Dispose();
        }
    }

    protected void btn_Send_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow g1 in gridBirthList.Rows)
        {
            CheckBox chkstat = g1.FindControl("chkstat") as CheckBox;
            Label lblpdid = g1.FindControl("lblpdid") as Label;
            Label lblpdMob = g1.FindControl("lblpdMob") as Label;
            Label lblName = g1.FindControl("lblName") as Label;


            if (chkstat.Checked == true)
            {

                if (lblpdMob.Text != "")
                {
                    String smsstring;
                    decimal TemplateId = 1207160931701257871;
                    smsstring = "Wish you a many many happy returns of the day. May God bless you with health, wealth and prosperity in your life. Aditya Homoeopathic Hospital and Healing center";
                    string strsendSMS = genral.GetResponse(lblpdMob.Text.Trim(), smsstring, TemplateId);

                    if (con.State == ConnectionState.Closed)
                        con.Open();

                    SqlCmd = new SqlCommand();
                    SqlCmd.CommandText = "sp_InsSMSTransaction";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Connection = con;
                    SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                    SqlCmd.Parameters.AddWithValue("@MobileNo", lblpdMob.Text.Trim());
                    SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                    SqlCmd.Parameters.AddWithValue("@SenderID", userid);
                    SqlCmd.Parameters.AddWithValue("@Pdid", lblpdid.Text);
                    SqlCmd.Parameters.AddWithValue("@Sources", "BirthDay");
                    SqlCmd.ExecuteNonQuery();
                    SqlCmd.Dispose();
                    con.Close();
                }
            }
        }

        lblMessage.Text = "BirthDay SMS Send Successfully";
    }
}