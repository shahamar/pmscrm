using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class AddNewReconsulting : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    //string userrole;
    string strSQL, StrSql, StrSQL = "";
    int RecID = 0;
    string msg;
    SqlDataReader sqldr;
    int Page_no = 0, Page_size = 20;
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CasePatientID"] != null)
            ViewState["pdId"] = Session["CasePatientID"].ToString();
        else
            Response.Redirect("Home.aspx?flag=1");

        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["RecID"] != null)
            RecID = Convert.ToInt32(Request.QueryString["RecID"].ToString());

        if (Request.QueryString["flag"] == "1")
            lblMessage.Text = "Reconsulting details saved successfully.";

        if (Request.QueryString["flag"] == "2")
            lblMessage.Text = "Reconsulting details Updated Successfully.";

        if (Request.QueryString["flag"] == "3")
            lblMessage.Text = "Reconsulting details Deleted successfully.";

        if (!IsPostBack)
        {
            txtReconsultingAmt.Text = "0";
            txtDOR.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();
            BindGVR();
        }
        if (!(Session["Report"] as string == "{CrystalDecisions.CrystalReports.Engine.ReportDocument}"))
        {
            CrystalReportViewer2.ReportSource = (ReportDocument)Session["Report"];
        }
    }
    private void FillData()
    {
        BindDoctor();
        OldCPN();
        if (RecID != 0)
        {
            strSQL = "select * from tblReconsultingDetails where RecId =" + RecID;
            SqlCmd.CommandType = CommandType.Text;
            SqlDataReader sqldr;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd = new SqlCommand(strSQL, con);
            sqldr = SqlCmd.ExecuteReader();
            while (sqldr.Read())
            {
                txtReconsultingNo.Text = sqldr["ReconsultingNo"].ToString();
                DDL_Pay_Mode.SelectedValue = sqldr["Payment_Mode"].ToString();
                DDDoctor.SelectedValue = sqldr["did"].ToString();
                ddRegFees.SelectedValue = sqldr["RecReg_Fees"].ToString();
                txtReconsultingAmt.Text = sqldr["ReconsultingCharges"].ToString();
                txtDOR.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(sqldr["RecDate"].ToString()));
                if (DDDoctor.SelectedValue == "")
                {
                    DDDoctor.Items.Insert(1, new ListItem(sqldr["FullName"].ToString()));
                }
            }

            sqldr.Close();
            SqlCmd.Dispose();
            con.Close();
        }
    }
    public void BindDoctor()
    {
        DDDoctor.Items.Clear();
        DDDoctor.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT * FROM vwGetDoctor", "did", "FullName", DDDoctor);
    }
    public void OldCPN()
    {
        try
        {
            StrSql = "Select pdCasePaperNo From tblPatientDetails where pdId = " + pdId;
            SqlCmd = new SqlCommand(StrSql, con);
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader Sqldr = SqlCmd.ExecuteReader();
            while (Sqldr.Read())
            {
                txtoldCPN.Text = Sqldr["pdCasePaperNo"].ToString();
            }
            Sqldr.Close();
            SqlCmd.Dispose();
            con.Close();
            //con.Dispose();
        }
        catch (Exception e)
        {

        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            //string RecReceiptNo = "";
            string finyear = gen.GetCurrentFinYear();
            //object o = gen.executeScalar("Select CONVERT(VARCHAR,ISNull(MAX(CAST(SubString(RecReceiptNo,1,len(RecReceiptNo)-4) As INT)),0)+1)+'" + finyear + "' From tblReconsultingDetails where RecCreatedDate > '2016-04-01 00:00:00.001'");
            //RecReceiptNo = o.ToString();
            if (RecID == 0)
                strSQL = "sp_InsReconsultingDetails";
            else
                strSQL = "sp_UpdtReconsultingDetails";
            

            string[] _dor = txtDOR.Text.ToString().Split('/');
            string dor = _dor[2] + "/" + _dor[1] + "/" + _dor[0];

            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@RecPatientId", pdId);
            SqlCmd.Parameters.AddWithValue("@RecDate", dor);
            SqlCmd.Parameters.AddWithValue("@RecReg_Fees", ddRegFees.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@RecCreatedBy", userid);
            SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@ReconsultingNo", txtReconsultingNo.Text);
            SqlCmd.Parameters.AddWithValue("@ReconsultingCharges", Convert.ToDecimal(txtReconsultingAmt.Text));
            SqlCmd.Parameters.AddWithValue("@did", DDDoctor.SelectedValue);
            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (RecID == 0)
            {
                SqlCmd.Parameters.AddWithValue("@RecID", RecID);
                SqlCmd.Parameters.AddWithValue("@RecReceiptNo", finyear);
            }
            else
            {
                SqlCmd.Parameters.AddWithValue("@RecID", RecID);
                //SqlCmd.Parameters.Add("@RecID", SqlDbType.Int).Direction = ParameterDirection.Output;
                SqlCmd.Parameters.AddWithValue("@RecReceiptNo", finyear);
                
            }
            SqlCmd.ExecuteNonQuery();
            
            if (chk_ReconsultingBill.Checked == true)
            {
                ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page), "click", @"<script>window.open('ReConsultingBill.aspx?Id=" + RecID + "','_newtab');</script>", false);
            }

           
            if (chk_PrintReconBill.Checked == true)
            {
                    CrystalReportViewer2.Visible = true;
                    string AksConn = ConfigurationManager.AppSettings["constring"];
                    bindReport(AksConn, pdId);
            }
           
            if (RecID == 0)
            {
                Response.Redirect("AddNewReconsulting.aspx?flag=1");
            }
            else
            {
                Response.Redirect("AddNewReconsulting.aspx?flag=2");
            }
            SqlCmd.Dispose();
            con.Close();       
        }
    }
    public void bindReport(string AksConn, int pdId)
    {
        DataTable dt = new DataTable();
        ReportDocument cryRpt = new ReportDocument();
        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = default(CrystalDecisions.CrystalReports.Engine.Tables);
        System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

        builder.ConnectionString = AksConn;

        string AksServer = builder["Server"] as string;
        string AksDatabase = builder["Database"] as string;
        string AksUsername = builder["Uid"] as string;
        string AksPassword = builder["Pwd"] as string;


        dt = GetReport(pdId);

        cryRpt.Load(Server.MapPath("~/Reports/ReconsultingPrintReport.rpt"));

        CrTables = cryRpt.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        cryRpt.SetDataSource(dt);
        cryRpt.Refresh();

        //cryRpt.SetParameterValue("FromDate", "");
        //cryRpt.SetParameterValue("ToDate", "");

        CrystalReportViewer2.ReportSourceID = "CrystalReportSource2";
        CrystalReportViewer2.ReportSource = cryRpt;

        System.IO.Stream oStream = null;
        byte[] byteArray = null;
        oStream = cryRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        byteArray = new byte[oStream.Length];

        Session.Add("COMMPrintingData", byteArray);
        Session.Add("PrintingEventForm", "COMM_Print");

        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
        Session["byteArray"] = byteArray;
        cryRpt.Close();
        cryRpt.Dispose();


    }
    public DataTable GetReport(int pdid)
    {
        DataTable dt = new DataTable();

        StrSQL = "Proc_GetNewCaseNReconsultingData";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@pdID", pdId);
        SqlCmd.Parameters.AddWithValue("@Flag", "NewReconsultingForm");
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();

            dt.Load(dr);

        }

        return dt;

    }
    //protected void Page_PreRender(object sender, EventArgs e)
    //{
    //    ViewState["update"] = Convert.ToString(Session["update"]);
    //}
    protected void ddRegFees_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddRegFees.SelectedValue == "1")
        {
            txtReconsultingAmt.Enabled = true;
            txtReconsultingAmt.Text = "750";
        }
        else if (ddRegFees.SelectedValue == "3")
        {
            txtReconsultingAmt.Text = "1000";
        }
        else
        {
            txtReconsultingAmt.Enabled = true;
            txtReconsultingAmt.Text = "0";
        }
    }
    public void BindGVR()
    {
        strSQL = "select ROW_NUMBER()OVER(PARTITION by  RecPatientId order by RecPatientId)as 'rank', dFName+' '+dLName as 'Doctor',CASE WHEN RecReg_Fees In (1,3) THEN '<a href=''ReConsultingBill.aspx?id='+CAST(RD.RecId AS VARCHAR)+'''& target=_blank><img src=''images/print1.png'' /></a>' else '' END AS 'PrintBill', * from tblReconsultingDetails RD Inner Join tblDoctorMaster DM on DM.did = RD.did where IsDelete = 0 and RecPatientID = " + pdId;
        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
    }
    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string RecID = e.CommandArgument.ToString();
        if (e.CommandName == "del")
        {
            try
            {
                gen.executeQuery("Update tblReconsultingDetails Set IsDelete = 1 Where RecId=" + RecID);
                Response.Redirect("AddNewReconsulting.aspx?flag=3", false);
            }
            catch
            {
            }
        }
        if (e.CommandName == "edt")
        {
            //int RecId = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("AddNewReconsulting.aspx?RecId=" + e.CommandArgument);
        }
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        BindGVR();
    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;
    }
}