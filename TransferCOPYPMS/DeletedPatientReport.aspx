﻿<%@ Page Title="Deleted Patient Report" Language="C#" MasterPageFile="~/MasterHome1.master"
    AutoEventWireup="true" CodeFile="DeletedPatientReport.aspx.cs" Inherits="DeletedPatientReport" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <link rel="stylesheet" type="text/css" href="css/radiotabstrip.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <style type="text/css">
        #tbsearch td
        {
            text-align: left;
            vertical-align: middle;
        }
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        #gvcol div
        {
            margin-top: -10px;
        }
        .head1
        {
            width: 10%;
        }
        .head2
        {
            width: 20%;
        }
        .head3
        {
            width: 8%;
        }
        .head4
        {
            width: 10%;
        }
        .head5
        {
            width: 13%;
        }
        .head5i
        {
            width: 13%;
        }
        .head5ii
        {
            width: 19%;
        }
        .head6
        {
            width: 7%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2 align="center">Report</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo first" id="li_1">
                                               <asp:LinkButton ID="lnkOpd" runat="server" CssClass="qt_tab active" PostBackUrl="~/OPDReport.aspx">OPD</asp:LinkButton>
                                            </li>
                                        <li class="qtab-HTML" id="li_2">
                                            <asp:LinkButton ID="lnk3dccg" runat="server" CssClass="qt_tab active" PostBackUrl="~/3DCCGReport.aspx">3DCCG</asp:LinkButton></li>
                                        <li class="qtab-HTML" id="li_3">
                                            <asp:LinkButton ID="lnlipd" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDReport.aspx">IPD</asp:LinkButton></li>
                                        <li class="qtab-HTML" id="li_4">
                                            <asp:LinkButton ID="lnkConsulting" runat="server" CssClass="qt_tab active" PostBackUrl="~/ConsultingReport.aspx">Consulting</asp:LinkButton></li>
                                        <li class="qtab-HTML" id="li_5">
                                         <asp:LinkButton ID="lnkPft" runat="server" CssClass="qt_tab active" Enabled="false">PFT</asp:LinkButton>
                                         </li>
                                        <li class="qtab-HTML" id="li_6">
                                            <asp:LinkButton ID="LnkReconsulting" runat="server" CssClass="qt_tab active" PostBackUrl="~/ReConsulting.aspx">Reconsulting</asp:LinkButton></li>
                                        <li class="qtab-HTML" id="li_7">
                                            <asp:LinkButton ID="LnkECG" runat="server" CssClass="qt_tab active" PostBackUrl="~/ECGReport.aspx">ECG</asp:LinkButton>
                                        </li>
                                        <li class="qtab-Demo last" id="li_8">
                                            <asp:LinkButton ID="LikCourier" runat="server" CssClass="qt_tab active" PostBackUrl="~/CourierReport.aspx">Courier</asp:LinkButton></li>
                                        <li class="qtab-Demo last" id="li_9">
                                            <asp:LinkButton ID="LnkAdvance" runat="server" CssClass="qt_tab active" PostBackUrl="~/AdvanceReport.aspx">Advance</asp:LinkButton>
                                        </li>
                                        <li class="qtab-Demo last" id="li_10">
                                            <asp:LinkButton ID="LnkBalance" runat="server" CssClass="qt_tab active" PostBackUrl="~/BalanceReport.aspx">Balance</asp:LinkButton>
                                        </li>
                                        <li class="qtab-Demo last active" id="li_11">
                                            <asp:LinkButton ID="LnkPnt" runat="server" CssClass="qt_tab active" PostBackUrl="~/DeletedPatientReport.aspx">Deleted Patient</asp:LinkButton>
                                        </li>
					<li class="qtab-Demo last" id="li_12">
                                            <asp:LinkButton ID="LnkDietn" runat="server" CssClass="qt_tab active" PostBackUrl="~/DieticianReport.aspx">Dietician</asp:LinkButton>
                                        </li>
                                        <li class="qtab-Demo last" id="li_13">
                                            <asp:LinkButton ID="LnkDelIPD" runat="server" CssClass="qt_tab active" PostBackUrl="~/DeletedIPDReport.aspx">Deleted IPD</asp:LinkButton>
                                        </li>
					<li class="qtab-HTML" id="li_14">
                                            <asp:LinkButton Id="LnkVoucher" runat="server" CssClass="qt_tab active" PostBackUrl="~/VoucherReport.aspx">Voucher</asp:LinkButton>
                                        </li>
					<li class="qtab-HTML last" id="li_15">
                                            <asp:LinkButton Id="LnkTotal" runat="server" CssClass="qt_tab active" PostBackurl="~/GetTotalInvoiceAmountofAllReport.aspx">Total Report</asp:LinkButton>
                                        </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto; border: 1.5px solid #E2E2E2;">
                                        <table width="100%" cellspacing="0px" cellpadding="2px">
                                            <tr>
                                                <td>
                                                    <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                        <tr>
                                                            <td class="tableh1">
                                                                <table id="tbsearch" style="width: 100%;">
                                                                    <tr>
                                                                        <td>
                                                                            Deleted From
                                                                      &nbsp;
                                                                            <asp:TextBox ID="txtfrm" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                                runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                width: 20px; height: 20px;" />
                                                                        </td>
                                                                        <td>
                                                                            To
                                                                      
                                                                            <asp:TextBox ID="txtto" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                                Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                width: 20px; height: 20px;" />
                                                                        </td>

                                                                         <td>Prepared By : &nbsp;
                                                                              <asp:DropDownList ID="DDL_User" runat="server" class="required field"  Width="150px">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>

                                                                       <tr>

                                                                    <td colspan="3">
                                                                   
                                                                     <table width="100%">
                                                                   <tr>
                                                                   <td style="width:2%">
                                                                             <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                                                                OnClientClick="aspnetForm.target ='_self';" OnClick="btnView_Click" />
                                                                        </td>
                                                                        <td style="width:2%">
                                                                             <div id="export" runat="server" style="float: right; display: none">
                                                                                <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" OnClick="imgexport_Click" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="color: Red; text-align: right;">
                                                                              <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                                        </td>
                                                                   </tr>
                                                                   </table>
                                                                    
                                                                    </td>

                                                                   
                                                                    </tr>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <asp:Panel ID="pnlHead" runat="server" Visible="false">
                                                <div id="rptHead" runat="server" visible="false">
                                                    <table width="100%" id="rounded-corner-report">
                                                        <tr>
                                                            <td align="center">
                                                                Aditya Homoeopathic Hospital & Healing Center<br />
                                                                Pimpri Gaon, Pune- 411 017
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="font-weight: bold">
                                                                Deleted Patient Report
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="gvcol">
                                                                <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                    CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" ShowHeader="true" Width="100%"
                                                                    OnPageIndexChanging="GRV1_PageIndexChanging" OnRowCommand="GRV1_RowCommand" OnRowDataBound="GRV1_RowDataBound">
                                                                    <Columns>
                                                                         <asp:TemplateField HeaderText="Prepared By" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                       <asp:label runat="server" ID="lblUser" Text='<%#Eval("UserName")%>'></asp:label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="ICard No">
                                                                            <ItemTemplate>
                                                                                <%# Eval("pdPatientICardNo")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Name">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkpdName" runat="server" Text='<%# Eval("pdName")%>' CommandArgument='<%# Eval("pdID") %>'
                                                                                    CommandName="case" Style="text-decoration: none; color: Blue; font-weight: normal;"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Gender">
                                                                            <ItemTemplate>
                                                                                <%# Eval("pdSex")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="DOC">
                                                                            <ItemTemplate>
                                                                                <%# Eval("pdDOC", "{0:dd-MMM-yyyy}")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Case Paper No.">
                                                                            <ItemTemplate>
                                                                                <%# Eval("pdCasePaperNo")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Cassette No.">
                                                                            <ItemTemplate>
                                                                                <%# Eval("pdCassetteNo")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Occupation">
                                                                            <ItemTemplate>
                                                                                <%# Eval("pdOccupation")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <%--<tr>
                                                            <td colspan="10">
                                                                <table width="100%" cellspacing="0" border="1" class="mGrid">
                                                                    <tr>
                                                                        <td style="width: 59%">
                                                                            <asp:Label ID="Label1" Font="Bold" runat="server" Text="Sub Payment"></asp:Label></b>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <b>
                                                                                <asp:Label ID="lblSubTotal" runat="server" Text="0.00"></asp:Label></b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 59%">
                                                                            <b>Total Payment</b>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <b>
                                                                                <asp:Label ID="lblGrandTotal" runat="server" Text="0.00"></asp:Label></b>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>--%>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
