﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;

public partial class tabipd : System.Web.UI.UserControl
{
    //string pdId = "0";
    string strSQL = "";
    SqlDataReader Sqldr;
    SqlCommand SqlCmd;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    //added by nikita on 12th march 2015------------------------------
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }

    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    //-----------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
            {
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            }
            else
            {
                Response.Redirect("Home.aspx?flag=1");
            }


            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            //else
            //{
            //    Session["ipdid"] = Convert.ToInt32(gen.executeScalar("Select ipdId From tblIpdMaster Where ipdpdId =" + pdId).ToString());
            //    ViewState["ipdid"] = Session["ipdid"];
            //}

            FillData();
        }
    }

    private void FillData()
    {
        strSQL = "Select pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName', pdPatientICardNo , pdAge , pdCasePaperNo , pdCassetteNo , pdemail , pdSex , pdMob , pdTele , pdOccupation , WardTypeName , wmWardNo , bmNoOfBads , IsNull(DM.dFName,DMH.dFName) + ' ' + IsNull(DM.dMName,DMH.dMName) + ' ' + isNull(DM.dLName,DMH.dLName) AS DoctorName ,GWSD.ipdAdmissionDate As ipdAdmissionDate ,GWSD.ipdAdmissionTime As ipdAdmissionTime ,GWSD.pdWeight As pdWeight ,  Upper(State) As State , Upper(City) As City , PD.pdDOC As pdDOC , pdAdd1 AddS From dbo.tblPatientDetails PD Left Join tblDoctorMaster DM On DM.did = PD.did Left Join tblDoctorHistoryDetails DMH On PD.did = DMH.did Left Join VW_GetWardNBedStatus_Test GWSD On PD.pdID = GWSD.pdID Left Join tblStateMaster On StateID = pdState Left Join tblCityMaster On CityID = pdcity where PD.pdID = " + pdId;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            lblPatientName.Text = Sqldr["pdName"].ToString();
            lblPatientId.Text = Sqldr["pdPatientICardNo"].ToString();
            lblAge.Text = Sqldr["pdAge"].ToString();
            lblCasePaperNo.Text = Sqldr["pdCasePaperNo"].ToString();
            lblCassetteNo.Text = Sqldr["pdCassetteNo"].ToString();
            lblemail.Text = Sqldr["pdemail"].ToString();
            lblGender.Text = Sqldr["pdSex"].ToString();
            lblmob.Text = Sqldr["pdMob"].ToString();
            lbltele.Text = Sqldr["pdTele"].ToString();
            lblDoctorName.Text = Sqldr["DoctorName"].ToString();

            if (Sqldr["pdOccupation"] != DBNull.Value)
            {
                lblOccupation.Text = Sqldr["pdOccupation"].ToString();
            }

            lblWardType.Text = Sqldr["WardTypeName"].ToString();
            lblWard.Text = Sqldr["wmWardNo"].ToString();
            lblBed.Text = Sqldr["bmNoOfBads"].ToString();

            lblWeight.Text = Sqldr["pdWeight"].ToString();

            if (Sqldr["ipdAdmissionDate"] != DBNull.Value || Sqldr["ipdAdmissionDate"].ToString() != "")
            lblAdmDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["ipdAdmissionDate"].ToString()));

            lblAdmTime.Text = Sqldr["ipdAdmissionTime"].ToString();

            lblState.Text = Sqldr["State"].ToString();
            lblCity.Text = Sqldr["City"].ToString();
            lblConsDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["pdDOC"].ToString()));
            lblAddress.Text = Sqldr["AddS"].ToString();
        }
        Sqldr.Close();
        SqlCmd.Dispose();

        BindPatientBillingDetails();

    }

    public void BindPatientBillingDetails()
    {
        decimal VoucherAmt = 0;
        strSQL = "Exec  sp_GenerateBill_CurrentBill " + ipdid;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            lblBill.Text = Sqldr["TotalAmount"].ToString();
            lblDisc.Text = Sqldr["Deposit"].ToString();
            lblBalance.Text = Sqldr["CurrentBillAmount"].ToString();
            lblBalance.ForeColor = Color.Red;
            VoucherAmt = Convert.ToDecimal(Sqldr["VoucherAmt"]);
            if (VoucherAmt > 0)
            {
                lblVoucherAmt.Text = " ( " + Sqldr["VoucherAmt"].ToString() + " ) ";
                lblVoucherAmt.Visible = true;
            }
            else
            {
                lblVoucherAmt.Text = "";
                lblVoucherAmt.Visible = false;
            }

        }
        Sqldr.Close();
        SqlCmd.Dispose();
    }
}
