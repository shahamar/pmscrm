﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;


public partial class AddIPDRound : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole, msg;
    string uid = "";
    int InvId = 0;

    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["PdID"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["PdID"].ToString());
        }
    }

    public string BloodTestFile
    {
        get
        {
            if (ViewState["BloodTestFile"] == null)
                return "";
            else
                return ViewState["BloodTestFile"].ToString();
        }
        set
        {
            ViewState["BloodTestFile"] = value;
        }
    }

    public string UrineTestFile
    {
        get
        {
            if (ViewState["UrineTestFile"] == null)
                return "";
            else
                return ViewState["UrineTestFile"].ToString();
        }
        set
        {
            ViewState["UrineTestFile"] = value;
        }
    }

    public string Monitor
    {
        get
        {
            if (ViewState["Monitor"] == null)
                return "";
            else
                return ViewState["Monitor"].ToString();
        }
        set
        {
            ViewState["Monitor"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Session["msg"] != null)
            msg = Session["msg"].ToString();

        if (Request.QueryString["Id"] != null)
            InvId = int.Parse(Request.QueryString["Id"].ToString());
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["PdID"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            else
            {
                Session["ipdid"] = Convert.ToInt32(gen.executeScalar("Select ipdId From tblIpdMaster Where ipdpdId =" + PdID).ToString());
                ViewState["ipdid"] = Session["ipdid"];
            }

            txt_InvDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txt_Vtime.Text = string.Format("{0:hh:mm tt}", DateTime.Now);

            FillData();

            if (ViewState["BloodTestFile"] != null)
                ViewState["BloodTestFile"] = null;

            if (ViewState["UrineTestFile"] != null)
                ViewState["UrineTestFile"] = null;

            if (ViewState["Monitor"] != null)
                ViewState["Monitor"] = null;

         
        }
    }

   
    protected void FillData()
    {
        DDL_Doctor.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select uId , uFname +' '+ uMname +' '+ uLname As DNAME from tblUser", "uId", "DNAME", DDL_Doctor);
        DDL_Doctor.SelectedValue = uid;

        DDL_Remedy.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select * From tblRemedyMaster where isDelete = 0 AND RMNAME != '0' ORDER BY REPLACE(RMNAME,' ','') ASC", "RMID", "RMNAME", DDL_Remedy);

        BindPatientKCODetails();
        BindPatientLastRemedyDetails();
        BindSuggestionInvDetails();
        BindPatientIPDRoundDetails();
     
    }

   
    public void BindPatientKCODetails()
    {
        if (File.Exists(Server.MapPath("Files/KCO/kco_" + PdID)))
        {
            lblkco.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + PdID).ToString());
            divkco.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/Investigation/inv_" + PdID)))
        {
            lblInv.Text = File.ReadAllText(Server.MapPath("Files/Investigation/inv_" + PdID).ToString());
            divinv.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/ChiefCo/chief_" + PdID)))
        {
            lblChiefCo.Text = File.ReadAllText(Server.MapPath("Files/ChiefCo/chief_" + PdID).ToString());
            divchief.Style.Add("display", "block");
            txt_FinalDign.Text = File.ReadAllText(Server.MapPath("Files/ChiefCo/chief_" + PdID).ToString());
            txt_FinalDign.Text = txt_FinalDign.Text.Replace("<br />", "");
        }
        if (File.Exists(Server.MapPath("Files/PastHo/pho_" + PdID)))
        {
            lblpho.Text = File.ReadAllText(Server.MapPath("Files/PastHo/pho_" + PdID).ToString());
            divpho.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/FamilyHo/fho_" + PdID)))
        {
            lblfho.Text = File.ReadAllText(Server.MapPath("Files/FamilyHo/fho_" + PdID).ToString());
            divfho.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/Mind/mnd_" + PdID)))
        {
            lblMind.Text = File.ReadAllText(Server.MapPath("Files/Mind/mnd_" + PdID).ToString());
            divmnd.Style.Add("display", "block");
        }
        if (File.Exists(Server.MapPath("Files/AF/af_" + PdID)))
        {
            lblAF.Text = File.ReadAllText(Server.MapPath("Files/AF/af_" + PdID).ToString());
            divAF.Style.Add("display", "block");
        }

        if (File.Exists(Server.MapPath("Files/Thermal/thermal_" + PdID)))
        {
            lblThermal.Text = File.ReadAllText(Server.MapPath("Files/Thermal/thermal_" + PdID).ToString());
            divThermal.Style.Add("display", "block");
        }
    }

    public void BindPatientLastRemedyDetails()

    {
        strSQL = "Select top 1 FD.fdRemedy  From tblFollowUpDatails FD Left Join tblRemedyPeriodMaster RM On FD.fdRemedyPeriod = RM.Rem_Per_ID Where FD.fdPatientId = " + PdID + " Order By FD.fdId Desc ";

        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();

        while (Sqldr.Read())
        {
            DDL_Remedy.SelectedValue = Sqldr["fdRemedy"].ToString();
        }
        Sqldr.Close();
        SqlCmd.Dispose();
        con.Close();
    }

    public void BindPatientIPDRoundDetails()
    {
        if (InvId != 0)
        {
            strSQL = "Exec GetRoundDetailsPatientWise " + InvId + " , Edit";
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd = new SqlCommand(strSQL, con);
            Sqldr = SqlCmd.ExecuteReader();
            while (Sqldr.Read())
            {
                if (Sqldr["InvDate"] != DBNull.Value && Sqldr["InvDate"] != "")
                {
                    txt_InvDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["InvDate"].ToString()));
                }
                txt_Vtime.Text = string.Format("{0:hh:mm tt}", Sqldr["VisitTime"].ToString());
                DDL_Doctor.SelectedValue = Sqldr["InvDoctor"].ToString();
                DDL_Remedy.SelectedValue = Sqldr["RMNAME"].ToString();
                txt_FinalDign.Text = Sqldr["OLDComplain"].ToString();
                txtFreshComplaint.Text = Sqldr["Complaint"].ToString();
                txt_PulseRt.Text = Sqldr["Pulse"].ToString();
                txl_Bp.Text = Sqldr["SBP"].ToString();
                txt_Bp_Dia.Text = Sqldr["DBP"].ToString();
                txt_GlucTest.Text = Sqldr["Temp"].ToString();
                txt_RR.Text = Sqldr["RR"].ToString();
                txtLatestSymptom.Text = Sqldr["SuggInv"].ToString();
                DDL_INDC.SelectedValue = Sqldr["IncDecStat"].ToString();
            }
            Sqldr.Close();
            SqlCmd.Dispose();
            con.Close();
        }

        if (txtLatestSymptom.Text != "")
        {
            CheckedSavedInv();
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Calc", "CheckRangeForColor()", true);
       
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            int docID, RemId;
            string[] _IOD = txt_InvDate.Text.ToString().Split('/');
            string IOD = _IOD[2] + "/" + _IOD[1] + "/" + _IOD[0];

            if (DDL_Doctor.SelectedValue == "")
            {
                docID = 0;
            }
            else
            {
                docID = Convert.ToInt32(DDL_Doctor.SelectedValue);
            }

            if (DDL_Remedy.SelectedValue == "")
            {
                RemId = 0;
            }
            else
            {
                RemId = Convert.ToInt32(DDL_Remedy.SelectedValue);
            }

            string SuggInv = "";
            SuggInv = GetSelectedInv();

            this.UploadFile("BloodTest", fu_BloodTest);
            this.UploadFile("UrineTest", fu_UrineTest);
            this.UploadFile("Monitor", fu_Monitor);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "proc_InsInvestigationDetails";
            cmd.CommandType = CommandType.StoredProcedure;

            if (con.State == ConnectionState.Closed)
                con.Open();

            cmd.Connection = con;
            cmd.Parameters.AddWithValue("@InvID", InvId);
            cmd.Parameters.AddWithValue("@VisitTime", txt_Vtime.Text);
            cmd.Parameters.AddWithValue("@wmID", "");
            cmd.Parameters.AddWithValue("@CreatedBy", uid);
            cmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.AddWithValue("@BedNo", "");
            cmd.Parameters.AddWithValue("@PatientID", PdID);
            cmd.Parameters.AddWithValue("@Complaint", txtFreshComplaint.Text.Trim());
            cmd.Parameters.AddWithValue("@Remedy", txt_FinalDign.Text.Trim());
            cmd.Parameters.AddWithValue("@BpMin", txt_GlucTest.Text.Trim());
            cmd.Parameters.AddWithValue("@BpMax", txt_RR.Text.Trim());
            cmd.Parameters.AddWithValue("@LatestSymptom", SuggInv);
            cmd.Parameters.AddWithValue("@ReferenceToThirdPartyDoc", txtReferencetoaThirdPartyDoctor.Text.Trim());
            cmd.Parameters.AddWithValue("@ShiftToAnotherHospital", txtWecanshifttoanotherhospital.Text.Trim());
            cmd.Parameters.AddWithValue("@BloodTestFile", BloodTestFile);
            cmd.Parameters.AddWithValue("@UrineTestFile", UrineTestFile);
            cmd.Parameters.AddWithValue("@MonitorFile", Monitor);

            cmd.Parameters.AddWithValue("@InvDoctor", docID);
            cmd.Parameters.AddWithValue("@InvRemedyNew", RemId);
            cmd.Parameters.AddWithValue("@InvGlucometerTest", "");
            cmd.Parameters.AddWithValue("@InvPulseRate", txt_PulseRt.Text.ToString());
            cmd.Parameters.AddWithValue("@IncDecStat", DDL_INDC.SelectedValue);
            cmd.Parameters.AddWithValue("@BP", txl_Bp.Text.ToString() + "/" + txt_Bp_Dia.Text.ToString());
            cmd.Parameters.AddWithValue("@InvDate", IOD.ToString());

            cmd.ExecuteNonQuery();
            int error = (int)cmd.Parameters["@Error"].Value;

            if (error == 0)
            {
                Response.Redirect("AddIPDRound.aspx?msg=Round Details added successfully");
            }
        }
    }
   
    private void UploadFile(string type, FileUpload f1)
    {
        string filename = string.Empty;
        string strExt = string.Empty;
        string strPath = string.Empty;

        if (f1.FileName != "")
        {
            if (type == "BloodTest")
            {
                strExt = System.IO.Path.GetExtension(fu_BloodTest.FileName);
                filename = "BloodTest_" + PdID + strExt;
                BloodTestFile = filename;
            }

            if (type == "UrineTest")
            {
                filename = "UrineTest_" + PdID + strExt;
                UrineTestFile = filename;
            }

            if (type == "Monitor")
            {
                filename = "Monitor_" + PdID + strExt;
                Monitor = filename;
            }

            strPath = "InvestigationFiles/" + filename;
            f1.SaveAs(Server.MapPath(strPath));
        }
    }

    private void ClearFields()
    {
        txtFreshComplaint.Text = string.Empty;
        txtLatestSymptom.Text = string.Empty;
        txtMedicineDetails.Text = string.Empty;
        txtSys.Text = string.Empty;
        txtDis.Text = string.Empty;
        txtReferencetoaThirdPartyDoctor.Text = string.Empty;
        txtWecanshifttoanotherhospital.Text = string.Empty;
    }

    public void BindSuggestionInvDetails()
    {
        strSQL = "Proc_GetSuggestiveInvestigation";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Flag", "%");
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                DDL_SuggInv.DataSource = dt;
                DDL_SuggInv.DataTextField = "tmTestName";
                DDL_SuggInv.DataValueField = "tmId";
                DDL_SuggInv.DataBind();
            }
            con.Close();
            dr.Dispose();
        }
    }

    public string GetSelectedInv()
    {
        string suginv = "";

        List<String> SuggInvID_list = new List<string>();
       // List<String> CountryName_list = new List<string>();
        
        foreach (System.Web.UI.WebControls.ListItem item in DDL_SuggInv.Items)
        {
            if (item.Selected)
            {
                SuggInvID_list.Add(item.Value);
               // CountryName_list.Add(item.Text);
            }

            suginv = String.Join(",", SuggInvID_list.ToArray());
           // lblCountryName.Text = "Country Name: "+ String.Join(",", CountryName_list.ToArray());
        }

        return suginv;
    }

    public void CheckedSavedInv()
    {
      string[] split = txtLatestSymptom.Text.Split(',');
      foreach (string item in split)
      {
          Console.WriteLine(item);

          List<String> SuggInvID_list = new List<string>();
          foreach (System.Web.UI.WebControls.ListItem SuggInvitem in DDL_SuggInv.Items)
          {
              if (SuggInvitem.Value == item)
              {
                  SuggInvitem.Selected = true;
              }
          }
      }
    }
}