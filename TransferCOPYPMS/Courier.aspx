﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="Courier.aspx.cs" Inherits="Courier" Title="Courier Master" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
 <style type="text/css">
        .hide
        {
            display: none;
        }
        .visible
        {
            display: inline;
        }
        .tblinner
        {
            padding: 10px;
        }
        .InsreqPos
        {
            position: absolute;
            margin-top: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width:100%; background-color:#FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                        <h2>Courier Master</h2>
                        <div class="formmenu">
                            <div class="loginform">
                            <ul class="quicktabs_tabs quicktabs-style-excel">
                                <li class="qtab-Demo active first" id="li_1">
                                    <a class="qt_tab active" href="javascript:void(0)">Add Courier</a>
                                </li>
                                
                                <li class="qtab-HTML last" id="li_9">
                                    <a class="qt_tab active" href="ManageCourier.aspx">Manage Courier</a>
                                </li>
                                
                                </ul>
                            
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel" style="height:auto;">
                            <table style="width: 100%;padding-left:20px" cellspacing="0px" cellpadding="0px">
                                    <tr>
                                        <td colspan="2" style="text-align:center; color:Red;">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align:center; color:Red;">
                                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Courier Name*</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCourierName" runat="server" CssClass="field required" 
                                                MaxLength="50" style="float:left;"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valFname" runat="server" 
                                                ControlToValidate="txtCourierName" Display="Dynamic" 
                                                ErrorMessage="Courier Name is required."><span class="error">Courier Name is 
                                            required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            
                                            &nbsp;</td>
                                    </tr>
                                  
                                    <tr>
                                        <td>
                                            Contact Person*</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtContactPerson" runat="server" CssClass="field required" 
                                                MaxLength="50" style="float:left;"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valCharges" runat="server" 
                                                ControlToValidate="txtContactPerson" Display="Dynamic" 
                                                ErrorMessage="Contact Person is required."><span class="error">Contact Person is required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            Mobile No*</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtMobileNo" runat="server" CssClass="field required" 
                                                MaxLength="50" style="float:left;"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="txtMobileNo" Display="Dynamic" 
                                                ErrorMessage="Mobile No is required."><span class="error">Mobile No is 
                                            required.</span>
                                            </asp:RequiredFieldValidator>
                                             <asp:RegularExpressionValidator ID="recmobileno" runat="server" ControlToValidate="txtMobileNo" 
                                           CssClass="reqPos" ValidationExpression= 
                                           "^[0-9]{10}$">
                                           <span class="error">Please enter valid mobile no</span></asp:RegularExpressionValidator>
                                             
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            Office No*</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtOfficeNo" runat="server" CssClass="field required" 
                                                MaxLength="50" style="float:left;"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                ControlToValidate="txtOfficeNo" Display="Dynamic" 
                                                ErrorMessage="Office No is required."><span class="error">Office No is required.</span>
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            Email ID</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtEmailID" runat="server" CssClass="field required" 
                                                MaxLength="50" style="float:left;"></asp:TextBox>
                                             </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    
                                   <tr>
                                        <td>
                                            Remark</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRemark" runat="server" CssClass="field required" 
                                                MaxLength="50" style="float:left;"></asp:TextBox>
                                         </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" 
                                                onclick="btnSave_Click" style="margin-top: 2px;" Text="Save" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                
                            </div>    
                                
                                 
                                                                  
                                
                                <div class="clr"></div>
                                </div>               
                        </div>
                    </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
        </ContentTemplate>
    </asp:UpdatePanel> 

</asp:Content>

