﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class BalanceBill : System.Web.UI.Page
{
    genral gen = new genral();
    int ipdId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
            ipdId = Convert.ToInt32(Request.QueryString["id"]);
        FillData();
    }

    protected void FillData()
    {
        SqlCommand sqlcmd = new SqlCommand();
        sqlcmd.CommandText = "sp_GetBalanceBill";
        sqlcmd.CommandType = CommandType.StoredProcedure;
        sqlcmd.Parameters.AddWithValue("@ipdId", ipdId);
        DataTable dt = new DataTable();
        dt = gen.getDataTable(sqlcmd);
        if (dt.Rows.Count > 0)
        {
            rptBill.DataSource = dt;
            rptBill.DataBind();
        }
    }
}