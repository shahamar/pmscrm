﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class ConsultPatient : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    //string pdId = "0";
    string uid = "0";
    string fdId = "0";
    string Stat = "0";
    string Source = "MC";
    string OLID = "0";
    //added by nikita on 12th march 2015------------------------------
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-----------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["fdId"] != null)
            fdId = Request.QueryString["fdId"].ToString();

        if (Request.QueryString["Stat"] != null)
            Stat = Request.QueryString["Stat"].ToString();

        if (Request.QueryString["SC"] != null)
            Source = Request.QueryString["SC"].ToString();


        if (Request.QueryString["OLID"] != null)
            OLID = Request.QueryString["OLID"].ToString();



        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            txtDOC.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtNfollow.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtCourierDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtCOnCharges.Text = "1500.00";
            FillData();
            FillCourier();
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "Calc", "CalculateRemedyCharges()", true);
        }
    }
    protected void FillCourier()
    {
        strSQL = "Select * from tblPatientDetails where pdID = " + pdId;
        DataTable dt = gen.getDataTable(strSQL);
        if (dt.Rows.Count > 0)
        {
            txtCName.Text = dt.Rows[0]["pdFname"].ToString() + ' ' + dt.Rows[0]["pdMname"].ToString() + ' ' + dt.Rows[0]["pdLname"].ToString();
            txtCAddress.Text = dt.Rows[0]["pdAdd1"].ToString() + ' ' + dt.Rows[0]["pdAdd2"].ToString() + ' ' + dt.Rows[0]["pdAdd3"].ToString() + ' ' + dt.Rows[0]["pdAdd4"].ToString();
            txtCMobileNo.Text = dt.Rows[0]["pdMob"].ToString();
            txtcEmailID.Text = dt.Rows[0]["pdemail"].ToString();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (Stat == "NO")
        {

            string msg = "Consult Details Allready Saved.....!!!!!";
            ScriptManager.RegisterStartupScript(this, this.GetType(),
            "alert",
            "alert('" + msg + "');window.location ='ConsultPatient.aspx?Stat=NO&fdId=" + fdId + "';",
            true);
            return;

        }

        string finyear = gen.GetCurrentFinYear();
        string FollReceiptNo = "";
        string cmdReceiptNo = "";
        object p = gen.executeScalar("Select CONVERT(VARCHAR,ISNull(MAX(CAST(SubString(fdReceiptNo,1,len(fdReceiptNo)-4) As INT)),0)+1)+'" + finyear + "' From dbo.tblFollowUpDatails where fdCreatedDate > '2014-04-01 00:00:00.001'");
        FollReceiptNo = p.ToString();
        object o = gen.executeScalar("Select CONVERT(VARCHAR,ISNull(MAX(CAST(SubString(cmdReceiptNo,1,len(cmdReceiptNo)-4) As INT)),0)+1)+'" + finyear + "' From dbo.tblCourierMedicinsDetails where cmdCreatedDate > '2016-04-01 00:00:00.001'");
        cmdReceiptNo = o.ToString();

        if (fdId == "0")
            strSQL = "sp_InsConsultingDetails";
        else
            strSQL = "sp_UpdtConsultingDetails";


        string[] _doc = txtDOC.Text.ToString().Split('/');
        string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

        string[] _ndof = txtNfollow.Text.ToString().Split('/');
        string ndof = _ndof[2] + "/" + _ndof[1] + "/" + _ndof[0];

        string[] _CDate = txtCourierDate.Text.ToString().Split('/');
        string CDate = _CDate[2] + "/" + _CDate[1] + "/" + _CDate[0];

        int IsCourier = 0;
        int IsExtraDoses = 0;

        //string[] restr = ddRemedyPeriod.SelectedValue.Split('-');
        //string val = restr[0];
        //string val1 = restr[1];


        string val = "";
        string val1 = "";

        if (ddRemedyPeriod.SelectedValue != "")
        {
            string[] restr = ddRemedyPeriod.SelectedValue.Split('-');
             val = restr[0];
            val1 = restr[1];
        }


        string CChqdt;

        if (txtChqdt.Text != "")
        {
            string[] _CheckDt = txtChqdt.Text.ToString().Split('/');
            CChqdt = _CheckDt[2] + "/" + _CheckDt[1] + "/" + _CheckDt[0];
        }
        else
        {
            CChqdt = "";
        }
        if (chkIsCourier.Checked == true)
        {
            IsCourier = 1;
        }


        if (chk_ExtraDoses.Checked == true)
        {
            IsExtraDoses = 1;
        }
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@fdPatientId", pdId);
        SqlCmd.Parameters.AddWithValue("@fdDate", doc);
        SqlCmd.Parameters.AddWithValue("@fdRemedy", DDL_RemList.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@fdNextFollowupDate", ndof);
        SqlCmd.Parameters.AddWithValue("@fdRemark", txtRemark.Text);
        SqlCmd.Parameters.AddWithValue("@fdRemedyPeriod", val);
        SqlCmd.Parameters.AddWithValue("@fdCharges", txtCharges.Text);
        SqlCmd.Parameters.AddWithValue("@fdCreatedBy", uid);
        SqlCmd.Parameters.AddWithValue("@fdDoseType", ddDoseType.SelectedValue.ToString());
        SqlCmd.Parameters.AddWithValue("@fdConsultingCharges", txtCOnCharges.Text);
        SqlCmd.Parameters.AddWithValue("@fdIsCourier", IsCourier);
        SqlCmd.Parameters.AddWithValue("@PatObservation", DDL_PatObservation.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@Bank_Name", txtCardnm.Text.ToString().ToUpper());
        SqlCmd.Parameters.AddWithValue("@Bank_Branch", DDL_CardType.SelectedValue.ToString());
        SqlCmd.Parameters.AddWithValue("@Cheque_No", txtCardno.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@Cheque_Date", CChqdt);
        SqlCmd.Parameters.AddWithValue("@cmdName", txtCName.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdAddress", txtCAddress.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdMobileNo", txtCMobileNo.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdEmailId", txtcEmailID.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmId", DDCourier.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@cmdDate", CDate);
        SqlCmd.Parameters.AddWithValue("@cmdDocketNo", txtCDocketNo.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdReceiptNo", cmdReceiptNo);
        SqlCmd.Parameters.AddWithValue("@CourierPaymentMode", DDL_CourierPaymentMode.SelectedValue);

        if (txtCCharges.Text != "")
        {
            SqlCmd.Parameters.AddWithValue("@cmdCharges", txtCCharges.Text.ToString());
        }
        else
        {
            SqlCmd.Parameters.AddWithValue("@cmdCharges", 0);
        }

        if (fdId != "0")
        {
            SqlCmd.Parameters.AddWithValue("@fdId", fdId);
            SqlCmd.Parameters.AddWithValue("@followReceiptNo", FollReceiptNo);
        }
        else
        {
            SqlCmd.Parameters.Add("@fdId", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.AddWithValue("@followReceiptNo", FollReceiptNo);
        }

        SqlCmd.Parameters.AddWithValue("@fdAddEmgAmt", DDL_AddEmg.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@ExtraDosAmt", DDL_ExtranDos.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@Source", Source);
        SqlCmd.Parameters.AddWithValue("@OLID", OLID);    //online ID
        SqlCmd.Parameters.AddWithValue("@IsExtraDoses", IsExtraDoses);
        SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;

        if (con.State == ConnectionState.Closed)
            con.Open();

        SqlCmd.ExecuteNonQuery();
        int reslt;
        reslt = (int)SqlCmd.Parameters["@Error"].Value;
        if (reslt == 0)
        {
            if (chkIsCourier.Checked == true)
            {
                if (txtCMobileNo.Text.Trim() != "" && txtCDocketNo.Text.Trim() != "")
                {
                    String smsstring;
                    decimal TemplateId=1207160931676032697;
                    SqlCmd = new SqlCommand();
                    smsstring = "" + txtCName.Text.ToString() + " Your medicine has been dispatched from Aditya Homoeopathic Hospital on " + txtCourierDate.Text.ToString() + " via " + DDCourier.SelectedItem.Text + " courier with tracking (CN) number " + txtCDocketNo.Text + ". For any queries contact non tariff number 9923294959.. visit us at www.drnikam.com";
                    genral.GetResponse(txtCMobileNo.Text.Trim(), smsstring, TemplateId);
                    //genral.GetResponse("9821479720", smsstring);

                    SqlCmd.CommandText = "sp_insSmsTransaction";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Connection = con;
                    SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                    SqlCmd.Parameters.AddWithValue("@MobileNo", txtCMobileNo.Text.Trim());
                    SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                    SqlCmd.Parameters.AddWithValue("@SenderID", uid);
                    SqlCmd.Parameters.AddWithValue("@Pdid", pdId);
                    SqlCmd.Parameters.AddWithValue("@Sources", "Followup");
                    SqlCmd.ExecuteNonQuery();
                }
            }
            if (fdId == "0")
            {
                //Response.Redirect("ProfarmaOfCase.aspx", false);
                Response.Redirect("Home.aspx?MSG=Consult details saved successfully", false);
            }
            else
            {
                //Response.Redirect("FollowupHistory.aspx", false);
                Response.Redirect("Home.aspx?MSG=Consult details Updated successfully", false);
            }
        }

        SqlCmd.Dispose();
        con.Close();
    }

    public void FillData()
    {
        DDCourier.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select cmID,cmCourierName From tblCourierMaster", "cmID", "cmCourierName", DDCourier);

        ddRemedyPeriod.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select CAST(Rem_Per_ID  As Varchar(10)) + ' - '+ CAST(Rem_Per_InDays  As Varchar(10)) As REMID ,  Rem_Per_Description As REMDESC from tblRemedyPeriodMaster Where Rem_Per_IsDelete = 0 order by Rem_Per_InDays", "REMID", "REMDESC", ddRemedyPeriod);

        DDL_RemList.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select * From tblRemedyMaster where isDelete = 0 AND RMNAME != '0' ORDER BY REPLACE(RMNAME,' ','') ASC", "RMID", "RMNAME", DDL_RemList);


        if (fdId == "0")
        {
            strSQL = "Select * From tblPatientDetails Where pdID=" + pdId;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd = new SqlCommand(strSQL, con);
            Sqldr = SqlCmd.ExecuteReader();
            while (Sqldr.Read())
            {
                //txtDOC.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["pdDOC"].ToString()));
                if (Sqldr["pdIsRegistrationFeesPaid"].ToString() == "1")
                    txtCOnCharges.Text = "1500.00";
                else
                    txtCOnCharges.Text = "0.00";
            }
            Sqldr.Close();
            SqlCmd.Dispose();
        }
        else
        {
            strSQL = "Select FD.fdDate , FD.fdRemedy  , FD.fdDoseType , FD.fdCharges , FD.fdConsultingCharges , FD.fdNextFollowupDate , FD.fdRemark , FD.fdCasseteNo , FD.fdReconsulting , FD.fdIsReconsulting  , CAST(Rem_Per_ID  As Varchar(10)) + ' - '+ CAST(Rem_Per_InDays  As Varchar(10)) As fdRemedyPeriod , FD.PatObservation , FD.Payment_Mode , FD.Bank_Name , FD.Bank_Branch , FD.Cheque_No , FD.Cheque_Date , IsNull(fdAddEmgAmt,0) As fdAddEmgAmt , IsNull(ExtraDosAmt,0) As ExtraDosAmt From tblFollowUpDatails FD Left Join tblRemedyPeriodMaster RM On FD.fdRemedyPeriod = RM.Rem_Per_ID Where FD.fdId=" + fdId;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd = new SqlCommand(strSQL, con);
            Sqldr = SqlCmd.ExecuteReader();
            while (Sqldr.Read())
            {
                txtDOC.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["fdDate"].ToString()));
                DDL_RemList.SelectedValue = Sqldr["fdRemedy"].ToString();
                ddRemedyPeriod.SelectedValue = Sqldr["fdRemedyPeriod"].ToString();
                ddDoseType.SelectedValue = Sqldr["fdDoseType"].ToString();
                txtCharges.Text = Sqldr["fdCharges"].ToString();
                txtCOnCharges.Text = Sqldr["fdConsultingCharges"].ToString();
                DDL_PatObservation.SelectedValue = Sqldr["PatObservation"].ToString();
                if (Sqldr["fdNextFollowupDate"] != DBNull.Value)
                {
                    txtNfollow.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["fdNextFollowupDate"].ToString()));
                }
                txtRemark.Text = Sqldr["fdRemark"].ToString();

                if (Sqldr["Payment_Mode"] != DBNull.Value || Sqldr["Payment_Mode"].ToString() != "")
                {
                    DDL_Pay_Mode.SelectedValue = Sqldr["Payment_Mode"].ToString();
                }
                txtCardnm.Text = Sqldr["Bank_Name"].ToString();
                DDL_CardType.SelectedValue = Sqldr["Bank_Branch"].ToString();
                txtCardno.Text = Sqldr["Cheque_No"].ToString();
                if (Sqldr["Cheque_Date"].ToString() != "NULL" || Sqldr["Cheque_Date"].ToString() != "")
                {
                    txtChqdt.Text = Sqldr["Cheque_Date"] != DBNull.Value ? string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["Cheque_Date"].ToString())) : "";
                }

                if (DDL_Pay_Mode.SelectedIndex == 2)
                {
                    tr_Bank.Style.Add("display", "table-row");
                    tr_Cheque.Style.Add("display", "table-row");
                    tr_Bank1.Style.Add("display", "table-row");
                    tr_Cheque1.Style.Add("display", "table-row");
                }
                else
                {
                    tr_Bank.Style.Add("display", "none");
                    tr_Cheque.Style.Add("display", "none");
                    tr_Bank1.Style.Add("display", "none");
                    tr_Cheque1.Style.Add("display", "none");
                }

                DDL_AddEmg.SelectedValue = Sqldr["fdAddEmgAmt"].ToString();
                DDL_ExtranDos.SelectedValue = Sqldr["ExtraDosAmt"].ToString();


            }
            Sqldr.Close();
            SqlCmd.Dispose();
        }

        if (Source == "ACR")
        {
            chkIsCourier.Checked = true;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Calc", "OnModeChangeOnline();", true);

            if (OLID != "0")
            {

                FillCourierAddress();
            }
        }
    }

    protected void FillCourierAddress()
    {
        strSQL = "Select * from vwConsultingHistoryNewAks_OLT Where cmId= " + OLID + " ";

        DataTable dt = gen.getDataTable(strSQL);

        if (dt.Rows.Count > 0)
        {
            txtCName.Text = dt.Rows[0]["pdName"].ToString();
            txtCAddress.Text = dt.Rows[0]["PADD"].ToString();
            txtCMobileNo.Text = dt.Rows[0]["pdMob"].ToString();
            txtcEmailID.Text = dt.Rows[0]["pdemail"].ToString();
            txtCCharges.Text = dt.Rows[0]["RegistrationFees"].ToString();
            DDL_CourierPaymentMode.SelectedValue = "OnLine";
        }

    }

    protected void ddRemedyPeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddRemedyPeriod.SelectedValue == "7")
        {
            ddDoseType.SelectedValue = "1";
            txtCharges.Text = "0";
        }
        if(ddRemedyPeriod.SelectedValue == null)
        {
            ddDoseType.SelectedValue = "3";
        }
    }

    protected void DDL_Pay_Mode_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDL_Pay_Mode.SelectedIndex == 2)
        {
            tr_Bank.Style.Add("display", "table-row");
            tr_Cheque.Style.Add("display", "table-row");
            tr_Bank1.Style.Add("display", "table-row");
            tr_Cheque1.Style.Add("display", "table-row");
        }
        else
        {
            tr_Bank.Style.Add("display", "none");
            tr_Cheque.Style.Add("display", "none");
            tr_Bank1.Style.Add("display", "none");
            tr_Cheque1.Style.Add("display", "none");
        }
    }

}
