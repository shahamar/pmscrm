﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CourierBill.aspx.cs" Inherits="CourierBill" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>CONSULTING</title>

<style type="text/css">
.address
{
	font-family: Calibri;
	font-size: 18px;
	border-bottom: solid 2px;
	text-align: center;
}
.Date
{
	font-family: Calibri;
	font-size: 12px;
	font-weight: bold;
	border-bottom: solid 2px;
	text-align: right;
}
.details
{
	font-family: Calibri;
	font-size: 14px;
	border-bottom: solid 2px;
	text-align: left;
}
.col1
{
	text-align: left;
	width: 30%;
}
.col2
{
	text-align: center;
	width: 100%;
	font-weight: bold;
	margin-left: 100px;
}
.maintable
{
	border: solid 1px;
	border-radius: 5px;
	padding: 0px;
	font-family: Calibri;	
}
.reportname
{
	text-align: center;
	border-bottom: solid thin;
	font-weight: bold;
	font-family: Calibri;
	font-size:16px;
	padding-bottom:5px
}
.reportname1
{
	text-align: center;
	border-bottom: solid thin;
	font-weight: bold;
	font-family: Calibri;	
	padding-bottom:5px
}
.label{	border-bottom: solid thin;}
</style>
</head>

<body>
    <form id="form1" runat="server">   
    <div style="text-align: center; height: 80%">
        <asp:Panel ID="pnlBill" runat="server" Height="50%">
            <div>
                <center>
                    <asp:Repeater ID="rptBill" runat="server">
                        <ItemTemplate>
                            <table width="100%" cellpadding="0" cellspacing="5" border="0" class="maintable" style="font-size:14px">
                            	<tbody>
                                	<tr>
                                    	<td>
                                        	<table width="100%" cellpadding="0" cellspacing="5" border="0">    
                                                <tr>
                                                   <td align="center" colspan="2">
                                                   <table>
                                                   <tr>
                                                     <td>
                                                        <img src="images/Aditya_logo_final.png" width="65px" height="50px" />
                                                    </td>
                                                    <td style="padding-left:20px">
                                                        <b style="font-size:18px;"> Aditya Homoeopathic Hospital & Healing Center</b><br />
                                                        <span style="font-size:14px">DR. AMARSINHA D. NIKAM (M.D.Homoeopath)</span>
                                                    </td> 
                                                   </tr>
                                                   </table>
                                                   </td> 
                                                </tr> 
                                                <tr>
                                                	<td style="border-bottom:1px solid" colspan="2"></td>
                                                </tr> 
                                                
                                                <tr>
                                                    <td align="center" colspan="2">
                                                    	<span style="font-size:13px"><strong>Address :</strong>  
                                                    	Old Kate Pimple Road,Near Shivaji Chawk,Pimprigaon,Pune – 411017 
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                    	<span style="font-size:13px"><strong>Tel :</strong> 020-27412197 / 8806061061&nbsp;&nbsp;&nbsp;&nbsp;
                                                    	<strong>Website :</strong> www.drnikam.com</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                	<td style="border-bottom:1px solid" colspan="2"></td>
                                                </tr>  
                                               							
                                                <tr>
                                                    <td class="reportname" colspan="2">Courier Bill</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td class="reportname1" colspan="2">
                                                        <div style="float: left;">Receipt No. :
                                                            <%--<asp:Label ID="lblRcptNo" runat="server"></asp:Label>--%>
                                                            <%# Eval("pdReceiptNo")%>
                                                        </div>
                                                        <div style="float: right;">Date :
                                                            <%--<asp:Label ID="lblDate" runat="server"></asp:Label>--%>
                                                            <%# Eval("Date") %>
                                                        </div>
                                                    </td>
                                                </tr>                                                          
                                                                                
                                                <tr>
                                                    <td align="left" colspan="2">                                     
                                                        <table width="100%" cellpadding="0" cellspacing="5">
                                                            <tr>
                                                                <td align="left"><%--<strong>Name :</strong>--%>
                                                                Name :
                                                                    <%# Eval("Name")%>
                                                                </td>
                                                                <td align="center"><%--Age :<%# Eval("Name")%>--%>
                                                               
                                                                </td>
                                                                <td align="right"><%--Case Paper No. :<%# Eval("Name")%>--%>
                                                               
                                                                </td>
                                                            </tr>
                                                       </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2">                                     
                                                        <table width="100%" cellpadding="0" cellspacing="5">
                                                            <tr>
                                                                <td align="left">
                                                                   <%-- <strong>Diagnosis : </strong>--%>
                                                                   Diagnosis :
                                                                    <%# Eval("Diagnosis")%>
                                                                </td>
                                                            </tr>                                                             
                                                        </table>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td align="left" colspan="2">                                     
                                                        <table width="100%" cellpadding="0" cellspacing="0" class="maintable">
                                                            <tr>
                                                                <td align="center" style="padding:4px;border-bottom: 1px solid;border-right: 1px solid"><%--<strong>Medicine Period</strong>--%> Medicine Period</td>
                                                                <td align="center" style="padding:4px;border-bottom: 1px solid;border-right: 1px solid"><%--<strong>Dose Type</strong>--%>Dose Type</td>
                                                                <td align="center" style="padding:4px;border-bottom: 1px solid;"><%--<strong>Total Cost / Amount</strong>--%>Total Cost / Amount</td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="padding:4px;border-right: 1px solid">
                                                                    <%# Eval("RemedyPeriod")%>
                                                                </td>
                                                                <td align="center" style="padding:4px;border-right: 1px solid">
                                                                    <%# Eval("fdDoseType")%>
                                                                </td>
                                                                <td align="center" style="padding:4px;">
                                                                    <div>
                                                                        <%--<strong>Rs.</strong>--%> Rs.
                                                                      <%--  <strong><%# Eval("fdCharges")%></strong>--%> <%# Eval("cmdCharges")%>
                                                                    </div>                                      
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                               
                                                <tr>
                                                    <td align="left" colspan="2">
                                                      <table width="100%" cellpadding="0" cellspacing="5">
                                                          <tr>
                                                              <td align="left">
                                                                <%--<strong>Amount In Rupees :</strong>--%>
                                                                Amount In Rupees :
                                                                <%--<asp:Label ID="lblPaidAmt" runat="server" CssClass="label"></asp:Label>--%>
                                                                <%# Eval("Charges")%>
                                                              </td>
                                                           </tr>
                                                     </table>
                                                    </td>
                                                </tr>                                
                                                                
                                                <tr>
                                                    <td>&nbsp;  Prepared By :  &nbsp; 
                                                    <asp:Label ID="lblPreparedBy" runat="server" Visible="false"></asp:Label>
                                                      <%# Eval("UNAME")%>
                                                       <strong style="padding-left:90px ; font-weight:500;"><%# Eval("Payment_Mode")%></strong>
                                                    </td>
                                                    <td style="float:right; text-align:center">
                                                        <b>Aditya Homoeopathic Hospital <br /> & Healing Center</b>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                </center>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
