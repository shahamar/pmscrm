﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="TodaysPatientList.aspx.cs" Inherits="TodaysPatientList" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />

<style>
.AksDisplay {display:none;}
.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
 <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
    <tr>
        <td></td>
    </tr>
    <tr>
        <td>
            <div class="login-area margin_ten_right" style="width: 100%; text-align:center">
                <h2><asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h2>
              	<%--<div class="formmenu">
                    <div class="loginform">--%>
                       <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                          <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">
                              <tr>
                                <td>    
                              		<table style="width:100%;" cellspacing="5px" cellpadding="5px">
                              			<tr>
                              				<td width="17%" align="left"><strong>Case Type :&nbsp;</strong>
                              					<asp:DropDownList ID="ddlApType" runat="server" CssClass="textbox1">
                                                    <asp:ListItem Text="All" Value="%" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Complete" Value="Y"></asp:ListItem>
                                                    <asp:ListItem Text="Pending" Value="N"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td width="16%" align="left"><strong>From :&nbsp;</strong>
                                                <asp:TextBox ID="txtfrm" runat="server" CssClass="field textbox"></asp:TextBox>
                                                <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                    runat="server" Enabled="True" TargetControlID="txtfrm">
                                                </cc1:CalendarExtender>&nbsp;
                                                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"  />
                                            </td>
                                            <td width="15%" align="left"><strong>To :&nbsp;</strong>
                                                <asp:TextBox ID="txtto" runat="server" CssClass="field textbox"></asp:TextBox>
                                                <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                    Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                </cc1:CalendarExtender>&nbsp;
                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"  />
                            				</td>
                                            <td width="10%" align="left">
                                               <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left" OnClick="btnView_Click" />
                                            </td>
                                            <td width="42%"></td>
                          				</tr>
                          			</table>
                          		</td>
                          	  </tr>
                        	  <tr>
                             	<td id="gvcol" style="width:100%; text-align: center;background:#fff">       
                                    <%--OPD Case--%>                       
                                    <asp:GridView ID="GVD_PatientList"  Width="100%" runat="server" CssClass="mGrid" AutoGenerateColumns="false"  
                                    PageSize="20" OnRowCommand="GVD_PatientList_RowCommand"  AllowPaging="true" OnRowDataBound="GVD_PatientList_RowDataBound" 
                                    DataKeyNames="pdId" onrowcancelingedit="GVD_PatientList_RowCancelingEdit" OnPageIndexChanging="GVD_PatientList_PageIndexChanging">
                                    <Columns>                                    
                                     <asp:BoundField DataField="SNO" HeaderText="Sr No." />
                                     <asp:BoundField DataField="App_Time" HeaderText="Time" />
                                         <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                           <ItemTemplate>
                                                <asp:LinkButton ID="lnkNewCasepdName" runat="server" Text='<%# Eval("pdName")%>' CommandArgument='<%# Eval("pdId") + ";" +Eval("fdId") %>'
                                                    CommandName="CaseFol" Style="text-decoration: none; color: Blue; font-weight: normal;"></asp:LinkButton>
                                            </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:BoundField DataField="pdCasePaperNo" HeaderText="Case Paper No." />
                                         <asp:BoundField DataField="pdOccupation" HeaderText="Occupation" />
                                         <asp:BoundField DataField="City" HeaderText="City" />
                                         <asp:BoundField DataField="LastFollowUpDate" HeaderText="Last Followup Date" />
                                         <asp:BoundField DataField="ContactNo" HeaderText="Contact No." />
                                          <asp:TemplateField HeaderText="Patient Type" Visible="false">
                                              <ItemTemplate>
                                                <asp:Label ID="lblPatType" Text='<%# Eval("PatType")%>' runat="server" Font-Bold="true" Font-Size="Large"></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>                                                                                
                                          <asp:TemplateField HeaderText="KCO" Visible="false">
                                               <ItemTemplate>
                                               <asp:Label ID="lblkco" runat="server"></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Stat" Visible="false">
                                              <ItemTemplate>
                                              <asp:Label ID="lblCaseStat" runat="server" Text='<%# Eval("CaseStat")%>' ></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>                                                                                     
                                          <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center" Visible="false">
                                              <ItemTemplate>
                                                    <asp:ImageButton ID="btnOldPatAppCancel" runat="server" CommandName="Cancel" 
                                                    CommandArgument='<%# Eval("adid") %>' Visible="false" ImageUrl="~/images/cancel.ico" ToolTip="Cancel Appoinmet"/>
                                          </ItemTemplate>    
                                          </asp:TemplateField>
                                     </Columns>
                                    </asp:GridView>

                                     <%--New Case --%>
                                    <asp:GridView ID="GVD_PatientList1"  Width="100%" runat="server" CssClass="mGrid" AutoGenerateColumns="false"  
                                    PageSize="20" OnRowCommand="GVD_PatientList_RowCommand"  AllowPaging="true" OnRowDataBound="GVD_PatientList_RowDataBound" 
                                    DataKeyNames="pdId" onrowcancelingedit="GVD_PatientList_RowCancelingEdit" OnPageIndexChanging="GVD_PatientList_PageIndexChanging">
                                    <Columns>                                        
                                         <asp:BoundField DataField="SNO" HeaderText="Sr No." />
					 <asp:BoundField DataField="App_Time" HeaderText="Time"/>
                                         <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                           <ItemTemplate>
                                                <asp:LinkButton ID="lnkNewCasepdName" runat="server" Text='<%# Eval("pdName")%>' CommandArgument='<%# Eval("pdId") + ";" +Eval("fdId") %>'
                                                    CommandName="CaseFol" Style="text-decoration: none; color: Blue; font-weight: normal;"></asp:LinkButton>
                                            </ItemTemplate>
                                         </asp:TemplateField>
                                          <asp:BoundField DataField="pdAge" HeaderText="Age" />
                                          <asp:BoundField DataField="pdSex" HeaderText="Gender" />   
                                          <asp:BoundField DataField="pdCasePaperNo" HeaderText="Case Paper No." />                                                                                 
                                          <asp:BoundField DataField="ConsDate" HeaderText="Consulting Date" />
                                          <asp:BoundField DataField="DocName" HeaderText="Doctor Name" />
                                          <asp:BoundField DataField="pdOccupation" HeaderText="Occupation" />
                                         <asp:BoundField DataField="City" HeaderText="City" />
                                          <asp:TemplateField HeaderText="Patient Type" Visible="false">
                                              <ItemTemplate>
                                                <asp:Label ID="lblPatType" Text='<%# Eval("PatType")%>' runat="server" Font-Bold="true" Font-Size="Large"></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>                                                                                
                                          <asp:TemplateField HeaderText="KCO" Visible="false">
                                               <ItemTemplate>
                                               <asp:Label ID="lblkco" runat="server"></asp:Label>
                                               </ItemTemplate>
                                           </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Stat" Visible="false">
                                              <ItemTemplate>
                                              <asp:Label ID="lblCaseStat" runat="server" Text='<%# Eval("CaseStat")%>' ></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>                                                                                     
                                          <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center" Visible="false">
                                              <ItemTemplate>
                                                    <asp:ImageButton ID="btnOldPatAppCancel" runat="server" CommandName="Cancel" 
                                                    CommandArgument='<%# Eval("adid") %>' Visible="false" ImageUrl="~/images/cancel.ico" ToolTip="Cancel Appoinmet"/>
                                          </ItemTemplate>    
                                          </asp:TemplateField>
                                     </Columns>
                                    </asp:GridView>
                                </td>                           
                        	  </tr>                       
                              <tr>
                                <td style="text-align: center;">
                                    <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Red;">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></font>
                                </td>
                              </tr>
                    	  </table>
                        </div>
                       <%-- <div class="clr">
                        </div>
                    </div>
                </div>--%>
            </div>
        </td>
    </tr>
    
</table>
</asp:Content>

