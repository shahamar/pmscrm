﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class FollowupHistory : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "",msg;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    //string pdId = "0";
    string uid = "0";
    int cnt = 0;
    //added by nikita on 12th march 2015------------------------------
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-----------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 30";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            FillData();
        }

    }

    public void FillData()
    {
        strSQL = "Select *,Case fdFlag When 'F' Then 'inline' ELSE 'none' END 'DEL' from vwConsultingHistoryNewAks Where fdPatientId=" + pdId + "Order by fdDate desc";
        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        if(msg == "1")
           // GRV1.Columns[6].Visible = false;
        //else
          //  GRV1.Columns[6].Visible = true;
        FillRemedyDetails();
    }

    protected void GRV1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "edt")
        {
            string arg = e.CommandArgument.ToString();
            string[] _arg = arg.Split('-');

            string fdid = _arg[0].ToString();
            string flag = _arg[1].ToString();

            if (flag == "C")
                Response.Redirect("ConsultPatient.aspx?fdId=" + fdid);
            else
                Response.Redirect("FollowupDetail.aspx?fdId=" + fdid);
        }

        if (e.CommandName == "case")
        {
            string arg = e.CommandArgument.ToString();
            Session["fdId"] = arg;
            Response.Redirect("ProfarmaOfCase.aspx");
        }

        if (e.CommandName == "del")
        {
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeleteFollowupDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@fdId", e.CommandArgument);
            SqlCmd.Parameters.AddWithValue("@deletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd.ExecuteNonQuery();
            con.Close();
            SqlCmd.Dispose();
            Response.Redirect("FollowupHistory.aspx");
            FillData();
        }
    }
    protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            cnt = cnt + 1;
            if (cnt == 1)
            {
               // e.Row.BackColor = System.Drawing.Color.FromName("#FDFA00");
            }

            Label lblISRec = (Label)e.Row.FindControl("lblISRec");
            if (lblISRec.Text == "True")
            {
               e.Row.BackColor = System.Drawing.Color.FromName("#FDFA00");
            }
        }
    }

    public void FillRemedyDetails()
    {
        strSQL = "Select RMNAME As irRemedy , irStartDate  From tblIpdRemedy IPR Left Join tblRemedyMaster RM On RM.RMID = IPR.irRemedy Where iripdId IN (Select ipdId From tblIpdMaster Where ipdpdId=" + pdId + ")";
        //strSQL += " AND wId = (SELECT MAX(wId) FROM dbo.tblPatientsWardDetails Where wipdId=" + ipdid + ")";
        DataTable _dt = gen.getDataTable(strSQL);
        GRV3.DataSource = _dt;
        GRV3.DataBind();
        if (_dt.Rows.Count >= 1)
        {
            pnlremedy.Visible = true;
        }
        else
        {
            pnlremedy.Visible = false;
        }
        _dt.Dispose();
    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        FillData();
    }

}
