﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class OccupiedBed : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string uid;
    int Page_no = 0, Page_size = 20;
    string flw = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx", false);

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 27";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------


        if (Request.QueryString["Flow"] != null)
        {
            flw = Request.QueryString["Flow"].ToString();
        }

        if (flw == "Adm")
        {
            li_9.Visible = false;
        }
        else
        {
            li_9.Visible = true;
        }

        if (!IsPostBack)
        {
            BindGVR();
        }
    }

    public void BindGVR()
    {
        try
        {
            //strSQL = "DECLARE @TempOccupiedBed TABLE(BedNo int,WID int,WNO varchar(30),WIPDID int,InDate smalldatetime)insert into @TempOccupiedBed select distinct wBedNo,max(wid),wNo,wipdId,wInDate from tblPatientsWardDetails where wOutDate is  null group by wNo,wBedNo,wipdId,wInDate select BedNo,WID,WNO,InDate, pdInitial +'  '+pdFname +'  '+pdMname +'  '+ pdLname AS 'PName',WIPDID from @TempOccupiedBed t  inner join tblIpdMaster i on t.WIPDID =i.ipdid inner join tblPatientDetails p on i.ipdpdId  =p.pdID  where 1=1 AND i.IsDelete=0 AND IsCancel = 0";

            strSQL = "DECLARE @TempOccupiedBed TABLE(BedNo int,WID int,WNO varchar(30),WIPDID int,InDate smalldatetime)insert into @TempOccupiedBed select distinct bm.bmNoOfBads,max(pwd.wid),wm.wmWardNo,pwd.wipdId,pwd.wInDate from tblWardMaster wm Inner Join tblPatientsWardDetails pwd On cast(wm.wmID As Varchar(50)) = pwd.wNo And wm.wmWardTypeId = pwd.wType Inner Join tblBadMaster bm On bm.wmID = wm.wmID And cast(bm.bmID As Varchar(50)) = pwd.wBedNo Inner Join tblIpdMaster IM On IM.ipdId = pwd.wipdId Where IM.ipdDischargeDate IS NULL AND IM.IsDelete = 0 And IM.ipdIsDischarge = 0 And pwd.wOutDate is Null group by wm.wmWardNo,bm.bmNoOfBads,pwd.wipdId,pwd.wInDate select BedNo,WID,WNO,InDate, pdInitial +'  '+pdFname +'  '+pdMname +'  '+ pdLname AS 'PName',WIPDID from @TempOccupiedBed t  inner join tblIpdMaster i on t.WIPDID =i.ipdid inner join tblPatientDetails p on i.ipdpdId  =p.pdID  where 1=1 AND i.IsDelete=0 AND IsCancel = 0";
           

            if (txtName.Text.ToString() != "")
                strSQL += " AND pdFname+' '+ pdMname +' '+ pdLname like '%" + txtName.Text.ToString() + "%'";

            if (txtward.Text != "")
                strSQL += " AND WNO Like '%" + txtward.Text + "%'";

            if (txtBed.Text != "")
                strSQL += " AND BedNo Like '%" + txtBed.Text + "%'";

            strSQL += " order by WNO,BedNo asc";


            DataTable dt = gen.getDataTable(strSQL);
            if (dt.Rows.Count > 0)
            {
                GRV1.DataSource = dt;
                GRV1.DataBind();
                createpaging(dt.Rows.Count, GRV1.PageCount);
            }
            else
            {
                GRV1.EmptyDataText.ToString();
                GRV1.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void lnkfilter_Click(object sender, EventArgs e)
    {
        BindGVR();
    }
    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGVR();
    }

    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;
    }
}
