﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class IPDPaper : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string uid = "0";
    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");
        
        if (!IsPostBack)
        {
            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            else
                Response.Redirect("IPDView.aspx", false);

            FillData();
        }        
    }

    protected void FillData()
    {
        try
        {
            strSQL = "proc_GetIPDPaperDetails";
            SqlCmd = new SqlCommand(strSQL,con);
            //SqlCmd.CommandText = "proc_GetIPDPaperDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
            dapt = new SqlDataAdapter(SqlCmd);
            DataTable dt = new DataTable();
            dapt.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                lblCasePaperNo.Text = dt.Rows[0]["pdCasePaperNo"].ToString();
                lblDate.Text = dt.Rows[0]["pdDOC"].ToString();
                lblRemedy.Text = dt.Rows[0]["irRemedy"].ToString();
                lblFName.Text = dt.Rows[0]["fname"].ToString();
                lblMName.Text = dt.Rows[0]["pdMname"].ToString();
                lblLName.Text = dt.Rows[0]["pdLname"].ToString();
                lblAge.Text = dt.Rows[0]["pdAge"].ToString();
                lblSex.Text = dt.Rows[0]["pdSex"].ToString();
                lblAddress.Text = dt.Rows[0]["fullAddress"].ToString();
                lblPhoneNo.Text = dt.Rows[0]["pdTele"].ToString();
                lblMobileNo.Text = dt.Rows[0]["pdMob"].ToString();
               // lblDiagnosis.Text = dt.Rows[0]["ReconsultingDiagnosis"].ToString() == "" ? dt.Rows[0]["ipdProvisionalDiagnosis"].ToString() : dt.Rows[0]["ReconsultingDiagnosis"].ToString();
                //lblFinalDiag.Text = dt.Rows[0]["ipdFinalDiagnosis"].ToString();
                lblAdmissionDate.Text = dt.Rows[0]["ipdAdmissionDate"].ToString();
                lblAdmissionTime.Text = dt.Rows[0]["ipdAdmissionTime"].ToString();
                lblDepositAmt.Text = dt.Rows[0]["ipdDepositAmount"].ToString();
                lblIpdNo.Text = dt.Rows[0]["IpdNo"].ToString();
                lblwNo.Text = dt.Rows[0]["wNo"].ToString();
                lblWtype.Text = dt.Rows[0]["wType"].ToString();
                lblBedNo.Text = dt.Rows[0]["wBedNo"].ToString();
                lblWt.Text = dt.Rows[0]["pdWeight"].ToString();
                lblCity.Text = dt.Rows[0]["City"].ToString();
                lblState.Text = dt.Rows[0]["State"].ToString();
                lblDepDt.Text = dt.Rows[0]["ipdAdmissionDate"].ToString();
                lblPreparedBy.Text = dt.Rows[0]["UNAME"].ToString();

                if (File.Exists(Server.MapPath("Files/KCO/kco_" + dt.Rows[0]["ipdpdId"].ToString())))
                {
                    lblDiagnosis.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + dt.Rows[0]["ipdpdId"].ToString()).ToString());
                }

               
                //lblDiagnosis.Text = lblDiagnosis.Text.Replace(Environment.NewLine, "Ashok");
                lblDiagnosis.Text = lblDiagnosis.Text.Replace("<br />", " ");
                //string Str = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + dt.Rows[0]["ipdpdId"].ToString()).ToString());
                //string StrNew = Str.Replace("\n", "\r");

            }
        }
        catch (Exception ex)
        {

        }
    }
}