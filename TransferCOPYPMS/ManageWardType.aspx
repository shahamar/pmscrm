﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="ManageWardType.aspx.cs" Inherits="ManageWardType" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Ward Type Master</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo first" id="li_1"><a class="qt_tab active" href="WardTypeMaster.aspx">
                                            Ward Type Master</a> </li>
                                        <li class="qtab-HTML active last" id="li_9"><a class="qt_tab active" href="javascript:void(0)">
                                            Manage Ward Type Master</a> </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto;">
                                        <div style="padding: 15px; height: auto;">
                                            <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                <tr>
                                                    <td colspan="3" style="text-align: center; color: Red;">
                                                        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblGridHeader" runat="server" ForeColor="Red" style="float:right;"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 50%">
                                                            <tr>
                                                                <th class="head1">
                                                                    Ward Type
                                                                </th>
                                                               
                                                                <th class="head1">
                                                                   Order No.
                                                                </th>

                                                                <th class="head6">
                                                                    Action
                                                                </th>
                                                            </tr>
                                                            
                                                        </table>
                                                    </td>
                                                    <tr>
                                                        <td id="gvcol" colspan="2">
                                                            <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="10" ShowHeader="false" Width="50%"
                                                                OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%# Eval("WardTypeName")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                   
                                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%# Eval("OrderNo")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("WardTypeID") %>'
                                                                                CommandName="edt" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                            <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("WardTypeID") %>'
                                                                                CommandName="del" ImageUrl="Images/delete.ico" OnClientClick="return confirm('Are you sure you want to delete this?');"
                                                                                ToolTip="Delete" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                       
                                                    </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

