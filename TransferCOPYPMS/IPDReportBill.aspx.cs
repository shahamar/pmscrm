﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class IPDReportBill : System.Web.UI.Page 
{
    string strSQL = "";
    genral gen = new genral();
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    //string IPDID = "";
    string uid = "0";
    //added by nikita on 12th march 2015------------------------------
    public int IPDID
    {
        get
        {
            if (ViewState["IpdID"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["IpdID"].ToString());
        }
    }
    //-------------------------------------------------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }

        if (!IsPostBack)
        {
            if (Session["IpdID"] != null)
                ViewState["IpdID"] = Session["IpdID"].ToString();

            FillData();
        }
    }
    protected void FillData()
    {

        //strSQL = "select CONVERT(VARCHAR(10), getdate(), 105) AS Date,[dbo].[fnNumToWords](sum(CASE wType WHEN 1 THEN (CASE WHEN wInDate = tIpm.ipdAdmissionDate THEN 250*(DATEDIFF(Day, wInDate,wOutDate)+1)ELSE 250*(DATEDIFF(Day, wInDate,wOutDate)) END)WHEN 2 THEN (CASE WHEN wInDate = tIpm.ipdAdmissionDate THEN 300*(DATEDIFF(Day, wInDate,wOutDate)+1) ELSE 300*(DATEDIFF(Day, wInDate,wOutDate)) END) END) + ISNULL(sum(CASE rwType WHEN 1 THEN (CASE WHEN rwInDate = tIpm.ipdAdmissionDate THEN 200*(DATEDIFF(Day, rwInDate,rwOutDate)+1) ELSE 200*(DATEDIFF(Day, rwInDate,rwOutDate)) END) WHEN 2 THEN (CASE WHEN rwInDate = tIpm.ipdAdmissionDate THEN 250*(DATEDIFF(Day, rwInDate,rwOutDate)+1) ELSE 250*(DATEDIFF(Day, rwInDate,rwOutDate)) END)END),0)+ isnull((SELECT SUM(itdcharges) FROM tblIpdTestDetails tItd WHERE  tIpm.ipdId=tItd.itdipdId AND tItd.itdtmId=5 group by itdipdId) ,0),'Y') 'wCharges',tIpm.ipdId,tpd.pdInitial + ' ' + tpd.pdFname + ' ' + tpd.pdMname + ' ' + tpd.pdLname 'Name',sum(CASE wType WHEN 1 THEN (CASE WHEN wInDate = tIpm.ipdAdmissionDate THEN 250*(DATEDIFF(Day, wInDate,wOutDate)+1)ELSE 250*(DATEDIFF(Day, wInDate,wOutDate)) END)WHEN 2 THEN (CASE WHEN wInDate = tIpm.ipdAdmissionDate THEN 300*(DATEDIFF(Day, wInDate,wOutDate)+1) ELSE 300*(DATEDIFF(Day, wInDate,wOutDate)) END) END) +ISNULL(sum(CASE rwType WHEN 1 THEN (CASE WHEN rwInDate = tIpm.ipdAdmissionDate THEN 200*(DATEDIFF(Day, rwInDate,rwOutDate)+1) ELSE 200*(DATEDIFF(Day, rwInDate,rwOutDate)) END) WHEN 2 THEN (CASE WHEN rwInDate = tIpm.ipdAdmissionDate THEN 250*(DATEDIFF(Day, rwInDate,rwOutDate)+1) ELSE 250*(DATEDIFF(Day, rwInDate,rwOutDate)) END)END),0)+ isnull((SELECT SUM(itdcharges) FROM tblIpdTestDetails tItd WHERE  tIpm.ipdId=tItd.itdipdId AND tItd.itdtmId=5 group by itdipdId) ,0) 'Charge',b.bmNo  ";
        //strSQL += " FROM tblIpdMaster tIpm  INNER JOIN tblPatientDetails tPd ON tIpm.ipdpdId =tPd.pdID inner JOIN  tblPatientsWardDetails tPwd ON tPwd.wipdId=tIpm.ipdId inner join tblBillMaster b on b.bmipdId =tIpm.ipdId  left join tblRelativeWardDetails r on r.ipdId =tIpm.ipdId ";
        //strSQL += " WHERE  tIpm.ipdDischargeDate IS NOT NULL and tIpm.ipdId =" + IPDID + " group by tIpm.ipdId,tIpm.ipdId,tpd.pdInitial, tpd.pdFname,tpd.pdMname,tpd.pdLname,b.bmNo";

        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "proc_GetIPDReportBill";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = conn;
        conn.Open();
        cmd.Parameters.AddWithValue("@ipdid", IPDID);

        DataTable dt = gen.getDataTable(cmd);
        rptBill.DataSource = dt;
        rptBill.DataBind();
        
    }

    #region AmountInWords
    public string AmountInWords(decimal Num)
    {
        string returnValue;
        //I have created this function for converting amount in indian rupees (INR).
        //You can manipulate as you wish like decimal setting, Doller (any currency) Prefix.


        string strNum;
        string strNumDec;
        string StrWord;
        strNum = Num.ToString();


        if (strNum.IndexOf(".") + 1 != 0)
        {
            strNumDec = strNum.Substring(strNum.IndexOf(".") + 2 - 1);


            if (strNumDec.Length == 1)
            {
                strNumDec = strNumDec + "0";
            }
            if (strNumDec.Length > 2)
            {
                strNumDec = strNumDec.Substring(0, 2);
            }
            strNum = strNum.Substring(0, strNum.IndexOf(".") + 0);
            StrWord = ((double.Parse(strNum) == 1) ? " Rupee " : " Rupees ") + NumToWord((decimal)(double.Parse(strNum))) + ((double.Parse(strNumDec) > 0) ? (" and Paise" + cWord3((decimal)(double.Parse(strNumDec)))) : "");
        }
        else
        {
            StrWord = ((double.Parse(strNum) == 1) ? " Rupee " : " Rupees ") + NumToWord((decimal)(double.Parse(strNum)));
        }
        returnValue = StrWord + " Only";
        return returnValue;
    }
    public static string NumToWord(decimal Num)
    {
        string returnValue;


        //I divided this function in two part.
        //1. Three or less digit number.
        //2. more than three digit number.
        string strNum;
        string StrWord;
        strNum = Num.ToString();


        if (strNum.Length <= 3)
        {
            StrWord = cWord3((decimal)(double.Parse(strNum)));
        }
        else
        {
            StrWord = cWordG3((decimal)(double.Parse(strNum.Substring(0, strNum.Length - 3)))) + " " + cWord3((decimal)(double.Parse(strNum.Substring(strNum.Length - 2 - 1))));
        }
        returnValue = StrWord;
        return returnValue;
    }
    public static string cWordG3(decimal Num)
    {
        string returnValue;
        //2. more than three digit number.
        string strNum = "";
        string StrWord = "";
        string readNum = "";
        strNum = Num.ToString();
        if (strNum.Length % 2 != 0)
        {
            readNum = System.Convert.ToString(double.Parse(strNum.Substring(0, 1)));
            if (readNum != "0")
            {
                StrWord = retWord(decimal.Parse(readNum));
                readNum = System.Convert.ToString(double.Parse("1" + strReplicate("0", strNum.Length - 1) + "000"));
                StrWord = StrWord + " " + retWord(decimal.Parse(readNum));
            }
            strNum = strNum.Substring(1);
        }
        while (!System.Convert.ToBoolean(strNum.Length == 0))
        {
            readNum = System.Convert.ToString(double.Parse(strNum.Substring(0, 2)));
            if (readNum != "0")
            {
                StrWord = StrWord + " " + cWord3(decimal.Parse(readNum));
                readNum = System.Convert.ToString(double.Parse("1" + strReplicate("0", strNum.Length - 2) + "000"));
                StrWord = StrWord + " " + retWord(decimal.Parse(readNum));
            }
            strNum = strNum.Substring(2);
        }
        returnValue = StrWord;
        return returnValue;
    }
    public static string cWord3(decimal Num)
    {
        string returnValue;
        //1. Three or less digit number.
        string strNum = "";
        string StrWord = "";
        string readNum = "";
        if (Num < 0)
        {
            Num = Num * -1;
        }
        strNum = Num.ToString();

        if (strNum.Length == 3)
        {
            readNum = System.Convert.ToString(double.Parse(strNum.Substring(0, 1)));
            StrWord = retWord(decimal.Parse(readNum)) + " Hundred";
            strNum = strNum.Substring(1, strNum.Length - 1);
        }


        if (strNum.Length <= 2)
        {
            if (double.Parse(strNum) >= 0 && double.Parse(strNum) <= 20)
            {
                StrWord = StrWord + " " + retWord((decimal)(double.Parse(strNum)));
            }
            else
            {
                StrWord = StrWord + " " + retWord((decimal)(System.Convert.ToDouble(strNum.Substring(0, 1) + "0"))) + " " + retWord((decimal)(double.Parse(strNum.Substring(1, 1))));
            }
        }
        strNum = Num.ToString();
        returnValue = StrWord;
        return returnValue;
    }
    public static string retWord(decimal Num)
    {
        string returnValue;
        //This two dimensional array store the primary word convertion of number.
        returnValue = "";
        object[,] ArrWordList = new object[,] { { 0, "" }, { 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" }, { 6, "Six" }, { 7, "Seven" }, { 8, "Eight" }, { 9, "Nine" }, { 10, "Ten" }, { 11, "Eleven" }, { 12, "Twelve" }, { 13, "Thirteen" }, { 14, "Fourteen" }, { 15, "Fifteen" }, { 16, "Sixteen" }, { 17, "Seventeen" }, { 18, "Eighteen" }, { 19, "Nineteen" }, { 20, "Twenty" }, { 30, "Thirty" }, { 40, "Forty" }, { 50, "Fifty" }, { 60, "Sixty" }, { 70, "Seventy" }, { 80, "Eighty" }, { 90, "Ninety" }, { 100, "Hundred" }, { 1000, "Thousand" }, { 100000, "Lakh" }, { 10000000, "Crore" } };


        int i;
        for (i = 0; i <= (ArrWordList.Length - 1); i++)
        {
            if (Num == System.Convert.ToDecimal(ArrWordList[i, 0]))
            {
                returnValue = (string)(ArrWordList[i, 1]);
                break;
            }
        }
        return returnValue;
    }
    public static string strReplicate(string str, int intD)
    {
        string returnValue;
        //This fucntion padded "0" after the number to evaluate hundred, thousand and on....
        //using this function you can replicate any Charactor with given string.
        int i;
        returnValue = "";
        for (i = 1; i <= intD; i++)
        {
            returnValue = returnValue + str;
        }
        return returnValue;
    }
    #endregion

}
