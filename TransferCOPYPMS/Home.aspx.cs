﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public partial class Home : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    string StrSQL, strSQLAks, strSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string Uroll = "0";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (Session["urole"] != null)
            Uroll = Session["urole"].ToString();

        if (Request.QueryString["flag"] != null)
        {
            string flag = Request.QueryString["flag"].ToString();
            if (flag == "1")
                lblmsg.Text = "Please <a href='SearchPatient.aspx' class='example7'>Click Here</a> to find patients";

        }
        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        if (Request.QueryString["status"] != null)
        {
            int status = Convert.ToInt32(Request.QueryString["status"]);
            if (status == 1)
                lblMessage.Text = "You are not authorized to view this form";
        }

        if (Request.QueryString["MSG"] != null && Request.QueryString["MSG"] !="")
        {
                string msg = Request.QueryString["MSG"].ToString();
                string script = "alert('" + msg + "');";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);

                ScriptManager.RegisterStartupScript(this, this.GetType(),
                "alert",
                "alert('" + msg + "');window.location ='Home.aspx';",
                true);

               // ScriptManager.RegisterStartupScript(this, this.GetType(), "onclick", "javascript:window.open( 'SomePage.aspx','_blank','height=600px,width=600px,scrollbars=1');", true);
             
                //var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
                //nameValues.Set("MSG", "");
        }

        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            BindBedOccStatusGrid();
            NEWPAT_APP_CONF();
            OLDPAT_APP_CONF();
            NEWPAT_APP_NOT_CONF();
            OLDPAT_APP_NOT_CONF();
            NEWPAT_APP_CONF_NON();
            GetTodaysAppCount();
            BindBedOccGrid();
            //OLDPAT_Discharge();

            //if (Uroll == "1")
            //{
            // GetBirthDayList();
            //}

        }
       
    }

    private void BindBedOccStatusGrid()
    {
        StrSQL = "Proc_GetBedOccupancystatus";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_BedOccStatus.DataSource = dt;
                GVD_BedOccStatus.DataBind();
            }
            else
            {
                GVD_BedOccStatus.DataSource = null;
                GVD_BedOccStatus.DataBind();
            }
            conn.Close();
            dr.Dispose();
          
        }
    }

    private void NEWPAT_APP_CONF()
    {
        StrSQL = "GetTodayAppointmentForDashboard";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Flag", "NEWPAT_APP_CONF");
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_NewPat.DataSource = dt;
                GVD_NewPat.DataBind();
            }
            else
            {
                GVD_NewPat.DataSource = null;
                GVD_NewPat.DataBind();
            }
            conn.Close();
            dr.Dispose();
        }
    }

    private void OLDPAT_APP_CONF()
    {
        try
        {

            StrSQL = "Select ROW_NUMBER() over(order By Ad.adid) As SNO,  PD.pdInitial +' '+ PD.pdFname+' '+ PD.pdMname +' '+ PD.pdLname As pdName , isNull(PD.pdPatientICardNo , '') As pdPatientICardNo";
            StrSQL += ", PD.pdMob , Ad.adid  , PD.pdId , IsNull(fdId,0) As fdId , AD.App_Time As App_Time,ISNULL(U.uShortName,'') as uShortName";
            StrSQL += ", CASE ISNULL(CAST(fdCharges AS VARCHAR),'') WHEN ''  THEN '<img src=''images/print1.png'' />' ELSE '<a href=''OPDBill.aspx?id='+CAST(fdId AS VARCHAR)+'&IsReconsulting='+ CAST(ISNULL(fdIsReconsulting,0) AS VARCHAR)";
            StrSQL += " +'''& target=_blank><img src=''images/print1.png'' /></a>' END AS 'PrintBill' ,Case When Not Exists (Select * from tblFollowUpDatails where fdPatientId = PD.pdID)";
            StrSQL += " And Not Exists (Select * from tblFollowUpDatails_OLD where fdPatientId = PD.pdID) then 'Black' When IsExtraDoses = 1 then 'Blue' When fdId IS Null Then 'RED' else 'GREEN' end As FStatus";
            StrSQL += " From tblAppointmentDetails AD Inner Join tblPatientDetails PD On AD.PatientID = PD.pdId Left outer JOIN  tblFollowUpDatails fud ON fud.fdPatientId=PD.pdID And CONVERT(date,  fud.fdDate) = CONVERT(date, getdate()) LEFT JOIN tblUser U ON AD.duid=U.uId";
            StrSQL += " Where AD.Pat_Type = 'O' And AD.PatientID <> 0 And AD.App_Confirm = 'Y' And AD.App_Cancel = 0 And CONVERT(date, AD.AppointmentDate) = CONVERT(date, getdate()) And PD.IsCancel =  0 ";

            if (txtSearch.Text.ToString() != "")
                StrSQL += "AND PD.pdFname +' '+ PD.pdMname +' '+ PD.pdLname Like '%" + txtSearch.Text.ToString() + "%'";

            StrSQL += " Order by SNO desc";
            DataTable dt = gen.getDataTable(StrSQL);
            if (dt.Rows.Count > 0)
            {
                GVD_OLDPat.DataSource = dt;
                GVD_OLDPat.DataBind();
            }
            else
            {
                //GVD_OLDPat.EmptyDataText.ToString();
                GVD_OLDPat.DataSource = dt;
                GVD_OLDPat.DataBind();

                //GVD_OLDPat.DataSource = null;
               // GVD_OLDPat.DataBind();
            }

            //StrSQL = "GetTodayAppointmentForDashboard";
            //SqlCmd = new SqlCommand(StrSQL, conn);
            //SqlDataReader dr;
            //SqlCmd.CommandType = CommandType.StoredProcedure;
            //SqlCmd.Parameters.AddWithValue("@Flag", "OLDPAT_APP_CONF");
            //if (conn.State == ConnectionState.Closed)
            //{
            //    conn.Open();
            //    dr = SqlCmd.ExecuteReader();
            //    DataTable dt = new DataTable();
            //    dt.Load(dr);
            //    if (dt.Rows.Count > 0)
            //    {
            //        GVD_OLDPat.DataSource = dt;
            //        GVD_OLDPat.DataBind();
            //    }
            //    else
            //    {
            //        GVD_OLDPat.DataSource = null;
            //        GVD_OLDPat.DataBind();
            //    }
            //    conn.Close();
            //    dr.Dispose();
            //}
        }
        catch (Exception ex)
        {

        }
    }

    private void OLDPAT_Discharge()
    {
        try
        {
            StrSQL = "Select ROW_NUMBER() over(order By Ad.adid) As SNO ,  PD.pdInitial +' '+ PD.pdFname+' '+ PD.pdMname +' '+ PD.pdLname As pdName , isNull(PD.pdPatientICardNo , '') As pdPatientICardNo";
            StrSQL += ", PD.pdMob , Ad.adid  , PD.pdId , IsNull(fdId,0) As fdId , AD.App_Time As App_Time";
            StrSQL += " ,CASE ISNULL(CAST(fdCharges AS VARCHAR),'') WHEN ''  THEN '<img src=''images/print1.png'' />' ELSE '<a href=''OPDBill.aspx?id='+CAST(fdId AS VARCHAR)+'&IsReconsulting='+ CAST(ISNULL(fdIsReconsulting,0) AS VARCHAR)";
            StrSQL += " +'''& target=_blank><img src=''images/print1.png'' /></a>' END AS 'PrintBill', Case When fdId IS Null Then 'RED' else 'GREEN' end As FStatus";
            StrSQL += " From tblAppointmentDetails AD Inner Join tblPatientDetails PD On AD.PatientID = PD.pdId Left outer JOIN  tblFollowUpDatails fud ON fud.fdPatientId=PD.pdID And CONVERT(date,  fud.fdDate) = CONVERT(date, getdate())";
            StrSQL += " left join tblIpdMaster im ON PD. pdID = IM.ipdpdId and CONVERT(date,  im.ipdDischargeDate) = CONVERT(date, getdate())";
            StrSQL += " Where AD.Pat_Type = 'O' And AD.PatientID <> 0 And AD.App_Cancel = 0 And PD.IsCancel = 0 and ipdIsDischarge = 1 ";

            //StrSQL = "Select * from vwGetHomeOPDDischarge Where";
            if (txtSearch.Text.ToString() != "")
                StrSQL += "AND PD.pdFname +' '+ PD.pdMname +' '+ PD.pdLname Like '%" + txtSearch.Text.ToString() + "%'";

            DataTable dt = gen.getDataTable(StrSQL);
            if (dt.Rows.Count > 0)
            {
                GVD_OLDPat.DataSource = dt;
                GVD_OLDPat.DataBind();
            }
            else
            {
                GVD_OLDPat.EmptyDataText.ToString();
                GVD_OLDPat.DataBind();

                //GVD_OLDPat.DataSource = null;
               // GVD_OLDPat.DataBind();
            }

            //    StrSQL = "GetTodayAppointmentForDashboard";
            //SqlCmd = new SqlCommand(StrSQL, conn);
            //SqlDataReader dr;
            //SqlCmd.CommandType = CommandType.StoredProcedure;
            //SqlCmd.Parameters.AddWithValue("@Flag", "Bind_Discharge_OPD_Patient");
            ////SqlCmd.Parameters.AddWithValue("@PName", txtSearch.Text.ToString());
            //if (conn.State == ConnectionState.Closed)
            //{
            //    conn.Open();
            //    dr = SqlCmd.ExecuteReader();
            //    DataTable dt = new DataTable();
            //    dt.Load(dr);
            //    if (dt.Rows.Count > 0)
            //    {
            //        GVD_OLDPat.DataSource = dt;
            //        GVD_OLDPat.DataBind();
            //    }
            //    else
            //    {
            //        GVD_OLDPat.DataSource = null;
            //        GVD_OLDPat.DataBind();
            //    }
            //    conn.Close();
            //    dr.Dispose();
            //}
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnSearch_onclick(object sender, EventArgs e)
    {
        OLDPAT_Discharge();
        OLDPAT_APP_CONF();
    }

    private void NEWPAT_APP_NOT_CONF()
    {
        StrSQL = "GetTodayAppointmentForDashboard";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Flag", "NEWPAT_APP_NOT_CONF");
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_NewPatApp.DataSource = dt;
                GVD_NewPatApp.DataBind();
            }
            else
            {
                GVD_NewPatApp.DataSource = null;
                GVD_NewPatApp.DataBind();
            }
            conn.Close();
            dr.Dispose();
        }
    }

    private void OLDPAT_APP_NOT_CONF()
    {
        StrSQL = "GetTodayAppointmentForDashboard";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Flag", "OLDPAT_APP_NOT_CONF");
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_OldPatApp.DataSource = dt;
                GVD_OldPatApp.DataBind();
            }
            else
            {
                GVD_OldPatApp.DataSource = null;
                GVD_OldPatApp.DataBind();
            }
            conn.Close();
            dr.Dispose();
        }
    }

    private void NEWPAT_APP_CONF_NON()
    {
        StrSQL = "GetTodayAppointmentForDashboard";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Flag", "NEWPAT_APP_CONF_NON");
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_NonApp.DataSource = dt;
                GVD_NonApp.DataBind();
            }
            else
            {
                GVD_NonApp.DataSource = null;
                GVD_NonApp.DataBind();
            }
            conn.Close();
            dr.Dispose();
        }
    }

    protected void GVD_NewPatApp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Confirm")
        {
            try
            {
        Response.Redirect("AddNewPatient.aspx?ADID=" + e.CommandArgument);
            }
            catch
            { }
        }
    }

    protected void GVD_OldPatApp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Confirm")
        {
            try
            {
                //Response.Redirect("AddNewPatient.aspx?ADID=" + e.CommandArgument);

                SqlCmd = new SqlCommand();
                SqlCmd.CommandText = "proc_ConfirmAppointmentDetails";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = conn;
                if (conn.State == ConnectionState.Closed)
                    conn.Open();
                SqlCmd.Parameters.AddWithValue("@adId", e.CommandArgument);
                SqlCmd.Parameters.AddWithValue("@ConfirmBy", uid);
                SqlCmd.Parameters.AddWithValue("@pdfinyear", gen.GetCurrentFinYear());
                SqlCmd.Parameters.AddWithValue("@Flag", "Confirm");
                SqlCmd.ExecuteNonQuery();
                conn.Close();
                SqlCmd.Dispose();
                Response.Redirect("Home.aspx");
            }
            catch
            { }
        }
    }

    private void GetTodaysAppCount()
    {
        StrSQL = "Exec Proc_GetTodayAppointmentCountOnDashBoard ";

        if (conn.State == ConnectionState.Closed)
            conn.Open();
        SqlCmd = new SqlCommand(StrSQL, conn);
        Sqldr = SqlCmd.ExecuteReader();

        while (Sqldr.Read())
        {
            lblTotalNewCaseCount.Text = Sqldr["NTotal"].ToString();
            lblTotalCompNewCase.Text = Sqldr["NC"].ToString();
            lblTotalNewCasePending.Text = Sqldr["NP"].ToString();

            lblTotalOPDCount.Text = Sqldr["OTotal"].ToString();
            lblTotalOPDCompleted.Text = Sqldr["OC"].ToString();
            lblTotalOPDPending.Text = Sqldr["OP"].ToString();
            lblUpcomingOPD.Text = Sqldr["UPComimg_pdID"].ToString();

        }
        Sqldr.Close();
        conn.Close();
    }

    protected void img_QuickR_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("QuickRegistration.aspx");
    }

    protected void img_NewCase_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("AddNewPatient.aspx?flag=new");
    }
    //protected void img_OPD_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.Redirect("AddNewPatient.aspx?flag=new");
    //}

    protected void GVD_NewPat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            try
            {
                Response.Redirect("AddNewPatient.aspx?ADID=" + e.CommandArgument);
            }
            catch
            { }
        }

        if (e.CommandName == "NewCaseFol")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                string fdID = arg[1];

                Session["ipdid"] = null;
                Session["CasePatientID"] = pdID;
                //Response.Redirect("ProfarmaOfCase.aspx", false);
                //Response.Redirect("FollowupDetail.aspx", false);
                //Response.Redirect("ConsultPatient.aspx", false);

                //int cnt = Convert.ToInt32(gen.executeScalar("Select Count(*) From tblFollowUpDatails Where fdPatientId=" + pdID).ToString());
                int cnt = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from vwConsultingHistoryNewAks Where fdPatientId=" + pdID).ToString());


                if (cnt >= 1)
                {
                    Response.Redirect("FollowupDetail.aspx?SC=HM&fdId=" + fdID);
                }
               else
                {
                    Response.Redirect("ConsultPatient.aspx?SC=HM&fdId=" + fdID);
                }


            }
            catch
            { }
        }

    }

    protected void GVD_NonApp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            try
            {
                Response.Redirect("AddNewPatient.aspx?ADID=" + e.CommandArgument);
            }
            catch
            { }
        }

        if (e.CommandName == "NewCaseFol")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                string fdID = arg[1];

                Session["ipdid"] = null;
                Session["CasePatientID"] = pdID;
                //Response.Redirect("ProfarmaOfCase.aspx", false);
                //Response.Redirect("FollowupDetail.aspx", false);
                //Response.Redirect("ConsultPatient.aspx", false);

                //int cnt = Convert.ToInt32(gen.executeScalar("Select Count(*) From tblFollowUpDatails Where fdPatientId=" + pdID).ToString());
                int cnt = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from vwConsultingHistoryNewAks Where fdPatientId=" + pdID).ToString());


                if (cnt >= 1)
                {
                    Response.Redirect("FollowupDetail.aspx?SC=HM&fdId=" + fdID);
                }
                else
                {
                    Response.Redirect("ConsultPatient.aspx?SC=HM&fdId=" + fdID);
                }


            }
            catch
            { }
        }

    }

    protected void GVD_OLDPat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            try
            {
                Response.Redirect("AddNewPatient.aspx?ADID=" + e.CommandArgument);
            }
            catch
            { }
        }

        if (e.CommandName == "ODPFol")
        {
            try
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                string fdID = arg[1];

                Session["ipdid"] = null;
                Session["CasePatientID"] = pdID;
                //Response.Redirect("ProfarmaOfCase.aspx", false);
                //Response.Redirect("FollowupDetail.aspx", false);

                //int cnt = Convert.ToInt32(gen.executeScalar("Select Count(*) From tblFollowUpDatails Where fdPatientId=" + pdID).ToString());
                int cnt = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from vwConsultingHistoryNewAks Where fdPatientId=" + pdID).ToString());
                int cnt2 = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from tblFollowUpDatails Where CONVERT(date, fdDate) = CONVERT(date, getdate()) and IsExtraDoses = 1 And fdPatientId = " + pdID).ToString());
                int cnt3 = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from tblFollowUpDatails Where CONVERT(date, fdDate) = CONVERT(date, getdate()) and fdSource = 'MC' And fdPatientId = " + pdID).ToString());

                if (cnt > 0)
                {

                    if (cnt == 1)
                    {

                        int cnt1 = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from vwConsultingHistoryNewAks Where CONVERT(date, fdDate) = CONVERT(date, getdate()) And fdPatientId=" + pdID).ToString());
                        
                        if (cnt1 > 0)
                        {
                            if (fdID != "0")
                            {
                                Response.Redirect("ConsultPatient.aspx?SC=HM&Stat=NO&fdId=" + fdID);
                            }
                            else if (fdID == "0")
                            {
                                Response.Redirect("ConsultPatient.aspx?SC=HM&fdId=" + fdID);
                            }
                        }
                        else
                        {
                            if (fdID != "0")
                            {
                                Response.Redirect("FollowupDetail.aspx?SC=HM&Stat=NO&fdId=" + fdID);
                            }
                            else if (fdID == "0")
                            {
                                Response.Redirect("FollowupDetail.aspx?SC=HM&fdId=" + fdID);
                            }
                        }

                       
                    }
                    else
                    {
                        if (fdID != "0")
                        {
                            if (cnt2 > 0)
                            {
                                if (cnt3 > 0)
                                { 
                                    if (fdID != "0")
                                    {
                                        Response.Redirect("FollowupDetail.aspx?SC=MC&Stat=NO&fdId=" + fdID);
                                    }
                                    else if(fdID == "0")
                                    {
                                        Response.Redirect("FollowupDetail.aspx");
                                    }
                                }
                                else
                                {
                                    if (fdID != "0")
                                    {
                                        Response.Redirect("FollowupDetail.aspx");
                                    }
                                    else if (fdID == "0")
                                    {
                                        Response.Redirect("FollowupDetail.aspx?SC=MC&Stat=NO&fdId=" + fdID);
                                    }
                                }
                            }
                            else
                            { 
                                Response.Redirect("FollowupDetail.aspx?SC=HM&Stat=NO&fdId=" + fdID);
                            }
                        }
                        else if (fdID == "0")
                        {
                            if (cnt2 > 0)
                            {
                                Response.Redirect("FollowupDetail.aspx?SC=MC&fdId=" + fdID);
                            }
                            else
                            {
                                Response.Redirect("FollowupDetail.aspx?SC=HM&fdId=" + fdID);
                            }
                        }
                    }

                }
                else
                {
                    Response.Redirect("ConsultPatient.aspx", false);
                }

            }
            catch
            { }
        }

    }

    private void BindBedOccGrid()
    {
        StrSQL = "sp_GetWardBedOcc";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_BedOcc.DataSource = dt;
                GVD_BedOcc.DataBind();
            }
            conn.Close();
            dr.Dispose();

        }
    }

    protected void img_btnNewApp_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("NewAppointment.aspx");
    }

    private void GetBirthDayList()
    {

        string StrDate = "";
        StrDate = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        string[] _doc = StrDate.ToString().Split('/');
        string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

        int cnt = Convert.ToInt32(gen.executeScalar("Select Count(STID) from tblSmsTransaction Where Sources In ('PatientBirthDay' , 'DoctorBirthDay' , 'UserBirthDay') And CONVERT(date, EventDate) ='" + doc + "'").ToString());

        if (cnt > 0)
        {
            if (Session["BirthSMSStatus"] != null)
            {
                if (Session["BirthSMSStatus"].ToString() == "SEND")
                {
                    pnlBirthList.Visible = false;
                }
            }
            else
            {
                Session["BirthSMSStatus"] = "SEND";
                BirthDayPatientList("IPDPatient");
                BirthDayDoctorsList();
                BirthDayUsersList();
                pnlBirthList.Visible = true;
            }
              
        }
        else
        {
            BirthDayPatientList("ALLPatient");
            BirthDayDoctorsList();
            BirthDayUsersList();

            if (Uroll == "1")
            {
                SendBirthSMSToPatient();
                SendBirthSMSToDoctor();
                SendBirthSMSToUser();
            }

            BirthDayPatientList("IPDPatient");
            Session["BirthSMSStatus"] = "SEND";
            pnlBirthList.Visible = true;
        }

    }

    private void BirthDayPatientList(string Type)
    {

        strSQL = "Proc_FillBirthListAks";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Flag", Type);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                gridPatientBirthList.DataSource = dt;
                gridPatientBirthList.DataBind();
            }
            else
            {
                gridPatientBirthList.DataSource = null;
                gridPatientBirthList.DataBind();
            }
            con.Close();
            dr.Dispose();
        }
    }

    private void BirthDayDoctorsList()
    {
        strSQL = "Proc_FillBirthListAks";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Flag", "Doctor");
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                gridDoctorsBirthList.DataSource = dt;
                gridDoctorsBirthList.DataBind();
            }
            else
            {
                gridDoctorsBirthList.DataSource = null;
                gridDoctorsBirthList.DataBind();
            }
            con.Close();
            dr.Dispose();
        }
    }

    private void BirthDayUsersList()
    {
        strSQL = "Proc_FillBirthListAks";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@Flag", "User");
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                gridUsersBirthList.DataSource = dt;
                gridUsersBirthList.DataBind();
            }
            else
            {
                gridUsersBirthList.DataSource = null;
                gridUsersBirthList.DataBind();
            }
            con.Close();
            dr.Dispose();
        }
    }

    private void SendBirthSMSToPatient()
    {

        foreach (GridViewRow g1 in gridPatientBirthList.Rows)
        {
          
            Label lblpdid = g1.FindControl("lblpdid") as Label;
            Label lblpdMob = g1.FindControl("lblpdMob") as Label;
            Label lblName = g1.FindControl("lblName") as Label;


                if (lblpdMob.Text != "")
                {
                    String smsstring;
                decimal TemplateId = 1207160931701257871;
                smsstring = "Wish you a many many happy returns of the day.May God bless you with health, wealth and prosperity in your life. \nFrom : Aditya homoeopathic hospital & healing centre. ";
                    string strsendSMS = genral.GetResponse(lblpdMob.Text.Trim(), smsstring, TemplateId);

                    if (con.State == ConnectionState.Closed)
                        con.Open();

                    SqlCmd = new SqlCommand();
                    SqlCmd.CommandText = "sp_InsSMSTransaction";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Connection = con;
                    SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                    SqlCmd.Parameters.AddWithValue("@MobileNo", lblpdMob.Text.Trim());
                    SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                    SqlCmd.Parameters.AddWithValue("@SenderID", uid);
                    SqlCmd.Parameters.AddWithValue("@Pdid", lblpdid.Text);
                    SqlCmd.Parameters.AddWithValue("@Sources", "PatientBirthDay");
                    SqlCmd.ExecuteNonQuery();
                    SqlCmd.Dispose();
                    con.Close();
                }
         }

    }

    private void SendBirthSMSToDoctor()
    {

        foreach (GridViewRow g1 in gridDoctorsBirthList.Rows)
        {

            Label lbldid = g1.FindControl("lbldid") as Label;
            Label lbldMobileNo = g1.FindControl("lbldMobileNo") as Label;
            Label lblName = g1.FindControl("lblName") as Label;


            if (lbldMobileNo.Text != "")
            {
                String smsstring;
                decimal TemplateId = 1207160931701257871;
                smsstring = "Wish you a many many happy returns of the day.May God bless you with health, wealth and prosperity in your life. \nFrom : Aditya homoeopathic hospital & healing centre. ";
                string strsendSMS = genral.GetResponse(lbldMobileNo.Text.Trim(), smsstring, TemplateId);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                SqlCmd = new SqlCommand();
                SqlCmd.CommandText = "sp_InsSMSTransaction";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = con;
                SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                SqlCmd.Parameters.AddWithValue("@MobileNo", lbldMobileNo.Text.Trim());
                SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                SqlCmd.Parameters.AddWithValue("@SenderID", uid);
                SqlCmd.Parameters.AddWithValue("@Pdid", lbldid.Text);
                SqlCmd.Parameters.AddWithValue("@Sources", "DoctorBirthDay");
                SqlCmd.ExecuteNonQuery();
                SqlCmd.Dispose();
                con.Close();
            }
        }

    }

    private void SendBirthSMSToUser()
    {

        foreach (GridViewRow g1 in gridUsersBirthList.Rows)
        {

            Label lbluid = g1.FindControl("lbluid") as Label;
            Label lbluContact = g1.FindControl("lbluContact") as Label;
            Label lblName = g1.FindControl("lblName") as Label;


            if (lbluContact.Text != "")
            {
                String smsstring;
                decimal TemplateId  = 1207160931701257871;
                smsstring = "Wish you a many many happy returns of the day.May God bless you with health, wealth and prosperity in your life. \nFrom : Aditya homoeopathic hospital & healing centre. ";
                string strsendSMS = genral.GetResponse(lbluContact.Text.Trim(), smsstring, TemplateId);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                SqlCmd = new SqlCommand();
                SqlCmd.CommandText = "sp_InsSMSTransaction";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = con;
                SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                SqlCmd.Parameters.AddWithValue("@MobileNo", lbluContact.Text.Trim());
                SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                SqlCmd.Parameters.AddWithValue("@SenderID", uid);
                SqlCmd.Parameters.AddWithValue("@Pdid", lbluid.Text);
                SqlCmd.Parameters.AddWithValue("@Sources", "UserBirthDay");
                SqlCmd.ExecuteNonQuery();
                SqlCmd.Dispose();
                con.Close();
            }
        }

    }

    protected void imgCloseButton_Click(object sender, ImageClickEventArgs e)
    {
        pnlBirthList.Visible = false;
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        BindBedOccStatusGrid();
        NEWPAT_APP_CONF();
        OLDPAT_APP_CONF();
        NEWPAT_APP_NOT_CONF();
        OLDPAT_APP_NOT_CONF();
        NEWPAT_APP_CONF_NON();
        GetTodaysAppCount();
        BindBedOccGrid();
        pnlBirthList.Visible = false;
       
        //ScriptManager.RegisterStartupScript(this, this.GetType(),
        //        "alert",
        //        "alert('Hello');window.location ='Home.aspx';",
        //        true);

    }

}
