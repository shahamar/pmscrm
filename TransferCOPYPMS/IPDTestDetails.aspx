﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="IPDTestDetails.aspx.cs" Inherits="IPDTestDetails" Title="IPD Test Details" %>

<%@ Register Src="tabipd.ascx" TagName="Tab" TagPrefix="tab1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <%--<script src="js/jquery-1.4.1.min.js" type="text/javascript"></script>--%>
    <%--<script src="js/jquery-1.4.4.js" type="text/javascript"></script>--%>
    <script language="javascript" type="text/javascript" src="js/actb.js"></script>
    <style type="text/css">
        .col1
        {
            vertical-align: top;
            width: 20%;
        }
        
        .col2
        {
            width: 80%;
            vertical-align: top;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function toggleSelection(source) {
            //alert("toggleSelection");
            $("#ctl00_ContentPlaceHolder1_GRV1 input[id*='cbAC']").each(function (index) {
                $(this).attr('checked', source.checked);
            });
        }

        function toggleSelection1(source) {
            //alert("toggleSelection");
            $("#ctl00_ContentPlaceHolder1_GRVSelected input[id*='cbSelAC']").each(function (index) {
                $(this).attr('checked', source.checked);
            });
        } 
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        IPD Test Details</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <%--<ul class="quicktabs_tabs quicktabs-style-excel">
                                <li class="qtab-Demo active first" id="li_1">
                                    <a class="qt_tab active" href="javascript:void(0)">Add New Patients</a>
                                </li>
                                
                                <li class="qtab-HTML last" id="li_9">
                                    <a class="qt_tab active" href="ManagePatients.aspx">Manage Patients</a>
                                </li>
                                
                                </ul>--%>
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto; border: 1.5px solid #E2E2E2;">
                                <table style="width: 100%;" cellspacing="0px" cellpadding="2px">
                                    <tr>
                                        <td>
                                            <tab1:Tab ID="tabcon" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; color: Red;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel runat="server" ID="UP1">
                                                <ContentTemplate>
                                                    <table style="width: 100%;" cellspacing="5px" cellpadding="2px">
                                                        <tr>
                                                            <td class="col1">
                                                                Test :
                                                            </td>
                                                            <td class="col2">
                                                                <asp:DropDownList ID="ddTests" runat="server" Width="180px" CssClass="required field">
                                                                </asp:DropDownList>
                                                                <asp:CustomValidator ID="cvTest" runat="server" ErrorMessage="* Please Specify Test" ValidationGroup="vgCaltest">
                                                                <span class="error" 
                                                            style="margin-left:1px;">Please Specify Test.</span>
                                                            </asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col1">
                                                                Test Conducted On :
                                                            </td>
                                                            <td class="col2">
                                                                <asp:Calendar ID="Calendar1" runat="server" OnDayRender="Calendar1_DayRender" OnPreRender="Calendar1_PreRender"
                                                                    OnSelectionChanged="Cal_SelectionChanged">
                                                                    <OtherMonthDayStyle BackColor="#EE0101" />
                                                                </asp:Calendar>
                                                                <asp:CustomValidator ID="cvCaltest" runat="server" ErrorMessage="* A date is required"
                                                                    ValidationGroup="vgCaltest">
                                                                <span class="error" 
                                                            style="margin-left: 650px;margin-top: 330px;position: absolute;">Test Conducted Date is Required.</span>
                                                                </asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col1">
                                                                Hours(In case of Monitor) :
                                                            </td>
                                                            <td class="col2">
                                                                <asp:TextBox ID="txtHours" runat="server" CssClass="required field" 
                                                                    Width="100px"></asp:TextBox>
                                                                <asp:CustomValidator ID="cvHrs" runat="server" ErrorMessage="" ControlToValidate="txtHours"
                                                                    ValidationGroup="vgCaltest"><span class="error">Please enter Integer Number Only </span> </asp:CustomValidator>
                                                                <asp:CustomValidator ID="cvHours" runat="server" ErrorMessage="" ControlToValidate="txtHours"
                                                                    ValidationGroup="vgCaltest"><span class="error">Please enter valid hours</span></asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                            <%--<tr>
                                                            <td class="col1">
                                                               Charges :
                                                            </td>
                                                            <td class="col2">
                                                                <asp:TextBox ID="txtCharges" runat="server" CssClass="field" Width="160px" 
                                                                    ></asp:TextBox>
                                                    
                                                 
                                                    <asp:RegularExpressionValidator ID="regremcharge" runat="server" ControlToValidate="txtCharges" Display="Dynamic"
                                                        ErrorMessage="Please Enter Only Numbers" ValidationExpression="^(-)?\d+(\.\d\d)?$"
                                                        ValidationGroup="vgCaltest">
                                            <span class="error">Please enter Only Numbers.</span>
                                                    </asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>--%>
                                                        <tr>
								<td></td>
                                                         <%--<td class="col1">
                                                               Payment Mode *:
                                                            </td>--%>
                                                        <td class="col2">
                                                            <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="160px" Visible="false"
                                    CssClass="field">
                                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                    <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                    <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                      <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                    <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                    <asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
                                    <asp:ListItem Text="Free" Value="Free"></asp:ListItem>
                                </asp:DropDownList>
					

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                        Display="Dynamic" ErrorMessage="Please specify Payment Mode" ValidationGroup="check">
                                                        <span class="error" >Please specify Payment Mode</span></asp:RequiredFieldValidator>
           				</td> 
                                                       </tr>
                                                        <tr>
                                                            <td class="col1">
                                                                &nbsp;
                                                            </td>
                                                            <td class="col2">
                                                                <asp:Button ID="btnAdd" runat="server" CssClass="textbutton b_submit" OnClick="btnAdd_Click"
                                                                    Style="margin-top: 2px;" Text="Add" Width="69px"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col1" colspan="2">
                                                                <asp:Panel ID="pnlfeat" runat="server" Visible="False">
                                                                    <table width="100%" cellspacing="0px" cellpadding="2px">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:GridView ID="GRV1" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                                                    CellPadding="3" Width="100%">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                                                                                            <HeaderTemplate>
                                                                                                <input id="cbAll" runat="server" onclick="toggleSelection(this)" type="checkbox"
                                                                                                    value="checked" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <input id="cbAC" runat="server" type="checkbox" value="checked" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderStyle-Width="20%" HeaderText="Test Name" ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-HorizontalAlign="Left">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbltmTestName" runat="server" Text='<%# Eval("tmTestName")%>'></asp:Label>
                                                                                                <asp:HiddenField ID="hdntmId" runat="server" Value='<%# Eval("itdTmid")%>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderStyle-Width="66%" HeaderText="Conducted On" ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-HorizontalAlign="Left">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbltmDate" runat="server" Text=' <%# Eval("tmDate")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderStyle-Width="12%" HeaderText="Hours" ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-HorizontalAlign="Left">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbltmHours" runat="server" Text=' <%# Eval("tmHours")%>'></asp:Label>
                                                                                            </ItemTemplate>                                                                                            
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderStyle-Width="12%" HeaderText="Payment Mode" ItemStyle-HorizontalAlign="Left"
                                                                                            HeaderStyle-HorizontalAlign="Left">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblPayment_Mode" runat="server" Text=' <%# Eval("Payment_Mode")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="btnRemove" runat="server" Text="Remove" CssClass="textbutton b_submit"
                                                                                    Width="69px" OnClick="btnRemove_Click" Style="margin-top: 5px" />
                                                                                <asp:Button ID="_btnSave" runat="server" CssClass="textbutton b_submit" OnClick="_btnSave_Click"
                                                                                    Text="Save" Width="69px" Style="margin-top: 5px" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
