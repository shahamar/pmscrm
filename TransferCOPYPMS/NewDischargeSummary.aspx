﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewDischargeSummary.aspx.cs"
    Inherits="NewDischargeSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Discharge Summery</title>
	<script type="text/javascript">
		function PrintDiv()
		{
			w = window.open();
			w.document.write(document.getElementById("Panel1").innerHTML);
			w.print();
			w.close();
			close();
		}
	</script>
</head>
<body>
    <form id="form1" runat="server">
    <style type="text/css">
        .sec1td1
        {
            text-align: left;
            width: 15%;
        }
        .sec1td2
        {
            text-align: left;
            width: 90%;
            padding-top: 20px;
        }
        .col1
        {
            text-align: left;
            width: 15%;
            border-left: 1px solid #000000;
            border-top: 1px solid #000000;
        }
        .col2
        {
            text-align: left;
            width: 35%;
            border-left: 1px solid #000000;
            border-top: 1px solid #000000;
        }
        .col5
        {
            text-align: left;
            width: 35%;
            border-left: 1px solid #000000;
            border-top: 1px solid #000000;
            border-right: 1px solid #000000;
        }
        .col3
        {
            width: 50%;
            text-align: right;
            border-left: 1px solid #000000;
            border-top: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }
        .col4
        {
            width: 50%;
            text-align: left;
            border-left: 1px solid #000000;
            border-top: 1px solid #000000;
            border-right: 1px solid #000000;
            border-bottom: 1px solid #000000;
        }
        .head1
        {
            width: 10%;
            text-align: right;
            padding-right: 5px;
            border-top: 1px solid #000000;
            border-right: 1px solid #000000;
            border-left: 1px solid #000000;
        }
        .head2
        {
            width: 51%;
            text-align: left;
            padding-left: 2px;
            border-top: 1px solid #000000;
            border-right: 1px solid #000000;
        }
        .head3
        {
            width: 13%;
            text-align: right;
            padding-right: 2px;
            border-top: 1px solid #000000;
            border-right: 1px solid #000000;
            padding-right: 10px;
        }
        .head4
        {
            width: 13%;
            text-align: right;
            padding-right: 2px;
            border-top: 1px solid #000000;
            border-right: 1px solid #000000;
            padding-right: 10px;
        }
        .head5
        {
            width: 13%;
            text-align: right;
            padding-right: 2px;
            border-top: 1px solid #000000;
            border-right: 1px solid #000000;
            padding-right: 10px;
        }
        .head6
        {
            text-align: left;
            padding-left: 2px;
            border-top: 1px solid #000000;
            border-right: 1px solid #000000;
            border-left: 1px solid #000000;
            border-bottom: 1px solid #000000;
            vertical-align: bottom;
            padding-bottom: 10px;
            padding-left: 10px;
        }
        .collast
        {
            border-bottom: 1px solid #000000;
            border-left: 1px solid #000000;
            border-right: 1px solid #000000;
            font-family: Times New Roman;
            font-size: 14px;
            height: 66px;
            padding-bottom: 10px;
            padding-left: 10px;
            vertical-align: bottom;
            text-align: left;
        }
    </style>
	<div>
		<table width="60%" align="center">
		<tr align="right">
			<td><img src="images/print.png" Style="cursor: pointer;" Onclick="PrintDiv()" /></td>
		</tr>
		</table>
	</div>
    <div style="text-align: center; height: 80%">
        <asp:Panel ID="Panel1" runat="server" Height="50%">
            <div>
                <center>
                    <asp:Repeater ID="rptParent" runat="server">
                        <ItemTemplate>
                            <table width="100%" cellpadding="0" cellspacing="5" border="0" class="maintable"
                                style="font-size: 14px">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table width="100%" align="center" cellspacing="0px" cellpadding="0px">
                                                <tr>
                                                    <td style="font-family: Times New Roman; border-left: 1px solid #000000; border-right: 1px solid #000000;
                                                        border-top: 1px solid #000000; padding-top: 5px; padding-bottom: 5px" align="center"
                                                        colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <img src="images/Aditya_logo_final.png" width="65px" height="50px" />
                                                                </td>
                                                                <td style="padding-left: 20px">
                                                                    <b style="font-size: 20px;">Aditya Homoeopathic Hospital & Healing Center</b><br />
                                                                    <span style="font-size: 14px">DR. AMARSINHA D. NIKAM (M.D.Homoeopath)</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-family: Times New Roman; border-left: 1px solid #000000; border-right: 1px solid #000000;
                                                        border-top: 1px solid #000000; padding-top: 5px; padding-bottom: 5px" align="center">
                                                        <span style="font-size: 18px"><strong>Address :</strong> Old Kate Pimple Road,Near Shivaji
                                                            Chawk,Pimprigaon,Pune – 411017
                                                            <br />
                                                            <b>Tel :</b> 020-27412197 / 8806061061 <b>Website :</b> www.drnikam.com</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="font-family: Times New Roman; border-left: 1px solid #000000;
                                                        border-right: 1px solid #000000; border-top: 1px solid #000000; padding-top: 5px;
                                                        padding-bottom: 5px">
                                                        <font style="font-size: 20px;"><b>Discharge Summary </b></font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table id="tblsec1" style="color: black; border-left: 1px solid #000000; border-right: 1px solid #000000;
                                                            border-top: 1px solid #000000; font-size: 16px;" width="100%">
                                                            <tr>
                                                                <td>
                                                                    Name :
                                                                    <%# DataBinder.Eval(Container.DataItem, "PName")%>
                                                                </td>
                                                                <td>
                                                                    Age :
                                                                    <%# DataBinder.Eval(Container.DataItem, "pdAge")%>
                                                                </td>
                                                                <td>
                                                                    Gender :
                                                                    <%# DataBinder.Eval(Container.DataItem, "pdSex")%>
                                                                </td>
                                                                <td>
                                                                    Case Paper No.:
                                                                    <%# DataBinder.Eval(Container.DataItem, "pdCasePaperNo")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    D.O.A. :
                                                                    <%# DataBinder.Eval(Container.DataItem, "DOA")%>
                                                                </td>
                                                                <td>
                                                                    D.O.D. :
                                                                    <%# DataBinder.Eval(Container.DataItem, "DOC")%>
                                                                </td>
                                                                <td>
                                                                    Consultant :
                                                                    <%--<%# DataBinder.Eval(Container.DataItem, "ipdRefBy")%>--%>
                                                                    Dr. Amarsinha Nikam
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr style="color: black; font-size: 16px;">
                                                                <td colspan="5" style="color: black; font-size: 16px;">
                                                                    <br />
                                                                    <br />
                                                                    <b>Diagnosis :</b>
                                                                    <%# DataBinder.Eval(Container.DataItem, "ipdFinalDiagnosis")%>                                                                    
                                                                </td>
                                                                <%--<table width="100%" style="color: black; font-size: 16px; padding-top: 10px; border-left: 1px solid #000000;
                                                                    border-right: 1px solid #000000;">
                                                                    <tr>
                                                                        <td class="sec1td1">
                                                                            <b>Diagnosis :</b>
                                                                        </td>
                                                                        <td width="50%">
                                                                            <%# DataBinder.Eval(Container.DataItem, "ipdFinalDiagnosis")%>
                                                                        </td>
                                                                    </tr>
                                                                </table>--%>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="color: black; font-size: 16px;">
                                                                    <br />
                                                                    <br />
                                                                    <b>Complaints and History of Present Illness :</b>
                                                                    <%# DataBinder.Eval(Container.DataItem, "ChiefCo")%>                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="color: black; font-size: 16px;">
                                                                    <br />
                                                                    <br />
                                                                    <b>Investigation :</b>
                                                                    <%# DataBinder.Eval(Container.DataItem, "Investigation")%>                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="color: black; font-size: 16px;">
                                                                    <br />
                                                                    <br />
                                                                    <b>Treatment :______________________</b>
                                                                    <%# DataBinder.Eval(Container.DataItem, "Treatment")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="color: black; font-size: 16px;">
                                                                    <br />
                                                                    <br />
                                                                    <b>Diet :_____________________</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="color: black; font-size: 16px;">
                                                                    <br />
                                                                    <br />
                                                                    <b>Druge :_____________________</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="color: black; font-size: 16px;">
                                                                    <br />
                                                                    <br />
                                                                    <b>Exercise :______________________</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <%--<td style="color: black; font-size: 16px;">
                                                                    <br />
                                                                    <br />
                                                                    <b>Next Visit On :</b>              
                                                                    ____________________________________________________________________                                                    
                                                                    <asp:HiddenField ID="hdnpdID" runat="server" Value="0" />
                                                                </td>--%>
                                                                <table width="100%" style="color: black; font-size: 16px; padding-top: 30px; border-left: 1px solid #000000;
                                                                    border-right: 1px solid #000000;">
                                                                    <tr>
                                                                        <td class="sec1td1">
                                                                            <b>Next Visit On :</b> 
									________________________
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                </table>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="color: black; font-size: 16px;border-bottom: 1px solid #000000;border-left: 1px solid #000000;
                                                                    border-right: 1px solid #000000;">
                                                                    <div style="float: right; font-family: Times New Roman; font-size: 20px; height: 50%;
                                                                        padding-right: 2px; text-align: center;">
                                                                        <br />
                                                                        <br />
                                                                        <br />
                                                                        <br />
                                                                        <b>Aditya Homoeopathic Hospital
                                                                            <br />
                                                                            & Healing Center</b>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Label ID="lblInv" runat="server" Text="" style="display:none"></asp:Label>
                    <asp:Label ID="lblChiefCo" runat="server" Text="" style="display:none"></asp:Label>
                </center>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
