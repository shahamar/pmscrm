﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class CalMulitpleSelection : System.Web.UI.Page
{
    public static List<string> list = new List<string>();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Cal_SelectionChanged(object sender, EventArgs e)
    {
        if (Session["SelectedDates"] != null)
        {
            List<string> selectedList = (List<string>)Session["SelectedDates"];
            foreach (string s in selectedList)
            {
                Calendar1.SelectedDates.Add(Convert.ToDateTime(s));
            }
            list.Clear();
        }
    }
    protected void Show_Click(object sender, EventArgs e)
    {
        if (Session["SelectedDates"] != null)
        {
            List<string> newList = (List<string>)Session["SelectedDates"];
            foreach (string s in newList)
            {
                Response.Write(s + "<BR/>");
            }
        }
    }

    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        if (e.Day.IsSelected == true)
        {
            list.Add(e.Day.Date.ToString("MM/dd/yyyy"));
        }
        Session["SelectedDates"] = list;
    }
}