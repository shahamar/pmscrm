﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="OldPatientAppointment.aspx.cs" Inherits="OldPatientAppointment" Title="Set Appoinment For Old Patient" %>

<%@ Register Src="tab.ascx" TagName="Tab" TagPrefix="tab1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <style type="text/css">
        .savebutton
        {
            text-align: left;
            float: left;
            padding: 3px;
        }
        .tabdiv
        {
            border-width: 2px 2px 0px;
            border-style: solid solid none;
            border-color: #E2E2E2;
            position: inherit;
            height: auto;
        }
        #tabtbl
        {
            color: #000;
            font-size: 12px;
            font-weight: lighter;
            padding: 2px;
            background-color: #F1F1F1;
        }
        .alt1
        {
            width: 65px;
        }
        .alt2
        {
            width: 125px;
        }
    </style>
    <script type="text/javascript">

        if (top.location != self.location) {
            top.location = self.location.href
        }
	
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblmsg" runat="server" ForeColor="DarkRed" Font-Bold="true" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        Patient Details</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <div class="tabdiv">
                                <table id="tabtbl" style="width: 100%;" cellpadding="2px" cellspacing="0px" border="1px">
                                    <tr>
                                        <td class="alt1">
                                            Patient Name :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lblPatientName" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="alt1">
                                            Age :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lblAge" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="alt1">
                                            Gender :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lblGender" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="alt1">
                                            Tele :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lbltele" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="alt1">
                                            Mobile :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lblmob" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="alt1">
                                            Email :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="alt1">
                                            Patient ID :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lblPatientId" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="alt1">
                                            Case Paper No :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lblCasePaperNo" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="alt1">
                                            Cassette No :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lblCassetteNo" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="alt1">
                                            Occupation :
                                        </td>
                                        <td class="alt2">
                                            <asp:Label ID="lblOccupation" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td class="alt1">
                                        </td>
                                        <td class="alt2">
                                        </td>
                                        <td class="alt1">
                                        </td>
                                        <td class="alt2">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                <div class="kwordcontdiv">
                                    <div style="text-align: left; width: 15%; float: left; margin-top: 10px;">
                                        Appointment Date :
                                    </div>
                                    <div style="text-align: left; width: 15%; float: left;">
                                        <asp:TextBox ID="txtDOC" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtDOC_CalendarExtender" runat="server" Enabled="True"
                                            Format="dd/MM/yyyy" PopupButtonID="imgDOC" TargetControlID="txtDOC">
                                        </cc1:CalendarExtender>
                                        <asp:Image ID="imgDOC" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit;
                                            width: 20px; height: 20px; top: 5px; left: -5px;" />
                                            <br />
                                        <asp:RequiredFieldValidator ID="req_DOC" runat="server" ControlToValidate="txtDOC"
                                            Display="Dynamic" CssClass="field required" ValidationGroup="valnew"><span class="error">
                                                        Appointment date is required!</span></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regDOC" runat="server" CssClass="field required"
                                            ValidationGroup="valnew" Display="Dynamic" ControlToValidate="txtDOC" ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                                                        <span class="error">Enter valid date!</span></asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="cvDOC" runat="server" Display="Dynamic" ControlToValidate="txtDOC"
                                            ValidationGroup="valnew" CssClass="field required" ClientValidationFunction="CallDateFun"
                                            ErrorMessage="Please enter valid date!">
                                                        <span class="error">Please enter valid date!</span></asp:CustomValidator>
                                    </div>
                                    <div class="savebutton">
                                        <asp:Button ID="btnNext" runat="server" CssClass="textbutton b_submit" Style="margin-top: 0px;"
                                            Text="Save" OnClick="btnNext_Click" ValidationGroup="valnew" />
                                    </div>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
