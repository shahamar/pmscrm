﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class ViewIPDPatientBillReport : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    string uid = "0";
    string tempDate = "";
    string tempDoctor = "";
    int Page_no = 0, Page_size = 20;
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }

        if (!IsPostBack)
        {
            //txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            //txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindGridView();
        }

        if (!(Session["Report"] as string == "{CrystalDecisions.CrystalReports.Engine.ReportDocument}"))
        {
            CrystalReportViewer2.ReportSource = (ReportDocument)Session["Report"];
        }

    }

    public static object ToDBNull(object value)
    {
        if (null != value)
            return value;
        return DBNull.Value;
    }

    private void BindGridView()
    {
        object FormDate = null;
        object ToDate = null;

        if (txtfrm.Text != "" && txtto.Text != "")
        {
            string[] frmdate = txtfrm.Text.ToString().Split('/');
            string[] todate = txtto.Text.ToString().Split('/');
            FormDate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
            ToDate = todate[2] + "/" + todate[1] + "/" + todate[0];
        }
       
        StrSQL = "Proc_GetAllIPDPatientBillDetailsAks";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@frmdate",ToDBNull(FormDate));
        SqlCmd.Parameters.AddWithValue("@todate", ToDBNull(ToDate));

        if (conn.State == ConnectionState.Closed)
        {
              conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                pnlHead.Visible = true;
                rptHead.Visible = true;
                gridIPDBill.DataSource = dt;
                gridIPDBill.DataBind();
                export.Style.Add("display", "block");
            }
            else
            {
                gridIPDBill.DataSource = null;
                gridIPDBill.DataBind();
                pnlHead.Visible = false;
                rptHead.Visible = false;
                export.Style.Add("display", "none");
            }
        }

        ScriptManager1.RegisterPostBackControl(this.imgexport);
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGridView();
       
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        CrystalReportViewer2.Visible = true;
        string AksConn = ConfigurationManager.AppSettings["constring"];
        bindReport(AksConn , 0);
    }

    public void bindReport(string AksConn , int IPDID)
    {
        DataTable dt = new DataTable();
        ReportDocument cryRpt = new ReportDocument();
        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = default(CrystalDecisions.CrystalReports.Engine.Tables);
        System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder();

        builder.ConnectionString = AksConn;

        string AksServer = builder["Server"] as string;
        string AksDatabase = builder["Database"] as string;
        string AksUsername = builder["Uid"] as string;
        string AksPassword = builder["Pwd"] as string;



        dt = GetIPDBillReport(IPDID);

        if (IPDID == 0)
        {
            cryRpt.Load(Server.MapPath("~/Reports/IPDBillPrintAks.rpt"));
        }
        else
        {
            cryRpt.Load(Server.MapPath("~/Reports/IPDPatientBill.rpt"));
        }

       



        CrTables = cryRpt.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        cryRpt.SetDataSource(dt);
        cryRpt.Refresh();

        //cryRpt.SetParameterValue("FromDate", txtfrm.Text);
        //cryRpt.SetParameterValue("ToDate", txtto.Text);

        CrystalReportViewer2.ReportSourceID = "CrystalReportSource2";
        CrystalReportViewer2.ReportSource = cryRpt;

        System.IO.Stream oStream = null;
        byte[] byteArray = null;
        oStream = cryRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        byteArray = new byte[oStream.Length];

        Session.Add("COMMPrintingData", byteArray);
        Session.Add("PrintingEventForm", "COMM_Print");

        oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1));
        Session["byteArray"] = byteArray;
        cryRpt.Close();
        cryRpt.Dispose();


    }

    public DataTable GetIPDBillReport(int IPDID)
    {
        DataTable dt = new DataTable();

        if (IPDID == 0)
        {
            StrSQL = "Proc_GetAllIPDPatientBillDetailsAks";
        }
        else
        {
            StrSQL = "Proc_GetIPDPatientBillDetailsAks";
        }

       
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;

        if (IPDID != 0)
        {
            SqlCmd.Parameters.AddWithValue("@IPDID", IPDID);
        }
       
       if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            dt.Load(dr);

        }

        return dt;


    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Convert.ToString(Session["update"]);
    }

    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {

       
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "IPDBillReport.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gridIPDBill.AllowPaging = false;
        this.BindGridView();
        gridIPDBill.Columns[15].Visible = false;
        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();


    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void gridIPDBill_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridIPDBill.PageIndex = e.NewPageIndex;
        BindGridView();
    }

    protected void gridIPDBill_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Print")
        {
            try
            {
                CrystalReportViewer2.Visible = true;
                string AksConn = ConfigurationManager.AppSettings["constring"];
                bindReport(AksConn, Convert.ToInt32(e.CommandArgument));
            }
            catch
            { }
        }

        ScriptManager1.RegisterPostBackControl(this.imgexport);
    }


}