﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class QuickRegistration : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string strSQL;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["msg"] != null)
            if (Request.QueryString["msg"] == "y")
                lblMessage.Text = "Quick Registration Done Successfully";

        if (!IsPostBack)
        {
            BindAutoCasePaperNo();
        }
    }


    public void BindAutoCasePaperNo()
    {

        DataTable dt = new DataTable();

        strSQL = "GetAutoIncCasePaperNo";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;

        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(SqlCmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            txtCasePaperNo.Text = ds.Tables[0].Rows[0]["CasePaperAutoNo"].ToString();
            //dr = SqlCmd.ExecuteReader();

            //dt.Load(dr);

            con.Close();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int CasePapercnt = Convert.ToInt32(gen.executeScalar("Select Count(pdCasePaperNo) As pdCasePaperNo from tblPatientDetails  Where IsCancel = 0  And pdCasePaperNo = '" + txtCasePaperNo.Text.ToString() + "'").ToString());
            if (CasePapercnt > 0)
            {
                string script = "alert('Case Paper No. Already Generated Kindly Enter New Case Paper No. ...!!!!');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                return;
            }

            strSQL = "InsQuickRegistration";

            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;

            SqlCmd.Parameters.Add("@QId", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.AddWithValue("@QFname", txtfname.Text.Trim().ToUpper());
            SqlCmd.Parameters.AddWithValue("@QMname", txtmname.Text.Trim().ToUpper());
            SqlCmd.Parameters.AddWithValue("@QLname", txtlname.Text.Trim().ToUpper());
            SqlCmd.Parameters.AddWithValue("@QCasePaperNo", txtCasePaperNo.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@QMobileNo", txtmob.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@QCreatedBy", userid);

            if (con.State == ConnectionState.Closed)
                con.Open();
            int reslt;
            SqlCmd.ExecuteNonQuery();
            reslt = (int)SqlCmd.Parameters["@Error"].Value;

            if (reslt == 0)
            {
                string QId = "";
                QId = SqlCmd.Parameters["@QId"].Value.ToString();

                if (txtmob.Text != "")
                {
                    String smsstring;
                    decimal TemplateId = 1207160931643569223;
                    SqlCmd = new SqlCommand();
                    smsstring = "Welcome to Aditya Homoeopathic Hospital and Healing center. Kindly Visit http://drnikam.com/OnlinePatient/PatientRegistration.aspx And Enter Details to Fill Patient Registration form. ";
                    string str21 = genral.GetResponse(txtmob.Text.Trim(), smsstring, TemplateId);

                    SqlCmd.CommandText = "sp_InsSMSTransaction";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Connection = con;
                    SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                    SqlCmd.Parameters.AddWithValue("@MobileNo", txtmob.Text.Trim());
                    SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                    SqlCmd.Parameters.AddWithValue("@SenderID", userid);
                    SqlCmd.Parameters.AddWithValue("@Pdid", QId);
                    SqlCmd.Parameters.AddWithValue("@Sources", "QuickRegistration");
                    SqlCmd.ExecuteNonQuery();
                }


                Response.Redirect("QuickRegistration.aspx?msg=y");


            }
            else
            {
                string script = "alert('Quick Registration Not Saved.');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                return;
            }


        }

    }
}