﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="NewAppointment.aspx.cs" Inherits="NewAppointment" Title="New Appointment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<script type="text/javascript">
    function Validate() {
        //alert("ok");
        var fname = document.getElementById("<%= txtfname.ClientID %>").value;
        var lname = document.getElementById("<%= txtlname.ClientID %>").value;
        var add1 = document.getElementById("<%= txtAddress1.ClientID %>").value;
        var add2 = document.getElementById("<%= txtAddress2.ClientID %>").value;
        var add2 = document.getElementById("<%= txtAddress2.ClientID %>").value;
        //alert("ok");

        var Err = "";

        if (fname == "") {
            //alert("fname_if");
            Err += "Please Fill the First Name";
            document.getElementById("<%= txtfname.ClientID %>").className = "field input-validation-error";
        }
        else {
            //alert("fname_else");
            document.getElementById("<%= txtfname.ClientID %>").className = "field";
        }

        if (lname == "") {
            //alert("lname_if");
            Err += "Please Fill the Last Name";
            document.getElementById("<%= txtlname.ClientID %>").className = "field input-validation-error";
        }
        else {
            //alert("lname_else");
            document.getElementById("<%= txtlname.ClientID %>").className = "field";
        }

        if (Err != "") {
            return false;
        }
    }
</script>
<style type="text/css">
.style1 { width: 300px;}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px"> 
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 100%;" cellspacing="0px" cellpadding="5px">
                            <span style="text-align:center;"><h2>Set Appoinment</h2></span>
                            <div class="formmenu">
                                <div class="loginform">
                                   <%-- <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">
                                            Add New Appoinment</a> </li>
                                        <li class="qtab-HTML last" id="li_9"><a class="qt_tab active" href="Confirm_Appoinment.aspx?PatType=1">
                                            Manage Appoinment</a> </li>
                                    </ul>--%>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel" style="height: auto;">
                                        <table style="width: 100%;" cellspacing="15px" cellpadding="0px">
                                            <!--<tr>
                                                <td colspan="2" style="text-align: center; color: Red;">&nbsp;</td>
                                            </tr>-->
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <!--<tr>
                                                <td colspan="2" style="text-align: center; color: Red;">&nbsp;</td>
                                            </tr>-->
                                           
                                            <tr>
                                            	<td class="style1"><strong>Initial* :</strong></td> 
                                            	<td><strong>First Name* :</strong> </td>
                                                <td><strong>Middle Name :</strong></td>
                                                <td><strong>Last Name* :</strong></td>
                                            </tr>
                                            <tr>
                                            <td class="style1" valign="top">
                                            	<asp:DropDownList ID="ddintial" runat="server" CssClass="field" Width="120px">
                                                         <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                         <asp:ListItem Text="MR" Value="MR"></asp:ListItem>
                                                         <asp:ListItem Text="MRS" Value="MRS"></asp:ListItem>
                                                         <asp:ListItem Text="MISS" Value="MISS"></asp:ListItem>
                                                         <asp:ListItem Text="MAST" Value="MAST"></asp:ListItem>
                                                         <asp:ListItem Text="BABY" Value="BABY"></asp:ListItem>
                                                         <asp:ListItem Text="DR" Value="DR"></asp:ListItem>
                                                        <asp:ListItem Text="SADHVI" Value="SADHVI"></asp:ListItem>
                                                        <asp:ListItem Text="SHREE" Value="SHREE"></asp:ListItem>
                                                        <asp:ListItem Text="ANIMAL" Value="ANIMAL"></asp:ListItem>
                                                        <asp:ListItem Text="BIRD" Value="BIRD"></asp:ListItem>
                                                        <asp:ListItem Text="SMT" Value="SMT"></asp:ListItem>
                                                    </asp:DropDownList>
                                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddintial"
                                                        Display="Dynamic" ErrorMessage="Please specify Initial." ValidationGroup="val">
                                                        <span class="error" >Please specify Initial.</span></asp:RequiredFieldValidator>--%>

                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddintial"
                                                        Display="Dynamic" ErrorMessage="Initial is required." ValidationGroup="valnew"><br />
                                                        <span class="error">Initial is required.</span>
                                                    </asp:RequiredFieldValidator>
                                                </td>
                                            	<td class="style1">
                                                    <asp:TextBox ID="txtfname" runat="server" CssClass="field required" MaxLength="50"
                                                        Style="float: left;"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="txtfname"
                                                        Display="Dynamic" ErrorMessage="First Name is required." ValidationGroup="valnew"><br />
                                                        <span class="error">First Name is required.</span>
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regFname" runat="server" CssClass="field required"
                                                        Display="Dynamic" ErrorMessage="Please enter valid name" ValidationExpression="^[a-zA-Z .\s]{0,50}$"
                                                        ControlToValidate="txtfname" ValidationGroup="valnew">
                                                             <span class="error">Please enter valid name!</span>
                                                    </asp:RegularExpressionValidator>
                                                </td>
                                                <td class="style1">
                                                    <asp:TextBox ID="txtmname" runat="server" CssClass="field" MaxLength="50"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regmname" runat="server" CssClass="field required"
                                                        ControlToValidate="txtmname" Display="Dynamic" ErrorMessage="Please enter valid name"
                                                        ValidationExpression="^[a-zA-Z .\s]{0,50}$" ValidationGroup="valnew"><br />
                                                        <span class="error">Please enter valid name!</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td class="style1">
                                                    <asp:TextBox ID="txtlname" runat="server" CssClass="field" MaxLength="50" Style="float: left;"></asp:TextBox>
                                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtlname"
                                                        Display="Dynamic" ErrorMessage="Last Name is required." ValidationGroup="valnew"><br />
                                                        <span class="error">Last Name is required.</span>
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="reg_lname" runat="server" CssClass="field required"
                                                        ControlToValidate="txtlname" Display="Dynamic" ErrorMessage="Please enter valid name"
                                                        ValidationExpression="^[a-zA-Z .\s]{0,50}$" ValidationGroup="valnew"><br />
                                                        <span class="error">Please enter valid name!</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style1"><strong>Consulting Date* : </strong></td>
                                                <td><strong>E-mail : </strong></td>
                                                <td class="style1"><strong>Address* :</strong></td>
                                            </tr>
                                            <tr>
                                                <td class="style1" valign="top">
                                                    <asp:TextBox ID="txtDOC" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txtDOC_CalendarExtender" runat="server" Enabled="True"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgDOC" TargetControlID="txtDOC">
                                                    </cc1:CalendarExtender>
                                                    <asp:Image ID="imgDOC" runat="server" ImageUrl="~/images/calimg.gif" Style="position: inherit;
                                                        width: 20px; height: 20px; top: 5px; left: -5px;" />
                                                    <asp:RequiredFieldValidator ID="req_DOC" runat="server" ControlToValidate="txtDOC"
                                                        Display="Dynamic" CssClass="field required" ValidationGroup="valnew"><br />
                                                        <span class="error">
                                                        Consulting date is required!</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regDOC" runat="server" CssClass="field required"
                                                        ValidationGroup="valnew" Display="Dynamic" ControlToValidate="txtDOC" ValidationExpression="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$">
                                                        <span class="error">Enter valid date!</span></asp:RegularExpressionValidator>
                                                    <asp:CustomValidator ID="cvDOC" runat="server" Display="Dynamic" ControlToValidate="txtDOC"
                                                        ValidationGroup="valnew" CssClass="field required" ClientValidationFunction="CallDateFun"
                                                        ErrorMessage="Please enter valid date!">
                                                        <span class="error">Please enter valid date!</span></asp:CustomValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtemail" runat="server" CssClass="field required" MaxLength="50"
                                                        Style="float: left"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regEmailID1" runat="server" ControlToValidate="txtemail"
                                                        CssClass="field required" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
                                                        ValidationGroup="valnew">
                                                        <span class="error">Please enter valid email Id</span>
                                                    </asp:RegularExpressionValidator>
                                                    <asp:RegularExpressionValidator ID="reg_EmailId1" runat="server" ControlToValidate="txtemail"
                                                        CssClass="field required" ValidationExpression="^[\s\S]{0,50}$" ValidationGroup="valnew">
                                                        <span class="error">Max 50 characters allowed</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td class="style1">
                                                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="required field" MaxLength="50" TextMode="MultiLine"
                                                        Style="float: left;" Width="250px"></asp:TextBox>

                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddress1"
                                                        Display="Dynamic" ErrorMessage="Address is required." ValidationGroup="valnew">
                                                        <span class="error">Address is required.</span>
                                                    </asp:RequiredFieldValidator>

                                                    <asp:RegularExpressionValidator ID="regAddress1" runat="server" Display="Dynamic"
                                                        CssClass="field required" ControlToValidate="txtAddress1" ErrorMessage="Please enter valid address!"
                                                        ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,150}$" ValidationGroup="valnew">
                                                    <span class="error">Please enter valid address!</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td style="display:none">
                                                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="required field" MaxLength="50"
                                                        Style="float: left;" Width="300px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regAddress2" runat="server" Display="Dynamic"
                                                        CssClass="field required" ControlToValidate="txtAddress2" ErrorMessage="Please enter valid address!"
                                                        ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid address!</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                           
                                           
                                            <tr style="display:none">
                                                <td class="style1">Address </td>
                                                <td>Address</td>
                                            </tr>
                                            <tr  style="display:none">
                                                <td class="style1">
                                                    <asp:TextBox ID="txtAddress3" runat="server" CssClass="field" MaxLength="50" Width="300px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regAddress3" runat="server" Display="Dynamic"
                                                        CssClass="field required" ControlToValidate="txtAddress3" ErrorMessage="Please enter valid address!"
                                                        ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid address!</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddress4" runat="server" CssClass="field" MaxLength="50" Width="300px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regAddress4" runat="server" Display="Dynamic"
                                                        CssClass="field required" ControlToValidate="txtAddress4" ErrorMessage="Please enter valid address!"
                                                        ValidationExpression="^[a-zA-Z0-9 -.@&,/\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid address!</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style1"><strong>Contact No* :</strong></td>                                                
                                            </tr>
                                            <tr>
                                                <td class="style1" colspan="2">
                                                  <img src="images/mobile_phone.png" /> &nbsp;
                                                    <asp:TextBox ID="txtmob" runat="server" CssClass="field" MaxLength="20" Width="200px"></asp:TextBox>
                                                     &nbsp; &nbsp;
                                                    <img src="images/phone.png" />&nbsp; &nbsp;<asp:TextBox ID="txttele" runat="server" CssClass="field"
                                                        MaxLength="20" Width="200px"></asp:TextBox>&nbsp;<br />
                                                    <asp:RegularExpressionValidator ID="regtele" runat="server" Display="Dynamic" CssClass="field required"
                                                        ControlToValidate="txttele" ErrorMessage="Please enter valid telephone number!"
                                                        ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid telephone number!</span></asp:RegularExpressionValidator>
                                                    <asp:RegularExpressionValidator ID="regmob" runat="server" Display="Dynamic" CssClass="field required"
                                                        Style="padding-left: 140px;" ControlToValidate="txtmob" ErrorMessage="Please enter valid mobile number!"
                                                        ValidationExpression="^[0-9 \s]{0,15}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid mobile number!</span></asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtmob"
                                                        Display="Dynamic" ErrorMessage="Contact No. is required." ValidationGroup="valnew">
                                                        <span class="error">Contact No. is required.</span>
                                                    </asp:RequiredFieldValidator>                                                     
                                                </td>    
                                                <td style="display:none">
                                                    <asp:TextBox ID="txtWeight" runat="server" CssClass="field" Width="70px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regWeight" runat="server" CssClass="field required"
                                                    ControlToValidate="txtWeight" Display="Dynamic" ErrorMessage="Please enter valid weight"
                                                    ValidationExpression="^[a-zA-Z0-9 .\s]{0,50}$" ValidationGroup="val">
                                                    <span class="error">Please enter valid weight!</span></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                                                                     
                                            <tr>
                                                <td class="style1" colspan="4" align="center">
                                                    <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                    Style="margin-top: 2px;" Text="Save" ValidationGroup="valnew" />
                                                </td>                                               
                                            </tr>
                                            
                                        </table>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
