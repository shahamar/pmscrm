﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class UpdateCourierDetails : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string userrole;
    string strSQL;
    int CID = 0;
    public string IsMsgSent;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["cmId"] != null)
            CID = Convert.ToInt32(Request.QueryString["cmId"].ToString());

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 36";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            FillData();
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string[] _doc = txtCourierDate.Text.ToString().Split('/');
        string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];


        if (CID != 0)
            strSQL = "sp_UpdateCourierDetails";

        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@cmdName", txtCName.Text.Trim());
        SqlCmd.Parameters.AddWithValue("@cmdAddress", txtCAddress.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdMobileNo", txtCMobileNo.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdEmailId ", txtcEmailID.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmdDate", doc);
        SqlCmd.Parameters.AddWithValue("@cmCreatedBy", userid);
        SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
        SqlCmd.Parameters.AddWithValue("@cmdDocketNo", txtCDocketNo.Text.ToString());
        //SqlCmd.Parameters.AddWithValue("@cmdCharges", txtCCharges.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@cmID", DDCourier.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@cmdID", CID);
        if (txtCDocketNo.Text != "")
        {
            SqlCmd.Parameters.AddWithValue("@IsMsgSent", 1);
        }
        else
        {
            SqlCmd.Parameters.AddWithValue("@IsMsgSent", 0);
        }

        if (con.State == ConnectionState.Closed)
            con.Open();
        int reslt;
        SqlCmd.ExecuteNonQuery();
        reslt = (int)SqlCmd.Parameters["@Error"].Value;
        if (reslt == 0)
        {
            if (txtCDocketNo.Text != "" && txtCDocketNo.Text != "0000")
            {
                if (txtCDocketNo.Text.Trim() != hidDocketNo.Value)
                {
                    if (txtCMobileNo.Text != "")
                    {
                        String smsstring;

                        decimal TemplateId = 1207160931676032697;
                        SqlCmd = new SqlCommand();
                        smsstring = "" + txtCName.Text.ToString() + " Your medicine has been dispatched from Aditya Homoeopathic Hospital on " + txtCourierDate.Text.ToString() + " via " + DDCourier.SelectedItem.Text + " courier with tracking (CN) number " + txtCDocketNo.Text + ". For any queries contact non tariff number 9923294959.. visit us at www.drnikam.com";
                        genral.GetResponse(txtCMobileNo.Text.Trim(), smsstring, TemplateId);
                        //genral.GetResponse("9821479720", smsstring);

                        SqlCmd.CommandText = "sp_insSmsTransaction";
                        SqlCmd.CommandType = CommandType.StoredProcedure;
                        SqlCmd.Connection = con;
                        SqlCmd.Parameters.AddWithValue("@Msg", smsstring);
                        SqlCmd.Parameters.AddWithValue("@MobileNo", txtCMobileNo.Text.Trim());
                        SqlCmd.Parameters.AddWithValue("@EventDate", DateTime.Now);
                        SqlCmd.Parameters.AddWithValue("@SenderID", userid);
                        SqlCmd.Parameters.AddWithValue("@Pdid", CID);
                        SqlCmd.Parameters.AddWithValue("@Sources", "Courier");
                        SqlCmd.ExecuteNonQuery();

                    }
                }
            }
            Response.Redirect("CourierView.aspx?msg=Courier details updated successfully");
        }

        SqlCmd.Dispose();
        con.Close();
    }

    private void FillData()
    {
        if (CID != 0)
        {

            DDCourier.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("Select cmID,cmCourierName From tblCourierMaster", "cmID", "cmCourierName", DDCourier);



            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "Select * From tblCourierMedicinsDetails cmd inner join tblCourierMaster cm on cm.cmID=cmd.cmID Where cmdID= " + CID + "";
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader sqldr = SqlCmd.ExecuteReader();
            while (sqldr.Read())
            {
                txtCName.Text = sqldr["cmdName"].ToString();
                txtCAddress.Text = sqldr["cmdAddress"].ToString();
                txtCMobileNo.Text = sqldr["cmdMobileNo"].ToString();
                txtcEmailID.Text = sqldr["cmdEmailId"].ToString();
                txtCourierDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(sqldr["cmdDate"].ToString()));
                DDCourier.SelectedValue = sqldr["cmID"].ToString();
                txtCDocketNo.Text = sqldr["cmdDocketNo"].ToString();
                txtCCharges.Text = sqldr["cmdCharges"].ToString();
                lblmsg.Text = sqldr["IsMsgSent"].ToString();
                hidDocketNo.Value = sqldr["cmdDocketNo"].ToString();
            }

            sqldr.Close();
            sqldr.Dispose();
            con.Close();
            con.Dispose();
        }
    }
}
