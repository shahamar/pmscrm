﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class TestMaster : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string userid;
    string userrole;
    string strSQL;
    int tmId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["userid"] != null)
            userid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["tmId"] != null)
            tmId = Convert.ToInt32(Request.QueryString["tmId"].ToString());

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 8";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["flag"] == "1")
            lblMessage.Text = "Test details saved successfully";

        if (!IsPostBack)
        {
            txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();

        }
    }
    private void FillData()
    {
        if (tmId != 0)
        {


            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "Select * from tblTestMaster Where tmId= " + tmId + " ";
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader sqldr = SqlCmd.ExecuteReader();
            while (sqldr.Read())
            {
                txtTestName.Text = sqldr["tmTestName"].ToString();
                txtCharges.Text = sqldr["tmBaseCharges"].ToString();
            }

            sqldr.Close();
            sqldr.Dispose();
            con.Close();
            con.Dispose();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string[] _sdate = txtStartDate.Text.ToString().Split('/');
            string sdate = _sdate[2] + "/" + _sdate[1] + "/" + _sdate[0];

            if (tmId == 0)
                strSQL = "InsTestMaster";
            else
                strSQL = "UptTestMaster";

            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@tmTestName", txtTestName.Text.Trim());
            SqlCmd.Parameters.AddWithValue("@tmBaseCharges", txtCharges.Text.ToString());
            SqlCmd.Parameters.AddWithValue("@StartDate", sdate);
            SqlCmd.Parameters.AddWithValue("@tmCreatedBy", userid);
            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            if (tmId == 0)
            {
                SqlCmd.Parameters.Add("@tmId", SqlDbType.Int).Direction = ParameterDirection.Output;

            }
            else
            {
                SqlCmd.Parameters.AddWithValue("@tmId", tmId);
            }
            if (con.State == ConnectionState.Closed)
                con.Open();
            int reslt;
            SqlCmd.ExecuteNonQuery();
            reslt = (int)SqlCmd.Parameters["@Error"].Value;
            if (reslt == 0)
            {
                if (tmId == 0)
                {
                    Response.Redirect("TestMaster.aspx?flag=1", false);
                }
                else
                {
                    Response.Redirect("ManageTest.aspx?flag=1", false);
                }
            }

            SqlCmd.Dispose();
            con.Close();

        }
    }
}
