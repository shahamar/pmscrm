﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class AddIPDRemedy : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    //string pdId = "";
    string uid = "";
    //string ipdid = "";
    //added by nikita on 12th march 2015------------------------------
    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            else
                Response.Redirect("IPDView.aspx");

            txtSDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            DDL_RemList.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("Select * From tblRemedyMaster where isDelete = 0 AND RMNAME != '0' ORDER BY REPLACE(RMNAME,' ','') ASC", "RMID", "RMNAME", DDL_RemList);

        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string sdate = gen.getdate(txtSDate.Text.ToString(), '/');

            strSQL = "InsIpdRemedy";
            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@Remedy", DDL_RemList.SelectedValue);
            SqlCmd.Parameters.AddWithValue("@SDate", sdate);
            SqlCmd.Parameters.AddWithValue("@CreatedBy", uid);
            SqlCmd.Parameters.AddWithValue("@iripdId", ipdid);
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            SqlCmd.Dispose();
            con.Close();
            con.Dispose();
            Response.Redirect("IPDView.aspx", false);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
