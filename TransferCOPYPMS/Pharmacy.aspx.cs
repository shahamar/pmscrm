﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Drawing;

public partial class Pharmacy : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string RepType = "";
    string frm, to;
    int i = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            Bind_FollowUpPaymentDetails();
            Bind_FollowUpPaymentDetails_ExtraDoses();
        }
    }

    private void Bind_FollowUpPaymentDetails()
    {
        string[] _frm;
        string[] _to;


        if (txtfrm.Text.Contains("/"))
        {
            _frm = txtfrm.Text.ToString().Split('/');
            _to = txtto.Text.ToString().Split('/');

            frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";
        }

        if (txtfrm.Text.Contains("-"))
        {
            _frm = txtfrm.Text.ToString().Split('-');
            _to = txtto.Text.ToString().Split('-');

            frm = _frm[2] + "-" + _frm[1] + "-" + _frm[0] + " 00:00:00";
            to = _to[2] + "-" + _to[1] + "-" + _to[0] + " 23:55:00";
        }

        StrSQL = "Proc_GetFollowupPharmacyDetails";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FrmDate", frm);
        SqlCmd.Parameters.AddWithValue("@ToDate", to);

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_FollowupPaymentDetails.DataSource = dt;
                GVD_FollowupPaymentDetails.DataBind();
            }
            else
            {
                GVD_FollowupPaymentDetails.DataSource = null;
                GVD_FollowupPaymentDetails.DataBind();
            }
            conn.Close();
            dr.Dispose();
        }
    }

    private void Bind_FollowUpPaymentDetails_ExtraDoses()
    {
        string[] _frm;
        string[] _to;


        if (txtfrm.Text.Contains("/"))
        {
            _frm = txtfrm.Text.ToString().Split('/');
            _to = txtto.Text.ToString().Split('/');

            frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";
        }

        if (txtfrm.Text.Contains("-"))
        {
            _frm = txtfrm.Text.ToString().Split('-');
            _to = txtto.Text.ToString().Split('-');

            frm = _frm[2] + "-" + _frm[1] + "-" + _frm[0] + " 00:00:00";
            to = _to[2] + "-" + _to[1] + "-" + _to[0] + " 23:55:00";
        }

        StrSQL = "Proc_GetExtraDosesPharmacyDetails";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@FrmDate", frm);
        SqlCmd.Parameters.AddWithValue("@ToDate", to);

        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                GVD_PharmacyExtraDoses.DataSource = dt;
                GVD_PharmacyExtraDoses.DataBind();
            }
            else
            {
                GVD_PharmacyExtraDoses.DataSource = null;
                GVD_PharmacyExtraDoses.DataBind();
            }
            conn.Close();
            dr.Dispose();
        }
    }


    protected void btnView_Click(object sender, EventArgs e)
    {
        Bind_FollowUpPaymentDetails();
        Bind_FollowUpPaymentDetails_ExtraDoses();
    }

    protected void GVD_FollowupPaymentDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "Pharmacy")
        {
            try
            {
              
                SqlCmd = new SqlCommand();
                SqlCmd.CommandText = "proc_SaveFollowupPharmacyDetails";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlCmd.Parameters.AddWithValue("@FDID", e.CommandArgument);
                SqlCmd.Parameters.AddWithValue("@PaidBy", uid);
                i = SqlCmd.ExecuteNonQuery();
                con.Close();
                SqlCmd.Dispose();
                if (i > 0)
                {
                    lblMessage.Text = "Pharmacy Details Saved Successfully";
                    lblMessage.ForeColor = Color.Green;
                    Bind_FollowUpPaymentDetails();
                }
                else
                {
                    lblMessage.Text = "Pharmacy Details Not Saved....!!!!!";
                    lblMessage.ForeColor = Color.DarkRed;
                }
		
            }
            catch
            { }
        }


    }

    protected void GVD_PharmacyExtraDoses_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Pharmacy")
        {
            try
            {

                SqlCmd = new SqlCommand();
                SqlCmd.CommandText = "proc_SaveFollowupPharmacyDetails";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlCmd.Parameters.AddWithValue("@FDID", e.CommandArgument);
                SqlCmd.Parameters.AddWithValue("@PaidBy", uid);
                i = SqlCmd.ExecuteNonQuery();
                con.Close();
                SqlCmd.Dispose();
                if (i > 0)
                {
                    lblMessage.Text = "Pharmacy Details Saved Successfully";
                    lblMessage.ForeColor = Color.Green;
                    Bind_FollowUpPaymentDetails_ExtraDoses();
                }
                else
                {
                    lblMessage.Text = "Pharmacy Details Not Saved....!!!!!";
                    lblMessage.ForeColor = Color.DarkRed;
                }

            }
            catch
            { }
        }


    }

    protected void GVD_FollowupPaymentDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVD_FollowupPaymentDetails.PageIndex = e.NewPageIndex;
        Bind_FollowUpPaymentDetails();
    }

    protected void GVD_PharmacyExtraDoses_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVD_PharmacyExtraDoses.PageIndex = e.NewPageIndex;
        Bind_FollowUpPaymentDetails_ExtraDoses();
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
	Bind_FollowUpPaymentDetails();
    }

    protected void Timer2_Tick(object sender, EventArgs e)
    {
        Bind_FollowUpPaymentDetails_ExtraDoses();
    }

    protected void chk_ExtraDoses1_CheckedChanged(object sender, EventArgs e)
    {
        SaveExtraDosesStatus(sender, 1);
    }

    protected void chk_ExtraDoses2_CheckedChanged(object sender, EventArgs e)
    {
        SaveExtraDosesStatus(sender, 2);
    }

    protected void chk_ExtraDoses3_CheckedChanged(object sender, EventArgs e)
    {
        SaveExtraDosesStatus(sender, 3);
    }

    protected void chk_ExtraDoses4_CheckedChanged(object sender, EventArgs e)
    {
        SaveExtraDosesStatus(sender, 4);
    }

    protected void chk_ExtraDoses5_CheckedChanged(object sender, EventArgs e)
    {
        SaveExtraDosesStatus(sender, 5);
    }

    public void SaveExtraDosesStatus(object sender, int Flag)
    {
        try
        {
            CheckBox cb = (CheckBox)sender;
            GridViewRow gr = (GridViewRow)cb.NamingContainer;

            string lblID = "lbl_ExtraDoses" + Flag;

            Label lblIDVal = gr.FindControl(lblID) as Label;

            int Value = 0;

            if (cb.Checked == true)
            {
                Value = 1;
            }

            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_SavePharmacyExtraDosesDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd.Parameters.AddWithValue("@FDID", lblIDVal.Text);
            SqlCmd.Parameters.AddWithValue("@Flag", Flag);
            SqlCmd.Parameters.AddWithValue("@Value", Value);
            i = SqlCmd.ExecuteNonQuery();
            con.Close();
            SqlCmd.Dispose();
            if (i > 0)
            {
                lblMessage.Text = "Extra Doses Details Saved Successfully";
                lblMessage.ForeColor = Color.Green;
                Bind_FollowUpPaymentDetails_ExtraDoses();
            }
            else
            {
                lblMessage.Text = "Extra Doses Details Not Saved....!!!!!";
                lblMessage.ForeColor = Color.DarkRed;
            }
        }
        catch
        { }

    }

}