﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterHome1.master" CodeFile="MissingFollowUp.aspx.cs" Inherits="MissingFollowUp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <link rel="stylesheet" type="text/css" href="css/radiotabstrip.css" />
    <style>
        #tbsearch td {
            text-align: left;
            vertical-align: middle;
        }

        #tbsearch label {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }

        #tbhead th {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }

        #gvcol div {
            margin-top: -10px;
        }

        .head1 {
            width: 10%;
        }

        .head2 {
            width: 18%;
        }

        .head3 {
            width: 9%;
        }

        .head4 {
            width: 10%;
        }

        .head4i {
            width: 18%;
        }

        .head5 {
            width: 10%;
        }

        .head6 {
            width: 10%;
        }

        .head7 {
            width: 18%;
        }

        .head8 {
            width: 5%;
            text-align: center;
        }

        .textbox {
            width: 70px;
            padding: 4px;
        }

        .datetime {
            position: absolute;
            width: 23px;
            height: 28px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table width="100%" cellspacing="5px" cellpadding="2px">
        <tr>
            <td width="100%">
                <table style="width: 100%; border: 1px solid #EFEFEF;" cellspacing="5px" cellpadding="5px">
                    <tr>
                        <td class="tableh1" width="100%">
                            <table id="tbsearch" style="width: 100%;">
                                <tr>
                                      <td>Patient Name : 
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtname" runat="server" CssClass="textbox">
                                        </asp:TextBox>
                                    </td>
                                    <td>Case Paper No. :
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcasepaperno" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>

                                    <td style="width: 5%">
                                        <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                            Style="padding: 3px;" OnClick="btnView_Click" />
                                    </td>

                                   <%-- <td>Follow Up From : 
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox">
                                        </asp:TextBox>
                                        <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                            runat="server" Enabled="True" TargetControlID="txtfrm">
                                        </cc1:CalendarExtender>
                                        &nbsp;
                                                                                <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                    </td>
                                    <td>To :
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtto" runat="server" CssClass="textbox"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True"
                                            TargetControlID="txtto" Format="dd/MM/yyyy" PopupButtonID="imgto">
                                        </cc1:CalendarExtender>
                                        &nbsp;
                                                                                <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                    </td>

                                    <td style="width: 5%">
                                        <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                            Style="padding: 3px;" OnClick="btnView_Click" />
                                    </td>--%>
                                    <td style="width: 5%">&nbsp;</td>
                                    <td style="width: 2%">
                                        <div id="export" runat="server" style="float: right; display: block;">
                                            <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" Height="27" OnClick="imgexport_Click" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="11">
                                        <table width="100%">
                                            <tr>


                                                <td style="color: Red; text-align: right;">
                                                    <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlHead" runat="server">
        <div id="rptHead" runat="server">
            <table width="100%" id="rounded-corner-report">
                <tr>
                    <td align="center">
                                                    Aditya Homoeopathic Hospital & Healing Center<br />
                                                    Pimpri Gaon, Pune- 411 017
                                                </td>
                </tr>
                <tr>
                    <td align="center" class="rptname">Missing OPD Report</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td id="gvcol">
                        <asp:GridView runat="server" ID="GRV1" AutoGenerateColumns="false" CssClass="mGrid" AllowPaging="True" AllowSorting="true"
                            PagerStyle-CssClass="pgr" PageSize="20"
                            Width="100%" OnPageIndexChanging="GRV1_PageIndexChanging">
                            <Columns>

                                <asp:TemplateField HeaderText="Patient Name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblPatient" Text='<%#Eval("PdName")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Case Paper No." ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%#Eval("pdCasePaperNo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date of Joining" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%#Eval("pdDOC")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Next Followup Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblnextfollowup" Text='<%#Eval("fdNextFollowupDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Followup Age in Days" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%#Eval("FollowupAgeInDays")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Followup Age in Months" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%#Eval("FollowupAgeInMonths")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Patient Age" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%#Eval("PdAge")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Kword" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%#Eval("kword")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>

