﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="ViewIPDPatientBillReport.aspx.cs" Inherits="ViewIPDPatientBillReport" EnableEventValidation="false" %>

  <%--     <%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

 <%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<style>
.mGrid { 
    width: 100%; 
    background-color: #eee; 
    margin: 5px 0 10px 0; 
    border: solid 1px #525252; 
    border-collapse:collapse; 
    
}
</style>
<script type="text/javascript">

    function ConfirmPrint(FormDep) {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Do you want to Print?")) {

            confirm_value.value = "Yes";

            if (FormDep == "COMM_Print")
            { OPDPrintFunction(); }

            else {
                confirm_value.value = "No";
            }
        }
        document.forms[0].appendChild(confirm_value);
    }


    function OPDPrintFunction() {


        setTimeout('DelayOPDPrintFunction()', 200);

    }

    function DelayOPDPrintFunction() {
        var myWindow = window.open("PrintReport.aspx");
        myWindow.focus();
        myWindow.print();
    }

</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 	
       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server"> 
        <ContentTemplate> 
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td></td>
                </tr>       
                <tr>
                    <td> 
                        <div class="login-area margin_ten_right" style="width: 100%;text-align:center"> 
                            <h2>IPD Bill Report</h2>
                            <div class="formmenu"> 
                                <div class="loginform"> 
                                   
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"                               
                                        style="height: auto; border: 1.5px solid #E2E2E2;">                                            
                                        <table width="100%" cellspacing="5px" cellpadding="2px">
                                            <tr> 
                                                <td width="100%"> 
                                                    <table style="width: 100%; border: 1px solid #EFEFEF;" cellspacing="5px" cellpadding="5px">
                                                        <tr>
                                                            <td class="tableh1" width="100%">
                                                                <table id="tbsearch" style="width: 100%;">

                                                                <tr>
                                                                 <td align="right" width="20%">Admission Date From :</td>
                                                                        <td align="center" width="8%">
                                                                            <asp:TextBox ID="txtfrm" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                                runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                width: 20px; height: 20px;" />
                                                                        </td>
                                                                        <td align="center" width="5%">To :</td>
                                                                        <td  align="left" width="10%">
                                                                            <asp:TextBox ID="txtto" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" 
                                                                            TargetControlID="txtto" Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                width: 20px; height: 20px;" />
                                                                        </td>
                                                               <!-- </tr>
                                                                    <tr> -->                                                                   
                                                                           <td width="15%">    
                                                                                                                                               
                                                                                <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit center"
                                                                                OnClick="btnView_Click" style="margin-right:5px" /> &nbsp;&nbsp;

                                                                                 <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="textbutton b_submit center"
                                                                                 OnClick="btnPrint_Click" OnClientClick="ConfirmPrint('COMM_Print');" />   
    
                                                                                 <div id="export" runat="server" style="float: right; display: none;">
                                                                                  <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" Height="27"
                                                                                  OnClick="imgexport_Click" />
                                                                                 </div>

                                                                        	</td>
                                                                        </tr>                                                          
                                                                </table>    
                                                            </td>
                                                        </tr>
                                                     </table>
                                                </td> 
                                            </tr>
                                         </table>
                                        <asp:Panel ID="pnlHead" runat="server" Visible="false">
                                        <div id="rptHead" runat="server" visible="false">
                                            <table width="100%" id="rounded-corner-report">
                                               <tr>
                                                    <td id="gvcol">
                                                        <asp:GridView ID="gridIPDBill"  Width="100%" runat="server" AutoGenerateColumns="false" OnPageIndexChanging="gridIPDBill_PageIndexChanging"
                                                CssClass="mGrid" AllowPaging="true" PageSize="20"  OnRowCommand="gridIPDBill_RowCommand">
						<AlternatingRowStyle BackColor="White" />
                                            <Columns>

                                 <asp:TemplateField HeaderText="pdid" Visible="false">
                                 <ItemTemplate>
                                      <asp:Label ID="lblipdId" runat="server" Text='<% #Eval("ipdId") %>'></asp:Label>  
                                      <asp:Label ID="lblpdID" runat="server" Text='<% #Eval("pdID") %>'></asp:Label>  
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Ward No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblwmWardNo" runat="server" Text='<%# Eval("wmWardNo")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

				  <asp:TemplateField HeaderText="Bed No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
					<ItemTemplate>
						<asp:Label ID="lblwBedNo" runat="server" Text='<%# Eval("wBedNo")%>'></asp:Label>
					</ItemTemplate>
				  </asp:TemplateField>

                                 <asp:TemplateField HeaderText="Patient Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblName" runat="server" Text='<%# Eval("PatientName")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Balance" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblBalance" runat="server" Text='<%# Eval("BalanceAmt")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("FStatus").ToString())%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblTotalBillAmt" runat="server" Text='<%# Eval("TotalBillAmt")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Advance" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("DepositAmt")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>


                                 <asp:TemplateField HeaderText="Ward Type (Per Day Chages)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false">
                                 <ItemTemplate>
                                      <asp:Label ID="lblWardTypeDetails" runat="server" Text='<%# Eval("WardTypeDetails")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Adm Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblwInDate" Text='<%#Eval("ipdAdmissionDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

				<asp:TemplateField HeaderText="Test Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                 <ItemTemplate>
                                      <asp:Label ID="lblTest_Name" runat="server" Text='<%# Eval("Test_Name")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Test Charges" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblTest_Name_TotalCharges" runat="server" Text='<%# Eval("Test_Name_TotalCharges")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Out Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblwOutDate" Text='<%#Eval("wOutDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                                  <asp:TemplateField HeaderText="Total Days" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblWardDayCount" runat="server" Text='<%# Eval("WardDayCount")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Ward Charges" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblWardDayTotalCharges" runat="server" Text='<%# Eval("WardDayTotalCharges")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Extra Particular (Charges) - Days" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblEP_NAME_Details" runat="server" Text='<%# Eval("EP_NAME_Details")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                 <asp:TemplateField HeaderText="Extra Particular Charges" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                 <ItemTemplate>
                                 <asp:Label ID="lblEP_NAME_TotalCharges" runat="server" Text='<%# Eval("EP_NAME_TotalCharges")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>


                                <%-- <asp:TemplateField HeaderText="Test Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                 <ItemTemplate>
                                      <asp:Label ID="lblTest_Name" runat="server" Text='<%# Eval("Test_Name")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Test Charges" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                 <ItemTemplate>
                                      <asp:Label ID="lblTest_Name_TotalCharges" runat="server" Text='<%# Eval("Test_Name_TotalCharges")%>'></asp:Label>     
                                 </ItemTemplate>
                                 </asp:TemplateField> --%>

                                    <asp:TemplateField HeaderText="Print" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="btnOPD" runat="server" CommandName="Print" CommandArgument='<%# Eval("ipdId") %>'
                                                                            ImageUrl="~/images/print1.png" ToolTip="Print Details" OnClientClick="ConfirmPrint('COMM_Print');"/>
                                                                    </ItemTemplate> 
                                                                 </asp:TemplateField>

                                             </Columns>
                                            </asp:GridView>
                                                    </td>
                                            	</tr>
                                        	</table>
                                        </div>
                                        </asp:Panel>
                                    </div> 
                                   <div class="clr"></div>
                                </div> 
                            </div> 
                        </div> 
                    </td> 
                </tr> 
                <tr>
                    <td></td>
                </tr>
            </table>
            
            <asp:Panel BorderStyle="Outset"  BorderWidth="5px" BorderColor="#C4D4B6" ID="PNLWorkSheet" runat="server" Visible="false">                                   
            <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right"  ImageUrl="~/Images/FancyClose.png" />
            <asp:Panel ID="pnl1" runat="server" ScrollBars="Auto" Width="945px">
               <CR:CrystalReportViewer ID="CrystalReportViewer2" runat="server" AutoDataBind="True" Height="500px"
               EnableParameterPrompt="False" ReuseParameterValuesOnRefresh="True" ToolPanelView="None" GroupTreeImagesFolderUrl=""
               ToolbarImagesFolderUrl="" ToolPanelWidth="200px" EnableDatabaseLogonPrompt="False" />
               <CR:CrystalReportSource ID="CrystalReportSource2" runat="server">
                  <Report FileName="~/Reports/OpdReportAks.rpt"></Report>
               </CR:CrystalReportSource>
           </asp:Panel>
           </asp:Panel> 
        </ContentTemplate> 
    </asp:UpdatePanel> 
</asp:Content>
