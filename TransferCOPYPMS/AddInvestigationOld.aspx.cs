﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class AddInvestigation : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    int userid;
    string userrole;
    string strSQL;
    int InvestID;
    int Page_no = 0, Page_size = 10;

    public string BloodTestFile
    {
        get
        {
            if (ViewState["BloodTestFile"] == null)
                return "";
            else
                return ViewState["BloodTestFile"].ToString();
        }
        set
        {
            ViewState["BloodTestFile"] = value;
        }
    }

    public string UrineTestFile
    {
        get
        {
            if (ViewState["UrineTestFile"] == null)
                return "";
            else
                return ViewState["UrineTestFile"].ToString();
        }
        set
        {
            ViewState["UrineTestFile"] = value;
        }
    }

    public string Monitor
    {
        get
        {
            if (ViewState["Monitor"] == null)
                return "";
            else
                return ViewState["Monitor"].ToString();
        }
        set
        {
            ViewState["Monitor"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 23";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();
        if (!IsPostBack)
        {
            //FillData();

            if (ViewState["BloodTestFile"] != null)
                ViewState["BloodTestFile"] = null;

            if (ViewState["UrineTestFile"] != null)
                ViewState["UrineTestFile"] = null;

            if (ViewState["Monitor"] != null)
                ViewState["Monitor"] = null;
        }

        //Up2.Visible = false;
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        //BindInvest();
        //Up2.Visible = true;
        //btnSave.Visible = true;
    }
    protected void FillData()
    {
        DDWardType.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT Distinct(wmType) FROM tblWardMaster", "wmType", "wmType", DDWardType);
    }

    protected void DDWardType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddWardNo.Items.Clear();
        ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT * FROM tblWardMaster WHERE wmType='" + DDWardType.SelectedValue + "' order by wmWardNo asc", "wmID", "wmWardNo", ddWardNo);

    }

    public void BindInvest()
    {
        try
        {
            ddPatientName.Items.Clear();
            ddPatientName.Items.Insert(0, new ListItem("Select", ""));

            strSQL = "DECLARE @TempOccupiedBed TABLE (BedNo int,WID int,WNO varchar(30),WIPDID int,InDate smalldatetime) insert into @TempOccupiedBed select distinct wBedNo,max(wid),wNo,wipdId,wInDate from tblPatientsWardDetails where wOutDate is  null group by wNo,wBedNo,wipdId,wInDate select t.BedNo,WID,WNO,InDate, pdInitial +'  '+pdFname +'  '+pdMname +'  '+ pdLname AS 'PName',WIPDID,p.pdID,Complaint,Remedy,BpMin,BpMax from @TempOccupiedBed t inner join tblIpdMaster i on t.WIPDID =i.ipdid inner join tblPatientDetails p on i.ipdpdId  =p.pdID  left join (SELECT * FROM dbo.tblInvestigationDetails WHERE flag=0) invest on invest.PatientID=p.pdID    where 1=1";
            strSQL += " AND WNO = " + ddWardNo.SelectedItem.Text + "";
            strSQL += " order by WNO,BedNo asc";

            DataTable dt = gen.getDataTable(strSQL);

            gen.FillDropDown(ddPatientName, dt, "pdID", "PName");
        }
        catch (Exception ex)
        {

        }
    }

    //protected void btnSave_Click(object sender, EventArgs e)
    //{

    //    strSQL = "sp_InsInvestigationDetails";

    //    SqlCmd = new SqlCommand(strSQL, con);
    //    SqlCmd.CommandType = CommandType.StoredProcedure;
    //    SqlCmd.Parameters.AddWithValue("@VisitTime", ddlVisitTime.SelectedValue);
    //    SqlCmd.Parameters.AddWithValue("@wmID", ddWardNo.SelectedValue);
    //    SqlCmd.Parameters.AddWithValue("@xmlString1", getXmlString());
    //    SqlCmd.Parameters.AddWithValue("@CreatedBy", userid);
    //    SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
    //    if (InvestID != 0)
    //    {
    //        //SqlCmd.Parameters.AddWithValue("@cmID", CID);
    //    }

    //    if (con.State == ConnectionState.Closed)
    //        con.Open();
    //    int reslt;
    //    SqlCmd.ExecuteNonQuery();
    //    reslt = (int)SqlCmd.Parameters["@Error"].Value;

    //    Up2.Visible = true;
    //    BindInvest();
    //    lblMessage.Text = "Investigation details saved successfully.";
    //    SqlCmd.Dispose();

    //    con.Close();
    //}

    //public string getXmlString()
    //{
    //    string XMLString = "<ROOT>";
    //    foreach (GridViewRow grvrow in GRV1.Rows)
    //    {
    //        HtmlInputCheckBox chk = (HtmlInputCheckBox)grvrow.FindControl("cbAC");
    //        Label lblBedNo = (grvrow.FindControl("lblBedNo") as Label);
    //        Label lblpdID = (grvrow.FindControl("lblpdID") as Label);
    //        TextBox lblRemark = (grvrow.FindControl("txtRemark") as TextBox);
    //        TextBox lblRemedy = (grvrow.FindControl("txtRemedy") as TextBox);
    //        TextBox lblSys = (grvrow.FindControl("txtSys") as TextBox);
    //        TextBox lblDis = (grvrow.FindControl("txtDis") as TextBox);

    //        if (chk.Checked == true)
    //        {
    //            XMLString = XMLString + "<OCS BedNo='" + lblBedNo.Text.ToString() + "' pdID='" + lblpdID.Text.ToString() + "' Complaint='" + lblRemark.Text.ToString() + "' Remedy='" + lblRemedy.Text.ToString() + "' BPMIN='" + lblSys.Text.ToString() + "' BPMAX='" + lblDis.Text.ToString() + "'/>";
    //        }
    //    }
    //    XMLString = XMLString + "</ROOT>";
    //    return XMLString;
    //}

    //public void BindInvest()
    //{
    //    try
    //    {
    //        strSQL = "DECLARE @TempOccupiedBed TABLE (BedNo int,WID int,WNO varchar(30),WIPDID int,InDate smalldatetime) insert into @TempOccupiedBed select distinct wBedNo,max(wid),wNo,wipdId,wInDate from tblPatientsWardDetails where wOutDate is  null group by wNo,wBedNo,wipdId,wInDate select t.BedNo,WID,WNO,InDate, pdInitial +'  '+pdFname +'  '+pdMname +'  '+ pdLname AS 'PName',WIPDID,p.pdID,Complaint,Remedy,BpMin,BpMax from @TempOccupiedBed t inner join tblIpdMaster i on t.WIPDID =i.ipdid inner join tblPatientDetails p on i.ipdpdId  =p.pdID  left join (SELECT * FROM dbo.tblInvestigationDetails WHERE flag=0) invest on invest.PatientID=p.pdID    where 1=1";
    //        strSQL += " AND WNO = " + ddWardNo.SelectedItem.Text + "";
    //        strSQL += " order by WNO,BedNo asc";

    //        DataTable dt = gen.getDataTable(strSQL);
    //        GRV1.DataSource = dt;
    //        GRV1.DataBind();
    //        createpaging(dt.Rows.Count, GRV1.PageCount);
    //        Up2.Visible = true;
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}

    //protected void GRV1_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //   /* DataRowView drview = e.Row.DataItem as DataRowView;
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        DropDownList dd = (DropDownList)e.Row.FindControl("ddlRemark");
    //        //DropDownList ddMin = (DropDownList)e.Row.FindControl("ddlBpMin");
    //        //DropDownList ddMax = (DropDownList)e.Row.FindControl("ddlBpMax");

    //        if (drview.Row["Complaint"].ToString() != "")
    //        {
    //            dd.SelectedValue = drview.Row["Complaint"].ToString();
    //            //ddMin.SelectedValue = drview.Row["BpMin"].ToString();
    //            //ddMax.SelectedValue = drview.Row["BpMax"].ToString();


    //        }
    //    }*/
    //}

    //protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    GRV1.PageIndex = e.NewPageIndex;
    //    Page_no = e.NewPageIndex + 1;
    //    BindInvest();
    //}
    //protected void createpaging(int intTotalRecords, int intTotalPages)
    //{
    //    int recEnd, recStart;

    //    if (Page_no == 0) { Page_no = 1; }
    //    if (intTotalRecords < (Page_no * Page_size))
    //    {
    //        recEnd = intTotalRecords;
    //    }
    //    else
    //    {
    //        recEnd = (Page_no * Page_size);
    //    }

    //    recStart = ((Page_no - 1) * Page_size) + 1;

    //    lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;
    //}
    protected void ddWardNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddWardNo.SelectedValue != "")
        {
            BindInvest();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            this.UploadFile("BloodTest",fu_BloodTest);
            this.UploadFile("UrineTest",fu_UrineTest);
            this.UploadFile("Monitor",fu_Monitor);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "proc_InsInvestigationDetails";
            cmd.CommandType = CommandType.StoredProcedure;

            if (con.State == ConnectionState.Closed)
                con.Open();

            cmd.Connection = con;
            cmd.Parameters.AddWithValue("@VisitTime", ddlVisitTime.SelectedValue);
            cmd.Parameters.AddWithValue("@wmID", ddWardNo.SelectedValue);
            cmd.Parameters.AddWithValue("@CreatedBy", userid);
            cmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.AddWithValue("@BedNo", lblBedNo.Text.Trim());
            cmd.Parameters.AddWithValue("@PatientID", ddPatientName.SelectedValue);
            cmd.Parameters.AddWithValue("@Complaint", txtFreshComplaint.Text.Trim());
            cmd.Parameters.AddWithValue("@Remedy", txtMedicineDetails.Text.Trim());
            cmd.Parameters.AddWithValue("@BpMin", txtSys.Text.Trim());
            cmd.Parameters.AddWithValue("@BpMax", txtDis.Text.Trim());
            cmd.Parameters.AddWithValue("@LatestSymptom", txtLatestSymptom.Text.Trim());
            cmd.Parameters.AddWithValue("@ReferenceToThirdPartyDoc", txtReferencetoaThirdPartyDoctor.Text.Trim());
            cmd.Parameters.AddWithValue("@ShiftToAnotherHospital", txtWecanshifttoanotherhospital.Text.Trim());
            cmd.Parameters.AddWithValue("@BloodTestFile", BloodTestFile);
            cmd.Parameters.AddWithValue("@UrineTestFile", UrineTestFile);
            cmd.Parameters.AddWithValue("@MonitorFile", Monitor);

            cmd.ExecuteNonQuery();
            int error = (int)cmd.Parameters["@Error"].Value;

            if (error == 0)
            {
                Response.Redirect("AddInvestigation.aspx?msg=Investigation Details added successfully");
            }
        }
    }
    protected void ddPatientName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddPatientName.SelectedValue != "")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "proc_GetInvestigationDetailsByPdID";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            cmd.Parameters.AddWithValue("@pdId", ddPatientName.SelectedValue);

            DataTable dt = new DataTable();
            dt = gen.getDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                lblBedNo.Text = dt.Rows[0]["BedNo"].ToString();
                txtFreshComplaint.Text = dt.Rows[0]["Complaint"].ToString();
                txtMedicineDetails.Text = dt.Rows[0]["Remedy"].ToString();
                txtLatestSymptom.Text = dt.Rows[0]["LatestSymptom"].ToString();
                txtReferencetoaThirdPartyDoctor.Text = dt.Rows[0]["ReferenceToThirdPartyDoc"].ToString();
                txtWecanshifttoanotherhospital.Text = dt.Rows[0]["ShiftToAnotherHospital"].ToString();
                txtSys.Text = dt.Rows[0]["BpMin"].ToString();
                txtDis.Text = dt.Rows[0]["BpMax"].ToString();
            }
        }
    }

    private void UploadFile(string type,FileUpload f1)
    {
        string filename = string.Empty;
        string strExt = string.Empty;
        string strPath = string.Empty;

        if (f1.FileName != "")
        {
            if (type == "BloodTest")
            {
                strExt = System.IO.Path.GetExtension(fu_BloodTest.FileName);
                filename = "BloodTest_" + ddPatientName.SelectedValue + strExt;
                BloodTestFile = filename;
            }

            if (type == "UrineTest")
            {
                filename = "UrineTest_" + ddPatientName.SelectedValue + strExt;
                UrineTestFile = filename;
            }

            if (type == "Monitor")
            {
                filename = "Monitor_" + ddPatientName.SelectedValue + strExt;
                Monitor = filename;
            }

            strPath = "InvestigationFiles/" + filename;
            f1.SaveAs(Server.MapPath(strPath));
        }
    }

    private void ClearFields()
    {
        lblBedNo.Text = string.Empty;
        txtFreshComplaint.Text = string.Empty;
        txtLatestSymptom.Text = string.Empty;
        txtMedicineDetails.Text = string.Empty;
        txtSys.Text = string.Empty;
        txtDis.Text = string.Empty;
        txtReferencetoaThirdPartyDoctor.Text = string.Empty;
        txtWecanshifttoanotherhospital.Text = string.Empty;
    }
}