using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using AjaxControlToolkit;

public partial class TodaysList : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    string uid = "0";
    string tempDate = "";
    string tempDoctor = "";
    int Page_no = 0, Page_size = 20;
    string frm, to;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }
        if (Request.QueryString["flag"] == "1")
            lblMsg.Text = "Record Updated Successfully....!!!";

        if (Request.QueryString["flag"] == "2")
            lblMsg.Text = "Record Deleted Successfully....!!!";
        
        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            BindGridView();
        }

    }
    
    protected void BindGridView()
    {
        string[] _frm;
        string[] _to;
        
        if (txtfrm.Text.Contains("/"))
        {
            _frm = txtfrm.Text.ToString().Split('/');
            _to = txtto.Text.ToString().Split('/');

            frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0];// +" 00:00:00";
            to = _to[2] + "/" + _to[1] + "/" + _to[0];// +" 23:55:00";
        }
       
        StrSQL = "Select Distinct ROW_NUMBER() over(order By THour, TMinute) As SNO , Tid , PD.pdInitial +' '+ PD.pdFname+' '+ PD.pdMname +' '+ PD.pdLname As pdName, case when (Select MIN(fdId) from tblFollowUpDatails Where fdPatientId = PD.pdId) > 0 then 'Green' else 'Red' end as 'FStatus' ,pdAge , case when pdSex = 'Male' then 'M' else 'F' end as pdSex , '' As KCO ,  PD.pdId   , TL.PatType as PatType, ISNULL(PD.pdMob,PD.pdTele) As ContactNo  ,PD.pdCasePaperNo, dFName As DocName, ";
        StrSQL += "Case PD.Pat_Type when 1 Then '.' when 2 Then '*' else '' end As PatType ,Case ISNULL((Select MIN(fdId) from tblFollowUpDatails Where fdPatientId = PD.pdId),'') When '' Then 'N' else 'Y' end As CaseStat ,THour , TMinute, TAMPM, C.City , PD.pdOccupation ,Convert(varchar(15),TCreatedDate,103) Date ,(Select MIN(fdId) from tblFollowUpDatails Where fdPatientId = PD.pdId) As fdId From tblTodaysList TL Inner Join tblPatientDetails PD On TL.TpdId = PD.pdId Left Join tblDoctorMaster DM On DM.did = PD.did Left Join tblCityMaster C On C.CityID = PD.pdcity Where TL.TpdId <> 0 And PD.IsCancel =  0 and TL.IsDelete = 0 ";

        if (frm != "" && to != "")
        {
            StrSQL += "And TL.TCreatedDate Between '" + frm + "' And '" + to + "' ";
        }
        if (txtSearchCPN.Text != "")
        {
            StrSQL += "And pdCasePaperNo Like '%" + txtSearchCPN.Text + "%' ";
        }
        DataTable dt = gen.getDataTable(StrSQL);
        if (dt.Rows.Count > 0)
        {
            GVD_PatientList.DataSource = dt;
            GVD_PatientList.DataBind();
        }
        else
        {
            GVD_PatientList.EmptyDataText.ToString();
            GVD_PatientList.DataBind();
        }

        //string[] frmdate = txtfrm.Text.ToString().Split('/');
        //string[] todate = txtto.Text.ToString().Split('/');
        //string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        //string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];
        //StrSQL = "TodaysList";
        //SqlCmd = new SqlCommand(StrSQL, conn);
        //SqlCmd.CommandType = CommandType.StoredProcedure;
        //SqlCmd.Parameters.AddWithValue("@frmDate", Frmdate);
        //SqlCmd.Parameters.AddWithValue("@toDate", Todate);
        
        //if (conn.State == ConnectionState.Closed)
        //{
        //    conn.Open();
        //    SqlDataReader dr = SqlCmd.ExecuteReader();
        //    DataTable dt = new DataTable();
        //    dt.Load(dr);
        //    if (dt.Rows.Count > 0)
        //    {
        //        export.Style.Add("display", "block");
        //        GVD_PatientList.DataSource = dt;
        //        GVD_PatientList.DataBind();
        //    }
        //    else
        //    {
        //        export.Style.Add("display", "none");
        //        GVD_PatientList.DataSource = null;
        //        GVD_PatientList.DataBind();
        //    }
        //    conn.Close();
        //    dr.Dispose();
        //}
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGridView();
        ScriptManager1.RegisterPostBackControl(this.imgexport);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void GVD_PatientList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
        {
            ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
            ImageButton btnDone = (ImageButton)e.Row.FindControl("btnDone");
            //TextBox txtSno = (TextBox)e.Row.FindControl("txtSno");
            TextBox txtMob = (TextBox)e.Row.FindControl("txtMob");
            TextBox txtDOA = (TextBox)e.Row.FindControl("txtDOA");
            CalendarExtender txtDOA_CalenderExtender = (CalendarExtender)e.Row.FindControl("txtDOA_CalenderExtender");
            DropDownList ddlpttype = (DropDownList)e.Row.FindControl("ddlpttype");
            HiddenField hdnpttype = (HiddenField)e.Row.FindControl("hdnpttype");
            DropDownList ddlHour = (DropDownList)e.Row.FindControl("ddlHour");
            DropDownList ddlMinute = (DropDownList)e.Row.FindControl("ddlMinute");
            //DropDownList ddlAMPM = (DropDownList)e.Row.FindControl("ddlAMPM");
            HiddenField hdnHour = (HiddenField)e.Row.FindControl("hdnHour");
            HiddenField hdnMinute = (HiddenField)e.Row.FindControl("hdnMinute");
            //HiddenField hdnAMPM = (HiddenField)e.Row.FindControl("hdnAMPM");
            
            ddlHour.SelectedValue = hdnHour.Value;
            ddlMinute.SelectedValue = hdnMinute.Value;
            ddlpttype.SelectedValue = hdnpttype.Value;
           // ddlAMPM.SelectedValue = hdnAMPM.Value;

            if (ddlpttype.SelectedValue != "")
            {
                //txtTOA.Enabled = txtMob.Enabled = txtSno.Enabled = btnDone.Visible = false;
                txtDOA_CalenderExtender.Enabled = false;
                ddlHour.Enabled = txtDOA.Enabled = ddlMinute.Enabled = ddlpttype.Enabled = txtMob.Enabled = btnDone.Visible = false; //ddlAMPM.Enabled =  txtSno.Enabled = 
                btnEdit.Visible = true;
            }
            else
            {
                //txtTOA.Enabled = txtMob.Enabled = txtSno.Enabled = btnDone.Visible = true;
                txtDOA_CalenderExtender.Enabled = true;
                ddlHour.Enabled = txtDOA.Enabled = ddlMinute.Enabled = ddlpttype.Enabled = txtMob.Enabled = btnDone.Visible = true; //ddlAMPM.Enabled = 
                 //txtSno.Enabled = 
                btnEdit.Visible = false;
            }
        }
        
    }

    protected void GVD_PatientList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVD_PatientList.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGridView();
    }

    protected void GVD_PatientList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            lblMsg.Text = "";

            if (e.CommandName == "CaseFol")
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                string pdID = arg[0];
                string fdID = arg[1];

                Session["ipdid"] = null;
                Session["CasePatientID"] = pdID;

                int cnt = Convert.ToInt32(gen.executeScalar("Select Count(fdId) from vwConsultingHistoryNewAks Where fdPatientId=" + pdID).ToString());

                if (cnt >= 1)
                {
                    if (fdID != "0")
                    {
                        Response.Redirect("FollowupDetail.aspx?Stat=NO&fdId=" + fdID);
                    }
                    else
                    {
                        Response.Redirect("FollowupDetail.aspx?fdId=" + fdID);
                    }


                }
                else
                {
                    Response.Redirect("ConsultPatient.aspx", false);
                }
            }
            if (e.CommandName == "done")
            {
                int Tid = Convert.ToInt32(e.CommandArgument);

                if (Tid != null)
                {
                    GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    ImageButton btnDone = (ImageButton)gvRow.FindControl("btnDone");
                    ImageButton btnEdit = (ImageButton)gvRow.FindControl("btnEdit");
                    TextBox txtMob = (TextBox)gvRow.FindControl("txtMob");
                    //TextBox txtSno = (TextBox)gvRow.FindControl("txtSno");
                    Label lblResult = (Label)gvRow.FindControl("lblResult");
                    Label txtpdid = (Label)gvRow.FindControl("txtpdid");
                    TextBox txtDOA = (TextBox)gvRow.FindControl("txtDOA");
                    CalendarExtender txtDOA_CalenderExtender = (CalendarExtender)gvRow.FindControl("txtDOA_CalenderExtender");

                    DropDownList ddlHour = (DropDownList)gvRow.FindControl("ddlHour");
                    DropDownList ddlMinute = (DropDownList)gvRow.FindControl("ddlMinute");
                    DropDownList ddlpttype = (DropDownList)gvRow.FindControl("ddlpttype");
                    //DropDownList ddlAMPM = (DropDownList)gvRow.FindControl("ddlAMPM");
                    
                    if (ddlpttype.SelectedValue == "")
                    {
                        lblResult.Visible = true;
                        lblResult.Text = "Required";
                        lblResult.ForeColor = Color.DarkRed;
                        return;
                    }
                    else
                    {
                        lblResult.Visible = true;
                        lblResult.Text = "";
                    }
                    
                    //btnDone.Visible = txtMob.Enabled = txtSno.Enabled = txtTOA.Enabled = false;
                    txtDOA_CalenderExtender.Enabled = false;
                    btnDone.Visible = txtDOA.Enabled = txtMob.Enabled = ddlHour.Enabled = ddlMinute.Enabled = ddlpttype.Enabled = false; //txtSno.Enabled =  ddlAMPM.Enabled 

                    btnEdit.Visible = true;

                    string[] _date = txtDOA.Text.ToString().Split('/');
                    string date = _date[2] + "/" + _date[1] + "/" + _date[0];

                    SqlCmd = new SqlCommand();
                    SqlCmd.CommandText = "proc_UpdateTodaysList";
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.AddWithValue("@Tid", Tid);
                    //SqlCmd.Parameters.AddWithValue("@TSno", "");
                    //SqlCmd.Parameters.AddWithValue("@TCreatedDate", date);
                    SqlCmd.Parameters.AddWithValue("@THour", ddlHour.SelectedValue);
                    SqlCmd.Parameters.AddWithValue("@TMinute", ddlMinute.SelectedValue);
                    //SqlCmd.Parameters.AddWithValue("@TAMPM", ""); ddlAMPM.SelectedValue);
                    SqlCmd.Parameters.AddWithValue("@TDOA", date);
                    SqlCmd.Parameters.AddWithValue("@TCreatedBy", uid);
                    SqlCmd.Parameters.AddWithValue("@patType", ddlpttype.SelectedValue);
                    SqlCmd.Parameters.AddWithValue("@pdID", txtpdid.Text.ToString());
                    SqlCmd.Parameters.AddWithValue("@Mob", txtMob.Text.ToString());
                    SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
                    SqlCmd.Connection = conn;
                    if (conn.State == ConnectionState.Closed)
                        conn.Open();

                    SqlCmd.ExecuteNonQuery();
                    conn.Close();
                    Response.Redirect("TodaysList.aspx?flag=1", true);

                }
            }
            if (e.CommandName == "editRow")
            {
                int Tid = Convert.ToInt32(e.CommandArgument);
                if (Tid != null)
                {
                    GridViewRow gvRow = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    ImageButton btnDone = (ImageButton)gvRow.FindControl("btnDone");
                    ImageButton btnEdit = (ImageButton)gvRow.FindControl("btnEdit");
                    TextBox txtMob = (TextBox)gvRow.FindControl("txtMob");
                    //TextBox txtSno = (TextBox)gvRow.FindControl("txtSno");
                    DropDownList ddlHour = (DropDownList)gvRow.FindControl("ddlHour");
                    DropDownList ddlMinute = (DropDownList)gvRow.FindControl("ddlMinute");
                    DropDownList ddlpttype = (DropDownList)gvRow.FindControl("ddlpttype");
                    //DropDownList ddlAMPM = (DropDownList)gvRow.FindControl("ddlAMPM");
                    TextBox txtDOA = (TextBox)gvRow.FindControl("txtDOA");
                    CalendarExtender txtDOA_CalenderExtender = (CalendarExtender)gvRow.FindControl("txtDOA_CalenderExtender");

                    //btnDone.Visible = txtSno.Enabled = txtMob.Enabled = txtTOA.Enabled = true;
                    txtDOA_CalenderExtender.Enabled = true;
                    btnDone.Visible = txtDOA.Enabled = txtMob.Enabled = ddlHour.Enabled = ddlMinute.Enabled = ddlpttype.Enabled = true; //txtSno.Enabled = ddlAMPM.Enabled 
                    btnEdit.Visible = false;
                }
            }
            if (e.CommandName == "del")
            {
                int Tid = Convert.ToInt32(e.CommandArgument);
                
                    gen.executeQuery("update tblTodaysList set IsDelete = 1 where Tid =" + Tid);
                    Response.Redirect("TodaysList.aspx?flag=2", true);
                
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "TodaysPatientList.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GVD_PatientList.AllowPaging = false;
        this.BindGridView();
        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ViewState["update"] = Convert.ToString(Session["update"]);
    }


}
