﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class CityMaster : System.Web.UI.Page
{
    SqlCommand Sqlcmd;
    genral gen = new genral();
    int wmID = 0;
    SqlConnection com = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string strSQL = "";
    int userid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        if (Request.QueryString["Id"] != null)
            wmID = int.Parse(Request.QueryString["Id"].ToString());

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 17";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["msg"] != null)
            lblMessage.Text = Request.QueryString["msg"].ToString();

        if (!IsPostBack)
        {
            FillData();
        }
    }

    protected void FillData()
    {

        DDLState.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT Distinct StateID As StateID , State As State FROM tblStateMaster Where IsDelete = 0", "StateID", "State", DDLState);

        if (wmID != 0)
        {
            SqlCommand SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "SELECT * FROM tblCityMaster WHERE CityID=" + wmID + "";
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = com;
            if (com.State == ConnectionState.Closed)
                com.Open();
            SqlDataReader dr = SqlCmd.ExecuteReader();
            while (dr.Read())
            {

                DDLState.SelectedValue = dr["StateID"].ToString();
                txt_City.Text = dr["City"].ToString();
            }
            dr.Close();
            dr.Dispose();
            com.Close();
            com.Dispose();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (com.State == ConnectionState.Closed)
                com.Open();

            if (wmID == 0)
                strSQL = "INSERT INTO tblCityMaster(City,StateID,CreatedBy) VALUES (@City,@StateID,@CreatedBy)";
            else
                strSQL = "UPDATE tblCityMaster SET City=@City , StateID = @StateID , ModifiedBy = @CreatedBy WHERE CityID=" + wmID + "";
            SqlCommand Sqlcmd = new SqlCommand(strSQL, com);
            Sqlcmd.CommandType = CommandType.Text;
            Sqlcmd.Parameters.AddWithValue("@City", txt_City.Text.Trim());
            Sqlcmd.Parameters.AddWithValue("@StateID", Convert.ToDecimal(DDLState.SelectedValue));
            Sqlcmd.Parameters.AddWithValue("@CreatedBy", userid);
            Sqlcmd.ExecuteNonQuery();

            if (wmID == 0)
                Response.Redirect("CityMaster.aspx?msg=City details saved successfully");
            else
                Response.Redirect("ManageCityMaster.aspx?flag=1");

            Sqlcmd.Dispose();
            com.Close();
        }
    }

}