﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using System.Data.SqlClient;

public partial class InvestigationHistory : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    String strSQL = "";
    int PatientID;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    int userid;
    DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] == null)
        {
            Response.Redirect("Login.aspx?msg=You are not logged in. Please login to proceed further");
        }
        else
        {
            //if (Session["userid"] != null)
            userid = Convert.ToInt32(Session["userid"]);
        }

        if (Request.QueryString["PatientID"] != null)
            PatientID = Convert.ToInt32(Request.QueryString["PatientID"]);

        if (!IsPostBack)
        {
            BindGridView("");

        }

    }

    protected void BindGridView(string sort)
    {
        strSQL = "select pdInitial +'  '+pdFname +'  '+pdMname +'  '+ pdLname AS 'PName',Complaint,Remedy,BpMin,BpMax,BedNo,VisitTime from tblInvestigationDetails i inner join tblPatientDetails p on i.PatientID =p.pdID inner join tblInvestigationMaster im on i.InvestID=im.InvestID  where i.PatientID=" + PatientID + "";
        dt = new DataTable();
        dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
        dt.Dispose();
    }
}