﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="DieticianReport.aspx.cs" Inherits="DietitianReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<link rel="stylesheet" type="text/css" href="css/radiotabstrip.css" />
<style>
#tbsearch td { text-align: left; vertical-align: middle;}
#tbsearch label 
{
    display: inline;
    margin-top: 3px;
    position: absolute;
}
#tbhead th
{
    text-align: left;
    background: url(images/nav-back.gif) repeat-x top;
    color: #FFFFFF;
    padding-left: 2px;
    padding-top: 3px;
}
#gvcol div { margin-top: -10px;}
.head1 { width: 10%;}
.head2 { width: 18%;}
.head3 { width: 9%;}
.head4 { width: 10%;}
.head4i { width: 18%;}
.head5 { width: 10%;}
.head6 { width: 10%;}
.head7 { width: 18}
.head8 { width: 5%; text-align: center;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server"> 
        <ContentTemplate> 
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr> 
                    <td> 
                        <div class="login-area margin_ten_right" style="width: 100%; text-align:center;"> 
                            <h2>Report</h2>
                            <div class="formmenu"> 
                                <div class="loginform"> 
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-HTML first" id="li_1">
                                           <asp:LinkButton ID="lnkOpd" runat="server" CssClass="qt_tab active" PostBackUrl="~/OPDReport.aspx">OPD</asp:LinkButton>
                                        </li>
                                        <li class="qtab-HTML" id="li_2">
                                            <asp:LinkButton ID="lnk3dccg" runat="server" CssClass="qt_tab active" PostBackUrl="~/3DCCGReport.aspx">3DCCG</asp:LinkButton></li>
                                        <li class="qtab-HTML" id="li_3">
                                            <asp:LinkButton ID="lnlipd" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDReport.aspx">IPD</asp:LinkButton></li>
                                        <li class="qtab-HTML" id="li_4">
                                            <asp:LinkButton ID="lnkConsulting" runat="server" CssClass="qt_tab active" PostBackUrl="~/ConsultingReport.aspx">
                                            Consulting</asp:LinkButton></li>
                                        <li class="qtab-Demo last" id="li_5">
                                          <asp:LinkButton ID="lnkPft" runat="server" CssClass="qt_tab active" PostBackUrl="~/PFTReport.aspx">PFT</asp:LinkButton>
                                        </li>
                                          <li class="qtab-HTML" id="li1">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="qt_tab active" PostBackUrl="~/ReConsulting.aspx">Reconsulting</asp:LinkButton></li>  
                                         <li class="qtab-HTML" id="li_6">
                                            <asp:LinkButton ID="LnkECG" runat="server" CssClass="qt_tab active" PostBackUrl="~/ECGReport.aspx">ECG</asp:LinkButton>
                                        </li>
                                        <li class="qtab-Demo last" id="li_8">
                                            <asp:LinkButton ID="LikCourier" runat="server" CssClass="qt_tab active" PostBackUrl="~/CourierReport.aspx">Courier</asp:LinkButton></li>
                                        <li class="qtab-Demo last" id="li_9">
                                            <asp:LinkButton ID="LnkAdvance" runat="server" CssClass="qt_tab active" PostBackUrl="~/AdvanceReport.aspx">Advance</asp:LinkButton>
                                        </li>
                                        <li class="qtab-Demo last" id="li_10">
                                            <asp:LinkButton ID="LnkBalance" runat="server" CssClass="qt_tab active" PostBackUrl="~/BalanceReport.aspx" Style="display:none;">Balance</asp:LinkButton>
                                        </li>
                                        <li class="qtab-Demo last" id="li_11">
                                            <asp:LinkButton ID="LnkPnt" runat="server" CssClass="qt_tab active" PostBackUrl="~/DeletedPatientReport.aspx" Style="display:none;">Deleted Patient</asp:LinkButton>
                                        </li>
                                        <li class="qtab-Demo active" id="li_12">
                                          <asp:LinkButton ID="LnkDietn" runat="server" CssClass="qt_tab active" Enabled="false">Dietician</asp:LinkButton>
                                        </li>
                                        <li class="qtab-Demo last" id="li_13">
                                            <asp:LinkButton ID="LnkDelIPD" runat="server" CssClass="qt_tab active" PostBackUrl="~/DeletedIPDReport.aspx" Style="display:none;">Deleted IPD</asp:LinkButton>
                                        </li>
					<li class="qtab-HTML" id="li_14">
                                            <asp:LinkButton Id="LnkVoucher" runat="server" CssClass="qt_tab active" PostBackUrl="~/VoucherReport.aspx">Voucher</asp:LinkButton>
                                        </li>
					<li class="qtab-HTML last" id="li_15">
                                            <asp:LinkButton Id="LnkTotal" runat="server" CssClass="qt_tab active" PostBackurl="~/GetTotalInvoiceAmountofAllReport.aspx">Total Report</asp:LinkButton>
                                        </li>
                                    </ul>
                                        
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto; border: 1.5px solid #E2E2E2;">
                                        <table width="100%" cellspacing="0px" cellpadding="2px">
                                            <tr>
                                                <td>
                                                    <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                        <tr>
                                                            <td class="tableh1">
                                                                <table id="tbsearch" style="width: 100%;">
                                                                    <tr>
                                                                        <td>Created From :</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtfrm" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                                runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                width: 20px; height: 20px;" />
                                                                        </td>
                                                                        <td> To :</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtto" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" 
                                                                            TargetControlID="txtto" Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                            </cc1:CalendarExtender>
                                                                            <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                width: 20px; height: 20px;" />
                                                                        </td>
                                                                        
                                                                           <td>Prepared By : &nbsp;
                                                                              
                                                                        	</td>

                                                                            <td>
                                                                              <asp:DropDownList ID="DDL_User" runat="server" class="required field"  Width="150px">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                      <td>
                                                                       Payment Mode : &nbsp;
                                                                      </td>
                                                                        <td>
                                                                           <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="160px" CssClass="field">
                                                                            
                                                                            <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                                            <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                                                            <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                                                            <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                                                            <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
										<asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
										<asp:ListItem Text="All" Value="%"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        </td>
                                                             </tr>

                                                                      <tr>

                                                                    <td colspan="8">
                                                                   
                                                                     <table width="100%">
                                                                   <tr>
                                                                   <td style="width:2%">
                                                                            <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                                                             OnClientClick="aspnetForm.target ='_self';" OnClick="btnView_Click" />
                                                                        </td>
                                                                        <td style="width:2%">
                                                                           <div id="export" runat="server" style="float: right; display: none">
                                                                                <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" 
                                                                                OnClick="imgexport_Click" />
                                                                            </div>
                                                                        </td>
                                                                        <td style="color: Red; text-align: right;">
                                                                           <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                                        </td>
                                                                   </tr>
                                                                   </table>
                                                                    
                                                                    </td>

                                                                   
                                                                    </tr>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <asp:Panel ID="pnlHead" runat="server" Visible="false">
                                                <div id="rptHead" runat="server" visible="false">
                                                    <table width="100%" id="rounded-corner-report">
                                                        <tr>
                                                            <td align="center">
                                                        		<strong>Aditya Homoeopathic Hospital & Healing Centre</strong><br />
                                                        		Pimpri Gaon, Pune - 411 017.
                                                    		</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="font-weight: bold; font-size:16px">Dietician Report</td>
                                                        </tr>
                                                        <tr>
                                                    		<td>&nbsp;</td>
                                                		</tr>
                                                        <tr>
                                                            <td id="gvcol">
                                                                <asp:GridView runat="server" ID="GRV1" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="false"
                                                                    CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" Width="100%" OnRowDataBound="GRV1_RowDataBound1"
                                                                    OnPageIndexChanging="GRV1_PageIndexChanging">
                                                                    <Columns>

                                                                         <asp:TemplateField HeaderText="Prepared By" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                       <asp:label runat="server" ID="lblUser" Text='<%#Eval("UserName")%>'></asp:label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="TestDate" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" ID="lbldate" Text='<%#Eval("itdTestDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Sr.No" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <%#Eval("rank")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <%#Eval("Name")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="N OF S" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                                            <ItemTemplate>
                                                                                <%#Eval("test")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="CasePaperNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <%#Eval("pdCasePaperNo")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Payment" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" ID="lblPayment" Text='<%#Eval("itdcharges")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                          <asp:TemplateField HeaderText="Patient Type" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" ID="lblPatType" Text='<%#Eval("PatType")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="Payment Mode" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%#Eval("Payment_Mode")%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                                                                        <ItemTemplate>
                                                                             <%# Eval("PrintBill") %>
                                                                        </ItemTemplate>
                                                                        </asp:TemplateField>    
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="10">
                                                                <table width="100%" cellspacing="0" border="1" class="mGrid">
                                                                    <tr>
                                                                        <td style="width: 59%">
                                                                            <asp:Label ID="Label1" Font="Bold" runat="server" Text="Sub Payment"></asp:Label></b>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <b>
                                                                                <asp:Label ID="lblSubTotal" runat="server" Text="0.00"></asp:Label></b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 59%">
                                                                            <b>Total Payment</b>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <b>
                                                                                <asp:Label ID="lblGrandTotal" runat="server" Text="0.00"></asp:Label></b>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

