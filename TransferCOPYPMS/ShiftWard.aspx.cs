﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class ShiftWard : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    //string pdId = "";
    string uid = "";
    //string ipdid = "";
    //added by nikita on 12th march 2015------------------------------
    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (!IsPostBack)
        {
            if (Session["Ward"] != null)
                ViewState["WardTypeID"] = Session["Ward"].ToString();

            if (Session["WaitingPatientID"] != null)
                ViewState["wID"] = Session["WaitingPatientID"].ToString();

            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");


            if (Session["ipdid"] != null)
                ViewState["ipdid"] = Session["ipdid"].ToString();
            else
                Response.Redirect("ProfarmaOfCase.aspx");

            txtInDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

            ddWard.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT Distinct WardTypeID As wmWardTypeId , WardTypeName As wmType FROM tblWardTypeMaster Where IsDelete = 0", "wmWardTypeId", "wmType", ddWard);

            if (ViewState["WardTypeID"] != null)
            {
                ddWard.SelectedValue = ViewState["WardTypeID"].ToString();
                ddWardNo.Items.Clear();
                ddWardNo.Items.Insert(0, new ListItem("Select", ""));
                gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + ddWard.SelectedValue + "' And IsDelete = 0 ", "wmID", "wmWardNo", ddWardNo);

            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string indate = gen.getdate(txtInDate.Text.ToString(), '/');

        strSQL = "ShiftWard";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@wipdId", ipdid);
        SqlCmd.Parameters.AddWithValue("@wType", ddWard.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@wNo", ddWardNo.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@wBedNo", ddBedNo.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@wInDate", indate);
        SqlCmd.Parameters.AddWithValue("@CreatedBy", uid);
        if (ViewState["wID"] != null)
        {
            int wID = Convert.ToInt32(ViewState["wID"]);
            SqlCmd.Parameters.AddWithValue("@wID", wID);
        }
        else
        {
            SqlCmd.Parameters.AddWithValue("@wID", 0);
        }

        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd.ExecuteNonQuery();
        ClearFileds();
        con.Close();
        SqlCmd.Dispose();
        con.Dispose();

        Response.Redirect("IPDView.aspx", false);
    }

    public void ClearFileds()
    {
        Session["Ward"] = null;
        Session["WaitingPatientID"] = null;
    }

    protected void ddWardNo_SelectedIndexChanged(object sender, EventArgs e)
    {

        ddBedNo.Items.Clear();
        ddBedNo.Items.Insert(0, new ListItem("Select", ""));
        /////// ------ Commented On 03/06/2016 By Ashok--------//////////// 
        // strSQL = "sp_GetBed"; 
        /////// ------ Commented On 03/06/2016 By Ashok--------////////////
        strSQL = "sp_GetBed_New";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@wmID", ddWardNo.SelectedValue);
        //SqlCmd.Parameters.AddWithValue("@toDate", ToDate);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            dr = SqlCmd.ExecuteReader();
            SqlCmd.Dispose();
            DataTable dt = new DataTable();
            dt.Load(dr);
            foreach (DataRow drow in dt.Rows)
            {
                ddBedNo.Items.Add(new ListItem(drow["bmNoOfBads"].ToString(), drow["bmid"].ToString()));
            }
        }
        con.Close();

    }
    protected void ddWard_SelectedIndexChanged(object sender, EventArgs e)
    {
        // if (ddWard.SelectedValue == "")
        // {
        //     ddWardNo.Items.Clear();
        //     ddBedNo.Items.Clear();
        //}

        // if (ddWard.SelectedValue == "1")
        // {
        //     ddWardNo.Items.Clear();
        //     ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        //     gen.FillDropDownList("select wmID,wmWardNo from tblWardMaster where wmType='GENERAL'", "wmID", "wmWardNo", ddWardNo);
        // }

        // if (ddWard.SelectedValue == "2")
        // {
        //     ddWardNo.Items.Clear();
        //     ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        //     gen.FillDropDownList("select wmID,wmWardNo from tblWardMaster where wmType='SEMI-SPECIAL'", "wmID", "wmWardNo", ddWardNo);
        // }

        // if (ddWard.SelectedValue == "3")
        // {
        //     ddWardNo.Items.Clear();
        //     ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        //     gen.FillDropDownList("select wmID,wmWardNo from tblWardMaster where wmType='SPECIAL'", "wmID", "wmWardNo", ddWardNo);
        // }

        // if (ddWard.SelectedValue == "4")
        // {
        //     ddWardNo.Items.Clear();
        //     ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        //     gen.FillDropDownList("select wmID,wmWardNo from tblWardMaster where wmType='DELUX'", "wmID", "wmWardNo", ddWardNo);
        // }

        ddWardNo.Items.Clear();
        ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + ddWard.SelectedValue + "' And IsDelete = 0 ", "wmID", "wmWardNo", ddWardNo);

    }
}
