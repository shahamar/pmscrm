﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;


public partial class OPDTestDetails : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    string strSQL = "",msg;
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    //string pdId = "0";
    string uid = "0";
    //added by nikita on 12th march 2015------------------------------
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-----------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");
        
        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 31";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------

        if (Request.QueryString["flag"] != null)
            lblMessage.Text = Request.QueryString["flag"];
        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");

            //---Added By Nikita On 30thMarch 2015----------------------------------------------------
            if (msg == "1")
            {
                btnSave.Enabled = false;
                btnPrint.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
                btnPrint.Enabled = true;
            }
            //-----------------------------------------------------------------------------------------

            FillData();
            FillHistory();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string finyear = gen.GetCurrentFinYear();
        string ptdReceiptNo = "";

        foreach (GridViewRow gr in GRV1.Rows)
        {

            HtmlInputCheckBox chk = (HtmlInputCheckBox)gr.FindControl("chkrow");
            HiddenField hidtmid = (gr.FindControl("hidtmId") as HiddenField);
            Date dt = (gr.FindControl("tDate") as Date);
            Label lblCharge = (gr.FindControl("lblBaseCharges") as Label);

            string[] _dot = dt.CalendarDateString.Split('/');
            string dot = _dot[2] + "/" + _dot[1] + "/" + _dot[0];

            if (chk.Checked == true)
            {
                object o = gen.executeScalar("Select CONVERT(VARCHAR,ISNull(MAX(CAST(SubString(ptdReceiptNo,1,len(ptdReceiptNo)-4) As INT)),0)+1)+'" + finyear + "' From dbo.tblPatientTestDetails");
                ptdReceiptNo = o.ToString();

                //values += ",('" + hidtmid.Value.ToString() + "','" + pdId + "','" + dot + "','" + lblCharge.Text + "','" + uid + "','" + ptdReceiptNo + "')";

                //values = values.Substring(1, values.Length - 1);
                strSQL = "INSERT INTO tblPatientTestDetails(ptdTMId,ptdpdId,ptdFromDate,ptdCharges,ptdCreatedBy,ptdReceiptNo,Payment_Mode) VALUES ('" + hidtmid.Value.ToString() + "','" + pdId + "','" + dot + "','" + lblCharge.Text + "','" + uid + "','" + ptdReceiptNo + "' , '" + DDL_Pay_Mode.SelectedValue + "')";
                gen.executeQuery(strSQL);

            }
        }
        Response.Redirect("OPDTestDetails.aspx?flag=OPD Test details saved successfully",false);
        FillHistory();
        
    }

    public void FillData()
    {
        strSQL = "SELECT *,getdate() as tDate From tblTestMaster WHERE tmTestName NOT IN ('Monitor')";
        DataTable dt = gen.getDataTable(strSQL);
        GRV1.DataSource = dt;
        GRV1.DataBind();
    }

    public void FillHistory()
    {
        strSQL = "Select ptd.ptdId,ptd.ptdTMId,ptd.ptdpdId,ptd.ptdFromDate,ptd.ptdToDate,ptd.ptdCharges,tm.tmTestName ";
        strSQL += " FROM dbo.tblPatientTestDetails ptd INNER JOIN dbo.tblTestMaster tm ON ptd.ptdTMId = tm.tmId Where ptdpdId=" + pdId;

        DataTable dt = gen.getDataTable(strSQL);
        GRV2.DataSource = dt;
        GRV2.DataBind();
        
        if (msg == "1")
            GRV2.Columns[4].Visible = false;
        else
            GRV2.Columns[4].Visible = true;

        if (dt.Rows.Count > 0)
        {
            pnlhis.Visible = true;
            btnPrint.Visible = true;
        }
        else
        {
            pnlhis.Visible = false;
            btnPrint.Visible = false;
        }
    }
    protected void GRV2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GRV2.EditIndex = -1;
        FillHistory();
    }
    protected void GRV2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            string ptdID = e.CommandArgument.ToString();
            //gen.executeQuery("Delete From tblPatientTestDetails Where ptdId =" + ptdID);
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_DeletePatientTestDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.AddWithValue("@ptdId", ptdID);
            SqlCmd.Parameters.AddWithValue("@deletedBy", uid);
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();

            SqlCmd.ExecuteNonQuery();
            con.Close();
            Response.Redirect("OPDTestDetails.aspx?flag=OPD Test details deleted successfully", false);
            FillHistory();
        }
    }
    protected void GRV2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GRV2.EditIndex = e.NewEditIndex;
        FillHistory();
    }
    protected void GRV2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GRV2.Rows[e.RowIndex];
        HiddenField hidptdId = (HiddenField)row.Cells[0].FindControl("hidptdId");

        TextBox _txtcharge = (TextBox)row.Cells[1].FindControl("txtBaseCharges2");
        Date _tDate2 = (Date)row.Cells[2].FindControl("tDate2");

        string[] _dot = _tDate2.CalendarDateString.Split('/');
        string dot = _dot[2] + "/" + _dot[1] + "/" + _dot[0];

        strSQL = "Update tblPatientTestDetails SET ptdCharges='" + _txtcharge.Text + "',ptdFromDate='" + dot + "'";
        strSQL += " where ptdId = " + hidptdId.Value.ToString() + "";
        gen.executeQuery(strSQL);
        GRV2.EditIndex = -1;
        FillHistory();

    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        string ptdId = "";
        string newptdId = "";
        string fptdId = "";

        foreach (GridViewRow grvrow in GRV2.Rows)
        {
            HtmlInputCheckBox chk = (HtmlInputCheckBox)grvrow.FindControl("cbAC");
            if (chk.Checked == true)
            {
                ptdId = chk.Value;
                newptdId += ',' + ptdId;
            }

        }
        fptdId = newptdId.Substring(1);
        Response.Redirect("BillFormat.aspx?id=" + fptdId + "");
        
    }
	protected void gridOPDTest_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        FillData();
    }
}