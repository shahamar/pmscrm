﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class IPD : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole, msg;
    string uid = "0";
    //added by nikita on 13th march 2015------------------------------
    public int ipdid
    {
        get
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }

    public int PdID
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        Session["ipdid"] = null;

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //strSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 32";
        //bool exists = gen.doesExist(strSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        //---Added By Nikita On 30thMarch 2015----------------------------------------------------
        if (Session["msg"] != null)
            msg = Session["msg"].ToString();
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {
            if (Session["Ward"] != null)
                ViewState["WardTypeID"] = Session["Ward"].ToString();

            if (Session["WaitingPatientID"] != null)
                ViewState["wID"] = Session["WaitingPatientID"].ToString();

            //added by nikita on 13th march 2015------------------------------
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");
            //-----------------------------------------------------------------------------------------

            //---Added By Nikita On 30thMarch 2015----------------------------------------------------
            if (msg == "1")
            {
                btnSave.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
            }
            //-----------------------------------------------------------------------------------------

            FillData(); 
            txtDOA.Text = string.Format("{0:dd/MM/yyyy}",DateTime.Now);
        }
    }

    public void FillData()
    {

        DDL_RemList.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("Select * From tblRemedyMaster where isDelete = 0 AND RMNAME != '0' ORDER BY REPLACE(RMNAME,' ','') ASC", "RMID", "RMNAME", DDL_RemList);

        ddWard.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT Distinct WardTypeID As wmWardTypeId , WardTypeName As wmType FROM tblWardTypeMaster Where IsDelete = 0", "wmWardTypeId", "wmType", ddWard);

        if (ViewState["WardTypeID"] != null)
        {
            ddWard.SelectedValue = ViewState["WardTypeID"].ToString();
            ddWardNo.Items.Clear();
            ddWardNo.Items.Insert(0, new ListItem("Select", ""));
            gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + ddWard.SelectedValue + "' And IsDelete = 0 ", "wmID", "wmWardNo", ddWardNo);

        }



        strSQL = "Select * From tblIpdMaster Where IsDelete = 0 AND ipdId=" + ipdid;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        if (Sqldr.HasRows == true)
        {
            while (Sqldr.Read())
            {
                txtProvisionalDiagnosis.Text = Sqldr["ipdProvisionalDiagnosis"].ToString();
                txtFinalDiagnosis.Text = Sqldr["ipdFinalDiagnosis"].ToString();
                txtOperation.Text = Sqldr["ipdOperation"].ToString();
                txtDOA.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Sqldr["ipdAdmissionDate"].ToString()));
                string[] time = gen.getTime(Sqldr["ipdAdmissionTime"].ToString());
                if (time[3] == "AM")
                    txtTOA.SetTime(Convert.ToInt32(time[0]), Convert.ToInt32(time[1]), Convert.ToInt32(time[2]), MKB.TimePicker.TimeSelector.AmPmSpec.AM);
                else
                    txtTOA.SetTime(Convert.ToInt32(time[0]), Convert.ToInt32(time[1]), Convert.ToInt32(time[2]), MKB.TimePicker.TimeSelector.AmPmSpec.PM);
                txtReconsultingDiagnosis.Text = Sqldr["ReconsultingDiagnosis"].ToString();
            }
        }
        else
        {
            SqlCmd.Dispose();
            Sqldr.Close();
            FillOPDData();
        }


    }

    public void FillOPDData()
    {
        if (File.Exists(Server.MapPath("Files/KCO/kco_" + PdID)))
        {


            //txtProvisionalDiagnosis.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + PdID).ToString());
            //txtFinalDiagnosis.Text = txtProvisionalDiagnosis.Text.Replace("\r\n", "<br />");
            ////txtProvisionalDiagnosis.Text = txtFinalDiagnosis.Text.Replace(Environment.NewLine, "<br />");
            //txtProvisionalDiagnosis.Text = txtFinalDiagnosis.Text.Replace("<br />", Environment.NewLine);

            txtProvisionalDiagnosis.Text = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + PdID).ToString());
            txtProvisionalDiagnosis.Text = txtProvisionalDiagnosis.Text.Replace("<br />", Environment.NewLine);

            string strval = "";
            strval = File.ReadAllText(Server.MapPath("Files/KCO/kco_" + PdID).ToString());
            txtFinalDiagnosis.Text = strval.Replace("<br />", "\r\n");


            strSQL = "Select top 1 fdRemedy  From tblFollowUpDatails  Where fdPatientId = " + PdID + " Order By fdId Desc ";
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd = new SqlCommand(strSQL, con);
            Sqldr = SqlCmd.ExecuteReader();
            if (Sqldr.HasRows == true)
            {
                while (Sqldr.Read())
                {
                    DDL_RemList.SelectedValue = Sqldr["fdRemedy"].ToString();
                }
            }

        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string finyear = gen.GetCurrentFinYear();
        string doa = gen.getdate(txtDOA.Text.ToString(), '/');
        string toa = txtTOA.Hour + ":" + txtTOA.Minute + ":" + txtTOA.Second + " " + txtTOA.AmPm;

        object receipt = gen.executeScalar("SELECT CONVERT(VARCHAR, ISNULL(MAX(CAST(SUBSTRING(adReceiptNo,1,LEN(adReceiptNo)-4) AS INT)),0)+1)+'" + finyear + "' FROM tblAdvanceDetails WHERE adCreatedDate > '2014-04-01 00:00:00.001'");
        string adReceiptNo = receipt.ToString();

        if (ipdid == 0)
            strSQL = "sp_InsIpdMaster";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@ipdpdId", PdID);
        SqlCmd.Parameters.AddWithValue("@ProvisionalDiagnosis", txtProvisionalDiagnosis.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@FinalDiagnosis", txtFinalDiagnosis.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@Operation", txtOperation.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@Doa", doa);
        SqlCmd.Parameters.AddWithValue("@Toa", toa);
        SqlCmd.Parameters.AddWithValue("@Ward", ddWard.SelectedValue.ToString());
        SqlCmd.Parameters.AddWithValue("@DepositAmt", txtDepositAmt.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@CreatedBy", uid);
        SqlCmd.Parameters.AddWithValue("@WardNo", ddWardNo.SelectedValue.ToString());
        SqlCmd.Parameters.AddWithValue("@BedNo", ddBedNo.SelectedValue.ToString());
        SqlCmd.Parameters.AddWithValue("@Remedy", DDL_RemList.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@RefBy", txtRefBy.Text.ToString());
        if (ipdid == 0)
        {
            SqlCmd.Parameters.Add("@ipdId", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.AddWithValue("@receiptNo", adReceiptNo);
            SqlCmd.Parameters.AddWithValue("@finYear", finyear);
        }
        SqlCmd.Parameters.AddWithValue("@ReconsultingDiagnosis", txtReconsultingDiagnosis.Text.ToString());
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);

        if (ViewState["wID"] != null)
        {
            int wID = Convert.ToInt32(ViewState["wID"]);
            SqlCmd.Parameters.AddWithValue("@wID", wID);
        }
        else
        {
            SqlCmd.Parameters.AddWithValue("@wID", 0);
        }

        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd.ExecuteNonQuery();
        if (ipdid == 0)
        {
            ViewState["ipdid"] = SqlCmd.Parameters["@ipdId"].Value.ToString();
            Session["ipdid"] = ViewState["ipdid"].ToString();
            Response.Redirect("IPDViewAks.aspx", false);

        }
        ClearFileds();
        con.Close();
        SqlCmd.Dispose();
        con.Dispose();
    }

    public void ClearFileds()
    {
        Session["Ward"] = null;
        Session["WaitingPatientID"] = null;
    }

    protected void ddWard_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddWard.SelectedValue == "1")
        //{
        //    ddWardNo.Items.Clear();
        //    ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        //    gen.FillDropDownList("select wmID,wmWardNo from tblWardMaster where wmType='GENERAL'", "wmID", "wmWardNo", ddWardNo);

        //}

        //if (ddWard.SelectedValue == "2")
        //{
        //    ddWardNo.Items.Clear();
        //    ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        //    gen.FillDropDownList("select wmID,wmWardNo from tblWardMaster where wmType='SEMI-SPECIAL'", "wmID", "wmWardNo", ddWardNo);

        //}

        //if (ddWard.SelectedValue == "3")
        //{
        //    ddWardNo.Items.Clear();
        //    ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        //    gen.FillDropDownList("select wmID,wmWardNo from tblWardMaster where wmType='SPECIAL'", "wmID", "wmWardNo", ddWardNo);
        //}

        //if (ddWard.SelectedValue == "4")
        //{
        //    ddWardNo.Items.Clear();
        //    ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        //    gen.FillDropDownList("select wmID,wmWardNo from tblWardMaster where wmType='DELUX'", "wmID", "wmWardNo", ddWardNo);
        //}

        ddWardNo.Items.Clear();
        ddWardNo.Items.Insert(0, new ListItem("Select", ""));
        gen.FillDropDownList("SELECT wmID , wmWardNo FROM tblWardMaster WHERE wmWardTypeId='" + ddWard.SelectedValue + "' And IsDelete = 0 ", "wmID", "wmWardNo", ddWardNo);

    }

    protected void ddWardNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddBedNo.Items.Clear();
        ddBedNo.Items.Insert(0, new ListItem("Select", ""));
        /////// ------ Commented On 03/06/2016 By Ashok--------//////////// 
        // strSQL = "sp_GetBed"; 
        /////// ------ Commented On 03/06/2016 By Ashok--------////////////
        strSQL = "sp_GetBed_New";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@wmID", ddWardNo.SelectedValue);
        //SqlCmd.Parameters.AddWithValue("@toDate", ToDate);
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
            dr = SqlCmd.ExecuteReader();
            SqlCmd.Dispose();
            DataTable dt = new DataTable();
            dt.Load(dr);
            foreach (DataRow drow in dt.Rows)
            {
                ddBedNo.Items.Add(new ListItem(drow["bmNoOfBads"].ToString(), drow["bmid"].ToString()));
            }
        }
        con.Close();
    }
}
