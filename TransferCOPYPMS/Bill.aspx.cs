﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Bill : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    SqlDataAdapter dapt;
    SqlDataReader Sqldr;
    string strSQL = "";
    genral gen = new genral();
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    string urole;
    string pdId = "0";
    string uid = "0";
    string VStat = "";
    //string ipdid = "0";
    DataSet ds;
    //added by nikita on 12th march 2015------------------------------
    public int ipdid
    {
        get 
        {
            if (ViewState["ipdid"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["ipdid"].ToString());
        }
    }
    //-------------------------------------------------------------
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();
        else
            Response.Redirect("Login.aspx?flag=1");

        if (!IsPostBack)
        {
            if (Session["BillPrintIpdId"] != null)
                ViewState["ipdid"] = Session["BillPrintIpdId"].ToString();
            else
                Response.Redirect("ManageBill.aspx?flag=1", false);

             if (Request.QueryString["V"] != null)
                 VStat = Request.QueryString["V"].ToString();


            FillData();
        }        
    }
    protected void FillData()
    {
        try
        {
            //strSQL = "Select pdInitial+' '+pdFname+' '+pdMname+' '+ pdLname As Name,IsNull(pdAdd1,'')+' '+ISNULL(pdAdd2,'')+' '+ISNULL(pdAdd3,'') [Address],pdAge";
            //strSQL += ",pdSex,im.ipdAdmissionDate,im.ipdFinalDiagnosis,im.ipdDischargeDate,DATEDIFF(DAY,im.ipdAdmissionDate,im.ipdDischargeDate)+1 Days,im.ipdDepositAmount,im.ipdId,bm.bmNo,bmTotalAmt,[dbo].[fnNumToWords](bmTotalAmt,'Y') 'AmtWord',bmAdvance,(bmTotalAmt-bmAdvance) bmBalance ";
            //strSQL += " ,im.ipdPreparedBy,im.ipdCheckeddBy ";
            //strSQL += "From dbo.tblBillMaster bm INNER JOIN dbo.tblIpdMaster im ON bm.bmipdId = im.ipdId INNER JOIN dbo.tblPatientDetails pd ON pd.pdID = im.ipdpdId Where bm.bmipdId=" + ipdid;

            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "proc_GetPrintBillDetails";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Connection = con;
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCmd.Parameters.AddWithValue("@ipdid", ipdid);

            DataTable dt = new DataTable();
            dt = gen.getDataTable(SqlCmd);
            rptParent.DataSource = dt;
            rptParent.DataBind();

            BindIPDAdvanced();

        }
        catch (Exception ex)
        {

        }
    }
    protected void rptParent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            SqlCmd = new SqlCommand();
            SqlCmd.CommandText = "sp_GenerateBill";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Connection = con;
            SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
            dapt = new SqlDataAdapter(SqlCmd);
            DataTable dt = new DataTable();
            dapt.Fill(dt);
            DataView dv = new DataView(dt);
            dv.RowFilter = "Particulars IS NOT NULL";
            Repeater childrpt = (Repeater)e.Item.FindControl("rptChild");
            childrpt.DataSource = dv;
            childrpt.DataBind();

            if (VStat == "N")
            {
                var tr_Adv = e.Item.FindControl("tr_Adv") as HtmlTableRow;
                var tr_Bal = e.Item.FindControl("tr_Bal") as HtmlTableRow;
                tr_Adv.Visible = false;
                tr_Bal.Visible = false;
                rd_AdvTitle.Visible = false;
                rd_AdvData.Visible = false;
            }
            else
            {
                var tr_Adv = e.Item.FindControl("tr_Adv") as HtmlTableRow;
                var tr_Bal = e.Item.FindControl("tr_Bal") as HtmlTableRow;
                tr_Adv.Visible = true;
                tr_Bal.Visible = true;
                rd_AdvTitle.Visible = true;
                rd_AdvData.Visible = true;
            }
            
          

        }
        catch(Exception ex)
        {
            
        }
    }

    private void BindIPDAdvanced()
    {
        string finyear = gen.GetCurrentFinYear();
        strSQL = "ManageAdvancedDetailsAks";
        SqlCmd = new SqlCommand(strSQL, con);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@DepID", "0");
        SqlCmd.Parameters.AddWithValue("@DOA", "");
        SqlCmd.Parameters.AddWithValue("@DepositAmt", 0);
        SqlCmd.Parameters.AddWithValue("@ipdId", ipdid);
        SqlCmd.Parameters.AddWithValue("@CreatedBy", 0);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", "");
        SqlCmd.Parameters.AddWithValue("@finyear", finyear);
        SqlCmd.Parameters.AddWithValue("@Flag", "GetDep");

        if (con.State == ConnectionState.Closed)
            con.Open();


        dr = SqlCmd.ExecuteReader();
        DataTable dt = new DataTable();
        dt.Load(dr);
        if (dt.Rows.Count > 0)
        {
            GRD_Adv.DataSource = dt;
            GRD_Adv.DataBind();
        }
        else
        {
            GRD_Adv.DataSource = null;
            GRD_Adv.DataBind();
        }
        con.Close();
        dr.Dispose();

    }



}
