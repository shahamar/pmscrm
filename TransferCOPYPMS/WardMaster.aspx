﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="WardMaster.aspx.cs" Inherits="WardMaster" Title="Ward Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />

    <script type="text/javascript">
        function Validate()
        {
            
            var txtWardNo = document.getElementById("<%= txtWardNo.ClientID %>").value;
            var DDWardType = document.getElementById("<%= DDWardType.ClientID %>").value;
            
            
            var Err = "";
                        
            if(txtWardNo == "")
            {
                Err += "Please Fill the WardNo";
                document.getElementById("<%= txtWardNo.ClientID %>").className = "field input-validation-error";
            }
            else
            {
                document.getElementById("<%= txtWardNo.ClientID %>").className = "field";
            }
            
            if(DDWardType == "")
            {
                Err += "Please Fill the WardType";
                document.getElementById("<%= DDWardType.ClientID %>").className = "field input-validation-error";
            }
            else
            {
                document.getElementById("<%= DDWardType.ClientID %>").className = "field";
            }
            
            
            
            if(Err != "")
            {
                return false;   
            }  
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 98%;">
                            <h2>
                                Ward Master</h2>
                            <div class="formmenu">
                                <div class="loginform">
                                    <ul class="quicktabs_tabs quicktabs-style-excel">
                                        <li class="qtab-Demo active first" id="li_1"><a class="qt_tab active" href="javascript:void(0)">
                                            Ward Master</a> </li>
                                        <li class="qtab-HTML last" id="li_9"><a class="qt_tab active" href="ManageWardMaster.aspx">
                                            Manage Ward</a> </li>
                                    </ul>
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="height: auto;">
                                        <table style="width: 100%; padding-left: 20px" cellspacing="0px" cellpadding="0px">
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: center; color: Red;">
                                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Ward Type*
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="DDWardType" runat="server" CssClass="field required">
                                                      <%--  <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="GENERAL" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="SEMI-SPECIAL" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="SPECIAL" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="DELUX" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="ICU" Value="5"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="valFname" runat="server" ControlToValidate="DDWardType"
                                                        Display="Dynamic" ErrorMessage="Ward Type is required." ValidationGroup="val">
                                                        <span class="error">Ward Type is required.</span></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Ward No. :*
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtWardNo" runat="server" CssClass="field required" MaxLength="50"
                                                        Style="float: left;"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="valCharges" runat="server" ControlToValidate="txtWardNo"
                                                        Display="Dynamic" ErrorMessage="Ward No. is required." ValidationGroup="val">
                                                        <span class="error">Ward No. is required.</span></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="reg_WardNo" runat="server" ControlToValidate="txtWardNo"
                                                        CssClass="field required" ValidationExpression="^[\s\S]{0,50}$" ValidationGroup="val">
                                                        <span class="error">Max 50 characters allowed</span></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>

                                            
                                            <tr>
                                                <td>
                                                    Ward Rate :
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txt_WardRate" runat="server" Text="0.00" CssClass="field required"
                                                        Style="float: left;"></asp:TextBox>
                                                    <asp:Label ID="lblOldRate" runat="server" Text="Label" Visible="false"></asp:Label>

                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>


                                             <tr>
                                                <td>
                                                    Ward Order :
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                     <asp:TextBox ID="txt_OrderNo" runat="server" CssClass="field required"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>


                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" OnClick="btnSave_Click"
                                                        Style="margin-top: 2px;" Text="Save" ValidationGroup="val" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
