﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="RecurrentComplaint1.aspx.cs" Inherits="RecurrentComplaint" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc1" %>

<%@ Register Src="tab.ascx" TagName="Tab" TagPrefix="tab1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat = "server" ID="UP1">
        <ContentTemplate>
        
    <table style="width:100%; background-color:#FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        Profarma Of Case Taking-Investigation</h2>
                    <div class="formmenu">
                        <div class="loginform">
                           <tab1:Tab ID="tabKCO" runat="server"></tab1:Tab>  
                            <ul class="quicktabs_tabs quicktabs-style-excel" style="border-left: 2px solid #E2E2E2;border-right: 2px solid #E2E2E2;">
                                <li class="qtab-Demo first" id="li_1">
                                    <asp:LinkButton ID="lnkKCO" runat="server" CssClass="qt_tab active" 
                                        PostBackUrl="~/ProfarmaOfCase.aspx">K/C/O</asp:LinkButton>
                                </li>
                                <li class="qtab-HTML" id="li_2">
                                    <asp:LinkButton ID="lnkInvestigation" runat="server" CssClass="qt_tab active" PostBackUrl="Investigation.aspx">Investigation</asp:LinkButton>
                                </li>
                                
                                <li class="qtab-HTML" id="li_3">
                                    <asp:LinkButton ID="lnkChiefCo" runat="server" CssClass="qt_tab active" PostBackUrl="~/ChiefCO.aspx">Chief C/O</asp:LinkButton>
                                </li>
                                
                                <li class="qtab-HTML active" id="li_4">
                                    <asp:LinkButton ID="lnkRecComplaint" runat="server" CssClass="qt_tab active" Enabled="False" >Any Recurrent Complaint</asp:LinkButton>
                                </li>
                                
                                
                                <li class="qtab-HTML" id="li_5">
                                    <asp:LinkButton ID="lnkPastHo" runat="server" CssClass="qt_tab active" 
                                        PostBackUrl="~/PastHO.aspx">Past H/o</asp:LinkButton>
                                </li>
                                
                                <li class="qtab-HTML" id="li_6">
                                    <asp:LinkButton ID="lnkFamilyHo" runat="server" CssClass="qt_tab active" 
                                        PostBackUrl="~/FamilyHo.aspx">Family H/o</asp:LinkButton>
                                </li>
                                
                                <li class="qtab-HTML" id="li_7">
                                    <asp:LinkButton ID="lnkPhysicalgenerals" runat="server" 
                                        CssClass="qt_tab active" PostBackUrl="~/PhysicalGenerals.aspx">Physical Generals</asp:LinkButton>
                                </li>
                                
                                <li class="qtab-HTML" id="li_8">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="qt_tab active" 
                                        PostBackUrl="~/Mind.aspx">Mind</asp:LinkButton>
                                </li>
                                
                                <li class="qtab-HTML last" id="li_9">
                                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="qt_tab active" 
                                        PostBackUrl="~/AF.aspx">A/F</asp:LinkButton>
                                </li>
                                
                                </ul>
                             
                             
                            
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                
                                            <div style="height:400px;">
                                            <cc1:Editor ID="kco" runat="server" />       
                                            </div>
                                            <div style="text-align: right;">
                                                <asp:Button ID="btnNext" runat="server" CssClass="textbutton b_submit" 
                                                    style="margin-top: 0px;" Text="Next" onclick="btnNext_Click" />
                                            </div>                   
                            </div>                                
                            <div class="clr">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

