﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="ManageReport.aspx.cs" Inherits="ManageReport" Title="Manage Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <link rel="stylesheet" type="text/css" href="css/radiotabstrip.css" />
    <script src="js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function cleartext() {
            document.getElementById("<%= txtSearchCase.ClientID %>").value = "";
        }
        function InitializeToolTip() {
            $(".gridViewToolTip").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {
                    return $($(this).next().html());
                },
                showURL: false
            });
        }

        $(function () {
            InitializeToolTip();
        });
    </script>
    <style type="text/css">
        #tbsearch td
        {
            text-align: left;
            vertical-align: middle;
        }
        
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        
        #gvcol div
        {
            margin-top: -10px;
        }
        
        
        .head1
        {
            width: 10%;
        }
        
        .head2
        {
            width: 15%;
        }
        
        .head3
        {
            width: 7%;
        }
        
        .head4
        {
            width: 10%;
        }
        
        .head4i
        {
            width: 15%;
        }
        
        .head5
        {
            width: 10%;
        }
        
        .head6
        {
            width: 10%;
        }
        
        .head7
        {
            width: 15%;
        }
        .head8
        {
            width: 5%;
            text-align: center;
        }
        #search label
        {
            font-weight: bold;
        }
        #search div
        {
            float: left;
        }
        /* Tool Tip Message */#tooltip
        {
            position: absolute;
            z-index: 3000;
            border: 1px solid #111;
            background-color: #FEE18D;
            padding: 5px;
            opacity: 0.85;
        }
        #tooltip h3, #tooltip div
        {
            margin: 0;
        }
        #tooltip table td
        {
            font-weight: normal;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        Manage Report</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto; border: 1.5px solid #E2E2E2;">
                                <table width="100%" cellspacing="0px" cellpadding="2px">
                                    <tr>
                                        <td>
                                            <table style="width: 100%; border: 1px solid #EFEFEF;">
                                                <tr>
                                                    <td class="tableh1">
                                                        <table id="tbsearch" style="width: 98%;">
                                                            <tr>
                                                                <td style="width: 14%">
                                                                    <asp:CheckBox ID="chkConsultingPeriod" runat="server" Text="Consulting Period :" />
                                                                </td>
                                                                <td style="width: 12%">
                                                                    <asp:TextBox ID="txtfrm" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                        runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                        width: 20px; height: 20px;" />
                                                                </td>
                                                                <td style="width: 2%">
                                                                    To
                                                                </td>
                                                                <td style="width: 12%">
                                                                    <asp:TextBox ID="txtto" runat="server" CssClass="field" Width="75px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                        Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                        width: 20px; height: 20px;" />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                                                        OnClick="btnView_Click" OnClientClick="aspnetForm.target ='_self';" />
                                                                </td>
                                                                <td style="width: 50%; color: Red; text-align: right;">
                                                                    <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div>
                                                            <div class="radiobuttoncontainer">
                                                                <asp:RadioButtonList ID="optTabs" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                                                    CssClass="radiobuttonlist">
                                                                    <asp:ListItem Text="K/C/O" Value="1" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Investigation" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Chief C/O" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="Past H/O" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="Family H/O" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="Physical Generals" Value="6"></asp:ListItem>
                                                                    <asp:ListItem Text="Mind" Value="7"></asp:ListItem>
                                                                    <asp:ListItem Text="A/F" Value="8"></asp:ListItem>
                                                                    <asp:ListItem Text="Thermal" Value="9"></asp:ListItem>
                                                                    <asp:ListItem Text="Remedy" Value="10"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                            <div class="radiobuttonbackground" style="width: 845px;">
                                                                <asp:TextBox ID="txtSearchCase" runat="server" Width="524px"></asp:TextBox>
                                                                <img src="images/cleartext.ico" onclick="cleartext()" style="cursor: pointer;" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                            <tr>
                                                                <th class="head2">
                                                                    Name
                                                                </th>
                                                                <th class="head3">
                                                                    Gender
                                                                </th>
                                                                <th class="head4">
                                                                    DOC
                                                                </th>
                                                                <th class="head4i">
                                                                    Address
                                                                </th>
                                                                <th class="head5">
                                                                    CasePaperNo
                                                                </th>
                                                                <th class="head6">
                                                                    CassetteNo
                                                                </th>
                                                                <th class="head7">
                                                                    Occupation
                                                                </th>
                                                                <th class="head1">
                                                                    Created By
                                                                </th>
                                                                <th class="head8">
                                                                    Action
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th class="head2">
                                                                    <asp:TextBox ID="txtname" runat="server" class="field" Width="160px"></asp:TextBox>
                                                                </th>
                                                                <th class="head3">
                                                                </th>
                                                                <th class="head4">
                                                                </th>
                                                                <th class="head4i">
                                                                    <asp:TextBox ID="txtAdd" runat="server" class="field" Width="150px"></asp:TextBox>
                                                                </th>
                                                                <th class="head5">
                                                                    <asp:TextBox ID="txtcasepaperno" runat="server" class="field" Width="90px"></asp:TextBox>
                                                                </th>
                                                                <th class="head6">
                                                                    <asp:TextBox ID="txtcassetteno" runat="server" class="field" Width="90px"></asp:TextBox>
                                                                </th>
                                                                <th class="head7">
                                                                    <asp:TextBox ID="txtOccupation" runat="server" class="field" Width="170px"></asp:TextBox>
                                                                </th>
                                                                <th class="head1">
                                                                </th>
                                                                <th class="head8">
                                                                    <div title="Filter" style="display: block; width: 20px; padding-left: 11px;">
                                                                        <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none; color: #FFF;
                                                                            text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click" OnClientClick="aspnetForm.target ='_self';">
                                                                <img src="images/filter.png" alt="Filter" style="border:0px;"/></asp:LinkButton>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="gvcol">
                                                        <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                            CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" ShowHeader="false" Width="100%"
                                                            OnRowCommand="GRV1_RowCommand" OnPageIndexChanging="GRV1_PageIndexChanging">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%--<a href="#" style="color: Black !Important">
                                                                            <%# Eval("pdName")%></a>--%>
                                                                        <asp:LinkButton ID="lnkpdName" runat="server" Text='<%# Eval("pdName")%>' CommandArgument='<%# Eval("pdID") %>'
                                                                        CommandName="case" Style="text-decoration: none; color: Blue; font-weight: normal;"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="7%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("pdSex")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("pdDOC", "{0:dd-MMM-yyyy}")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("pdAdd")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("pdCasePaperNo")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("pdCassetteNo")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("pdOccupation")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%# Eval("uName") %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgGenReport" runat="server" CommandArgument='<%# Eval("pdId") %>'
                                                                            CommandName="report" ImageUrl="Images/report.png" ToolTip="Click to Genrate Report"
                                                                            OnClientClick="aspnetForm.target ='_blank';" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
