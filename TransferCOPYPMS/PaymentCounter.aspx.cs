﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Drawing;

public partial class PaymentCounter : System.Web.UI.Page
{
    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    SqlDataReader Sqldr;
    string uid = "0";
    string RepType = "";
    string frm, to;
    int i = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            uid = Session["userid"].ToString();

        if (!IsPostBack)
        {
            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            //FillData();
            Bind_FollowUpPaymentDetails();
        }

    }

    //protected void FillData()
    //{
    //    DDL_Doctor.Items.Insert(0, new ListItem("Select", "%"));
    //    gen.FillDropDownList("Select uId , uFname +' '+ uMname +' '+ uLname As DNAME from tblUser Where IsDelete = 0 and uRole = 2 ", "uId", "DNAME", DDL_Doctor);
    // }

    private void Bind_FollowUpPaymentDetails()
    {
        string[] _frm;
        string[] _to;


        if (txtfrm.Text.Contains("/"))
        {
            _frm = txtfrm.Text.ToString().Split('/');
            _to = txtto.Text.ToString().Split('/');

            frm = _frm[2] + "/" + _frm[1] + "/" + _frm[0] + " 00:00:00";
            to = _to[2] + "/" + _to[1] + "/" + _to[0] + " 23:55:00";
        }

        if (txtfrm.Text.Contains("-"))
        {
            _frm = txtfrm.Text.ToString().Split('-');
            _to = txtto.Text.ToString().Split('-');

            frm = _frm[2] + "-" + _frm[1] + "-" + _frm[0] + " 00:00:00";
            to = _to[2] + "-" + _to[1] + "-" + _to[0] + " 23:55:00";
        }

        StrSQL = "Select ROW_NUMBER() over (Order By fdId desc) As SNO, fdId As fdId, pdInitial +' '+ pdFname +' '+ pdMname +' '+ pdLname As PNAME , pdCasePaperNo ";
        StrSQL += ",Rem_Per_Description As RemedyPeriod, Case When fdDoseType = 1 Then 'Regular(3 Times/Day)' when fdDoseType = 2 Then 'Double(6 Times/Day)' When fdDoseType = 3 Then 'Hourly(10 Times/Day)' When fdDoseType = 5 Then 'Liquid' When fdDoseType = 6 Then 'Single' else '' end As DoseType ";
        StrSQL += ",isNull(fdAddEmgAmt,0) As AddEmg , IsNull(ExtraDosAmt,0) As ExtraDos, Case When fdIsReconsulting = 0 Then isNull(fdCharges,0) when fdIsReconsulting = 1 Then IsNull(fdConsultingCharges,0) else 0 end As Charges ";
        StrSQL += ",[dbo].[GetPreviousBalanceAks] (fdPatientId,fdId) As PreviousBalance, isNull(fdAddEmgAmt,0) + IsNull(ExtraDosAmt,0) + IsNull(Case When fdIsReconsulting = 0 Then isNull(fdCharges,0) when fdIsReconsulting = 1 Then IsNull(fdConsultingCharges,0) else 0 end,0) + IsNull([dbo].[GetPreviousBalanceAks] (fdPatientId,fdId),0) As TotalAmt ";
        StrSQL += ",IsNull(PaidAmount, isNull(fdAddEmgAmt,0) + IsNull(ExtraDosAmt,0) + IsNull(Case When fdIsReconsulting = 0 Then isNull(fdCharges,0) when fdIsReconsulting = 1 Then IsNull(fdConsultingCharges,0) else 0 end,0) + IsNull([dbo].[GetPreviousBalanceAks] (fdPatientId,fdId),0)) As PaidAmount ";
        StrSQL += ",IsNull(FD.Payment_Mode,'Cash') As Payment_Mode, fdCreatedBy, Case When PaidAmount IS Null Then '0' else '1' end As Stat, CASE ISNULL(CAST(fdCharges AS VARCHAR),'') WHEN ''  THEN '' ELSE '<a href=''OPDBill.aspx?id='+CAST(fdId AS VARCHAR)+'&IsReconsulting='+ CAST(ISNULL(fdIsReconsulting,0) AS VARCHAR) +'''& target=_blank><img src=''images/print1.png'' /></a>' END AS 'PrintBill' ";
        StrSQL += "From tblPatientDetails PD Inner Join tblFollowUpDatails FD On PD.pdID = FD.fdPatientId Left Join tblRemedyPeriodMaster RPM On RPM.Rem_Per_ID = FD.fdRemedyPeriod Where IsPharmacyDone = 1 And ";

        if(frm != "" && to != "")
        {
            StrSQL += "fdDate Between '"+ frm +"' and '"+ to +"' ";
        }
        //if(DDL_Doctor.SelectedValue != "")
        //{
        //    StrSQL += "And fdCreatedBy Like '%"+ DDL_Doctor.SelectedValue +"%' ";
        //}
        if(txtSearchCPN.Text != "")
        {
            StrSQL += "And PD.pdCasePaperNo Like '%" + txtSearchCPN.Text + "%' ";
        }
        if(DDL_Pay_Mode.SelectedValue != "")
        {
            StrSQL += "And FD.Payment_Mode Like '%"+ DDL_Pay_Mode.SelectedValue +"%' ";
        }
        if(txtSearch.Text != "")
        {
            StrSQL += "AND PD.pdFname +' '+ PD.pdMname +' '+ PD.pdLname Like REPLACE('%" + txtSearch.Text.Trim() + "%', ' ' , '%') ";
        }

        StrSQL += "Order By fdId desc";
        DataTable dt = gen.getDataTable(StrSQL);
        if (dt.Rows.Count > 0)
        {
            GVD_FollowupPaymentDetails.DataSource = dt;
            GVD_FollowupPaymentDetails.DataBind();
        }
        else
        {
            GVD_FollowupPaymentDetails.EmptyDataText.ToString();
            GVD_FollowupPaymentDetails.DataBind();
        }
            //StrSQL = "Proc_GetFollowupPaymentDetails";
            //SqlCmd = new SqlCommand(StrSQL, conn);
            //SqlDataReader dr;
            //SqlCmd.CommandType = CommandType.StoredProcedure;

            //SqlCmd.Parameters.AddWithValue("@FrmDate", frm);
            //SqlCmd.Parameters.AddWithValue("@ToDate", to);
            //SqlCmd.Parameters.AddWithValue("@User", DDL_Doctor.SelectedValue);
            //SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
            //if (conn.State == ConnectionState.Closed)
            //{
            //    conn.Open();
            //    dr = SqlCmd.ExecuteReader();
            //    DataTable dt = new DataTable();
            //    dt.Load(dr);
            //    if (dt.Rows.Count > 0)
            //    {
            //        GVD_FollowupPaymentDetails.DataSource = dt;
            //        GVD_FollowupPaymentDetails.DataBind();
            //    }
            //    else
            //    {
            //        GVD_FollowupPaymentDetails.DataSource = null;
            //        GVD_FollowupPaymentDetails.DataBind();
            //    }
            //    conn.Close();
            //    dr.Dispose();
            //}
        }

    protected void btnView_Click(object sender, EventArgs e)
    {
        Bind_FollowUpPaymentDetails();
    }

    protected void GVD_FollowupPaymentDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "Payment")
        {
            try
            {
                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                TextBox txt_ChargesPaid = row.FindControl("txt_ChargesPaid") as TextBox;
                DropDownList DDL_Pay_Mode = row.FindControl("DDL_Pay_Mode") as DropDownList;
                Label lblResult = row.FindControl("lblResult") as Label;

                if (DDL_Pay_Mode.SelectedValue == "")
                {
                    lblResult.Visible = true;
                    lblResult.Text = "Compulsory";
                    lblResult.ForeColor = Color.DarkRed;
                    return;
                }
                

                decimal paidcharge = txt_ChargesPaid.Text == "" ? 0 : decimal.Parse(txt_ChargesPaid.Text);
                SqlCmd = new SqlCommand();
                SqlCmd.CommandText = "proc_SaveFollowupPaymentDetails";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Connection = con;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlCmd.Parameters.AddWithValue("@FDID", e.CommandArgument);
                SqlCmd.Parameters.AddWithValue("@PaidAmt", paidcharge);
                SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
                SqlCmd.Parameters.AddWithValue("@PaidBy", uid);
              
                i = SqlCmd.ExecuteNonQuery();
                con.Close();
                SqlCmd.Dispose();

                if (i > 0)
                {
                    lblMessage.Text = "Follow up Charges Saved Successfully";
                    lblMessage.ForeColor = Color.Green;
                    Bind_FollowUpPaymentDetails();
                    
                }
                else
                {
                    lblMessage.Text = "Follow up Charges Not Saved....!!!!!";
                    lblMessage.ForeColor = Color.DarkRed;
                }

            }
            catch
            { }
        }

        if (e.CommandName == "Allow")
        {
            GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
            TextBox txt_ChargesPaid = row.FindControl("txt_ChargesPaid") as TextBox;
            DropDownList DDL_Pay_Mode = row.FindControl("DDL_Pay_Mode") as DropDownList;
            ImageButton btnPayAmount = row.FindControl("btnPayAmount") as ImageButton;

            txt_ChargesPaid.Enabled = false;
            DDL_Pay_Mode.Enabled = true;
            btnPayAmount.Visible = true;
        }
        
    }

    protected void GVD_FollowupPaymentDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList DDL_Pay_Mode = (DropDownList)e.Row.FindControl("DDL_Pay_Mode");
            string lbl_Pay_Mode = (e.Row.FindControl("lbl_Pay_Mode") as Label).Text;
	    
            DDL_Pay_Mode.Items.FindByValue(lbl_Pay_Mode.ToString()).Selected = true;
	    

            Label lbl_Stat = (Label)e.Row.FindControl("lbl_Stat");
            TextBox txt_ChargesPaid = (TextBox)e.Row.FindControl("txt_ChargesPaid");
            ImageButton btnPayAmount = (ImageButton)e.Row.FindControl("btnPayAmount");
            txt_ChargesPaid.Enabled = false;
            if (lbl_Stat.Text == "1")
            {
                txt_ChargesPaid.Enabled = false;
                DDL_Pay_Mode.Enabled = false;
            }
            if (lbl_Stat.Text == "0")
            {
               txt_ChargesPaid.BackColor = Color.Yellow;	
            }

            if (DDL_Pay_Mode.SelectedValue != "")
            {
                btnPayAmount.Visible = false;
            }
        }
	
    }

    protected void GVD_FollowupPaymentDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVD_FollowupPaymentDetails.PageIndex = e.NewPageIndex;
        Bind_FollowUpPaymentDetails();
        
    }

    //protected void GVD_FollowupPaymentDetails_RowEditing(object sender, GridViewEditEventArgs e)
    //{

    //}



}