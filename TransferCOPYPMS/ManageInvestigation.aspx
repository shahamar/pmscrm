﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="ManageInvestigation.aspx.cs" Inherits="ManageInvestigation" Title="Manage Investigation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />

<script type="text/javascript" src="js/custom-form-elements.js"></script>

<style type="text/css">
#tbsearch td { text-align: left; vertical-align: middle;}
#tbsearch label
{
    display: inline;
    margin-top: 3px;
    position: absolute;
}
#tbhead th
{
    text-align: left;
    background: url(images/nav-back.gif) repeat-x top;
    color: #FFFFFF;
    padding-left: 2px;
    padding-top: 3px;
}
#gvcol div { margin-top: -10px;}
.head1 { width: 10%;}
.head2 { width: 10%;}
.head3 { width: 40%;}
.head4 { width: 30%;}
.head5 { width: 10%;}
.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 100%; text-align:center"> 
                            <h2>IPD Round Report</h2>
                            <div class="formmenu">
                            	<div class="loginform">                                   
                                	<div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">                                            
                                        <table  style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">
                                            <tr>
                                                <td>    
                                                    <table style="width:100%;" cellspacing="5px" cellpadding="5px">
                                                        <tr>
                                                            <td>
                                                               <asp:Label ID="Label1" runat="server" Text="Status :" Width="100px"></asp:Label>
                                                                   <asp:DropDownList ID="DDL_INDC" runat="server" CssClass="required field" Width="100px">
								   
                                                                   <asp:ListItem Value="1" Text="Severe"></asp:ListItem>
                                                                   <asp:ListItem Value="2" Text="Moderate"></asp:ListItem>
                                                                   <asp:ListItem Value="3" Text="Mild"></asp:ListItem>
                                                                   <asp:ListItem Value="4" Text="Relived"></asp:ListItem>
                                                               </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label3" runat="server" Text="Time Slot :" Width="100px"></asp:Label>
                                                                   <asp:DropDownList ID="DDL_TimeSlot" runat="server" CssClass="required field" Width="100px">
								   
                                                                   <asp:ListItem Value="Morning" Text="Morning"></asp:ListItem>
                                                                   <asp:ListItem Value="AfterNoon" Text="AfterNoon"></asp:ListItem>
                                                                   <asp:ListItem Value="Evening" Text="Evening"></asp:ListItem>
                                                                   <asp:ListItem Value="Night" Text="Night"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label2" runat="server" Text="Round Date :" Width="100px"></asp:Label>
                                                                <asp:TextBox ID="txt_InvDate" runat="server" Width="80px" style="padding-left:6px"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtInvDate_CalendarExtender" runat="server" Enabled="True"
                                                                   Format="dd/MM/yyyy" PopupButtonID="imgDOC" TargetControlID="txt_InvDate">
                                                                </cc1:CalendarExtender>
                                                            </td>
                                                            <td valign="bottom">
                                                                 <asp:Button ID="btnSave" runat="server" CssClass="textbutton b_submit" Text="View"
                                                                OnClick="lnkfilter_Click" ValidationGroup="val" />
                                                            </td>
                                                        </tr>                                             
                                                        <tr>                                               
                                                            <td colspan="4">
                                                                <asp:Label ID="lblGridHeader" runat="server" style="float:right; color:Red;"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr style="display:none;">
                                                            <td colspan="4">
                                                                <table id="tbhead" border="0" cellpadding="0" cellspacing="1" style="width: 100%">
                                                                    <tr style="display:none;">
                                                                        <th class="head5">Visite Time</th>
                                                                        <th class="head1">Ward No</th>
                                                                        <th class="head2">Bed No</th>
                                                                        <th class="head3">Patient Name</th>
                                                                        <th class="head4">Complaint</th>
                                                                    </tr>
                                                                    <tr style="display:none;">
                                                                        <th class="head5"></th>
                                                                        <th class="head1">
                                                                            <asp:TextBox ID="txtWard" runat="server" class="field" Width="100px"></asp:TextBox>
                                                                        </th>
                                                                        <th class="head2">
                                                                            <asp:TextBox ID="txtBedNo" runat="server" class="field" Width="100px"></asp:TextBox>
                                                                        </th>
                                                                        <th class="head3">
                                                                            <asp:TextBox ID="txtPatient" runat="server" class="field" Width="200px"></asp:TextBox>
                                                                        </th>
                                                                        <th class="head4">
                                                                            <div style="display: block;">
                                                                                <asp:LinkButton ID="lnkfilter" runat="server" Style="text-decoration: none; color: #FFF;
                                                                                    text-align: center; margin-left: 2px;" OnClick="lnkfilter_Click">
                                                                                    <img src="images/filter.png" style="border:0px;"/>Filter
                                                                                </asp:LinkButton>
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="gvcol" colspan="4">
                                                                <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False" 
                                                                 DataKeyNames="pdId" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20" Width="100%"  
                                                                 OnRowDataBound="GRV1_RowDataBound"  OnPageIndexChanging="GRV1_PageIndexChanging">
                                                                     <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="0%" ItemStyle-HorizontalAlign="Left"  HeaderText="Date" 
                                                                        Visible="false">
                                                                            <ItemTemplate>
                                                                                <%# Eval("InvDate", "{0:dd-MMM-yyyy}")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-Width="0%" ItemStyle-HorizontalAlign="Left"  HeaderText="Time" 
                                                                        Visible="false">
                                                                            <ItemTemplate>
                                                                                <%# Eval("VisitTime")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"  HeaderText="Ward/Bed">
                                                                            <ItemTemplate>
                                                                                <%# Eval("wmWardNo")%> / <%# Eval("bmNoOfBads")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-Width="0%" ItemStyle-HorizontalAlign="Left"  HeaderText="Bed" 
                                                                        Visible="false">
                                                                            <ItemTemplate>
                                                                                <%# Eval("bmNoOfBads")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"  HeaderText="Patient">
                                                                            <ItemTemplate>
                                                                               <%-- <a href="InvestigationHistory.aspx?PatientID=<%# Eval("pdid") %>" class="example7 abctd">
                                                                                    <%# Eval("PName")%> </a>--%>
                                                                                     <%# Eval("PName")%> 
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Diagnosis" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                          <ItemTemplate>
                                                                             <div style="overflow-y: scroll; height:80px; margin-top:0px; border:1; float:left; width:100%">
                                                                                <asp:Label ID="lblkco" runat="server" Width="20px"></asp:Label>
                                                                             </div>
                                                                          </ItemTemplate>
                                                                        </asp:TemplateField>
            
                                                                        <asp:TemplateField HeaderText="Chief C/O" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                                                          <ItemTemplate>
                                                                            <div style="overflow-y: scroll; height:80px; margin-top:0px; border:1; float:left; width:100%">
                                                                                <asp:Label ID="lblco" runat="server"></asp:Label>
                                                                            </div>
                                                                           </ItemTemplate>
                                                                        </asp:TemplateField>
            
                                                                        <asp:TemplateField HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Left"  HeaderText="Fresh C/O">
                                                                            <ItemTemplate>
                                                                                <div style="overflow-y: scroll; height:80px; margin-top:0px; border:1; float:left; width:100%">
                                                                                <%# Eval("Complaint")%>
                                                                                 </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                           <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"  HeaderText="Remedy">
                                                                            <ItemTemplate>
                                                                                <%# Eval("RMNAME")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderText="Vitals (P|B|T|R)">
                                                                            <ItemTemplate>
                                                                             <asp:Label ID="lblcomName1" runat="server" Text='<%# Eval("Pulse")%>' 
                                                                                ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("PulseColor").ToString())%>'>
                                                                             </asp:Label> 
                                                                              |
                                                                             <asp:Label ID="Label1" runat="server" Text='<%# Eval("SYSBP")%>' 
                                                                               ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("SYSBPColor").ToString())%>'>
                                                                             </asp:Label> 
                                                                              /
                                                                             <asp:Label ID="Label2" runat="server" Text='<%# Eval("DiaBP")%>' 
                                                                                ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("DiaBPColor").ToString())%>'>
                                                                             </asp:Label> 
                                                                              |
                                                                             <asp:Label ID="Label3" runat="server" Text='<%# Eval("Temp")%>' 
                                                                                ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("TempColor").ToString())%>'>
                                                                             </asp:Label> 
                                                                              |
                                                                             <asp:Label ID="Label4" runat="server" Text='<%# Eval("RR")%>' 
                                                                                ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("RRColor").ToString())%>'>
                                                                             </asp:Label>                                                                       
                                                                              <%--  <%# Eval("Pulse")%> |   <%# Eval("BP")%> |    <%# Eval("Temp")%> |    <%# Eval("RR")%> --%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
            
                                                                        <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"  HeaderText="Sugg. Inv.">
                                                                            <ItemTemplate>
                                                                                <%# Eval("SuggInv")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
            
                                                                        <asp:TemplateField HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Left"  HeaderText="Doctor Name">
                                                                            <ItemTemplate>
                                                                                <%# Eval("DoctorNm")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
            
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="0%" HeaderText="Action" 
                                                                        Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("pdid") %>'
                                                                                    CausesValidation="false" CommandName="del" ToolTip="Delete" ImageUrl="~/images/delete.ico" />
                                                                                </asp:ImageButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>                                                
                                             		</table> 
                                                </td>
                                            </tr>
                                         </table>                                           
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
