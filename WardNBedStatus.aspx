﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="WardNBedStatus.aspx.cs" EnableEventValidation="false" Inherits="WardNBedStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <style type="text/css">
        #tbsearch td
        Pa
            text-align: left;
            vertical-align: middle;
        }
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        #gvcol div
        {
            margin-top: -10px;
        }
        .head1
        {
            width: 25%;
        }
        .head2
        {
            width: 20%;
        }
        .head3
        {
            width: 20%;
        }
        .head4
        {
            width: 25%;
        }
        .hidecontroll
        {
            display: none;
        }
        .padding
        {
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 100%; text-align: center">
                            <span style="text-align: center;">
                                <h2>
                                    Indoor Patient Department</h2>
                            </span>
                            <table width="100%" border="0">
                                <tr>
                                    <td width="40%" align="left">
                                        <asp:Label ID="lblcasepno" runat="server" Font-Size="14px" ForeColor="Green" Font-Bold="true"
                                            Text="Case Paper No. (For All Patient)"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="txtCasePaperno" runat="server" CssClass="field" Width="150px"></asp:TextBox>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btn_CaseSearch" runat="server" CssClass="textbutton b_submit" OnClick="btn_CaseSearch_Click"
                                            Style="margin-top: 2px;" Text="Search" />
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblPatNm" runat="server" Font-Size="14px" ForeColor="Green" Font-Bold="true"
                                            Text="Patient Name (For Admitted Patient)"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="txt_PatName" runat="server" CssClass="field" Width="350px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp
                                        <input type="hidden" id="hdnInsId" runat="server" />
                                        <asp:Button ID="btn_PatSearch" runat="server" CssClass="textbutton b_submit" OnClick="btn_PatSearch_Click"
                                            Style="margin-top: 2px;" Text="Search" />
                                    </td>
                                </tr>
                            </table>
                            <div class="formmenu">
                                <div class="loginform">
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="border: 0">
                                        <table style="width: 100%; background: #f2f2f2" cellspacing="10px" cellpadding="10px">
                                            <tr>
                                                <td align="center">
                                                    <asp:GridView ID="GVDWardNBedStatus" runat="server" AllowPaging="True" ShowHeader="false"
                                                        AllowSorting="true" AutoGenerateColumns="False" PageSize="100" Width="80%" GridLines="none"
                                                        OnRowDataBound="GVDWardNBedStatus_RowDataBound" EmptyDataText="No records found...">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="100%" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <%-- <%# Eval("wmWardNo")%>--%>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("WardTypeName")%>' ForeColor="DarkRed"
                                                                        Font-Bold="true" Font-Size="Medium"></asp:Label>
                                                                    <asp:Label ID="lblWardTypeID" runat="server" Text='<%# Eval("WardTypeID")%>' CssClass="hidecontroll">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-Width="0%" HeaderStyle-Width="0%">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td colspan="100%" colspan="2" valign="top">
                                                                            <div id="Ward-<%# Eval("WardTypeID") %>" style="display: block; position: relative;
                                                                                left: 0px; width: 100%; margin-top: 10px; margin-bottom: 10px; border: 1px dashed #d3d3d3">
                                                                                <asp:GridView ID="Grd_Ward" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                                                                    GridLines="Both" Width="100%" OnRowDataBound="Grd_Ward_RowDataBound">
                                                                                    <Columns>
                                                                                        <asp:TemplateField ItemStyle-Width="40%" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-BackColor="White">
                                                                                            <ItemTemplate>
                                                                                                <%-- <%# Eval("wmWardNo")%>--%>
                                                                                                <asp:Label ID="lblw1" runat="server" Text='<%# Eval("wmWardNo1")%>' ForeColor="DarkRed"
                                                                                                    Font-Size="22px" Style="padding: 10px"></asp:Label><br />
                                                                                                <asp:Label ID="lblWard1" runat="server" Text='<%# Eval("wmID1")%>' CssClass="hidecontroll">
                                                                                                </asp:Label><br />
                                                                                                <div id="Bed1-<%# Eval("wmID1") %>" style="display: block; position: relative; left: 0px;
                                                                                                    width: 100%;">
                                                                                                    <asp:GridView ID="Grd_Bed1" CellPadding="1" CellSpacing="1" runat="server" AutoGenerateColumns="False"
                                                                                                        ShowHeader="false" GridLines="None" OnRowCommand="Grd_Bed1_RowCommand" Width="100%">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField ItemStyle-Width="1%" ItemStyle-Font-Size="Larger" ItemStyle-HorizontalAlign="center">
                                                                                                                <ItemTemplate>
                                                                                                                    <%# Eval("bmNoOfBads")%>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField ItemStyle-Width="10%" ItemStyle-Font-Size="Larger" ItemStyle-HorizontalAlign="Left">
                                                                                                                <ItemTemplate>
                                                                                                                    <%--  <%# Eval("PName")%>--%>
                                                                                                                    <asp:LinkButton ID="lnkpdName1" runat="server" Text='<%# Eval("PName")%>' CommandArgument='<%# Eval("pdID") + ";" +Eval("ipdid")%>'
                                                                                                                        CommandName="IPDDetails" Style="text-decoration: none; color: black; font-weight: normal;">\
                                                                                                                    </asp:LinkButton>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField ItemStyle-Width="40%" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-BackColor="white">
                                                                                            <ItemTemplate>
                                                                                                <%-- <%# Eval("wmWardNo")%>--%>
                                                                                                <asp:Label ID="lblw2" runat="server" Text='<%# Eval("wmWardNo2")%>' ForeColor="DarkRed"
                                                                                                    Font-Size="22px" Style="padding: 10px"></asp:Label><br />
                                                                                                <asp:Label ID="lblWard2" runat="server" Text='<%# Eval("wmID2")%>' CssClass="hidecontroll">
                                                                                                </asp:Label><br />
                                                                                                <div id="Bed2-<%# Eval("wmID2") %>" style="display: block; position: relative; left: 0px;
                                                                                                    width: 100%;">
                                                                                                    <asp:GridView ID="Grd_Bed2" CellPadding="1" CellSpacing="1" runat="server" AutoGenerateColumns="False"
                                                                                                        ShowHeader="false" GridLines="None" OnRowCommand="Grd_Bed2_RowCommand" Width="100%">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField ItemStyle-Width="1%" ItemStyle-Font-Size="Larger" ItemStyle-HorizontalAlign="center">
                                                                                                                <ItemTemplate>
                                                                                                                    <%# Eval("bmNoOfBads")%>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:TemplateField ItemStyle-Width="10%" ItemStyle-Font-Size="Larger" ItemStyle-HorizontalAlign="Left">
                                                                                                                <ItemTemplate>
                                                                                                                    <%--  <%# Eval("PName")%>--%>
                                                                                                                    <asp:LinkButton ID="lnkpdName2" runat="server" Text='<%# Eval("PName")%>' CommandArgument='<%# Eval("pdID") + ";" +Eval("ipdid")%>'
                                                                                                                        CommandName="IPDDetails" Style="text-decoration: none; color: black; font-weight: normal;">
                                                                                                                    </asp:LinkButton>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                    </asp:GridView>
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="width: 100%; background: #f2f2f2">
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="width: 98%;">
                                                        <%--      style="color: DarkRed !important; font-weight:bold;"--%>
                                                        <h2>
                                                            Waiting Patient List <span class="fltrt"><a href="SearchPatient.aspx?AppType=W" class='example7'>
                                                                <asp:ImageButton ID="img_OPD" Width="60px" Style="vertical-align: middle; color: DarkRed !important;
                                                                    font-weight: bold;" runat="server" ImageUrl="~/images/add-button-blue-th.png"
                                                                    ToolTip="Add Patient" />
                                                            </a></span>
								<div id="export" runat="server" style="float: right; display: none;">
                                                                                  <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" Height="30"
                                                                                   OnClick="imgexport_Click" />
                                                                </div>
								
                                                        </h2>
							<asp:Panel ID="pnlHead" runat="server">
							<div class="formmenu">
                                                            <div class="loginform">
                                                                <div id="Div1" class="quicktabs_main quicktabs-style-excel" style="height: auto;">
                                                                    <div style="padding: 15px; height: auto;">
                                                                        <asp:Label ID="lblMsg" runat="server" ForeColor="DarkRed" Font-Bold="true" Font-Size="Medium"></asp:Label>
                                                                        <asp:GridView ID="GRVWaitingList" runat="server" AllowPaging="True" AllowSorting="true"
                                                                            AutoGenerateColumns="False" CssClass="mGrid" PagerStyle-CssClass="pgr" PageSize="20"
                                                                            ShowHeader="true" Width="100%" OnRowCommand="GRVWaitingList_RowCommand" OnRowDataBound="GRVWaitingList_RowDataBound"
                                                                            OnPageIndexChanging="GRVWaitingList_PageIndexChanging" EmptyDataText="No records found...">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Left" HeaderText="Sr No">
                                                                                    <ItemTemplate>
                                                                                        <%# Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderText="Date">
                                                                                    <ItemTemplate>
												<asp:TextBox ID="txtDOW" runat="server" Width="60%" CssClass="field" Text='<%# Eval("Date")%>'></asp:TextBox>
												<cc1:CalendarExtender ID="txtDOW_CalenderExtender" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgDOW" TargetControlID="txtDOW"></cc1:CalendarExtender>
												<asp:Image ID="imgDOW" runat="server" ImageUrl="~/images/calimg.gif" Style="position:inherit;width: 18px; height: 18px; top: 5px; left: -5px;" />
												<%--<asp:Label ID="lblDOW" runat="server" Visible="false" Text='<%# Eval("Date")%>'></asp:Label> --%>
                                                                                       <%-- <%# Eval("Date", "{0:dd/MM/yyyy}")%> --%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" HeaderText="Req. Room">
                                                                                    <ItemTemplate>
                                                                                        <asp:HiddenField ID="hdnWard" runat="server" Value='<%#Eval("wWardTypeId") %>' />
                                                                                        <asp:DropDownList ID="ddWard" runat="server" class="form-control select">
                                                                                        </asp:DropDownList>
											<asp:Label ID="lblResult" runat="server" Visible="false" ></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="35%" ItemStyle-HorizontalAlign="Left" HeaderText="Patient Name">
                                                                                    <ItemTemplate>
                                                                                        <asp:HiddenField ID="hdnwID" runat="server" Value='<%#Eval("wID") %>' />
                                                                                        <asp:LinkButton ID="lnkpdName" runat="server" Text=' <%# Eval("Name")%>' CommandArgument='<%# Eval("pdId")%>'
                                                                                            CommandName="IPD" Style="text-decoration: none; font-weight: normal;"
											    ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("WStatus").ToString())%>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Mobile No">
                                                                                    <ItemTemplate>
											<asp:TextBox ID="txtMob" runat="server" Width="90%" Value='<%# Eval("pdMob")%>' MaxLength="10" ></asp:TextBox>
											<asp:label ID="txtpdid" runat="server" Text='<%# Eval("pdId") %>' Style="display:none;" ></asp:label>
											
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="City">
                                                                                    <ItemTemplate>
                                                                                        <%# Eval("City")%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%" HeaderText="Action">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="btnDone" runat="server" CommandArgument='<%# Eval("wID") %>'
                                                                                            CommandName="done" ImageUrl="Images/button_ok.png" ToolTip="Done"/>
                                                                                        <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("wID") %>'
                                                                                            CommandName="editRow" ImageUrl="Images/edit.ico" ToolTip="Edit" Visible="false" />
                                                                                        <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("wID") %>'
                                                                                            CommandName="del" ImageUrl="Images/delete.ico" OnClientClick="return confirm('Are you sure you want to delete this?');"
                                                                                            ToolTip="Delete" />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
								
                                                                <div class="clr">
                                                                </div>
                                                            </div>
                                                        </div>
							</asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css"
        rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>--%>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });
        function SearchText() {
            $("#<%=txt_PatName.ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "WardNBedStatus.aspx/GetPatientDetails",
                        data: "{'PName':'" + document.getElementById('<%=txt_PatName.ClientID %>').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            $("#<%=hdnInsId.ClientID %>").val("");

                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('$')[1] + " ( " + item.split('$')[4] + " - " + item.split('$')[5] + " ) ",
                                    val: item.split('$')[0],
                                    val1: item.split('$')[1],
                                    val2: item.split('$')[2],
                                    val3: item.split('$')[3],
                                    val2: item.split('$')[4],
                                    val3: item.split('$')[5]
                                }
                            }))
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hdnInsId.ClientID %>").val(i.item.val);
                }
            });
        }  
    </script>
</asp:Content>
