﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="WaitingPatientStatus.aspx.cs" EnableEventValidation="false" Inherits="WaitingPatientStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <script type="text/javascript" src="js/custom-form-elements.js"></script>
    <style type="text/css">
        #tbsearch td
        Pa
            text-align: left;
            vertical-align: middle;
        }
        #tbsearch label
        {
            display: inline;
            margin-top: 3px;
            position: absolute;
        }
        #tbhead th
        {
            text-align: left;
            background: url(images/nav-back.gif) repeat-x top;
            color: #FFFFFF;
            padding-left: 2px;
            padding-top: 3px;
        }
        #gvcol div
        {
            margin-top: -10px;
        }
        .head1
        {
            width: 25%;
        }
        .head2
        {
            width: 20%;
        }
        .head3
        {
            width: 20%;
        }
        .head4
        {
            width: 25%;
        }
        .hidecontroll
        {
            display: none;
        }
        .padding
        {
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
               
                <tr>
                    <td>
                        <div class="login-area margin_ten_right" style="width: 100%; text-align: center">
                            
                            <div class="formmenu">
                                <div class="loginform">
                                    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                        style="border: 0">
                                        
                                        <table style="width: 100%; background: #f2f2f2">
                                            
                                            <tr>
                                                <td>
                                                    <div style="width: 98%;">
                                                        <%--      style="color: DarkRed !important; font-weight:bold;"--%>
                                                        <h2>
                                                            Waiting Patient List <span class="fltrt"><a href="SearchPatient.aspx?AppType=W" class='example7'>
                                                                <asp:ImageButton ID="img_OPD" Width="60px" Style="vertical-align: middle; color: DarkRed !important;
                                                                    font-weight: bold;" runat="server" ImageUrl="~/images/add-button-blue-th.png"
                                                                    ToolTip="Add Patient" />
                                                            </a></span>
								<div id="export" runat="server" style="float: right; display: none;">
                                                                                  <asp:ImageButton ID="imgexport" runat="server" ImageUrl="~/images/export.png" Height="30"
                                                                                   OnClick="imgexport_Click" />
                                                                </div>
								
                                                            <h2></h2>
                                                            <asp:Panel ID="pnlHead" runat="server">
                                                                <div class="formmenu">
                                                                    <div class="loginform">
                                                                        <div id="Div1" class="quicktabs_main quicktabs-style-excel" style="height: auto;">
                                                                            <div style="padding: 15px; height: auto;">
                                                                                <asp:Label ID="lblMsg" runat="server" Font-Bold="true" Font-Size="Medium" ForeColor="DarkRed"></asp:Label>
                                                                                <asp:GridView ID="GRVWaitingList" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False" CssClass="mGrid" EmptyDataText="No records found..." OnPageIndexChanging="GRVWaitingList_PageIndexChanging" OnRowCommand="GRVWaitingList_RowCommand" OnRowDataBound="GRVWaitingList_RowDataBound" PagerStyle-CssClass="pgr" PageSize="20" ShowHeader="true" Width="100%">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Sr No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                            <ItemTemplate>
                                                                                                <%# Container.DataItemIndex+1 %>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="txtDOW" runat="server" CssClass="field" Text='<%# Eval("Date")%>' Width="60%"></asp:TextBox>
                                                                                                <cc1:CalendarExtender ID="txtDOW_CalenderExtender" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgDOW" TargetControlID="txtDOW">
                                                                                                </cc1:CalendarExtender>
                                                                                                <asp:Image ID="imgDOW" runat="server" ImageUrl="~/images/calimg.gif" Style="position:inherit;width: 18px; height: 18px; top: 5px; left: -5px;" />
                                                                                                <%--<asp:Label ID="lblDOW" runat="server" Visible="false" Text='<%# Eval("Date")%>'></asp:Label> --%><%-- <%# Eval("Date", "{0:dd/MM/yyyy}")%> --%>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Req. Room" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="hdnWard" runat="server" Value='<%#Eval("wWardTypeId") %>' />
                                                                                                <asp:DropDownList ID="ddWard" runat="server" class="form-control select">
                                                                                                </asp:DropDownList>
                                                                                                <asp:Label ID="lblResult" runat="server" Visible="false"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Patient Name" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="35%">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="hdnwID" runat="server" Value='<%#Eval("wID") %>' />
                                                                                                <asp:LinkButton ID="lnkpdName" runat="server" CommandArgument='<%# Eval("pdId")%>' CommandName="IPD" ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("WStatus").ToString())%>' Style="text-decoration: none; font-weight: normal;" Text=' <%# Eval("Name")%>'></asp:LinkButton>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Mobile No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="txtMob" runat="server" MaxLength="10" Value='<%# Eval("pdMob")%>' Width="90%"></asp:TextBox>
                                                                                                <asp:Label ID="txtpdid" runat="server" Style="display:none;" Text='<%# Eval("pdId") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="City" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                                                                                            <ItemTemplate>
                                                                                                <%# Eval("City")%>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="btnDone" runat="server" CommandArgument='<%# Eval("wID") %>' CommandName="done" ImageUrl="Images/button_ok.png" ToolTip="Done" />
                                                                                                <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("wID") %>' CommandName="editRow" ImageUrl="Images/edit.ico" ToolTip="Edit" Visible="false" />
                                                                                                <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("wID") %>' CommandName="del" ImageUrl="Images/delete.ico" OnClientClick="return confirm('Are you sure you want to delete this?');" ToolTip="Delete" />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clr">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
								
                                                        </h2>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css"
        rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>--%>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
</asp:Content>
