﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;

public partial class CourierReport : System.Web.UI.Page
{

    SqlCommand SqlCmd;
    string StrSQL;
    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    string uid = "0";
    string tempDate = "";
    string tempUser = "";
    int Page_no = 0, Page_size = 20;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
        {
            uid = Session["userid"].ToString();
        }
        else
        {
            Response.Redirect("Login.aspx?flag=1");
        }

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        //StrSQL = "select * from Menu_Privilage where [User_ID] = " + uid + " and Menu_ID = 46";
        //bool exists = gen.doesExist(StrSQL);
        //if (exists == false)
        //    Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------

        if (!IsPostBack)
        {

            txtfrm.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            txtto.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();
            BindGridView();
        }

    }

    protected void FillData()
    {
        DDL_User.Items.Insert(0, new ListItem("Select", "%"));
        gen.FillDropDownList("Select uId , uFname +' '+ uMname +' '+ uLname As DNAME from tblUser Where IsDelete = 0 and uRole = 2", "uId", "DNAME", DDL_User);
        //DDL_User.SelectedValue = uid;

    }

    protected void GRV1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GRV1.PageIndex = e.NewPageIndex;
        Page_no = e.NewPageIndex + 1;
        BindGridView();
    }

    private void BindGridView()
    {
        string[] frmdate = txtfrm.Text.ToString().Split('/');
        string[] todate = txtto.Text.ToString().Split('/');
        string Frmdate = frmdate[2] + "/" + frmdate[1] + "/" + frmdate[0];
        string Todate = todate[2] + "/" + todate[1] + "/" + todate[0];
        StrSQL = "sp_GetCourierReport";
        SqlCmd = new SqlCommand(StrSQL, conn);
        SqlDataReader dr;
        SqlCmd.CommandType = CommandType.StoredProcedure;
        SqlCmd.Parameters.AddWithValue("@frmDate", Frmdate);
        SqlCmd.Parameters.AddWithValue("@toDate", Todate);
        SqlCmd.Parameters.AddWithValue("@Payment_Mode", DDL_Pay_Mode.SelectedValue);
        SqlCmd.Parameters.AddWithValue("@User", DDL_User.SelectedValue);
        if (conn.State == ConnectionState.Closed)
        {
            conn.Open();
            dr = SqlCmd.ExecuteReader();
            //SqlCmd.Dispose();
            DataTable dt = new DataTable();
            dt.Load(dr);
            if (dt.Rows.Count > 0)
            {
                pnlHead.Visible = true;
                rptHead.Visible = true;
                GRV1.DataSource = dt;
                GRV1.DataBind();
                decimal Total = 0;

                foreach (GridViewRow row in GRV1.Rows)
                {

                    Label lblsub = (Label)row.FindControl("lblPayment");
                    Total += Decimal.Parse(lblsub.Text.ToString());

                }
                decimal tot = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tot += decimal.Parse(dt.Rows[i]["cmdCharges"].ToString());
                }

                lblGrandTotal.Text = tot.ToString();
                lblSubTotal.Text = Total.ToString();
                export.Style.Add("display", "block");
                createpaging(dt.Rows.Count, GRV1.PageCount);
                decimal subtot = Decimal.Parse(lblSubTotal.Text);
                lblSubTotal.Text = subtot.ToString();
                Label1.Visible = true;
                lblSubTotal.Visible = true;
            }
            else
            {
                pnlHead.Visible = false;
                rptHead.Visible = false;
                export.Style.Add("display", "none");
                lblGridHeader.Text = "No record found";
            }
            SqlCmd.Dispose();
        }





    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        BindGridView();
        ScriptManager1.RegisterPostBackControl(this.imgexport);

    }
    private void GenerateUniqueData(int cellno)
    {

        //string tempdate = GRV1.Rows[0].Cells[0].Text.ToString();
        //for (int i = 1; i < GRV1.Rows.Count; i++)
        //{
        //    if (GRV1.Rows[i].Cells[cellno].Text.ToString() == tempdate)
        //    {
        //        GRV1.Rows[i].Cells[cellno].Text = string.Empty;

        //    }
        //    else
        //    {
        //        tempdate = GRV1.Rows[i].Cells[cellno].Text;
        //    }
        //}
    }

    protected void GRV1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    if (((Label)e.Row.FindControl("lbldate")).Text == tempDate)
        //    {
        //        e.Row.Cells[0].Text = string.Empty;
        //    }
        //    else
        //    {
        //        tempDate = ((Label)e.Row.FindControl("lbldate")).Text;
        //    }
        //}
    }
    protected void imgexport_Click(object sender, ImageClickEventArgs e)
    {
        Label1.Visible = false;
        lblSubTotal.Visible = false;
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "CourierReport.xls"));
        Response.ContentType = "application/vnd.ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GRV1.AllowPaging = false;
        this.BindGridView();
        GRV1.Columns[7].Visible = false;
        GRV1.HeaderRow.Style.Add("background-color", "#FFFFFF");

        pnlHead.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }


    protected void createpaging(int intTotalRecords, int intTotalPages)
    {
        int recEnd, recStart;

        if (Page_no == 0) { Page_no = 1; }
        if (intTotalRecords < (Page_no * Page_size))
        {
            recEnd = intTotalRecords;
        }
        else
        {
            recEnd = (Page_no * Page_size);
        }

        recStart = ((Page_no - 1) * Page_size) + 1;

        lblGridHeader.Text = "Showing Results " + recStart + " to " + recEnd + " of " + intTotalRecords;

    }
}
