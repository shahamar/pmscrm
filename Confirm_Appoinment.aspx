﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="Confirm_Appoinment.aspx.cs" Inherits="Confirm_Appoinment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<script type="text/javascript" src="js/custom-form-elements.js"></script>
<style type="text/css">
.AksDisplay {display:none;}
.loginform { font-family: Verdana;}
.left { float: left;}
.formmenu
{
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #E6E6E6;
    margin-bottom: 10px;
    padding: 10px;
}

#tbsearch td
{
    text-align: left;
    vertical-align: middle;
}
#tbsearch label
{
    display: inline;
    margin-top: 3px;
    position: absolute;
}
#tbhead th
{
    text-align: left;
    background: url(images/nav-back.gif) repeat-x top;
    color: #FFFFFF;
    padding-left: 2px;
    padding-top: 3px;
}
#gvcol div { margin-top: -10px;}
.head1 { width: 9%; font-size: small;}
.head2 { width: 18%; font-size: small;}
.head4 {  width: 10%; font-size: small;}
.head4i { width: 20%;}
.head5 { width: 12%; font-size: small;}
.head5i{ width: 12%; font-size: small;}
.head5ii { width: 20%;  font-size: small;}
.head6 { width: 10%; font-size: small;}

.textbox {width:70px; padding:4px;}
.textbox1 {width:90px; padding:4px;}
.datetime {position: absolute; width: 23px; height: 28px;}
</style>
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  
    <asp:UpdatePanel ID="UP1" runat="server">
        <ContentTemplate>
            <div>
                <table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="login-area margin_ten_right"  style="width: 100%;" cellspacing="0px" cellpadding="5px">
                              <span style="text-align:center;"><h2> Confirm Appointment</h2></span>
                                <div class="formmenu">
                                    <div class="loginform">
                                        <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                                            <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">
                                                <tr>
                                                    <td colspan="3">
                                                        <table id="tbsearch" style="width:100%;" cellspacing="5px" cellpadding="5px">
                                                            <tr>
                                                                <td style="width: 17%">
                                                                    <asp:CheckBox ID="chkConsultingPeriod" runat="server" Checked="true" 
                                                                    Text="<strong>Appointment Period :</strong>" Enabled="false" />
                                                                </td>
                                                                <td style="width: 12%">
                                                                    <asp:TextBox ID="txtfrm" runat="server" CssClass="textbox" ></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtfrm_CalendarExtender" Format="dd/MM/yyyy" PopupButtonID="imgfrm"
                                                                        runat="server" Enabled="True" TargetControlID="txtfrm">
                                                                    </cc1:CalendarExtender>&nbsp;
                                                                    <asp:Image ID="imgfrm" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime"  />
                                                                </td>
                                                                <td style="width: 4%"><strong>To :</strong></td>
                                                                <td style="width: 12%">
                                                                    <asp:TextBox ID="txtto" runat="server" CssClass="textbox" ></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="txtto_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtto"
                                                                        Format="dd/MM/yyyy" PopupButtonID="imgto">
                                                                    </cc1:CalendarExtender>&nbsp;
                                                                    <asp:Image ID="imgto" runat="server" ImageUrl="~/images/calimg.gif" CssClass="datetime" />
                                                                </td>
                                                                <td style="width: 8%">
                                                                    <asp:Button ID="btnView" runat="server" Text="View" CssClass="textbutton b_submit left"
                                                                        OnClick="btnView_Click" />
                                                                </td>
                                                                <td style="width: 7%"></td>
                                                                <td style="width: 40%; color: Red; text-align: right;">
                                                                    <asp:Label ID="lblGridHeader" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="display:none">
                                                                <td style="width: 16%">
                                                                    <asp:CheckBox ID="chkApType" runat="server" Checked="true" Text="Appointment Type :" />
                                                                </td>
                                                                <td style="width: 13%">
                                                                    <asp:DropDownList ID="ddlApType" runat="server" Enabled="false">
                                                                        <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="New" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Follow Up" Value="2"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <table class="mGrid" border="0" cellpadding="0" cellspacing="1" style="width: 100%; border: 1px solid #C1C1C1;">
                                                            <tr style="border: 1px solid #fff;">
                                                                <th class="head2">Name</th>
                                                                <th class="head4">DOC</th>
                                                                <th class="head4i">Address</th>
                                                                <th class="head5">Mobile No</th>
                                                                <th class="head6">Confirm Appoinment</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="head2">
                                                                    <asp:TextBox ID="txtname" runat="server" class="field" Width="250px"></asp:TextBox>
                                                                </th>
                                                                <th class="head4"></th>
                                                                 <th class="head4"></th>
                                                                <th class="head5">
                                                                    <asp:TextBox ID="txtmobno" runat="server" class="field" Width="180px"></asp:TextBox>
                                                                </th>
                                                                <th class="head6"></th>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <tr>
                                                        <td id="gvcol" colspan="3" style="width:100%; text-align: center;background:#fff">
                                                            <asp:Panel ID="pnlHead" runat="server" Visible="false">
                                                            <asp:GridView ID="GRV1" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                CssClass="mGrid" PagerStyle-CssClass="pgr" ShowHeader="false" Width="100%" OnRowCommand="GRV1_RowCommand"
                                                                OnPageIndexChanging="GRV1_PageIndexChanging" EmptyDataText="No Records Found." 
                                                                OnRowDataBound="GRV1_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="27.8%" ItemStyle-HorizontalAlign="center" HeaderText="Patient Name">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkpdName" runat="server" Text='<%# Eval("pdName")%>' 
                                                                            CommandArgument='<%# Eval("adid") %>'
                                                                                CommandName="history" Style="text-decoration: none; color: Blue; font-weight: normal;"
                                                                                OnClientClick="aspnetForm.target ='_blank';"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="13.2%" ItemStyle-HorizontalAlign="center" HeaderText="DOC">
                                                                        <ItemTemplate>
                                                                            <%# Eval("apDate", "{0:dd-MMM-yyyy}")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="26.1%" ItemStyle-HorizontalAlign="center" HeaderText="Address">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblcpno" runat="server" Text='<%# Eval("pdAdd1")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-Width="19.8%" ItemStyle-HorizontalAlign="center" HeaderText="Mobile No">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblcno" runat="server" Text='<%# Eval("pdMob")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                   <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="btnConfirm" runat="server" CommandName="Confirm" 
                                                                            CommandArgument='<%# Eval("adid") %>'
                                                                                ImageUrl="~/images/button_ok.png" ToolTip="Confirm Appoinmet"/>
                                                                                 <asp:ImageButton ID="btnDeleteApp" runat="server" CommandName="Delete" 
                                                                                 CommandArgument='<%# Eval("adid") %>'
                                                                                ImageUrl="~/images/cancel.ico" ToolTip="Cancel Appoinmet"/>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    
                                            </table>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

