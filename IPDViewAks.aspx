﻿<%@ Page Title="" Language="C#" validateRequest="false" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="IPDViewAks.aspx.cs" Inherits="IPDViewAks" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="tabipd.ascx" TagName="Tab" TagPrefix="tab1" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<link rel="stylesheet" type="text/css" href="css/tabmenu.css" />
<style type="text/css">
.divin
{ font:12px ,Arial;margin-left:156px;margin-top:-14px;position:inherit;}
.fontin
{
color: #767676;
font: 12px Verdana,Arial;
}
.coladd1
{
width: 15%;
}
.coladd2
{
width: 85%;
}
.style1
{
width: 15%;
}
.style2
{
width: 85%;
}
</style>
<script type="text/javascript" language="javascript">
function toggleSelection(source) {
//alert("toggleSelection");
$("#ctl00_ContentPlaceHolder1_GRV1 input[id*='cbAC']").each(function (index) {
$(this).attr('checked', source.checked);
});
}  
</script>
<script type="text/javascript" language="javascript">
function ChkValidate() {
var isValid = false;
var gridView = document.getElementById("ctl00_ContentPlaceHolder1_GRV1");
for (var i = 1; i < gridView.rows.length; i++) {
var inputs = gridView.rows[i].getElementsByTagName('input');
if (inputs != null) {
if (inputs[0].type == "checkbox") {
if (inputs[0].checked) {
    isValid = true;
    return true;
}
}
}
}
alert("Please select atleast one checkbox for Bill printing");
return false;
}
</script>

<script language="javascript" type = "text/javascript">
function logout(val) {
window.open('IPDViewAks.aspx?DISPROW=' + val);
}
</script>

<script type="text/javascript">

    function ConfirmPrint(FormDep) {

     
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Print?")) {

                confirm_value.value = "Yes";

                if (FormDep == "COMM_Print")
                { OPDPrintFunction(); }

                else {
                    confirm_value.value = "No";
                }
            }
            document.forms[0].appendChild(confirm_value);
    }


    function OPDPrintFunction() {


        setTimeout('DelayOPDPrintFunction()', 200);

    }

    function DelayOPDPrintFunction() {
        var myWindow = window.open("PrintReport.aspx");
        myWindow.focus();
        myWindow.print();
    }

</script>

 <script type="text/javascript">
     function CalculateDietCharges() {
         var DietGiven = document.getElementById("<%= DDL_DietGiven.ClientID %>").value;

         if (DietGiven == "YES") {
             document.getElementById("<%= txt_DietCharge.ClientID %>").value = 500;
         }
         else {
             document.getElementById("<%= txt_DietCharge.ClientID %>").value = 0;
         }

     }
 </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<table align="center" cellpadding="5" cellspacing="5" style="width: 100%; background-color: #FFFFFF;">
<tr>
<td>
</td>
</tr>
<tr>
<td>
<div class="login-area margin_ten_right" style="width: 98%;">
<h2>
IPD Details</h2>
<div class="formmenu">
<div class="loginform">
    <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
        style="height: auto; border: 1.5px solid #E2E2E2;">
        <table style="width: 100%;" cellspacing="5px" cellpadding="5px">

            <tr>
                <td>
                    <tab1:Tab ID="tabcon" runat="server" />
                </td>
            </tr>

             
           <tr runat="server" id="AksRow">
            <td>
                <ul class="quicktabs_tabs quicktabs-style-excel">
                <li class="qtab-HTML" id="li_lnkRound" runat="server">
                    <asp:LinkButton ID="lnkRound" runat="server" CssClass="qt_tab active" PostBackUrl="~/AddIPDRound.aspx?DISPROW=IR">IPD Round</asp:LinkButton>
                </li>

                  <li class="qtab-HTML" id="li_IpdRoundDetails" runat="server">
                    <asp:LinkButton ID="lnkIpdRoundDetails" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=MR">Manage Round</asp:LinkButton></li>

                <li class="qtab-HTML" id="li_lnkExtraP" runat="server">
                    <asp:LinkButton ID="lnkExtraP" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=EP">Extra Particulars</asp:LinkButton></li>

                <li class="qtab-HTML" id="li_lnkRemedies" runat="server">
                    <asp:LinkButton ID="lnkRemedies" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=RD">Remedies</asp:LinkButton></li>

                <li class="qtab-HTML " id="li_lnkDiag" runat="server">
                    <asp:LinkButton ID="lnkDiag" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=DG">Diagnosis</asp:LinkButton></li>

                <li class="qtab-HTML" id="li_lnkAdm"  runat="server">
                    <asp:LinkButton ID="lnkAdm" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=AD">Addmission</asp:LinkButton></li>

                <li class="qtab-HTML" id="li_lnkAdv"  runat="server">
            <asp:LinkButton ID="lnkAdv" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=ADV">Advanced</asp:LinkButton></li>

                <li class="qtab-HTML" id="li_lnkWard"  runat="server">
                    <asp:LinkButton ID="lnkWard" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=WD">Ward</asp:LinkButton>
                    </li>

                <li class="qtab-HTML" id="li_lnkRelBed" runat="server" style="display:none;">
                    <asp:LinkButton ID="lnkRelBed" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=RTB">Relative Bed</asp:LinkButton></li>  

                <li  class="qtab-HTML" id="li_lnkInv" runat="server">
                    <asp:LinkButton ID="lnkInv" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=IN">Investigation</asp:LinkButton></li>  

                <li  class="qtab-HTML" id="li_lnkDiet" runat="server">
                     <asp:LinkButton ID="lnkDiet" runat="server" CssClass="qt_tab active" PostBackUrl="~/IPDViewAks.aspx?DISPROW=DT">Diet Details</asp:LinkButton></li>  

		<li  class="qtab-Demo last" id="li_lnkVoucher" runat="server">
		     <asp:LinkButton ID="lnkVoucher" runat="server" CssClass="qt_tab active" postBackUrl="~/IPDViewAks.aspx?DISPROW=VD">Voucher</asp:LinkButton></li>


            </ul>
            </td>
            </tr>

              <tr>
                <td>
                  
                </td>
            </tr>
	    <tr id="Row_Remedies" runat="server">
                <td>
                    <fieldset>
                        <legend>Remedies Given</legend>
                        <asp:UpdatePanel ID="UP3" runat="server">
                            <ContentTemplate>
                                <table id="tblremedy" width="100%" cellspacing="5px" cellpadding="2px">
                                    <tr>
                                        <td style="text-align: center;">
                                            <asp:Panel ID="pnlremedy" runat="server">
                                                <asp:GridView ID="GRV3" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"  OnRowDataBound="GRV3_RowDataBound"
                                                    CssClass="mGrid" Width="60%" OnRowCancelingEdit="GRV3_RowCancelingEdit" OnRowEditing="GRV3_RowEditing"
                                                    OnRowUpdating="GRV3_RowUpdating">
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="Remedy" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="50%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblirRemedy" runat="server" Text='<%# Eval("RMNAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblirRemID" runat="server" Text='<%# Eval("irRemedy")%>' Visible = "false"></asp:Label>
                                                                <asp:HiddenField ID="hdnirId" runat="server" Value='<%# Eval("irId") %>' />
                                                                    <asp:DropDownList ID="DDL_RemList" runat="server" Width="170px">
                                                                    </asp:DropDownList>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblirStartDate" runat="server" Text='<%# Eval("irStartDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtStartDate" runat="server" Width="70%" Text='<%# Eval("irStartDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" PopupButtonID="imgDOO"
                                                                    TargetControlID="txtStartDate" Enabled="True" Format="dd-MMM-yyyy">
                                                                </cc1:CalendarExtender>
                                                                <asp:Image ID="imgDOO" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                    width: 20px; height: 20px;" />
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Edit">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnEditRemedy" runat="server" CommandArgument='<%# Eval("irId") %>'
                                                                    CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:ImageButton ID="btnEditCancel" runat="server" CommandArgument='<%# Eval("irId") %>'
                                                                    CommandName="cancel" ImageUrl="images/cancel.ico" ToolTip="Cancel" />
                                                                <asp:ImageButton ID="btnUpdate" runat="server" CommandArgument='<%# Eval("irId") %>'
                                                                    CommandName="update" ImageUrl="images/update.ico" ToolTip="Update" />
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                            <div id="divRemedy" style="color: #767676; padding: 15px;" runat="server">
                                                No Remedies Found
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div style="margin-bottom: -6px; float: right;" id="div_Remedy" runat="server">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="rpeditLf t11 edpd">
                                        <a href="AddIPDRemedy.aspx" class="example7">&nbsp;&nbsp;Add Remedy</a>
                                    </td>
                                    <td class="rpeditRt">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>

            <tr id="Row_Diagnosis" runat="server">
                <td>
                    <fieldset>
                        <legend>Diagnosis Details</legend>
                        <table id="tbldiag" width="100%" cellspacing="5px" cellpadding="2px">
                            <tr>
                                <td>
                                    <asp:Label ID="lblProvisionalDiagnosis" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFinalDiagnosis" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblReconsultingDiagnosis" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblOperation" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div style="margin-bottom: -6px; float: right;" id="div_Diagnosis" runat="server">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="rpeditLf t11 edpd">
                                        <a href="EditDiagnosis.aspx" class='example7'>&nbsp;&nbsp;Edit Section</a>
                                    </td>
                                    <td class="rpeditRt">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>

            <tr id="Row_Adm" runat="server">
                <td>
                    <fieldset>
                        <legend>Addmission/Discharge Details</legend>
                        <table id="tbladd" width="100%" cellspacing="5px" cellpadding="2px">
                            <tr>
                                <td class="style1">
                                    <font class="fontin">Addmission Date :</font>
                                </td>
                                <td class="style2">
                                    <asp:Label ID="lblDOA" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="coladd1">
                                    <font class="fontin">Addmission Time :</font>
                                </td>
                                <td class="coladd2">
                                    <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="coladd1">
                                    <font class="fontin">Ref By :</font>
                                </td>
                                <td class="coladd2">
                                    <asp:Label ID="lblRefBy" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="coladd1">
                                    <font class="fontin">Prepared By :</font>
                                </td>
                                <td class="coladd2">
                                    <asp:Label ID="lblPrepared" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="coladd1">
                                    <font class="fontin">Checked By :</font>
                                </td>
                                <td class="coladd2">
                                    <asp:Label ID="lblChecked" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="coladd1">
                                    <font class="fontin">Treatment :</font>
                                </td>
                                <td class="coladd2">
                                    <asp:Label ID="lblTreatment" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="coladd1">
                                    <font class="fontin">Discharge Date :</font>
                                </td>
                                <td class="coladd2">
                                    <asp:Label ID="lblDichargeDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="coladd1">
                                    <font class="fontin">Discharge Time : </font>
                                </td>
                                <td class="coladd2">
                                    <asp:Label ID="lblDischargeTime" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="coladd1">
                                    <font class="fontin">Deposited Amount :</font>
                                </td>
                                <td class="coladd2">
                                    <asp:Label ID="lblDepAmt" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div style="margin-bottom: -7px; float: right;" id="div_Addmission" runat="server">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="rpeditLf t11 edpd">
                                        <a href="EditAddmissionDetails.aspx" class='example7'>&nbsp;&nbsp;Edit Section</a>
                                    </td>
                                    <td class="rpeditRt">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>

            <tr id="Row_Ward" runat="server">
                <td>
                    <fieldset>
                        <legend>Ward Details</legend>
                        <asp:UpdatePanel runat="server" ID="UP1">
                            <ContentTemplate>
                                <table id="tblward" width="100%" cellspacing="5px" cellpadding="2px">
                                    <tr>
                                        <td style="text-align: center;">
                                            <asp:GridView ID="GRV2" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                CssClass="mGrid" OnRowCancelingEdit="GRV2_RowCancelingEdit" OnRowEditing="GRV2_RowEditing"
                                                OnRowUpdating="GRV2_RowUpdating" Width="80%" OnRowDataBound="GRV2_RowDataBound" OnRowCommand="GRV2_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Ward Type" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblwType" runat="server" Text='<%# Eval("WardTypeName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddwType" runat="server" CssClass="required field" Style="margin-top: 5px;
                                                                width: 150px;"  OnSelectedIndexChanged="ddwType_SelectedIndexChanged" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hdnwId" runat="server" Value='<%# Eval("wId") %>' />
                                                            <asp:HiddenField ID="hdnwType" runat="server" Value='<%# Eval("WardTypeID") %>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ward No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblWardNo" runat="server" Text='<%# Eval("wmWardNo")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtWardNo" runat="server" Text='<%# Eval("wmWardNo") %>' Visible="false"></asp:TextBox>
                                                                <asp:DropDownList ID="ddWardNo" runat="server" Width="150px" class="required field"
                                                                    OnSelectedIndexChanged="ddWardNo_SelectedIndexChanged" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            <asp:HiddenField ID="hdnWardNo" runat="server" Value='<%# Eval("wmID") %>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Bed No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblBedNo" runat="server" Text='<%# Eval("bmNoOfBads") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtBedNo" runat="server" Text='<%# Eval("bmNoOfBads") %>' Visible="false"></asp:TextBox>

                                                                <asp:DropDownList ID="ddBedNo" runat="server" Width="150px" class="required field">
                                                                </asp:DropDownList>

                                                                <asp:HiddenField ID="hdnBedNo" runat="server" Value='<%# Eval("bmID") %>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="In Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInDate" runat="server" Text='<%# Eval("wInDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtInDate" runat="server" Width="70%" Text='<%# Eval("wInDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtInDate_CalendarExtender" runat="server" PopupButtonID="imgDOIn"
                                                                TargetControlID="txtInDate" Enabled="True" Format="dd-MMM-yyyy">
                                                            </cc1:CalendarExtender>
                                                            <asp:Image ID="imgDOIn" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                width: 20px; height: 20px;" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Out Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOutDate" runat="server" Text='<%# Eval("wOutDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtOutDate" runat="server" Width="70%" Text='<%# Eval("wOutDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtOutDate_CalendarExtender" runat="server" PopupButtonID="imgDOOut"
                                                                TargetControlID="txtOutDate" Enabled="True" Format="dd-MMM-yyyy">
                                                            </cc1:CalendarExtender>
                                                            <asp:Image ID="imgDOOut" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                width: 20px; height: 20px;" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                      <asp:TemplateField HeaderText="Days" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDays" runat="server" Text='<%# Eval("WDays") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnEdit0" runat="server" CommandArgument='<%# Eval("wId") %>'
                                                                CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:ImageButton ID="btnEditCancel" runat="server" CommandArgument='<%# Eval("wId") %>'
                                                                CommandName="cancel" ImageUrl="images/cancel.ico" ToolTip="Cancel" />
                                                            <asp:ImageButton ID="btnUpdate" runat="server" CommandArgument='<%# Eval("wId") %>'
                                                                CommandName="update" ImageUrl="images/update.ico" ToolTip="Update" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnDelete0" runat="server" CommandArgument='<%# Eval("wId") %>'
                                                                CommandName="delete" ImageUrl="~/images/delete.ico" ToolTip="Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div id="divshiftward" runat="server" style="margin-bottom: -7px; float: right;">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="rpeditLf t11 edpd">
                                        <a href="ShiftWard.aspx" class='example7'>&nbsp;&nbsp;Shift Ward</a>
                                    </td>
                                    <td class="rpeditRt">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>

            <tr id="Row_Relatives" runat="server" style="display:none;">
                <td>
                    <fieldset>
                        <legend>Relative Bed Details</legend>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                            <ContentTemplate>
                                <table id="Table2" width="100%" cellspacing="5px" cellpadding="2px">
                                    <tr>
                                        <td style="text-align: center;">
                                            <asp:GridView ID="GRVRelative" runat="server" AllowPaging="True" AllowSorting="true"
                                                AutoGenerateColumns="False" CssClass="mGrid" OnRowCancelingEdit="GRVRelative_RowCancelingEdit"
                                                OnRowEditing="GRVRelative_RowEditing" OnRowUpdating="GRVRelative_RowUpdating"
                                                Width="80%" OnRowDataBound="GRVRelative_RowDataBound" OnRowCommand="GRVRelative_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Ward Type" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblwType" runat="server" Text='<%# Eval("_wType")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddrwType" runat="server" CssClass="required field" Style="margin-top: 5px;
                                                                width: 150px;">
                                                                <asp:ListItem Text="General" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Semi-Special" Value="2"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hdnrwId" runat="server" Value='<%# Eval("rwId") %>' />
                                                            <asp:HiddenField ID="hdnrwType" runat="server" Value='<%# Eval("rwType") %>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ward No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblWardNo" runat="server" Text='<%# Eval("rwNo")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtrWardNo" runat="server" Text='<%# Eval("rwNo") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Bed No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblBedNo" runat="server" Text='<%# Eval("rwBedNo") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtrBedNo" runat="server" Text='<%# Eval("rwBedNo") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="In Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblInDate" runat="server" Text='<%# Eval("rwInDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtInDate" runat="server" Width="70%" Text='<%# Eval("rwInDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtInDate_CalendarExtender" runat="server" PopupButtonID="imgDOA"
                                                                TargetControlID="txtInDate" Enabled="True" Format="dd-MMM-yyyy">
                                                            </cc1:CalendarExtender>
                                                            <asp:Image ID="imgDOA" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                width: 20px; height: 20px;" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Out Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOutDate" runat="server" Text='<%# Eval("rwOutDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtOutDate" runat="server" Width="70%" Text='<%# Eval("rwOutDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtOutDate_CalendarExtender" runat="server" PopupButtonID="imgDOO"
                                                                TargetControlID="txtOutDate" Enabled="True" Format="dd-MMM-yyyy">
                                                            </cc1:CalendarExtender>
                                                            <asp:Image ID="imgDOO" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                width: 20px; height: 20px;" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnEdit0" runat="server" CommandArgument='<%# Eval("rwId") %>'
                                                                CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                            <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("rwId") %>'
                                                                CommandName="del" ImageUrl="Images/delete.ico" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete the relative bed details?');" />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:ImageButton ID="btnEditCancel" runat="server" CommandArgument='<%# Eval("rwId") %>'
                                                                CommandName="cancel" ImageUrl="images/cancel.ico" ToolTip="Cancel" />
                                                            <asp:ImageButton ID="btnUpdate" runat="server" CommandArgument='<%# Eval("rwId") %>'
                                                                CommandName="update" ImageUrl="images/update.ico" ToolTip="Update" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div id="divrelativward" runat="server" style="margin-bottom: -7px; float: right;">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="rpeditLf t11 edpd">
                                        <a href="RelativeShiftWard.aspx" class='example7'>&nbsp;&nbsp;Relative Bed</a>
                                    </td>
                                    <td class="rpeditRt">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>

            <tr id="Row_Inv" runat="server">
                <td>
                    <fieldset>
                        <legend>Investigation Done</legend>
                        <asp:UpdatePanel ID="UP2" runat="server">
                            <ContentTemplate>
                                <table id="Table1" width="100%" cellspacing="5px" cellpadding="2px">
                                    <tr>
                                        <td style="text-align: center;">
                                            <asp:Panel ID="pnlfeat" runat="server" Visible="False">
                                                <table cellpadding="2px" cellspacing="0px" width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="GRV1" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                                AllowPaging="false" CssClass="mGrid" CellPadding="3" Width="100%" OnRowCommand="GRV1_RowCommand">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <input id="cbAll" runat="server" onclick="toggleSelection(this);" type="checkbox" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <input id="cbAC" runat="server" type="checkbox" value='<%# Eval("itdId") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="20%" HeaderText="Test Name"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbltmTestName" runat="server" Text='<%# Eval("tmTestName")%>'></asp:Label>
                                                                            <asp:HiddenField ID="hdntmId" runat="server" Value='<%# Eval("itdTmid")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="78%" HeaderText="Conducted On"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbltmDate" runat="server" Text=' <%# Eval("itdtestdate")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderText="Action"
                                                                        ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <%--<asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("itdTmid") %>' CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />--%>
                                                                            <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("itdId") %>'
                                                                                CommandName="del" ImageUrl="Images/delete.ico" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete the test for all occurances?');" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <div id="divInv" style="color: #767676; padding: 15px;" runat="server">
                                                No Investigation Found
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div id="div_print" style="margin-bottom: -7px; float: right;" runat="server">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnPrint" runat="server" Text="Print Bill" CssClass="textbutton b_submit"
                                            OnClientClick="return ChkValidate()" OnClick="btnPrint_Click" />
                                    </td>

                                </tr>
                                <tr>
                                    <td class="rpeditLf t11 edpd" align="left">
                                        <a href="IPDTestDetails.aspx" class='example7'>Add IPD Test</a>
                                    </td>
                                        <td class="rpeditRt">
                                        &nbsp;&nbsp;
                                    </td>
                                    <td class="rpeditLf t11 edpd" align="right">
                                        <a href="AddIPDRound.aspx">IPD Round</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>

            <tr id="Row_Extra" runat="server">
                <td>
                    <fieldset>
                        <legend>Extra Particular</legend>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <table id="Table3" width="100%" cellspacing="5px" cellpadding="2px">
                                    <tr>
                                        <td style="text-align: center;">
                                            <asp:Panel ID="Panel1" runat="server">
                                                <table cellpadding="2px" cellspacing="0px" width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="GVD_EP" runat="server" AllowSorting="true" 
                                                                AutoGenerateColumns="false" OnRowCommand="GVD_EP_RowCommand"  
                                                                AllowPaging="false" CssClass="mGrid" CellPadding="3" Width="100%" 
                                                                onrowdatabound="GVD_EP_RowDataBound" 
                                                                onrowcancelingedit="GVD_EP_RowCancelingEdit" >
                                                                <Columns>

                                                                  <%--  <asp:BoundField DataField='<%# Container.DataItemIndex + 1 %>' HeaderText="Sr.No." ItemStyle-Width="5%" Visible="false" />--%>

                                                                   
                                                                     <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Left" HeaderText="Sr. No.">
                                                                        <ItemTemplate>
                                                                             <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="center" Width="8%" />
                                                                    </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderText="Particular">
                                                                        <ItemTemplate>
                                                                            <asp:DropDownList ID="DDL_ExtP" runat="server" Width="150px"  AutoPostBack="true" onselectedindexchanged="DDL_ExtP_SelectedIndexChanged">
                                                                            </asp:DropDownList>
                                                                            <asp:Label ID="lbl_ExtP" runat="server" Visible="false" Text='<%# Eval("ExtParticular")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="17%" />
                                                                    </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderText="Charge">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txt_Charge" Width="100px" Text='<%# Eval("Charge")%>' AppendDataBoundItems="true" Enabled="false"
                                                                                CssClass="roundexdbox"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderText="Quantity">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txt_Quantity" Width="100px" Text='<%# Eval("Quantity")%>' AppendDataBoundItems="true" AutoPostBack="true" OnTextChanged="txt_Quantity_TextChanged"
                                                                                CssClass="roundexdbox"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Given Date">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txt_GivenDate" Width="100px" Text='<%# Eval("GivenDate" , "{0:dd/MM/yyyy}")%>' AppendDataBoundItems="true"
                                                                                CssClass="roundedbox"></asp:TextBox>
                                                                                <br />
                                                                                    <cc1:CalendarExtender ID="CE_txt_GivenDate" runat="server"  Enabled="True" TargetControlID="txt_GivenDate"
                                                                                    Format="dd/MM/yyyy">
                                                                                    </cc1:CalendarExtender>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderText="Return Date">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txt_ReturnDate" Width="100px" Text='<%# Eval("ReturnDate" , "{0:dd/MM/yyyy}")%>' AppendDataBoundItems="true" AutoPostBack="true" OnTextChanged="txt_ReturnDate_TextChanged"
                                                                                CssClass="roundedbox" placeholder='<%# DateTime.Now.ToString("dd/MM/yyyy")%>'></asp:TextBox>
                                                                                <br />
                                                                                    <cc1:CalendarExtender ID="CE_txt_ReturnDate" runat="server"  Enabled="True" TargetControlID="txt_ReturnDate"
                                                                                    Format="dd/MM/yyyy">
                                                                                    </cc1:CalendarExtender>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:TemplateField>

                                                                     <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Left" HeaderText="Days">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txt_Days" Width="60px" Text='<%# Eval("Days")%>' AppendDataBoundItems="true" Enabled="false"
                                                                                CssClass="roundexdbox"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left" HeaderText="Total Charge">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox runat="server" ID="txt_TotalCharge" Width="100px" Text='<%# Eval("TotalCharge")%>' AppendDataBoundItems="true" Enabled="false"
                                                                                CssClass="roundexdbox"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:TemplateField>


                                                                        <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>

                                                                              <asp:ImageButton ID="btnOldPatAppCancel" runat="server" CommandName="Cancel" CommandArgument='<%# Container.DataItemIndex%>' 
                                                                                    ImageUrl="~/images/cancel.ico" ToolTip="Cancel Appoinmet"/>

                                                                            </ItemTemplate>
                                                                         </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>

                                                            </td>
                                                    </tr>
                                                    <tr>
                                                    <td align="left">
                                                        <asp:LinkButton ID="lnk_emp_rowAdd" runat="server" CausesValidation="False" OnClientClick="return validategrid()"
                    OnClick="lnk_emp_rowAdd_Click"><img src="images/add.png"/>&nbsp;Add</asp:LinkButton>
                                                    </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <div id="div1" style="color: #767676; padding: 15px;" runat="server">
                                                No Extra Particulars
                                                     <asp:Label ID="lblGtDt" runat="server" Text="dssf"></asp:Label>
                                          <asp:Label ID="lbllblRtDt" runat="server" Text=""></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div id="div2" style="margin-bottom: -7px; float: center;" runat="server">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="btn_SaveExtraP" runat="server" Text="Save" CssClass="textbutton b_submit" OnClick="btn_SaveExtraP_Click" />

                                     

                                    </td>
                                </tr>
                                                       
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>

            <tr id="Row_ManageRound" runat="server">
                <td>
                    <fieldset>
                        <legend>Manage Round</legend>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <table id="Table4" width="100%" cellspacing="5px" cellpadding="2px">
                                    <tr>
                                        <td style="text-align: center;">
                                            <asp:Panel ID="Panel2" runat="server">
                                                <asp:GridView ID="GVD_ManageRound" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                    CssClass="mGrid" Width="100%" OnRowCommand="GVD_ManageRound_RowCommand">
                                                   <Columns>
                                                                <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Left"  HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("InvDate", "{0:dd-MM-yyyy}")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Left"  HeaderText="Time">
                                                                    <ItemTemplate>
                                                                        <%# Eval("VisitTime")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"  HeaderText="Ward/Bed" Visible="false">
                                                                    <ItemTemplate>
                                                                        <%# Eval("wmWardNo")%> / <%# Eval("bmNoOfBads")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-Width="0%" ItemStyle-HorizontalAlign="Left"  HeaderText="Bed" Visible="false">
                                                                    <ItemTemplate>
                                                                        <%# Eval("bmNoOfBads")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"  HeaderText="Patient" Visible="false">
                                                                    <ItemTemplate>
                                                                        <a href="InvestigationHistory.aspx?PatientID=<%# Eval("pdid") %>" class="example7 abctd">
                                                                            <%# Eval("PName")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                               <asp:TemplateField HeaderText="Diagnosis" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Left" Visible="false">
                                                              <ItemTemplate>
                                                                 <div style="overflow-y: scroll; height:80px; margin-top:0px; border:1; float:left; width:100%">
                                                               <asp:Label ID="lblkco" runat="server" Width="20px"></asp:Label>
                                                                 </div>
                                                              </ItemTemplate>
                                                               </asp:TemplateField>

                                                               <asp:TemplateField HeaderText="Chief C/O" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Left"  Visible="false">
                                                              <ItemTemplate>
                                                                 <div style="overflow-y: scroll; height:80px; margin-top:0px; border:1; float:left; width:100%">
                                                               <asp:Label ID="lblco" runat="server"></asp:Label>
                                                                </div>
                                                               </ItemTemplate>
                                                               </asp:TemplateField>

                                                                 <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"  HeaderText="Vitals (P|B|T|R)">
                                                                    <ItemTemplate>
                                                                      <asp:Label ID="lblcomName1" runat="server" Text='<%# Eval("Pulse")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("PulseColor").ToString())%>'></asp:Label> 
                                                                      |
                                                                       <asp:Label ID="Label1" runat="server" Text='<%# Eval("SYSBP")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("SYSBPColor").ToString())%>'></asp:Label> 
                                                                      /
                                                                       <asp:Label ID="Label2" runat="server" Text='<%# Eval("DiaBP")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("DiaBPColor").ToString())%>'></asp:Label> 
                                                                      |
                                                                       <asp:Label ID="Label3" runat="server" Text='<%# Eval("Temp")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("TempColor").ToString())%>'></asp:Label> 
                                                                      |
                                                                       <asp:Label ID="Label4" runat="server" Text='<%# Eval("RR")%>' ForeColor='<%# System.Drawing.ColorTranslator.FromHtml(Eval("RRColor").ToString())%>'></asp:Label> 
                                                                      
                                                                      <%--  <%# Eval("Pulse")%> |   <%# Eval("BP")%> |    <%# Eval("Temp")%> |  <%# Eval("RR")%> --%>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="Left"  HeaderText="Fresh C/O">
                                                                    <ItemTemplate>
                                                                        <div style="overflow-y: scroll; height:40px; margin-top:0px; border:1; float:left; width:100%">
                                                                        <%# Eval("Complaint")%>
                                                                         </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                   <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"  HeaderText="Remedy">
                                                                    <ItemTemplate>
                                                                        <%# Eval("RMNAME")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                  <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Left"  HeaderText="Sugg. Inv." Visible="false">
                                                                    <ItemTemplate>
                                                                        <%# Eval("SuggInv")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%" HeaderText="Action">
                                                                <ItemTemplate>
                                                                <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("InvestID") %>' CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                    <asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("InvestID") %>'
                                                                        CausesValidation="false" CommandName="del" ToolTip="Delete" ImageUrl="~/images/delete.ico" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                          
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </fieldset>
                </td>
            </tr>

            <tr id="Row_Advanced" runat="server">
                <td>
                    <fieldset>
                        <legend>Advanced/Balance Details</legend>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <table id="Table5" width="100%" cellspacing="5px" cellpadding="2px">

                                <tr>
                                <td>
                                <table width="100%">
                                <tr>

                                <td>
                                Date :
                                </td>

                                <td>
                                <asp:TextBox ID="txtDOA" runat="server" CssClass="required field"
                                                             Width="70px"></asp:TextBox>
                                                        <cc1:CalendarExtender ID="txtDOA_CalendarExtender" runat="server" 
                                                            Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgDOA" 
                                                            TargetControlID="txtDOA">
                                                        </cc1:CalendarExtender>
                                                        <asp:Image ID="imgDOA" runat="server" ImageUrl="~/images/calimg.gif" 
                                                            style="position: absolute; width:20px; height:20px;" />
                                                            <br />
                                                        <asp:RequiredFieldValidator ID="valDOC" runat="server"  ValidationGroup="checkDep" Display="Dynamic"
                                                            ControlToValidate="txtDOA" CssClass="reqPos" 
                                                            ErrorMessage="Addmission Date is Required."><span class="error" 
                                                            style="margin-left:21px;">Addmission Date is Required.</span>
                                                        </asp:RequiredFieldValidator>
                                                        
                                                        <asp:RegularExpressionValidator ID="regDOA" runat="server"  ValidationGroup="checkDep" Display="Dynamic"
                                                            ControlToValidate="txtDOA" ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" >
                                                            <span class="error" style="margin-left:21px;">Please enter date in dd/mm/yyyy</span>    
                                                        </asp:RegularExpressionValidator>    
                                </td>

                                <td>
                               Amount :
                                </td>
                                <td>
                                 <asp:TextBox ID="txtDepositAmt" runat="server" CssClass="required field" Width="120px"> </asp:TextBox>
                                 <br />
                                                     <asp:RequiredFieldValidator ID="reqDepAmt" runat="server"  ValidationGroup="checkDep"
                                                            ControlToValidate="txtDepositAmt" CssClass="reqPos"  Display="Dynamic"
                                                            ErrorMessage="Please specify Ward."><span class="error">Please enter Deposit 
                                                        Amount.</span></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="regDepAmt" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtDepositAmt" ErrorMessage="Please Enter Only Numbers" 
                                                            ValidationExpression="^(-)?\d+(\.\d\d)?$" ValidationGroup="checkDep">
                                                        <span class="error">Please enter Only Numbers.</span>
                                                        </asp:RegularExpressionValidator>

                                 <asp:Label ID="lblDepID" runat="server" Text="0" Visible="false"></asp:Label>

                                </td>

                                <td>
                                Payment Mode* : 
                                </td>
                                <td>
                                 <asp:DropDownList ID="DDL_Pay_Mode" runat="server" Width="160px"
                                    CssClass="field">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                    <%--<asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>--%>
                                    <asp:ListItem Text="Card" Value="Card"></asp:ListItem>
                                      <asp:ListItem Text="Paytm" Value="Paytm"></asp:ListItem>
                                    <asp:ListItem Text="OnLine" Value="OnLine"></asp:ListItem>
                                    <asp:ListItem Text="UPI" Value="UPI"></asp:ListItem>
					<asp:ListItem Text="Free" Value="Free"></asp:ListItem>
                                </asp:DropDownList>

                                    <br />

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="DDL_Pay_Mode"
                                                        Display="Dynamic" ErrorMessage="Please specify Payment Mode" ValidationGroup="checkDep">
                                                        <span class="error" >Please specify Payment Mode</span></asp:RequiredFieldValidator>
                                </td>

                                
                                <td>
                                  <asp:Button ID="btnAdvSave" runat="server" CssClass="textbutton b_submit"  ValidationGroup="checkDep"
                                                         onclick="btnAdvSave_Click" style="margin-top: 2px;" Text="Save" />
                                </td>

                                </tr>

                               <%-- <tr>
                                <td colspan="6" align="center">
                                 
                                </td>
                                </tr>--%>
                                
                                </table>
                                </td>
                                </tr>

                                <tr>
                                        <td style="text-align: center;">
                                            <asp:Panel ID="Panel3" runat="server">
                                                <asp:GridView ID="GRD_Adv" runat="server" AllowPaging="false" AllowSorting="true" AutoGenerateColumns="False"
                                                    CssClass="mGrid" Width="100%" OnRowCommand="GRD_Adv_RowCommand" OnRowEditing="GRD_Adv_RowEditing">
                                                    <Columns>

                                                      <asp:TemplateField HeaderText="Dep. Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDepDate" runat="server" Text='<%# Eval("DepDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDepAmt" runat="server" Text='<%# Eval("DepAmt")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>

                                                              <asp:TemplateField HeaderText="Payment Mode" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="30%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPayment_Mode" runat="server" Text='<%# Eval("Payment_Mode")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            </asp:TemplateField>

                                                       <asp:TemplateField HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="btnEditDep" runat="server" CommandArgument='<%# Eval("DepID") %>'
                                                                    CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />

                                                               <asp:ImageButton ID="btnDeleteDep" runat="server" CommandArgument='<%# Eval("DepID") %>' OnClientClick="return confirm('Are you sure you want to delete this?');"
                                                                    CausesValidation="false" CommandName="del" ToolTip="Delete" ImageUrl="~/images/delete.ico" />

                                                                <%# Eval("PrintAdvRec") %>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                            <div id="div3" style="color: #767676; padding: 15px;" runat="server">
                                                No Advanced Found
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                      
                    </fieldset>
                </td>
            </tr>

            <tr id="Row_Diet" runat="server">
                <td>
                    <fieldset>
                        <legend>Diet</legend>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                        <table id="Table6" width="100%" cellspacing="5px" cellpadding="2px">

                        <tr>
                        <td colspan="4" align="center">
                         <asp:Label ID="lbl_Result" runat="server"></asp:Label>
                        </td>
                        </tr>

                            <tr>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text="Diet"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DDL_Diet" runat="server" Width="160px"
                                    CssClass="field">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Req" Value="Req"></asp:ListItem>
                                    <asp:ListItem Text="Not-Req" Value="Not-Req"></asp:ListItem>
                                </asp:DropDownList>
                                </td>
                                <td>
                               Diet Date
                                </td>
                                <td>
                                 <asp:TextBox ID="txt_DietDate" runat="server" CssClass="required field"
                                                             Width="70px"></asp:TextBox>
                                                             
                                  <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                            Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgDD" 
                                                            TargetControlID="txt_DietDate">
                                                        </cc1:CalendarExtender>
                                                        <asp:Image ID="imgDD" runat="server" ImageUrl="~/images/calimg.gif" 
                                                            style="position: absolute; width:20px; height:20px;" />
                                </td>
                            </tr>
                          
                            <tr>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text="Diet Given"></asp:Label>
                                </td>

                                <td>
                                    <asp:DropDownList ID="DDL_DietGiven" runat="server" Width="160px" onchange="CalculateDietCharges()"
                                    CssClass="field">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="YES" Value="YES"></asp:ListItem>
                                    <asp:ListItem Text="NO" Value="NO"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td>
                               Diet Given Date
                                </td>
                                <td>
                                 <asp:TextBox ID="txt_DietGivenDate" runat="server" CssClass="required field"
                                                             Width="70px"></asp:TextBox>
                                                             
                                  <cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                            Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgDGD" 
                                                            TargetControlID="txt_DietGivenDate">
                                                        </cc1:CalendarExtender>
                                                        <asp:Image ID="imgDGD" runat="server" ImageUrl="~/images/calimg.gif" 
                                                            style="position: absolute; width:20px; height:20px;" />
                                </td>

                            </tr>

                            <tr>
                            <td>
                            Diet Charge 
                            </td>

                            <td colspan="3">
                               <asp:TextBox ID="txt_DietCharge" runat="server" CssClass="required field" Text="0" Enabled="false"
                                                             Width="150px"></asp:TextBox>
                            </td>
                            </tr>

                            <tr>
                                <td colspan="4" align="center">
                                   <asp:Button ID="btn_Diet" runat="server" CssClass="textbutton b_submit"
                                                         onclick="btn_Diet_Click" style="margin-top: 2px;" Text="Save" />

                                    <asp:Button ID="btn_PrintDietForm" runat="server" CssClass="textbutton b_submit" OnClientClick="ConfirmPrint('COMM_Print');"
                                                         onclick="btn_PrintDietForm_Click" style="margin-top: 2px;" Text="Print" />
                                </td>
                            </tr>
                        </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </fieldset>
                </td>
            </tr>

            <tr style="display:none;">
                <td>
                    <fieldset>
                        <div style="margin-bottom: -6px; float: left;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnPrintIPD" runat="server" Text="IPD Paper" CssClass="textbutton b_submit"
                                            Width="67px" OnClick="btnPrintIPD_Click" OnClientClick="aspnetForm.target ='_blank';" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </td>
            </tr>
	    <tr id="Row_Voucher" runat="server">
		<td>
		<fieldset>
			<legend>Voucher</legend>
			<asp:UpdatePanel ID="UPVoucher" runat="server">
				<ContentTemplate>
					<table id="tblVoucher" with="100%" cellspacing="5px" cellpadding="2px">
					<tr>
						<td>
						<table width="100%">
						<tr>
							<td>Voucher Date :</td>
							<td><asp:TextBox ID="txtDOV" runat="server" CssClass="required field" width="70px"></asp:TextBox>
								<cc1:CalendarExtender ID="txtDOV_CalendarExtender" runat="server" 
                                                            Enabled="True" Format="dd/MM/yyyy" PopupButtonID="imgDOV" 
                                                            TargetControlID="txtDOV">
                                                        </cc1:CalendarExtender>
                                                        <asp:Image ID="imgDOV" runat="server" ImageUrl="~/images/calimg.gif" 
                                                            style="position: absolute; width:20px; height:20px;" />
                                                            <br />
                                                        <asp:RequiredFieldValidator ID="valDOV" runat="server"  ValidationGroup="checkDep" Display="Dynamic"
                                                            ControlToValidate="txtDOV" CssClass="reqPos" 
                                                            ErrorMessage="Addmission Date is Required."><span class="error" 
                                                            style="margin-left:21px;">Addmission Date is Required.</span>
                                                        </asp:RequiredFieldValidator>
                                                        
                                                        <asp:RegularExpressionValidator ID="regDOV" runat="server"  ValidationGroup="checkDep" Display="Dynamic"
                                                            ControlToValidate="txtDOV" ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" >
                                                            <span class="error" style="margin-left:21px;">Please enter date in dd/mm/yyyy</span>    
                                                        </asp:RegularExpressionValidator></td>
							<td style="padding-left:50px;">Enter Return Amount:</td>
							<td><asp:TextBox ID="txtReturnAmt" runat="server" width="80px" Text="0.00"></asp:TextBox></td>
							<td style="padding-left:50px;">Being</td>
							<td><asp:TextBox ID="txtBeing" runat="server" Width="150px" Text="Return access amount."></asp:TextBox></td>
							<td style="padding-left:50px;"><asp:Button ID="btnVoucherSave" runat="server" CssClass="textbutton b_submit" Text="Save" OnClick="btnVoucherSave_Click" /></td>
							<td><asp:Label ID="lblVouID" runat="server" Text="0" Visible="false"></asp:Label></td>
						</tr>
						<tr>
							<td colspan="7" style="text-align:center;">
								<asp:Panel ID="PanelVoucher" runat="server">
									<asp:GridView ID="GRV_Voucher" OnRowCommand="GRV_Voucher_RowCommand" OnRowEditing="GRD_Vou_RowEditing" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CssClass="mGrid" Width="100%"  runat="server">
									<Columns>
										<asp:TemplateField HeaderText="Voucher Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
											<ItemTemplate>
												<asp:Label ID="lblVouDate" runat="server" Text='<%# Eval("VoucherDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Being" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
											<ItemTemplate>
												<asp:Label ID="lblBeing" runat="server" Text='<%# Eval("Being")%>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Voucher Amount" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
											<ItemTemplate>
												<asp:Label ID="lblVouAmt" runat="server" Text='<%# Eval("ReturmAmt")%>'></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
											<ItemTemplate>
												<asp:ImageButton ID="btnEditVou" runat="server" CommandArgument='<%# Eval("VoucherID") %>' CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit"></asp:ImageButton>
												<asp:ImageButton ID="btnDeleteVou" runat="server" CommandArgument='<%# Eval("VoucherID") %>' OnClientClick="return confirm('Are you sure you want to delete this?');" CausesValidation="false" CommandName="del" ToolTip="Delete" ImageUrl="~/images/delete.ico" />
												<%# Eval("PrintVoucher") %>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									</asp:GridView>
								</asp:Panel>
								<div id="divVou" style="color: #767676; padding: 15px;" runat="server">
                                                			No Advanced Found
                                            			</div>
							</td>
						</tr>
						</table>
						</td>
					</tr>
					</table>
				</ContentTemplate>
			</asp:UpdatePanel>
		</fieldset>
		</td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</div>
</div>
</div>
</td>
</tr>
</table>


	<asp:Panel BorderStyle="Outset"  BorderWidth="5px" BorderColor="#C4D4B6" ID="PNLWorkSheet" runat="server" Visible="false">                                   
	<asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right"  ImageUrl="~/Images/FancyClose.png" />
    <asp:Panel ID="pnl1" runat="server" ScrollBars="Auto" Width="945px">
        <CR:CrystalReportViewer ID="CrystalReportViewer2" runat="server"  AutoDataBind="True" Height="500px" EnableParameterPrompt="False" 
            ReuseParameterValuesOnRefresh="True" ToolPanelView="None" GroupTreeImagesFolderUrl="" ToolbarImagesFolderUrl="" ToolPanelWidth="200px"
           	EnableDatabaseLogonPrompt="False" />
        <CR:CrystalReportSource ID="CrystalReportSource2" runat="server">
           <Report FileName="~/Reports/OpdReportAks.rpt"></Report>
        </CR:CrystalReportSource>
	</asp:Panel>
	</asp:Panel>

</asp:Content>

