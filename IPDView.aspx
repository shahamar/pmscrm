﻿<%@ Page Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true"
    CodeFile="IPDView.aspx.cs" Inherits="IPDView" Title="IPD View" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="tabipd.ascx" TagName="Tab" TagPrefix="tab1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/myGrid.css" />
    <style type="text/css">
        .divin
        { <%--margin-left:45px;font:12pxVerdana,Arial;--%>font:12pxVerdana,Arial;margin-left:156px;margin-top:-14px;position:inherit;}
        .fontin
        {
            color: #767676;
            font: 12px Verdana,Arial;
        }
        .coladd1
        {
            width: 15%;
        }
        .coladd2
        {
            width: 85%;
        }
        .style1
        {
            width: 15%;
        }
        .style2
        {
            width: 85%;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function toggleSelection(source) {
            //alert("toggleSelection");
            $("#ctl00_ContentPlaceHolder1_GRV1 input[id*='cbAC']").each(function (index) {
                $(this).attr('checked', source.checked);
            });
        }  
    </script>
    <script type="text/javascript" language="javascript">
        function ChkValidate() {
            var isValid = false;
            var gridView = document.getElementById("ctl00_ContentPlaceHolder1_GRV1");
            for (var i = 1; i < gridView.rows.length; i++) {
                var inputs = gridView.rows[i].getElementsByTagName('input');
                if (inputs != null) {
                    if (inputs[0].type == "checkbox") {
                        if (inputs[0].checked) {
                            isValid = true;
                            return true;
                        }
                    }
                }
            }
            alert("Please select atleast one checkbox for Bill printing");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%; background-color: #FFFFFF;">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 98%;">
                    <h2>
                        IPD Details</h2>
                    <div class="formmenu">
                        <div class="loginform">
                            <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel"
                                style="height: auto; border: 1.5px solid #E2E2E2;">
                                <table style="width: 100%;" cellspacing="0px" cellpadding="2px">
                                    <tr>
                                        <td colspan="2">
                                            <tab1:Tab ID="tabcon" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center; color: Red;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <fieldset>
                                                <legend>Remedies Given</legend>
                                                <asp:UpdatePanel ID="UP3" runat="server">
                                                    <ContentTemplate>
                                                        <table id="tblremedy" width="100%" cellspacing="5px" cellpadding="2px">
                                                            <tr>
                                                                <td style="text-align: center;">
                                                                    <asp:Panel ID="pnlremedy" runat="server">
                                                                        <asp:GridView ID="GRV3" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"  OnRowDataBound="GRV3_RowDataBound"
                                                                            CssClass="mGrid" Width="60%" OnRowCancelingEdit="GRV3_RowCancelingEdit" OnRowEditing="GRV3_RowEditing"
                                                                            OnRowUpdating="GRV3_RowUpdating">
                                                                            <Columns>

                                                                                <asp:TemplateField HeaderText="Remedy" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="50%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblirRemedy" runat="server" Text='<%# Eval("RMNAME")%>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <EditItemTemplate>
                                                                                       <asp:Label ID="lblirRemID" runat="server" Text='<%# Eval("irRemedy")%>' Visible = "false"></asp:Label>
                                                                                        <asp:HiddenField ID="hdnirId" runat="server" Value='<%# Eval("irId") %>' />
                                                                                          <asp:DropDownList ID="DDL_RemList" runat="server" Width="170px">
                                                                                          </asp:DropDownList>
                                                                                    </EditItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblirStartDate" runat="server" Text='<%# Eval("irStartDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <EditItemTemplate>
                                                                                        <asp:TextBox ID="txtStartDate" runat="server" Width="70%" Text='<%# Eval("irStartDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                                                        <cc1:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" PopupButtonID="imgDOO"
                                                                                            TargetControlID="txtStartDate" Enabled="True" Format="dd-MMM-yyyy">
                                                                                        </cc1:CalendarExtender>
                                                                                        <asp:Image ID="imgDOO" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                            width: 20px; height: 20px;" />
                                                                                    </EditItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Edit">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="btnEditRemedy" runat="server" CommandArgument='<%# Eval("irId") %>'
                                                                                            CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                                    </ItemTemplate>
                                                                                    <EditItemTemplate>
                                                                                        <asp:ImageButton ID="btnEditCancel" runat="server" CommandArgument='<%# Eval("irId") %>'
                                                                                            CommandName="cancel" ImageUrl="images/cancel.ico" ToolTip="Cancel" />
                                                                                        <asp:ImageButton ID="btnUpdate" runat="server" CommandArgument='<%# Eval("irId") %>'
                                                                                            CommandName="update" ImageUrl="images/update.ico" ToolTip="Update" />
                                                                                    </EditItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                    <div id="divRemedy" style="color: #767676; padding: 15px;" runat="server">
                                                                        No Remedies Found
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div style="margin-bottom: -6px; float: right;" id="div_Remedy" runat="server">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="rpeditLf t11 edpd">
                                                                <a href="AddIPDRemedy.aspx" class="example7">&nbsp;&nbsp;Add Remedy</a>
                                                            </td>
                                                            <td class="rpeditRt">
                                                                &nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <fieldset>
                                                <legend>Diagnosis Details</legend>
                                                <table id="tbldiag" width="100%" cellspacing="5px" cellpadding="2px">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblProvisionalDiagnosis" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblFinalDiagnosis" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblReconsultingDiagnosis" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblOperation" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div style="margin-bottom: -6px; float: right;" id="div_Diagnosis" runat="server">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="rpeditLf t11 edpd">
                                                                <a href="EditDiagnosis.aspx" class='example7'>&nbsp;&nbsp;Edit Section</a>
                                                            </td>
                                                            <td class="rpeditRt">
                                                                &nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <fieldset>
                                                <legend>Addmission Details</legend>
                                                <table id="tbladd" width="100%" cellspacing="5px" cellpadding="2px">
                                                    <tr>
                                                        <td class="style1">
                                                            <font class="fontin">Addmission Date :</font>
                                                        </td>
                                                        <td class="style2">
                                                            <asp:Label ID="lblDOA" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="coladd1">
                                                            <font class="fontin">Addmission Time :</font>
                                                        </td>
                                                        <td class="coladd2">
                                                            <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="coladd1">
                                                            <font class="fontin">Ref By :</font>
                                                        </td>
                                                        <td class="coladd2">
                                                            <asp:Label ID="lblRefBy" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="coladd1">
                                                            <font class="fontin">Prepared By :</font>
                                                        </td>
                                                        <td class="coladd2">
                                                            <asp:Label ID="lblPrepared" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="coladd1">
                                                            <font class="fontin">Checked By :</font>
                                                        </td>
                                                        <td class="coladd2">
                                                            <asp:Label ID="lblChecked" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="coladd1">
                                                            <font class="fontin">Treatment :</font>
                                                        </td>
                                                        <td class="coladd2">
                                                            <asp:Label ID="lblTreatment" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="coladd1">
                                                            <font class="fontin">Discharge Date :</font>
                                                        </td>
                                                        <td class="coladd2">
                                                            <asp:Label ID="lblDichargeDate" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="coladd1">
                                                            <font class="fontin">Discharge Time : </font>
                                                        </td>
                                                        <td class="coladd2">
                                                            <asp:Label ID="lblDischargeTime" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="coladd1">
                                                            <font class="fontin">Deposited Amount :</font>
                                                        </td>
                                                        <td class="coladd2">
                                                            <asp:Label ID="lblDepAmt" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div style="margin-bottom: -7px; float: right;" id="div_Addmission" runat="server">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="rpeditLf t11 edpd">
                                                                <a href="EditAddmissionDetails.aspx" class='example7'>&nbsp;&nbsp;Edit Section</a>
                                                            </td>
                                                            <td class="rpeditRt">
                                                                &nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <fieldset>
                                                <legend>Ward Details</legend>
                                                <asp:UpdatePanel runat="server" ID="UP1">
                                                    <ContentTemplate>
                                                        <table id="tblward" width="100%" cellspacing="5px" cellpadding="2px">
                                                            <tr>
                                                                <td style="text-align: center;">
                                                                    <asp:GridView ID="GRV2" runat="server" AllowPaging="True" AllowSorting="true" AutoGenerateColumns="False"
                                                                        CssClass="mGrid" OnRowCancelingEdit="GRV2_RowCancelingEdit" OnRowEditing="GRV2_RowEditing"
                                                                        OnRowUpdating="GRV2_RowUpdating" Width="80%" OnRowDataBound="GRV2_RowDataBound" OnRowCommand="GRV2_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Ward Type" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblwType" runat="server" Text='<%# Eval("WardTypeName")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:DropDownList ID="ddwType" runat="server" CssClass="required field" Style="margin-top: 5px;
                                                                                        width: 150px;"  OnSelectedIndexChanged="ddwType_SelectedIndexChanged" AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                    <asp:HiddenField ID="hdnwId" runat="server" Value='<%# Eval("wId") %>' />
                                                                                    <asp:HiddenField ID="hdnwType" runat="server" Value='<%# Eval("WardTypeID") %>' />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Ward No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblWardNo" runat="server" Text='<%# Eval("wmWardNo")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtWardNo" runat="server" Text='<%# Eval("wmWardNo") %>' Visible="false"></asp:TextBox>
                                                                                     <asp:DropDownList ID="ddWardNo" runat="server" Width="150px" class="required field"
                                                                                         OnSelectedIndexChanged="ddWardNo_SelectedIndexChanged" AutoPostBack="True">
                                                                                     </asp:DropDownList>
                                                                                  <asp:HiddenField ID="hdnWardNo" runat="server" Value='<%# Eval("wmID") %>' />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Bed No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblBedNo" runat="server" Text='<%# Eval("bmNoOfBads") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtBedNo" runat="server" Text='<%# Eval("bmNoOfBads") %>' Visible="false"></asp:TextBox>

                                                                                      <asp:DropDownList ID="ddBedNo" runat="server" Width="150px" class="required field">
                                                                                     </asp:DropDownList>

                                                                                      <asp:HiddenField ID="hdnBedNo" runat="server" Value='<%# Eval("bmID") %>' />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="In Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblInDate" runat="server" Text='<%# Eval("wInDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtInDate" runat="server" Width="70%" Text='<%# Eval("wInDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                                                    <cc1:CalendarExtender ID="txtInDate_CalendarExtender" runat="server" PopupButtonID="imgDOIn"
                                                                                        TargetControlID="txtInDate" Enabled="True" Format="dd-MMM-yyyy">
                                                                                    </cc1:CalendarExtender>
                                                                                    <asp:Image ID="imgDOIn" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                        width: 20px; height: 20px;" />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Out Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblOutDate" runat="server" Text='<%# Eval("wOutDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtOutDate" runat="server" Width="70%" Text='<%# Eval("wOutDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                                                    <cc1:CalendarExtender ID="txtOutDate_CalendarExtender" runat="server" PopupButtonID="imgDOOut"
                                                                                        TargetControlID="txtOutDate" Enabled="True" Format="dd-MMM-yyyy">
                                                                                    </cc1:CalendarExtender>
                                                                                    <asp:Image ID="imgDOOut" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                        width: 20px; height: 20px;" />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>

                                                                               <asp:TemplateField HeaderText="Days" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblDays" runat="server" Text='<%# Eval("WDays") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="btnEdit0" runat="server" CommandArgument='<%# Eval("wId") %>'
                                                                                        CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:ImageButton ID="btnEditCancel" runat="server" CommandArgument='<%# Eval("wId") %>'
                                                                                        CommandName="cancel" ImageUrl="images/cancel.ico" ToolTip="Cancel" />
                                                                                    <asp:ImageButton ID="btnUpdate" runat="server" CommandArgument='<%# Eval("wId") %>'
                                                                                        CommandName="update" ImageUrl="images/update.ico" ToolTip="Update" />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="btnDelete0" runat="server" CommandArgument='<%# Eval("wId") %>'
                                                                                        CommandName="delete" ImageUrl="~/images/delete.ico" ToolTip="Delete" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div id="divshiftward" runat="server" style="margin-bottom: -7px; float: right;">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="rpeditLf t11 edpd">
                                                                <a href="ShiftWard.aspx" class='example7'>&nbsp;&nbsp;Shift Ward</a>
                                                            </td>
                                                            <td class="rpeditRt">
                                                                &nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <fieldset>
                                                <legend>Relative Bed Details</legend>
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                                    <ContentTemplate>
                                                        <table id="Table2" width="100%" cellspacing="5px" cellpadding="2px">
                                                            <tr>
                                                                <td style="text-align: center;">
                                                                    <asp:GridView ID="GRVRelative" runat="server" AllowPaging="True" AllowSorting="true"
                                                                        AutoGenerateColumns="False" CssClass="mGrid" OnRowCancelingEdit="GRVRelative_RowCancelingEdit"
                                                                        OnRowEditing="GRVRelative_RowEditing" OnRowUpdating="GRVRelative_RowUpdating"
                                                                        Width="80%" OnRowDataBound="GRVRelative_RowDataBound" OnRowCommand="GRVRelative_RowCommand">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Ward Type" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblwType" runat="server" Text='<%# Eval("_wType")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:DropDownList ID="ddrwType" runat="server" CssClass="required field" Style="margin-top: 5px;
                                                                                        width: 150px;">
                                                                                        <asp:ListItem Text="General" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem Text="Semi-Special" Value="2"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                    <asp:HiddenField ID="hdnrwId" runat="server" Value='<%# Eval("rwId") %>' />
                                                                                    <asp:HiddenField ID="hdnrwType" runat="server" Value='<%# Eval("rwType") %>' />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Ward No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblWardNo" runat="server" Text='<%# Eval("rwNo")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtrWardNo" runat="server" Text='<%# Eval("rwNo") %>'></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Bed No" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblBedNo" runat="server" Text='<%# Eval("rwBedNo") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtrBedNo" runat="server" Text='<%# Eval("rwBedNo") %>'></asp:TextBox>
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="In Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblInDate" runat="server" Text='<%# Eval("rwInDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtInDate" runat="server" Width="70%" Text='<%# Eval("rwInDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                                                    <cc1:CalendarExtender ID="txtInDate_CalendarExtender" runat="server" PopupButtonID="imgDOA"
                                                                                        TargetControlID="txtInDate" Enabled="True" Format="dd-MMM-yyyy">
                                                                                    </cc1:CalendarExtender>
                                                                                    <asp:Image ID="imgDOA" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                        width: 20px; height: 20px;" />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Out Date" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="18%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblOutDate" runat="server" Text='<%# Eval("rwOutDate", "{0:dd-MMM-yyyy}")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:TextBox ID="txtOutDate" runat="server" Width="70%" Text='<%# Eval("rwOutDate", "{0:dd-MMM-yyyy}")%>'></asp:TextBox>
                                                                                    <cc1:CalendarExtender ID="txtOutDate_CalendarExtender" runat="server" PopupButtonID="imgDOO"
                                                                                        TargetControlID="txtOutDate" Enabled="True" Format="dd-MMM-yyyy">
                                                                                    </cc1:CalendarExtender>
                                                                                    <asp:Image ID="imgDOO" runat="server" ImageUrl="~/images/calimg.gif" Style="position: absolute;
                                                                                        width: 20px; height: 20px;" />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="btnEdit0" runat="server" CommandArgument='<%# Eval("rwId") %>'
                                                                                        CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />
                                                                                    <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("rwId") %>'
                                                                                        CommandName="del" ImageUrl="Images/delete.ico" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete the relative bed details?');" />
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <asp:ImageButton ID="btnEditCancel" runat="server" CommandArgument='<%# Eval("rwId") %>'
                                                                                        CommandName="cancel" ImageUrl="images/cancel.ico" ToolTip="Cancel" />
                                                                                    <asp:ImageButton ID="btnUpdate" runat="server" CommandArgument='<%# Eval("rwId") %>'
                                                                                        CommandName="update" ImageUrl="images/update.ico" ToolTip="Update" />
                                                                                </EditItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div id="divrelativward" runat="server" style="margin-bottom: -7px; float: right;">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="rpeditLf t11 edpd">
                                                                <a href="RelativeShiftWard.aspx" class='example7'>&nbsp;&nbsp;Relative Bed</a>
                                                            </td>
                                                            <td class="rpeditRt">
                                                                &nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <fieldset>
                                                <legend>Investigation Done</legend>
                                                <asp:UpdatePanel ID="UP2" runat="server">
                                                    <ContentTemplate>
                                                        <table id="Table1" width="100%" cellspacing="5px" cellpadding="2px">
                                                            <tr>
                                                                <td style="text-align: center;">
                                                                    <asp:Panel ID="pnlfeat" runat="server" Visible="False">
                                                                        <table cellpadding="2px" cellspacing="0px" width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:GridView ID="GRV1" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                                                        AllowPaging="false" CssClass="mGrid" CellPadding="3" Width="100%" OnRowCommand="GRV1_RowCommand">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
                                                                                                <HeaderTemplate>
                                                                                                    <input id="cbAll" runat="server" onclick="toggleSelection(this);" type="checkbox" />
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <input id="cbAC" runat="server" type="checkbox" value='<%# Eval("itdId") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="20%" HeaderText="Test Name"
                                                                                                ItemStyle-HorizontalAlign="Left">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbltmTestName" runat="server" Text='<%# Eval("tmTestName")%>'></asp:Label>
                                                                                                    <asp:HiddenField ID="hdntmId" runat="server" Value='<%# Eval("itdTmid")%>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="78%" HeaderText="Conducted On"
                                                                                                ItemStyle-HorizontalAlign="Left">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lbltmDate" runat="server" Text=' <%# Eval("itdtestdate")%>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderText="Action"
                                                                                                ItemStyle-HorizontalAlign="Center">
                                                                                                <ItemTemplate>
                                                                                                    <%--<asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("itdTmid") %>' CommandName="edit" ImageUrl="Images/edit.ico" ToolTip="Edit" />--%>
                                                                                                    <asp:ImageButton ID="btnDel" runat="server" CommandArgument='<%# Eval("itdId") %>'
                                                                                                        CommandName="del" ImageUrl="Images/delete.ico" ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to delete the test for all occurances?');" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                    <div id="divInv" style="color: #767676; padding: 15px;" runat="server">
                                                                        No Investigation Found
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div id="div_print" style="margin-bottom: -7px; float: right;" runat="server">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnPrint" runat="server" Text="Print Bill" CssClass="textbutton b_submit"
                                                                    OnClientClick="return ChkValidate()" OnClick="btnPrint_Click" />
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td class="rpeditLf t11 edpd" align="left">
                                                              <a href="IPDTestDetails.aspx" class='example7'>Add IPD Test</a>
                                                            </td>
                                                              <td class="rpeditRt">
                                                                &nbsp;&nbsp;
                                                            </td>
                                                            <td class="rpeditLf t11 edpd" align="right">
                                                             <a href="AddIPDRound.aspx">IPD Round</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <fieldset>
                                                <div style="margin-bottom: -6px; float: left;">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnPrintIPD" runat="server" Text="IPD Paper" CssClass="textbutton b_submit"
                                                                    Width="67px" OnClick="btnPrintIPD_Click" OnClientClick="aspnetForm.target ='_blank';" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
