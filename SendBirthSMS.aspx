﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHome1.master" AutoEventWireup="true" CodeFile="SendBirthSMS.aspx.cs" Inherits="SendBirthSMS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<link rel="stylesheet" type="text/css" href="css/myGrid.css" />
<script type="text/javascript">

    function SelectAllCheckboxes(chk) {
        $('#<%=gridBirthList.ClientID %>').find("input:checkbox").each(function () {
            if (this != chk) {
                this.checked = chk.checked;
            }
        });
    }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 	<table style="width: 100%; background-color: #FFFFFF;" cellspacing="0px" cellpadding="5px">
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <div class="login-area margin_ten_right" style="width: 100%; text-align:center;">
                   <h2><asp:Label ID="lblHeader" runat="server" Text="Birthday SMS"></asp:Label></h2>                
                   <div id="quicktabs_container_rounded_corner" class="quicktabs_main quicktabs-style-excel">
                      <table style="width:100%; background:#f2f2f2" cellspacing="0px" cellpadding="5px">    
                        <tr>
                            <td style="text-align: center;">
                                <font style="font-size: 14px; font-family: Verdana; font-weight: bold; color: Green;">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label></font>
                            </td>
                        </tr>                          
                        <tr>    
                            <td id="gvcol" align="center" style="width:100%; text-align: center;">
                              <center>
                                <asp:GridView ID="gridBirthList"  Width="100%" runat="server" AutoGenerateColumns="false" CssClass="mGrid" AllowPaging="false"> 
                                    <Columns>        
                                        <asp:TemplateField HeaderText="Select All" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left" ItemStyle-Width="15%">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkstat" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />Select
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkstat" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
    
                                        <asp:TemplateField HeaderText="pdid" Visible="false">
                                            <ItemTemplate>
                                                  <asp:Label ID="lblpdid" runat="server" Text='<% #Eval("pdid") %>'></asp:Label>  
                                            </ItemTemplate>
                                        </asp:TemplateField>
    
                                        <asp:TemplateField HeaderText="Patient Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left" >
                                            <ItemTemplate>
                                                 <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>     
                                            </ItemTemplate>
                                        </asp:TemplateField>
    
                                        <asp:TemplateField HeaderText="Case Paper No." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left" >
                                            <ItemTemplate>
                                                 <asp:Label ID="lblCasePaperNo" runat="server" Text='<%# Eval("CasePaperNo")%>'></asp:Label>     
                                            </ItemTemplate>
                                        </asp:TemplateField>
                         
                                        <asp:TemplateField HeaderText="Mobile No." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="left" >
                                            <ItemTemplate>
                                                 <asp:Label ID="lblpdMob" runat="server" Text='<%# Eval("pdMob")%>'></asp:Label>     
                                            </ItemTemplate>
                                        </asp:TemplateField>        
                                    </Columns>
                                </asp:GridView>
                              </center>
                            </td>
                       </tr>
                       <tr>
                            <td width="100%" align="center" valign="top">
                                <asp:Button Style="width: 60px; margin-right: 5px;" ID="btn_Send" runat="server" Text="Send"
                                CssClass="textbutton b_submit" onclick="btn_Send_Click" />
                            </td>
                       </tr> 
                       <tr>
                       		<td>&nbsp;</td>
                       </tr>   
                      </table>
                   </div>
                </div>
            </td>
        </tr>
    </table>
	
</asp:Content>
