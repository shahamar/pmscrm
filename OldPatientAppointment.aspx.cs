﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class OldPatientAppointment : System.Web.UI.Page
{
    //string pdId = "";
    string strSQL = "";
    SqlDataReader Sqldr;
    SqlCommand SqlCmd;
    SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["constring"]);
    genral gen = new genral();
    int userid;
    int DUID;
    //added by nikita on 12th march 2015------------------------------
    public int pdId
    {
        get
        {
            if (ViewState["pdId"] == null)
                return 0;
            else
                return Convert.ToInt32(ViewState["pdId"].ToString());
        }
    }
    //-----------------------------------------------------------------

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
            userid = int.Parse(Session["userid"].ToString());
        else
            Response.Redirect("Login.aspx?flag=1");

        //---Added By Nikita On 10thMarch 2015----------------------------------------------------
        strSQL = "select * from Menu_Privilage where [User_ID] = " + userid + " and Menu_ID = 25";
        bool exists = gen.doesExist(strSQL);
        if (exists == false)
            Response.Redirect("Home.aspx?status=1");
        //-----------------------------------------------------------------------------------------


        if (Request.QueryString["msg"] != null)
            lblmsg.Text = Request.QueryString["msg"].ToString();
        if (Request.QueryString["DID"] != null)
        {
            DUID = Convert.ToInt32(Request.QueryString["DID"].ToString());
        }

        if (!IsPostBack)
        {
            if (Session["CasePatientID"] != null)
                ViewState["pdId"] = Session["CasePatientID"].ToString();
            else
                Response.Redirect("Home.aspx?flag=1");


            txtDOC.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            FillData();

        }
    }
    private void FillData()
    {
        strSQL = "Select *,pdInitial +' '+ pdFname+' '+ pdMname +' '+ pdLname As 'pdName' From dbo.tblPatientDetails where pdID=" + pdId;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCmd = new SqlCommand(strSQL, con);
        Sqldr = SqlCmd.ExecuteReader();
        while (Sqldr.Read())
        {
            lblPatientName.Text = Sqldr["pdName"].ToString();
            lblPatientId.Text = Sqldr["pdPatientICardNo"].ToString();
            lblAge.Text = Sqldr["pdAge"].ToString();
            lblCasePaperNo.Text = Sqldr["pdCasePaperNo"].ToString();
            lblCassetteNo.Text = Sqldr["pdCassetteNo"].ToString();
            lblemail.Text = Sqldr["pdemail"].ToString();
            lblGender.Text = Sqldr["pdSex"].ToString();
            lblmob.Text = Sqldr["pdMob"].ToString();
            lbltele.Text = Sqldr["pdTele"].ToString();

            if (Sqldr["pdOccupation"] != DBNull.Value)
            {
                lblOccupation.Text = Sqldr["pdOccupation"].ToString();
            }
        }
        Sqldr.Close();
        SqlCmd.Dispose();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            //string[] _doc = txtDOC.Text.ToString().Split('/');
            //string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

            //strSQL = "INSERT INTO tblAppointmentDetails (PatientID,AppointmentDate,adCreatedBy) VALUES (" + pdId + ",'" + doc + "'," + Session["userid"] + ")";
            //gen.executeQuery(strSQL);
            //Response.Redirect("SetAppointment.aspx?msg=Appointment saved Successfully.", true);

            string[] _doc = txtDOC.Text.ToString().Split('/');
            string doc = _doc[2] + "/" + _doc[1] + "/" + _doc[0];

            strSQL = "InsAppoinmentDetails";

            SqlCmd = new SqlCommand(strSQL, con);
            SqlCmd.CommandType = CommandType.StoredProcedure;

            SqlCmd.Parameters.AddWithValue("@PatientID", pdId);
            SqlCmd.Parameters.AddWithValue("@AppointmentDate", doc);
            SqlCmd.Parameters.AddWithValue("@adCreatedBy", userid);
            SqlCmd.Parameters.AddWithValue("@App_Type", "CALL");

            SqlCmd.Parameters.AddWithValue("@pdInitial", "");
            SqlCmd.Parameters.AddWithValue("@pdFname", "");
            SqlCmd.Parameters.AddWithValue("@pdMname", "");
            SqlCmd.Parameters.AddWithValue("@pdLname", "");

            SqlCmd.Parameters.AddWithValue("@pdAdd1", "");
            SqlCmd.Parameters.AddWithValue("@pdAdd2", "");
            SqlCmd.Parameters.AddWithValue("@pdAdd3", "");
            SqlCmd.Parameters.AddWithValue("@pdAdd4", "");
            SqlCmd.Parameters.AddWithValue("@pdTele", "");
            SqlCmd.Parameters.AddWithValue("@pdMob", "");
            SqlCmd.Parameters.AddWithValue("@pdemail", "");

            SqlCmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.Add("@adid", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlCmd.Parameters.AddWithValue("@pdWeight", "");
            SqlCmd.Parameters.AddWithValue("@duid", DUID);
            SqlCmd.Parameters.AddWithValue("@Pat_Type", "O");

            if (con.State == ConnectionState.Closed)
                con.Open();
            int reslt;
            SqlCmd.ExecuteNonQuery();
            reslt = (int)SqlCmd.Parameters["@Error"].Value;

            if (reslt == 0)
            {
               // Response.Redirect("OldPatientAppointment.aspx?msg=Appointment saved successfully.");
                Response.Redirect("Home.aspx?MSG=Patient Appointment saved successfully");
            }
            SqlCmd.Dispose();
            con.Close();

        }
    }
}